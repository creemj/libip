# Pick up pre-existing config
-include Makefile.config

all:
	$(MAKE) -C src all

.PHONY: clean veryclean config check check_simd

# Detect architecture and new config if none was pre-existing.
include scripts/Makefile.arch

# Include OS specific features.
# These set CC, CSTD, CWARNINGS, CARCH and EXTRAINCLUDES
ifeq ($(KERNEL),Linux)
include scripts/Makefile.linux
WNOUNUSEDFUNCTION = -Wno-unused-function
endif

ifeq ($(KERNEL),Darwin)
include scripts/Makefile.macosx
WNOUNUSEDFUNCTION = -Wno-unused-function
endif

include scripts/Makefile.features

# pkg-config file
libip.pc: libip.a libip_pc_template Makefile
	sed -e s,--p--,$(PREFIX), -e s,--c--,$(INSTINCDIR), -e s,--l--,$(INSTLIB), libip_pc_template > libip.pc || rm -f libip.pc

libipdisp.pc: libip.a libipdisp.a libipdisp_pc_template Makefile
	sed -e s,--p--,$(PREFIX), -e s,--c--,$(INSTINCDIR), -e s,--l--,$(INSTLIB), libipdisp_pc_template > libipdisp.pc || rm -f libipdisp.pc


#
# Cleaning rules.
#


veryclean: clean
	$(MAKE) -C src veryclean
	$(MAKE) -C tests veryclean
	$(MAKE) -C utils veryclean
	rm -f config.h Makefile.config

clean:
	$(MAKE) -C src clean
	$(MAKE) -C tests clean
	$(MAKE) -C utils clean

all: config


#install: $(HEADERS)
#	cp $(HEADERS) $(INSTINC)

#
# For rebuilding the config file.
#

config: Makefile.config config.h

Makefile.config:
	@echo No Makefile.config present so recreating Makefile.config
	@echo "KERNEL = $(KERNEL)" > Makefile.config
	@echo "ARCH = $(ARCH)" >> Makefile.config
	@echo "MACHINE_NATURAL_SIZE = $(MACHINE_NATURAL_SIZE)" >> Makefile.config
	@echo "ENDIAN = $(ENDIAN)" >> Makefile.config
	@echo "CACHE_LINE_SIZE = $(CACHE_LINE_SIZE)" >> Makefile.config
	@echo "L1_CACHE_SIZE = $(L1_CACHE_SIZE)" >> Makefile.config
	@echo "L1_CACHE_ASSOC = $(L1_CACHE_ASSOC)" >> Makefile.config
	@echo "L2_CACHE_SIZE = $(L2_CACHE_SIZE)" >> Makefile.config
	@echo "L2_CACHE_ASSOC = $(L2_CACHE_ASSOC)" >> Makefile.config
	@echo "HAVE_64BIT = $(HAVE_64BIT)" >> Makefile.config
	@echo "HAVE_EFFICIENT_IDIV = $(HAVE_EFFICIENT_IDIV)" >> Makefile.config
	@echo "HAVE_SIMD = $(HAVE_SIMD)" >> Makefile.config
	@echo "HAVE_OPENMP = $(HAVE_OPENMP)" >> Makefile.config
	@echo "SIMD_VECTOR_SIZE = $(SIMD_VECTOR_SIZE)" >> Makefile.config
	@echo "CFLAGS_ARCH = $(CFLAGS_ARCH)" >> Makefile.config
	@echo "CFLAGS_SIMD = $(CFLAGS_SIMD)" >> Makefile.config
	@echo "CDEFS_SIMD = $(CDEFS_SIMD)" >> Makefile.config
	@echo "HDRS_SIMD = $(HDRS_SIMD)" >> Makefile.config
	@echo "Check Makefile.config contents:"
	@echo "------->"
	@cat Makefile.config
	@echo "<-------"

ifeq ($(HAVE_64BIT),y)
  outconf_64bit = "\#define HAVE_64BIT"
else
  outconf_64bit = "\#undef HAVE_64BIT"
endif
ifeq ($(HAVE_EFFICIENT_IDIV),y)
  outconf_idiv = "\#define HAVE_EFFICIENT_IDIV"
else
  outconf_idiv = "\#undef HAVE_EFFICIENT_IDIV"
endif
ifeq ($(HAVE_SIMD),y)
  outconf_simd = "\#define HAVE_SIMD"
  outconf_vsize = "\#define SIMD_VECTOR_SIZE $(SIMD_VECTOR_SIZE)"
else
  outconf_simd = "\#undef HAVE_SIMD"
  outconf_vsize = "\#undef SIMD_VECTOR_SIZE"
endif
ifeq ($(ENDIAN),little)
  outconf_endian = "\#define HAVE_LITTLE_ENDIAN"
else ifeq ($(ENDIAN),big)
  outconf_endian = "\#define HAVE_BIG_ENDIAN"
else
  outconf_endian = "/* Endianness of host not detected in config. */"
endif
ifeq ($(HAVE_OPENMP),y)
  outconf_openmp = "\#define HAVE_OPENMP"
else
  outconf_openmp = "\#undef HAVE_OPENMP"
endif

config.h: Makefile.config
	@echo "#define MACHINE_NATURAL_SIZE $(MACHINE_NATURAL_SIZE)" > config.h
	@echo "#define CACHE_LINE_SIZE $(CACHE_LINE_SIZE)" >> config.h
	@echo "#define L1_CACHE_SIZE $(L1_CACHE_SIZE)" >> config.h
	@echo "#define L1_CACHE_ASSOC $(L1_CACHE_ASSOC)" >> config.h
	@echo "#define L2_CACHE_SIZE $(L2_CACHE_SIZE)" >> config.h
	@echo "#define L2_CACHE_ASSOC $(L2_CACHE_ASSOC)" >> config.h
	@echo $(outconf_64bit) >> config.h
	@echo $(outconf_endian) >> config.h
	@echo $(outconf_idiv) >> config.h
	@echo $(outconf_simd) >> config.h
	@echo $(outconf_vsize) >> config.h
	@echo $(outconf_openmp) >> config.h

#
# Make tests (target check)
#

check_simd:
	make -C src check

check: check_simd
