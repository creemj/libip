# Detect arch
# If not provided by environment then try to detect.
# We recognise


# If a Debian like system easy to get system architecture
darch := $(shell dpkg-architecture -q DEB_HOST_ARCH 2>/dev/null || echo not)

ifeq ($(darch),not)

# Not a debian architecture so try uname which is unreliable...

uname_M := $(shell uname -m 2>/dev/null || echo not)
ARCH ?= $(shell echo $(uname_M) | sed -e s/i.86/i386/ -e s/armv[45]*/armel/ \
                                      -e s/arm[67].*/armhf/ -e s/aarch64.*/arm64/ \
                                      -e s/ppc.*/ppc/ -e s/alpha.*/alpha/)
else

ifeq ($(darch),amd64)
  ARCH ?= x86_64
else ifeq ($(darch),powerpc)
  ARCH ?= ppc
else
  ARCH ?= $(darch)
endif

ENDIAN ?= $(shell dpkg-architecture -q DEB_HOST_ARCH_ENDIAN)

endif

uname_S := $(shell uname -s 2>/dev/null || echo not)
KERNEL ?= $(shell echo $(uname_S))



ifeq ($(ARCH),x86_64)
HAVE_SIMD ?= y
ifdef USE_AVX2
CFLAGS_SIMD ?= -mavx -mavx2
CDEFS_SIMD ?= -DHAVE_INTEL_AVX2
HDRS_SIMD ?= simd/simd.h simd/simd_intel_avx2.h
SIMD_VECTOR_SIZE ?= 32
else
CFLAGS_SIMD ?= -msse2
CDEFS_SIMD ?= -DHAVE_INTEL_SSE2
HDRS_SIMD ?= simd/simd.h simd/simd_intel_sse.h
SIMD_VECTOR_SIZE ?= 16
ifdef USE_SSSE3
USE_SSE3 = y
endif
ifdef USE_SSE41
USE_SSE3 = y
USE_SSSE3 = y
endif
ifdef USE_SSE42
USE_SSE3 = y
USE_SSSE3 = y
USE_SSE41 = y
endif
ifdef USE_SSE3
CFLAGS_SIMD += -msse3
CDEFS_SIMD += -DHAVE_INTEL_SSE3
endif
ifdef USE_SSSE3
CFLAGS_SIMD += -mssse3
CDEFS_SIMD += -DHAVE_INTEL_SSSE3
endif
ifdef USE_SSE41
CFLAGS_SIMD += -msse4.1
CDEFS_SIMD += -DHAVE_INTEL_SSE41
endif
ifdef USE_SSE42
CFLAGS_SIMD += -msse4.2
CDEFS_SIMD += -DHAVE_INTEL_SSE42
endif
endif
MACHINE_NATURAL_SIZE ?= 8
CACHE_LINE_SIZE ?= 64
L1_CACHE_SIZE ?= 32
L1_CACHE_ASSOC ?= 8
L2_CACHE_SIZE ?= 256
L2_CACHE_ASSOC ?= 8
HAVE_64BIT ?= y
ENDIAN ?= little
endif

ifeq ($(ARCH),i386)
ifeq ($(USE_SIMD),y)
USE_MMX = y
endif
ifdef USE_MMXSSE
USE_MMX = y
endif
ifdef USE_SSE2
undefine USE_MMX
undefine USE_MMXSSE
endif
MACHINE_NATURAL_SIZE ?= 4
CACHE_LINE_SIZE ?= 64
L1_CACHE_SIZE ?= 16
L1_CACHE_ASSOC ?= 2
L2_CACHE_SIZE ?= 256
L2_CACHE_ASSOC ?= 2
ENDIAN ?= little
ifdef USE_SSE2
HAVE_SIMD ?= y
CFLAGS_SIMD ?= -msse2
CDEFS_SIMD ?= -DHAVE_INTEL_SSE2
SIMD_VECTOR_SIZE ?= 16
HDRS_SIMD ?= simd/simd.h simd/simd_intel_sse.h
endif
ifdef USE_MMX
HAVE_SIMD ?= y
CFLAGS_SIMD ?= -mmmx
CDEFS_SIMD ?= -DHAVE_INTEL_MMX
HDRS_SIMD ?= simd/simd.h simd/simd_intel_mmx.h
SIMD_VECTOR_SIZE ?= 8
ifdef USE_MMXSSE
CFLAGS_SIMD += -msse
CDEFS_SIMD += -DHAVE_INTEL_MMXSSE
endif
endif
endif

ifeq ($(ARCH),armel)
MACHINE_NATURAL_SIZE ?=4
CACHE_LINE_SIZE ?= 64
L1_CACHE_SIZE ?= 16
L1_CACHE_ASSOC ?= 2
L2_CACHE_SIZE ?= 128
L2_CACHE_ASSOC ?= 2
HAVE_EFFICIENT_IDIV ?= y
ENDIAN ?= little
endif

ifeq ($(ARCH),armhf)
ifeq ($(USE_SIMD),y)
USE_NEON = y
endif
ifdef USE_NEON
HAVE_SIMD ?= y
SIMD_VECTOR_SIZE ?= 16
CFLAGS_SIMD ?= -mfloat-abi=hard -mfpu=neon
CDEFS_SIMD ?= -DHAVE_ARM_NEON
HDRS_SIMD ?= simd/simd.h simd/simd_arm_neon.h
endif
MACHINE_NATURAL_SIZE ?=4
CACHE_LINE_SIZE ?= 64
L1_CACHE_SIZE ?= 16
L1_CACHE_ASSOC ?= 2
L2_CACHE_SIZE ?= 128
L2_CACHE_ASSOC ?= 2
ENDIAN ?= little
endif

ifeq ($(ARCH),arm64)
ifeq ($(USE_SIMD),y)
USE_NEON = y
endif
CFLAGS_ARCH ?= -mabi=lp64 -mstrict-align
ifdef USE_NEON
HAVE_SIMD ?= y
SIMD_VECTOR_SIZE ?= 16
CFLAGS_SIMD ?=
CDEFS_SIMD ?= -DHAVE_ARM_NEON
HDRS_SIMD ?= simd/simd.h simd/simd_arm_neon.h
endif
MACHINE_NATURAL_SIZE ?=8
CACHE_LINE_SIZE ?= 64
L1_CACHE_SIZE ?= 32
L1_CACHE_ASSOC ?= 2
L2_CACHE_SIZE ?= 256
L2_CACHE_ASSOC ?= 2
HAVE_64BIT ?= y
HAVE_EFFICIENT_IDIV ?= y
ENDIAN ?= little
endif

ifeq ($(ARCH),alpha)
ifeq ($(USE_SIMD),y)
USE_MVI = y
endif
ifdef USE_EV67
CFLAGS_ARCH ?= -mcpu=ev67
HAVE_SIMD ?= y
SIMD_VECTOR_SIZE ?= 8
CFLAGS_SIMD ?= -mcpu=ev67
CDEFS_SIMD ?= -DHAVE_ALPHA_MVI
HDRS_SIMD ?= simd/simd.h simd/simd_alpha_mvi.h
L1_CACHE_SIZE ?= 64
L1_CACHE_ASSOC ?= 2
L2_CACHE_SIZE ?= 1024
L2_CACHE_ASSOC ?= 2
endif
ifdef USE_MVI
CFLAGS_ARCH ?= -mbwx
HAVE_SIMD ?= y
SIMD_VECTOR_SIZE ?= 8
CFLAGS_SIMD ?= -mbwx -mmax
CDEFS_SIMD ?= -DHAVE_ALPHA_MVI
HDRS_SIMD ?= simd/simd.h simd/simd_alpha_mvi.h
endif
ifdef USE_BWX
CFLAGS_ARCH ?= -mbwx
endif
MACHINE_NATURAL_SIZE ?=8
CACHE_LINE_SIZE ?= 64
L1_CACHE_SIZE ?= 16
L1_CACHE_ASSOC ?= 2
L2_CACHE_SIZE ?= 256
L2_CACHE_ASSOC ?= 2
HAVE_64BIT ?= y
ENDIAN ?= little
endif

ifeq ($(ARCH),ppc)
ifeq ($(USE_SIMD),y)
USE_ALTIVEC = y
endif
MACHINE_NATURAL_SIZE ?=4
CACHE_LINE_SIZE ?= 64
L1_CACHE_SIZE ?= 16
L1_CACHE_ASSOC ?= 2
L2_CACHE_SIZE ?= 512
L2_CACHE_ASSOC ?= 2
ENDIAN ?= big
ifdef USE_ALTIVEC
HAVE_SIMD ?= y
SIMD_VECTOR_SIZE ?= 16
CFLAGS_SIMD ?= -maltivec -mabi=altivec
CDEFS_SIMD ?= -DHAVE_POWERPC_ALTIVEC
HDRS_SIMD ?= simd/simd.h simd/simd_powerpc_altivec.h
endif
endif

ifeq ($(ARCH),ppc64)
ifeq ($(USE_SIMD),y)
USE_ALTIVEC = y
endif
MACHINE_NATURAL_SIZE ?=8
CACHE_LINE_SIZE ?= 64
L1_CACHE_SIZE ?= 16
L1_CACHE_ASSOC ?= 2
L2_CACHE_SIZE ?= 512
L2_CACHE_ASSOC ?= 2
HAVE_64BIT ?= y
ENDIAN = big
ifdef USE_ALTIVEC
HAVE_SIMD ?= y
SIMD_VECTOR_SIZE ?= 16
CFLAGS_SIMD ?= -maltivec -mabi=altivec
CDEFS_SIMD ?= -DHAVE_POWERPC_ALTIVEC
HDRS_SIMD ?= simd/simd.h simd/simd_powerpc_altivec.h
endif
endif

# We don't know about other arches so a calculated guess on these:
MACHINE_NATURAL_SIZE ?=4
CACHE_LINE_SIZE ?= 64
L1_CACHE_SIZE ?= 16
L1_CACHE_ASSOC ?= 2
L2_CACHE_SIZE ?= 128
L2_CACHE_ASSOC ?= 2
ENDIAN ?= unknown

ifdef USE_SIMD_CFLAGS_FOR_ARCH
CFLAGS_ARCH ?= $(CFLAGS_SIMD)
else
CFLAGS_ARCH ?=
endif

CFLAGS_SIMD ?=
CDEFS_SIMD ?=
HDRS_SIMD ?=

ifndef HAVE_SIMD
  HAVE_SIMD = n
endif
ifndef HAVE_64BIT
  HAVE_64BIT = n
endif
ifndef HAVE_EFFICIENT_IDIV
  HAVE_EFFICIENT_IDIV = n
endif
