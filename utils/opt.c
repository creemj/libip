/*
 * Module: mjc/opt.c
 * Author: M. J. Cree
 *
 * Copyright (C) 1992-2007 Michael J. Cree
 *
 * Description:
 *
 * Set of C routines to simplify evalualating options in the argument list
 * passed to a C programme. Calls available are:-
 *
 *      printhelp()     - Prints help message.
 *      parseargs()     - Parse command line.
 *      getarg()        - Get parsed argument.
 *      freeargs()      - Free resources allocated in parseargs().
 *	register_evi()  - Register external version info.
 *
 * See helpers.h for more info.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <ctype.h>

#include "helpers.h"


static const char *type[] = {
    "",				/* OPT_SWITCH */
    "(int) ",			/* OPT_INT */
    "(float) ",			/* OPT_FLOAT */
    "(string) ",			/* OPT_STRING */
    "(var) ",			/* OPT_VARIANT */
    "",				/* OPT_MULT */
    "(float) ",			/* OPT_DOUBLE */
    "(long) ",			/* OPT_LONG */
    "(int32_t)",			/* OPT_INT32 */
    "(uint32_t)",		/* OPT_UINT32 */
    "(int64_t)",			/* OPT_INT64 */
    "(uint64_t)",		/* OPT_UINT64 */
    "(coord) ",			/* OPT_COORD */
    "(complex) ",		/* OPT_COMPLEX */
};


struct evi {
    struct evi *next;
    char *str;
};

static struct evi *evilist = NULL;


/* Internal routines. */
static int makeopt(char *, struct op *, int *);
static void parse_stdarg(char *key, struct progenv *prg, struct op *optspec);
static void read_parameter(optlist *ol, char *argv[], int arg, int type, struct mlnode *ml);

static int valid_opt(struct op *opt);

static optlist *allocmlist(struct progenv *pe, struct op *optspec);
static struct mlnode *locateml(optlist *ol, struct op *opt);
static struct mlnode *createml(optlist *ol, struct op *opt);

static int incarg(optlist *ol, int, int);
static void chknotopt(optlist *ol, char **, int );
static coord get_coord(optlist *ol, char *str);
static complex get_complex(optlist *ol, char *str);

static void errmsg(char *pn, char *s, ...);


/*

  NAME:

  printhelp() \- Print help or usage message for programme

  PROTOTYPE:

  #include <mjc/opt.h>

  void  printhelp( output, progname, optlist, flag )
  FILE *output;
  char *progname;
  struct op *optlist;
  int flag;

  DESCRIPTION:

  Print help message about programme. 'Optlist' is the help structure
  of the programme containing all command line parsing info.  'Flag'
  can be one of:

  \&   HLP_BRIEF   Print usage information only.
  \&   HLP_VERBOSE Print full help message.

  \&-   'Output' is the output stream to send all messages to.  Normally
  this should be set to stdout.  'Progname' is a string containing
  programme name.

  SEE ALSO:

  parseargs()    getarg()    freeargs()

*/


void printhelp(FILE *f, char *progname, struct op *optspec, int flag)
{
    int length;			/* Length of line so far. */
    int optlen;			/* Length of brief option string. */
    int delayed;			/* Count of delayed ']'s. */
    int newsent;			/* TRUE if new sentence starting. */
    int i;			/* loop counter. */
    char *cptr;			/* loop pointer to char. */
    char **sptr;			/* loop pointer to string. */
    struct op *opt;		/* loop pointer to option. */
    char optstr[80];		/* Buffer for constructing line of output.  */

    /*** Construct brief usage message ***/

    /* Print programme name and standard help options. */

    sprintf(optstr, "Usage: %s", progname);
    strcat(optstr, " [-U] [-h] [-V]");
    length = strlen(optstr);
    fprintf(f, optstr);

    /* Print options in optlist (formatting to 80 char wide display). */

    delayed=0;
    for (opt=optspec; opt->type != OPT_DONE; opt++) {
	optlen = makeopt(optstr, opt, &delayed);
	length += optlen+1;
	if (length > 80) {
	    fprintf(f,"\n       ");
	    length = optlen+7;
	}else
	    fputc(' ',f);
	fprintf(f, optstr);
    }

    /* Tidy up at end of brief usage message */

    if (length + delayed > 80)
	fprintf(f, "\n       ");
    for (i=0; i<delayed; i++)
	fputc(']', f);
    fputc('\n', f);

    /*** Print full help message if required ***/

    if (flag & HLP_VERBOSE) {

	/* Standard help options */

	fputc('\n', f);
	fprintf(f, " -U\tPrint usage message and exit.\n");
	fprintf(f, " -h\tPrint this help message and exit.\n");
	fprintf(f, " -V\tPrint version information and exit.\n");

	/* Options in optlist */

	for (opt=optspec; opt->type != OPT_DONE; opt++) {
	    switch (opt->type & OPT_TYPEMASK) {
	    case OPT_SWITCH:
	    case OPT_INT:
	    case OPT_LONG:
	    case OPT_FLOAT:
	    case OPT_DOUBLE:
	    case OPT_INT32:
	    case OPT_UINT32:
	    case OPT_INT64:
	    case OPT_UINT64:
	    case OPT_COORD:
	    case OPT_COMPLEX:
	    case OPT_STRING:
	    case OPT_VARIANT:
		if (opt->type & OPT_LONELY)	/* Option without a "key" */
		    fprintf(f, " <%s>%c%s%s\n", opt->parm, "\t "[strlen(opt->parm)>=5],
			    type[opt->type & OPT_TYPEMASK], (char *)opt->helpmsg);
		else			/* Option with a "key" */
		    fprintf(f," -%s%c%s%s\n", opt->key, "\t "[strlen(opt->key)>=6],
			    type[opt->type & OPT_TYPEMASK], (char *)opt->helpmsg);
		break;
	    case OPT_MULT:
		for (cptr = opt->parm, sptr=opt->helpmsg; *cptr ; cptr++,sptr++)
		    fprintf(f," -%s%c%c%s%s\n", opt->key, *cptr,
			    "\t "[strlen(opt->key)>=5], type[OPT_SWITCH],*sptr);
		break;
	    case OPT_MORETYPE:
		break;
	    default:
		errmsg(progname, "Badly formed option list. Fix and recompile me.");
		break;
	    }
	}

	/* If OPT_DONE has helpmsg field set, then it contains a short
	   message describing programme.  Print it if it is present. */

	if (opt->helpmsg) {
	    fputc('\n',f);
	    cptr = opt->helpmsg;		/* Programme short description msg. */
	    while (*cptr == ' ')		/* Skip white space at start. */
		cptr++;
	    newsent = length = 0;
	    while (*cptr) {
		if (*cptr == '\n') {	/* Do new paragraph/newline. */
		    fputc(*cptr++, f);
		    newsent = length = 0;
		}else if (*cptr == '\t' && length == 0) {
		    fprintf(f, "      ");	/* Indent a new line.  */
		    cptr++;
		    length = 6;
		}else{			/* Print a word. */
					/* Get word length */
		    optlen = strcspn(cptr, " \t\n");
		    if (length + optlen + newsent + 1 > 80) {
			fputc('\n', f);
			newsent = length = 0;
		    }
		    if (length == 0) {	/* Always indent newline by a space */
			fputc(' ', f);
			length = 1;
		    }
		    if (newsent) {		/* Extra space at start of sentence */
			fputc(' ', f);
			newsent = 0;
			length++;
		    }
		    fputc(' ',f);
		    for (i=0; i<optlen; i++)	/* Print word */
			fputc(*cptr++, f);
		    if (*(cptr-1) == '.' || *(cptr-1) == '!' || *(cptr-1) == '?')
			newsent = 1;		/* Check if end of sentence */
		    length += optlen + 1;
		}
		while (*cptr == ' ')	/* Skip any white space. */
		    cptr++;
	    }
	    if (length) fputc('\n', f);
	}
    }
}



static int makeopt(char *str, struct op *opt, int *delayed)
{
    char buff[80];
    int i;

    *str = 0;
    if (NOT (opt->type & OPT_REQ))
	strcat(str, "[");
    if (NOT (opt->type & OPT_LONELY)) {
	strcat(str, "-");
	strcat(str, opt->key);
    }
    switch (opt->type & OPT_TYPEMASK) {
    case OPT_INT:
    case OPT_INT32:
    case OPT_UINT32:
    case OPT_INT64:
    case OPT_UINT64:
    case OPT_LONG:
    case OPT_FLOAT:
    case OPT_DOUBLE:
    case OPT_COORD:
    case OPT_COMPLEX:
    case OPT_STRING:
    case OPT_VARIANT:
	if (NOT (opt->type & OPT_LONELY))
	    strcat(str, " ");
	sprintf(buff, "<%s>", opt->parm);
	break;
    case OPT_MULT:
	sprintf(buff, "{%s}", opt->parm);
	break;
    case OPT_SWITCH:
	buff[0] = 0;
	break;
    case OPT_MORETYPE:
	strcpy(buff, opt->parm);
	break;
    default:
	if (opt->parm)		/* Best we can do in error condition! */
	    sprintf(buff, " <%s>", opt->parm); /* Error is flagged above */
	else
	    buff[0] = 0;
	break;
    }
    strcat(str,buff);
    if (NOT (opt->type & OPT_REQ)) {
	if (opt->type & OPT_NIN)
	    *delayed += 1;
	else {
	    for (i=0; i<*delayed; i++)
		strcat(str, "]");
	    *delayed = 0;
	    strcat(str, "]");
	}
    }
    return strlen(str);
}


/*

  NAME:

  register_evi() \- Register extra version info string.

  PROTOTYPE:

  #include <mjc/opt.h>

  void register_evi(evi)
  char * evi;

  DESCRIPTION:

  Register a string 'evi' which is to be printed whenever the
  option -V is given.  All 'evi's registered are printed after
  the standard version information is given and before the
  programme exits.  Register_evi() should be called before the
  call to parseargs().  It may be called a number of times to
  register a number of strings.  Note that string 'evi' must
  hang around during the call to parseargs() - it isn'' copied.

*/

void register_evi(char *evistr)
{
    struct evi *this, *prev;

    prev = evilist;
    for (prev = evilist; prev && prev->next; prev = prev->next)
	;
    if ((this = malloc(sizeof(struct evi))) != NULL) {
	this->str = evistr;
	this->next = NULL;
	if (prev)
	    prev->next = this;
	else
	    evilist = this;
    }
}


/*

  NAME:

  parseargs() \- Parse arguments on command line

  PROTOTYPE:

  #include <mjc/opt.h>

  optlist * parseargs( argc, argv, prg, optspec )
  int argc;
  char *argv[];
  struct progenv *prg;
  struct op *optspec;

  DESCRIPTION:

  Parse the arguments on the command line according to the command line
  specification given in 'optspec'.  'Argc' and 'argv' are as passed to
  main().  'Prg' is the programme environment structure describing programme
  name, version, etc.  Parseargs() parses the arguments on the command line
  and checks that they are consistent with the 'optspec' description.  If any
  errors are detected it prints a message to stdout and exits.  If the command
  line is consistent it returns a list of commands in the 'optlist' pointer.
  The 'optlist' object is not for your perusal, but for passing to the routine
  getarg() which returns each argument for you.  Once argument processing is
  complete then call freeargs() with the 'optlist' pointer returned by
  parseargs() to free all resources allocated.

  Parseargs() also manages the options ''-h''  (or ''-help''), ''-U''
  (or ''-Usage'') and ''-V''  (or ''-Version'') and prints out help,
  usage or version messages as appropriate and exits the programme
  whenever one of these options is present.

  An example of use is:

  \|   #include <stdio.h>
  \|   #include <mjc/opt.h>
  \|
  \|   struct progenv prog = {
  \|      stdout,           // Default output stream
  \|      "programme",      // Programme name
  \|      "1.00"            // Version string
  \|      "20/4/96"         // Date of this version
  \|   };
  \|
  \|   struct op help[] = {
  \|
  \|      // This programme can take the option '-b'.
  \|      {OPT_SWITCH, "b", NULL,
  \|          "File is binary" },
  \|
  \|      // It always takes a filename argument.
  \|      {OPT_STRING | OPT_LONELY | OPT_REQ,  NULL, "filename",
  \|          "Input file name." }
  \|
  \|      // Indicate end of array and programme description.
  \|      {OPT_DONE, NULL, NULL,
  \|         "Short message about programme. "
  \|         "Terminate paragraphs with '\n'. "
  \|         "To really force a new line use '\n'. "
  \|         "To indent a line use '\t' at start of line. "
  \|         "Note space at end of sentences in lines above. ",
  \|         "Do not put a '\n' at end of last line." },
  \|
  \|   };
  \|
  \|   // This list identifies the arguments by number.
  \|
  \|   enum {
  \|      O_BINARY,
  \|      O_FILENAME
  \|   }
  \|
  \|   main(int argc, char *argv[])
  \|      char *filename;
  \|      int is_binary;
  \|      optlist *optlist;
  \|         ...
  \|
  \|      optlist = parseargs(argc,argv,&prog,help);
  \|
  \|      getarg(optlist, O_BINARY, &is_binary);
  \|      getarg(optlist, O_FILENAME, &filename);
  \|
  \|      freeargs(optlist);
  \|
  \|      // Rest of programme follows....
  \|

  Simple, eh!  See the header file mjc/opt.h for the desription
  of the structures/objects involved.

  SEE ALSO:

  printhelp()    getarg()    freeargs()

*/


optlist *parseargs(int argc, char *argv[],
		   struct progenv *prg, struct op *optspec)
{
    int arg, found, i;
    int more_ok;
    struct op *opt;
    char *key;
    optlist *ol;
    struct mlnode *ml;
    char str[80];

    ol = allocmlist(prg, optspec);

    /* For each argument on command line do */

    for (arg=1; arg<argc; arg++) {

	if (*argv[arg] == '-'		/* If a key then */
	    && NOT STREQUAL(argv[arg],"-")
	    && NOT STREQUAL(argv[arg],"--")) {

	    /* Parse each argument on command line which has a key, that
	       is, it starts off with a '-'. */

	    key = argv[arg];	/* Pointer to the key */

	    /* If standard key, eg. '-h', parse it specially. Note:
	       parse_stdarg() doesn't return if it is a standard key. */

	    parse_stdarg(key, prg, optspec);

	    key++;			/* Step past initial '-' */

	    /* Step through each option in programme option list and see
	       if we can find the key there. */

	    found = FALSE;		/* Not found yet */

	    for (opt = optspec; (NOT found) && opt->type != OPT_DONE; opt++) {

		if (opt->type & OPT_LONELY)
		    break;		/* Break, since no more options with a key */

		if ((opt->type & OPT_TYPEMASK) == OPT_MULT) {

		    /* Check key against an OPT_MULT type option */

		    for (i=0; (NOT found) && i<strlen(opt->parm) ; i++) {
			strcpy(str, opt->key);
			str[strlen(str) + 1] = 0;
			str[strlen(str)] = opt->parm[i];
			if (STREQUAL(key,str)) {

			    /* Found - is a OPT_MULT type key  */

			    found = TRUE;
			    if ((ml = locateml(ol, opt))) {
				if (opt->type & OPT_UNIQUE) {
				    errmsg(ol->pn, "Multiple definitions of base option"
					   " '-%s' not allowed.",opt->key);
				}
				ml->num++;
				ml->data.mult[i]++;
			    }else{
				ml = createml(ol, opt);
				ml->data.mult[i] = 1;
			    }
			}
		    }

		}else{

		    /* Check key against normal command line option */

		    if (STREQUAL(key, opt->key)) {

			/* Found - is normal type key */

			found = TRUE;
			if ((ml = locateml(ol, opt))) {

			    /* Option already in command line. */

			    if ((opt->type & OPT_TYPEMASK) != OPT_SWITCH) {
				errmsg(ol->pn, "Option %s multiply specified.", argv[arg]);
			    }

			    ml->num++;

			}else{

			    /* New option on command line - create new ml. */

			    ml = createml(ol, opt);

			    /* Most take an argument, go to next argument on
			       command line and read it if necessary. */

			    if ((opt->type & OPT_TYPEMASK) != OPT_SWITCH) {

				arg = incarg(ol, argc,arg);
				read_parameter(ol, argv, arg, opt->type, ml);

			    }

			}
		    }
		}
	    }

	    if (NOT found) {	/* Keys must be matched to programme
				   command line specs */
		errmsg(ol->pn, "Invalid option '%s'.", argv[arg]);
	    }

	}else{

	    /* We have parsed all options taking a key.  All that is left
	       is parameters on command line that stand alone and come
	       consequectively, for example, file names. */

	    /* Step through programme command line specs until we get to
	       options at end that don't take a key. */

	    more_ok = FALSE;
	    for (opt=optspec; (opt->type != OPT_DONE)
		     && (NOT (opt->type & OPT_LONELY))
		     && (opt->type != OPT_MORE); opt++ ) ;

	    /* If current option is '--' should step past it. */

	    if (STREQUAL(argv[arg], "--")) {
		arg++;
		if (arg == argc) {
		    errmsg(ol->pn, "What's the use of using '--' at end of command line?");
		}
	    }

	    /* If there are no such options in programme specs, but there
	       is more on command line, then error. */

	    if (opt->type == OPT_DONE) {
		errmsg(ol->pn, "Extra garbage on command line.  Try '%s -h'.", ol->pn);
	    }

	    /* Step through programme specs and match to what's left on
	       command line. */

	    for ( ; arg < argc && opt->type != OPT_DONE; arg++,opt++) {

		ml = createml(ol, opt);

		switch (opt->type & OPT_TYPEMASK) {

		case OPT_INT:
		case OPT_INT32:
		case OPT_UINT32:
		case OPT_INT64:
		case OPT_UINT64:
		case OPT_LONG:
		case OPT_FLOAT:
		case OPT_DOUBLE:
		case OPT_COORD:
		case OPT_COMPLEX:
		case OPT_STRING:
		case OPT_VARIANT:

		    read_parameter(ol, argv, arg, opt->type, ml);
		    break;

		case OPT_MORETYPE:

		    if (more_ok == TRUE) {
			errmsg(ol->pn, "Extra OPT_MORE in opt list. Fix and recompile me!");
		    }
		    ml->data.along = arg;
		    more_ok = TRUE;
		    break;

		case OPT_SWITCH:
		case OPT_MULT:

		    errmsg(ol->pn, "Illegal type with OPT_LONELY. Fix and recompile me.");
		    break;

		default:

		    errmsg(ol->pn, "Unrecognised option type. Fix and recompile me!");
		    break;

		}
	    }

	    /* If still more arguments on command line and nothing left
	       in programme specs then error */

	    if ((arg < argc) && (NOT more_ok)) {
		errmsg(ol->pn, "Extra garbage on command line. Try '%s -h'.", ol->pn);
	    }
	}
    }

    /* Parsed command line - must check that got required parameters. */

    for (opt= optspec; opt->type != OPT_DONE; opt++) {
	if (opt->type & OPT_REQ) {
	    if (NOT locateml(ol, opt)) {
		errmsg(ol->pn, "Required argument missing. Try '%s -h'.", ol->pn);
	    }
	}
	if (opt->type == OPT_MORE) {
	    /* No MORE type options specified, insert default value. */
	    if (NOT locateml(ol, opt)) {
		ml = createml(ol, opt);
		ml->data.along = argc;
	    }
	}
    }

    return ol;
}



static void parse_stdarg(char *key, struct progenv *prg, struct op *optspec)
{
    struct evi *evinode;

    if (STREQUAL(key, "-h") || STREQUAL(key, "-help")) {
	printhelp(prg->output, prg->progname, optspec, HLP_VERBOSE);
	exit(0);
    }
    if (STREQUAL(key, "-U") || STREQUAL(key, "-Usage")) {
	printhelp(prg->output, prg->progname, optspec, HLP_BRIEF);
	exit(0);
    }
    if (STREQUAL(key, "-V") || STREQUAL(key, "-Version")) {
	fprintf(prg->output, "%s: version %s (%s)\n",
		prg->progname, prg->version, prg->date);
	for (evinode = evilist; evinode ; evinode=evinode->next)
	    fprintf(prg->output, "%s\n", evinode->str);
	exit(0);
    }
}




static void chknotopt(optlist *ol, char *argv[], int arg)
{
    if (*argv[arg] == '-') {
	if (isdigit(*(argv[arg]+1)))   /* Negative numbers can be okay */
	    return;
	errmsg(ol->pn, "Required parameter for option %s left out.", argv[arg-1]);
    }
}



static struct mlnode *locateml(optlist *ol, struct op *opt)
{
    struct mlnode *ml;

    for (ml = ol->ml->next; ml; ml=ml->next) {
	if (ml->opt == opt)
	    break;
    }
    return ml;
}


static int valid_opt(struct op *opt)
{
    if (opt->type == OPT_DONE) return 1;
    if (opt->type & OPT_UNUSEDBITS) return 0;
    return ((opt->type & OPT_TYPEMASK) < (OPT_TYPEMAX+1));
}



static optlist *allocmlist(struct progenv *pe, struct op *optspec)
{
    optlist *ol;
    int numop;
    struct mlnode *ml;
    struct op *opt;

    for (opt = optspec,numop=0; (numop < OPT_MAXNUM)&&(opt->type != OPT_DONE); opt++,numop++) {
	if (NOT valid_opt(opt))
	    errmsg(pe->progname, "Bad option specification list - Fix and recompile me.\n");
    }

    if ((ol = (optlist *)malloc(sizeof(optlist))) == NULL) {
	errmsg(pe->progname, "Out of memory!!!");
    }
    ol->pe = pe;
    ol->pn = pe->progname;
    ol->os = optspec;
    ol->numop = numop;
    if ((ml = (struct mlnode *)malloc(sizeof(struct mlnode))) == NULL) {
	errmsg(pe->progname, "Out of memory!!!");
    }
    ol->ml = ml;
    ml->opt = optspec;
    ml->next = 0;			/* Clear as yet unused fields. */
    ml->num = 0;
    ml->data.along = 0;
    return ol;
}



static struct mlnode *createml(optlist *ol, struct op *opt)
{
    struct mlnode *ml,*mlprev;

    for (ml = ol->ml; ml->next; ml = ml->next) ;
    mlprev = ml;
    if ((ml = (struct mlnode *)malloc(sizeof(struct mlnode))) == NULL) {
	errmsg(ol->pn,"Out of memory!!!");
    }
    mlprev->next = ml;
    ml->next = NULL;
    ml->num = 1;
    ml->data.along = 0;
    ml->opt = opt;
    if ((opt->type & OPT_TYPEMASK) == OPT_MULT) {
	if ((ml->data.mult = (int *)calloc(sizeof(int), strlen(opt->parm)))== NULL){
	    errmsg(ol->pn,"Out of memory.");
	}
    }
    return ml;
}



static int incarg(optlist *ol, int argc, int arg)
{
    arg++;
    if (arg >= argc) {
	errmsg(ol->pn, "Unexpected termination to command line.");
    }
    return arg;
}




/*

  NAME:

  freeargs() \- Free optlist previously allocated by parseargs()

  PROTOTYPE:

  #include <mjc/opt.h>

  void  freeargs( optlist )
  optlist *optlist;

  DESCRIPTION:

  Free up resources/memory allocated by parseargs().

  SEE ALSO:

  parseargs()

*/

void freeargs(optlist *ol)
{
    struct mlnode *ml,*next;
    struct evi *evinode, *evinext;

    for (ml=ol->ml; ml; ml = next) {
	next = ml->next;
	if ((ml->opt->type & OPT_TYPEMASK) == OPT_MULT)
	    free(ml->data.mult);
	free(ml);
    }
    free(ol);
    for (evinode = evilist; evinode; evinode = evinext) {
	evinext = evinode->next;
	free(evinode);
    }
}




/*

  NAME:

  getarg() \- Get an argument from command line.

  PROTOTYPE:

  #include <mjc/types.h>
  #include <mjc/opt.h>

  int  getarg( optlist, optnum, dataptr )
  optlist *optlist;
  int optnum;
  void *dataptr;

  DESCRIPTION:

  Get back a parameter that is specified on the command line and that
  has been parsed by parseargs().  'Optlist' is the object returned by
  parseargs(), 'optnum' is the index into the 'optspec' array
  (see parseargs()) to the particular argument you want to check, and
  'dataptr' is a pointer to some memory space that contains enough
  space to receive the parameter you are reading from the command
  line.

  The integer returned from getarg() is the number of times the
  parameter was specified on the command line.  Normally it is 0
  (wasn''t specified on command line) or 1 (appeared once).

  For most types of arguments (such as integers, float values, etc.)
  a value is written into the memory pointed to by 'dataptr' if and
  only if the argument is specified on the command line.  This means
  you can initialise the space pointed to by 'dataptr' with a default
  value before calling getarg(), which will only be changed if the
  argument is specified.

  The exceptions to the above are for OPT_SWITCH arguments which
  always stores 1 (TRUE) if the argument is present or 0 (FALSE) if
  the argument is absent into 'dataptr'.  The OPT_MORE argument also
  always stores a result into 'dataptr', namely the value of 'argc'
  passed into main() is stored into 'dataptr' if no more arguments
  are present on the command line.

  The possible option types and the type that 'dataptr' should be
  are:

  \|      Parameter           dataptr type
  \|   OPT_SWITCH                int *
  \|   OPT_INT                   int *
  \|   OPT_LONG                  long *
  \|   OPT_INT32                 int32_t *
  \|   OPT_UINT32                uint32_t *
  \|   OPT_INT64                 int64_t *
  \|   OPT_UINT64                uint64_t *
  \|   OPT_FLOAT                 float *
  \|   OPT_DOUBLE                double *
  \|   OPT_STRING                char **
  \|   OPT_VARIANT               char **
  \|   OPT_MULT                  int *
  \|   OPT_MULT | OPT_CHARBACK   char *
  \|   OPT_MORE                  int *
  \|   OPT_COORD                 coord *
  \|   OPT_COMPLEX               complex *

  The two last types (coord and complex) are defined in mjc/types.h.  While
  this is included by mjc/opt.h it is nevertheless good to explicitly include
  mjc/types.h if you use coord or complex so that the reader realises that
  you intend use of the extra defined types.

  See parseargs() man page for an example of use.

  SEE ALSO:

  parseargs()

*/

int getarg(optlist *ol, int optnum, void *dataptr)
{
    struct mlnode *ml;
    struct op *opt;
    int i;

    if (optnum < 0 || optnum >= ol->numop) {
	errmsg(ol->pn, "Requested bad option index %d in call to getarg().",optnum);
    }

    opt = &ol->os[optnum];
    if ((ml = locateml(ol, opt)) == NULL) {
	if ((opt->type & OPT_TYPEMASK) == OPT_SWITCH) {
	    if (dataptr)
		*(int *)dataptr = FALSE;
	    return 0;
	}
	return 0;
    }else{
	if (dataptr)
	    switch (opt->type & OPT_TYPEMASK) {
	    case OPT_SWITCH:
		*(int *)dataptr = TRUE;
		break;
	    case OPT_INT:
		*(int *)dataptr = ml->data.along;
		break;
	    case OPT_LONG:
		*(long *)dataptr = ml->data.along;
		break;
	    case OPT_INT32:
		*(int32_t *)dataptr = (int32_t)ml->data.aint64;
		break;
	    case OPT_UINT32:
		*(uint32_t *)dataptr = (uint32_t)ml->data.auint64;
		break;
	    case OPT_INT64:
		*(int64_t *)dataptr = ml->data.aint64;
		break;
	    case OPT_UINT64:
		*(uint64_t *)dataptr = ml->data.auint64;
		break;
	    case OPT_FLOAT:
		*(float *)dataptr = ml->data.afloat;
		break;
	    case OPT_DOUBLE:
		*(double *)dataptr = ml->data.afloat;
		break;
	    case OPT_COORD:
		*(coord *)dataptr = ml->data.acoord;
		break;
	    case OPT_COMPLEX:
		*(complex *)dataptr = ml->data.acomplex;
		break;
	    case OPT_STRING:
	    case OPT_VARIANT:
		*(char **)dataptr = ml->data.astr;
		break;
	    case OPT_MULT:
		for (i=0; ml->data.mult[i] == 0; i++) ;
		*(int *)dataptr = (opt->type & OPT_CHARBCK) ? opt->parm[i] : i;
		break;
	    case OPT_MORETYPE:
		*(int *)dataptr = ml->data.along;
		break;
	    default:
		errmsg(ol->pn, "Corrupt option list. Fix and recompile me.");
	    }
    }
    return ml->num;
}



static void read_parameter(optlist *ol, char *argv[], int arg, int type, struct mlnode *ml)
{
    char *rest;

    switch (type & OPT_TYPEMASK) {
    case OPT_INT:
    case OPT_LONG:
	chknotopt(ol, argv, arg);
	ml->data.along = strtol(argv[arg], &rest, 10);
	if (*rest) errmsg(ol->pn, "Badly formatted integer number %s", argv[arg]);
	break;
    case OPT_INT32:
    case OPT_INT64:
	chknotopt(ol, argv, arg);
	ml->data.aint64 = strtoll(argv[arg], &rest, 10);
	if (*rest) errmsg(ol->pn, "Badly formatted integer number %s", argv[arg]);
	break;
    case OPT_UINT32:
    case OPT_UINT64:
	chknotopt(ol, argv, arg);
	ml->data.aint64 = strtoull(argv[arg], &rest, 10);
	if (*rest) errmsg(ol->pn, "Badly formatted integer number %s", argv[arg]);
	break;
    case OPT_FLOAT:
    case OPT_DOUBLE:
	chknotopt(ol, argv, arg);
	ml->data.afloat = strtod(argv[arg], &rest);
	if (*rest) errmsg(ol->pn, "Badly formatted real number %s", argv[arg]);
	break;
    case OPT_COORD:
	chknotopt(ol, argv, arg);
	ml->data.acoord = get_coord(ol, argv[arg]);
	break;
    case OPT_COMPLEX:
	chknotopt(ol, argv, arg);
	ml->data.acomplex = get_complex(ol, argv[arg]);
	break;
    case OPT_STRING:
    case OPT_VARIANT:
	ml->data.astr = argv[arg];
	break;
    case OPT_SWITCH:
	break;
    default:
	errmsg(ol->pn, "Unrecognised option type. Fix and recompile me!");
	break;
    }
}



static coord get_coord(optlist *ol, char *str)
{
    coord pt;

    if (sscanf(str, "%d%*1[x,]%d", &pt.x, &pt.y) != 2) {
	errmsg(ol->pn, "Incorrectly formatted coordinate %s, should be '<x>,<y>'.",str);
    }
    return pt;
}


static complex get_complex(optlist *ol, char *str)
{
    complex x;

    if (sscanf(str, "%g +i%g", &x.r, &x.i) != 2) {
	errmsg(ol->pn, "Incorrectly formatted complex number,"
	       "  should be '<real>+i<imag>'.");
    }
    return x;
}


static void errmsg(char *prgname, char *s, ...)
{
    va_list ap;

    va_start(ap,s);
    fprintf(stderr, "%s: (argument parsing)\n", prgname);
    fprintf(stderr, "ERROR: ");
    vfprintf(stderr, s, ap);
    fprintf(stderr, "\n");
    va_end(ap);
    exit(EXIT_FAILURE);
}



#ifdef OPT_DEBUG
void dumpmlist(optlist *ol)
{
    struct mlnode *ml;
    struct mlnode *mlist;
    struct op *opt;
    int i;

    mlist = ol->ml->next;
    for (ml=mlist; ml; ml=ml->next) {
	opt = ml->opt;
	printf("ml:\n  num = %d.\n", ml->num);
	if (opt->type & OPT_LONELY)
	    printf("  parm = %s.\n", opt->parm);
	else
	    printf("  key = %s.\n", opt->key);
	switch (opt->type & OPT_TYPEMASK) {
	case OPT_INT:
	case OPT_LONG:
	    printf("  integer = %ld.\n", ml->data.along);
	    break;
	case OPT_INT32:
	case OPT_INT64:
	    printf("  integer = %lld.\n", ml->data.aint64);
	    break;
	case OPT_UINT32:
	case OPT_UINT64:
	    printf("  integer = %llu.\n", ml->data.auint64);
	    break;
	case OPT_FLOAT:
	case OPT_DOUBLE:
	    printf("  float = %f.\n", ml->data.afloat);
	    break;
	case OPT_COORD:
	    printf("  coord = (%d,%d).\n", ml->data.acoord.x, ml->data.acoord.y);
	    break;
	case OPT_COMPLEX:
	    printf("  complex = %g + i%g.\n",
		   ml->data.acomplex.r, ml->data.acomplex.i);
	    break;
	case OPT_STRING:
	case OPT_VARIANT:
	    printf("  string = %s.\n", ml->data.astr);
	    break;
	case OPT_MULT:
	    for (i=0; i<strlen(opt->parm); i++)
		printf("  mult[%d] = %d.\n", i, ml->data.mult[i]);
	    break;
	case OPT_SWITCH:
	    printf("  option set.\n");
	    break;
	default:
	    printf("  Don't know what this means....\n");
	}
    }
}

#endif
