/*
   Header: utils/helpers.h
   Author: M. J. Cree

   Copyright (C) 1996-2007, 2019 Michael J. Cree

   Header file for many routines in my own mjc lib imported to the utils
   directory of the IP library.

*/

#ifndef IP_UTILS_HELPERS_H
#define IP_UTILS_HELPERS_H

#include <stdarg.h>
#include <stdio.h>
#include <stdint.h>

/*
 * Get types and some of my favourite defns from the IP library
 */

#include <ip/types.h>
typedef ip_coord2d coord;
typedef ip_complex complex;


/******************************/
/***        termsize.o        */
/******************************/

extern int termsize(int *numrows, int *numcols);


/******************************/
/***        message.o       ***/
/******************************/

/* type field of open_mfile() takes one of these. */
#define MFILE_CLEAR   1
#define MFILE_APPEND  0

/* Open/close message file. */
extern int open_mfile(char * fname, int type);
extern void close_mfile(void);

/* Redirect messages to different file streams. */
extern void mredirect(FILE *f);
extern void iredirect(FILE *f);
extern void wredirect(FILE *f);
extern void eredirect(FILE *f);

/* Print messages to stdout/stderr and to message file, if open. */
extern void mprintf(char *s, ...); /* Print message */
extern void iprintf(char *s, ...); /* Print info */
extern void wprintf(char *s, ...); /* Print warning */
extern void eprintf(char *s, ...); /* Print error */

extern void vmprintf(char *s, va_list); /* Print message */
extern void viprintf(char *s, va_list); /* Print info */
extern void vwprintf(char *s, va_list); /* Print warning */
extern void veprintf(char *s, va_list); /* Print error */


/******************************/
/***          opt.o         ***/
/******************************/

/* For use in flag argument of printhelp() */
#define HLP_BRIEF 0x0000
#define HLP_VERBOSE 0x0001


/*
 * Each possible argument on command line must have one of these defined (in
 * an array terminated with OPT_DONE).
 */

struct op {
    int type;			/* Type of option. */
    char *key;			/* option key, eg. "a" will give "-a". */
    char *parm;			/* The parameters the option takes. */
    void *helpmsg;		/* The help messages. */
};

/* type field takes the following arguments. */

#define OPT_TYPEMASK 0x000f	/* Mask type of opt. */


/* Must have one and only one of the types listed below. */

#define OPT_SWITCH 0		/* Option is boolean switch. */
#define OPT_INT 1		/* Option takes one integer. */
#define OPT_FLOAT 2		/* Option takes one float. */
#define OPT_STRING 3		/* Option takes one string. */
#define OPT_VARIANT 4		/* Option type is variant. */
#define OPT_MULT 5		/* Option takes chars straight after option. */
#define OPT_DOUBLE 6		/* Similar to OPT_FLOAT but returns double. */
#define OPT_LONG 7		/* Similar to OPT_INT but returns long. */
#define OPT_INT32 8		/* Similar to OPT_INT but returns int32_t */
#define OPT_UINT32 9		/* Similar to OPT_INT but returns uint32_t */
#define OPT_INT64 10		/* Similar to OPT_INT but returns int64_t */
#define OPT_UINT64 11		/* Similar to OPT_INT but returns uint64_t */
#define OPT_COORD 12		/* Option takes coordinate '(<x>,<y>)' */
#define OPT_COMPLEX 13		/* Option takes complex '<real> + i<imag>' */
/*      OPT_MORE                   Defined and described below (value = 14). */

#define OPT_DONE (~0)		/* No more options - must terminate array.
				   NB takes value 0x0f when ANDed with OPT_TYPEMASK */

/*
 * These flags can be used in conjuction with the above basic types.  They
 * should be bitwise OR-ed with the basic type.
 */

#define OPT_CHARBCK  0x0800	/* OPT_MULT return char not integer index. */
#define OPT_UNIQUE 0x1000	/* Force OPT_MULT to have one entry only. */
#define OPT_LONELY 0x2000	/* Parameter doesn't have key. */
#define OPT_REQ 0x4000		/* Option is always required. */
#define OPT_NIN 0x8000		/* Next option only if this option is. */
#define OPT_MORETYPE 14		/* Use OPT_MORE described below */

/* This can be used as an option type too. */
#define OPT_MORE (OPT_MORETYPE | OPT_LONELY)
				/* Extra arguments can be on command line,
				 * returns index of where 'more' args start.
				 * If there are no more arguments then
				 * returns argc.
				 */

/*
 * Note that the last field of the help array must be one of:
 *
 * {OPT_DONE, NULL, NULL, NULL}
 *
 * or
 *
 * {OPT_DONE, NULL, NULL,
 *    "Short message about programme. "
 *    "Terminate paragraphs with '\n' (rather than lines). "
 *    "To force a new line use '\n'. "
 *    "To indent a line use '\t' at start of line."
 * }
 */

/* Used internally to opt code */
#define OPT_TYPEMAX OPT_MORETYPE
#define OPT_UNUSEDBITS (~(OPT_TYPEMASK|OPT_CHARBCK|OPT_UNIQUE|OPT_LONELY|OPT_REQ|OPT_NIN))
#define OPT_MAXNUM 500

/* Programme environment structure. */

struct progenv {
    FILE * output;		/* Where help messages etc., are sent */
    char * progname;		/* Progamme name */
    char * version;		/* Version, eg. "1.00" */
    char * date;		/* Date of this version, eg. "24/10/96" */
};

/*
 * Structures for returning parsed command line. These should be treated as
 * ADTs and the internal fields never referenced. The function calls
 * described below should suffice.
 */

union ad {
    int64_t aint64;
    uint64_t auint64;
    long along;
    double afloat;
    char *astr;
    int *mult;
    coord acoord;
    complex acomplex;
};

struct mlnode {
    struct mlnode *next;
    struct op *opt;
    int num;
    union ad data;
};

struct ml {
    struct progenv *pe;
    char *pn;
    struct op *os;
    int numop;
    struct mlnode *ml;
};

typedef struct ml optlist;

#define ML_END (~0)

/* Procedure calls. */

/* Print help message. */
extern void printhelp(FILE *, char *, struct op *, int);

/* Register a string to be printed with version info */
void register_evi(char *evistr);

/* Parse arguments on command line */
optlist *parseargs(int, char **, struct progenv *, struct op *);

/* Get back a particular argument out of parsed command line */
extern int getarg(optlist *, int, void *);

/* Free resources allocated by parsearg(). */
extern void freeargs(optlist *);

#ifdef OPT_DEBUG
void dumpmlist(optlist *);	/* Only for debugging purposes. */
#endif

#endif
