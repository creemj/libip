/*
   Programme: imstats.c
   Author:    M.J.Cree

   Copyright (C) 1996, 1997, 2000-2002, 2005, 2015 by Michael J. Cree.

Description:

   Prints various stats on a raw, TIFF, PNG or JPEG image files.

*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <limits.h>
#include <float.h>

#include <ip/types.h>
#include <ip/image.h>
#include <ip/error.h>
#include <ip/flags.h>
#include <ip/stats.h>
#include <ip/proto.h>

#include <ip/file.h>
#include <ip/raw.h>
#include <ip/png.h>
#include <ip/tiff.h>
#include <ip/jpeg.h>
#include <ip/raw.h>

#if 0
#include <ip/visilog.h>
#include <ip/cimages.h>
#endif

#include "helpers.h"

#define PROGRAMME "imstats"
#define VERSION "2.00"
#define DATE "26/10/2015"


struct progenv prog = {
    NULL,
    PROGRAMME,
    VERSION,
    DATE
};

struct op help[] = {
    {OPT_SWITCH,  "rub", NULL,   "Raw ubyte (unsigned 8-bit) image." },
    {OPT_SWITCH,  "rb",  NULL,   "Raw byte (signed 8-bit) image." },
    {OPT_SWITCH,  "rus", NULL,   "Raw ushort int (unsigned 16-bit) image." },
    {OPT_SWITCH,  "rs",  NULL,   "Raw short int (signed 16-bit) image." },
    {OPT_SWITCH,  "rul", NULL,   "Raw ulong int (unsigned 32-bit) image." },
    {OPT_SWITCH,  "rl",  NULL,   "Raw long int (signed 32-bit) image." },
    {OPT_SWITCH,  "rf",  NULL,   "Raw float (IEEE single precision) image." },
    {OPT_SWITCH,  "rd",  NULL,   "Raw double (IEEE double precision) image." },
    {OPT_SWITCH,  "rc",  NULL,   "Raw complex float image." },
    {OPT_SWITCH,  "el",  NULL,   "Little endian encoded raw image [def=native]." },
    {OPT_SWITCH,  "eb",  NULL,   "Big endian encoded raw image [def=native]." },
    {OPT_INT,     "H",   "skip", "Skip header at start of image (raw image only)." },
    {OPT_INT,     "k",   "row_skip", "Row skip (in bytes) to next row of image (raw image)." },
    {OPT_COORD,	  "s",   "size", "Image size (raw image only)." },
    {OPT_COORD,	  "ro",  "ROI origin", "Origin of region of interest. [Def: (0,0)]" },
    {OPT_COORD,	  "rs",  "ROI size", "Size of region of interest. [Def: Image size]" },
    {OPT_STRING,  "roi", "ROI image", "Binary image that defines ROI"},
    {OPT_SWITCH,  "db",  NULL,   "Output min, max, mean and standard deviation "
                                 "suitable for a database."},
    {OPT_SWITCH,  "t",   NULL,   "Output image name, type, datatype and size only."},
    {OPT_SWITCH,  "data",NULL,   "Dump all image data."},
    {OPT_INT,     "row", "rownum","Dump image data in specified row."},
    {OPT_STRING | OPT_LONELY | OPT_NIN, NULL, "image",
                                 "Input image filename. [Def: stdin]" },
    {OPT_MORE,	   NULL, "...",  NULL},
    {OPT_DONE, NULL, NULL,
     "Calculate statistics on images.  Reads and automatically detects "
     "TIFF, PNG and JPEG images; RAW images must be specified on command"
     "line.  Calculates image minimum, maximum, summed pixels, mean, RMS and "
     "standard-deviation.  Reports presence of infinities or NaNs in "
     "floating-point images."}
};

enum {				/* Must be same order as above. */
    O_UBYTE, O_BYTE, O_USHORT, O_SHORT, O_ULONG, O_LONG, O_FLOAT, O_DOUBLE, O_COMPLEX,
    O_LITTLENDIAN, O_BIGENDIAN,
    O_HEADER, O_ROWSKIP, O_SIZE,
    O_ROIORG, O_ROISIZE, O_ROIIM,
    O_DB, O_IMTYPE, O_DUMP, O_DUMPROW,
    O_IMAGE,
    O_MORE
};


static void calc_image_stats(ip_image *im, ip_stats *st, ip_roi *roi, ip_image *roiim);
static void print_imstats(ip_image *im, const char *fname, int fltype,  ip_roi *roi,ip_image *roiim,  int );
static void dump_image_data(ip_image *im, ip_roi *roi, int row);
static void dump_row(ip_image *im, int row, int start, int end);


static void bombout(ip_error_logstack *ipr, int error, const char *e, va_list ap)
{
    static int aritherr = 0;

    fflush(stdout);
    if (error == ERR_SIGFPE) {
	if (NOT aritherr) {
	    ip_print_error(PROGRAMME,ipr,error,e,ap);
	    fprintf(stderr,"(Processing continuing...)\n\n");
	    aritherr = TRUE;
	}
    }else{
	ip_print_error(PROGRAMME,ipr,error,e,ap);
	exit(1);
    }
}



int main(argc,argv)
int argc;
char *argv[];
{
    ip_image *im, *roiim;
    ip_coord2d size;
    ip_box droi;
    ip_roi *roi;
    int useroi;
    int header;
    int rowskip;
    int type;
    int fltype;
    int marg;
    int db;
    int dump;
    int row;
    const char *fname, *nroi;
    FILE *f;
    unsigned char hdrbytes[4];
    void *fh;
    struct ml *optlist;

    /* Parse command line */

    register_evi(ip_version());

    ip_register_error_handler(bombout);

    prog.output = stdout;
    optlist = parseargs(argc,argv,&prog,help);

    fname = NULL;
    getarg(optlist,O_IMAGE, &fname);

    type = -1;
    if (getarg(optlist,O_BYTE, NULL))
	type = IM_BYTE;
    if (getarg(optlist,O_UBYTE, NULL))
	type = IM_UBYTE;
    if (getarg(optlist,O_SHORT, NULL))
	type = IM_SHORT;
    if (getarg(optlist,O_USHORT, NULL))
	type = IM_USHORT;
    if (getarg(optlist,O_LONG, NULL))
	type = IM_LONG;
    if (getarg(optlist,O_ULONG, NULL))
	type = IM_ULONG;
    if (getarg(optlist,O_FLOAT, NULL))
	type = IM_FLOAT;
    if (getarg(optlist,O_DOUBLE, NULL))
	type = IM_DOUBLE;
    if (getarg(optlist,O_COMPLEX, NULL))
	type = IM_COMPLEX;

    header = rowskip = 0;
    size.x = size.y = INT_MIN;
    if (getarg(optlist, O_HEADER, &header) && type == -1) {
	eprintf("Option '-H' only valid with raw image types.\n");
	exit(1);
    }
    if (getarg(optlist, O_ROWSKIP, &rowskip) && type == -1) {
	eprintf("Option '-H' only valid with raw image types.\n");
	exit(1);
    }
    if (getarg(optlist, O_SIZE, &size) && type == -1) {
	eprintf("Option '-s' only valid with raw image types.\n");
	exit(1);
    }
    if (type != -1 && size.x == INT_MIN) {
	eprintf("Must specify image size for raw image type.\n");
	exit(1);
    }

    droi.origin.x = droi.origin.y = 0;
    droi.size.x = droi.size.y = -1;

    useroi = getarg(optlist,O_ROIORG, &droi.origin);
    useroi |= getarg(optlist,O_ROISIZE, &droi.size);

    nroi = NULL;
    getarg(optlist,O_ROIIM, &nroi);
    if (nroi && useroi) {
	eprintf("Can only use one of -roi or of -ao and -as options.\n");
	exit(1);
    }

    getarg(optlist,O_MORE, &marg);

    db = 0;
    if (getarg(optlist,O_IMTYPE,NULL)) db = 2;
    if (getarg(optlist,O_DB,NULL)) db = 1;

    getarg(optlist,O_DUMP,&dump);
    row = -1;
    if (getarg(optlist,O_DUMPROW,&row)) {
	dump = TRUE;
	if (row < -1) {
	    eprintf("Can't have negative row number.\n");
	    exit(1);
	}
    }

    freeargs(optlist);

    /* Done command line interpretation, now process images */

    if (fname && STREQUAL(fname,"-")) fname = NULL;

    if (fname == NULL) {
	/* No images in command line - use stdin */

	fname = ip_msg_stdin;
	if (type  == -1) {
	    fltype = ip_read_filehdr(NULL, &f, hdrbytes);
	    switch (fltype) {
#if 0
	    case FT_VISILOG:
		fh = open_visilog_imagex(f,fname,hdrbytes,NO_FLAG);
		im = read_visilog_image(fh);
		close_visilog_image(fh);
		break;
#endif
	    case FT_PNG:
		fh = open_png_imagex(f,fname,hdrbytes,NO_FLAG);
		im = read_png_image(fh);
		close_png_image(fh);
		break;
	    case FT_JPEG:
		fh = open_jpeg_imagex(f,fname,hdrbytes,NO_FLAG);
		im = read_jpeg_image(fh);
		close_jpeg_image(fh);
		break;
	    case FT_TIFF:
		eprintf("Can't read %s images from stdin.\n",
			ip_msg_filetype[fltype]);
		exit(1);
		break;
	    default:
		eprintf("Unrecognised image type.\n");
		exit(1);
	    }
	}else{
	    fltype = FT_UNKNOWN;
	    im = read_raw_imagex(NULL,type,size.x,size.y,header,rowskip,0,NO_FLAG);
	}

	if (useroi) {
	    if (droi.size.x == -1 && droi.size.y == -1) {
		droi.size.x = im->size.x - droi.origin.x;
		droi.size.y = im->size.y - droi.origin.y;
	    }
	    roi = ip_alloc_roi(ROI_RECTANGLE, droi);
	}else{
	    roi = ip_alloc_roi(ROI_IMAGE, im);
	}

	roiim = NULL;
	if (nroi) {
	    roiim = ip_read_image(nroi, IM_BINARY, -1);
	    if (roiim->size.x != im->size.x || roiim->size.y != im->size.y) {
		eprintf("ROI image must be same size as analysis image.\n");
		exit(1);
	    }
	}

	print_imstats(im, fname, fltype, roi, roiim, db);
	if (dump) {
	    dump_image_data(im, roi, row);
	}

	if (nroi) ip_free_image(roiim);
	ip_free_image(im);
	ip_free_roi(roi);

    }else{
	/* For each image in command line do */


	roiim = NULL;
	if (nroi) {
	    roiim = ip_read_image(nroi, IM_BINARY, -1);
	}

	for (marg = marg-1; marg < argc ; marg++) {

	    if (type == -1) {
		fltype = ip_filetype(argv[marg]);
		switch (fltype) {
#if 0
		case FT_VISILOG:
		    fh = open_visilog_image(argv[marg]);
		    im = read_visilog_image(fh);
		    close_visilog_image(fh);
		    break;
		case FT_CIMAGES:
		    fh = open_cimages_image(argv[marg]);
		    im = read_cimages_image(fh);
		    close_cimages_image(fh);
		    break;
#endif
		case FT_TIFF:
		    fh = open_tiff_image(argv[marg]);
		    im = read_tiff_image(fh);
		    close_tiff_image(fh);
		    break;
		case FT_PNG:
		    fh = open_png_image(argv[marg]);
		    im = read_png_image(fh);
		    close_png_image(fh);
		    break;
		case FT_JPEG:
		    fh = open_jpeg_image(argv[marg]);
		    im = read_jpeg_image(fh);
		    close_jpeg_image(fh);
		    break;
		default:
		    eprintf("Unrecognised image type.\n");
		    exit(1);
		}
	    }else{
		fltype = FT_UNKNOWN;
		im = read_raw_imagex(argv[marg],type,size.x,size.y,header,rowskip,0,NO_FLAG);
	    }

	    if (nroi) {
		if (roiim->size.x != im->size.x || roiim->size.y != im->size.y) {
		    eprintf("ROI image must be same size as analysis image.\n");
		    exit(1);
		}
	    }

	    if (useroi) {
		if (droi.size.x == -1 && droi.size.y == -1) {
		    droi.size.x = im->size.x - droi.origin.x;
		    droi.size.y = im->size.y - droi.origin.y;
		}
		roi = ip_alloc_roi(ROI_RECTANGLE, droi);
	    }else{
		roi = ip_alloc_roi(ROI_IMAGE, im);
	    }

	    print_imstats(im, argv[marg], fltype, roi, roiim, db);

	    if (dump) {
		dump_image_data(im, roi, row);
	    }

	    ip_free_image(im);
	    ip_free_roi(roi);
	}
	if (nroi)
	    ip_free_image(roiim);
    }

    return 0;
}

#define crealf(x) (x).r
#define cimagf(x) (x).i

void print_imstats(ip_image *im, const char *fname, int fltype,
		   ip_roi *roi, ip_image *roiim, int db)
{
    ip_stats *st;

    st = ip_alloc_stats(NO_FLAG);
    if (db == 1) {		/* Short database format */
	calc_image_stats(im, st, roi, roiim);
	mprintf("%s %g %g %g %g %g\n",
		fname,st->min,st->max,st->mean,st->stddev,st->rms);
    }else if (db == 2) {		/* Only want image types */
	mprintf("%s ",fname);
	if (fltype == FT_UNKNOWN) {
	    mprintf("RAW ");
	}else{
	    mprintf("%s ",ip_msg_filetype[fltype]);
	}
	mprintf("%s %dx%d\n",ip_image_type_info[im->type].name,im->size.x,im->size.y);
    }else{			/* Full stats listing */
	calc_image_stats(im, st, roi, roiim);
	mprintf("%s:\n",fname);
	if (fltype == FT_UNKNOWN) {
	    mprintf(" [RAW] ");
	}else{
	    mprintf(" [%s] ",ip_msg_filetype[fltype]);
	}
	mprintf("%s size=(%d,%d)\n",ip_image_type_info[st->type].name,st->size.x,st->size.y);
	if (st->type == IM_COMPLEX || st->type == IM_DCOMPLEX) {
	    mprintf(" Magnitude: min at (%d,%d) = %g, max at (%d,%d) = %g.\n",
		    st->minpt.x,st->minpt.y,st->min,
		    st->maxpt.x,st->maxpt.y,st->max);
	    mprintf("            sum %g,   mean %g\n",
		    st->sum,st->mean);
	    mprintf("            stddev %g,   RMS %g\n",
		    st->stddev,st->rms);
	    mprintf(" Real:      min at (%d,%d) = %g, max at (%d,%d) = %g.\n",
		    st->cminpt_r.x,st->cminpt_r.y,crealf(st->cmin),
		    st->cmaxpt_r.x,st->cmaxpt_r.y,crealf(st->cmax));
	    mprintf(" Imaginery: min at (%d,%d) = %g, max at (%d,%d) = %g.\n",
		    st->cminpt_i.x,st->cminpt_i.y,cimagf(st->cmin),
		    st->cmaxpt_i.x,st->cmaxpt_i.y,cimagf(st->cmax));
	    mprintf(" Sum:    %g %c i %g\n",
		    crealf(st->csum),"+-"[cimagf(st->csum) < 0],fabs(cimagf(st->csum)));
	    mprintf(" Mean:      %g %c i %g\n",
		    crealf(st->cmean),"+-"[cimagf(st->cmean) < 0],fabs(cimagf(st->cmean)));
	    mprintf(" Stddev:    %g %c i %g\n",
		    crealf(st->cstddev),"+-"[cimagf(st->cstddev) < 0],fabs(cimagf(st->cstddev)));
	    mprintf(" RMS:       %g %c i %g\n",
		    crealf(st->crms),"+-"[cimagf(st->crms) < 0],fabs(cimagf(st->crms)));
	}else if (image_type_clrim(im)) {
	    mprintf(" Intensity: min at (%d,%d) = %g, max at (%d,%d) = %g.\n",
		    st->minpt.x,st->minpt.y,st->min,
		    st->maxpt.x,st->maxpt.y,st->max);
	    mprintf("            sum %g,   mean %g\n",
		    st->sum,st->mean);
	    mprintf("            stddev %g,   RMS %g\n",
		    st->stddev,st->rms);
	    mprintf(" Red:       min at (%d,%d) = %g, max at (%d,%d) = %g.\n",
		    st->rminpt_r.x,st->rminpt_r.y,st->rmin.r,
		    st->rmaxpt_r.x,st->rmaxpt_r.y,st->rmax.r);
	    mprintf(" Green:     min at (%d,%d) = %g, max at (%d,%d) = %g.\n",
		    st->rminpt_g.x,st->rminpt_g.y,st->rmin.g,
		    st->rmaxpt_g.x,st->rmaxpt_g.y,st->rmax.g);
	    mprintf(" Blue:      min at (%d,%d) = %g, max at (%d,%d) = %g.\n",
		    st->rminpt_b.x,st->rminpt_b.y,st->rmin.b,
		    st->rmaxpt_b.x,st->rmaxpt_b.y,st->rmax.b);
	    mprintf(" Sum:    (%g, %g, %g)\n",
		    st->rsum.r,st->rsum.g,st->rsum.b);
	    mprintf(" Mean:      (%g, %g, %g)\n",
		    st->rmean.r,st->rmean.g,st->rmean.b);
	    mprintf(" Stddev:    (%g, %g, %g)\n",
		    st->rstddev.r,st->rstddev.g,st->rstddev.b);
	    mprintf(" RMS:       (%g, %g, %g)\n",
		    st->rrms.r,st->rrms.g,st->rrms.b);
	    if (im->type == IM_PALETTE) {
		mprintf(" Pen:       min = %d,  max = %d\n",st->penmin,st->penmax);
	    }
	}else{
	    mprintf(" Min at (%d,%d) = %g, Max at (%d,%d) = %g.\n",
		    st->minpt.x,st->minpt.y,st->min,
		    st->maxpt.x,st->maxpt.y,st->max);
	    mprintf(" Sum = %g,  mean = %g,  stddev = %g,  RMS = %g\n",
		    st->sum,st->mean,st->stddev,st->rms);
	}
	if (st->infinities)
	    mprintf(" WARNING: Image contains infinities or NaNs.\n");
    }
    ip_free_stats(st);
}



void dump_image_data(ip_image *im, ip_roi *roi, int row)
{
    ip_coord2d origin;
    ip_coord2d botright;

    origin = roi_get(ROI_ORIGIN,roi).pt;
    botright = roi_get(ROI_BOTRIGHT,roi).pt;

    mprintf("\n");
    if (row>=0) {
	if (row >= im->size.y) {
	    eprintf("Row (%d) outside of image bounds.\n",row);
	    exit(1);
	}
	dump_row(im,row,origin.x,botright.x);
    }else{
	for (row=origin.y; row<=botright.y; row++) {
	    dump_row(im,row,origin.x,botright.x);
	}
    }
}


void dump_row(ip_image *im, int row, int start, int end)
{
    int height;
    int width;
    int numfields;
    int indent;
    int linecnt;
    int i;
    char colstr[16];

    indent = 6;
    if (start != 0) {
	sprintf(colstr,"(%d)",start);
	indent += strlen(colstr);
    }

    if (termsize(&height, &width)) {
	width -= indent;
    }else{
	width = 80-indent;
    }

    switch (im->type){
    case IM_BINARY:
	numfields = width / 2;
	break;
    case IM_UBYTE:
    case IM_BYTE:
    case IM_PALETTE:
	numfields = width / 4;
	break;
    case IM_USHORT:
    case IM_SHORT:
	numfields = width / 7;
	break;
    case IM_LONG:
    case IM_ULONG:
    case IM_FLOAT:
    case IM_DOUBLE:
	numfields = width / 12;
	break;
    case IM_COMPLEX:
    case IM_DCOMPLEX:
	numfields = width / 24;
	break;
    case IM_RGB:
	numfields = width / 14;
	break;
    case IM_RGB16:
    case IM_RGBA:
	numfields = width / 20;
	break;
    default:
	numfields = 1;
    }

    mprintf("%4d:",row);
    if (start != 0) mprintf(colstr);
    mprintf(" ");

    linecnt = 0;
    for (i=start; i<=end; i++) {
	switch(im->type) {
	case IM_BINARY:
	    mprintf("%d", IM_PIXEL(im,i,row,b) ? 1 : 0);
	    break;
	case IM_PALETTE:
	case IM_UBYTE:
	    mprintf("%-3d", IM_PIXEL(im,i,row,ub));
	    break;
	case IM_BYTE:
	    mprintf("%-3d", IM_PIXEL(im,i,row,b));
	    break;
	case IM_USHORT:
	    mprintf("%-3d", IM_PIXEL(im,i,row,us));
	    break;
	case IM_SHORT:
	    mprintf("%-6d", IM_PIXEL(im,i,row,s));
	    break;
	case IM_ULONG:
	    mprintf("%-11d", IM_PIXEL(im,i,row,ul));
	    break;
	case IM_LONG:
	    mprintf("%-11d", IM_PIXEL(im,i,row,l));
	    break;
	case IM_FLOAT:
	    mprintf("%-11.5g", IM_PIXEL(im,i,row,f));
	    break;
	case IM_DOUBLE:
	    mprintf("%-11.4g", IM_PIXEL(im,i,row,d));
	    break;
	case IM_COMPLEX:
	    mprintf("(%-10g,%-10g)",
		    crealf(IM_PIXEL(im,i,row,c)),cimagf(IM_PIXEL(im,i,row,c)));
	    break;
	case IM_DCOMPLEX:
	    mprintf("(%-10g,%-10g)",
		    crealf(IM_PIXEL(im,i,row,dc)),cimagf(IM_PIXEL(im,i,row,dc)));
	    break;
	case IM_RGB:
	    mprintf("(%-3d,%-3d,%-3d)",
		    IM_PIXEL(im,i,row,r).r,
		    IM_PIXEL(im,i,row,r).g,
		    IM_PIXEL(im,i,row,r).b);
	    break;
	case IM_RGBA:
	    mprintf("(%-3d,%-3d,%-3d,%-3d)",
		    IM_PIXEL(im,i,row,ra).r,
		    IM_PIXEL(im,i,row,ra).g,
		    IM_PIXEL(im,i,row,ra).b,
		    IM_PIXEL(im,i,row,ra).a);
	    break;
	case IM_RGB16:
	    mprintf("(%-5d,%-5d,%-5d)",
		    IM_PIXEL(im,i,row,r16).r,
		    IM_PIXEL(im,i,row,r16).g,
		    IM_PIXEL(im,i,row,r16).b);
	    break;
	default:
	    eprintf("Not support image type [%s].\n", ip_type_name(im->type));
	    exit(1);
	}
	linecnt++;
	if (linecnt == numfields) {
	    linecnt = 0;
	    mprintf("\n");
	    if (i != end) {
		if (indent == 6) {
		    mprintf("      ");
		}else{
		    int ii;
		    for (ii=0; ii<indent; ii++) {
			mprintf(" ");
		    }
		}
	    }
	}else{
	    if (i != end)
		mprintf(" ");
	    else
		mprintf("\n");
	}
    }
}



void calc_image_stats(ip_image *im, ip_stats *st, ip_roi *roi, ip_image *roiim)
{
    /* These arrays should really be in the IP library, but they don't appear
       to be so declare them here. */


    if (roiim == NULL) {
	im_stats(im,st,roi);
    }else{
	ip_image *tmp;
	ip_stats *tstats;
	double numpixs, fixnum;

	/* Admittedly this is a rather slow way of calculating stats over an
	   arbitrary ROI, but at least this works without having to write tons
	   of code for im_stats() and the ROI code in the ip library. */

	tstats = ip_alloc_stats(NO_FLAG);
	im_stats(roiim, tstats, NULL);
	numpixs = tstats->sum;
	fixnum = numpixs / ((double)im->size.x * im->size.y);

	im_stats(im, tstats, NO_FLAG);

	tmp = im_copy(im);
	im_mask(tmp, roiim, 0, NO_FLAG);
	im_stats(tmp, st, NO_FLAG);

	st->mean /= fixnum;
	st->rms /= sqrt(fixnum);
	st->stddev = sqrt(st->rms*st->rms - st->mean*st->mean);

	if (st->min == 0) {
	    /* Need to find new min */
	    im_mask(tmp, roiim, ip_image_type_info[im->type].maxvalue, NO_FLAG);
	    im_stats(tmp, tstats, NULL);
	    st->min = tstats->min;
	    st->minpt = tstats->minpt;
	}
	if (st->max == 0) {
	    /* Need to find new max */
	    im_mask(tmp, roiim, ip_image_type_info[im->type].minvalue, NO_FLAG);
	    im_stats(tmp, tstats, NULL);
	    st->max = tstats->max;
	    st->maxpt = tstats->maxpt;
	}

	ip_free_image(tmp);
	ip_free_stats(tstats);
    }
}
