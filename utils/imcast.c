/*
   Programme: imcast
   Author: M.J.Cree

   Copyright (C) 1997, 2000-2001, 2019 by Michael J. Cree.

   Cast an image to a different type.
   For example, convert a byte image to a floating point image.

*/

#include <stdlib.h>
#include <string.h>

#include <ip/ip.h>
#include <ip/file.h>
#include <ip/raw.h>
#include <ip/tiff.h>
#include <ip/png.h>

#include "helpers.h"

#define PROGRAMME "imcast"
#define VERSION "1.06"
#define DATE "3/1/2019"


/* Program command line parsing and opt data structures */

static char progstr[] = PROGRAMME " (V" VERSION ")";

struct progenv prog = {
    NULL,
    PROGRAMME,
    VERSION,
    DATE
};


struct op help[] = {
    {OPT_SWITCH,		"binary", NULL,	 "Convert to binary mask (1 bit)." },
    {OPT_SWITCH,		"ubyte", NULL,	 "Convert to unsigned byte (8 bit)." },
    {OPT_SWITCH,		"ushort", NULL,	 "Convert to unsigned short (16 bit)." },
    {OPT_SWITCH,		"ulong", NULL,	 "Convert to unsigned long (32 bit)." },
    {OPT_SWITCH,		"float", NULL,	 "Convert to IEEE float." },
    {OPT_SWITCH,		"double", NULL,	 "Convert to IEEE double." },
    {OPT_SWITCH,		"complex", NULL, "Convert to single prec. complex." },
    {OPT_SWITCH,		"dcomplex", NULL, "Convert to double prec. complex." },
    {OPT_SWITCH,		"palette", NULL, "Convert to palette colour image." },
    {OPT_SWITCH,		"rgb", NULL,	 "Convert to RGB colour image." },
    {OPT_SWITCH,		"rgb16", NULL,	 "Convert to RGB16 colour image." },
    {OPT_SWITCH,		"b1", NULL,	 "Preserve unity values of binary "
     "images when upcasting [def = 255]."},
    {OPT_SWITCH,		"clip", NULL,	 "Clip pixel values to new image"
     " range when downcasting." },
    {OPT_SWITCH,		"real", NULL,	 "Real part of complex image." },
    {OPT_SWITCH,		"imag", NULL,	 "Imaginery part of complex image." },
    {OPT_SWITCH,		"mag", NULL,	 "Magnitude part of complex image." },
    {OPT_SWITCH,		"phase", NULL,	 "Phase part of complex image." },
    {OPT_SWITCH,		"red", NULL,	 "Red part of colour image." },
    {OPT_SWITCH,		"green", NULL,	 "Green part of colour image." },
    {OPT_SWITCH,		"blue", NULL,	 "Blue part of colour image." },
    {OPT_SWITCH,		"intmag", NULL,	 "Intensity (mag) of colour image." },
    {OPT_SWITCH,		"intyuv", NULL,	 "Intensity (y part of yuv) of colour image." },
    {OPT_SWITCH,		"tiff", NULL,
     "Write result as TIFF file [def = png]"},
    {OPT_SWITCH,		"png", NULL,
     "Write result as PNG file [def = png]"},
    {OPT_SWITCH,         "nc", NULL,      "Do not compress output image."},
    {OPT_STRING | OPT_LONELY | OPT_NIN, NULL, "imagein",
     "Input image filename [def = stdin]" },
    {OPT_STRING | OPT_LONELY, NULL,"imageout",
     "Output image filename [def = stdout]" },
    {OPT_DONE, NULL, NULL,
     "Cast one image type to another image type."}
};


enum {
    /* Image type options should come first */
    O_BINARY, O_UBYTE, O_USHORT, O_ULONG,
    O_FLOAT, O_DOUBLE, O_COMPLEX, O_DCOMPLEX,
    O_PALETTE, O_RGB, O_RGB16,
    /* Then other options */
    O_B1,
    O_CLIP,
    O_REAL, O_IMAG, O_MAG, O_PHASE,
    O_RED, O_GREEN, O_BLUE, O_INTMAG, O_INTYUV,
    O_TIFF, O_PNG, O_NOCOMPRESS,
    O_IMAGEIN, O_IMAGEOUT
};




static void bombout(ip_error_logstack *ipr, int error, const char *extra, va_list ap)
{
    ip_print_error(PROGRAMME, ipr, error, extra, ap);
    exit(1);
}

/*
 * For inserting types into cnvrt_type array below to make the connection
 * between the option enumeration numbers defined above and the IP library
 * image type.
 */
#define INSERT_IMTYPE(t) [O_##t] = IM_##t

static int type_arg(optlist *ol, int option, int type)
{
    static int cnvrt_type[] = {
	INSERT_IMTYPE(BINARY),
	INSERT_IMTYPE(UBYTE),
	INSERT_IMTYPE(USHORT),
	INSERT_IMTYPE(ULONG),
	INSERT_IMTYPE(FLOAT),
	INSERT_IMTYPE(DOUBLE),
	INSERT_IMTYPE(COMPLEX),
	INSERT_IMTYPE(DCOMPLEX),
	INSERT_IMTYPE(PALETTE),
	INSERT_IMTYPE(RGB),
	INSERT_IMTYPE(RGB16)
    };

    if (getarg(ol, option, NULL)) {
	if (type != -1) {
	    eprintf("Only one output type can be specified on command line.\n");
	    exit(1);
	}
	type = cnvrt_type[option];
    }
    return type;
}



int main(int argc, char *argv[])
{
    optlist *ol;
    const char *nin, *nout;
    ip_image *im, *dest;
    int flags;
    int type;
    enum {TIFF, PNG} otype;
    int nc;
    char descstr[256];

    register_evi(ip_version());

    prog.output = stdout;
    ol = parseargs(argc,argv,&prog,help);

    nin = nout = NULL;
    flags = NO_FLAG;

    /* Parse output image type */

    type = -1;
    type = type_arg(ol, O_BINARY, type);
    type = type_arg(ol, O_UBYTE, type);
    type = type_arg(ol, O_USHORT, type);
    type = type_arg(ol, O_ULONG, type);
    type = type_arg(ol, O_FLOAT, type);
    type = type_arg(ol, O_DOUBLE, type);
    type = type_arg(ol, O_COMPLEX, type);
    type = type_arg(ol, O_PALETTE, type);
    type = type_arg(ol, O_RGB, type);
    type = type_arg(ol, O_RGB16, type);

    if (type == -1) {
	eprintf("Must specify an output type.\n");
	exit(1);
    }

    if (getarg(ol, O_CLIP, NULL))
	flags |= FLG_CLIP;

    if (getarg(ol, O_B1, NULL))
	flags |= FLG_B1;

    if (getarg(ol, O_RED, NULL))
	flags |= FLG_RED;
    if (getarg(ol, O_GREEN, NULL))
	flags |= FLG_GREEN;
    if (getarg(ol, O_BLUE, NULL))
	flags |= FLG_BLUE;

    if (getarg(ol, O_REAL, NULL))
	flags |= FLG_REAL;
    if (getarg(ol, O_IMAG, NULL))
	flags |= FLG_IMAG;
    if (getarg(ol, O_MAG, NULL))
	flags |= FLG_MAG;
    if (getarg(ol, O_PHASE, NULL))
	flags |= FLG_ANGLE;

    if (getarg(ol, O_INTYUV, NULL))
	flags |= FLG_MAGYUV;
    if (getarg(ol, O_INTMAG, NULL))
	flags |= FLG_MAG;

    otype = PNG;
    if (getarg(ol, O_TIFF, NULL))
	otype = TIFF;
    if (getarg(ol, O_PNG, NULL))
	otype = PNG;
    getarg(ol, O_NOCOMPRESS, &nc);

    getarg(ol, O_IMAGEIN, &nin);
    getarg(ol, O_IMAGEOUT, &nout);

    freeargs(ol);

    ip_register_error_handler(bombout);

    /* Load image to convert */

    if (nin && STREQUAL(nin,"-")) nin = NULL;
    if (nout && STREQUAL(nout,"-")) nout = NULL;

    im = ip_read_image(nin, IM_BINARY, IM_UBYTE, IM_USHORT, IM_ULONG, IM_FLOAT,
		       IM_DOUBLE, IM_COMPLEX, IM_DCOMPLEX, IM_PALETTE, IM_RGB,
		       IM_RGB16, -1);

    if (nin == NULL) nin = ip_msg_stdin;

    if (im->type == type) {
	eprintf("Image %s is already of type specified, why cast it?", nin);
	exit(1);
    }

    /* Cast image to new type */

    dest = im_convert(im, type, flags);
    ip_free_image(im);
    im = dest;

    /* And save it. */

    switch (otype) {
    case TIFF:
	sprintf(descstr, "Image %s cast to type %s.", nin, ip_type_name(type));
	write_tiff_imagex(nout, im, FLG_TAGLIST,
			  TAG_SOFTWARE, progstr,
			  TAG_DESCRIPTION, descstr,
			  TAG_DATE,
			  nc ? TAG_NOCOMP : TAG_DONE,
			  TAG_DONE);
	break;
    case PNG:
	write_png_image(nout, im);
	break;
    }

    return 0;
}

