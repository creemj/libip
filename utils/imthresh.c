/*
   Programme: imthresh
   Author:    M.J.Cree

   Copyright (C) 1997, 1998, 2000, 2019 by Michael J. Cree

DESCRIPTION:

   Threshold an image.

*/

#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <float.h>

#include <ip/ip.h>
#include <ip/file.h>
#include <ip/tiff.h>
#include <ip/png.h>

#include "helpers.h"

#define PROGRAMME "imthresh"
#define VERSION "1.045"
#define DATE "3/1/2019"


/* Program command line parsing and opt data structures */

static char progstr[] = PROGRAMME " (V" VERSION ")";

struct progenv prog = {
    NULL,
    PROGRAMME,
    VERSION,
    DATE
};

struct op help[] = {
    {OPT_DOUBLE | OPT_REQ,"t", "threshold", "Threshold level."},
    {OPT_SWITCH,		"inv",  NULL, "Invert sense of threshold."},
    {OPT_SWITCH,		"tiff", NULL, "Output image as TIFF. [def = PNG]"},
    {OPT_SWITCH,		"png",  NULL, "Output image as PNG. [def = PNG]"},
    {OPT_SWITCH,         "nc",   NULL, "Do not compress output image."},
    {OPT_STRING | OPT_LONELY | OPT_NIN, NULL, "imagein",
				      "Input image filename. [def = stdin]" },
    {OPT_STRING | OPT_LONELY,	       NULL, "imageout",
				      "Output (thresholded) image filename."
				      " [def = stdout]" },
    {OPT_DONE, NULL, NULL,
       "Threshold an image to produce a BINARY image.\n" }
};

enum {
    O_THRESH, O_INVERT,
    O_TIFF, O_PNG, O_NOCOMP,
    O_IMAGEIN,   O_IMAGEOUT
};



static void bombout(ip_error_logstack *ipr, int error, const char *extra, va_list ap)
{
    ip_print_error(PROGRAMME,ipr,error,extra,ap);
    exit(1);
}



int main(int argc, char *argv[])
{
    const char *nin, *nout;
    ip_image *im, *th;
    double lower, upper;
    int tiff, png, nocomp;
    char desc[250];
    optlist *ol;

    /* Parse arguments */

    nin = nout = NULL;

    register_evi(ip_version());

    prog.output = stdout;
    ol = parseargs(argc, argv, &prog, help);

    getarg(ol, O_TIFF, &tiff);
    getarg(ol, O_PNG, &png);
    getarg(ol, O_NOCOMP, &nocomp);

    getarg(ol, O_THRESH, &lower);

    getarg(ol, O_IMAGEIN, &nin);
    getarg(ol, O_IMAGEOUT, &nout);

    ip_register_error_handler(bombout);

    /* Read in image */

    if (nin && STREQUAL(nin, "-")) nin = NULL;
    if (nout && STREQUAL(nout, "-")) nout = NULL;

    im = ip_read_image(nin, IM_UBYTE, IM_SHORT, IM_USHORT, IM_LONG, IM_ULONG,
		            IM_FLOAT, IM_DOUBLE, -1);

    upper = ip_type_max(im->type);

    if (getarg(ol, O_INVERT, NULL)) {
	double t = lower;
	lower = upper;
	upper = t;
    }

    freeargs(ol);

    /* Threshold image it */

    th = im_threshold(im, lower, upper);
    ip_free_image(im);

    /* Write out result */

    sprintf(desc,"%s thresholded at value %g.", nin ? nin : ip_msg_stdin, lower);

    if (tiff)
	write_tiff_imagex(nout, th, FLG_TAGLIST,
			  TAG_SOFTWARE, progstr,
			  TAG_DESCRIPTION, desc,
			  TAG_DATE,
			  nocomp ? TAG_NOCOMP : TAG_DONE,
			  TAG_DONE);
    else
	write_png_image(nout, th);

    return 0;
}
