/*
   Programme: imarith
   Author:    M.J.Cree

   Copyright (C) 1996, 1997, 2000, 2001, 2015, 2018 by Michael J. Cree

DESCRIPTION:

   Perform various arithmetic operations on images.

*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <ip/ip.h>
#include <ip/file.h>
#include <ip/tiff.h>
#include <ip/png.h>

#include "helpers.h"


#define PROGRAMME "imarith"
#define VERSION "2.01"
#define DATE "5/1/2018"

/* Program command line parsing and opt data structures */

static char progstr[128];

struct progenv prog = {
  NULL,
  PROGRAMME,
  VERSION,
  DATE
};

 struct op help[] = {
   {OPT_DOUBLE,		"c", "constant","Unary arithmetic: Constant value."},
   {OPT_STRING ,        "i", "image",	"Binary arithmetic: Image filename."
					" ['-' = stdin]" },
   {OPT_SWITCH,		"clip", NULL,	"Clip result to image type range."},
   {OPT_SWITCH,		"tiff", NULL,	"Write result as TIFF file"
					" [def = png]"},
   {OPT_SWITCH,		"png", NULL,	"Write result as PNG file"
					" [def = png]"},
   {OPT_STRING | OPT_LONELY | OPT_NIN, NULL, "imagein",
					"Input image filename."
					" [def = stdin]" },
   {OPT_STRING | OPT_LONELY, NULL,"imageout",
					"Output image filename."
					" [def = stdout]" },
   {OPT_DONE, NULL, NULL, NULL}
};


enum {
   O_CONST,   O_IMAGE2,
   O_CLIP,
   O_TIFF, O_PNG,
   O_IMAGEIN, O_IMAGEOUT,
   O_DESC
};


char *imadd_desc = "Add a scalar constant or another image to an image.";
char *imsub_desc = "Subtract a scalar constant or another image from an"
                   " image.";
char *immul_desc = "Multiply an image by a scalar constant or by another"
		   " image";
char *imdiv_desc = "Divide an image by a scalar constant or by another"
		   " image";


static void bombout(ip_error_logstack *ipr, int error, const char *extra, va_list ap)
{
    ip_print_error(prog.progname,ipr,error,extra,ap);
    exit(1);
}


int main(int argc, char *argv[])
{
    const char *nin,*nout;
    double constant;
    const char *nim2;
    int flags;
    enum {TIFF, PNG} otype;
    optlist *ol;
    char desc[500];
    enum {
	IMADD, IMSUB, IMMUL, IMDIV
    } operation;
    ip_image *im,*im2;


   /* Sort out which programme we are running */

    if ((prog.progname = strrchr(argv[0], '/')) == NULL)
       prog.progname = argv[0];
    else
	prog.progname++;
    prog.output = stdout;

    if (STREQUAL(prog.progname, "imadd")) {
	operation = IMADD;
	help[O_DESC].helpmsg = imadd_desc;
    }else if (STREQUAL(prog.progname, "imsub")) {
	operation = IMSUB;
	help[O_DESC].helpmsg = imsub_desc;
    }else if (STREQUAL(prog.progname, "immul")) {
	operation = IMMUL;
	help[O_DESC].helpmsg = immul_desc;
    }else if (STREQUAL(prog.progname, "imdiv")) {
	operation = IMDIV;
	help[O_DESC].helpmsg = imdiv_desc;
    }else{
	eprintf("What is '%s'???    I'm confused...\n",prog.progname);
	eprintf("Please call me 'imadd', 'imsub', 'immul' or 'imdiv'.\n");
	exit(1);
    }

    snprintf(progstr, 128, "%s (V%s)", prog.progname, prog.version);

    /* Parse command line */

    register_evi(ip_version());

    ol = parseargs(argc,argv,&prog,help);

    nin = nout = NULL;
    constant = 0.0;
    nim2 = NULL;

    if (getarg(ol,O_CONST,&constant) && constant == 0.0) {
	eprintf("What a stupid constant value. Try something other than zero!\n");
	exit(1);
    }

    if (getarg(ol,O_IMAGE2,&nim2) && constant != 0.0) {
	eprintf("Can only have one of '-c' and '-i' options.\n");
	exit(1);
    }

    if (constant == 0.0 && nim2 == NULL) {
	eprintf("Must have one of '-c' or '-i' options specified.\n");
	exit(1);
    }

    flags = NO_FLAG;
    if (getarg(ol, O_CLIP, NULL))
	flags |= FLG_CLIP;

    otype = PNG;
    if (getarg(ol, O_TIFF, NULL))
	otype = TIFF;
    if (getarg(ol, O_PNG ,NULL))
	otype = PNG;
    getarg(ol, O_IMAGEIN, &nin);
    getarg(ol, O_IMAGEOUT, &nout);

    freeargs(ol);

    ip_register_error_handler(bombout);

    /* Load image(s) */

    if (nin && STREQUAL(nin,"-")) nin = NULL;
    if (nout && STREQUAL(nout,"-")) nout = NULL;

    im = ip_read_image(nin,IM_BYTE,IM_UBYTE,IM_USHORT,IM_SHORT,IM_ULONG,IM_LONG,
		       IM_FLOAT,IM_DOUBLE,IM_COMPLEX,IM_DCOMPLEX,IM_RGB,IM_RGB16,-1);

    if (nin == NULL) nin = ip_msg_stdin;

    if (nim2) {

	/* Load second image */

	if (STREQUAL(nim2,"-")) nim2 = NULL;

	im2 = ip_read_image(nim2, IM_BYTE,IM_UBYTE,IM_USHORT,IM_SHORT,IM_ULONG,IM_LONG,
			    IM_FLOAT,IM_DOUBLE,IM_COMPLEX,IM_DCOMPLEX,IM_RGB,IM_RGB16,-1);
	if (nim2 == NULL) nim2 = ip_msg_stdin;

	if (im2->type != im->type) {
	    eprintf("Both images must be same type.\n");
	    exit(1);
	}

	/* Perform arithmetic between two images */

	switch(operation) {
	case IMADD:
	    im_add(im,im2,flags);
	    sprintf(desc,"%s added with the image %s.",nin,nim2);
	    break;
	case IMSUB:
	    im_sub(im,im2,flags);
	    sprintf(desc,"%s minus the image %s.",nin,nim2);
	    break;
	case IMMUL:
	    im_mul(im,im2,flags);
	    sprintf(desc,"%s multiplied by the image %s.",nin,nim2);
	    break;
	case IMDIV:
	    im_div(im,im2,flags);
	    sprintf(desc,"%s divided by the image %s.",nin,nim2);
	    break;
	}

    }else{

	/* Perform arithmetic between image and scalar constant. */

	switch (operation) {
	case IMADD:
	    im_addc(im,constant,flags);
	    sprintf(desc,"%s added with the constant value %g.",nin,constant);
	    break;
       case IMSUB:
	   im_subc(im,constant,flags);
	   sprintf(desc,"%s minus the constant value %g.",nin,constant);
	   break;
	case IMMUL:
	    im_mulc(im,constant,flags);
	    sprintf(desc,"%s multiplied by the constant value %g.",nin,constant);
	    break;
	case IMDIV:
	    im_divc(im,constant,flags);
	    sprintf(desc,"%s divided by the constant value %g.",nin,constant);
	    break;
	}
    }

    switch (otype) {
    case TIFF:
	write_tiff_imagex(nout,im,FLG_TAGLIST,
			     TAG_SOFTWARE, progstr,
			     TAG_DESCRIPTION, desc,
			     TAG_DATE,
			     TAG_DONE);
	break;
    case PNG:
	write_png_image(nout,im);
	break;
    }

    return 0;
}
