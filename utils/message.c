/*
    Module: mjc/message.c
    Author: M.J.Cree

    (C) 1996-2007 Michael Cree

    For printing messages!

*/


#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

#include "helpers.h"


static FILE *mfile = NULL;	/* Data file to save messages to. */

static FILE *fm = NULL;		/* Default streams for messages */
static FILE *fi = NULL;		/* Set up on first call to vmsgprintf() */
static FILE *fw = NULL;
static FILE *fe = NULL;

static void vmsgprintf(int type, char *s, va_list ap);

#define M_MSG	  0
#define M_INFO	  1
#define M_WARNING 2
#define M_ERROR	  3




/*

NAME:

   open_mfile() \- Open a log file to attach to message routines

PROTOTYPE:

   #include <mjc/mjc.h>

   int  open_mfile( fname, flag )
   char *fname;
   int flag;

DESCRIPTION:

   Attach a file to the message printing routines so that any message
   printed will also be stored in the file.  The filename is given by
   'fname' and 'flag' must be one of:

\&   MFILE_CLEAR  Clear the file before writing any messages to it.
\&   MFILE_APPEND Append any messages to end of exisiting file.

\&-   Returns TRUE if the file was succesfully opened and FALSE if any
   errors occurred.

SEE ALSO:

   close_mfile()
   mprintf()    iprintf()    wprintf()    eprintf()
   vmprintf()   viprintf()   vwprintf()   veprintf()

*/

int open_mfile(char * fname, int flag)
{
    mfile = fopen(fname, flag == MFILE_CLEAR ? "w" : "w+");
    return (mfile != NULL);
}


/*

NAME:

   close_mfile() \- Close previously opened message log file

PROTOTYPE:

   #include <mjc/mjc.h>

   void  close_mfile( void )

DESCRIPTION:

   Close the file previously opened by open_mfile().

SEE ALSO:

   open_mfile()

*/


void close_mfile(void)
{
    if (mfile) {
	fclose(mfile);
	mfile = NULL;
    }
}



void mredirect(FILE *f)
{
    fflush(fm);
    fm = f;
}

void iredirect(FILE *f)
{
    fflush(fi);
    fi = f;
}

void wredirect(FILE *f)
{
    fflush(fw);
    fw = f;
}

void eredirect(FILE *f)
{
    fflush(fe);
    fe = f;
}



/*

  NAME:

  mprintf() \- Print a general message type message

  PROTOTYPE:

  #include <mjc/mjc.h>

  void mprintf( msg, ... )
  char *msg;

  DESCRIPTION:

  Print the message to stdout and to any opened log file.  'Msg' is a
  printf() formatted string.  'Msg' may also be NULL in which case a
  new-line character is output.

  SEE ALSO:

  open_mfile()   close_mfile()
  iprintf()      wprintf()       eprintf()
  vmprintf()     viprintf()      vwprintf()      veprintf()

*/

void mprintf(char *s, ...)
{
    va_list ap;

    va_start(ap,s);
    vmsgprintf(M_MSG,s,ap);
    va_end(ap);
}



/*

  NAME:

  iprintf() \- Print an info type message

  PROTOTYPE:

  #include <mjc/mjc.h>

  void iprintf( msg, ... )
  char *msg;

  DESCRIPTION:

  Print the information message to stdout and to any opened log file.
  'Msg' is a printf() formatted string.  The message is formatted
  with the word ''INFO:'' prefixed to the message.  'Msg' may also be
  NULL in which case a new-line character is printed without the ''INFO:''
  prefix.

  SEE ALSO:

  open_mfile()   close_mfile()
  mprintf()      wprintf()       eprintf()
  vmprintf()     viprintf()      vwprintf()      veprintf()

*/

void iprintf(char *s, ...)
{
    va_list ap;

    va_start(ap, s);
    vmsgprintf(M_INFO, s, ap);
    va_end(ap);
}



/*

  NAME:

  wprintf() \- Print a warning message

  PROTOTYPE:

  #include <mjc/mjc.h>

  void wprintf( msg, ... )
  char *msg;

  DESCRIPTION:

  Print the warning message to stderr and to any opened log file.
  'Msg' is a printf() formatted string.  The message is formatted
  with the word ''WARNING:'' prefixed to the message.  'Msg' may also
  be NULL in which case a new-line character is printed without
  the ''WARNING:'' prefix.

  SEE ALSO:

  open_mfile()   close_mfile()
  mprintf()      iprintf()       eprintf()
  vmprintf()     viprintf()      vwprintf()      veprintf()

*/

#ifdef WIN32
void xwprintf(char *s, ...)
#else
    void wprintf(char *s, ...)
#endif
{
    va_list ap;

    va_start(ap, s);
    vmsgprintf(M_WARNING, s, ap);
    va_end(ap);
}



/*

  NAME:

  eprintf() \- Print an error message

  PROTOTYPE:

  #include <mjc/mjc.h>

  void eprintf( msg, ... )
  char *msg;

  DESCRIPTION:

  Print the error message to stderr and to any opened log file.
  'Msg' is a printf() formatted string.  The message is formatted
  with the word ''ERROR:'' prefixed to the message.  'Msg' may also
  be NULL in which case a new-line character is printed without
  the ''ERROR:'' prefix.

  SEE ALSO:

  open_mfile()   close_mfile()
  mprintf()      iprintf()       wprintf()
  vmprintf()     viprintf()      vwprintf()      veprintf()

*/

void eprintf(char *s, ...)
{
    va_list ap;

    va_start(ap, s);
    vmsgprintf(M_ERROR, s, ap);
    va_end(ap);
}


/*

  NAME:

  vmprintf() \- Print a message

  PROTOTYPE:

  #include <mjc/mjc.h>

  void  vmprintf( msg, argptr )
  char *msg;
  va_list argptr;

  DESCRIPTION:

  The vprintf() like entry point for the routine mprintf().

  SEE ALSO:

  mprintf()

*/


void vmprintf(char *s, va_list ap)
{
    vmsgprintf(M_MSG, s, ap);
}



/*

  NAME:

  viprintf() \- Print an info message

  PROTOTYPE:

  #include <mjc/mjc.h>

  void  viprintf( msg, argptr )
  char *msg;
  va_list argptr;

  DESCRIPTION:

  The vprintf() like entry point for the routine iprintf().

  SEE ALSO:

  iprintf()

*/
void viprintf(char *s, va_list ap)
{
    vmsgprintf(M_INFO, s, ap);
}



/*

  NAME:

  vwprintf() \- Print a warning message

  PROTOTYPE:

  #include <mjc/mjc.h>

  void  vwprintf( msg, argptr )
  char *msg;
  va_list argptr;

  DESCRIPTION:

  The vprintf() like entry point for the routine wprintf().

  SEE ALSO:

  wprintf()

*/

#ifdef WIN32
void vxwprintf(char *s, va_list ap)
#else
    void vwprintf(char *s, va_list ap)
#endif
{
    vmsgprintf(M_WARNING, s, ap);
}



/*

  NAME:

  veprintf() \- Print an error message

  PROTOTYPE:

  #include <mjc/mjc.h>

  void  veprintf( msg, argptr )
  char *msg;
  va_list argptr;

  DESCRIPTION:

  The vprintf() like entry point for the routine eprintf().

  SEE ALSO:

  eprintf()

*/

void veprintf(char *s, va_list ap)
{
    vmsgprintf(M_ERROR, s, ap);
}



static void vmsgprintf(int type, char *s, va_list ap)
{
    FILE *f;
    char *msg;

    if (fm == NULL) {
	fm = stdout;
	fi = stdout;
	fw = stderr;
	fe = stderr;
    }

    switch (type) {
    case M_MSG:
	msg = NULL;
	f = fm;
	break;
    case M_INFO:
	msg = "INFO: ";
	f = fi;
	break;
    case M_WARNING:
	msg = "WARNING: ";
	f = fw;
	break;
    case M_ERROR:
	msg = "ERROR: ";
	f = fe;
	break;
    default:
	msg = NULL;
	f = fm;
	break;
    }
    if (s) {
	if (msg) fprintf(f, msg);
	vfprintf(f, s, ap);
	if (mfile) {
	    if (msg) fprintf(mfile, msg);
	    vfprintf(mfile, s, ap);
	}
    }else{
	fputc('\n', f);
	if (mfile)
	    fputc('\n', mfile);
    }
}
