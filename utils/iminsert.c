/*
   Programme: iminsert
   Author: M.J.Cree

   Copyright (C) 2002, 2019 by Michael J. Cree.

   Inserts images into another image

*/


#include <stdlib.h>
#include <string.h>

#include <ip/ip.h>
#include <ip/file.h>
#include <ip/tiff.h>
#include <ip/png.h>

#include "helpers.h"

#define PROGRAMME "iminsert"
#define VERSION "1.02"
#define DATE "3/1/2019"

/* Program command line parsing and opt data structures */

static char progstr[] = PROGRAMME " (V" VERSION ")";

struct progenv prog = {
    NULL,
    PROGRAMME,
    VERSION,
    DATE
};


struct op help[] = {
    {OPT_STRING,         "i", "insert","Filename of image to insert" },
    {OPT_COORD,          "o", "origin", "Origin of insert. [def = (0,0)]"},
    {OPT_SWITCH,		"fit", NULL,   "Cut insert to fit"},
    {OPT_SWITCH,		"tiff", NULL,
     "Write result as TIFF file [def = png]"},
    {OPT_SWITCH,		"png", NULL,
     "Write result as PNG file [def = png]"},
    {OPT_SWITCH,         "nc", NULL,    "Do not compress output image."},
    {OPT_STRING | OPT_LONELY | OPT_NIN, NULL, "imagein",
     "Input image filename [def = stdin]" },
    {OPT_STRING | OPT_LONELY, NULL,"imageout",
     "Output image filename [def = stdout]" },
    {OPT_MORE,           NULL, "...", NULL},
    {OPT_DONE, NULL, NULL, "Inserts the specified image into another image. "
     "You can specify multiple images to insert by alternately listing "
     "their coords and the filename in the argument list following the "
     "input/output filenames." }
};


enum {
    O_INSERT,
    O_ORIGIN, O_FIT,
    O_TIFF, O_PNG, O_NOCOMP,
    O_IMAGEIN, O_IMAGEOUT,
    O_MORE
};


static void bombout(ip_error_logstack *ipr, int error, const char *extra, va_list ap)
{
    ip_print_error(PROGRAMME, ipr, error, extra, ap);
    exit(1);
}


static void  do_insert(ip_image *im, ip_image *insert, ip_coord2d origin, int fit)
{
    ip_image *tmp;
    ip_coord2d size, ssize, sorigin;

    if  ((origin.x + insert->size.x > im->size.x)
	 || (origin.y + insert->size.y > im->size.y)) {
	if (fit) {
	    ip_coord2d origin;

	    origin.x = origin.y = 0;
	    size = insert->size;
	    if (origin.x + insert->size.x > im->size.x)
		size.x = im->size.x - origin.x;
	    if (origin.y + insert->size.y > im->size.y)
		size.y = im->size.y - origin.y;
	    tmp = im_cut(insert, origin, size);
	    ip_free_image(insert);
	    insert = tmp;
	}else{
	    eprintf("Given origin and size extend outside image of size (%d,%d).\n",
		    im->size.y, im->size.y);
	    exit(1);
	}
    }

    /* Do the dirty deed */

    ssize = insert->size;
    sorigin.x = sorigin.y = 0;
    im_insert(im, origin, insert, sorigin, ssize);

}


int main(int argc, char *argv[])
{
    optlist *ol;
    const char *nin, *nout, *ninsert;
    ip_image *im,  *insert;
    enum {TIFF, PNG} otype;
    int nocomp, fit;
    char descstr[256];
    ip_coord2d origin;
    int marg;

    register_evi(ip_version());

    prog.output = stdout;
    ol = parseargs(argc, argv, &prog, help);

    nin = nout = NULL;

    origin.x = origin.y = 0;
    getarg(ol, O_ORIGIN, &origin);

    if (origin.x < 0 || origin.y < 0) {
	eprintf("Given origin (%d,%d) contains negative numbers.\n",
		origin.x, origin.y);
	exit(1);
    }

    getarg(ol, O_FIT, &fit);

    otype = PNG;
    if (getarg(ol, O_PNG, NULL)) otype = PNG;
    if (getarg(ol, O_TIFF, NULL)) otype = TIFF;

    getarg(ol, O_NOCOMP, &nocomp);

    insert = NULL;
    getarg(ol, O_INSERT, &ninsert);
    getarg(ol, O_IMAGEIN, &nin);
    getarg(ol, O_IMAGEOUT, &nout);

    getarg(ol, O_MORE, &marg);

    freeargs(ol);

    ip_register_error_handler(bombout);

    if (ninsert == NULL && marg >= argc) {
	eprintf("No images to insert specified!\n");
	exit(1);
    }

    /* Load image to cut */

    if (nin && STREQUAL(nin, "-")) nin = NULL;
    if (nout && STREQUAL(nout, "-")) nout = NULL;

    im = ip_read_image(nin, IM_BINARY, IM_BYTE, IM_SHORT, IM_LONG,
		       IM_UBYTE, IM_USHORT, IM_ULONG,
		       IM_FLOAT, IM_DOUBLE, IM_COMPLEX, IM_DCOMPLEX,
		       IM_PALETTE, IM_RGB, IM_RGB16, -1);

    if (nin == NULL) nin = ip_msg_stdin;

    if (origin.x >= im->size.x || origin.y >= im->size.y) {
	eprintf("Origin (%d,%d) outside image of size = (%d,%d)\n",
		origin.x, origin.y, im->size.x, im->size.y);
	exit(1);
    }

    if (ninsert) {
	insert = ip_read_image(ninsert, IM_BYTE, IM_SHORT, IM_LONG,
		       IM_UBYTE, IM_USHORT, IM_ULONG,
		       IM_FLOAT, IM_DOUBLE, IM_COMPLEX, IM_DCOMPLEX,
		       IM_PALETTE, IM_RGB, IM_RGB16, -1);
	if (insert->type != im->type) {
	    eprintf("Image types differ. Can't proceed.\n");
	    exit(1);
	}

	do_insert(im, insert, origin, fit);
    }

    while (marg < argc) {
	if (sscanf(argv[marg], "%d%*1[x,]%d", &origin.x, &origin.y) != 2) {
	    eprintf("Incorrectly formatted coordinate %s.\n", argv[marg]);
	    exit(1);
	}
	marg++;
	if (marg >= argc) {
	    eprintf("Mismatched position and insert arguments.\n");
	    exit(1);
	}
	ninsert = argv[marg];
	insert = ip_read_image(ninsert, IM_BYTE, IM_SHORT, IM_LONG,
		       IM_UBYTE, IM_USHORT, IM_ULONG,
		       IM_FLOAT, IM_DOUBLE, IM_COMPLEX, IM_DCOMPLEX,
		       IM_PALETTE, IM_RGB, IM_RGB16, -1);

	if (insert->type != im->type) {
	    wprintf("Image types differ. Skipping image %s\n", argv[marg]);
	}else{
	    do_insert(im, insert, origin, fit);
	}

	marg++;
    }


    /* And save it. */

    switch (otype) {
    case TIFF:
	sprintf(descstr, "Image %s inserted into %s [origin=(%d,%d)].",
		ninsert, nin, origin.x, origin.y);
	write_tiff_imagex(nout, im, FLG_TAGLIST,
			  TAG_SOFTWARE, progstr,
			  TAG_DESCRIPTION, descstr,
			  TAG_DATE,
			  nocomp ? TAG_NOCOMP : TAG_DONE,
			  TAG_DONE);
	break;
    case PNG:
	write_png_image(nout, im);
	break;
    }

    return 0;
}
