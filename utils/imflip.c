/*
   Programme: imflip
   Author:    M.J.Cree

   Copyright (C) 2001, 2019 by Michael J. Cree.

DESCRIPTION:

   Flip an image or rotate an image by 90deg.

*/

#include <stdlib.h>
#include <string.h>

#include <ip/ip.h>
#include <ip/file.h>
#include <ip/tiff.h>
#include <ip/png.h>

#include "helpers.h"

#define PROGRAMME "imflip"
#define VERSION "1.02"
#define DATE "1/3/2019"

/* Program command line parsing and opt data structures */

static char progstr[] = PROGRAMME " (V" VERSION ")";

struct progenv prog = {
   NULL,
   PROGRAMME,
   VERSION,
   DATE
};

struct op help[] = {
   {OPT_SWITCH,	"flipx",NULL,       "Flip image horizontally"},
   {OPT_SWITCH,	"flipy",NULL,       "Flip image vertically"},
   {OPT_SWITCH,	"rot90",NULL,       "Rotate 90 degrees (anticlockwise)"},
   {OPT_SWITCH,	"rotm90",NULL,      "Rotate 90 degrees (clockwise)"},
   {OPT_SWITCH,	"tiff",	NULL,       "Output image as TIFF. [Def = png]"},
   {OPT_SWITCH,	"png",	NULL,       "Output image as PNG. [Def = png]"},
   {OPT_SWITCH, "nc",   NULL,       "Do not compress output image."},
   {OPT_STRING | OPT_LONELY | OPT_NIN, NULL, "imagein",
				      "Input image filename. [Def = stdin]" },
   {OPT_STRING | OPT_LONELY,	       NULL, "imageout",
				      "Output (inverted) image filename."
				      " [Def = stdout]" },
   {OPT_DONE, NULL, NULL,
      "Flip and/or rotate images.  Note that the flips are always done"
      " before the rotation.\n"}
};

enum {
   O_FLIPX, O_FLIPY,
   O_ROT90, O_ROTM90,
   O_TIFF, O_PNG, O_NOCOMP,
   O_IMAGEIN,   O_IMAGEOUT
};


static void bombout(ip_error_logstack *ipr, int error, const char *extra, va_list ap)
{
    ip_print_error(PROGRAMME, ipr, error, extra, ap);
    exit(1);
}


int main(int argc, char *argv[])
{
   const char *nin,*nout;
   ip_image *im, *tmp;
   optlist *ol;
   int nocomp;
   enum {TIFF, PNG} otype;
   int flipx, flipy, rot90, rotm90;
   char desc[250];
   char delim[2];

   /* Parse arguments */

   nin = nout = NULL;

   register_evi(ip_version());

   prog.output = stdout;
   ol = parseargs(argc, argv, &prog, help);

   getarg(ol, O_FLIPX, &flipx);
   getarg(ol, O_FLIPY, &flipy);
   getarg(ol, O_ROT90, &rot90);
   getarg(ol, O_ROTM90, &rotm90);

   if (rot90 && rotm90) {
      eprintf("What is use of doing both an anticlockwise and clockwise rotate?\n");
      exit(1);
   }

   otype = PNG;
   if (getarg(ol, O_TIFF, NULL)) otype = TIFF;
   if (getarg(ol, O_PNG, NULL)) otype = PNG;
   getarg(ol, O_NOCOMP, &nocomp);

   getarg(ol, O_IMAGEIN, &nin);
   getarg(ol, O_IMAGEOUT, &nout);

   freeargs(ol);

   ip_register_error_handler(bombout);

   /* Read in image */

   if (nin && STREQUAL(nin,"-")) nin = NULL;
   if (nout && STREQUAL(nout,"-")) nout = NULL;

   im = ip_read_image(nin, IM_BINARY, IM_BYTE, IM_SHORT, IM_LONG,
		           IM_UBYTE, IM_USHORT, IM_ULONG,
		           IM_FLOAT, IM_DOUBLE, IM_COMPLEX, IM_DCOMPLEX,
		           IM_PALETTE, IM_RGB, IM_RGB16,  -1);

   sprintf(desc, "Image %s with following operations: ", nin);
   delim[1] = 0;
   delim[0] = '[';

   /* Do flips */

   if (flipx) {
      im_flip(im, FLG_HORIZ);
      strcat(desc, delim);
      strcat(desc, "flip=x");
      delim[0] = ',';
   }

   if (flipy) {
      im_flip(im, FLG_VERT);
      strcat(desc, delim);
      strcat(desc, "flip=y");
      delim[0] = ',';
   }

   /* Do rotations */

   if (rot90) {
      tmp = im_rot90(NULL, im, 90);
      ip_free_image(im);
      im = tmp;
      strcat(desc, delim);
      strcat(desc, "rot=90");
      delim[0] = ',';
   }

   if (rotm90) {
      tmp = im_rot90(NULL, im, -90);
      ip_free_image(im);
      im = tmp;
      strcat(desc, delim);
      strcat(desc, "rot=-90");
      delim[0] = ',';
   }

   strcat(desc,"]");

   /* Write out result */

   switch(otype) {
   case TIFF:
      write_tiff_imagex(nout, im, FLG_TAGLIST,
			TAG_SOFTWARE, progstr,
			TAG_DESCRIPTION, desc,
			TAG_DATE,
                        nocomp ? TAG_NOCOMP : TAG_DONE,
			TAG_DONE);
      break;
   case PNG:
      write_png_image(nout, im);
      break;
   }

   return 0;
}
