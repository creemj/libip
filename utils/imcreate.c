/*
  Programme: imcreate
  Author:    M.J.Cree

  Copyright (C) 1996-1998, 2000, 2015 by Michael J. Cree.

  Create a test image.
*/


#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <ip/ip.h>
#include <ip/file.h>
#include <ip/raw.h>
#include <ip/tiff.h>
#include <ip/png.h>

#include "helpers.h"


#define PROGRAMME "imcreate"
#define VERSION "2.00"
#define DATE "26/10/2015"



/* Program command line parsing and opt data structures */

static char progstr[] = PROGRAMME " (V" VERSION ")";


struct progenv prog = {
    NULL,
    PROGRAMME,
    VERSION,
    DATE
};

struct op help[] = {
    {OPT_SWITCH,    "rub",   NULL, "ubyte (unsigned 8-bit) image [default]." },
    {OPT_SWITCH,    "rb",    NULL, "byte (signed 8-bit) image." },
    {OPT_SWITCH,    "rus",   NULL, "ushort int (unsigned 16-bit) image." },
    {OPT_SWITCH,    "rs",    NULL, "short int (signed 16-bit) image." },
    {OPT_SWITCH,    "rul",   NULL, "ulong int (unsigned 32-bit) image." },
    {OPT_SWITCH,    "rl",    NULL, "long int (signed 32-bit) image." },
    {OPT_SWITCH,    "rf",    NULL, "float (IEEE single precision) image." },
    {OPT_SWITCH,    "rd",    NULL, "double (IEEE double precision) image." },
    {OPT_SWITCH,    "rc",    NULL, "Complex float image." },
    {OPT_SWITCH,	  "const", NULL, "Constant image [default image function]." },
    {OPT_SWITCH,	  "hwedge",NULL, "Horizontal wedge." },
    {OPT_SWITCH,	  "vwedge",NULL, "Vertical wedge." },
    {OPT_SWITCH,	  "cone",  NULL, "Cone centred on origin" },
    {OPT_SWITCH,	  "gaussian", NULL, "Two-dimensional Gaussian function" },
    {OPT_SWITCH,	  "wdwcos", NULL, "Cosine window." },
    {OPT_SWITCH,	  "wdwbutw", NULL, "First order Butterworth window." },
    {OPT_COORD,	  "o", "origin", "Origin of image [depends on image type]." },
    {OPT_DOUBLE,	  "c", "scale",  "Scale to multiply each pixel by [1.0]." },
    {OPT_DOUBLE,	  "f", "offset", "Offset added to each pixel [0.0]." },
    {OPT_DOUBLE,	  "p", "spread", "`Spread' of function. [1.0]" },
    {OPT_SWITCH,	  "trans", NULL, "Transpose quadrants for Fourier space."},
    {OPT_SWITCH,	  "clip",  NULL, "Clip values to image type range."},
    {OPT_SWITCH,	  "raw",   NULL, "Save image as RAW image [PNG image]."},
    {OPT_SWITCH,	  "tiff",  NULL, "Save image as TIFF image [PNG image]."},
    {OPT_SWITCH,   "nc",    NULL, "Do not compress output image."},
    {OPT_COORD | OPT_REQ,	"s", "size", "Image size." },
    {OPT_STRING | OPT_LONELY,NULL, "image", "Output image filename. [stdout]" },
    {OPT_DONE, NULL, NULL,
     "Creates an image. "
     "Use the options to specify the image data type, size and test"
     " function h(x), where x is pixel position. "
     "The value put into a pixel is c*h((x-o)/p)+f, where c, o, p and f are"
     " the optional parameters listed above."
    }
};

enum options {
    O_UBYTE, O_BYTE, O_USHORT, O_SHORT, O_ULONG, O_LONG, O_FLOAT, O_DOUBLE, O_COMPLEX,
    O_CONST, O_HWEDGE, O_VWEDGE, O_CONE, O_GAUSSIAN, O_WDWCOS, O_WDWBUTW1,
    O_ORIGIN, O_SCALE, O_OFFSET, O_SPREAD,
    O_TRANSPOSE, O_CLIP,
    O_RAW, O_TIFF, O_NOCOMP,
    O_SIZE, O_IMAGE
};


static void bombout(ip_error_logstack *ipr, int error, const char *e, va_list ap)
{
    ip_print_error(PROGRAMME,ipr,error,e,ap);
    exit(1);
}



int main(int argc, char *argv[])
{
    ip_image *im, *tmp;
    ip_coord2d size;
    ip_coord2d origin;
    double scale,offset,spread;
    int type,test,imsave,nocomp;
    char *fname;
    char desc[256];
    optlist *ol;

    register_evi(ip_version());

    prog.output = stdout;
    ip_register_error_handler(bombout);

    ol = parseargs(argc,argv,&prog,help);

    type = IM_UBYTE;
    if (getarg(ol,O_BYTE, NULL))
	type = IM_BYTE;
    if (getarg(ol,O_USHORT, NULL))
	type = IM_USHORT;
    if (getarg(ol,O_SHORT, NULL))
	type = IM_SHORT;
    if (getarg(ol,O_ULONG, NULL))
	type = IM_ULONG;
    if (getarg(ol,O_LONG, NULL))
	type = IM_LONG;
    if (getarg(ol,O_FLOAT, NULL))
	type = IM_FLOAT;
    if (getarg(ol,O_DOUBLE, NULL))
	type = IM_DOUBLE;
    if (getarg(ol,O_COMPLEX, NULL))
	type = IM_COMPLEX;

    fname = NULL;
    getarg(ol, O_IMAGE, &fname);
    getarg(ol, O_SIZE, &size);

    test = TI_CONST;
    strcpy(desc,"Constant image");
    origin.x = origin.y = 0;
    if (getarg(ol,O_CONST, NULL)) {
	test = TI_CONST;
    }
    if (getarg(ol,O_HWEDGE, NULL)){
	test = TI_HWEDGE;
	origin.x = origin.y = 0;
	strcpy(desc,"Horizontal wedge");
    }
    if (getarg(ol,O_VWEDGE, NULL)) {
	test = TI_VWEDGE;
	origin.x = origin.y = 0;
	strcpy(desc,"Vertical wedge");
    }
    if (getarg(ol,O_CONE, NULL)) {
	test = TI_CONE;
	origin.x = size.x / 2;
	origin.y = size.y / 2;
	strcpy(desc,"Cone image");
    }
    if (getarg(ol,O_GAUSSIAN, NULL)) {
	test = TI_GAUSSIAN;
	origin.x = size.x / 2;
	origin.y = size.y / 2;
	strcpy(desc,"Two-dimensional Gaussian function");
    }
    if (getarg(ol,O_WDWCOS, NULL)) {
	test = TI_WDWCOSINE;
	origin.x = size.x / 2;
	origin.y = size.y / 2;
	strcpy(desc,"Cosine window");
    }
    if (getarg(ol,O_WDWBUTW1, NULL)) {
	test = TI_WDWBUTTW1;
	origin.x = size.x / 2;
	origin.y = size.y / 2;
	strcpy(desc,"1st order Butterworth window");
    }

    if (getarg(ol,O_TRANSPOSE, NULL)) {
	test |= FLG_TRANSPOSE;
	strcat(desc," [transposed]");
    }
    if (getarg(ol,O_CLIP, NULL)) {
	test |= FLG_CLIP;
	strcat(desc," [clipped]");
    }

    scale = 1.0;
    offset = 0.0;
    spread = 1.0;

    getarg(ol,O_ORIGIN, &origin);
    getarg(ol,O_SCALE, &scale );
    getarg(ol,O_OFFSET, &offset );
    getarg(ol,O_SPREAD, &spread );

    snprintf(desc+strlen(desc), 256-strlen(desc),
	     ": origin=(%d,%d)  scale=%g  offset=%g  spread=%g",
	     origin.x, origin.y, scale, offset, spread);

    imsave = 0;
    if (getarg(ol,O_RAW, NULL))
	imsave = O_RAW;
    if (getarg(ol,O_TIFF, NULL)) {
	if (imsave == O_RAW) {
	    eprintf("Options '-tiff' and '-raw' are mutually exclusive.\n");
	    exit(1);
	}
	imsave = O_TIFF;
    }
    getarg(ol,O_NOCOMP,&nocomp);

    freeargs(ol);

    im = ip_alloc_image(type == IM_COMPLEX ? IM_FLOAT : type, size);
    im_create(im, origin, scale, offset, spread, test);

    if (type == IM_COMPLEX) {
	tmp = im_convert(im, IM_COMPLEX, FLG_REAL);
	ip_free_image(im);
	im = tmp;
    }

    if (fname && STREQUAL(fname,"-")) fname = NULL;

    switch (imsave) {
    case O_RAW:
	write_raw_image(fname,im);
	break;
    case O_TIFF:
	write_tiff_imagex(fname,im,FLG_TAGLIST,
			  TAG_SOFTWARE, progstr,
			  TAG_DESCRIPTION, desc,
			  TAG_DATE,
			  nocomp ? TAG_NOCOMP : TAG_DONE,
			  TAG_DONE);
	break;
    default:
	write_png_image(fname,im);
	break;
    }

    ip_free_image(im);

    return 0;
}
