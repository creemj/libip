/*
   Programme: imcmp
   Author: M.J.Cree

   Copyright (C) 1997, 2000, 2002, 2015, 2016 by Michael J. Cree

   Compares the image data of two images together.

*/


#include <stdlib.h>
#include <string.h>
#include <math.h>

#include <ip/ip.h>
#include <ip/stats.h>
#include <ip/file.h>
#include <ip/png.h>
#include <ip/tiff.h>

#include "helpers.h"


#define PROGRAMME "imcmp"
#define VERSION "2.01"
#define DATE "17/7/2016"

/* Program command line parsing and opt data structures */

struct progenv prog = {
   NULL,
   PROGRAMME,
   VERSION,
   DATE
};


struct op help[] = {
   {OPT_SWITCH, "a", NULL, "Do thorough analysis of image differences"},
   {OPT_COORD, "ro", "roi_origin", "Region of interest origin."},
   {OPT_COORD, "rs", "roi_size", "Region of interest size."},
   {OPT_STRING | OPT_LONELY | OPT_REQ, NULL, "image1","Input image 1" },
   {OPT_STRING | OPT_LONELY,           NULL, "image2","Input image 2" },
   {OPT_DONE, NULL, NULL,
      "Compare the pixel data of two images together.\n"}
};


enum {
   O_ANALYSE,
   O_ORIGIN, O_SIZE,
   O_IMAGE1, O_IMAGE2
};


static void do_deep_analysis(ip_image *, ip_image *,const char *,const char *,ip_roi *);
#if 0
static double im_crosscorr(ip_image *im1, ip_image *im2, ip_roi *roi);
#endif

static void bombout(ip_error_logstack *ipr, int error, const char *extra, va_list ap)
{
   ip_print_error(PROGRAMME,ipr,error,extra,ap);
   exit(2);
}


static int image_type_impro(ip_image *im)
{
   return (image_type_integer(im) || image_type_real(im));
}


int main(int argc, char *argv[])
{
   const char *nim1, *nim2;
   ip_image *im1, *im2;
   int docmp;
   int analyse;
   int numdiff;
   ip_coord2d badpos;
   ip_coord2d origin, size;
   int doroi;
   double badval1, badval2;
   ip_lrgb badclr1, badclr2;
   ip_dcomplex badz1, badz2;
   optlist *ol;

   /* To shut the effing compiler up */
   badval1 = badval2 = 0.0;
   badclr1 = badclr2 = (ip_lrgb){0,0,0};
   badz1 = badz2 = (ip_dcomplex){0.0, 0.0};

   register_evi(ip_version());

   prog.output = stdout;
   ol = parseargs(argc,argv,&prog,help);

   getarg(ol,O_ANALYSE,&analyse);

   nim2 = NULL;
   getarg(ol,O_IMAGE1,&nim1);
   getarg(ol,O_IMAGE2,&nim2);

   origin.x = origin.y = 0;
   doroi = FALSE;
   if (getarg(ol, O_ORIGIN, &origin)) {
      if (origin.x < 0 || origin.y < 0) {
	 eprintf("Origin coordinates must be within image bounds.\n");
	 exit(1);
      }
      doroi = TRUE;
   }

   if (getarg(ol, O_SIZE, &size)) {
      if (size.x <= 0 || size.y <= 0) {
	 eprintf("Size can't have negative or zero components.\n");
	 exit(1);
      }
      doroi = TRUE;
   }else{
      size.x = size.y = 0;
   }

   freeargs(ol);

   ip_register_error_handler(bombout);

   if (nim1 && STREQUAL(nim1,"-")) nim1 = NULL;
   if (nim2 && STREQUAL(nim2,"-")) nim2 = NULL;

   im1 = ip_read_image(nim1, IM_BINARY, IM_BYTE, IM_UBYTE,
			     IM_USHORT, IM_SHORT, IM_ULONG, IM_LONG, IM_FLOAT,
		             IM_DOUBLE, IM_COMPLEX, IM_DCOMPLEX,
		             IM_PALETTE, IM_RGB, IM_RGB16, -1);

   im2 = ip_read_image(nim2, IM_BINARY, IM_BYTE, IM_UBYTE,
			     IM_USHORT, IM_SHORT, IM_ULONG, IM_LONG, IM_FLOAT,
		             IM_DOUBLE, IM_COMPLEX, IM_DCOMPLEX,
			     IM_PALETTE, IM_RGB, IM_RGB16, -1);
   if (nim2 == NULL)
      nim2 = ip_msg_stdin;

   docmp = TRUE;

   if (im1->size.x != im2->size.x || im1->size.y != im2->size.y) {
      mprintf("Images not same size.\n");
      docmp = FALSE;
   }

   if (im1->type != im2->type) {
      mprintf("Image types disagree (%s and %s).\n",
	      ip_type_name(im1->type), ip_type_name(im2->type));
      if (im1->type == IM_BINARY || im2->type == IM_BINARY)
	 docmp = FALSE;
      else if ((image_type_impro(im1) && image_type_clrim(im2))
	       || (image_type_clrim(im1) && image_type_impro(im2)))
	 docmp = FALSE;
      else if ((type_complex(im1->type) && !type_complex(im2->type))
	       || (!type_complex(im1->type) && type_complex(im2->type)))
	 docmp = FALSE;
   }

   if (NOT docmp) {
      mprintf("Image types incompatible - can't compare image data.\n");
      exit(1);
   }

   if (doroi) {
      ip_image *tmp;

      if (origin.x >= size.x || origin.y >= size.y) {
	 eprintf("ROI origin must be within image bounds.\n");
	 exit(1);
      }

      if (size.x == 0) {
	 size = im1->size;
	 size.x -= origin.x;
	 size.y -= origin.y;
      }

      if (origin.x + size.x > im1->size.x || origin.y + size.y > im1->size.y) {
	 eprintf("ROI not completely within image bounds.\n");
	 exit(1);
      }

      tmp = im_cut(im1, origin, size);
      ip_free_image(im1);
      im1 = tmp;

      tmp = im_cut(im2, origin, size);
      ip_free_image(im2);
      im2 = tmp;
   }

   numdiff = 0; docmp = TRUE;
   if (image_type_impro(im1)) {
      double val1, val2;

      for (int j=0; j<im1->size.y; j++) {
	 for (int i=0; i<im1->size.x; i++) {
	    switch (im1->type) {
	     case IM_UBYTE:
	       val1 = IM_PIXEL(im1, i, j, ub);
	       break;
	     case IM_BYTE:
	       val1 = IM_PIXEL(im1, i, j, b);
	       break;
	     case IM_USHORT:
	       val1 = IM_PIXEL(im1, i, j, us);
	       break;
	     case IM_SHORT:
	       val1 = IM_PIXEL(im1, i, j, s);
	       break;
	     case IM_LONG:
	       val1 = IM_PIXEL(im1, i, j, l);
	       break;
	     case IM_ULONG:
	       val1 = IM_PIXEL(im1, i, j, ul);
	       break;
	     case IM_FLOAT:
	       val1 = IM_PIXEL(im1, i, j, f);
	       break;
	     case IM_DOUBLE:
	       val1 = IM_PIXEL(im1, i, j, d);
	       break;
	    default:
		val1 = 0.0;
		break;
	    }
	    switch (im2->type) {
	     case IM_UBYTE:
	       val2 = IM_PIXEL(im2, i, j, ub);
	       break;
	     case IM_BYTE:
	       val2 = IM_PIXEL(im2, i, j, b);
	       break;
	     case IM_USHORT:
	       val2 = IM_PIXEL(im2, i, j, us);
	       break;
	     case IM_SHORT:
	       val2 = IM_PIXEL(im2, i, j, s);
	       break;
	     case IM_LONG:
	       val2 = IM_PIXEL(im2, i, j, l);
	       break;
	     case IM_ULONG:
	       val2 = IM_PIXEL(im2, i, j, ul);
	       break;
	     case IM_FLOAT:
	       val2 = IM_PIXEL(im2, i, j, f);
	       break;
	     case IM_DOUBLE:
	       val2 = IM_PIXEL(im2, i, j, d);
	       break;
	    default:
		val2 = 1.0;
		break;
	    }
	    if (val1 != val2) {
	       numdiff ++;
	       if (docmp) {
		  badpos.x = i;
		  badpos.y = j;
		  badval1 = val1;
		  badval2 = val2;
		  docmp = FALSE;
	       }
	    }
	 }
      }
   }else if (im1->type == IM_BINARY) {
      for (int j=0; j<im1->size.y; j++) {
	 for (int i=0; i<im1->size.x; i++) {
	    if (IM_PIXEL(im1, i, j, b) != IM_PIXEL(im2, i, j, b)) {
	       numdiff++;
	       if (docmp) {
		  badpos.x = i;
		  badpos.y = j;
		  badval1 = (double)(IM_PIXEL(im1, i, j, b) != 0);
		  badval2 = (double)(IM_PIXEL(im2, i, j, b) != 0);
		  docmp = FALSE;
	       }
	    }
	 }
      }
   }else if (type_clrim(im1->type)) {
      uint8_t idx;
      ip_lrgb val1, val2;

      for (int j=0; j<im1->size.y; j++) {
	 for (int i=0; i<im1->size.x; i++) {
	    switch (im1->type) {
	     case IM_PALETTE:
	       idx = IM_PIXEL(im1, i, j, b);
	       val1.r = im1->palette[idx].r;
	       val1.g = im1->palette[idx].g;
	       val1.b = im1->palette[idx].b;
	       break;
	     case IM_RGB:
	       val1.r = IM_PIXEL(im1, i, j, r).r;
	       val1.b = IM_PIXEL(im1, i, j, r).b;
	       val1.g = IM_PIXEL(im1, i, j, r).g;
	       break;
	     case IM_RGB16:
	       val1 = IM_PIXEL(im1, i, j, r16);
	       break;
	    default:
		val1 = (ip_lrgb){0,0,0};
		break;
	    }
	    switch (im2->type) {
	     case IM_PALETTE:
	       idx = IM_PIXEL(im2, i, j, b);
	       val2.r = im2->palette[idx].r;
	       val2.g = im2->palette[idx].g;
	       val2.b = im2->palette[idx].b;
	       break;
	     case IM_RGB:
	       val2.r = IM_PIXEL(im2, i, j, r).r;
	       val2.b = IM_PIXEL(im2, i, j, r).b;
	       val2.g = IM_PIXEL(im2, i, j, r).g;
	       break;
	     case IM_RGB16:
	       val2 = IM_PIXEL(im2, i, j, r16);
	       break;
	    default:
		val2 = (ip_lrgb){1,2,2};
		break;
	    }
	    if (val1.r != val2.r || val1.g != val2.g || val1.b != val2.b) {
	       numdiff ++;
	       if (docmp) {
		  badpos.x = i;
		  badpos.y = j;
		  badclr1 = val1;
		  badclr2 = val2;
		  docmp = FALSE;
	       }
	    }
	 }
      }
   }else if (type_complex(im1->type)) {
       for (int j=0; j<im1->size.y; j++) {
	   for (int i=0; i<im1->size.x; i++) {
	       ip_dcomplex val1, val2;
	       switch (im1->type) {
	       case IM_COMPLEX:
		   val1.r = (double)IM_PIXEL(im1, i, j, c).r;
		   val1.i = (double)IM_PIXEL(im1, i, j, c).i;
		   break;
	       case IM_DCOMPLEX:
		   val1 = IM_PIXEL(im1, i, j, dc);
		   break;
	       default:
		   val1 = (ip_dcomplex){1.0, -1.0};
		   break;
	       }
	       switch (im2->type) {
	       case IM_COMPLEX:
		   val2.r = (double)IM_PIXEL(im2, i, j, c).r;
		   val2.i = (double)IM_PIXEL(im2, i, j, c).i;
		   break;
	       case IM_DCOMPLEX:
		   val2 = IM_PIXEL(im2, i, j, dc);
		   break;
	       default:
		   val2 = (ip_dcomplex){100.0, 0.0};
		   break;
	       }
	       if (val1.r != val2.r || val1.i != val2.i) {
		   numdiff++;
		   if (docmp) {
		       badpos.x = i;
		       badpos.y = j;
		       badz1 = val1;
		       badz2 = val2;
		       docmp = FALSE;
		   }
	       }
	   }
       }
   }else{
      eprintf("Sorry - havn't implemented comparision of those type images.\n");
      exit(2);
   }

   if (docmp) {
      mprintf("Image data are the same.\n");
   }else{
      if (doroi) {
	 badpos.x += origin.x;
	 badpos.y += origin.y;
      }
      if (image_type_clrim(im1)) {
	 mprintf("Image value differs: (%d,%d) (%d,%d,%d) vrs (%d,%d,%d)%\n",
		 badpos.x, badpos.y,
		 badclr1.r, badclr1.g, badclr1.b,
		 badclr2.r, badclr2.g, badclr2.b);
	 mprintf("Number of differing pixels: %d (out of %d)\n",
		 numdiff, im1->size.x*im1->size.y);
      }else if (type_complex(im1->type)) {
	 mprintf("Image value differs: (%d,%d) (%g+i%g) vrs (%g+i%g)%\n",
		 badpos.x, badpos.y, badz1.r, badz1.i, badz2.r, badz2.i);
	 mprintf("Number of differing pixels: %d (out of %d)\n",
		 numdiff, im1->size.x*im1->size.y);
      }else{
	 mprintf("Image value differs: (%d,%d) %g vrs %g\n",
		 badpos.x, badpos.y, badval1, badval2);
	 mprintf("Number of differing pixels: %d (out of %d)\n",
		 numdiff, im1->size.x*im1->size.y);
	 if (image_type_impro(im1) && analyse) {
	    do_deep_analysis(im1, im2, nim1, nim2, NULL);
	 }
      }
      ip_free_image(im1);
      ip_free_image(im2);

      exit(1);
   }

   ip_free_image(im1);
   ip_free_image(im2);

   return 0;
}





#if 1

static void do_deep_analysis(ip_image *im1, ip_image *im2,
			     const char *fn1, const char *fn2, ip_roi *roi)
{
   return;
}

#else
static void do_deep_analysis(ip_image *im1, ip_image *im2,
			     const char *fn1, const char *fn2, ip_roi *roi)
{
   ip_image *tim1, *tim2;
   int type;
   double crosscorr;
   ip_stats *st;
   int32_t laplacian_l[9] = {
      0,  -1,  0,
      -1,  4, -1,
      0,  -1, 0
   };
   double laplacian_d[9] = {
      0,  -1,  0,
      -1,  4, -1,
      0,  -1, 0
   };

   type = IM_SHORT;
   if (im1->type == IM_LONG || im2->type == IM_LONG)
      type = IM_LONG;
   if (im1->type == IM_FLOAT || im2->type == IM_FLOAT)
      type = IM_FLOAT;
   if (im1->type == IM_DOUBLE || im2->type == IM_DOUBLE)
      type = IM_DOUBLE;

   if (im1->type != type)
      tim1 = im_convert(im1, type, NO_FLAG);
   else
      tim1 = im_copy(im1);
   if (im2->type != type)
      tim2 = im_convert(im2, type, NO_FLAG);
   else
      tim2 = im_copy(im2);

   st = alloc_stats(NO_FLAG);
   im_extrema(tim1, st, roi);
   if (st->min == st->max) {
      mprintf("Image [%s] is single valued with value %g.\n",fn1,st->min);
   }
   im_extrema(tim2, st, roi);
   if (st->min == st->max) {
      mprintf("Image [%s] is single valued with value %g.\n",fn2,st->min);
   }
   im_sub(tim1, tim2, NO_FLAG);
   im_stats(tim1, st, roi);
   mprintf("\nImages differ by:\n"
                "  Min error = %g,  Max error = %g\n"
                "  Mean error = %g,  RMS error = %g\n",
                st->min, st->max, st->mean, st->rmsmean);

#if 0
   if (sdiff) {
      if (roi)
	 im_roiset(tim1, roi, 0, NO_FLAG);
      write_tiff_image();
   }
#endif

   free_stats(st);
   free_image(tim1);

   if (im1->type != type)
      tim1 = im_convert(im1, type, NO_FLAG);
   else
      tim1 = im_copy(im1);

   crosscorr = im_crosscorr(tim1,tim2,roi);

   im_convolve(tim1, type_integer(type) ?
	             (void *)laplacian_l : (void *)laplacian_d, 3, NO_FLAG);
   im_convolve(tim2, type_integer(type) ?
	             (void *)laplacian_l : (void *)laplacian_d, 3, NO_FLAG);

   mprintf("  Cross-corr. = %g,  Laplacian cross-corr. = %g.\n\n",
                   crosscorr, im_crosscorr(tim1,tim2,roi));

   free_image(tim1);
   free_image(tim2);
}


static double im_crosscorr(ip_image *im1, ip_image *im2, ip_roi *roi)
{
   double s, t, st, ss, tt;
   double size;
   int j;
   int ownroi;
   ip_coord2d topl, botr;

   ownroi = FALSE;
   if (roi == NULL) {
      roi = alloc_roi(ROI_IMAGE,im1);
      ownroi = TRUE;
   }

   s = t = st = ss = tt = 0;
   topl = roi_get(ROI_ORIGIN,roi).pt;
   botr = roi_get(ROI_BOTRIGHT,roi).pt;

   switch (im1->type) {
    case IM_SHORT:
      for (j=topl.y; j<=botr.y; j++) {
         int i;
         int16_t *sptr, *tptr;
	 double a,b;

         sptr = IM_ROWADR(im1, j, s);
         tptr = IM_ROWADR(im2, j, s);
         for (i=topl.x; i<=botr.x; i++) {
	    a = (double)*sptr++;
	    b = (double)*tptr++;
            s += a;
            t += b;
            st += a*b;
            ss += a*a;
            tt += b*b;
         }
      }
      break;

    case IM_LONG:
      for (j=topl.y; j<=botr.y; j++) {
         int i;
         int32_t *sptr, *tptr;
	 double a,b;

         sptr = IM_ROWADR(im1, j, l);
         tptr = IM_ROWADR(im2, j, l);
         for (i=topl.x; i<=botr.x; i++) {
	    a = (double)*sptr++;
	    b = (double)*tptr++;
            s += a;
            t += b;
            st += a*b;
            ss += a*a;
            tt += b*b;
         }
      }
      break;

    case IM_FLOAT:
      for (j=topl.y; j<=botr.y; j++) {
         int i;
         float *sptr, *tptr;
	 double a,b;

         sptr = IM_ROWADR(im1, j, f);
         tptr = IM_ROWADR(im2, j, f);
         for (i=topl.x; i<=botr.x; i++) {
	    a = (double)*sptr++;
	    b = (double)*tptr++;
            s += a;
            t += b;
            st += a*b;
            ss += a*a;
            tt += b*b;
         }
      }
      break;

    case IM_DOUBLE:
      for (j=topl.y; j<=botr.y; j++) {
         int i;
         double *sptr, *tptr;
	 double a,b;

         sptr = IM_ROWADR(im1, j, d);
         tptr = IM_ROWADR(im2, j, d);
         for (i=topl.x; i<=botr.x; i++) {
	    a = (double)*sptr++;
	    b = (double)*tptr++;
            s += a;
            t += b;
            st += a*b;
            ss += a*a;
            tt += b*b;
         }
      }
      break;
   default:
       break;
   }

   topl = roi_get(ROI_SIZE,roi).pt;
   size = (double)topl.x * topl.y;

   if (ownroi) free_roi(roi);

   return ( (st-s*t/size) / sqrt((ss-s*s/size)*(tt-t*t/size)) );
}
#endif
