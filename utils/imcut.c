/*
  Programme: imcut
  Author: M.J.Cree

  Copyright (C) 2000, 2002, 2016 by Michael J. Cree.

  Programme to cut pieces out of (i.e. crop) an image.
*/


#include <stdlib.h>
#include <string.h>

#include <ip/ip.h>
#include <ip/stats.h>
#include <ip/file.h>
#include <ip/tiff.h>
#include <ip/png.h>

#include "helpers.h"

#define PROGRAMME "imcut"
#define VERSION "1.03"
#define DATE "23/9/2016"

/* Program command line parsing and opt data structures */

static char progstr[] = PROGRAMME " (V" VERSION ")";

struct progenv prog = {
    NULL,
    PROGRAMME,
    VERSION,
    DATE
};


struct op help[] = {
    {OPT_COORD,          "o", "origin", "Origin of cut. [def = (0,0)]"},
    {OPT_COORD,          "s", "size",   "Size of cut."},
    {OPT_COORD,          "br","bottom-right", "Specify bottom-right corner."},
    {OPT_SWITCH,		"tiff", NULL,
     "Write result as TIFF file [def]"},
    {OPT_SWITCH,		"png", NULL,
     "Write result as PNG file"},
    {OPT_SWITCH,         "nc", NULL,    "Do not compress output image."},
    {OPT_STRING | OPT_LONELY | OPT_NIN, NULL, "imagein",
     "Input image filename [def = stdin]" },
    {OPT_STRING | OPT_LONELY, NULL,"imageout",
     "Output image filename [def = stdout]" },
    {OPT_DONE, NULL, NULL, NULL}
};


enum {
    O_ORIGIN, O_SIZE, O_BR,
    O_TIFF, O_PNG, O_NOCOMP,
    O_IMAGEIN, O_IMAGEOUT
};


static void bombout(ip_error_logstack *ipr, int error, const char *extra, va_list ap)
{
    ip_print_error(PROGRAMME,ipr,error,extra,ap);
    exit(1);
}


int main(int argc, char *argv[])
{
    optlist *ol;
    const char *nin,*nout;
    ip_image *in, *out;
    enum {TIFF, PNG} otype;
    int nocomp;
    char descstr[256];
    ip_coord2d origin,size;

    register_evi(ip_version());

    prog.output = stdout;
    ol = parseargs(argc,argv,&prog,help);

    nin = nout = NULL;

    origin.x = origin.y = 0;
    getarg(ol,O_ORIGIN,&origin);
    if (getarg(ol,O_BR,&size)) {
	size.x = size.x - origin.x + 1;
	size.y = size.y - origin.y + 1;
	if (getarg(ol,O_SIZE,NULL)) {
	    eprintf("Can't specify both -s and -br options.\n");
	    exit(1);
	}
    }else{
	size.x = size.y = 0;
	getarg(ol,O_SIZE,&size);
    }

    if (origin.x < 0 || origin.y < 0) {
	eprintf("Given origin (%d,%d) contains negative numbers.\n",
		origin.x,origin.y);
	exit(1);
    }

    if (size.x < 0 || size.y < 0) {
	eprintf("Given coordinates result in image size = (%d,%d)!\n",
		size.x,size.y);
	exit(1);
    }

    otype = TIFF;
    if (getarg(ol,O_PNG,NULL)) otype = PNG;
    if (getarg(ol,O_TIFF,NULL)) {
	if (otype == PNG) {
	    eprintf("Hmm, can't write an image as both TIFF and PNG!\n");
	    exit(1);
	}
	otype = TIFF;
    }

    getarg(ol,O_NOCOMP,&nocomp);

    getarg(ol,O_IMAGEIN,&nin);
    getarg(ol,O_IMAGEOUT,&nout);

    freeargs(ol);

    ip_register_error_handler(bombout);

    /* Load image to cut/crop */

    if (nin && STREQUAL(nin,"-")) nin = NULL;
    if (nout && STREQUAL(nout,"-")) nout = NULL;

    in = ip_read_image(nin, IM_TYPE_ANY, -1);

    if (nin == NULL) nin = ip_msg_stdin;

    if (origin.x >= in->size.x || origin.y >= in->size.y) {
	eprintf("Origin (%d,%d) outside image of size = (%d,%d)\n",
		origin.x,origin.y,in->size.x,in->size.y);
	exit(1);
    }

    if ((origin.x + size.x > in->size.x) || (origin.y + size.y > in->size.y)) {
	eprintf("Given origin and size extend outside image of size (%d,%d).\n",
		in->size.x,in->size.y);
	exit(1);
    }

    /* Do the dirty deed */

    out = im_cut(in, origin, size);

    /* And save it. */

    switch (otype) {
    case TIFF:
	sprintf(descstr, "Image cut from %s [origin=(%d,%d)].",
		nin, origin.x, origin.y);
	write_tiff_imagex(nout,out,FLG_TAGLIST,
			  TAG_SOFTWARE, progstr,
			  TAG_DESCRIPTION, descstr,
			  TAG_DATE,
			  nocomp ? TAG_NOCOMP : TAG_DONE,
			  TAG_DONE);
	break;
    case PNG:
	write_png_image(nout,out);
	break;
    }

    ip_free_image(out);
    ip_free_image(in);

    return 0;
}
