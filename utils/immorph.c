/*
   Programme: immorph
   Author:    M.J.Cree

   Copyright (C) 2000, 2019 by Michael J. Cree

DESCRIPTION:

   Morphological operations on grey-scale images.

*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>

#include <ip/ip.h>
#include <ip/morph.h>
#include <ip/file.h>
#include <ip/png.h>
#include <ip/tiff.h>

#include "helpers.h"

#define PROGRAMME "immorph"
#define VERSION "1.02"
#define DATE "3/1/2019"


/* Program command line parsing and opt data structures */

char progstr[256];

struct progenv prog = {
    NULL,
    PROGRAMME,
    VERSION,
    DATE
};


struct op help[] = {
    {OPT_SWITCH,     "line", NULL, "Line structuring element (p1=length, p2=angle)"},
    {OPT_SWITCH,     "square", NULL, "Square structuring element (p1=size) [default]"},
    {OPT_SWITCH,     "rect", NULL, "Rectangular structuring element (p1=xsize, p2=ysize)"},
    {OPT_SWITCH,     "disc", NULL, "Disc structuring element (p1=radius)"},
    {OPT_INT,        "p1", "parm1", "Mask size (parameter 1) to use [def = 3]"},
    {OPT_INT,        "p2", "parm2", "Mask size (parameter 2) to use [def dependent]"},
    {OPT_SWITCH,		"tiff", NULL,	"Write result as TIFF file"
     " [def = png]"},
    {OPT_STRING | OPT_LONELY | OPT_NIN, NULL, "image",
     "Input image filename."
     " [def = stdin]" },
    {OPT_STRING | OPT_LONELY, NULL,"imageout",
     "Output image filename."
     " [def = stdout]" },
    {OPT_DONE, NULL, NULL, NULL}
};


enum {
    O_SE_LINE, O_SE_SQUARE, O_SE_RECT, O_SE_DISC,
    O_PARM_1, O_PARM_2,
    O_TIFF,
    O_IMAGEIN, O_IMAGEOUT,
    O_DESC
};

char *imopen_desc = "Morphological opening on a grey-scale image.";
char *imclose_desc = "Morphological closing on a grey-scale image.";
char *imdilate_desc = "Morphological dilation on a grey-scale image.";
char *imerode_desc = "Morphological erosion on a grey-scale image.";

static void bombout(ip_error_logstack *ipr, int error, const char *extra, va_list ap)
{
    ip_print_error(prog.progname, ipr, error, extra, ap);
    exit(1);
}


int main(int argc, char *argv[])
{
    const char *nin, *nout;
    ip_image *im;
    ip_strel *se;
    int p1, p2;
    int tiff;
    optlist *ol;
    char desc[240], desc_se[120];
    int se_type = IP_STREL_SQUARE;
    enum {
	IMOPEN, IMCLOSE, IMDILATE, IMERODE
    } operation;

    /* Sort out which programme we are running */

    if ((prog.progname = strrchr(argv[0], '/')) == NULL)
	prog.progname = argv[0];
    else
	prog.progname++;
    prog.output = stdout;

    if (STREQUAL(prog.progname, "imopen")) {
	operation = IMOPEN;
	help[O_DESC].helpmsg = imopen_desc;
    }else if (STREQUAL(prog.progname, "imclose")) {
	operation = IMCLOSE;
	help[O_DESC].helpmsg = imclose_desc;
    }else if (STREQUAL(prog.progname, "imdilate")) {
	operation = IMDILATE;
	help[O_DESC].helpmsg = imdilate_desc;
    }else if (STREQUAL(prog.progname, "imerode")) {
	operation = IMERODE;
	help[O_DESC].helpmsg = imerode_desc;
    }else{
	eprintf("What is '%s'???    I'm confused...\n", prog.progname);
	eprintf("Please call me 'imopen,' 'imclose,' 'imdilate,' or 'imerode.'\n");
	exit(1);
    }

    sprintf(progstr,"%s (V%s)", prog.progname, prog.version);

    /* Parse command line */

    register_evi(ip_version());

    ol = parseargs(argc, argv, &prog, help);

    nin = nout = NULL;
    p1 = 3;

    if (getarg(ol, O_SE_LINE, NULL)) se_type = IP_STREL_LINE;
    if (getarg(ol, O_SE_SQUARE, NULL)) se_type = IP_STREL_SQUARE;
    if (getarg(ol, O_SE_RECT, NULL)) se_type = IP_STREL_RECTANGLE;
    if (getarg(ol, O_SE_DISC, NULL)) se_type = IP_STREL_DISC;

    getarg(ol, O_PARM_1, &p1);
    if (p1 < 2 || p1 > 10000) {
	eprintf("Ridulous parameter 1 (size) value given.\n");
	exit(1);
    }

    p2 = INT_MIN;
    getarg(ol, O_PARM_2, &p2);

    switch (se_type) {
    case IP_STREL_LINE:
	if (p2 == INT_MIN) {
	    eprintf("Must specify argument -p2 (angle) for line structuring element.\n");
	    exit(1);
	}
	if (NOT (p2 == 0 || p2 == 90 || p2 == 180 || p2 == 270)) {
	    eprintf("Line angle of %d not supported; use 0, 90, 180 or 270 deg only.\n",
		    p2);
	    exit(1);
	}
	sprintf(desc_se, "line structuring element; length = %d, angle = %d", p1, p2);
	break;
    case IP_STREL_RECTANGLE:
	if (p2 == INT_MIN) {
	    wprintf("Second parameter (-p2) not given; assuming same as -p1.\n", p1);
	    se_type = IP_STREL_SQUARE;
	    sprintf(desc_se, "square structuring element; size = %d", p1);
	    break;
	}
	if (p2 < 2 || p2 > 10000) {
	    eprintf("Ridulous parameter 2 (ysize) value given for rect. struct. elem.\n");
	    exit(1);
	}
	sprintf(desc_se, "rectangular structuring element; width = %d, height = %d", p1, p2);
	break;
    case IP_STREL_SQUARE:
	if (p2 != INT_MIN)
	    wprintf("Ignoring irrelevant -p2 argument of %d.\n", p2);
	sprintf(desc_se, "square structuring element; size = %d", p1);
	break;
    case IP_STREL_DISC:
	if (p2 != INT_MIN)
	    wprintf("Ignoring irrelevant -p2 argument of %d.\n", p2);
	sprintf(desc_se, "disc structuring element; radius = %d", p1);
	break;
    }

    getarg(ol, O_TIFF, &tiff);
    getarg(ol, O_IMAGEIN, &nin);
    getarg(ol, O_IMAGEOUT, &nout);

    freeargs(ol);

    ip_register_error_handler(bombout);

    /* Load images */

    if (nin && STREQUAL(nin,"-")) nin = NULL;
    im = ip_read_image(nin, IM_BINARY, IM_BYTE, IM_UBYTE, IM_SHORT, IM_USHORT,
		       IM_LONG, IM_ULONG, IM_FLOAT, IM_DOUBLE, -1);

    if (nin == NULL) nin = ip_msg_stdin;

    /* Get structuring element */
    se = ip_alloc_strel(se_type, p1, p2);

    /* Do required morphological operation (depending on programme name) */
    switch (operation) {
    case IMOPEN:
	im_open(im, se);
	snprintf(desc, 240, "%s opened, %s.", nin, desc_se);
	break;
    case IMCLOSE:
	im_close(im, se);
	snprintf(desc, 240, "%s closed, %s.", nin, desc_se);
	break;
    case IMDILATE:
	im_dilate(im, se);
	snprintf(desc, 240, "%s dilated, %s.", nin, desc_se);
	break;
    case IMERODE:
	im_erode(im, se);
	snprintf(desc, 240, "%s eroded, %s.", nin, desc_se);
	break;
    }

    /* Write out result */
    if (tiff)
	write_tiff_imagex(nout, im, FLG_TAGLIST,
			  TAG_SOFTWARE, progstr,
			  TAG_DESCRIPTION, desc,
			  TAG_DATE,
			  TAG_DONE);
    else
	write_png_image(nout, im);

    ip_free_strel(se);
    ip_free_image(im);

    return 0;
}
