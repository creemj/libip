/*
   Programme: imdisp
   Author:    M.J.Cree

   Copyright (C) 1996-1998, 2001, 2014, 2015 Michael J. Cree

DESCRIPTION:

   Displays image on display screen.

   Can load Raw, TIFF, PNG and JPEG images.

*/

#define _XOPEN_SOURCE
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <ctype.h>
#include <math.h>

#include <unistd.h>

#include <ip/ip.h>

#include <ip/file.h>
#include <ip/raw.h>
#include <ip/tiff.h>
#include <ip/png.h>
#include <ip/jpeg.h>

#include "helpers.h"

#define PROGRAMME "imdisp"
#define VERSION "2.0"
#define DATE "14/03/2014"

/* Program command line parsing and opt data structures */

struct progenv prog = {
    NULL,
    PROGRAMME,
    VERSION,
    DATE
};

char *imagetypes[] = {
    "Raw unsigned byte image.",
    "Raw unsigned short image.",
    "Raw signed short image.",
    "Raw signed long image.",
    "Raw float image.",
    "Raw double image.",
    "Raw complex image."
};

/* Must match "raw complex image." string above */
#define RAW_TYPE_COMPLEX 6

char *complexparts[] = {
    "Complex real part.",
    "Complex imaginary part.",
    "Complex magnitude.",
    "Complex angle (phase)."
};

struct op help[] = {
   {OPT_MULT | OPT_UNIQUE,"r", "buslfdc", imagetypes },
   {OPT_COORD,		"s", "size",    "Raw image size." },
   {OPT_INT,		"H","headerskip","Skip header at start of raw image."},
   {OPT_INT,		"R","rowskip",  "Skip rowskip bytes at end of each row."},
   {OPT_MULT | OPT_UNIQUE,"C", "rima",	complexparts },
   {OPT_SWITCH,		"a", NULL,	"Resize image to fit display"
					" (bit replication)." },
   {OPT_SWITCH,		"f", NULL,      "Fit image to display size"
					" (interpolation)." },
   {OPT_DOUBLE,		"g", "gamma",   "Gamma correct image. [def = 1.0]" },
   {OPT_DOUBLE,		"c", "contrast","Modify contrast. [def = 1.0]" },
   {OPT_SWITCH,		"hb", NULL,	"Hot-body (sepia) palette." },
   {OPT_SWITCH,		"go", NULL,	"Grey-object palette." },
   {OPT_SWITCH,		"log", NULL,	"log base 10 palette."},
   {OPT_INT,		"ut", "uthresh","Upper threshold." },
   {OPT_INT,		"lt", "lthresh","Lower threshold." },
   {OPT_DOUBLE,		"min", "min_pix_val", "Set pixel value corresponding to"
					" black [def = 0.0/-pi]"},
   {OPT_DOUBLE,		"max", "max_pix_val", "Set pixel value corresponding to"
					" white [def = 255.0/pi]"},
   {OPT_SWITCH,		"sc", NULL,    "min_pix_val and max_pix_val determined from image."},
   {OPT_STRING | OPT_LONELY, NULL, "image", "Image to display. ['-' = stdin]" },
   {OPT_MORE,		NULL, "...", NULL},
   {OPT_DONE, NULL, NULL,
      "Display RAW, TIFF, JPEG or PNG images. "
      "Supports BINARY, BYTE, SHORT, LONG, FLOAT, DOUBLE and COMPLEX"
      " image datatypes"
      " and has partial support for the PALETTE image datatype.\n"}
};


enum {
    O_RAWTYPE = 0, O_RAWSIZE, O_HEADER, O_ROWSKIP,
    O_COMPPART,
    O_RESIZE, O_FITIM,
    O_GAMMA, O_CONTRAST,
    O_HOTBODY, O_GREYOBJ, O_LOG10,
    O_UTHRESH, O_LTHRESH,
    O_MIN, O_MAX,
    O_SCALE,
    O_IMAGE,
    O_MORE
};


/* For use in palette global variable */

#define IDP_GREY    0x00
#define IDP_HOTBODY 0x01
#define IDP_GREYOBJ 0x02
#define IDP_LOG10   0x03
#define IDP_PALETTE 0x3f

#define IDP_LOW     0x40
#define IDP_HIGH    0x80


/* Display maximum dimensions. */

#define DISPMAX_X 2000
#define DISPMAX_Y 1440

/* Resize images if above this size when resize requested. */

#define DISPR_X   2000
#define DISPR_Y   1400


static void bombout(ip_error_logstack *ipr, int error, const char *extra, va_list ap);

static ip_image *load_image(const char *fname, int type, ip_coord2d size, int header, int rowskip);
static ip_image *resize_image(ip_image *);
static void init_display(ip_coord2d size, const char *extr);


#if 0
void fix_palette_im(ip_image *im);
void init_palette(ip_rgb *pal, int type, double contrast, int*,int*,int*,int*);
void init_palettes_and_pens(ip_image *im);
void save_tiff_image(const char *fname, ip_image *im, ip_rgb *pal);
void set_canvas_size(ip_coord2d size);
void alloc_overlays(void);
void disp_overlays(int num, char *name, int);
void disp_info(char *msg);
ip_image *image_fit(ip_image *im, ip_coord2d size);
#endif

void wait_for_quit(void);
int interpret_key(char c);

static ip_image *get_complex_part(ip_image *im, int cp);



/* Globals */

double imgamma;			/* Gamma correction */
double contrast;			/* Contrast adjustment */
int spalette;			/* Palette selected */
int scaling;			/* TRUE = scale image to fit black to white */
int fitim;			/* TRUE = Fit image to display size. */

/* And resizing of images */

ip_coord2d lastsize = {0,0};

/* Type conversion from command line to ip library. */

int convtype[7] = {
    IM_UBYTE, IM_USHORT, IM_SHORT, IM_LONG, IM_FLOAT, IM_DOUBLE, IM_COMPLEX,
};


int main(int argc, char **argv)
{
    const char *fname;
    const char *cptr;
    ip_image *im,*dim;
    struct ml *optlist;
    int type;
    int header,rowskip,resize;
    int complex_part;
    int arg;
    ip_coord2d size;
    double min, max;

    type = -1;
    size.x = size.y = -1;
    complex_part = -1;
    imgamma = contrast = 1.0;
    fname = NULL;
    arg = argc;
    header = rowskip = 0;
    spalette = PAL_GREY;

    register_evi(ip_version());

    prog.output = stdout;
    optlist = parseargs(argc,argv,&prog,help);

    /*
     * For handling raw images; type will be >= 0 if image to be read in is
     * a raw image.  Then header gives the number of bytes to skip at start
     * of file before reading image data, and rowskip specifies the number
     * bytes to skip between each row of the image.
     *
     * Header and rowskip only apply to raw images so we print out a warning
     * if someone is dumb enough to specify it for other images types.
     */

    getarg(optlist,O_RAWTYPE,&type);
    getarg(optlist,O_RAWSIZE,&size);
    if (getarg(optlist,O_HEADER,&header) && type == -1)
	wprintf("Redundant header size given. Ignoring it.\n");
    if (getarg(optlist,O_ROWSKIP,&rowskip) && type == -1)
	wprintf("Redundant rowskip given. Ignoring it.\n");

    if (type >= 0)
	type = convtype[type];	/* To ip lib image type */

    if (type >= 0 && size.x == -1 && size.y == -1) {
	eprintf("Raw data types require size specification (use option '-s')\n");
	exit(1);
    }

    if (type == -1 && size.x != -1) {
	wprintf("Redundant size specification given. Ignoring it.\n");
    }

    /*
     * If complex image then get which part (mag, phase, real...) of the
     * complex image to display.
     */
    getarg(optlist, O_COMPPART, &complex_part);

    /* Get palette to use */
    if (getarg(optlist,O_HOTBODY,NULL)) spalette = PAL_HOTBODY;
    if (getarg(optlist,O_GREYOBJ,NULL)) spalette = PAL_GREY;
#if 0
    if (getarg(optlist,O_LOG10,NULL)) spalette = PAL_LOG10;
    /* TBD -- currently not implemented */
    if (getarg(optlist,O_LTHRESH,&i)) spalette |= (i<<8) | IDP_LOW;
    if (getarg(optlist,O_UTHRESH,&i)) spalette |= (i<<16) | IDP_HIGH;
#endif

    /* Get contrast and gamma correction to use */
    getarg(optlist,O_CONTRAST,&contrast);
    if (getarg(optlist,O_GAMMA,&imgamma)) {
	imgamma = 1.0 / imgamma;
	spalette |= PAL_GAMCORR;
    }

    /* Scaling means fits palette to image min and max pixel */
    getarg(optlist,O_SCALE,&scaling);

    /* Or the user can specify how to map palette to image min and max */
    min = max = INFINITY;
    getarg(optlist,O_MIN,&min);
    getarg(optlist,O_MAX,&max);

    if ((isfinite(min) || isfinite(max)) && isinf(min+max)) {
	eprintf("Must specify both -min and -max options together.\n");
	exit(1);
    }

    if (scaling && isfinite(min)) {
	wprintf("Redundant -sc option given with -min/-max.  Ignoring -sc.\n");
	scaling = 0;
    }

    if (isinf(min) || isinf(max)) {
	/*
	 * Not specified on command line so default to settings suitable for
	 * a UBYTE image.
	 */
	min = 0.0;
	max = 255.0;
    }

    /* Any resizing of the image */
    getarg(optlist,O_RESIZE,&resize);
    getarg(optlist,O_FITIM,&fitim);

    /* Filename of image to display */
    getarg(optlist,O_IMAGE,&fname);
    getarg(optlist,O_MORE,&arg);

    freeargs(optlist);

    ip_register_error_handler(bombout);

    /* Display images... */

    if (arg < argc) {
	/* View many images -- TBD not currently implemented */
	eprintf("Multiple image display currently unsupported.\n");
	exit(1);
    }

    /* View just one image */

    ip_coord2d origin;
    int own_im = 0;

    im = load_image(fname, type, size, header, rowskip);

    if (complex_part != -1 && im->type != IM_COMPLEX)
	wprintf("Ignoring complex part specification as image not complex.");

    if ((resize && (im->size.x > DISPR_X || im->size.y > DISPR_Y)) || fitim) {
	dim = resize_image(im);
	own_im = 1;
    }else{
	dim = im;
    }

    if (im->type == IM_COMPLEX) {
	dim = get_complex_part(im, (complex_part >= 0) ? complex_part : 0);
	own_im = 1;
	if (complex_part == 3) {
	    /* Angle/phase part image */
	    min = -M_PI;
	    max = M_PI;
	}
    }

    if (NOT fname || STREQUAL(fname, "-"))
	fname = ip_msg_stdin;

    if (fname) {
	cptr = strrchr(fname, '/');
	if (cptr == NULL)
	    cptr = fname;
	else
	    cptr++;
    }else{
	cptr = NULL;
    }

    init_display(dim->size, cptr);

    display_contrast(contrast);
    display_gamma(imgamma);
    display_palette(spalette);

    origin.x = origin.y = 0;
    if (scaling)
	min = max = 0.0;

    display_imagex(FLG_NEWIMAGE, dim, origin, min, max);

    if (own_im) {
	ip_free_image(dim);
    }
    ip_free_image(im);

    wait_for_quit();

    return 0;
}




static void bombout(ip_error_logstack *ipr, int error, const char *extra, va_list ap)
{
    ip_print_error(PROGRAMME, ipr, error, extra, ap);
    if (error != ERR_DISPLAY_CLEAR)
	exit(1);
}



int interpret_key(char c)
{
    int result;

    result = 1;
    switch (c) {
    case 'q':
    case 'Q':
    case 'e':
    case 'E':
    case 'x':
    case 'X':
	result = -1;
	break;

    case ']':
    case '}':
	imgamma = 1.0 / imgamma;
	if (imgamma < 5)
	    imgamma += 0.1;
	imgamma = 1.0 / imgamma;
	break;

    case '[':
    case '{':
	imgamma = 1.0 / imgamma;
	if (imgamma > 0.15)
	    imgamma -= 0.1;
	imgamma = 1.0 / imgamma;
	break;

    case ',':
    case '<':
	if (contrast > 0)
	    contrast -= 0.1;
	break;

    case '.':
    case '>':
	if (contrast < 10)
	    contrast += 0.1;
	break;

    case 'g':
    case 'G':
	spalette &= ~IDP_PALETTE;
	spalette |= IDP_GREY;
	result = 2;
	break;

    case 'o':
    case 'O':
	spalette &= ~IDP_PALETTE;
	spalette |= IDP_GREYOBJ;
	result = 2;
	break;

    case 'h':
    case 'H':
	spalette &= ~IDP_PALETTE;
	spalette |= IDP_HOTBODY;
	result = 2;
	break;

    default:
	result = 0;
	break;
    }
    return result;
}



void wait_for_quit(void)
{
   int playing;
   int res;

   playing = TRUE;
   while (playing) {
       res = interpret_key(display_keypress(FLG_WAIT));
       if (res == -1) {
	   playing = FALSE;
       }else if (res >= 1) {
	   display_gamma(1.0 / imgamma);
#if 0
	   if (ownpal)
	       init_palette(normpal,spalette,contrast,&fg,&bg,&sdw,&text);
	   load_palette(optpal ? PAL_OPTIMUM : 256,normpal);
#endif
       }
   }
}



static ip_image *load_image(const char *fname, int type, ip_coord2d size,
			    int header, int rowskip)
{
    ip_image *im;

    /* If fname is "-" then use stdin */

    if (fname && STREQUAL(fname,"-"))
	fname = NULL;

    /* Read in image */

    if (type >= 0) { 		/* RAW image */

	im = read_raw_imagex(fname, type, size.x, size.y, header, rowskip, 0, NO_FLAG);

    }else{			/* Formatted image file (TIFF, JPEG, ...) */

	im = ip_read_image(fname, IM_BINARY, IM_UBYTE, IM_SHORT, IM_USHORT, IM_FLOAT,
			   IM_DOUBLE, IM_PALETTE, IM_RGB, -1);
    }

    if (im->type == IM_RGB16) {
	eprintf("Do not support %s typed images yet!\n",
		ip_image_type_info[im->type].name);
	exit(1);
    }

    return im;
}



static void init_display(ip_coord2d size, const char *extr)
{
    char title[128];

    if (size.x > DISPMAX_X)
	size.x = DISPMAX_X;
    if (size.y > DISPMAX_Y)
	size.y = DISPMAX_Y;
    lastsize = size;

    strcpy(title, prog.progname);
    title[0] = toupper(title[0]);
    if (extr) {
	strcat(title," - ");
	strcat(title,extr);
    }
    display_init(NULL, NULL, size, title, NO_FLAG);
}



static ip_image *resize_image(ip_image *im)
{
    ip_image *res;
    int factor;
    double tofit;

    if (fitim) {
	tofit = 512.0 / (im->size.x > im->size.y ? im->size.x : im->size.y);
	res = im_zoom(im, tofit, tofit, FLG_BILINEAR);
    }else{
	factor = (im->size.x > im->size.y ? im->size.x : im->size.y) - 1;
	factor /= 512;
	res = im_reduce(im,factor+1,FLG_SAMPLE);
    }
    return res;
}


static ip_image *get_complex_part(ip_image *im, int cp)
{
    ip_image *dim;
    static const int cpflag[4] = {FLG_REAL, FLG_IMAG, FLG_MAG, FLG_ANGLE};

    dim = im_convert(im, IM_FLOAT, cpflag[cp]);
    return dim;
}
