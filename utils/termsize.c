/*
   Module: mjc/termsize.c
   Author: M.J.Cree

   (C) 1999-2007 Michael Cree

   Routine to return connected terminal number of rows and cols.

*/

#include <stdio.h>
#include <stdlib.h>

#ifdef __APPLE__
#  include <sys/ioctl.h>
#else
#  include <termio.h>
#endif

#include "helpers.h"



/*

NAME:

   termsize()

PROTOTYPE:

   #include <mjc/mjc.h>

   int  termsize( numrows, numcols )
   int *numrows;
   int *numcols;

DESCRIPTION:

   Determine the number of rows in the connected terminal.  Returns TRUE if
   could determine terminal size else returns FALSE.

*/

int termsize(int *numrows, int *numcols)
{
    struct winsize ws;
    int errcode;

    errcode = ioctl(0, TIOCGWINSZ, &ws);
    if (errcode == -1) {
	/* error occurred */
	return FALSE;
    }else{
	*numrows = ws.ws_row;
	*numcols = ws.ws_col;
	return TRUE;
    }
}
