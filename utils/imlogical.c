/*
   Programme: imlogical
   Author:    M.J.Cree

   Copyright (C) 1997, 2019 by Michael J. Cree

DESCRIPTION:

   Bitwise logical operations between two images.

*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <ip/ip.h>
#include <ip/file.h>
#include <ip/tiff.h>
#include <ip/png.h>

#include "helpers.h"

#define PROGRAMME "imlogical"
#define VERSION "1.04"
#define DATE "3/1/2019"

/* Program command line parsing and opt data structures */

char progstr[256];

struct progenv prog = {
    NULL,
    PROGRAMME,
    VERSION,
    DATE
};


struct op help[] = {
    {OPT_SWITCH,		"tiff", NULL,	"Write result as TIFF file"
     " [def = png]"},
    {OPT_SWITCH,         "nc",   NULL,   "Do not compress output image."},
    {OPT_STRING | OPT_LONELY | OPT_REQ, NULL, "image1",
     "Input image filename." },
    {OPT_STRING | OPT_LONELY | OPT_NIN, NULL, "image2",
     "Input image filename."
     " [def = stdin]" },
    {OPT_STRING | OPT_LONELY, NULL,"imageout",
     "Output image filename."
     " [def = stdout]" },
    {OPT_DONE, NULL, NULL, NULL}
};


enum {
    O_TIFF, O_NOCOMP,
    O_IMAGE,
    O_IMAGEIN,   O_IMAGEOUT,
    O_DESC
};

char *imor_desc = "Bitwise OR two images together.";
char *imand_desc = "Bitwise AND two images together.";
char *imxor_desc = "Bitwise XOR two images together.";


static void bombout(ip_error_logstack *ipr, int error, const char *extra, va_list ap)
{
    ip_print_error(prog.progname, ipr, error, extra, ap);
    exit(1);
}


int main(int argc, char *argv[])
{
    const char *nin1, *nin, *nout;
    ip_image *im,*im1;
    int tiff, nocomp;
    optlist *ol;
    char desc[500];
    enum {
	IMOR, IMAND, IMXOR
    } operation;

    /* Sort out which programme we are running */

    if ((prog.progname = strrchr(argv[0], '/')) == NULL)
	prog.progname = argv[0];
    else
	prog.progname++;
    prog.output = stdout;

    if (STREQUAL(prog.progname, "imor")) {
	operation = IMOR;
	help[O_DESC].helpmsg = imor_desc;
    }else if (STREQUAL(prog.progname, "imand")) {
	operation = IMAND;
	help[O_DESC].helpmsg = imand_desc;
    }else if (STREQUAL(prog.progname, "imxor")) {
	operation = IMXOR;
	help[O_DESC].helpmsg = imxor_desc;
    }else{
	eprintf("What is '%s'???    I'm confused...\n",prog.progname);
	eprintf("Please call me 'imor', 'imand' or 'imxor'.\n");
	exit(1);
    }

    sprintf(progstr,"%s (V%s)",prog.progname,prog.version);

    /* Parse command line */

    register_evi(ip_version());

    ol = parseargs(argc,argv,&prog,help);

    nin1 = nin = nout = NULL;

    getarg(ol, O_TIFF, &tiff);
    getarg(ol, O_NOCOMP, &nocomp);
    getarg(ol, O_IMAGE, &nin1);
    getarg(ol, O_IMAGEIN, &nin);
    getarg(ol, O_IMAGEOUT, &nout);

    freeargs(ol);

    ip_register_error_handler(bombout);

    /* Load images */

    if (STREQUAL(nin1,"-")) nin1 = NULL;
    im1 = ip_read_image(nin1, IM_BINARY, IM_BYTE, IM_SHORT, IM_LONG, -1);
    if (nin1 == NULL) nin1 = ip_msg_stdin;

    if (nin && STREQUAL(nin,"-")) nin = NULL;
    im = ip_read_image(nin, IM_BINARY, IM_BYTE, IM_SHORT, IM_LONG, -1);
    if (nin == NULL) nin = ip_msg_stdin;

    if (im1->type != im->type) {
	eprintf("Both images must be same type.\n");
	exit(1);
    }

    /* Do required logical operation (depending on programme name) */

    switch (operation) {
    case IMOR:
	im_or(im1, im);
	sprintf(desc,"%s ORed with the image %s.",nin1,nin);
	break;
    case IMAND:
	im_and(im1, im);
	sprintf(desc,"%s ANDed with the image %s.",nin1,nin);
	break;
    case IMXOR:
	im_xor(im1, im);
	sprintf(desc,"%s XORed with the image %s.",nin1,nin);
	break;
    }

    /* Write out result */

    if (tiff)
	write_tiff_imagex(nout, im1, FLG_TAGLIST,
			  TAG_SOFTWARE, progstr,
			  TAG_DESCRIPTION, desc,
			  TAG_DATE,
			  nocomp ? TAG_NOCOMP : TAG_DONE,
			  TAG_DONE);
    else
	write_png_image(nout, im1);

    return 0;
}

