/*
   Header: ip/tiff.h
   Author: M.J.Cree

   Copyright (C) 1996-2007, 2016 Michael J. Cree

   Declarations for interfacing (reading/writing) to tiff images.

*/

#ifndef IP_TIFF_H
#define IP_TIFF_H

#include <ip/types.h>
#include <ip/image.h>
#include <stdio.h>

/* Tiff image handle */

typedef struct {
   uint16_t pi;				/* Photometic interp. */
   uint16_t bps;			/* Bits per sample */
   uint16_t sf;				/* Sample format */
   uint16_t spp;			/* Samples per pixel */
   uint16_t es_cnt;			/* Extra samples; number of them */
   uint16_t *es_values;
   uint16_t comp;			/* Compression scheme */
   int type;				/* ip library image type */
   ip_coord2d size;			/* Image size */
   void *f;				/* TIFF file handle */
   const char * fname;			/* Pointer to filename */
} ip_tiff_handle;

/* Tags for write_tiff_imagex() */
enum {
   TAG_MAXMIN = 1,
   TAG_MINIMUM,
   TAG_MAXIMUM,
   TAG_SOFTWARE,
   TAG_DOCNAME,
   TAG_DESCRIPTION,
   TAG_HOST,
   TAG_DATE,
   TAG_XRES,
   TAG_YRES,
   TAG_OPHINFO,
   TAG_NOCOMP,
   TAG_SKIP = 0x1000,
   TAG_SKIPINT,
   TAG_SKIPFLOAT,
   TAG_SKIPPTR
};

#ifndef TAG_DONE
#define TAG_DONE -1
#endif

/* Entry points to functions. */

extern ip_tiff_handle *open_tiff_image( const char *fname );
extern ip_image *read_tiff_image(ip_tiff_handle *);
extern ip_image *read_tiff_imagex(ip_tiff_handle *th, int first_row, int nrows);
extern int read_tiff_tag(ip_tiff_handle *, int tag, ...);
extern int write_tiff_imagex(const char *fname, ip_image *im, int flag, ...);
extern void close_tiff_image(ip_tiff_handle *);

#define write_tiff_image(f,i) write_tiff_imagex(f,i,NO_FLAG)

#endif
