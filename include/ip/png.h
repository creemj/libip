/* $Id$ */
/*
   Header: ip/png.h
   Author: M.J.Cree

   (C) 2001-2007 Michael Cree

   Declarations for interfacing (reading/writing) to PNG images.

*/

#ifndef IP_PNG_H
#define IP_PNG_H

#include <ip/types.h>
#include <ip/image.h>
#include <stdio.h>

#ifdef IPINTERNAL
#include <png.h>
#endif

/* Png image handle */

typedef struct {
   int type;			/* ip library image type */
   ip_coord2d size;		/* Image size */
   int bps;			/* Bits per sample */
   int ptype;			/* PNG image type */
   int comp;			/* Compression algorithm */
   int filter;			/* Compression filter */
   int interlace;		/* Interlacing */
   FILE *f;			/* file handle */
   const char * fname;		/* Pointer to filename */
#ifdef IPINTERNAL
   png_structp png;		/* PNG structure pointer */
   png_infop info;		/* PNG info pointer */
#else
   void *png;			/* PNG structure pointer */
   void *info;			/* PNG info pointer */
#endif
} ip_png_handle;


/* Entry points to functions. */

extern ip_png_handle *open_png_image( const char *fname );
extern ip_png_handle *open_png_imagex(FILE *f, const char *fname,
				  unsigned char hdr[4], int flag);
extern ip_image *read_png_image(ip_png_handle *);
extern ip_image *read_png_imagex(ip_png_handle *th, int first_row, int nrows);
extern int read_png_tag(ip_png_handle *, int tag, ...);
extern void close_png_image(ip_png_handle *);

extern int write_png_image(const char *fname, ip_image *im);
extern int write_png_imagex(const char *fname, ip_image *im, int flag);


#endif
