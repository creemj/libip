/*
  Header: filter.h
  Author: M. J. Cree

  Copyright (C) 2015 Michael J. Cree

  Declarations for kernels used in image filtering.

*/

#ifndef IP_FILTER_H
#define IP_FILTER_H

#include <ip/types.h>
#include <ip/image.h>

/*
 * Kernel used in image filtering.
 *
 * Treat this as private to the library.  Use only the functions described
 * below to manipulate an ip_kernel object.
 */

typedef struct {
    int type;			/* 1d horoz/vert/2d/separable */
    int reqtype;
    ip_coord2d size;
    int addend;
    int divider;
    const double *hfilter;
    const double *vfilter;
    const double *filter;
} ip_kernel;


/* The kernel types one can request in ip_alloc_kernel() */

#define IP_KERNEL_VERTICAL	0x1000
#define IP_KERNEL_HORIZONTAL	0x2000
#define IP_KERNEL_2D		0x4000

#define IP_KERNEL_GTYPE_MASK    0xf000

/*
 * First and second derivatives by differencing in one direction only; These
 * are 1-D kernels; OR with IP_KERNEL_VERTICAL or IP_KERNEL_HORIZONTAL
 * above.
 */
#define IP_KERNEL_FORWARD_DIFF  1
#define IP_KERNEL_BACKWARD_DIFF 2
#define IP_KERNEL_CENTRE_DIFF   3
#define IP_KERNEL_LAPLACE_DIFF	4

/*
 * Mean/Gaussian filtering can be 1-D (any length) or 2-D (NxM); OR with any
 * of VERTICAL, HORIZONTAL or 2D flags above.  If 2-D, the Gaussian filter
 * is circularly symmetric.
 */
#define IP_KERNEL_MEAN		5
#define IP_KERNEL_GAUSSIAN	6

/*
 * Edge detection (gradient) with 3x3 kernel; OR with HORIZONTAL or VERTICAL
 * flags above.
 */
#define IP_KERNEL_PREWITT	10
#define IP_KERNEL_SOBEL		11

/*
 * Laplacian with 3x3 kernel; OR with 2D flag above only.
 */
#define IP_KERNEL_LAPLACIAN	12

/*
 * Laplacian of Gaussian; OR with 2D flag above only.
 */
#define IP_KERNEL_LOG		13

extern ip_kernel *ip_alloc_kernel(int type, ...);
extern int ip_free_kernel(ip_kernel *k);
extern int ip_kernel_valid(ip_kernel *k);
extern void ip_kernel_dump(FILE *f, ip_kernel *kern);

extern int im_filter(ip_image *im, ip_kernel *kern, int flag);

#endif
