/*
   Header: ip/image.h
   Author: M. J. Cree

   � 1996-2013, 2015 Michael J. Cree

   Declarations of image related structures and types.

*/

#ifndef IP_IMAGE_H
#define IP_IMAGE_H

#include <ip/types.h>


union imagerow {
    void **v;			/* To ref image indep of type */
    uint8_t **ub;		/* UBYTE (also BINARY and PALETTE) images */
    int8_t **b;			/* BYTE */
    uint16_t **us;		/* Short unsigned int images */
    int16_t **s;		/* Short int images */
    uint32_t  **ul;		/* Long unsigned int images */
    int32_t  **l;		/* Long int images */
    uint64_t **ull;		/* Long long (64bit) unsigned int images */
    int64_t **ll;		/* Long long (64bit) int images */
    float **f;			/* Float images */
    double **d;			/* Double images */
    ip_complex **c;		/* Complex float images */
    ip_dcomplex **dc;		/* Complex double images */
    ip_rgb **r;			/* RGB colour image */
    ip_rgba **ra;		/* RGBA colour image */
    ip_lrgb **r16;		/* RGB 16-bit colour image */
};


/*
 *  All the possible image types.  Some are not supported throughout IP
 *  library.
 *
 *  The following image types have comprehensive support:
 *  IM_BINARY, IM_UBYTE, IM_SHORT, IM_USHORT, IM_ULONG, IM_FLOAT, IM_DOUBLE,
 *  IM_COMPLEX, IM_DCOMPLEX, IM_PALETTE, IM_RGB
 */

typedef enum {
    IM_UNDEFINED = 0,
    /* binary/logical type */
    IM_BINARY,
    /* supported integer types */
    IM_BYTE, IM_UBYTE,  IM_SHORT, IM_USHORT, IM_LONG, IM_ULONG,
    /* float types */
    IM_FLOAT, IM_DOUBLE,
    /* complex types */
    IM_COMPLEX, IM_DCOMPLEX,
    /* colour palette based images */
    IM_PALETTE,
    /* colour images */
    IM_RGB, IM_RGBA, IM_RGB16,
    /* proposed integer types; very little support exists for these */
    IM_LONG64, IM_ULONG64
} ip_image_type;


/*
 * Define the total number of image types supported:
 * IM_TYPEMAX -- total number of supported and compiled in image types.
 * IM_TYPETOTAL -- the full number of image types defined above, namely 18.
 *
 * By default the LONG64 and ULONG64 (for which support in the library is
 * nearly non-existant) is not included in IM_TYPEMAX so that one cannot
 * allocate or use (U)LONG64 images.
 */
#ifdef IP_INCLUDE_LONG64

#define IM_TYPEMAX (IM_ULONG64+1)
#define IM_TYPETOTAL (IM_ULONG64+1)

#else

/* Disable (U)LONG64 support with this option (which is the default) */
#define IM_TYPEMAX (IM_RGB16+1)
#define IM_TYPETOTAL (IM_ULONG64+1)

#endif

/*
 * A flag that can be pass as an image type in certain situations to match
 * any image type.
 */
#define IM_TYPE_ANY IM_TYPETOTAL


/*** Image data structure ***/

typedef struct {

    /* Caller programmes may read (but not modify) these fields. */

    ip_image_type type;		/* Image type - Byte, Short int, etc  */
    ip_coord2d size;		/* Image size (x,y) */

    /*
     * All fields below are private. Do NOT access in any way whatsoever.
     */

    size_t row_size;		/* Size of row including padding (in bytes). */
    unsigned int status;	/* Status - obvious, eh? */
    ip_rgb *palette;		/* For IM_PALETTE images. */
    uint8_t *imbuff;		/* Image data array */
    union imagerow imrow;	/* Pointers to each row of data */

    /*
     * Row buffers for working space and a dummy save space.  Only allocated
     * if some image procesing operator actually needs this.
     */

    int num_extra_rows;
    void *extra_rows;

    /*
     * Hold FFTW planners for this image (only used for COMPLEX type images,
     * and then only if an FFT is called on it). The first (zeroth index) is
     * for the forward transform and the second (index = 1) is for the
     * inverse transform.
     */

    void *fftw_plans[2];

} ip_image;


/* status field can take these values. */

/*
 * IP_IMDATA_ONE_ARRAY - All of image data allocated in one array,
 *   nevertheless there may be padding bytes at the end of each row.
 * If not set then memory allocations are per row.
 */
#define IP_IMDATA_ONE_ARRAY 0x01


/* Holds information about image types */
typedef struct {
    ip_image_type type;		/* Confirms type */
    int status;			/* 1 = well supported in IP library */
    size_t typesize;		/* Size of type in bytes */
    size_t basesize;		/* Size of basetype in bytes */
    double minvalue;		/* Minimum value of type can take */
    double maxvalue;		/* Maximum value of type can take */
    char *name;			/* type name written out */
} ip_image_type_info_t;

extern ip_image_type_info_t ip_image_type_info[IM_TYPETOTAL+1];

/***

    User routines for accessing pixel within image.

    There are a number of ways to access individual pixels in an image.
    Safest and most supportable (and also slowest) are im_value() and
    im_drawpoint().  Use these for one off image accesses (but note that
    COMPLEX and RGB images are not supported by these routines).

    If wanting access to image data for implementing an image processing
    routine that is not in the library then use im_rowadr() which returns a
    pointer to a row in an image.  It is guarenteed that all pixels in a row
    lie consequtively one after each other (as in an array) and that the row
    is at least a multiple of MACHINE_NATURAL_SIZE bytes long (i.e. there is
    byte padding at the end of the row to achieve this).  See im_rowadr()
    for examples of use.  The macro IM_ROWADR(), below, does the same thing
    as im_rowadr(), except it doesn't do any checking on the validity of the
    row, and directly accesses image structure.  Best to stick to
    im_rowadr() (because it's used outside the innermost loop, it is only
    fractionally slower then IM_ROWADR(), so speed arguments can't be used
    to justify the use of IM_ROWADR()).

    If you don't need access to a whole row, or need fast random access to
    an image, or access to COMPLEX or RGB images, then you can use the macro
    IM_PIXEL() which gives direct access to a pixel.  It is reasonably fast
    too.

    The t parameter of IM_ROWADR() and IM_PIXEL() is the type specifier.  It
    must be one of 'b', 's', etc., as listed in union imagerow.

    Note that the definition of IM_ROWADR() may be soon be changed to remove
    the t parameter and to return a pointer to void. Don't rely upon the
    current definition.

***/

#define IM_ROWADR(i,r,t) ((i)->imrow.t[r])
#define IM_PIXEL(i,x,y,t) ((i)->imrow.t[y][x])



/*** Function entry points. ***/

/* Get version string/numbers. */
extern char *ip_version(void);
extern int ip_version_major(void);
extern int ip_version_minor(void);

/* Set IP library global flags; flags are below */
extern void ip_set_global_flags(int flags);
extern void ip_clear_global_flags(int flags);
extern int ip_get_global_flags(void);
extern void ip_set_cache_size(int type, int size, int assoc);

#define IP_FAST_CODE 0x01
#define IP_HWACCEL 0x02
#define IP_FLAGS_MASK (IP_FAST_CODE | IP_HWACCEL)

/* Image allocation and de-allocation */
extern ip_image * ip_alloc_image(ip_image_type type, ip_coord2d size);
extern void ip_free_image(ip_image *im);

/* Allocate image from existing arrays */
size_t ip_imarray_rowsize(int type, ip_coord2d size);
void *ip_alloc_imarray(int type, ip_coord2d size);
ip_image *ip_alloc_image_from_array(ip_image_type type, ip_coord2d size,
				    void *array, size_t row_stride);
ip_image *ip_alloc_image_using_imarray(ip_image_type type, ip_coord2d size,
				       void *array);
void *ip_free_image_leaving_imarray(ip_image *im);

/* Switch pixel arrays between two compatible images */
int im_switch(ip_image *im1, ip_image *im2);

/* Clearing image */
extern int im_clear(ip_image *im);

/* Set all pixels in an image to a value. */
extern int im_set(ip_image *, double val, int flag);
extern int im_set_av(ip_image *, ip_anyval av, int flag);

/* Reading a pixel in image (or use IM_PIXEL() macro above). */
extern ip_anyval im_value(ip_image *im, ip_coord2d pt);

/* Get row address in image. */
extern void *im_rowadr(ip_image *im, int row);

/*
 * To set a pixel in image see graphics functions in proto.h or use
 * IM_PIXEL() macro above.
 */

/* Helper functions to access the image type info */
static inline size_t ip_type_basesize(int type)
{
    return ip_image_type_info[type].basesize;
}

static inline size_t ip_type_size(int type)
{
    return ip_image_type_info[type].typesize;
}

static inline double ip_type_min(int type)
{
    return ip_image_type_info[type].minvalue;
}

static inline double ip_type_max(int type)
{
    return ip_image_type_info[type].maxvalue;
}

static inline const char *ip_type_name(int type)
{
    return ip_image_type_info[type].name;
}

/* Helper functions for calculating size, and topl/botr to box conversion */
static inline int64_t ip_size_numpixels(ip_coord2d size)
{
    return (int64_t)size.x * (int64_t)size.y;
}

static inline int64_t ip_image_numpixels(ip_image *im)
{
    return ip_size_numpixels(im->size);
}

static inline int64_t ip_box_numpixels(ip_box box)
{
    return ip_size_numpixels(box.size);
}

static inline ip_coord2d ip_botr_size(ip_coord2d origin, ip_coord2d botr)
{
    return (ip_coord2d){botr.x-origin.x+1, botr.y-origin.y+1};
}

static inline ip_box ip_botr_box(ip_coord2d origin, ip_coord2d botr)
{
    return (ip_box){origin, ip_botr_size(origin, botr)};
}

static inline ip_coord2d ip_box_botr(ip_box box)
{
    return (ip_coord2d){box.origin.x+box.size.x-1, box.origin.y+box.size.y-1};
}

/* These are intended primarily for use within ip library */
extern int ip_im_alloc_extra_rows(ip_image *im, int numrows);
extern void ip_im_set_rowptr_padding(ip_image *im, int flag);

#define IP_ROWPTRPAD_LASTROW 1
#define IP_ROWPTRPAD_EXTRAROW1 2

extern ip_anyval ip_init_anyval_dddd(int type, double v1, double v2, double v3, double v4);
extern int image_valid(ip_image *im);
extern int images_same_size(ip_image *, ip_image*);
extern int images_same_type(ip_image *, ip_image*);
extern int images_compatible(ip_image *, ip_image *);
extern int image_not_clrim(ip_image *im);
extern int image_bad_type(ip_image *im, ...);
extern int ip_point_in_image(char *parm, ip_coord2d c, ip_image *im);
extern int ip_value_in_image(char *parm, double v, ip_image *im);
extern int ip_anyval_in_image(char *desc, ip_anyval v, ip_image *im);
extern int ip_box_in_image(char *parm, ip_box b, ip_image *im);
extern int type_integer(int type);
extern int type_real(int type);
extern int type_complex(int type);
extern int type_scalar(int type);
extern int type_clrim(int type);
extern int type_rgbim(int type);
extern int type_ul(int type);
extern int type_ull(int type);
extern int type_d(int type);
extern int type_signed(int type);
extern int in_image_val(ip_image *, double val1, double val2);
extern int ip_flag_ok(int flag, ... );
extern int ip_flag_bits_unused(int flag, int unused);
extern int ip_parm_powof2(char *parm, int v);
extern int ip_parm_mult2(char *parm, int v);
extern int ip_parm_odd(char *parm, int val);
extern int ip_parm_inrange(char *parm, double val, double min, double max);
extern int ip_parm_less(char *parm, double val, double max);
extern int ip_parm_greater(char *parm, double val, double min);
extern int ip_parm_lesseq(char *parm, double val, double max);
extern int ip_parm_greatereq(char *parm, double val, double min);
extern int ip_parm_point_in_box(char *parm, ip_coord2d coord, ip_box box);

/* These ones implemented as macros. */

#define image_type_integer(x) type_integer((x)->type)
#define image_type_real(x) type_real((x)->type)
#define image_type_clrim(x) type_clrim((x)->type)
#define image_type_rgbim(x) type_rgbim((x)->type)
#define image_type_complex(x) type_complex((x)->type)


#endif
