/*
   Name:  version.h

   Version file of the IP library.

*/

#define IP_LIBRARY "ip"
#define IP_VERSION "10.5"
#define IP_DATE "19/7/2016"

#define IP_VERSION_MAJOR 10
#define IP_VERSION_MINOR 5
