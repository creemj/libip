/*
   Header: ip/hist.h
   Author: M.J.Cree

   Copyright (C) 2000, 2013, 2015, 2017-2019 Michael J. Cree.
*/

#ifndef IP_HIST_H
#define IP_HIST_H

/* To get memset() */
#include <string.h>

#include <ip/types.h>
#include <ip/stats.h>

/* To get ip_clz() */
#include <ip/divide.h>

/*
 * If you add an histogram type here you must also:
 * 1) Implement code for the histogram type in hist.c.
 * 2) Add the histogram type string and add code to ip_dump_hist() in debug.c.
 * 3) Update the check on htype parameter in ip_alloc_hist().
 */

typedef enum {
    HIST_COMPONENTS, HIST_INTENSITY, HIST_MAGPHASE, HIST_LAYERED
} ip_hist_type;

#define IP_HIST_MAX_TYPE HIST_LAYERED



/*
 * layered histogram parts; only used in HIST_LAYERED
 */
struct ip_hist_layer {
    int count;
    union {
	struct ip_hist_layer *layer;
	int *bins;
    };
};

typedef struct ip_hist_layer ip_hist_layer;

/*
 * A histogram part is a complete histogram in its own right.
 */
typedef struct {
    double low;
    double high;
    double width;
    int numbins;
    int under;
    int over;
    int nan;
    int *bins;
    /* Only used in layered histograms */
    int numlayers;
    int layer_numbins;
    int offset;
    union {
	int *layer0;
	ip_hist_layer *layer;
    };
} ip_hist_part;


/*
 * A histogram consists of a number of histogram parts. This enables a
 * histogram to be returned from im_hist() with a number of parts for images
 * with multiple components, .e.g., three parts being a red, green, and blue
 * histogram part for each component of an RGB image, OR for a layered
 * sparse-dense histogram.
 */

typedef struct {
    /* Duplicates image info from which this histogram derived. */
    int imtype;
    ip_coord2d imsize;

    /* ROI over which this histogram calculated. */
    ip_box box;

    /* Histogram for each component */
    ip_hist_type htype;
    int num_parts;
    ip_hist_part *hp;
} ip_hist;


/*
 * To be used in the call to im_hist_x() to provide a more extensive
 * interface for creating histograms than is possible with im_hist().
 */

struct ip_hist_parts_spec {
    double low, high;
    int numbins;
};

struct ip_hist_args {
    ip_hist_type htype : 8;
    unsigned int hflag : 8;
    unsigned int numparts : 8;
    ip_roi *roi;
    double low, high;
    int numbins;
    struct ip_hist_parts_spec *parts_spec;
};

/* hflag can be one of the following (or NO_FLAG/FLG_NONE/0) */
#define IP_HIST_HIGH_IS_HIGHBIN_RHS 0
#define IP_HIST_HIGH_IS_OVER_LHS 1

/* Histogram allocation/ de-allocation */
extern ip_hist *ip_alloc_hist(ip_hist_type htype, int numparts, struct ip_hist_parts_spec *hps);
extern void ip_free_hist(ip_hist *h);
extern void ip_hist_reset(ip_hist *h);

/* Get image histogram */
extern ip_hist *im_hist_x(ip_image *im, struct ip_hist_args hargs);
extern ip_hist *im_hist(ip_image *, ip_roi *roi, double low, double high, int numbins);
#define im_hist_a(im, ...) \
    im_hist_x(im, (struct ip_hist_args){.htype=HIST_COMPONENTS,		\
		.hflag = IP_HIST_HIGH_IS_HIGHBIN_RHS, .roi=NULL,	\
		.low=ip_type_min(im->type), .high=ip_type_max(im->type), \
		.numbins = 256, .numparts = 0, .parts_spec=NULL,  __VA_ARGS__})

/* Set an offset to the bin indexing (use with caution; experimental) */
static inline void ip_hist_set_bin_offset(ip_hist *h, int p, int offset)
{
    h->hp[p].offset = offset;
    h->hp[p].bins -= offset;
    if (h->htype == HIST_LAYERED)
	h->hp[0].layer0 -= offset >> ((h->hp[0].numbins == 256) ? 4 : 8);
}

/* Manipulate image histogram */
extern int ip_hist_smooth(ip_hist *h, int p, int kernelsize);

/* Statistics on histogram */
extern long int ip_hist_total_count(ip_hist *h, int p);
extern int ip_hist_peak(ip_hist *h, int p, int *location, int flag);
extern double ip_hist_entropy(ip_hist *h, int p);
extern void ip_hist_stats(ip_hist *h, ip_stats *st);

/* Points in bin */
static inline double ip_hist_bin_width(ip_hist *h, int p)
{
    return h->hp[p].width;
}

static inline double ip_hist_bin_left(ip_hist *h, int p, int b)
{
    return h->hp[p].low + b*ip_hist_bin_width(h, p);
}

static inline double ip_hist_bin_centre(ip_hist *h, int p, int b)
{
    return h->hp[p].low + (b+0.5)*ip_hist_bin_width(h, p);
}

/* Access to bins */
static inline void ip_hist_bin_set(ip_hist *h, int p, int b, int count)
{
    h->hp[p].bins[b] = count;
}

static inline int ip_hist_bin_count(ip_hist *h, int p, int b)
{
    return h->hp[p].bins[b];
}

static inline void ip_hist_bin_inc(ip_hist *h, int p, int b)
{
    h->hp[p].bins[b]++;
}

static inline void ip_hist_bin_dec(ip_hist *h, int p, int b)
{
    h->hp[p].bins[b]--;
}

static inline void ip_hist_bin_add(ip_hist *h, int p, int b, int x)
{
    h->hp[p].bins[b] += x;
}

static inline void ip_hist_bin_sub(ip_hist *h, int p, int b, int x)
{
    h->hp[p].bins[b] -= x;
}

static inline void ip_hist_over_inc(ip_hist *h, int p)
{
    h->hp[p].over++;
}

static inline int ip_hist_over_count(ip_hist *h, int p)
{
    return h->hp[p].over;
}

static inline void ip_hist_under_inc(ip_hist *h, int p)
{
    h->hp[p].under++;
}

static inline int ip_hist_under_count(ip_hist *h, int p)
{
    return h->hp[p].under;
}

static inline void ip_hist_nan_inc(ip_hist *h, int p)
{
    h->hp[p].nan++;
}

static inline int ip_hist_nan_count(ip_hist *h, int p)
{
    return h->hp[p].nan;
}

static inline void ip_hp_bin_inc(ip_hist_part *hp, int b)
{
    hp->bins[b]++;
}

static inline void ip_hp_over_inc(ip_hist_part *hp)
{
    hp->over++;
}

static inline void ip_hp_under_inc(ip_hist_part *hp)
{
    hp->under++;
}

static inline void ip_hp_nan_inc(ip_hist_part *hp)
{
    hp->nan++;
}

/*
 * Layered histograms; first for 16-bit data
 *
 * Notes:
 *
 * (1) bin_next and bin_prev routines are un-protected; the caller must know
 * that a non-zero bin exists in the direction searched before calling these
 * two routines.
 *
 * (2) memory allocation failures result in the count not being included in
 * the histogram bin and the nan field being incremented.  The histogram
 * code does not signal an error on memory allocation failure of a sub-layer
 * or bin of a HIST_LAYER histogram, thus the caller should check the nan
 * field before freeing the histogram to detect any incorrect operation due
 * to memory allocation failure.
 */


static inline int ip_hist_l_shift(ip_hist *h)
{
    return 31 - ip_clz(h->hp[0].layer_numbins);
}

static inline int ip_hist_l_mask(ip_hist *h)
{
    return h->hp[0].layer_numbins - 1;
}

static inline int ip_hist_l_coarse_count(ip_hist *h, int b, int shift)
{
    int high = b>>shift;
    return h->hp[0].layer0[high];
}

static inline int ip_hist_l_bin_count(ip_hist *h, int b, int shift)
{
    return h->hp[0].bins[b];
}


/* The lu8 routines assume bin b is strictly between 0 and 255 inclusive. */

static inline void ip_hist_lu8_bin_add(ip_hist *h, int b, int count)
{
    int high = b>>4;
    ip_hist_part *hp = h->hp;
    hp->bins[b] += count;
    hp->layer0[high] += count;
}

static inline void ip_hist_lu8_bin_sub(ip_hist *h, int b, int count)
{
    int high = b>>4;
    ip_hist_part *hp = h->hp;
    hp->bins[b] -= count;
    hp->layer0[high] -= count;
}

static inline void ip_hist_lu8_bin_inc(ip_hist *h, int b)
{
    int high = b>>4;
    ip_hist_part *hp = h->hp;
    hp->bins[b]++;
    hp->layer0[high]++;
}

static inline void ip_hist_lu8_bin_dec(ip_hist *h, int b)
{
    int high = b>>4;
    ip_hist_part *hp = h->hp;
    hp->bins[b]--;
    hp->layer0[high]--;
}

static inline int ip_hist_lu8_coarse_count(ip_hist *h, int b)
{
    return ip_hist_l_coarse_count(h, b, 4);
}

static inline int ip_hist_lu8_bin_count(ip_hist *h, int b)
{
    return ip_hist_l_bin_count(h, b, 4);
}

static inline unsigned int ip_hist_lu8_bin_next(ip_hist *h, int b)
{
    int high = b>>4;
    ip_hist_part *hp = h->hp;

    if (hp->layer0[high]) {
	int bend = (b | 0x0f) + 1;
	while (b < bend && hp->bins[b] == 0)
	    b++;
	if (b < bend)
	    return b;
    }

    high++;
    while (hp->layer0[high] == 0) {
	high++;
    }

    b = high << 4;
    while (hp->bins[b] == 0) {
	b++;
    }

    return b;
}


static inline unsigned int ip_hist_lu8_bin_prev(ip_hist *h, int b)
{
    int high = b>>4;
    ip_hist_part *hp = h->hp;

    if (hp->layer0[high]) {
	int bend = (b & (~0x0f)) - 1;
	while (b > bend && hp->bins[b] == 0)
	    b--;
	if (b > bend)
	    return b;
    }

    high--;
    while (hp->layer0[high] == 0) {
	high--;
    }

    b = (high << 4) | 0x0f;
    while (hp->bins[b] == 0) {
	b--;
    }

    return b;
}


/* The lu16 routines assume bin b is between 0 and 65535 inclusive */

static inline void ip_hist_lu16_bin_add(ip_hist *h, int b, int count)
{
    int high = b>>8;
    ip_hist_part *hp = h->hp;
    hp->bins[b] += count;
    hp->layer0[high] += count;
}

static inline void ip_hist_lu16_bin_sub(ip_hist *h, int b, int count)
{
    int high = b>>8;
    ip_hist_part *hp = h->hp;
    hp->bins[b] -= count;
    hp->layer0[high] -= count;
}

static inline void ip_hist_lu16_bin_inc(ip_hist *h, int b)
{
    int high = b>>8;
    ip_hist_part *hp = h->hp;
    hp->bins[b]++;
    hp->layer0[high]++;
}

static inline void ip_hist_lu16_bin_dec(ip_hist *h, int b)
{
    int high = b>>8;
    ip_hist_part *hp = h->hp;
    hp->bins[b]--;
    hp->layer0[high]--;
}

static inline int ip_hist_lu16_coarse_count(ip_hist *h, int b)
{
    int high = b >> 8;
    return h->hp->layer0[high];
}

static inline int ip_hist_lu16_bin_count(ip_hist *h, int b)
{
    return h->hp->bins[b];
}


static inline unsigned int ip_hist_lu16_bin_next(ip_hist *h, int b)
{
    int high = b>>8;
    ip_hist_part *hp = h->hp;

    if (hp->layer0[high]) {
	int bend = (b | 0xff) + 1;
	while (b < bend && hp->bins[b] == 0)
	    b++;
	if (b < bend)
	    return b;
    }

    high++;
    while (hp->layer0[high] == 0) {
	high++;
    }

    b = high << 8;
    while (hp->bins[b] == 0) {
	b++;
    }

    return b;
}


static inline unsigned int ip_hist_lu16_bin_prev(ip_hist *h, int b)
{
    int high = b>>8;
    ip_hist_part *hp = h->hp;

    if (hp->layer0[high]) {
	int bend = (b & (~0xff)) - 1;
	while (b > bend && hp->bins[b] == 0)
	    b--;
	if (b > bend)
	    return b;
    }

    high--;
    while (hp->layer0[high] == 0) {
	high--;
    }

    b = (high << 8) | 0xff;
    while (hp->bins[b] == 0) {
	b--;
    }

    return b;
}


/* Colour based routines. */
extern int im_palette_compress(ip_image *im, ip_hist *hg);

/* Debugging routines. */
extern void ip_dump_hist(FILE *f, ip_hist *h, int flag);

#endif
