/*
   Header: ip/stats.h
   Author: M.J.Cree

   (C) 1995-2007, 2015 Michael Cree

   Statistics routines header file.

*/

#ifndef IP_STATS_H
#define IP_STATS_H

#include <stdio.h>
#include <ip/types.h>

typedef struct {
    /* Private fields - do not access or change. */

    int id;
    void *ptr;

    /*
     * Public fields follow - these may be read after calling im_stats() or
     * im_extrema() on image.
     */

    /* General image particulars */

    int type;			/* Image type */
    ip_coord2d size;		/* Image size */
    ip_box box;			/* ROI over which statistics calculated. */
    int infinities;		/* TRUE = NaNs or infinities present */

    /*
     * For most images the following values are for the image.
     *
     * For COMPLEX images the following values are calculated on the
     * magnitude part.
     *
     * For RGB, RGB16, RGBA and PALETTE images the following values are
     * calculated on the intensity = 0.299*red + 0.587*green + 0.114*blue.
     */

    double min, max;		/* Minimum, Maximum */
    ip_coord2d minpt, maxpt;	/* Location of minimum and maximum */

    double sum;			/* Summation of pixel values */
    double sumsq;		/* Summation of the squared pixel values */
    double mean;		/* Mean */
    double rms;			/* RMS (square root of the mean squared values) */
    double stddev;		/* Standard devation */

    /*
     * Only valid for COMPLEX images. Contains statistics for the separate
     * imaginary and real parts.
     */

    ip_dcomplex cmin, cmax;	 /* Minima, Maxima */
    ip_coord2d cminpt_r,cminpt_i; /* Location of minimum for real & imag parts */
    ip_coord2d cmaxpt_r,cmaxpt_i; /* Location of maximum for real & imag parts */
    ip_dcomplex csum;		 /* Summation of real and imag pixel values */
    ip_dcomplex csumsq;		 /* Summation of squared real and imag pixel values */
    ip_dcomplex cmean;		 /* Mean */
    ip_dcomplex crms;		 /* RMS */
    ip_dcomplex cstddev;	 /* Standard deviation */

    /*
     * Only valid for RGB, RGB16, RGBA and PALETTE images. Contains
     * statistics for the separate reg, green and blue fields.
     */

    ip_drgba rmin, rmax;
    ip_coord2d rminpt_r, rminpt_g, rminpt_b, rminpt_a;
    ip_coord2d rmaxpt_r, rmaxpt_g, rmaxpt_b, rmaxpt_a;
    ip_drgba rsum, rsumsq, rmean, rrms, rstddev;

    /* Only valid for PALETTE images. */

    uint8_t penmin, penmax;

    /* The following calculated by ip_hist_stats() */

    double median;
    double entropy;

} ip_stats;


extern ip_stats *ip_alloc_stats(int);
extern void ip_free_stats(ip_stats *st);
extern void ip_dump_stats(FILE *f, ip_stats *st);
extern int im_stats(ip_image *im, ip_stats *st, ip_roi *roi);
extern int im_extrema(ip_image *im, ip_stats *st, ip_roi *roi);

#endif
