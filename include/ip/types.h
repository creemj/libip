/*
   Header: ip/types.h
   Author: M. J. Cree

   (C) 1995-2013 Michael J. Cree

   Type declarations for IP library routines.

*/

#ifndef IP_TYPES_H
#define IP_TYPES_H

/* Note that we rely upon C99 type definitions */
#include <stddef.h>
#include <stdint.h>
/* Actually we don't; the c99 complex type appears to have fundamental
   limitations which limit the ability to write efficient code ...
#include <complex.h>
*/

/* My favourite def'ns */

#ifndef TRUE
#define TRUE 1
#endif
#ifndef FALSE
#define FALSE 0
#endif

#define STREQUAL(x,y) (strcmp(x,y)==0)
#define NOT !

/*
 * Basic integer types used in images. We assume that integers up to at
 * least 32 bits are available on the target system.
 *
 * For new code use the C99 type specifiers; the IP_* types are now removed.
 *
 * The types available for use are: int8_t, uint8_t, int16_t, etc.
 */


/*
 * Complex numbers
 *
 * We don't use C99 complex type as it appears we cannot set the real or
 * imaginary part of the c99 complex scalar alone.
 *
 * The trickery with MJC_TYPES_H is to get a compatible defn with my
 * private MJC-common library which also defines a complex type.
 */
#ifdef MJC_TYPES_H
typedef complex ip_complex;
#else
typedef struct {
   float r;			/* Real part */
   float i;			/* Imaginary part */
} ip_complex;
#endif

/*
 * Double complex -- for extra precision in special cases.  N.B., there are
 * very few image operators that support this complex type yet.
 */
typedef struct {
   double r;			/* Real part */
   double i;			/* Imaginary part */
} ip_dcomplex;


/* For RGB colour images use a RGB triplet. */
typedef struct {
   uint8_t r;
   uint8_t g;
   uint8_t b;
} ip_rgb;

typedef struct {
   uint16_t r;
   uint16_t g;
   uint16_t b;
} ip_lrgb;

/* RGB with an alpha channel */
typedef struct {
   uint8_t r;
   uint8_t g;
   uint8_t b;
   uint8_t a;
} ip_rgba;

/* RGBA with all fields double.  Used in ip_stats object for example. */
typedef struct {
   double r;
   double g;
   double b;
   double a;
} ip_drgba;

/* 2d Coordinate or point or size */
#ifdef MJC_TYPES_H
typedef coord ip_coord2d;
#else
typedef struct {
   int x;			/* X-coordinate */
   int y;			/* Y-coordinate */
} ip_coord2d;
#endif

/* 2d coordinate pair where the components can be non-integral */
typedef struct {
   double x;			/* X-coordinate */
   double y;			/* Y-coordinate */
} ip_vector2d;


/*
 * A rectangular box with top-left corner at origin, usually specified
 * w.r.t. to an image's coordinate system.
 */
typedef struct {
   ip_coord2d origin;		/* Top-left corner of box */
   ip_coord2d size;		/* Size of box */
} ip_box;

#if 1
/* An obsolote def'n of a rectangular box that is here because I
   havn't updated all the code in the library. (It is still used in
   object code). Don't use this for any new code.
*/

typedef struct {
   ip_coord2d tl;
   ip_coord2d br;
} ip_obox;
#endif


/*
 * A union to hold any possible pixel value (and other thingies too).
 * Useful for returning any sort of value from a function (this is really
 * getting to the point where I must learn C++ one day!)  Actually, I have
 * taken a look at C++ and I'm not convinced.  Inheritance is greatly over
 * rated.
 */

typedef union {
   uint8_t b;
   int16_t s;
   uint16_t us;
   int32_t l;
   uint32_t ul;
   float f;
   double d;
   ip_complex c;
   ip_rgb r;
   ip_coord2d pt;
   ip_vector2d v2d;
   ip_box box;
   int i;
   void *ptr;
} ip_datum;


/*
 * A better struct than the above for representing any pixel value of any
 * image type.  Uses at most four CPU registers even on a 32-bit arch so
 * should be able to be past by value as registers in function calls.
 *
 * The convention is that if you are passing a constant value to an image
 * processing operator then pass the constant in the ull[0] field if
 * operating on a scalar integer image type, in the d[0] field if operating
 * on a real (floating point) image, in the d[0] field (real part) and the
 * d[1] field (imaginary part) if operating on a complex image, and in the
 * ul[0] to ul[2] fields (and the ul[3] field if using alpha channel) if
 * operating on an RGB image type.
 */

typedef union {
    uint32_t ul[4];
    uint64_t ull[2];
    double d[2];
} ip_anyval;


/*
 * Region of interest structure.  Note that this should be treated
 * _strictly_ as an abstract data type, since I will likely change it in the
 * future. Use function calls to allocate an ROI or to access values from
 * ROI.
 */

typedef struct {
   unsigned int type;		/* Type of ROI */
   ip_box rbox;			/* Bounding box if ROI type == ROI_RECTANGLE */
} ip_roi;

/* Polynomials (used in warping routines and elsewhere too!) */
typedef struct {
   int dimension;		/* Dimension of polynomial domain, 1 or 2. */
   int order;			/* Order of polynomial, 1 upto 4 */
   ip_vector2d origin;		/* Origin */
   double *X;			/* Coefficients */
} ip_poly;


typedef struct {
   int dimension;		/* Dimension of polynomial domain, 1 or 2. */
   int order;			/* Order of polynomial, 1 upto 4 */
   double *X;			/* Coefficients of X polynomial */
   double *Y;			/* Coefficients of Y polynomial */
} ip_poly2d;

#define POLY2D_MAXORDER 3

typedef struct {
   double correlation;		/* Correlation coeff returned by registration*/
   double rotate;		/* Rotation angle */
   double scale;		/* Scaling */
   ip_coord2d trans;		/* Translation */
} regparms;


/* Display routine mouse pointer/key-press event structure */

typedef struct {
   int type;			/* Type of event reported */
   int id;			/* ID of image window pointer is in */
   ip_coord2d pos;	        /* Position of pointer inside image window */
   int left;			/* TRUE = left mouse button down */
   int right;			/* TRUE = right mouse button down */
   int middle;			/* TRUE = middle mouse button down */
   int drag;
   int chr;			/* Key-press */
} event_info;

/* Type field can take one of */

#define EVT_MOUSEMOVE   1
#define EVT_MOUSEBUTTON 2
#define EVT_KEYPRESS    3

#endif
