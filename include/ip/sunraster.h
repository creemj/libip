/* $Id$ */
/*
   Header: ip/sunraster.h
   Author: M.J.Cree

   (C) 1996-2007 Michael Cree

   Declarations for interfacing (reading/writing) to sunraster images.

*/

#ifndef IP_SUNRASTER_H
#define IP_SUNRASTER_H

#include <ip/types.h>
#include <ip/image.h>
#include <stdio.h>



/* SunRaster image handle */

typedef struct {
   int rtype;
   int type; 
   ip_coord2d size;
   int depth;
   int rowlen;
   int imagelen;
   int maptype;
   int maplength;
   FILE *f;
   const char *fname;
} ip_sunraster_handle;



/* Entry points to functions. */

extern ip_sunraster_handle 
   *open_sunraster_image( const char *name );
extern ip_sunraster_handle 
   *open_sunraster_imagex(FILE *f, const char *fname, 
			  unsigned char hdr[4], int flag);
extern ip_image *read_sunraster_image(ip_sunraster_handle *);
extern ip_image *read_sunraster_imagex(ip_sunraster_handle *, 
				       int first_row, int nrows);
extern int write_sunraster_image(const char *fname, ip_image *im);
extern int write_sunraster_imagex(const char *fname, ip_image *im, int flag);
extern void close_sunraster_image(ip_sunraster_handle *);


#endif
