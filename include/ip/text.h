/*

   Header: ip/text.h
   Author: M. J. Cree

   Public interface for writing text into images

*/

#ifndef IP_TEXT_H
#define IP_TEXT_H

#ifdef IP_TEXTINT
/* private interface */
typedef struct {
   FT_Face face;
   int pixsize;
   int leading;
} ip_font;
#else
/* public interface */
typedef void ip_font;
#endif

extern ip_font *ip_alloc_font(char *font, int pixsize, int leading);
extern void ip_free_font(ip_font *ft);
extern int im_text(ip_image *im, ip_font *font, char *str, ip_coord2d pos, 
		   double fg, double bg);

#endif
