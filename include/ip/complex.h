#ifndef IP_COMPLEX_H
#define IP_COMPLEX_H


/* Operators on complex numbers */

#include <math.h>
#include <ip/types.h>

static inline ip_complex ip_complex_neg(ip_complex z)
{
    return (ip_complex){-z.r, -z.i};
}

static inline ip_complex ip_complex_conj(ip_complex z)
{
    return (ip_complex){z.r, -z.i};
}

static inline ip_complex ip_complex_add(ip_complex a, ip_complex b)
{
    return (ip_complex){a.r + b.r, a.i + b.i};
}

static inline ip_complex ip_complex_sub(ip_complex a, ip_complex b)
{
    return (ip_complex){a.r - b.r, a.i - b.i};
}

static inline ip_complex ip_complex_mul(ip_complex a, ip_complex b)
{
    return (ip_complex){a.r*b.r - a.i*b.i, a.r*b.i + a.i*b.r};
}

/*
 * Note: we take care to preserve the full potential domain of complex
 * division.  A naive implementation can overflow even though the correct
 * result is representable in the complex type.  For example, gcc 3.3 gives
 * incorrect results if you write A / B with complex A and B, and when
 * mag(B)>sqrt(MAX_FLT).
 */
static inline ip_complex ip_complex_div(ip_complex d, ip_complex s)
{
    ip_complex res;

#if 1
    /* The if statement is costly, maybe try something else? */
    float dc, denom;
    if (fabs(s.r) > fabs(s.i)) {
        dc = s.i/s.r;
        denom = s.r + s.i*dc;
        res.r = (d.r + d.i * dc) / denom;
        res.i = (d.i - d.r * dc) / denom;
    }else{
        dc = s.r/s.i;
        denom = s.r*dc + s.i;
        res.r = (d.r*dc + d.i) / denom;
        res.i = (d.i*dc - d.r) / denom;
    }
    /* Fix up infinities/zeros computed as Nan + iNan. */
    if (isnan(res.r) && isnan(res.i)) {
	if (denom == 0.0 && (!isnan(d.r) || !isnan(d.i))) {
	    /* nonzero / zero */
	    res.r = copysignf(INFINITY, s.r) * d.r;
	    res.i = copysignf(INFINITY, s.r) * d.i;
	}else if ((isinf(d.r)  || isinf(d.i)) && isfinite(s.r) && isfinite(s.i)) {
	    /* infinite / finite */
	    d.r = copysignf(isinf(d.r) ? 1 : 0, d.r);
	    d.i = copysignf(isinf(d.r) ? 1 : 0, d.i);
	    res.r = INFINITY * (d.r*s.r + d.i*s.i);
	    res.i = INFINITY * (d.i*s.r - d.r*s.i);
	}else if ((isinf(s.r) || isinf(s.i)) && isfinite(d.r) && isfinite(d.i)) {
	    /* finite / infinite */
	    s.r = copysignf(isinf(s.r) ? 1 : 0, s.r);
	    s.i = copysignf(isinf(d.r) ? 1 : 0, s.i);
	    res.r = 0.0F * (d.r*s.r + d.i*s.i);
	    res.i = 0.0F * (d.i*s.r - d.r*s.i);
	}
    }
#else
    /*
     * Faster to avoid an if statement and process as a double but does not
     * have same rounding properties as calculated in float.
     */
    double denom, x, y, u, v;
    u = (double)s.r;
    v = (double)s.i;
    x = (double)d.r;
    y = (double)d.i;
    denom = u*u + v*v;
    res.r = (float)((u*x+v*y)/denom);
    res.i = (float)((y*u -v*x)/denom);
#endif

    return res;
}



static inline ip_dcomplex ip_dcomplex_neg(ip_dcomplex z)
{
    return (ip_dcomplex){-z.r, -z.i};
}

static inline ip_dcomplex ip_dcomplex_conj(ip_dcomplex z)
{
    return (ip_dcomplex){z.r, -z.i};
}

static inline ip_dcomplex ip_dcomplex_add(ip_dcomplex a, ip_dcomplex b)
{
    return (ip_dcomplex){a.r + b.r, a.i + b.i};
}

static inline ip_dcomplex ip_dcomplex_sub(ip_dcomplex a, ip_dcomplex b)
{
    return (ip_dcomplex){a.r - b.r, a.i - b.i};
}

static inline ip_dcomplex ip_dcomplex_mul(ip_dcomplex a, ip_dcomplex b)
{
    return (ip_dcomplex){a.r*b.r - a.i*b.i, a.r*b.i + a.i*b.r};
}

static inline ip_dcomplex ip_dcomplex_div(ip_dcomplex d, ip_dcomplex s)
{
    ip_dcomplex res;
    double dc, denom;
    if (fabs(s.r) > fabs(s.i)) {
        dc = s.i/s.r;
        denom = s.r + s.i*dc;
        res.r = (d.r + d.i * dc) / denom;
        res.i = (d.i - d.r * dc) / denom;
    }else{
        dc = s.r/s.i;
        denom = s.r*dc + s.i;
        res.r = (d.r*dc + d.i) / denom;
        res.i = (d.i*dc - d.r) / denom;
    }
    /* Fix up infinities/zeros computed as Nan + iNan. */
    if (isnan(res.r) && isnan(res.i)) {
	if (denom == 0.0 && (!isnan(d.r) || !isnan(d.i))) {
	    /* nonzero / zero */
	    res.r = copysign(INFINITY, s.r) * d.r;
	    res.i = copysign(INFINITY, s.r) * d.i;
	}else if ((isinf(d.r)  || isinf(d.i)) && isfinite(s.r) && isfinite(s.i)) {
	    /* infinite / finite */
	    d.r = copysign(isinf(d.r) ? 1 : 0, d.r);
	    d.i = copysign(isinf(d.r) ? 1 : 0, d.i);
	    res.r = INFINITY * (d.r*s.r + d.i*s.i);
	    res.i = INFINITY * (d.i*s.r - d.r*s.i);
	}else if ((isinf(s.r) || isinf(s.i)) && isfinite(d.r) && isfinite(d.i)) {
	    /* finite / infinite */
	    s.r = copysign(isinf(s.r) ? 1 : 0, s.r);
	    s.i = copysign(isinf(d.r) ? 1 : 0, s.i);
	    res.r = 0.0 * (d.r*s.r + d.i*s.i);
	    res.i = 0.0 * (d.i*s.r - d.r*s.i);
	}
    }
    return res;
}

#endif
