/*
    Header: ip/random.h
    Author: M. J. Cree

    (C) 2012, 2015, 2018 Michael J. Cree

    Functions for generating random numbers.
*/

#ifndef IP_RANDOM_H
#define IP_RANDOM_H

#include <stdlib.h>

#define RAND_MAXP1 ((double)RAND_MAX + 1.0)

/*
 * Random uniform deviate returning value between 0 (inclusive) and 1
 * (exclusive).
 */

static inline double ip_rand_uniform(void) {
    return (double)random() / RAND_MAXP1;
}


/*
 * Random Gaussian (normal) deviate with mean 0 and standard deviation of 1.
 */

extern double ip_rand_normal(void);


/*
 * Random number generation useful in the IP library test suite.
 */

/*
 * return random integers within range of the specified type with each
 * binade approximately equally likely (except for a few of the least
 * significant). This is useful for getting represention all of small,
 * medium and large random numbers appropriate to the type in the random
 * number generation.
 */

static inline int16_t ip_rand_wshort(void)
{
    int16_t num = (int)((unsigned int)random() / (unsigned int)((RAND_MAX+1ULL)>>16));
    unsigned int shift = random() / (RAND_MAX/12);
    return num >> shift;
}

static inline uint16_t ip_rand_wushort(void)
{
    unsigned int num = random() / (int)((RAND_MAX+1LL)>>16);
    unsigned int shift = random() / (RAND_MAX/12);
    return num >> shift;
}

static inline int32_t ip_rand_wlong(void)
{
#if RAND_MAX >= 1073741824
    int num = (int)((unsigned int)random() << 1);
#elif  RAND_MAX >= 536870912
    int num = (int)((unsigned int)random() << 2);
#else
#error "No support for such a small RAND_MAX."
#endif
    unsigned int shift = random() / (RAND_MAX/24);
    return num >> shift;
}

static inline uint32_t ip_rand_wulong(void)
{
#if RAND_MAX >= 1073741824
    unsigned int num = random() << 1;
#elif  RAND_MAX >= 536870912
    unsigned int num = random() << 2;
#else
#error "No support for such a small RAND_MAX."
#endif
    unsigned int shift = random() / (RAND_MAX/24);
    return num >> shift;
}



#endif
