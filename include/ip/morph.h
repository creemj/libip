/*
  Header: morph.h
  Author: M. J. Cree

  Copyright (C) 2015 Michael J. Cree

  Declarations for structuring elements and image morphology.

*/


#ifndef IP_MORPH_H
#define IP_MORPH_H

#include <ip/types.h>
#include <ip/image.h>

/*
 * The ip_chord and ip_strel definitions below should be considered private
 * to the library.  Use the functions defined below to manipulate them.
 */

/* A chord (contiguous straight line segment) in a structuring element. */
typedef struct {
    int x, y;			/* Position in structuing element; can be -ve */
    int len;			/* Length of the chord */
    int ulidx;			/* Index into the unique length array */
    void *ct_row;		/* Pointer to row in the chord table */
} ip_chord;


/*
 * Structuring element used for image morphology.
 *
 * The origin is always at location (0, 0).  The bbox origin defines the
 * top-left corner of the extent of the structuring element relative to the
 * origin. It can have negative or zero coordinates.  It is assumed that the
 * structuring element always includes the origin thus the bbox.origin
 * coordinates cannot be both positive and non-zero.
 *
 * The structuring element is broken down into its chords, either by using
 * horizontal chords, or by using vertical chords, whichever produces the
 * fewest chords.
 *
 * The unique chord lengths are then recorded in the unique chord length
 * array with extra chord lengths inserted to ensure that the jth chord
 * length in the array is not longer than 2*((j-1)th chord length) in the
 * array.
 */
typedef struct {
    ip_box bbox;	   /* Extent of the structuring element relative to origin */
    int direction;	   /* 0 = horizontal chords, 1 = vertical chords */
    int rectangular;	   /* True if a rectangular structuring element */
    int num_chords;	   /* number of chords in structuring element */
    ip_chord *chords;	   /* array of chords defining structuing element */
    int num_unique;	   /* The number of unique chord lengths */
    int *ulength;    /* Sorted and enhanced array of unique chord lengths */
} ip_strel;


/* The possible structuring element types to use in ip_alloc_strel() */
#define IP_STREL_LINE      1
#define IP_STREL_SQUARE    2
#define IP_STREL_RECTANGLE 3
#define IP_STREL_DISC      4
#define IP_STREL_DIAMOND   5
#define IP_STREL_IMAGE     6
#define IP_STREL_FOUR_NEIGHBOURS 7
#define IP_STREL_EIGHT_NEIGHBOURS 8

#define IP_STREL_N4 IP_STREL_FOUR_NEIGHBOURS
#define IP_STREL_N8 IP_STREL_EIGHT_NEIGHBOURS

#define FLG_BITS_STREL 0x0f

/* Flags for use in im_reconstruct() */
#define FLG_RECON_FROM_ABOVE 0x100

/*
 * Functions to manipulate structuring elements.
 */

extern ip_strel *ip_alloc_strel(int type, ...);
extern void ip_free_strel(ip_strel *se);

extern int ip_strel_set_origin(ip_strel *se, ip_coord2d origin);
int ip_strel_shift_origin(ip_strel *se, ip_coord2d shift);
extern void ip_strel_reflect(ip_strel *se);
extern void ip_strel_dump(FILE *f, ip_strel *se);


/*
 * Morphology with arbitrary flat structuring elements.
 */
extern int im_dilate(ip_image *im, ip_strel *se);
extern int im_erode(ip_image *im, ip_strel *se);
extern int im_open(ip_image *im, ip_strel *se);
extern int im_close(ip_image *im, ip_strel *se);

/*
 * Specific morphological algorithms called by the general purpose entry
 * points above.  For normal use one should be calling im_dilate(), etc.
 */
extern int im_morph1d_horiz_erode(ip_image *im, int se_len, int se_origin);
extern int im_morph1d_vert_erode(ip_image *im, int se_len, int se_origin);
extern int im_morph1d_horiz_dilate(ip_image *im, int se_len, int se_origin);
extern int im_morph1d_vert_dilate(ip_image *im, int se_len, int se_origin);
extern int im_morph_chord_dilate(ip_image *im, ip_strel *se);
extern int im_morph_chord_erode(ip_image *im, ip_strel *se);

/* Morphological Reconstruction */
int im_reconstruct(ip_image *marker, ip_image *mask, int flag);

#if 0
/* For testing purposes only; not normally compiled in library */
extern int im_morph1d_horiz_recur_erode(ip_image *im, int se_len, int se_origin);
extern int im_morph1d_vert_recur_erode(ip_image *im, int se_len, int se_origin);
#endif

#endif
