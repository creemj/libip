/*
   Header: ip/raw.h
   Author: M. J. Cree

   Copyright (C) 1995-2007, 2014 by Michael J. Cree

   Declarations for interfacing (reading/writing) to raw images.

*/

#ifndef IP_RAW_H
#define IP_RAW_H

#include <ip/types.h>
#include <ip/image.h>

/* Entry points to functions. */

extern ip_image *read_raw_imagex(const char *fname,
				 int type, int Xsize, int Ysize,
				 long headskip, long rowskip,
				 long itemskip, int flag );
extern int write_raw_image(const char *fname, ip_image *im);

/* Useful constructions */

static inline ip_image *read_raw_image(const char *fname,
				       int type, int Xsize, int Ysize)
{
    return read_raw_imagex(fname,type,Xsize,Ysize,0,0,0,NO_FLAG);
}

#endif
