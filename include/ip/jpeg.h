/* $Id:$ */
/*
   Header: ip/jpeg.h
   Author: M.J.Cree

   (C) 2005-2007 Michael Cree

   Declarations for interfacing (reading/writing) to JPEG images.

*/

#ifndef IP_JPEG_H
#define IP_JPEG_H

#include <ip/types.h>
#include <ip/image.h>
#include <stdio.h>

#ifdef IPINTERNAL
#include <jpeglib.h>
#endif

/* for handling jpeglib generated errors internally */
#ifdef IPINTERNAL
#include <setjmp.h>
typedef struct {
   struct jpeg_error_mgr pub;
   jmp_buf sj_buffer;
} ip_jpeg_err;
#endif

/* JPEG image handle */

typedef struct {
   int type;			/* ip library image type */
   ip_coord2d size;		/* Image size */
   FILE *f;			/* file handle */
   const char * fname;		/* Pointer to filename */
   int init;			/* TRUE if dcinfo or cinfo allocated */
#ifdef IPINTERNAL
   struct jpeg_decompress_struct *dcinfo; /* JPEG structure pointer */
   struct jpeg_compress_struct *cinfo;	/* JPEG structure pointer */
   ip_jpeg_err *jerr;		/* JPEG error manager */
#else
   void * dcinfo;		/* JPEG structure pointer */
   void * cinfo;		/* JPEG structure pointer */
   void * jerr;
#endif
} ip_jpeg_handle;


/* Entry points to functions. */

extern ip_jpeg_handle *open_jpeg_image( const char *fname );
extern ip_jpeg_handle *open_jpeg_imagex(FILE *f, const char *fname,
				  unsigned char hdr[4], int flag);
extern ip_image *read_jpeg_image(ip_jpeg_handle *);
extern ip_image *read_jpeg_imagex(ip_jpeg_handle *th, int first_row, int nrows);
extern int read_jpeg_tag(ip_jpeg_handle *, int tag, ...);
extern void close_jpeg_image(ip_jpeg_handle *);

#if 0
/* Not implemented */
extern int write_jpeg_image(const char *fname, ip_image *im);
extern int write_jpeg_imagex(const char *fname, ip_image *im, int flag);
#endif

#endif
