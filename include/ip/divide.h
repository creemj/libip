/*
 * Header: ip/divide.h
 * Author: Michael J. Cree
 *
 * Integer division routines.
 *
 * Copyright (C) 2016-2017 by Michael J. Cree
 */

#ifndef IP_DIVIDE_H
#define IP_DIVIDE_H


/*
 * Having a count leading zeros instruction is useful.
 */

static inline int ip_clz(int x)
{
    return __builtin_clz(x);
}


/*
 * Signed and Unsigned integer multiply high word.  These functions multiply
 * two 32-bit integers to give a 64-bit integer and return the high 32-bits.
 *
 * We hope the compiler is smart enough to reduce these to one CPU
 * instruction when such an instruction is available on the architecture.
 */

static inline int32_t ip_imulh32(int32_t a, int32_t b)
{
    return (int32_t)(((int64_t)a * (int64_t)b)>>32);
}

static inline uint32_t ip_umulh32(uint32_t a, uint32_t b)
{
    return (uint32_t)(((uint64_t)a * (uint64_t)b)>>32);
}


/*
 * Fused 32-bit integer multiply-add high
 * I.e., calculates 32-bit integer multiply and add at higher precision,
 * sufficient to ensure no loss of precision or carry propagation, and
 * return high 32-bits.
 *
 * On 64-bit arch this can be done efficiently.  On 32-bit arch this is only
 * efficient if the arch has a special instruction (which is unlikely unless
 * it has DSP support).
 */

static inline uint32_t ip_umaddh32(uint32_t a, uint32_t b, uint32_t c)
{
    return (uint32_t)(((uint64_t)a * (uint64_t)b + (uint64_t)c)>>32);
}

/*
 * Calculate unsigned integer reciprocal of d, i.e., the multiplier m to be
 * used to calculate floor(n/d) essentially by multiplying m by n for
 * unsigned integer n and d of size 32 bits.  Coupled with ip_udiv32_n32()
 * this is usually the most efficient way to calculate repeated unsigned
 * 32-bit integer divisions with the same divisor on 32-bit architectures.
 */

struct ip_uint32_rcp_n32 {
    uint32_t m;			/* the shift adjusted reciprocal */
    int shift;			/* the shift */
    int shift_1;		/* normally 1, but is 0 if d == 1 */
};


extern struct ip_uint32_rcp_n32 ip_urcp32_n32(uint32_t d);


/*
 * Calculate unsigned integer division n/d by multiplication by the integer
 * reciprocal calculated from d by ip_urcp32_n32().
 *
 * This implements the second half of the algorithm presented in Figure 4.1
 * of Granlund and Montgomery 1994.
 */

static inline uint32_t ip_udiv32_n32(uint32_t n, struct ip_uint32_rcp_n32 r)
{
    uint32_t t = ip_umulh32(r.m, n);
    return (t + ((n-t)>>r.shift_1)) >> r.shift;
}


/*
 * This version of udiv32() uses a preshift hard-coded to 1 thus valid only
 * for values of d > 1.  (On some architectures an immediate argument for
 * the shift amount is more efficient, hence this optimisation.)
 */

static inline uint32_t ip_udiv32q_n32(uint32_t n, struct ip_uint32_rcp_n32 r)
{
    uint32_t t = ip_umulh32(r.m, n);
    return (t + ((n-t)>>1)) >> r.shift;
}


/*
 * Integer division for signed 32 bit integers
 */

struct ip_int32_rcp {
    int32_t m;
    int shift;
    int dsign;
};

extern struct ip_int32_rcp ip_ircp32(int32_t d);


/*
 * Calculate signed integer division n/d by multiplication with precomputed
 * reciprocal r of d (i.e. r is pre-computed with ip_ircp32()).
 *
 * This implements the second half of the algorithm presented in Figure 5.1
 * of Granlund and Montgomery 1994.
 */

static inline int32_t ip_idiv32(int32_t n, struct ip_int32_rcp r)
{
    int32_t q0 = n + ip_imulh32(r.m, n);
    q0 = (q0 >> r.shift) - (n >> 31);
    return (q0 ^ r.dsign) - r.dsign;
}


/*
 * The same as ip_idiv32() except that it is known that the common divisor
 * is positive: it is only the dividend that can be negative.  This enables
 * a small optimisation in calculation.
 */

static inline int32_t ip_idiv32u(int32_t n, struct ip_int32_rcp r)
{
    int32_t q0 = n + ip_imulh32(r.m, n);
    return (q0 >> r.shift) - (n >> 31);
}


/*
 * This implements Robison's 2005 improved unsigned 32-bit integer division
 * routine that is more efficient if the architecture can perform a fused
 * integer muliply-add, i.e., the muliply-add is calculated at greater than
 * 32-bit precision and only the final result is rounded to the upper
 * 32-bits.
 *
 * This routine is to be preferred over ip_urcp32_n32()/ip_udiv32_n32() on
 * 64-bit architectures.
 */

struct ip_uint32_rcp_n33 {
    uint32_t m;			/* multiplier being reciprocal of divisor */
    uint32_t b;			/* is zero or m */
    int shift;			/* the shift */
};

extern struct ip_uint32_rcp_n33 ip_urcp32_n33(uint32_t d);

static inline uint32_t ip_udiv32_n33(uint32_t n, struct ip_uint32_rcp_n33 r)
{
    return ip_umaddh32(r.m, n, r.b) >> r.shift;
}


/*
 * Choose the best unsigned division routine.
 *
 * Currently if have a 64-bit arch we assume that ip_udiv32_n33() is likely
 * to be an advantage.  (It is on X86_64, Arm64 and Alpha tested.)
 */

#ifdef HAVE_64BIT
#define ip_uint32_rcp ip_uint32_rcp_n33
#define ip_urcp32(d) ip_urcp32_n33(d)
#define ip_udiv32(n, r) ip_udiv32_n33((n), (r))
#define ip_udiv32q(n, r) ip_udiv32_n33((n), (r))
#else
#define ip_uint32_rcp ip_uint32_rcp_n32
#define ip_urcp32(d) ip_urcp32_n32(d)
#define ip_udiv32(n, r) ip_udiv32_n32((n), (r))
#define ip_udiv32q(n, r) ip_udiv32q_n32((n), (r))
#endif


/*
 * Provide 16-bit reciprocals.  These are used by the SIMD code.
 *
 * If only simd_mulhi_USHORT() is available the simd arch headers will
 * define simd_udiv_USHORT() to use struct ip_uint16_rcp_n16() as the
 * reciprocal and simd_mulhi_USHORT() to calculate the division.
 */


struct ip_uint16_rcp_n16 {
    uint16_t m;			/* the shift adjusted reciprocal */
    int shift;			/* the shift */
    int shift_1;		/* normally 1, but is 0 if d == 1 */
};

extern struct ip_uint16_rcp_n16 ip_urcp16_n16(uint16_t d);

struct ip_uint16_rcp_n17 {
    uint16_t m;			/* multiplier being reciprocal of divisor */
    uint16_t b;			/* is zero or m */
    int shift;			/* the shift */
};

extern struct ip_uint16_rcp_n17 ip_urcp16_n17(uint16_t d);

struct ip_int16_rcp {
    int16_t m;
    int16_t dsign;
    int shift;
};

extern struct ip_int16_rcp ip_ircp16(int16_t d);

#endif
