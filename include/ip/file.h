/*
  Header: file.h
  Author: M. J. Cree

  (C) 2007 Michael Cree

  General purpose file handling routines.

*/

#ifndef IP_FILE_H
#define IP_FILE_H

extern const char *ip_msg_stdin;
extern const char *ip_msg_stdout;
extern const char *ip_msg_filetype[];

/* General file handling routines. */
extern int ip_filetype(const char *fn);
extern int ip_filetypex(unsigned char header[4]);
extern int ip_read_filehdr(const char *fname,FILE **f,unsigned char header[4]);
extern ip_image *ip_read_image(const char *fname, ...);

#endif
