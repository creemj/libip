/*
   Header: ip/error.h
   Author: M.J.Cree

   Copyright (C) 1995-2007, 2014 Michael J. Cree

   Error routine definitions for IP library.
*/

#ifndef IP_ERROR_H
#define IP_ERROR_H

#include <stdarg.h>

/* The stack containing the list of routines entered */

#define MAX_LOG 10
typedef struct {
    int index;
    const char *routine[MAX_LOG];
} ip_error_logstack;

/* Many routines save any error conditions here. */

#ifndef IPERROR_INTERNAL
extern int ip_error;
#endif

/* Use ip_debug_messages() to set/clear this. */

extern int ip_verbose;

/* The error codes that the IP library can produce. */

#define OK 0
#define ERR_OUT_OF_MEM 1
#define ERR_NOT_IMPLEMENTED 2
#define ERR_TOO_DEEP_LOG 3
#define ERR_POPPED_TOO_MANY 4
#define ERR_ALGORITHM_FAULT 5
#define ERR_BAD_MEM_FREE 6
#define ERR_OUT_OF_CONTROL 7
#define ERR_MATHS_EXCEPTION 8
#define ERR_NR_FAULT 9
#define ERR_INVALID_IMAGE 10

#define ERR_INVALID_IMAGE_TYPE 20
#define ERR_INVALID_IMAGE_SIZE 21

#define LOWER_FILE_ERR_LIMIT 30

#define ERR_FILE_OPEN	30
#define ERR_SHORT_FILE	31
#define ERR_FILEIO	32
#define ERR_FILEIO_OR_TOO_SHORT	33
#define ERR_FILE_CLOSE 34

#define ERRNO_LIMIT 34

#define ERR_NO_STDIN   35
#define ERR_NO_STDOUT  36
#define ERR_BAD_DATABASE 38

#define ERR_NOT_VISILOG 40
#define ERR_INVALID_VISILOG_TYPE 41
#define ERR_UNSUPPORTED_VISILOG_WRITE 42

#define ERR_NOT_CIMAGES 45
#define ERR_INVALID_CIMAGES_TYPE 46
#define ERR_UNSUPPORTED_CIMAGES_WRITE 47

#define ERR_NOT_TIFF 50
#define ERR_INVALID_TIFF_TYPE 51
#define ERR_UNSUPPORTED_TIFF_WRITE 52
#define ERR_TIFF 53
#define ERR_BAD_TIFF_FILE 54

#define ERR_NOT_SUNRASTER 60
#define ERR_INVALID_SUNRASTER_TYPE 61
#define ERR_UNSUPPORTED_SUNRASTER_WRITE 62
#define ERR_BAD_SUNRASTER_FILE 63

#define ERR_NOT_PNG 70
#define ERR_INVALID_PNG_TYPE 71
#define ERR_UNSUPPORTED_PNG_WRITE 72
#define ERR_PNG 73
#define ERR_BAD_PNG_FILE 74

#define ERR_NOT_JPEG 80
#define ERR_INVALID_JPEG_TYPE 81
#define ERR_JPEG 83
#define ERR_BAD_JPEG_FILE 84


#define HIGHER_FILE_ERR_LIMIT 90

#define ERR_UNSUPPORTED_FILETYPE 91
#define ERR_WRONG_IMAGE_TYPE 92

#define ERR_UNSUPPORTED_TYPE 100
#define ERR_SAME_TYPE 101
#define ERR_BAD_CONVERSION 102
#define ERR_BAD_FLAG 103
#define ERR_BAD_TYPE 104

#define ERR_BAD_FLAG2 105

#define ERR_BAD_PARAMETER 106

#define ERR_PARM_BAD 110
#define ERR_PARM_BAD_VALUE 111
#define ERR_PARM_NOT_MULT_2 112
#define ERR_PARM_NOT_POWER_2 113
#define ERR_PARM_NOT_ODD 114
#define ERR_PARM_OOR 115
#define ERR_PARM_TOOSMALL 116
#define ERR_PARM_TOOBIG 117
#define ERR_PARM_BAD_COORD 118
#define ERR_PARM_BAD_BOX 119
#define ERR_PARM_BAD_ROI 120
#define ERR_PARM_NOT_IN_BOX 121
#define ERR_PARM_NULL 122


#define ERR_NOT_SAME_TYPE 130
#define ERR_NOT_SAME_SIZE 131
#define ERR_BAD_ACTION 132
#define ERR_BAD_TAG 133
#define ERR_BAD_BOX 134
#define ERR_BAD_COORD 135
#define ERR_BAD_ROWADR 136
#define ERR_BAD_VALUE  137
#define ERR_BAD_ROI    138

#define ERR_RLE_ARRAY_OVERFLOW 200
#define ERR_OBJ_INVALID_ROW 201
#define ERR_SPLIT_OBJECT 202
#define ERR_BADLY_FORMED_OBJECT 203

#define ERR_CLSFY_STACK_OVERFLOW 250

#define ERR_BAD_ROI_TYPE 300
#define ERR_BAD_ROI_ACTION 301

#define ERR_SVD_NO_CONVERGENCE 350
#define ERR_POLY_NOT_SAME_ORIGIN 360
#define ERR_POLY_NOT_SAME_DIMENSION 361

#define ERR_INVALID_KERNEL 400

#define ERR_FTLIB_ERROR 900
#define ERR_FTLIB_INIT 901
#define ERR_FTLIB_FONT_NOT_FOUND 902
#define ERR_FTLIB_UNKNOWN_FORMAT 903

#define ERR_GSLLIB 920

#define ERR_OPEN_DISPLAY_FAIL 1000
#define ERR_DISPLAY_NOT_OPEN 1001
#define ERR_DISPLAY_CANVAS 1002
#define ERR_DISPLAY_IMAGE  1003
#define ERR_DISPLAY_UPDATE 1004
#define ERR_DISPLAY_CLEAR  1005
#define ERR_DISPLAY_LABEL  1006
#define ERR_DISPLAY_MOUSE  1007
#define ERR_DISPLAY_FREE   1008
#define ERR_DISPLAY_OPENED 1009

#define ERR_DISPLAY_BAD_ID 1010
#define ERR_DISPLAY_BAD_TYPE 1011
#define ERR_DISPLAY_BAD_UPDATE 1012

#define ERR_UNKNOWN_PALETTE 1013
#define ERR_PALETTE 1014
#define ERR_UNAVAILABLE_PEN 1015


#define ERR_SIGNAL 10000
#define ERR_SIGFPE 10001

#define ERR_PPC_ALTIVEC 11001
#define ERR_OSX_VIMAGE 11002
#define ERR_OSX_VDSP 11003

#define IMPOSSIBLE_ERROR -1


/* Callable routines */

extern void ip_register_error_handler(void (*f)(ip_error_logstack *, int, const char *, va_list));
extern void ip_deregister_error_handler(void);

extern void ip_debug_messages(int on_off);

extern void ip_print_error(const char *prg, ip_error_logstack *ipr, int error, const char *extinfo, va_list ap);

/* For logging errors */
extern void ip_log_routine(const char *name);
extern void ip_pop_routine(void);
extern void ip_reset_routine_stack(void);
extern int ip_log_error(int error_code, ...);
extern void ip_printlog(void);

/* For installing error handler to catch GSL errors */
extern void ip_register_gsl_error_handler(void);

/* For interpreting error codes */
extern const char *ip_error_code_as_str(int error_code);
extern const char *ip_error_code_msg(int error_code);


#endif
