/*
   Header: ip/object.h
   Author: M.J.Cree

   Copyright (C) 1995-1997, 2002, 2018 Michael J. Cree.

   Definitions for use for managing objects.
*/

#ifndef IP_OBJECT_H
#define IP_OBJECT_H

#include <ip/image.h>


/*
 * Internal object structures.
 * Don't assume these two structures will remain same through versions.
 * Therefore treat them as ghosts - they don't exist, except internally
 * to ip routines.
 */


/* The runlength encoding for one row. */

typedef struct {
   short int start;
   short int length;
} rletype;

/* The row list within the object */

typedef struct {
   short int allocated;
   short int numentries;
   rletype *rle;
} rowtype;



/*
 * An object in the object list.
 *
 * This is the structure returned by object creation routines. Note that
 * the calling program may access some fields, which are guaranteed to
 * remain same through any version.
 */


struct  ip_object_struct {
    /*
     * The four following fields are open to calling programs for reading. In
     * general, they should never be directly modified (it's safe to modify
     * 'userdata' and 'userptr' which are provided specifically for the
     * calling program to be able to attach some user defined data to the
     * object).  Don't ever modify 'origin' or any of the private fields
     * defined below, because unpredictable, or even catastrophic, results
     * may follow.  'next' is best modified by using ip object
     * insertion/removal routines, however, it can be modified directly, if
     * so desired. Do not ever modify origin.
     */

    struct ip_object_struct *next;	/* Next object in list. */
    unsigned long userdata;	/* For the caller programme to use */
    void *userptr;		/* For the caller programme to use */
    ip_coord2d origin;		/* origin: top row, then left most point */

    /*
     * The following fields are private to ip routines - calling programs
     * must never change these fields and cannot depend on them to contain
     * any particular value. They may change with new versions of ip.  Use
     * routines in measures.c to read any of these fields.
     */

    ip_obox bbox;			/* Bounding box - still in obox type :-( */
    int numpixels;		/* Number of pixels ( == area ) */
    unsigned long status;	/* See intobject.c for defns of status */
    int toprow;			/* toprow and numrows specify row */
    int numrows;			/*   structure, also depn. on status. */
    rowtype *row;		/* Pointer to array of rows in object */
};

typedef struct ip_object_struct ip_object;

/* Object list header */

typedef struct {
    ip_image *image;
    int numobjs;
    ip_object *firstobj;
} ip_objlist;


/* Actions for the object_get() routine. */

enum {
   OBJ_ORIGIN = 0,
   OBJ_AREA,
   OBJ_BBOX,
   OBJ_COA,
   OBJ_LENGTH,
   OBJ_BREADTH,
   OBJ_PERIMETER,
   OBJ_MAX,
   OBJ_MIN,
   OBJ_MAXPT,
   OBJ_MINPT,
   OBJ_INTEGRAL,
   OBJ_VARIANCE,
   OBJ_BNDMEAN,
   OBJ_OVERLAP,
   OBJ_FARPOINT
};

/*
 *  Routines available to the calling program.
 */


/* In measures.c */

/* See above for valid actions for object_get()*/
extern ip_datum ip_object_get(int action, ip_object *, ...);

extern double ip_measure_object_area(ip_object *o);
extern ip_box ip_measure_object_bbox(ip_object *o);

extern ip_vector2d ip_measure_object_coa(ip_object *o);
extern double ip_measure_object_length(ip_object *o, ip_vector2d pt, 
				       ip_coord2d *pt1, ip_coord2d *pt2);
extern double ip_measure_object_breadth(ip_object *obj, ip_coord2d pt1, ip_coord2d pt2);
double ip_measure_object_perimeter(ip_object *obj);

extern ip_coord2d ip_measure_object_max_point(ip_image *, ip_object *);
extern double ip_measure_object_max(ip_image *, ip_object *, ip_coord2d *);
extern ip_coord2d ip_measure_object_min_point(ip_image *, ip_object *);
extern double ip_measure_object_min(ip_image *, ip_object *, ip_coord2d *);

extern double ip_measure_object_integral(ip_image *, ip_object *);
extern ip_drgba ip_measure_object_integral_rgb(ip_image *, ip_object *);
extern double ip_measure_object_variance(ip_image *, ip_object *);
extern ip_drgba ip_measure_object_variance_rgb(ip_image *im, ip_object *obj);
extern double ip_measure_boundary_mean(ip_image *im, ip_object *obj);

extern double ip_measure_object_overlap(ip_object *ref, ip_object *other);

extern ip_coord2d ip_measure_object_farpoint(ip_object *o, ip_coord2d x);

/* In object.c */
extern ip_coord2d ip_object_coord(ip_object *o, ip_coord2d x);
extern ip_coord2d ip_image_coord(ip_object *o, ip_coord2d x);

extern int ip_set_object_into_image(ip_image *im, ip_object *obj, double val);
extern int ip_set_object_into_imagex(ip_image *im, ip_object *obj, 
				  double val1, double val2, double val3);
extern int ip_mask_object_into_image(ip_image *im, ip_object *obj, int immin, int immax, int objmin, int objmax);
int ip_point_in_object(ip_object *o, ip_coord2d pt);

extern ip_objlist *ip_alloc_object_list(ip_image *im);
extern ip_object *ip_alloc_object(ip_coord2d origin);
extern void ip_free_object(ip_object *);
extern void ip_free_object_list(ip_objlist *);
extern void ip_delete_object(ip_objlist *ol, ip_object *obj);
extern ip_object *ip_remove_object(ip_objlist *ol, ip_object *obj);
extern ip_object *ip_insert_object(ip_objlist *ol, ip_object *pos, ip_object *obj);
extern ip_object *ip_append_object(ip_objlist *ol, ip_object *obj);
extern ip_object *ip_copy_object(ip_object *srcobj);
extern ip_objlist *ip_copy_object_list(ip_objlist *olist);

extern void ip_dump_object(FILE *f, ip_object *, int flags);
extern void ip_dump_object_list(FILE *f, ip_objlist *ol, int flags);

extern int ip_object_set(unsigned int action, ip_object *o, ...);
extern int ip_object_modify(unsigned int action, ip_object *o, ...);
/* object_get() is in measures.c */

/* In objgrow.c */
extern ip_objlist *im_create_object_list(ip_image *im,
					 int (*in_object)(ip_image *, void *rowptr, int idx),
					 int flags);

#if 0
/* Was in trackobj.c */
extern ip_objlist *im_create_object_list(ip_image *im);
extern ip_object *track_object(ip_image *im, ip_coord2d seed);

/* Was in reggrow.c */
extern ip_object *region_grow(ip_image *im, int (*in_region)(ip_image *i, ip_coord2d pt), ip_coord2d seed);

/* Was in dispobj.c */
extern int display_objlist_over_image(int id, ip_image *, ip_objlist *, int pen, int flag);
extern int display_object_over_image(int id, ip_image *, ip_object *, int pen, int flag);
#endif

#endif
