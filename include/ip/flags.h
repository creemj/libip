/*
   Header: ip/flags.h
   Author: M. J. Cree

   Copyright (C) 1995-1997, 1999-2002, 2005, 2007, 2013-2014 Michael J. Cree

   All the flag definitions for a variety of image processing routines.

*/

#ifndef IP_FLAGS_H
#define IP_FLAGS_H

/* When using flag_ok() one must terminate list with LASTFLAG
   Only to be used internally to ip library */
#define LASTFLAG -1

/* To fill flag parameter with a no operation */

#define NO_FLAG 0
#define FLG_NONE NO_FLAG

/* Says that more flags (called tags) and parameters (a tag list)
   follow after the flag.  Routines that use this have a variable
   argument description.  Usually terminate tag list with TAG_DONE.
   N.B. No other flag sets this bit. */

#define FLG_TAGLIST 0x40000000

/* Some routines take this flag as an indicator that is OK to use hardware
   accelerators (e.g. altivec on powerpc) should they be available.
   N.B. No other flag sets this bit.  */

#define FLG_ACCELERATE 0x20000000

/* Returned by file_type() routine */

#define FT_ERROR   -1
#define FT_UNKNOWN 0
#define FT_VISILOG 1
#define FT_CIMAGES 2
#define FT_TIFF    3
#define FT_PPM     4
#define FT_VIFF    5
#define FT_SUNRASTER 6
#define FT_PNG     7
#define FT_JPEG    8
#define FT_BMP     9
#define FT_GIF    10
#define FT_RAW    11
/* If add more filetypes, remember to insert a string into message.c */


/* For im_load() */
#define FLG_COPYARRAY 1

/*** Image conversion flags only occupy these bits ***/

#define FLGPRT_CNVRT    0x0f

/* For use with conversion from/to complex image types (also used by
   binary arithmetic and im_mask()). */
#define FLG_REAL	0x01
#define FLG_IMAG	0x02
#define FLG_MAG		0x03
#define FLG_ANGLE	0x04

/* For use with conversion from BINARY images */
#define FLG_B1		0x05
#define FLG_B255	0x06

/* For use with conversion from/to RGB images */
#define FLG_RED		0x07
#define FLG_GREEN	0x08
#define FLG_BLUE	0x09
/*      FLG_MAG         defined above */
#define FLG_MAGYUV      0x0a
#define FLG_GREY	FLG_MAGYUV

/*** Setting image values with im_set() ***/
/* These flags must be ORable with FLG_CLIP and FLG_ZEROREST */

#define FLG_SREAL  0x01
#define FLG_SIMAG  0x02
#define FLG_SRED   0x04
#define FLG_SGREEN 0x08
#define FLG_SBLUE  0x10
#define FLG_SALPHA 0x20
#define FLG_ZEROREST 0x1000
#define FLGPRT_COLOUR (FLG_SRED|FLG_SBLUE|FLG_SGREEN|FLG_SALPHA)

/*** Casting/clipping flags only involve these bits. ***/

/* FLG_CLIP is also used by im_create() and im_addc() and siblings so must
   be careful to make sure these bits don't colide with FLGPRT_TESTIM, nor
   with FLG_TRANSPOSE nor with FLG_SREAL, etc., above */

#define FLGPRT_CLIP     0x300

/* For use with conversion involving downcasting. */
#define FLG_CLIP	0x100
#define FLG_ROUND	0x200

/* Those bits not used by im_convert() */

#define FLGPRT_CNVRT_NOTUSED    (~(FLGPRT_CLIP|FLGPRT_CNVRT))

/* Used by im_div()/im_divc() */
#define FLG_PROTECT 0x10000
#define FLG_INTDIV 0x20000
#define FLG_FASTFLOAT 0x40000

/*** For use with write_*_imagex() routines. ***/
#define FLG_BINARY 7
#define FLG_BYTE 0

/*** For use with im_create() ***/
#define FLGPRT_TESTIM 0xff
#define TI_CONST 1
#define TI_VWEDGE 2
#define TI_HWEDGE 3
#define TI_CONE 4
#define TI_GAUSSIAN 5

#define TI_WDWCOSINE 0x80
#define TI_WDWBUTTW1 0x81

#define FLG_TRANSPOSE 0x8000

#define FLGPRT_TESTIM_UNUSED (~(FLG_TRANSPOSE|FLGPRT_TESTIM|FLG_CLIP))
/* im_create also uses FLG_CLIP. */

/* For use with im_random() */
#define FLG_RANDOM_UNIFORM 0x10
#define FLG_RANDOM_NORMAL 0x20


/*** For use with im_reduce(), im_zoom() and im_warp() ***/
#define FLG_AVERAGE 1
#define FLG_SAMPLE 2
#define FLG_ZOOM 3
#define FLG_INTERP 4
#define FLG_NEAREST 5
#define FLG_BILINEAR 6
#define FLG_BSPLINE_0 10
#define FLG_BSPLINE_1 11
#define FLG_BSPLINE_2 12
#define FLG_BSPLINE_3 13
#define FLG_BSPLINE_4 14
#define FLG_BSPLINE_5 15
#define FLG_BSPLINE_6 16
#define FLG_BSPLINE_7 17
#define FLG_BSPLINE_8 18
#define FLG_BSPLINE_9 19

/* For use with im_flip() */

#define FLG_HORIZ 0
#define FLG_VERT  1

/* For use with im_register() */
#define FLG_ROTTRANS 1
/* FLG_TRANS defined with value 2 below (object display). */

/*** For use with various filtering functions. ***/
#define FLG_COPYBORDER  0x00	/* Filter that part of image kernel fits in completely; rest unchanged */
#define FLG_ZEROBORDER  0x01	/* Filter that part of image kernel fits in completely; rest zeroed */
#define FLG_SUBEXTEND   0x08	/* Not too sure what this was meant to be! */
#define FLG_ZEROEXTEND  0x0a	/* Filter to edge of image; image surrounded by zeroes */
#define FLG_EDGEEXTEND  0x0b	/* Filter to edge of image; image surrounded by replicated edge pixel */
#define FLG_WRAPEXTEND  0x0c	/* Filter to edge of image; image edge wraps to opposite edge */

#define FLG_NORM   0x40		/* Normalise convolution kernel */

/* #define FLGMSK_BORDER 0x07 */

/* Direction of operator in sobel, etc. filter */
#define FLG_FHORIZ 0x10
#define FLG_FVERT  0x20
#define FLG_FMAG   0x30

/* Filter processing operator in RGB images */
#define FILT_ANNF   0x10
#define FILT_ANNGF  0x20
#define FILT_ANNGF2 0x30
#define FILT_FVF_W1 0x40
#define FILT_FVF_W2 0x50
#define FILT_FVF_W3 0x60

#define FLGMSK_FILTER 0xf0

/* Vector distance measure in RGB image filters
   L1 = sum (abs(differences))
   L2 = euclidean distance (ie. sqrt( sum (differences^2)))
*/
#define DIST_L1      0x100
#define DIST_L2      0x200
#define DIST_ANGLE   0x300
#define DIST_L1L2    0x400
#define DIST_L1ANGLE 0x500
#define DIST_L2ANGLE 0x600
#define DIST_SIMUL   0x700
#define DIST_MINE1   0x800
#define DIST_MINE2   0x900

#define FLGMSK_DIST  0xf00

#define FLG_VMF     (DIST_L1)
#define FLG_BVDF    (DIST_ANGLE)
#define FLG_VANNF   (FLG_ANNF  | DIST_ANGLE)
#define FLG_VANNMF  (FLG_ANNF  | DIST_SIMUL)

#define FLGMSK_CBIAS 0xf000

#define FLG_CBIAS_1  0x1000
#define FLG_CBIAS_2  0x2000
#define FLG_CBIAS_3  0x3000

/* For use with hist_peak() */
#define FLG_SKIPZERO 0x01
#define FLG_SKIP255  0x02

/* For use with im_mask() (can't hit im_convert()'s FLGPRT_CONVERT) */
#define FLG_INVERSE 0x100

/* For use with im_evenlines() and im_oddlines() */
#define FLG_DOUBLE 1

/* For use with im_addnoise() */
#define FLG_GAUSSIAN 0x01
#define FLG_LINEAR   0x02

#define FLGPRT_NOISE 0xff
#define FLGPRT_NOISE_NOTUSED (~FLGPRT_NOISE)

/* For use with im_form_rgb() */
#define FLG_RGB8     0x00
#define FLG_RGB16    0x01

/*** For defining/using region-of-interests ***/
/* For use with alloc_roi() */
#define ROI_IMAGE 1
#define ROI_RECTANGLE 2
#define ROI_RECTOS 3
/* For use with roi_set(), etc. */
#define ROI_ORIGIN 0
#define ROI_SIZE 1
#define ROI_BOTRIGHT 2
/* Only valid with roi_modify() */
#define ROI_INCREASE 3
#define ROI_REDUCE 4

/*** For use with graphics routines ***/
#define FLG_FILLIN 1

/*** For use with display routines ***/

/* In display_init() */
#define FLG_DRAGABLE 1

/* Actions for use in display_set() */

#define DSP_TITLE      1
#define DSP_SIZE       2
#define DSP_MAXSIZE    3
#define DSP_DRAGABLE   4
#define DSP_PALENTRIES 5

/* In display_imagex() (in ID field) */
#define FLG_NEWIMAGE 0

/* For display_clear() */
#define FLG_CANVAS  1
#define FLG_IMAGE   2
#define FLG_OVERLAY 3

/* In mouse routines */

#define FLG_NOWAIT 0x00
#define FLG_WAIT   0x01

#define FLG_MOUSELEFT  0x10
#define FLG_MOUSERIGHT  0x20
#define FLG_MOUSEMIDDLE  0x40

/* Predefined palettes */
#define PAL_START	0
#define PAL_GREY	0
#define PAL_HOTBODY	1
#define PAL_GREYOBJ	2
#define PAL_MRI		3
#define PAL_GEOGRAPHIC	4
#define PAL_HBEQUAL	5
#define PAL_BGR		6
#define PAL_OMR		7
#define PAL_RGB		8
#define PAL_END		PAL_RGB

/* OR this with PAL_* selections to gamma correct for display screen */
#define PAL_GAMCORR 0x8000

/* The OPTIMUM palette to be used in load_palette() */
#define PAL_OPTIMUM 0

/* Overlay mask predefined colours */
#define OVL_START   0
#define OVL_BLACK   0
#define OVL_WHITE   1
#define OVL_RED     2
#define OVL_GREEN   3
#define OVL_BLUE    4
#define OVL_YELLOW  5
#define OVL_CYAN    6
#define OVL_MAGENTA 7
#define OVL_END     OVL_MAGENTA

/* Pens out of colour palette */
#define PEN_BLACK display_pen(OVL_BLACK)
#define PEN_WHITE display_pen(OVL_WHITE)
#define PEN_RED   display_pen(OVL_RED)
#define PEN_GREEN display_pen(OVL_GREEN)
#define PEN_BLUE  display_pen(OVL_BLUE)
#define PEN_YELLOW display_pen(OVL_YELLOW)
#define PEN_CYAN  display_pen(OVL_CYAN)
#define PEN_MAGENTA display_pen(OVL_MAGENTA)

/* Flags for object display */
#define FLG_OPAQUE 1
#define FLG_TRANS  2
#define FLG_CLEAR  0
#define FLG_BRTTRANS 3
/* FLG_TRANS also used by im_register() */

#define FLG_FULLIM 0x10
#define FLG_OBJECT 0x00


/*** For use with debug routines. ***/

#define DBG_STATS   0x01
#define DBG_VERBOSE 0x02
#define DBG_PICTORIAL 0x04

#endif

