/*
   Header: ip/proto.h
   Author: M.J.Cree

   (C) 1995-2007, 2018 Michael Cree

   Prototypes for most general purposed image processing functions in IP
   library.

*/

#ifndef IP_PROTO_H
#define IP_PROTO_H

#include <stdio.h>
#include <ip/image.h>

/* Image allocation/deallocation and im_set() defined in image.h */


/* Creating a test image. */
extern int im_create(ip_image *im, ip_coord2d origin, double scale, double offset, double spread, int type);
#define im_crtest(i,o,c,f,t) im_create(i,o,c,f,1.0,t)
extern int im_random(ip_image *im, double parm1, double parm2, int flag);

/* Image copying and type conversion */
extern ip_image *im_copy(ip_image *);
extern ip_image *im_convert(ip_image *, ip_image_type type, int flag);

/* Image transpose */
extern ip_image *im_transpose(ip_image *res, ip_image*src);

/* Image cutting, inserting and resizing. */
extern ip_image *im_cut(ip_image *, ip_coord2d origin, ip_coord2d size);
extern ip_image *im_sample(ip_image *, int factor);
extern ip_image *im_reduce(ip_image *, int factor, int flag);
extern ip_image *im_reducex(ip_image *, int xskip, int yskip, int flag);
extern ip_image *im_expand(ip_image *, int factor, int flag);
extern ip_image *im_expandx(ip_image * src, int xfactor, int yfactor, int flag );
extern ip_image *im_zoom(ip_image *, double xfactor, double yfactor, int flag);
extern int im_insert(ip_image *dest, ip_coord2d dorg,
		     ip_image *src, ip_coord2d sorg, ip_coord2d size);


/* Flipping/rotating (also see warping) images */
extern int im_flip(ip_image *im, int flag);
extern ip_image * im_rot90(ip_image *dest, ip_image *im, int flag);


/* Image statistics functions defined in stats.h */

/* Replacing a value in an image with another. */
extern int im_replace(ip_image *im, double new, double old);

/* Basic arithmetic between two images */
extern int im_add(ip_image *dest, ip_image *src, int flag);
extern int im_sub(ip_image *dest, ip_image *src, int flag);
extern int im_mul(ip_image *dest, ip_image *src, int flag);
extern int im_div(ip_image *dest, ip_image *src, int flag);

/* Basic arithmetic on an image with a constant */
extern int im_addc(ip_image *im, double val, int flag);
extern int im_addc_complex(ip_image *im, ip_dcomplex val, int flag);
extern int im_addc_rgb(ip_image *im,
		       double r, double g, double b, double a, int flag);

extern int im_subc(ip_image *im, double val, int flag);
extern int im_subc_complex(ip_image *im, ip_dcomplex val, int flag);
extern int im_subc_rgb(ip_image *im,
		       double r, double g, double b, double a, int flag);

extern int im_mulc(ip_image *im, double val, int flag);
extern int im_mulc_complex(ip_image *im, ip_dcomplex z, int flag);
extern int im_mulc_rgb(ip_image *im,
		       double r, double g, double b, double a, int flag);

extern int im_divc(ip_image *im, double val, int flag);
extern int im_divc_complex(ip_image *im, ip_dcomplex z, int flag);
static inline int
im_divc_rgb(ip_image *x, double r, double g, double b, double a, int f)
{
    return im_mulc_rgb(x, 1.0/r, 1.0/g, 1.0/b, 1.0/a, f);
}

/* Logical operators on images */
extern int im_or(ip_image *dest, ip_image *src);
extern int im_xor(ip_image *dest, ip_image *src);
extern int im_and(ip_image *dest, ip_image *src);
#define im_eor(d,s) im_xor(d,s)
extern int im_not(ip_image *im);

/* Mathematical functions on images */
extern int im_sqrt(ip_image *im, int flag);
extern int im_log(ip_image *im, int flag);

/* Max/Min operators */
extern int im_maximum(ip_image *dest, ip_image *src);
extern int im_minimum(ip_image *dest, ip_image *src);

/* Returns 1 if the two images differ in pixel data, 0 if identical */
extern int im_differ(ip_image *dest, ip_image *src);

/* Simple image processing */
extern int im_invert(ip_image *);
extern int im_norm(ip_image *, double imin, double imax,
			    double fmin, double fmax, int flag);
extern int im_bthresh(ip_image *, int lower, int higher);
extern ip_image *im_threshold(ip_image *, double lower, double higher);

/* Filter support routines - mainly intended for internal use. */
extern void im_clear_border(ip_image *im, int width);

/* Smoothing filters */
extern int im_median(ip_image *, int kwidth, int flag);
extern int im_percentile(ip_image *, int kwidth, double percent, int flag);
extern int im_mean(ip_image *, int kwidth, int flag);

/* Convolution based filters */
extern int im_convolve(ip_image *, void * kernel, int kwidth, int flag);

/* Edge detection */
extern int im_sobel(ip_image *, int flag);

/* See morph.h for morphological operators */

/* Image transforms */
extern int im_fft(ip_image *, int direction);
extern int im_fftshift(ip_image *);

/* Warping/registering images */
extern int im_warp(ip_image *im, ip_poly2d *ip_poly, int flag);
extern int im_warpx(ip_image *dest, ip_image *src, ip_poly2d *ip_poly, int flag);
extern regparms *im_register(ip_image *ref, ip_image *toreg, double maxtrans,
			     double maxrot, double maxscale, int flags);
extern ip_poly2d *warp_to_poly2d(regparms *rp, ip_vector2d o);

/* Adding noise to images */
extern int im_addnoise(ip_image *im, double skew, double amount, int flag);

/* Graphics drawing routines */
extern int im_drawpoint(ip_image *im, double val, ip_coord2d pt);
extern int im_drawline(ip_image *im, double val, ip_coord2d start, ip_coord2d end);
extern int im_drawbox(ip_image *im, double val, ip_box tbox);
extern int im_drawcircle(ip_image *im, double val, ip_coord2d origin, int radius);
extern int im_drawellipse(ip_image *im, double val, ip_coord2d origin, int a, int b);
extern int im_drawellipsex(ip_image *im, double val, double fillval,
                           ip_coord2d origin, int a, int b, int flag);
extern int im_drawarrow(ip_image *im, double val, ip_coord2d pt,double angle,int size);
int im_drawnumber(ip_image *im, double val, ip_coord2d pt, int number);

/* IM_PALETTE type image operations */
extern int im_palette_set(ip_image *im, ip_rgb *pal);
extern int im_palette_get(ip_image *im, ip_rgb *pal);
extern int im_palette_copy(ip_image *dest, ip_image *src);
extern int im_ubyte_to_palette(ip_image *im, ip_rgb *pal);
extern int im_palette_to_ubyte(ip_image *im);
/* im_palette_compress() is in hist.h */

/* IM_RGB special operations */
extern ip_image *im_form_rgb(ip_image *im, ip_image *r, ip_image *g, ip_image *b, ip_image *a);
extern int im_rgbmedian(ip_image *, int kwidth, int flag);
extern int im_rgbannf( ip_image *im, int width, double alpha, double r, int flag );
extern int im_rgbmyf( ip_image *im, int width, int flag );


/* IM_BINARY special operations */
extern int im_binary_to_ubyte(ip_image *im, int val);
extern int im_ubyte_to_binary(ip_image *im);
extern int im_mask(ip_image *im, ip_image *mask, double val, int flag);
/* im_threshold() defined above */

/* IM_COMPLEX special operations */
extern ip_image *im_form_complex(ip_image *dest, ip_image *r, ip_image *i);

/* For use with interlaced scanned images */
extern ip_image *im_oddlines(ip_image *im, int flag);
extern ip_image *im_evenlines(ip_image *im, int flag);

/* Display interface routines */
extern int display_init(int *argc, char **argv, ip_coord2d size, char *title, int flag);
extern int display_set(int action, ...);
extern ip_datum display_get(int action);
extern int display_canvas(ip_coord2d size, int flag);
extern int display_clear(int id, int flag, ...);

extern int display_image(int id, ip_image *, ip_coord2d origin);
extern int display_imagex(int id, ip_image *, ip_coord2d origin,
			  double min, double max);
extern int display_free(int id);

extern int display_overlay(int id, ip_image *im, ip_coord2d pos, int flag);
extern int display_label(int id, char *s, int pen, int size, ip_coord2d pt);
extern int display_labelwidth(char *s, int size);

extern int display_palette(int pal);
extern int load_palette(int numentries, ip_rgb *pal);
extern int display_pen(int colour);
extern int display_gamma(double g);
extern int display_contrast(double c);
extern int display_palentries(void);
extern int display_getpal(ip_rgb *pal);

extern event_info *display_mouse(int flag);
extern event_info *display_click(int flag);
extern int display_keypress(int flag);
extern event_info *display_event(int flag);

extern void display_save_view(char *filename);

extern void ip_wait_for_quit(void);

/* Region-of-interest routines */
extern ip_roi *ip_alloc_roi(int type, ...);
extern void ip_free_roi(ip_roi *roi);
extern int roi_set(int type, ip_roi *roi, ...);
extern int roi_modify(int type, ip_roi *roi, ...);
extern ip_datum roi_get(int type, ip_roi *roi);

extern int roi_in_image( ip_image *im, ip_roi *roi);

int im_roiset(ip_image *im, ip_roi *roi, double val, int flag);

/* Debugging routines to dump structures to stdout */
extern void ip_dump_image(FILE *f, ip_image *, int flag);
extern void ip_dump_roi(FILE *f, ip_roi *roi);
extern void dump_poly(ip_poly *p);
extern void dump_poly2d(ip_poly2d *p);
/* dump_stats() is in stats.h */
/* dump_hist() is in hist.h */

#endif
