/* $Id$ */
/*
   Programme: testip
   Author: M.J.Cree

   This is software routines to test the ip library.

   THIS SOFTWARE DOES NASTY THINGS AND ACCESSES IMAGES AND OBJECTS
   WITH FORBIDDEN METHODS. IT DOES THIS TO FORCE SOME CERTAIN ERROR
   CONDITIONS OR SITUATIONS THAT WOULD NOT BE EASILY POSSIBLE
   OTHERWISE.  DON'T TAKE THIS AS EXAMPLE CODE TO USE IP LIBRARY!

*/

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <limits.h>
#include <float.h>
#include <unistd.h>
#include <time.h>

#include <ip/internal.h>
#include <ip/raw.h>
#include <ip/tiff.h>
#include <ip/visilog.h>
#include <ip/cimages.h>
#include <ip/object.h>
#include <ip/stats.h>

#include <jhh/math_funcs.h>

#if 0
#include <ip/classify.h>
#endif

#include <common/common.h>
#include <common/string.h>

#if DBMALLOC
#include <dbmalloc/malloc.h>
#else
#include <debug/malloc.h>
#endif

/**** Defined in testold.c ****/

extern void test_old(void);
extern int old_im_warp(image *src, poly2d *poly, int flag);
extern int old_im_fft(image *, int);

/**** Internal routines ****/

void bombout(logstack *ipr, int error, char *extra, va_list ap);
int in_region(image *im, coord pt);

void filebuf_operations(void);

/* Stubs to avoid having to use structures in ip function calls */
static void im_drawlinez(image *im,double val, int sx, int sy, int ex, int ey);
static void im_drawboxz(image *im, double val, int ox, int oy, int sx, int sy);
static int display_imagexz(int id, image *im, int ox, int oy,
			    double min, double max);
static int display_imagez(int id, image *im, int ox, int oy);

/* Some internal test routines */
#if 0
void test_classify(int argc, char *argv[]);
#endif

void do_basic_tests(void);

void test_error_routines(void);
void test_zoom(void);
void test_stats(image *im);
void test_eider(image *im);
void test_included(void);
void test_arith(int (*im_arith)(image *,image *,int), int type, double scale, int flag);
void test_object_measure(void);
void test_filetype(char *);
object *create_object(coord origin);
void test_polyfit(void);
void test_graphics(void);
void test_filters(void);
void test_morph(void);
void test_convert(void);
void test_fft(void);
void test_noise(void);
void compare_images(image *gim, image *rim);
void test_create(void);
void test_addmul(void);
void test_fourier(void);
void test_functions(void);
void test_warp(void);

image *get_memory_image(void);
int print_memory_list(void);

void timeit_I(int (*proc)(image *), image *);
void timeit_II(int (*proc)(image *, image *), image *, image *);

void timeit_If(int (*proc)(image *,int), image *, int f);
void timeit_IIf(int (*proc)(image *, image *,int), image *dest, image *src, int f);
void timeit_Iif(int (*proc)(image *,int,int), image *, int, int f);
void timeit_Iiif(int (*proc)(image *,int,int,int), image *, int, int, int f);
void timeit_Idf(int (*proc)(image *, double, int), image *im, double d, int f);
void timeit_Ipf(int (*proc)(image *, poly2d *, int), image *im, poly2d *p, int f);
void timeit_IIpf(int (*proc)(image *, image *, poly2d *, int), image *dest, image *src, poly2d *p, int f);
void timeit_Ivif(int (*proc)(image *,void *,int,int), image *, void *, int, int);

void timeit_ISR(int (*proc)(image *, stats *st, ROI *roi), image *im, stats *st, ROI *roi);

image * timeit_xIif(image * (*proc)(image *,int,int), image *, int, int f);
image * timeit_xIiif(image * (*proc)(image *,int,int,int), image *, int, int, int f);
image * timeit_xIddf(image * (*proc)(image *,double,double,int), image *, double, double, int f);

#define timeit_Ii(p,i,c) timeit_If(p,i,c)
#define timeit_Iiii(p,i,c,d,e) timeit_Iiif(p,i,c,d,e)

void prtstats(image *im, char *name, int flag);
void dispim(image *im);
void set_parameters(void);
void chk_modified(int modified, image *im);

void not_implemented(void);

double im_crosscorr(image *im1, image *im2, ROI *roi);
void conjugate_image(image *im);

static void readline(char *buf, int len, FILE *f);
static char * chkparm(char * str, char *id, int numparms);
static int intparm(void);
static double dblparm(void);
static void bdformat(void);

#define NUMMEMORY 10

/* Global image buffer */

image *gim;			/* Global image */
image *rim;			/* Reference image */
image *memory[NUMMEMORY];	/* Ten image memory locations */
char *memstr[NUMMEMORY];	/* Ten strings describing memory */
int di_min_b, di_max_b;		/* Display min/max BYTE images */
double di_min, di_max;		/* Display min/max other images */
int dispid = FLG_NEWIMAGE;	/* Display image ID */
coord dispsize;			/* Display size */

/* Other global variables */

int do_timing;			/* TRUE if to time routines */
ROI *groi;			/* ROI for analysis routines */


main(argc,argv)
int argc;
char *argv[];
{
   int test;
   coord pt;
   static char *tests[] = {
      "File and buffer operations.",
      "Display global image.",
      "Display reference image.",
      "Create an image and put into global buffer.",
      "Compare global image to reference image.",
      "Set global parameters.",
      "Corrupt global image with noise.",
      "Test filtering routines.",
      "Test morphological routines.",
      "Test graphics routines.",
      "Test polynomial fitting.",
      "Test zooming routines.",
      "Test warping routines.",
      "Test arithmetical/mathematical functions.",
      "Test Fourier routines.",
      "Test old functions for comparision."
   };

#ifdef _DEBUG_MALLOC_INC     /* Malloc debugging (see: dbmalloc) */
   unsigned long  histid1, histid2, orig_size, current_size;
   union dbmalloptarg  m;

   m.i = M_HANDLE_IGNORE;
   dbmallopt(MALLOC_WARN, &m);
   
   m.i = M_HANDLE_EXIT;
   dbmallopt(MALLOC_FATAL, &m);
   
   m.i = 1;
   dbmallopt(MALLOC_CKCHAIN, &m);
#endif  

   /* The following routines do memory allocations for working space
      so call them before calling malloc_inuse() so that we don't
      register them as being unfreed when exit programme. */

   register_error_handler(bombout);
   printf("%s\n\n",ip_version());

   do_basic_tests();

   dispsize.x = 640;
   dispsize.y = 580;
   display_init(&argc, argv, dispsize, "For testing IP library", NO_FLAG);

   random();			/* Dummy call (since allocates memory) */

#ifdef _DEBUG_MALLOC_INC   
   orig_size = malloc_inuse(&histid1);
#endif

   for (test=0; test<NUMMEMORY; test++) {
      memory[test] = NULL;
   }

   gim = alloc_image(IM_BYTE, 256, 256);
   im_clear(gim);
   rim = alloc_image(IM_BYTE, 256, 256);
   im_clear(rim);

   do_timing = FALSE;
   di_min_b = 0;
   di_max_b = 255;
   di_min = di_max = 0.0;

   do {
      test = ask_choice(tests, 16, 1);

      switch (test) {
       case -1:
	 printf("\nYou have requested to quit the programme.\n");
	 if (NOT ask_yesno("Are you sure you want to quit")) {
	    test = -2;
	    putchar('\n');
	 }
	 break;
       case 0:
	 filebuf_operations();
	 break;

       case 1:
	 dispim(gim);
	 break;

       case 2:
	 dispim(rim);
	 break;

       case 3:
	 test_create();
	 break;

       case 4:
	 compare_images(gim,rim);
	 break;

       case 5:
	 set_parameters();
	 break;

       case 6:
	 test_noise();
	 dispim(gim);
	 break;

       case 7:
	 test_filters();
	 break;

       case 8:
	 test_morph();
	 break;

       case 9:
	 test_graphics();
	 break;

       case 10:
	 test_polyfit();
	 break;

       case 11:
	 test_zoom();
	 break;

       case 12:
	 test_warp();
	 break;

       case 13:
	 test_functions();
	 break;

       case 14:
	 test_fourier(); 
	 break;

       case 15:
	 test_old();
	 break;

      }

   } while (test != -1);

   for (test=0; test<NUMMEMORY; test++) {
      if (memory[test]) {
	 free_image(memory[test]);
	 if (memstr[test]) ip_free(memstr[test]);
      }
   }
   if (gim) free_image(gim);
   if (rim) free_image(rim);
   if (groi) free_roi(groi);

#ifdef _DEBUG_MALLOC_INC
   current_size = malloc_inuse(&histid2);
 
   if( current_size != orig_size )
      malloc_list(2, histid1, histid2);
#endif

   return(0);
}



void bombout(logstack *ipr, int error, char *extra, va_list ap)
{
   static int inhere = 0;
   char name[14];
   int mem;

   print_error("test",ipr,error,extra,ap);
   fflush(stdout);
   if (error == ERR_FILE_OPEN) {
      fprintf(stderr,"\n");
      return;
   }
   if (inhere) {
      fprintf(stderr,"*** SORRY - Error while processing previous error"
	             " - ABORTING ***\n");
      exit(1);
   }
   if (NOT ask_yesno("Should I continue testing")) {
      if (ask_yesno("Should I save images before exiting???")) {
	 inhere++;
	 if (gim) write_tiff_image("global.tif",gim);
	 if (rim) write_tiff_image("reference.tif",rim);
	 for (mem=0; mem<NUMMEMORY; mem++) {
	    if (memory[mem]) {
	       sprintf(name, "memory%d.tif", mem+1);
	       write_tiff_image(name, memory[mem]);
	    }
	 }
	 inhere--;
      }
      exit(1);
   }
}


void do_basic_tests(void)
{
  if (ip_typesize[IM_BYTE] != 1)
    printf("WARNING: BYTE typesize is %d (should be 1)\n",(int)ip_typesize[IM_BYTE]);
  if (ip_typesize[IM_SHORT] != 2)
    printf("WARNING: SHORT typesize is %d (should be 2)\n",(int)ip_typesize[IM_SHORT]);
  if (ip_typesize[IM_LONG] != 4)
    printf("WARNING: LONG typesize is %d (should be 4)\n",(int)ip_typesize[IM_LONG]);
  if (ip_typesize[IM_FLOAT] != 4)
    printf("WARNING: FLOAT typesize is %d (should be 4)\n",(int)ip_typesize[IM_FLOAT]);
  if (ip_typesize[IM_DOUBLE] != 8)
    printf("WARNING: DOUBLE typesize is %d (should be 8)\n",(int)ip_typesize[IM_DOUBLE]);
  if (ip_typesize[IM_COMPLEX] != 8)
    printf("WARNING: COMPLEX typesize is %d (should be 8)\n",(int)ip_typesize[IM_COMPLEX]);
  if (ip_typesize[IM_RGB] != 3)
    printf("WARNING: RGB typesize is %d (should be 3)\n",(int)ip_typesize[IM_RGB]);
  if (ip_typesize[IM_RGB16] != 6)
    printf("WARNING: RGB typesize is %d (should be 6)\n",(int)ip_typesize[IM_RGB16]);
}




void filebuf_operations(void)
{
   int option, mem;
   image *tim;
   char *fn, *str;
   char *options[]= {
      "Load an image from a disc file into global buffer.",
      "Load an image from a disc file into reference buffer.",
      "Save global image to disc file.",
      "Copy global buffer to reference buffer.",
      "Copy reference buffer to global buffer.",
      "Exchange global and reference buffers.",
      "Put global image into memory.",
      "Get global image from memory.",
      "List images."
   };

   do {
      option = ask_choice(options, 9, 1);

      switch (option) {
       case 0:
	 fn = ask_string("Filename: ");
	 tim = ip_read_image(fn, IM_BINARY, IM_BYTE, IM_SHORT, IM_LONG,
					IM_FLOAT, IM_DOUBLE, -1);
	 if (tim) {
	    if (gim) free_image(gim);
	    gim = tim;
	    dispim(gim);
	 }
	 break;

       case 1:
	 fn = ask_string("Filename: ");
	 tim = ip_read_image(fn, IM_BINARY, IM_BYTE, IM_SHORT, IM_LONG,
					IM_FLOAT, IM_DOUBLE, -1);
	 if (tim) {
	    if (rim) free_image(rim);
	    rim = tim;
	    dispim(rim);
	 }
	 break;

       case 2:
	 if (gim->type == IM_COMPLEX) {
	    fn = ask_string("(Complex Image) Base Filename: ");
	    str = ip_malloc(strlen(fn)+7);
	    strcpy(str,fn);
	    strcat(str,"-r.tif");
	    printf("Writing real part as %s.\n",str);
	    tim = im_convert(gim, IM_FLOAT, FLG_REAL);
	    write_tiff_image(str, tim);
	    free_image(tim);
	    strcpy(str,fn);
	    strcat(str,"-i.tif");
	    printf("Writing imaginery part as %s.\n",str);
	    tim = im_convert(gim, IM_FLOAT, FLG_IMAG);
	    write_tiff_image(str, tim);
	    free_image(tim);
	    ip_free(str);
	 }else{
	    fn = ask_string("Filename: ");
	    write_tiff_image(fn, gim);
	 }
	 break;

       case 3:
	 free_image(rim);
	 rim = im_copy(gim);
	 dispim(rim);
	 break;

       case 4:
	 free_image(gim);
	 gim = im_copy(rim);
	 dispim(gim);
	 break;

       case 5:
	 tim = gim;
	 gim = rim;
	 rim = tim;
	 break;

       case 6:
	 mem = ask_double("Memory location", 0, NUMMEMORY);
	 if (mem > 0) {
	    str = ask_string("Short name: ");
	    mem--;
	    if (memory[mem]) {
	       free_image(memory[mem]);
	       if (memstr[mem]) ip_free(memstr[mem]);
	    }
	    memory[mem] = im_copy(gim);
	    memstr[mem] = ip_mallocx(strlen(str)+1);
	    strcpy(memstr[mem],str);
	    dispim(gim);
	 }
	 break;

       case 7:
	 if ((tim = get_memory_image())) {
	    if (gim)
	       free_image(gim);
	    gim = im_copy(tim);
	    dispim(gim);
	 }	 
	 break;

       case 8:
	 printf("     ");
	 prtstats(gim, "*Global*", 0);
	 printf("     ");
	 prtstats(rim, "*Reference*", 0);
	 print_memory_list();
	 printf("\n");
	 break;
	 
      }
   } while (option != -1);
}




int in_region(image *im, coord pt)
{
   return (im->image.b[Xi(pt.x,pt.y)]);
}




void test_error_routines(void)
{
   image *im;

   /* Try some illegal allocates */

   alloc_image(24,1882,8838);
   alloc_image(1,1882,-8838);
   alloc_image(IM_DOUBLE,SHRT_MAXV,SHRT_MAXV);

   /* Try an illegal copy to get nested routines give error */

   if ((im = alloc_image(IM_BYTE,10,10)) != NULL) {
      im->size.x = im->Nx = im->size.y = im->Ny = SHRT_MAXV;
      im_copy(im);
      im->size.x = im->Nx = im->size.y = im->Ny = 10;
      free_image(im);
   }

   /* Some error messages called by ourselves */

   log_error(ERR_OUT_OF_MEM);
   log_routine("1st");
   log_routine("2nd");
   log_routine("3rd");
   log_routine("4th");
   log_error(16002);		/* An impossible error */
   pop_routine();
   pop_routine();
   pop_routine();
   log_error(ERR_WRONG_IMAGE_TYPE,"freddy","BYTE");
   log_error(ERR_FILE_OPEN,"/home/user/freddy");
   pop_routine();
   pop_routine();		/* Ooops, one too many! */
   log_error(ERR_ALGORITHM_FAULT,"followed by the digit one...%d",1);

}


void test_zoom(void)
{
   image *im, *tmp;
   int i,j;
   double xs, ys;
   int option;
   int modified;
   char *options[]= {
      "Re-load local image from global image.",
      "Put local image into global image.",
      "Put local image into reference image.",
      "Run im_reduce() with FLG_SAMPLE.",
      "Run im_reduce() with FLG_AVERAGE.",
      "Run im_expand() with FLG_ZOOM.",
      "Run im_expand() with FLG_INTERP.",
      "Run im_reducex() with FLG_SAMPLE.",
      "Run im_reducex() with FLG_AVERAGE.",
      "Run im_zoom() with FLG_BILINEAR.",
      "Run im_flip() with FLG_HORIZ.",
      "Run im_flip() with FLG_VERT.",      
   };

   im = im_copy(gim);
   modified = FALSE;

   dispim(im);

   do {
      option = ask_choice(options, 12, 1);

      if (option >= 0) {
	 modified = TRUE;
      }

      switch (option) {
       case 0:
	 free_image(im);
	 im = im_copy(gim);
	 dispim(im);
	 break;
       case 1:
	 free_image(gim);
	 gim = im_copy(im);
	 modified = FALSE;
	 break;
       case 2:
	 free_image(rim);
	 rim = im_copy(im);
	 modified = FALSE;
	 break;
       case 3:
	 i = ask_double("Sample by", 1, 100);
	 tmp = timeit_xIif(im_reduce, im, i, FLG_SAMPLE);
	 if (tmp != NULL) {
	   free_image(im);
	   im = tmp;
	   dispim(im);
	 }
	 break;
       case 4:
	 i = ask_double("Reduce by", 1, 100);
	 tmp = timeit_xIif(im_reduce, im, i, FLG_AVERAGE);
	 if (tmp != NULL) {
	   free_image(im);
	   im = tmp;
	   dispim(im);
	 }
	 break;
       case 5:
	 i = ask_double("Expand by", 1, 100);
	 tmp = timeit_xIif(im_expand, im, i, FLG_ZOOM);
	 if (tmp != NULL) {
	   free_image(im);
	   im = tmp;
	   dispim(im);
	 }
	 break;
       case 6:
	 i = ask_double("Expand by", 1, 100);
	 tmp = timeit_xIif(im_expand, im, i, FLG_INTERP);
	 if (tmp != NULL) {
	   free_image(im);
	   im = tmp;
	   dispim(im);
	 }
	 break;
       case 7:
	 i = ask_double("Sample x by", 1, 100);
	 j = ask_double("Sample y by", 1, 100);
	 tmp = timeit_xIiif(im_reducex, im, i, j, FLG_SAMPLE);
	 if (tmp != NULL) {
	   free_image(im);
	   im = tmp;
	   dispim(im);
	 }
	 break;
       case 8:
	 i = ask_double("Reduce x by", 1, 100);
	 j = ask_double("Reduce y by", 1, 100);
	 tmp = timeit_xIiif(im_reducex, im, i, j, FLG_AVERAGE);
	 if (tmp != NULL) {
	   free_image(im);
	   im = tmp;
	   dispim(im);
	 }
	 break;
       case 9:
	 xs = ask_double("X skip", 0, 100);
	 ys = ask_double("Y skip", 0, 100);
	 tmp = timeit_xIddf(im_zoom, im, xs, ys, FLG_BILINEAR);
	 if (tmp != NULL) {
	   free_image(im);
	   im = tmp;
	   dispim(im);
	 }
	 break;
       case 10:
	 timeit_If(im_flip, im, FLG_HORIZ);
	 dispim(im);
	 break;
       case 11:
	 timeit_If(im_flip, im, FLG_VERT);
	 dispim(im);
	 break;
      }
   } while (option >= 0);

   chk_modified(modified, im);

   free_image(im);
}


void test_warp(void)
{
   image *im, *tmp;
   int i,j;
   poly2d *poly;
   int order,dim;
   int option;
   int modified;
   vector2d *refpt, *objpt;
   vector2d chisq;
   coord pt,ptmin,ptmax;
   char *fn,*cptr;
   char str[256];
   FILE *f;
   char *options[]= {
      "Re-load local image from global image.",
      "Put local image into global image.",
      "Put local image into reference image.",
      "Initialise local poly.",
      "Initialise local poly by point_map.",
      "Dump local poly.",
      "Load local poly from file.",
      "Save local poly to file.",
      "Run im_warp with FLG_NEAREST",
      "Run im_warp with FLG_BILINEAR",
      "Run im_warpx with FLG_NEAREST",
      "Run im_warpx with FLG_BILINEAR",
      "Run im_warpx with FLG_BSPLINE_3",
#if 0
      /* These two options now deleted. */
      "Run old_im_warp with FLG_NEAREST",
      "Run old_im_warp with FLG_BILINEAR"
#endif
   };
   static char *Xnames2d[] = {
     "1: ",
     "x: ",
     "y: ",
     "x^2: ",
     "xy: ",
     "y^2: ",
     "x^3: ",
     "x^2y: ",
     "xy^2: ",
     "y^3: ",
     "x^4: ",
     "x^3y: ",
     "x^2y^2: ",
     "xy^3: ",
     "y^4: "
   };

   im = im_copy(gim);
   modified = FALSE;
   poly = NULL;

   dispim(im);

   do {
      option = ask_choice(options, 13, 1);

      if (option >= 0) {
	 modified = TRUE;
      }

      switch (option) {
       case 0:
	 free_image(im);
	 im = im_copy(gim);
	 dispim(im);
	 break;
       case 1:
	 free_image(gim);
	 gim = im_copy(im);
	 modified = FALSE;
	 break;
       case 2:
	 free_image(rim);
	 rim = im_copy(im);
	 modified = FALSE;
	 break;
       case 3:
	 order = ask_double("Order of poly: ",0,3);
	 free_poly2d(poly);
	 poly = alloc_poly2d(2,order);
	 order = poly2d_numcoeffs(poly);
	 printf("Enter coefficients of X transformation.\n");
	 for (i=0; i<order; i++) {
	    poly->X[i] = ask_double(Xnames2d[i],-1E12,1E12);
	 }
	 printf("Enter coefficients of Y transformation.\n");
	 for (i=0; i<order; i++) {
	    poly->Y[i] = ask_double(Xnames2d[i],-1E12,1E12);
	 }
	 break;
       case 4:
	 order = ask_double("Order of poly: ",0,3);
	 j = ask_double("Number of points: ",1,100);
	 refpt = ip_mallocx(sizeof(vector2d)*j);
	 objpt = ip_mallocx(sizeof(vector2d)*j);
	 ptmin.x = ptmin.y = -1000000;
	 ptmax.x = ptmax.y = 1000000;
	 for (i=0; i<j; i++) {
	   sprintf(str,"Ref pt %d: ",i);
	   pt = ask_coord(str,ptmin,ptmax);
	   refpt[i].x = pt.x;
	   refpt[i].y = pt.y;
	   sprintf(str,"Obj pt %d: ",i);
	   pt = ask_coord(str,ptmin,ptmax);
	   objpt[i].x = pt.x;
	   objpt[i].y = pt.y;
	 }
	 free_poly2d(poly);
	 poly = ip_pointmap(j,refpt,objpt,order,&chisq);
	 ip_free(refpt);
	 ip_free(objpt);
	 printf("Fitted with chi-square = (%g,%g)\n",chisq.x,chisq.y);
	 break;
       case 5:
	 if (poly) 
	   dump_poly2d(poly);
	 break;
       case 6:
	 fn = ask_string("filename: ");
	 if (f = fopen(fn,"r")) {
	    readline(str,200,f);
	    if (cptr = chkparm(str,"poly2d",2)) {
	       dim = intparm();
	       order = intparm();
	       if (dim == 2) {
		 free_poly2d(poly);
		 poly = alloc_poly2d(dim,order);
		 j = poly2d_numcoeffs(poly);
		 readline(str,200,f);
		 cptr = chkparm(str,"x",j);
		 for (i=0; i<j; i++) 
		    poly->X[i] = dblparm();
		 readline(str,200,f);
		 cptr = chkparm(str,"y",j);
		 for (i=0; i<j; i++) 
		    poly->Y[i] = dblparm();
	       }else{
		 eprintf("Expect a 2d poly (not 1d)\n");
		 fclose(f);
	       }
	    }else{
	      eprintf("File not poly2d or badly formatted.\n");
	      fclose(f);
	    }
	 }else{
	   eprintf("Couldn't open %s.\n",fn);
	 }
	 break;
       case 7:
	 if (poly == NULL) {
	    eprintf("No poly allocated.");
	    break;
	 }
	 fn = ask_string("filename: ");
	 if (f = fopen(fn,"w")) {
	    fprintf(f,"poly2d: %d %d\n",poly->dimension,poly->order);
	    j = poly2d_numcoeffs(poly);
	    fprintf(f,"x:");
	    for (i=0; i<j; i++) {
	       fprintf(f," %g",poly->X[i]);
	    }
	    fputc('\n',f);
	    if (poly->dimension == 2) {
	       fprintf(f,"y:");
	       for (i=0; i<j; i++) {
		  fprintf(f," %g",poly->Y[i]);
	       }
	       fputc('\n',f);
	    }
	    fclose(f);
	 }else{
	    eprintf("Couldn't open %s for writing.\n",fn);
	 }
	 break;
       case 8:
	 if (poly) {
	   timeit_Ipf(im_warp,im,poly,FLG_NEAREST);
	   dispim(im);
	 }
	 break;
       case 9:
	 if (poly) {
	   timeit_Ipf(im_warp,im,poly,FLG_BILINEAR);
	   dispim(im);
	 }
	 break;
       case 10:
	 if (poly) {
	   tmp = alloc_image(im->type, im->size.x, im->size.y);
	   if (tmp) {
	     timeit_IIpf(im_warpx, tmp, im, poly, FLG_NEAREST);
	     free_image(im);
	     im = tmp;
	     dispim(im);
	   }
	 }
	 break;
       case 11:
	 if (poly) {
	   tmp = alloc_image(im->type, im->size.x, im->size.y);
	   if (tmp) {
	     timeit_IIpf(im_warpx, tmp, im, poly, FLG_BILINEAR);
	     free_image(im);
	     im = tmp;
	     dispim(im);
	   }
	 }
	 break;
       case 12:
	 if (poly) {
	   tmp = alloc_image(im->type, im->size.x, im->size.y);
	   if (tmp) {
	     timeit_IIpf(im_warpx, tmp, im, poly, FLG_BSPLINE_3);
	     free_image(im);
	     im = tmp;
	     dispim(im);
	   }
	 }
	 break;
#if 0
       case 13:
	 if (poly) {
	   timeit_Ipf(old_im_warp,im,poly,FLG_NEAREST);
	   dispim(im);
	 }
	 break;
       case 14:
	 if (poly) {
	   timeit_Ipf(old_im_warp,im,poly,FLG_BILINEAR);
	   dispim(im);
	 }
	 break;
#endif
      }
   } while (option >= 0);

   chk_modified(modified, im);

   free_poly2d(poly);
   free_image(im);
}




void test_functions(void)
{
   image *im, *memim;
   double val;
   int option;
   int modified;
   int flags;
   char *options[]= {
      "Re-load local image from global image.",
      "Put local image into global image.",
      "Put local image into reference image.",
      "Set flags: [NO_FLAG]",
      "Run im_add() between local and memory",
      "Run im_sub() between local and memory",
      "Run im_mul() between local and memory",
      "Run im_div() between local and memory",
      "Run im_addc() between local and constant",
      "Run im_subc() between local and constant",
      "Run im_mulc() between local and constant",
      "Run im_divc() between local and constant",
      "Run im_or() between local and memory",
      "Run im_and() between local and memory",
      "Run im_xor() between local and memory",
      "Run im_not()",
      "Run im_invert()",
      "Run im_sqrt()",
      "Run im_log()"
   };

   im = im_copy(gim);
   modified = FALSE;

   dispim(im);

   flags = NO_FLAG;

   do {
      option = ask_choice(options, 15, 1);

      if (option >= 0) {
	 modified = TRUE;
      }

      switch (option) {
       case 0:
	 free_image(im);
	 im = im_copy(gim);
	 dispim(im);
	 break;
       case 1:
	 free_image(gim);
	 gim = im_copy(im);
	 modified = FALSE;
	 break;
       case 2:
	 free_image(rim);
	 rim = im_copy(im);
	 modified = FALSE;
	 break;
       case 3:
	 printf("Sorry: flags set to NO_FLAG\n");
	 break;
       case 4:
	 if ((memim = get_memory_image())) {
	    timeit_IIf(im_add, im, memim, flags);
	    dispim(im);
	 }
	 break;
       case 5:
	 if ((memim = get_memory_image())) {
	    timeit_IIf(im_sub, im, memim, flags);
	    dispim(im);
	 }
	 break;
       case 6:
	 if ((memim = get_memory_image())) {
	    timeit_IIf(im_mul, im, memim, flags);
	    dispim(im);
	 }
	 break;
       case 7:
	 if ((memim = get_memory_image())) {
	    timeit_IIf(im_div, im, memim, flags);
	    dispim(im);
	 }
	 break;
       case 8:
	 val = ask_double("Value to add to image",-1e300,1e300);
	 timeit_Idf(im_addc, im, val, flags);
	 dispim(im);
	 break;
       case 9:
	 val = ask_double("Value to subtract from image",-1e300,1e300);
	 timeit_Idf(im_subc, im, val, flags);
	 dispim(im);
	 break;
       case 10:
	 val = ask_double("Value to multiply image by",-1e300,1e300);
	 timeit_Idf(im_mulc, im, val, flags);
	 dispim(im);
	 break;
       case 11:
	 val = ask_double("Value to divide image by",-1e300,1e300);
	 timeit_Idf(im_divc, im, val, flags);
	 dispim(im);
	 break;
       case 12:
	 if ((memim = get_memory_image())) {
	    timeit_II(im_or, im, memim);
	    dispim(im);
	 }
	 break;
       case 13:
	 if ((memim = get_memory_image())) {
	    timeit_II(im_and, im, memim);
	    dispim(im);
	 }
	 break;
       case 14:
	 if ((memim = get_memory_image())) {
	    timeit_II(im_xor, im, memim);
	    dispim(im);
	 }
	 break;
       case 15:
	 timeit_I(im_not, im);
	 dispim(im);
	 break;
       case 16:
	 timeit_I(im_invert, im);
	 dispim(im);
	 break;
       case 17:
	 timeit_If(im_sqrt, im, NO_FLAG);
	 dispim(im);
	 break;
       case 18:
	 timeit_If(im_log, im, NO_FLAG);
	 dispim(im);
	 break;
      }
   } while (option >= 0);

   chk_modified(modified, im);

   free_image(im);
}



void test_fourier(void)
{
   image *im, *memim, *tmp;
   int option;
   int modified;
   char *options[]= {
      "Re-load local image from global image.",
      "Put local image into global image.",
      "Put local image into reference image.",
      "Convert to complex (real part only)",
      "Convert complex to float",
      "Run im_fft() - Forward",
      "Run im_fft() - Inverse",
      "Run old_im_fft() - Forward",
      "Run old_im_fft() - Inverse",
      "Run im_fftshift()",
      "Zeropad image (image at centre)",
      "De-zeropad image (image from centre)",
      "Convolution between local and memory",
      "Correlation between local and memory",
      "Auto-correlate"
   };
   int partopt;
   char *partopts[] = {
      "Real part",
      "Imaginary part",
      "Magnitude",
      "Phase"
   };

   im = im_copy(gim);
   modified = FALSE;

   dispim(im);

   do {
      option = ask_choice(options, 13, 1);

      if (option >= 0) {
	 modified = TRUE;
      }

      switch (option) {
       case 0:
	 free_image(im);
	 im = im_copy(gim);
	 dispim(im);
	 break;
       case 1:
	 free_image(gim);
	 gim = im_copy(im);
	 modified = FALSE;
	 break;
       case 2:
	 free_image(rim);
	 rim = im_copy(im);
	 modified = FALSE;
	 break;
       case 3:
	 tmp = im_convert(im, IM_COMPLEX, FLG_REAL);
	 if (tmp) {
	   free_image(im);
	   im = tmp;
	 }
	 dispim(im);
	 break;
       case 4:
	 tmp = NULL;
	 partopt = ask_choice(partopts, 4, 0);
	 switch (partopt) {
	  case 0:
	    tmp = im_convert(im, IM_FLOAT, FLG_REAL);
	    break;
	  case 1:
	    tmp = im_convert(im, IM_FLOAT, FLG_IMAG);
	    break;
	  case 2:
	    tmp = im_convert(im, IM_FLOAT, FLG_MAG);
	    break;
	  case 3:
	    tmp = im_convert(im, IM_FLOAT, FLG_ANGLE);
	    break;
	 }	    
	 if (tmp) {
	   free_image(im);
	   im = tmp;
	 }
	 dispim(im);
	 break;

       case 5:
	 timeit_If(im_fft, im, FLG_NORM);
	 dispim(im);
	 break;
       case 6:
	 timeit_If(im_fft, im, FLG_NORM|FLG_INVERSE);
	 dispim(im);
	 break;

       case 7:
	 timeit_If(old_im_fft, im, FLG_NORM);
	 dispim(im);
	 break;
       case 8:
	 timeit_If(old_im_fft, im, FLG_NORM|FLG_INVERSE);
	 dispim(im);
	 break;

       case 9:
	 timeit_I(im_fftshift, im);
	 dispim(im);
	 break;

       case 10: {
	 coord size, dorg, sorg;

	 size.x = im->size.x * 2;
	 size.y = im->size.y * 2;
	 if ((tmp = alloc_image(im->type, size.x, size.y)) != NULL) {
	    im_clear(tmp);
	    dorg.x = im->size.x / 2;
	    dorg.y = im->size.y / 2;
	    sorg.x = sorg.y = 0;
	    if (im_insert(tmp,dorg, im,sorg,im->size) == OK) {
	       free_image(im);
	       im = tmp;
	    }else{
	      free_image(tmp);
	    }
	 }
	 dispim(im);
         }break;

       case 11: {
	 coord origin, size;

	 origin.x = im->size.x / 4;
	 origin.y = im->size.y / 4;
	 size.x = im->size.x / 2;
	 size.y = im->size.y / 2;
	 if ((tmp = im_cut(im, origin, size)) != NULL) {
	    free_image(im);
	    im = tmp;
	 }
	 dispim(im);
         }break;
	 
       case 12:
	 if ((memim = get_memory_image())) {
	    if (im->type != IM_COMPLEX || memim->type != IM_COMPLEX) {
	       eprintf("Both local and memory image must be complex.");
	    }else{
	       im_fft(im, FLG_NORM);
	       tmp = im_copy(memim);
	       im_fft(tmp, FLG_NORM);
	       im_mul(im, tmp, NO_FLAG);
	       free_image(tmp);
	       im_fft(im, FLG_NORM | FLG_INVERSE);
	       dispim(im);
	    }
	 }
	 break;

       case 13:
	 if ((memim = get_memory_image())) {
	    if (im->type != IM_COMPLEX || memim->type != IM_COMPLEX) {
	       eprintf("Both local and memory image must be complex.");
	    }else{
	       im_fft(im, FLG_NORM);
	       tmp = im_copy(memim);
	       im_fft(tmp, FLG_NORM);
	       conjugate_image(tmp);
	       im_mul(im, tmp, NO_FLAG);
	       free_image(tmp);
	       im_fft(im, FLG_NORM | FLG_INVERSE);
	       dispim(im);
	    }
	 }
	 break;

       case 14:
	 if (im->type != IM_COMPLEX) {
	       printf("\nlocal image must be complex.\n\n");
	 }else{
	   im_fft(im, FLG_NORM);
	   tmp = im_copy(im);
	   conjugate_image(tmp);
	   im_mul(im, tmp, NO_FLAG);
	   free_image(tmp);
	   im_fft(im, FLG_NORM | FLG_INVERSE);
	   dispim(im);
	 }
	 break;

      }
   } while (option >= 0);

   chk_modified(modified, im);

   free_image(im);
}




void prtstats(image *im, char *name, int flag)
{
   char *str;

   if (name) {
      str = ip_mallocx(strlen(name)+3);
      str[0] = '[';
      str[1] = 0;
      strcat(str,name);
      strcat(str,"]");
      printf("%-13s ",str);
      ip_free(str);
   }
   printf("%-7s (%d,%d)\n", ipmsg_type[im->type], im->size.x, im->size.y);
   if (flag)
      test_stats(im);
}



void test_stats(image *im)
{
   stats st;

   im_stats(im,&st,NULL);

   printf("  min = %g at (%d,%d)\n",st.min,st.minpt.x,st.minpt.y);
   printf("  max = %g at (%d,%d)\n",st.max,st.maxpt.x,st.maxpt.y);
   printf("  intsum = %g, mean = %g, rmsmean = %g, stddev = %g\n",
		st.intsum,st.mean,st.rmsmean,st.stddev);
}



void test_included(void)
{
   image *im;

   im = alloc_image(IM_BYTE,128,128);
   if (im) free_image(im);
   im = alloc_image(IM_SHORT,128,128);
   if (im) free_image(im);
   im = alloc_image(IM_FLOAT,128,128);
   if (im) free_image(im);
   im = alloc_image(IM_COMPLEX,128,128);
   if (im) free_image(im);
}


void test_addmul(void)
{
   printf("Three tests IM_BYTE addition\n");
   test_arith(im_add,IM_BYTE,1,0);
   test_arith(im_add,IM_BYTE,4,0);
   test_arith(im_add,IM_BYTE,4,FLG_CLIP);

   printf("Two tests IM_SHORT addition\n");
   test_arith(im_add,IM_SHORT,1,0);
   test_arith(im_add,IM_SHORT,512,0);

   printf("Test IM_COMPLEX addition\n");
   test_arith(im_add,IM_COMPLEX,1,0);

   printf("Two tests IM_BYTE multiplication\n");
   test_arith(im_mul,IM_BYTE,1,0);
   test_arith(im_mul,IM_BYTE,1,FLG_CLIP);

   printf("Test IM_COMPLEX multiplication\n");
   test_arith(im_mul,IM_COMPLEX,2,0);
}


void test_arith(int (*im_arith)(image *,image *,int), int type, double scale, int flag)
{
   image *im1,*im2;
   int i;

   im1 = alloc_image(type,8,8);
   im2 = alloc_image(type,8,8);

   if (im1 == NULL || im2 == NULL) {
      printf("Unexpected image allocation fail - aborting test!\n");
      return ;
   }

   for (i=0; i<64; i++) {
      switch (type) {
       case IM_BYTE:
	 im1->image.b[i] = i*scale;
	 im2->image.b[i] = (64- i) * scale;
	 break;
       case  IM_SHORT:
	 im1->image.s[i] = i*scale;
	 im2->image.s[i] = (64- i) * scale;
	 break;
       case  IM_LONG:
	 im1->image.l[i] = i*scale;
	 im2->image.l[i] = (64- i) * scale;
	 break;
       case  IM_FLOAT:
	 im1->image.f[i] = i*scale;
	 im2->image.f[i] = (64- i) * scale;
	 break;
       case  IM_DOUBLE:
	 im1->image.d[i] = i*scale;
	 im2->image.d[i] = (64- i) * scale;
	 break;
       case IM_COMPLEX:
	 im1->image.c[i].r = i*scale;
	 im2->image.c[i].r = (64- i) * scale;
	 im1->image.c[i].i = i*scale / 2;
	 im2->image.c[i].i = (64- i) * scale / 2;
	 break;
      } 
   }

   im_arith(im1,im2,flag);
      
   for (i=0; i<64; i++) {
      switch (type) {
       case IM_BYTE:
	 printf("%6d",im1->image.b[i]);
	 break;
       case IM_SHORT:
	 printf("%6d",im1->image.s[i]);
	 break;
       case IM_LONG:
	 printf("%9ld",(long)im1->image.l[i]);
	 break;
       case IM_FLOAT:
	 printf("%9g",im1->image.f[i]);
	 break;
       case IM_DOUBLE:
	 printf("%9g",im1->image.d[i]);
	 break;
       case IM_COMPLEX:
	 printf("%4g+i%4g",im1->image.c[i].r,im1->image.c[i].i);
	 break;
      }
      printf("%c"," \n"[i%8==7]);
   }
   printf("\n");

   free_image(im1);
   free_image(im2);
}


/* Hand code an object for testing purposes.
   This uses "nasty" code - don't take this as an example
   how to use ip object routines!
*/
object *create_object(coord origin)
{
   object *obj;
   static rletype run[11] = {
      {0,4}, {7,4},
      {0,5}, {7,4},
      {0,6}, {7,4},
      {0,14},
      {0,11},
      {0,11},
      {1,10},
      {11,1}
   };

   obj = alloc_object(origin);
   
   obj->bbox.br.x = obj->bbox.tl.x + 13;
   obj->bbox.br.y = obj->bbox.tl.y + 7;
   obj->numrows = 8;
   obj->numpixels = 74;
   obj->row = malloc(8 * sizeof(rowtype));

   obj->row[0].rle = run;
   obj->row[0].numentries = 2;
   obj->row[1].rle = run+2;
   obj->row[1].numentries = 2;
   obj->row[2].rle = run+4;
   obj->row[2].numentries = 2;
   obj->row[3].rle = run+6;
   obj->row[3].numentries = 1;
   obj->row[4].rle = run+7;
   obj->row[4].numentries = 1;
   obj->row[5].rle = run+8;
   obj->row[5].numentries = 1;
   obj->row[6].rle = run+9;
   obj->row[6].numentries = 1;
   obj->row[7].rle = run+10;
   obj->row[7].numentries = 1;

   obj->status |= 0x0001U;	/* Indicate 'completed' object */

   return obj;
}


void test_object_measure(void)
{
   image *im;
   coord origin,pt;
   object *obj;
   double length;

   im = alloc_image(IM_BYTE,256,256);
   origin.x = origin.y = 0;

   pt.x = pt.y = 10;
   obj = create_object(pt);

   im_crtest(im,origin,100.0,0.0,TI_CONST);
   dispim(im);
   dispid = display_object_over_image(dispid, im,obj, PEN_WHITE, FLG_OPAQUE);
   length = measure_object_integral(im, obj);
   printf("integral = %g\n",length);
   length = measure_boundary_mean(im, obj);
   printf("bnd mean = %g\n",length);

   im_crtest(im,origin,1.0,0.0,TI_HWEDGE);
   dispim(im);
   dispid = display_object_over_image(dispid, im,obj, PEN_WHITE, FLG_OPAQUE);
   length = measure_object_integral(im, obj);
   printf("integral = %g\n",length);
   length = measure_boundary_mean(im, obj);
   printf("bnd mean = %g\n",length);
}


#if 0
void test_classify(int argc, char *argv[])
{
   database *db1,*db2;
   treenode *tree;
   double alpha;
   int i,k;
   int thiscnt,lastcnt;

   if (argc != 4) {
      fprintf(stderr,"Usage: test <image>\n");
      exit(1);
   }

   db1 = read_classification_database(argv[1]);
   db2 = read_classification_database(argv[2]);

   alpha = atof(argv[3]);

   printf("%g\n",alpha);

   printf("Database 1: %s\n",argv[1]);
   printf("Classes: %d\n",db1->numclasses);
   printf("Features: %d\n",db1->numfeatures);
   printf("Entries: %d\n",db1->numentries);
   printf("Allocated: %d\n",db1->allocated);

   printf("Num in class:\n");
   for (i=0; i<db1->numclasses; i++)
      printf("  (%d) %s - %d\n",i,db1->classmap[i],db1->classnum[i]);

   printf("Database 2: %s\n",argv[2]);
   printf("Classes: %d\n",db2->numclasses);
   printf("Features: %d\n",db2->numfeatures);
   printf("Entries: %d\n",db2->numentries);
   printf("Allocated: %d\n",db2->allocated);

   printf("Num in class:\n");
   for (i=0; i<db2->numclasses; i++)
      printf("  (%d) %s - %d\n",i,db2->classmap[i],db2->classnum[i]);

   printf("Interation 0 (xx,xx): Growing L1, pruning L2\n");
   tree = db_classify(db1,alpha);
   lastcnt = tree_numnodes(tree);

   printf("\nTree root:\n  entries=%d,  class=%d,  impurity=%g\n",
	  tree->numentries, tree->class, tree->impurity);
   printf("  Cut on feature %d at %g\n",tree->rule.feature,tree->rule.cut);

/*   view_tree(tree); */

   tree_prune(tree,db2,alpha);
   thiscnt = tree_numnodes(tree);

/*   view_tree(tree); */


   for (k = 1; lastcnt != thiscnt; k++) {
      printf("Iteration %d (%d,%d):",k,lastcnt,thiscnt);
      if (k % 2 == 0) {
	 printf(" Growing L1, pruning L2\n");
	 tree_regrow(tree,db1,alpha);
/*	 view_tree(tree); */
	 tree_prune(tree,db2,alpha);
      }else{
	 printf(" Growing L2, pruning L1\n");
	 tree_regrow(tree,db2,alpha);
/*	 view_tree(tree); */
	 tree_prune(tree,db1,alpha);
      }
      view_tree(tree);
      lastcnt = thiscnt;
      thiscnt = tree_numnodes(tree);
   }
   
   printf("Done: (%d,%d)\n",lastcnt,thiscnt);

/*   view_tree(tree); */

   free_tree(tree);

   free_database(db1);
   free_database(db2);


}

#endif


void test_filetype(char *fn)
{
   int i;
   static char *ft_msg[] = {
      "FT_ERROR",
      "FT_UNKNOWN",
      "FT_VISILOG",
      "FT_CIMAGES",
      "FT_TIFF",
      "FT_PPM",
      "FT_VIFF"
   };

   i = ip_filetype(fn);

   printf("Filetype return = %d ",i);
   if (i>= -1 && i <= 5)
      printf("(%s)",ft_msg[i+1]);
   printf("\n");
}



void test_polyfit(void)
{
   vector2d pts[20];
   vector2d tvec;
   double xpos[20];
   double val[20];
   poly *p;
   double chisq;
   int i;
   int option;
   int order;
   char *choice[] = {
      "Test one-dimensional fit",
      "Test two-dimensional fit"
   };


   do {
      putchar('\n');

      option = ask_choice(choice, 2, 1);

      switch (option) {

       case 0:			/* one-dimensional */
	 order = ask_double("Order of polynomial to use", 0, 64);

	 printf("\nDoing one-dimensional (order %d) fit.\n"
		"  Poly = 10  - 7.456x + 21.5x^2\n\n",order);

	 for (i=0; i<12; i++) {
	    xpos[i] = (i-5) * 30;
	    val[i] = 10 - 7.456*xpos[i]  + 21.5*xpos[i]*xpos[i];
	 }

	 p = poly_fit1d(12, xpos, val, 0.0, order, 0x00, &chisq);
	 if (p) 
	    printf("RMS = %g.\n",sqrt(chisq));
	 
	 dump_poly(p);

	 if (p) {
	    for (i=0; i<12; i++) {
	       printf("%2d: p(%g)=%g   poly = %g\n",
		      i, xpos[i], val[i],
		      poly_evaluate(p, xpos[i], 0.0));
	    }
	    free_poly(p);
	 }
	 break;

       case 1:
	 order = ask_double("Order of polynomial to use", 0, 16);

	 printf("\nDoing two-dimensional (order %d) fit.\n"
		"  Poly = 10 + 21.5y -7.456xy\n\n",order);

	 for (i=0; i<12; i++) {
	    pts[i].x = (i%3) * 250;
	    pts[i].y = (i/3) * 350;
	    val[i] = 10 + 21.5 * pts[i].y - 7.456 * pts[i].x * pts[i].y;
	 }

	 tvec.x = tvec.y = 0;
	 p = poly_fit2d(12, pts, val, tvec, order, 0, &chisq);
	 if (p) 
	    printf("RMS = %g.\n",sqrt(chisq));

	 dump_poly(p);

	 if (p) {
	    for (i=0; i<12; i++) {
	       printf("%2d: (%3g,%3g)=%g   poly = %g\n",
		      i, pts[i].x, pts[i].y, val[i],
		      poly_evaluate(p, pts[i].x, pts[i].y));
	    }
	    free_poly(p);
	 }

	 break;
      }
   } while (option >= 0);
}



void test_graphics(void)
{
   image *im;
   int i;
   coord start, end, posn;
   box tbox;

   display_palette(PAL_GREYOBJ);

   im = alloc_image(IM_BYTE, 256,256);
   im_clear(im);

   /* Test line drawing. */

   tbox.origin.x = tbox.origin.y = 0;
   tbox.size.x = tbox.size.y = 235;
   im_drawbox(im, PEN_WHITE, tbox);

   for (i = 20; i < 244; i+=20) {
      start.x = start.y = 11;
      end.x = 243;
      end.y = i;
      im_drawline(im, PEN_RED, start, end);
   }

   for (i = 20; i < 243; i+=20) {
      start.x = 11;
      start.y = 243;
      end.x = 243;
      end.y = i;
      im_drawline(im, PEN_GREEN, start, end);
   }

   for (i = 20; i < 243; i+=20) {
      start.x = start.y = 11;
      end.x = i;
      end.y = 243;
      im_drawline(im, PEN_BLUE, start, end);
   }


   for (i = 20; i < 243; i+=20) {
      start.x = 11;
      start.y = 243;
      end.x = i;
      end.y = 11;
      im_drawline(im, PEN_YELLOW, start, end);
   }


   start.x = start.y = 10;
   end.x = end.y = 244;
   im_drawline(im, PEN_WHITE, start, end);
   start.x = 244;
   start.y = 10;
   end.x = 10;
   end.y = 244;
   im_drawline(im, PEN_WHITE,  start, end);

   dispim(im);

   /* Test circle/ellipses */

   im_clear(im);

   for (i=0; i<32; i++) {
      posn.x = posn.y = 128;
      im_drawellipse(im, PEN_BLUE-31+i, posn, i+2,i+2);
      im_drawellipse(im, PEN_YELLOW-31+i, posn, 2*i+32,2*i+50);
   }
   posn.x = 256;
   posn.y = 0;
   dispid = display_image(dispid, im, posn);

   /* Test arrows, etc. */

   im_clear(im);

   for (i=0; i<8; i++) {
      posn.x = posn.y = 128;
      im_drawarrow(im, PEN_BLUE, posn, i*M_PI/8.0, (4*i)+1);
      posn.x = 100+i*7;
      posn.y = 128;
      im_drawarrow(im, PEN_YELLOW, posn,  i*M_PI/8.0+M_PI,(i*18)+1);
   }
   posn.x = 0;
   posn.y = 256;
   dispid = display_image(dispid, im, posn);

   ip_wait_for_quit();
   display_palette(PAL_GREY);
   
   free_image(im);
}


void test_create(void)
{
   image *im, *tim;
   int option;
   int modified;
   coord botright;
   coord origin, size, steppos, pt;
   coord start,end;
   coord lower, upper;
   box tbox;
   int type;
   int i;
   char *cptr;
   double pixmin, pixmax;
   double value, stepvalx, stepvaly, incval;
   static char newim[100] = "New image type/size and clear it [BYTE 256x256]";
   static char *options[]= {
      "Load local create image from global image.",
      newim,
      "Put newly created image back into global image.",
      "Modify single pixel value.",
      "Modify an array of pixel values.",
      "Add in constant region.",
      "Add in vertical ramp",
      "Add in horizontal ramp",
      "Add in cone",
      "Add in Gaussian",
      "Add in line",
      "Add in vertical lines",
      "Add in horizontal lines",
      "Add in box",
      "Add in circle/ellipse"
   };
   static char *imtypes[] = {
      "BYTE", "SHORT", "LONG", "FLOAT", "DOUBLE",
   };
   static int types[] = {
      IM_BYTE, IM_SHORT, IM_LONG, IM_FLOAT, IM_DOUBLE
   };
   static double typemin[] = {
      0, SHRT_MINV, LONG_MINV, FLT_MIN, DBL_MIN
   };
   static double typemax[] = {
      UBYTE_MAXV, SHRT_MAXV, LONG_MAXV, FLT_MAX, DBL_MAX
   };

   im = alloc_image(IM_BYTE, 256, 256);
   im_clear(im);
   modified = FALSE;
   botright.x = botright.y = 255;
   pixmin = typemin[im->type];
   pixmax = typemax[im->type];

   do {
      option = ask_choice(options, 15, 1);

      if (option >= 0) modified = TRUE;

      switch (option) {
       case 0:			/* Use global image. */
	 free_image(im);
	 im = im_copy(gim);
	 botright.x = im->size.x-1;
	 botright.y = im->size.y-1;
	 pixmin = typemin[im->type];
	 pixmax = typemax[im->type];
	 cptr = strrchr(newim, '[');
	 cptr++;
	 sprintf(cptr, "%s %dx%d]", imtypes[im->type], im->size.x, im->size.y);
	 break;

       case 1:			/* New image/type */
	 type = ask_choice(imtypes, 5, 0);
	 lower.x = lower.y = 0;
	 upper.x = upper.y = 512;
	 size = ask_coord("Size", lower, upper);
	 free_image(im);
	 im = alloc_image(types[type], size.x, size.y);
	 im_clear(im);
	 dispim(im);
	 botright.x = im->size.x-1;
	 botright.y = im->size.y-1;
	 pixmin = typemin[im->type];
	 pixmax = typemax[im->type];
	 cptr = strrchr(newim, '[');
	 cptr++;
	 sprintf(cptr, "%s %dx%d]", imtypes[type], size.x, size.y);
	 break;
       case 2:			/* Save to global */
	 free_image(gim);
	 gim = im_copy(im);
	 modified = FALSE;
	 break;
       case 3:			/* Point */
	 lower.x = lower.y = 0;
	 origin = ask_coord("Coordinate", lower, botright);
	 value = ask_double("Pixel value",pixmin,pixmax);
	 im_drawpoint(im, value, origin);
	 dispim(im);
	 break;
       case 4:			/* Array of points */
	 lower.x = lower.y = 0;
	 origin = ask_coord("Start coordinate", lower, botright);
	 size = ask_coord("End coordinate", origin, botright);
	 printf("start %dx%d   end %d,%d\n",origin.x,origin.y,size.x,size.y);
	 lower.x = lower.y = 1;
	 upper.x = size.x-origin.x+1;
	 upper.y = size.y-origin.y+1;
	 steppos = ask_coord("Step increment in each direction",
			     lower, upper);
	 value = ask_double("Start pixel value",pixmin,pixmax);
	 stepvalx = ask_double("Pixel value increment in x direction",
			       im->type == IM_BYTE ? -pixmax : pixmin,
			       pixmax);
	 stepvaly = ask_double("Pixel value increment in y direction",
			       im->type == IM_BYTE ? -pixmax : pixmin,
			       pixmax);
	 for (pt.y=origin.y; pt.y<=size.y; pt.y+=steppos.y) {
	    incval = value;
	    for (pt.x=origin.x; pt.x<=size.x; pt.x+=steppos.x) {
	       printf("%d,%d-%g\n",pt.x,pt.y,incval);
	       im_drawpoint(im, incval, pt);
	       incval += stepvalx;
	       if (incval > pixmax) incval = pixmax;
	       if (incval < pixmin) incval = pixmin;
	    }
	    value += stepvaly;
	    if (value > pixmax) value = pixmax;
	    if (value < pixmin) value = pixmin;
	 }
	 dispim(im);
	 break;

       case 5:			/* Rectangle */
	 lower.x = lower.y = 0;
	 origin = ask_coord("Origin", lower, botright);
	 lower.x = lower.y = 1;
	 upper.x = im->size.x-origin.x;
	 upper.y = im->size.y-origin.y;
	 size = ask_coord("Size", lower, upper);
	 value = ask_double("Pixel value",pixmin,pixmax);
	 tim = alloc_image(im->type, size.x, size.y);
	 im_set(tim,value);
	 lower.x = lower.y = 0;
	 im_insert(im, origin, tim, lower, size);
	 free_image(tim);
	 dispim(im);
	 break;

       case 6:			/* V-wedge */
	 lower.x = lower.y = 0;
	 origin = ask_coord("Origin", lower, botright);
	 lower.x = lower.y = 1;
	 upper.x = im->size.x-origin.x;
	 upper.y = im->size.y-origin.y;
	 size = ask_coord("Size", lower, upper);
	 value = ask_double("Start pixel value",pixmin,pixmax);
	 stepvalx = ask_double("End pixel value",pixmin,pixmax);
	 tim = alloc_image(im->type, size.x, size.y);
	 lower.x = lower.y = 0;
	 im_create(tim, lower,
		   (stepvalx-value)/(size.x-1), value, 1, TI_HWEDGE);
	 im_insert(im, origin, tim, lower, size);
	 free_image(tim);
	 dispim(im);
	 break;

       case 7:			/* H-wedge */
	 lower.x = lower.y = 0;
	 origin = ask_coord("Origin", lower, botright);
	 lower.x = lower.y = 1;
	 upper.x = im->size.x-origin.x;
	 upper.y = im->size.y-origin.y;
	 size = ask_coord("Size", lower, upper);
	 value = ask_double("Start pixel value",pixmin,pixmax);
	 stepvaly = ask_double("End pixel value",pixmin,pixmax);
	 tim = alloc_image(im->type, size.x, size.y);
	 lower.x = lower.y = 0;
	 im_create(tim, lower,
		   (stepvaly-value)/(size.y-1), value, 1, TI_VWEDGE);
	 im_insert(im, origin, tim, lower, size);
	 free_image(tim);
	 dispim(im);
	 break;

       case 8:			/* Cone */
#if 0
	 lower.x = lower.y = 0;
	 origin = ask_coord("Origin", lower, botright);
	 radius = ask_double("Radius", 1, im->size.x);
	 value = ask_double("Centre pixel value",pixmin,pixmax);
	 stepvalx = ask_double("Edge pixel value",pixmin,pixmax);
#endif
	 not_implemented();
	 break;

       case 9:			/* Gaussian */
	 not_implemented();
	 break;

       case 10:			/* Line */
	 lower.x = lower.y = 0;
	 origin = ask_coord("Start point", lower, botright);
	 size = ask_coord("End point", lower, botright);
	 value = ask_double("Pixel value",pixmin,pixmax);
	 im_drawline(im, value, origin, size);
	 dispim(im);
	 break;

       case 11:			/* Vertical lines */
	 lower.x = lower.y = 0;
	 origin = ask_coord("Top-left coordinate", lower, botright);
	 size = ask_coord("Bottom-right coordinate", origin, botright);
	 steppos.x = ask_double("Line spacing in x-direction",
				1,size.x-origin.x+1);
	 value = ask_double("Start pixel value",pixmin,pixmax);
	 stepvalx = ask_double("End pixel value",pixmin,pixmax);
	 incval = value;
	 for (i=origin.x; i<=size.x; i+=steppos.x) {
	    lower.x = i;
	    lower.y = origin.y;
	    upper.x = i;
	    upper.y = size.y;
	    im_drawline(im, incval, lower, upper);
	    incval += (stepvalx-value)/steppos.x;
	 }
	 dispim(im);
	 break;

       case 12:			/* Horizontal lines */
	 lower.x = lower.y = 0;
	 origin = ask_coord("Top-left coordinate", lower, botright);
	 size = ask_coord("Bottom-right coordinate", origin, botright);
	 steppos.y = ask_double("Line spacing in y-direction",
				1,size.y-origin.y+1);
	 value = ask_double("Start pixel value",pixmin,pixmax);
	 stepvaly = ask_double("End pixel value",pixmin,pixmax);
	 incval = value;
	 for (i=origin.y; i<=size.y; i+=steppos.y) {
	    lower.x = origin.x;
	    lower.y = i;
	    upper.x = size.x;
	    upper.y = i;
	    im_drawline(im, incval, lower, upper);
	    incval += (stepvaly-value)/steppos.y;
	 }
	 dispim(im);
	 break;

       case 13:			/* Box */
	 lower.x = lower.y = 0;
	 tbox.origin = ask_coord("Top-left coordinate", lower, botright);
	 lower.x = lower.y = 1;
	 upper.x = botright.x-origin.x;
	 upper.y = botright.y-origin.y;
	 tbox.size = ask_coord("Size",lower, upper);
	 value = ask_double("Pixel value",pixmin,pixmax);
	 im_drawbox(im, value, tbox);
	 dispim(im);
	 break;

       case 14:			/* Circle/ellipse */
	 lower.x = lower.y = 0;
	 origin = ask_coord("Top-left coordinate", lower, botright);
	 size.x = ask_double("Parameter a",1,im->size.x);
	 size.y = ask_double("Parameter b",1,im->size.y);
	 value = ask_double("Pixel value",pixmin,pixmax);
	 im_drawellipse(im,value, origin, size.x, size.y);
	 dispim(im);
	 break;
      }
   } while (option >= 0);

   chk_modified(modified, im);

   free_image(im); 
}



void test_morph(void)
{
   image *im;
   int option;
   int modified;
   int size, num, denom;
   char *options[]= {
      "Re-load test image from global test image.",
      "Put local test image into global image.",
      "Put local test image into reference image.",
      "Load internal test image.",
      "Run erode at specified size.",
      "Run dilate at specified size.",
      "Run open at specified size.",
      "Run close at specified size.",
      "Run lerode at specified angle and size.",
      "Run ldilate at specified angle and size.",
      "Run lopen at specified angle and size.",
      "Run lclose at specified angle and size."
   };

   im = im_copy(gim);
   modified = FALSE;

   do {
      option = ask_choice(options, 12, 1);

      if (option >= 0) modified = TRUE;

      switch (option) {
       case 0:
	 free_image(im);
	 im = im_copy(gim);
	 dispim(im);
	 modified = FALSE;
	 break;
       case 1:
	 free_image(gim);
	 gim = im_copy(im);
	 modified = FALSE;
	 break;
       case 2:
	 free_image(rim);
	 rim = im_copy(im);
	 modified = FALSE;
	 break;
       case 3:
	 free_image(im);
	 im = alloc_image(IM_BYTE, 256,256);
	 im_clear(im);
	 im_drawboxz(im, 255, 10,10,  235,235);
	 im_drawlinez(im, 128, 11,11,  244,244);
	 im_drawlinez(im, 128, 244,11, 11,244);
	 im_drawlinez(im, 128, 51,52,  200,201);
	 im_drawlinez(im, 128, 201,53, 52,202);
	 dispim(im);
	 break;
       case 4:
	 size = ask_double("Erode mask size",1,im->size.x);
	 timeit_Ii(im_erode, im, size);
	 dispim(im);
	 break;
       case 5:
	 size = ask_double("Dilation mask size",1,im->size.x);
	 timeit_Ii(im_dilate, im, size);
	 dispim(im);
	 break;
       case 6:
	 size = ask_double("Open mask size",1,im->size.x);
	 timeit_Ii(im_open, im, size);
	 dispim(im);
	 break;
       case 7:
	 size = ask_double("Close mask size",1,im->size.x);
	 timeit_Ii(im_close, im, size);
	 dispim(im);
	 break;
       case 8:
	 size = ask_double("Linear erode size",1,im->size.x);
	 denom = ask_double("Linear erode rotation denominator",1,1000000);
	 num = ask_double("Linear erode rotation numerator",0,denom-1);
	 timeit_Iiii(im_lerode, im, size, num, denom);
	 dispim(im);
	 break;
       case 9:
	 size = ask_double("Linear dilation size",1,im->size.x);
	 denom = ask_double("Linear dilation rotation denominator",1,1000000);
	 num = ask_double("Linear dilation rotation numerator",0,denom-1);
	 timeit_Iiii(im_ldilate, im, size, num, denom);
	 dispim(im);
	 break;
       case 10:
	 size = ask_double("Linear open size",1,im->size.x);
	 denom = ask_double("Linear open rotation denominator",1,1000000);
	 num = ask_double("Linear open rotation numerator",0,denom-1);
	 timeit_Iiii(im_lopen, im, size, num, denom);
	 dispim(im);
	 break;
       case 11:
	 size = ask_double("Linear close size",1,im->size.x);
	 denom = ask_double("Linear close rotation denominator",1,1000000);
	 num = ask_double("Linear close rotation numerator",0,denom-1);
	 timeit_Iiii(im_lclose, im, size, num, denom);
	 dispim(im);
	 break;
      }
   } while (option >= 0);

   chk_modified(modified, im);

   free_image(im);
}


void test_filters(void)
{
   image *im;
   int i;
   int option;
   int size;
   int modified;
   long *kernel;
   char *options[]= {
      "Re-load local image from global image.",
      "Put local image into global image.",
      "Put local image into reference image.",
      "Median filter with specified mask size.",
      "Mean filter with specified mask size.",
      "Convolution mean filter with specified mask size.",
      "Horizontal Sobel filter.",
      "Vertical Sobel filter.",
      "Magnitude of Sobel operator.",
      "Run experiment.",
   };

   im = im_copy(gim);
   modified = FALSE;

   dispim(im);

   do {
      option = ask_choice(options, 10, 1);

      if (option >= 0) {
	 modified = TRUE;
      }

      switch (option) {
       case 0:
	 free_image(im);
	 im = im_copy(gim);
	 dispim(im);
	 modified = FALSE;
	 break;
       case 1:
	 free_image(gim);
	 gim = im_copy(im);
	 modified = FALSE;
	 break;
       case 2:
	 free_image(rim);
	 rim = im_copy(im);
	 modified = FALSE;
	 break;
       case 3:
	 size = ask_double("Median filter mask size",1,im->size.x);
	 timeit_Iif(im_median, im, size, NO_FLAG);
	 dispim(im);
	 break;
       case 4:
	 size = ask_double("Mean filter mask size",1,im->size.x);
	 timeit_Iif(im_mean, im, size, NO_FLAG);
	 dispim(im);
	 break;
       case 5:
	 size = ask_double("Convolution filter mask size",1,im->size.x);
	 kernel = ip_mallocx(size*size*sizeof(long));
	 if (kernel) {
	    for (i=0; i<size*size; i++) kernel[i] = 1;
	    timeit_Ivif(im_convolve, im, kernel, size, FLG_NORM);
	    ip_free(kernel);
	 }
	 dispim(im);
	 break;

       case 6:
	 timeit_If(im_sobel, im, FLG_FHORIZ | FLG_ZERO);
	 dispim(im);
	 break;

       case 7:
	 timeit_If(im_sobel, im, FLG_FVERT | FLG_ZERO);
	 dispim(im);
	 break;

       case 8:
	 timeit_If(im_sobel, im, FLG_FMAG | FLG_ZERO);
	 dispim(im);
	 break;

       case 9:
	 size = ask_double("Convolution filter mask size",1,im->size.x);
	 {
	    image *delta, *mean_delta, *prob, *mask;
	    int i,j,val;
	    UBYTE *dp,*sp;
	    ROI *roi;
	    stats st;

	    delta = im_copy(im);
	    im_mean(delta, size, NO_FLAG);
	    for (j=0; j<delta->size.y; j++) {
	       dp = im_rowadr(delta, j);
	       sp = im_rowadr(im, j);
	       for (i=0; i<delta->size.x; i++) {
		  val = (int)(*dp) - *sp;
		  if (val < 0) val = -val;
		  *dp = (UBYTE)val;
		  sp++;
		  dp++;
	       }
	    }
	    dispim(delta);
	    ask_yesno("Delta");
	    mean_delta = im_copy(delta);
	    im_mean(mean_delta, size, NO_FLAG);
	    dispim(mean_delta);
	    ask_yesno("Mean delta");
	    prob = im_convert(delta, IM_FLOAT, NO_FLAG);
	    free_image(delta);
	    im_replace(mean_delta, 1, 0);
	    delta = im_convert(mean_delta, IM_FLOAT, NO_FLAG);
	    free_image(mean_delta);
	    im_div(prob, delta, NO_FLAG);
	    free_image(delta);
	    roi = alloc_roi(ROI_IMAGE, prob);
	    roi_modify(ROI_REDUCE, roi, size);
	    im_stats(prob, &st, roi);
	    free_roi(roi);
	    printf("Min = %g, Max = %g, Mean = %g,\n",st.min, st.max, st.mean);
	    im_clear_border(prob, size);
	    dispim(prob);
	    ask_yesno("Probability, go on");
	    mask = im_threshold(prob, 1, st.max);
	    dispim(mask);
	    ask_yesno("Thresholded, go on");
	    free_image(prob);
	    delta = im_copy(im);
	    im_median(delta, size, NO_FLAG);
	    im_mask(delta, mask, 0, NO_FLAG);
	    im_mask(im, mask, 0, FLG_INVERSE);
	    im_add(im, delta, NO_FLAG);
	    free_image(delta);
	    free_image(mask);
	 } 
	 dispim(im);
	 break;
      }
   } while (option >= 0);

   chk_modified(modified, im);

   free_image(im);
}




void test_convert(void)
{
   image *im, *d, *c;
   int i;

   d = NULL;
   im = alloc_image(IM_LONG, 8, 8);
   for (i=0; i<48; i++) {
      im->image.l[i] = i * 16;
   }
   im->image.l[48] = 254;
   im->image.l[49] = 255;
   im->image.l[50] = 256;
   im->image.l[51] = 257;
   im->image.l[52] = -256;
   im->image.l[53] = -255;
   im->image.l[54] = -1;
   im->image.l[55] = 0;
   
   im->image.l[56] = LONG_MINV;
   im->image.l[57] = LONG_MAXV;
   im->image.l[58] = SHRT_MINV-1;
   im->image.l[59] = SHRT_MINV;
   im->image.l[60] = SHRT_MINV+1;
   im->image.l[61] = SHRT_MAXV-1;
   im->image.l[62] = SHRT_MAXV;
   im->image.l[63] = SHRT_MAXV+1;

   printf("Original image:\n");
   dump_image(im, DBG_VERBOSE);

   printf("CONVERT: LONG to BYTE, NO_FLAG\n");
   d = im_convert(im, IM_BYTE, NO_FLAG);
   dump_image(d, DBG_VERBOSE);
   free_image(d);

   printf("CONVERT: LONG to BYTE, FLG_CLIP\n");
   if (d) {
      d = im_convert(im, IM_BYTE, FLG_CLIP);
      dump_image(d, DBG_VERBOSE);
      free_image(d);
   }

   printf("CONVERT: LONG to BYTE, FLG_MODULO\n");
   if (d) {
      d = im_convert(im, IM_BYTE, FLG_MODULO);
      dump_image(d, DBG_VERBOSE);
      free_image(d);
   }

   while (NOT ask_yesno("\nReady to go on yet")) ;

   printf("\nOriginal image:\n");
   dump_image(im, DBG_VERBOSE);

   printf("CONVERT: LONG to SHORT, NO_FLAG\n");
   if (d) {
      d = im_convert(im, IM_SHORT, NO_FLAG);
      dump_image(d, DBG_VERBOSE);
      free_image(d);
   }

   printf("CONVERT: LONG to SHORT, FLG_CLIP\n");
   if (d) {
      d = im_convert(im, IM_SHORT, FLG_CLIP);
      dump_image(d, DBG_VERBOSE);
      free_image(d);
   }

   printf("CONVERT: LONG to SHORT, FLG_MODULO\n");
   if (d) {
      d = im_convert(im, IM_SHORT, FLG_MODULO);
      dump_image(d, DBG_VERBOSE);
      free_image(d);
   }

   free_image(im);
   while (NOT ask_yesno("\nReady to go on yet")) ;

   im = alloc_image(IM_FLOAT, 8, 8);
   for (i=0; i<40; i++) {
      im->image.f[i] = i * 16;
   }
   im->image.f[40] = 0.000001;
   im->image.f[41] = 0.499999;
   im->image.f[42] = 0.5;
   im->image.f[43] = 0.9;
   im->image.f[44] = -0.9;
   im->image.f[45] = -0.5;
   im->image.f[46] = FLT_MIN;
   im->image.f[47] = FLT_MAX;
   
   im->image.f[48] = 254;
   im->image.f[49] = 255;
   im->image.f[50] = 256;
   im->image.f[51] = 257;
   im->image.f[52] = -256;
   im->image.f[53] = -255;
   im->image.f[54] = -1;
   im->image.f[55] = 0;
   
   im->image.f[56] = LONG_MINV;
   im->image.f[57] = LONG_MAXV;
   im->image.f[58] = SHRT_MINV-1;
   im->image.f[59] = SHRT_MINV;
   im->image.f[60] = SHRT_MINV+1;
   im->image.f[61] = SHRT_MAXV-1;
   im->image.f[62] = SHRT_MAXV;
   im->image.f[63] = SHRT_MAXV+1;

   printf("\nOriginal image:\n");
   dump_image(im, DBG_VERBOSE);

   printf("CONVERT: FLOAT to BYTE, NO_FLAG\n");
   if (d) {
      d = im_convert(im, IM_BYTE, NO_FLAG);
      dump_image(d, DBG_VERBOSE);
      free_image(d);
   }

   printf("CONVERT: FLOAT to BYTE, FLG_CLIP\n");
   if (d) {
      d = im_convert(im, IM_BYTE, FLG_CLIP);
      dump_image(d, DBG_VERBOSE);
      free_image(d);
   }

   printf("CONVERT: FLOAT to BYTE, FLG_MODULO\n");
   if (d) {
      d = im_convert(im, IM_BYTE, FLG_MODULO);
      dump_image(d, DBG_VERBOSE);
      free_image(d);
   }

   while (NOT ask_yesno("\nReady to go on yet")) ;

   printf("\nOriginal image:\n");
   dump_image(im, DBG_VERBOSE);

   printf("CONVERT: FLOAT to SHORT, NO_FLAG\n");
   if (d) {
      d = im_convert(im, IM_SHORT, NO_FLAG);
      dump_image(d, DBG_VERBOSE);
      free_image(d);
   }

   printf("CONVERT: FLOAT to SHORT, FLG_CLIP\n");
   if (d) {
      d = im_convert(im, IM_SHORT, FLG_CLIP);
      dump_image(d, DBG_VERBOSE);
      free_image(d);
   }

   printf("CONVERT: FLOAT to SHORT, FLG_MODULO\n");
   if (d) {
      d = im_convert(im, IM_SHORT, FLG_MODULO);
      dump_image(d, DBG_VERBOSE);
      free_image(d);
   }

   while (NOT ask_yesno("\nReady to go on yet")) ;

   printf("\nOriginal image:\n");
   dump_image(im, DBG_VERBOSE);

   printf("CONVERT: FLOAT to COMPLEX, FLG_REAL\n");
   if (d) {
      d = im_convert(im, IM_COMPLEX, FLG_REAL);
      dump_image(d, DBG_VERBOSE);
   }

   if (d) {
      printf("And back again... FLG_REAL\n");
      c = im_convert(d, IM_FLOAT, FLG_REAL);
      if (c) {
	 dump_image(c, DBG_VERBOSE);
	 free_image(c);
      }
      free_image(d);
   }

   printf("CONVERT: FLOAT to COMPLEX, FLG_IMAG\n");
   if (d) {
      d = im_convert(im, IM_COMPLEX, FLG_IMAG);
      dump_image(d, DBG_VERBOSE);
      free_image(d);
   }

   if (d) {
      printf("And back again... FLG_MAG\n");
      c = im_convert(d, IM_FLOAT, FLG_MAG);
      if (c) {
	 dump_image(c, DBG_VERBOSE);
	 free_image(c);
      }
      free_image(d);
   }


}



void test_fft(void)
{
   image *ft, *d;

   printf("\nTesting im_fft()\n");

   ft = alloc_image(IM_COMPLEX, 128, 128);
   im_clear(ft);

   ft->image.c[X(ft,0,0)].r = 100;

   d = im_convert(ft, IM_FLOAT, FLG_REAL);
   dispim(d);
   free_image(d);
   ask_yesno("\nSet image coord (0,0) to 100. Ready to continue");

   im_fft(ft, 1);

   d = im_convert(ft, IM_FLOAT, FLG_MAG);
   dispid = display_imagez(dispid, d, 128,0);
   free_image(d);
   ask_yesno("\nDisplaying FFT. Ready to continue for iFFT");

   im_fft(ft, -1);

   d = im_convert(ft, IM_FLOAT, FLG_MAG);
   dispid = display_imagez(dispid, d, 256,0);
   free_image(d);
   ask_yesno("\nDisplaying inverse FFT. Ready to continue");

   free_image(ft);

   d = alloc_image(IM_FLOAT, 128, 128);
   im_set(d, 100);
   ft = im_convert(d, IM_COMPLEX, FLG_REAL);
   dispid = display_imagez(dispid, d, 0,128);
   free_image(d);

   ask_yesno("\nSet whole image to 100. Ready to continue");

   im_fft(ft, 1);

   d = im_convert(ft, IM_FLOAT, FLG_MAG);
   dispid = display_imagez(dispid, d, 128,128);
   free_image(d);
   ask_yesno("\nDisplaying FFT. Ready to continue for iFFT");

   im_fft(ft, -1);

   d = im_convert(ft, IM_FLOAT, FLG_MAG);
   dispid = display_imagez(dispid, d, 256,128);
   free_image(d);
   ask_yesno("\nDisplaying inverse FFT. Ready to continue");


   d = alloc_image(IM_FLOAT, 128, 128);
   im_set(d, 100);
   ft = im_convert(d, IM_COMPLEX, FLG_REAL);
   dispid = display_imagez(dispid, d, 0,256);
   free_image(d);

   ask_yesno("\nSet whole image to 100. Ready to continue");

   im_fft(ft, FLG_NORM);

   d = im_convert(ft, IM_FLOAT, FLG_MAG);
   dispid = display_imagez(dispid, d, 128,256);
   free_image(d);
   ask_yesno("\nDisplaying normalised FFT. Ready to continue for iFFT");

   im_fft(ft, FLG_INVERSE | FLG_NORM);

   d = im_convert(ft, IM_FLOAT, FLG_MAG);
   dispid = display_imagez(dispid, d, 256,256);
   free_image(d);
   ask_yesno("\nDisplaying normalised inverse FFT. Ready to continue");
}



void test_noise(void)
{
#if 0
   int i;
   double val, min, max, mean;
#endif
   double skew, amount;

#if 0
   printf("\nDoing 1000 calls to ip_rand_linear()\n\n");

   mean = 0.0;
   min = 3.0;
   max = -1.0;
   for (i=0; i<1000; i++) {
      val = ip_rand_linear();
      mean += val;
      if (val < min) min = val;
      if (val > max) max = val;
      printf("%8.6f%c",val," \n"[(i%4)==3]);
   }
   printf("\nMin = %g,  Max = %g,  Mean = %g.\n",min,max,mean/1000);

   ask_yesno("Go on?");

   printf("\nDoing 1000 calls to ip_rand_gaussian()\n\n");

   mean = 0.0;
   min = 3.0;
   max = -1.0;
   for (i=0; i<1000; i++) {
      val = ip_rand_gaussian();
      mean += val;
      if (val < min) min = val;
      if (val > max) max = val;
      printf("%8.6f%c",val," \n"[(i%4)==3]);
   }
   printf("\nMin = %g,  Max = %g,  Mean = %g.\n",min,max,mean/1000);

   ask_yesno("Go on?");
#endif

   amount = ask_double("Amount", 0.0, 1000000000.0);
   skew = ask_double("Skew", -100000000.0, 100000000.0);

   im_addnoise(gim, skew, amount, FLG_GAUSSIAN);
}


void timeit_I(int (*proc)(image *), image *im)
{
   clock_t start, finish;

   start = clock();
   proc(im);
   finish = clock();
   if (do_timing) {
      printf("Procedure took %g seconds.\n",
	     ((double)(finish-start)/CLOCKS_PER_SEC));
   }
}


void timeit_II(int (*proc)(image *, image *), image *dest, image *src)
{
   clock_t start, finish;

   start = clock();
   proc(dest, src);
   finish = clock();
   if (do_timing) {
      printf("Procedure took %g seconds.\n",
	     ((double)(finish-start)/CLOCKS_PER_SEC));
   }
}


void timeit_If(int (*proc)(image *, int), image *im, int f)
{
   clock_t start, finish;

   start = clock();
   proc(im, f);
   finish = clock();
   if (do_timing) {
      printf("Procedure took %g seconds.\n",
	     ((double)(finish-start)/CLOCKS_PER_SEC));
   }
}


void timeit_IIf(int (*proc)(image *, image *, int f), image *dest, image *src, int f)
{
   clock_t start, finish;

   start = clock();
   proc(dest, src, f);
   finish = clock();
   if (do_timing) {
      printf("Procedure took %g seconds.\n",
	     ((double)(finish-start)/CLOCKS_PER_SEC));
   }
}



void timeit_Iif(int (*proc)(image *, int, int), image *im, int i, int f)
{
   clock_t start, finish;

   start = clock();
   proc(im, i, f);
   finish = clock();
   if (do_timing) {
      printf("Procedure took %g seconds.\n",
	     ((double)(finish-start)/CLOCKS_PER_SEC));
   }
}

void timeit_Idf(int (*proc)(image *, double, int), image *im, double d, int f)
{
   clock_t start, finish;

   start = clock();
   proc(im, d, f);
   finish = clock();
   if (do_timing) {
      printf("Procedure took %g seconds.\n",
	     ((double)(finish-start)/CLOCKS_PER_SEC));
   }
}

void timeit_Iiif(int (*proc)(image *, int, int, int), image *im, int i, int j, int f)
{
   clock_t start, finish;

   start = clock();
   proc(im, i, j, f);
   finish = clock();
   if (do_timing) {
      printf("Procedure took %g seconds.\n",
	     ((double)(finish-start)/CLOCKS_PER_SEC));
   }
}


void timeit_Ipf(int (*proc)(image *, poly2d *, int), image *im, poly2d *p, int f)
{
   clock_t start, finish;

   start = clock();
   proc(im, p, f);
   finish = clock();
   if (do_timing) {
      printf("Procedure took %g seconds.\n",
	     ((double)(finish-start)/CLOCKS_PER_SEC));
   }
}


void timeit_IIpf(int (*proc)(image *, image *, poly2d *, int), image *dest, image *src, poly2d *p, int f)
{
   clock_t start, finish;

   start = clock();
   proc(dest, src, p, f);
   finish = clock();
   if (do_timing) {
      printf("Procedure took %g seconds.\n",
	     ((double)(finish-start)/CLOCKS_PER_SEC));
   }
}


void timeit_Ivif(int (*proc)(image *, void *, int, int), image *im, void *v, int i, int f)
{
   clock_t start, finish;

   start = clock();
   proc(im, v, i, f);
   finish = clock();
   if (do_timing) {
      printf("Procedure took %g seconds.\n",
	     ((double)(finish-start)/CLOCKS_PER_SEC));
   }
}


void timeit_ISR(int (*proc)(image *, stats *st, ROI *roi), image *im, stats *st, ROI *roi)
{
   clock_t start, finish;

   start = clock();
   proc(im, st, roi);
   finish = clock();
   if (do_timing) {
      printf("Procedure took %g seconds.\n",
	     ((double)(finish-start)/CLOCKS_PER_SEC));
   }
}


image * timeit_xIif(image * (*proc)(image *, int, int), image *im, int i, int f)
{
   image *res;
   clock_t start, finish;

   start = clock();
   res = proc(im, i, f);
   finish = clock();
   if (do_timing) {
      printf("Procedure took %g seconds.\n",
	     ((double)(finish-start)/CLOCKS_PER_SEC));
   }
   return res;
}

image * timeit_xIiif(image * (*proc)(image *, int, int, int), image *im, int i, int j, int f)
{
   image *res;
   clock_t start, finish;

   start = clock();
   res = proc(im, i, j, f);
   finish = clock();
   if (do_timing) {
      printf("Procedure took %g seconds.\n",
	     ((double)(finish-start)/CLOCKS_PER_SEC));
   }
   return res;
}


image * timeit_xIddf(image * (*proc)(image *, double, double, int), image *im, double i, double j, int f)
{
   image *res;
   clock_t start, finish;

   start = clock();
   res = proc(im, i, j, f);
   finish = clock();
   if (do_timing) {
      printf("Procedure took %g seconds.\n",
	     ((double)(finish-start)/CLOCKS_PER_SEC));
   }
   return res;
}



void dispim(image *im)
{
   image *tmp;
   int resize;

   resize = FALSE;
   if (im->size.x > dispsize.x) {
      if (dispsize.x < 1024) {
	 dispsize.x = MIN(im->size.x, 1024);
	 resize = TRUE;
      }
   }
   if (im->size.y > dispsize.y) {
      if (dispsize.y < 768) {
	 dispsize.y = MIN(im->size.y, 768);
	 resize = TRUE;
      }
   }
   if (resize) {
      display_set(DSP_SIZE, dispsize);
   }
   if (im->type ==  IM_BYTE) {
      dispid = display_imagexz(dispid, im, 0,0, di_min_b, di_max_b);
   }else if (im->type != IM_COMPLEX) {
      dispid = display_imagexz(dispid, im, 0,0, di_min, di_max);
   }else{
      tmp = im_convert(im, IM_FLOAT, FLG_MAG);
      dispid = display_imagexz(dispid, tmp, 0,0, di_min, di_max);
      free_image(tmp);
   }
}



void set_parameters(void)
{
   coord origin, size;
   int option;
   static int do_drag = FALSE;
   static char timing[] = "Turn timing flag on/off. [Off]";
   static char roi[120] = "Set ROI for analysis [Per image size]";
   static char di_byte[80] = "Set BYTE image display min/max [0,255]";
   static char di_other[100] = "Set other images display min/max [Per image]";
   static char di_drag[80] = "Allow displayed image to be dragged. [Off]";
   static char * choice[] = {
      timing,
      roi,
      "Remove analysis ROI.",
      di_byte,
      "Set BYTE image display to scale per image.",
      di_other,
      "Set other image display to scale per image.",
      di_drag
   };
   int val;
   char *cptr;

   do {
      option = ask_choice(choice, 8, 1);

      switch (option) {
       case 0:
	 do_timing = NOT do_timing;
	 cptr = strrchr(timing, '[');
	 cptr++;
	 strcpy(cptr, do_timing ? "On]" : "Off]");	 
	 break;
       case 1:
	 val = ask_double("Come in by how many pixels", 0, gim->size.x);
	 if (groi) free_roi(groi);
	 groi = alloc_roi(ROI_IMAGE,gim);
	 roi_modify(ROI_REDUCE, groi, val);
	 origin = roi_get(ROI_ORIGIN, groi).pt;
	 size = roi_get(ROI_SIZE, groi).pt;
	 cptr = strrchr(roi, '[');
	 cptr ++;
	 sprintf(cptr, "origin=(%d,%d) size=(%d,%d)]",
		 origin.x, origin.y, size.x, size.y);
	 break;
       case 2:
	 if (groi) free_roi(groi);
	 groi = NULL;
	 cptr = strrchr(roi, '[');
	 cptr ++;
	 strcpy(cptr, "Image size]");
	 break;
       case 3:
	 di_min_b = ask_double("BYTE image min value", 0, 254);
	 di_max_b = ask_double("BYTE image max value", di_min_b+1, 255);
	 cptr = strrchr(di_byte, '[');
	 cptr ++;
	 sprintf(cptr, "%d,%d]",di_min_b,di_max_b);
	 break;
       case 4:
	 di_min_b = di_max_b = 0;
	 cptr = strrchr(di_byte, '[');
	 cptr ++;
	 strcpy(cptr, "Per image]");
	 break;
       case 5:
	 di_min = ask_double("Other image min value", -1e300, 1e300);
	 di_max = ask_double("Other image max value", di_min, 1e300);
	 cptr = strrchr(di_other, '[');
	 cptr ++;
	 sprintf(cptr, "%g,%g]",di_min,di_max);
	 break;
       case 6:
	 di_min = di_max = 0;
	 cptr = strrchr(di_other, '[');
	 cptr ++;
	 strcpy(cptr, "Per image]");
	 break;
       case 7:
	 do_drag = NOT do_drag;
	 cptr = strrchr(di_drag, '[');
	 cptr++;
	 strcpy(cptr, do_drag ? "On]" : "Off]");
	 display_set(DSP_DRAGABLE, do_drag);
	 break;
      }
      
   } while (option >= 0);
}



void compare_images(image *gim, image *rim)
{
   image *tim, *tim2;
   stats st;
   long laplacian_l[9] = {
      0,  -1,  0,
      -1,  4, -1,
      0,  -1, 0
   };
   double laplacian_d[9] = {
      0,  -1,  0,
      -1,  4, -1,
      0,  -1, 0
   };

   if (gim->type != rim->type) {
      printf("\n\aImage types don't match.  Forget it.\n\n");
      return;
   }
   if (gim->size.x != rim->size.x || gim->size.y != rim->size.y) {
      printf("\n\aImage sizes don't match.  Forget it.\n\n");
      return;;
   }
   
   if (rim->type == IM_BYTE) {
      tim = im_convert(rim, IM_SHORT, NO_FLAG);
      tim2 = im_convert(gim, IM_SHORT, NO_FLAG);
   }else{
      tim = im_copy(rim);
      tim2 = im_copy(gim);
   }
   if (tim && tim2) {
      im_extrema(tim2, &st, groi);
      if (st.min == st.max) {
	 printf("\nGlobal image is single valued with value %g.\n",st.min);
      }
      im_sub(tim, tim2, NO_FLAG);
      im_stats(tim, &st, groi);
      if (st.max == 0.0 && st.min == 0.0) {
	 printf("\nGlobal image is identical to reference image.\n\n");
      }else{
	 printf("\nGlobal image differs from reference image:\n"
		"  Min error = %g,  Max error = %g\n"
		"  Mean error = %g,  RMS error = %g\n",
		st.min, st.max, st.mean, st.rmsmean);
	 if (groi)
	   im_roiset(tim, groi, 0, NO_FLAG);
	 dispim(tim);
	 if (gim->type == IM_COMPLEX) {
	    printf("  COMPLEX image - not doing correlation.\n\n");
	 }else{
	    free_image(tim);
	    if (gim->type == IM_BYTE) {
	       tim = im_convert(rim, IM_SHORT, NO_FLAG);
	    }else{
	       tim = im_copy(rim);
	    }
	    if (tim) {
	      im_convolve(tim,
			image_type_integer(tim) ? (void *)laplacian_l : (void *)laplacian_d,
			3, NO_FLAG);
	      im_convolve(tim2,
			image_type_integer(tim2) ? (void *)laplacian_l : (void *)laplacian_d,
			3, NO_FLAG);
	      printf("  Cross-corr. = %g,  Laplacian cross-corr. = %g.\n\n", 
		   im_crosscorr(rim,gim,groi), im_crosscorr(tim,tim2,groi));
	    }
	 }
      }
   }
   if (tim) free_image(tim);
   if (tim2) free_image(tim2);

}



double im_crosscorr(image *im1, image *im2, ROI *roi)
{
   double s, t, st, ss, tt;
   double size;
   int j;
   int ownroi;
   coord topl, botr;

   ownroi = FALSE;
   if (roi == NULL) {
      if ((roi = alloc_roi(ROI_IMAGE,im1)) == NULL) {
	 return -1.0;
      }
      ownroi = TRUE;
   }

   s = t = st = ss = tt = 0;
   topl = roi_get(ROI_ORIGIN,roi).pt;
   botr = roi_get(ROI_BOTRIGHT,roi).pt;

   switch (im1->type) {
    case IM_BYTE:
      for (j=topl.y; j<=botr.y; j++) {
	 int i;
	 UBYTE *sptr, *tptr;

	 sptr = IM_ROWADR(im1, j, b);
	 tptr = IM_ROWADR(im2, j, b);
	 for (i=topl.x; i<=botr.x; i++) {
	    s += (double)*sptr;
	    t += (double)*tptr;
	    st += (double)*sptr * *tptr;
	    ss += (double)*sptr * *sptr;
	    tt += (double)*tptr * *tptr;
	    sptr++; tptr++;
	 }
      }
      break;

    case IM_SHORT:
      for (j=topl.y; j<=botr.y; j++) {
	 int i;
	 SHORT *sptr, *tptr;

	 sptr = IM_ROWADR(im1, j, s);
	 tptr = IM_ROWADR(im2, j, s);
	 for (i=topl.x; i<=botr.x; i++) {
	    s += (double)*sptr;
	    t += (double)*tptr;
	    st += (double)*sptr * *tptr;
	    ss += (double)*sptr * *sptr;
	    tt += (double)*tptr * *tptr;
	    sptr++; tptr++;
	 }
      }
      break;

    case IM_LONG:
      for (j=topl.y; j<=botr.y; j++) {
	 int i;
	 LONG *sptr, *tptr;

	 sptr = IM_ROWADR(im1, j, l);
	 tptr = IM_ROWADR(im2, j, l);
	 for (i=topl.x; i<=botr.x; i++) {
	    s += (double)*sptr;
	    t += (double)*tptr;
	    st += (double)*sptr * *tptr;
	    ss += (double)*sptr * *sptr;
	    tt += (double)*tptr * *tptr;
	    sptr++; tptr++;
	 }
      }
      break;

    case IM_FLOAT:
      for (j=topl.y; j<=botr.y; j++) {
	 int i;
	 float *sptr, *tptr;

	 sptr = IM_ROWADR(im1, j, f);
	 tptr = IM_ROWADR(im2, j, f);
	 for (i=topl.x; i<=botr.x; i++) {
	    s += (double)*sptr;
	    t += (double)*tptr;
	    st += (double)*sptr * *tptr;
	    ss += (double)*sptr * *sptr;
	    tt += (double)*tptr * *tptr;
	    sptr++; tptr++;
	 }
      }
      break;

    case IM_DOUBLE:
      for (j=topl.y; j<=botr.y; j++) {
	 int i;
	 double *sptr, *tptr;

	 sptr = IM_ROWADR(im1, j, d);
	 tptr = IM_ROWADR(im2, j, d);
	 for (i=topl.x; i<=botr.x; i++) {
	    s += *sptr;
	    t += *tptr;
	    st += *sptr * *tptr;
	    ss += *sptr * *sptr;
	    tt += *tptr * *tptr;
	    sptr++; tptr++;
	 }
      }
      break;

   }

   topl = roi_get(ROI_SIZE,roi).pt;
   size = (double)topl.x * topl.y;

   if (ownroi) free_roi(roi);

   return ( (st-s*t/size) / sqrt((ss-s*s/size)*(tt-t*t/size)) );
}



void conjugate_image(image *im)
{
   int j;

   if (im->type == IM_COMPLEX) {
      for (j=0; j<im->size.y; j++) {
	 int i;
	 complex *cptr;

	 cptr = im_rowadr(im, j);
	 for (i=0; i<im->size.x; i++) {
	    cptr->i = -cptr->i;
	    cptr++;
	 }
      }
   }
}


void chk_modified(int modified, image *im)
{
   if (modified) {
      if (ask_yesno("Modified image not saved back to global image."
		    " Should I save it")) {
	 free_image(gim);
	 gim = im_copy(im);
      }
   }
}


int print_memory_list(void)
{
   int mem,nummem;

   nummem = 0;
   for (mem=0; mem<NUMMEMORY; mem++) {
      if (memory[mem]) {
	 printf(" %2d: ",mem+1);
	 prtstats(memory[mem], memstr[mem], 0);
	 nummem++;
      }
   }
   return nummem;
}


image *get_memory_image(void)
{
   int mem, nummem;
   image *im;

   nummem = print_memory_list();
   if (nummem) {
      mem = ask_double("Memory location", 0, NUMMEMORY);
      if (mem) {
         mem--;
	 if ((im = memory[mem]) == NULL) {
	    printf("\nThere is no image in this memory location!\n\n");
	 }
      }else{
	 im = NULL;
      }
   }else{
      printf("\nSorry - but there are no images in the memory bank!\n\n");
      im = NULL;
   }

   return im;
}


void not_implemented(void)
{
   printf("\nSorry - the function you requested has not been implemented.\n\n");
}



static void im_drawlinez(image *im, double val, int sx, int sy, int ex, int ey)
{
   coord start, end;

   start.x = sx;
   start.y = sy;
   end.x = ex;
   end.y = ey;
   im_drawline(im, val, start, end);
}

static void im_drawboxz(image *im, double val, int ox, int oy, int sx, int sy)
{
   box tbox;

   tbox.origin.x = ox;
   tbox.origin.y = oy;
   tbox.size.x = sx;
   tbox.size.y = sy;
   im_drawbox(im, val, tbox);
}


static int display_imagexz(int id, image *im, int ox, int oy,
			    double min, double max)
{
   coord origin;

   origin.x = ox;
   origin.y = oy;
   return display_imagex(id, im, origin, min, max);
}

static int display_imagez(int id, image *im, int ox, int oy)
{
   coord origin;

   origin.x = ox;
   origin.y = oy;
   return display_image(id, im, origin);
}


static void readline(char *buf, int len, FILE *f)
{
   fgets(buf,len,f);
   while (!feof(f) && buf[0] == '#')
     fgets(buf,len,f);
   if (feof(f)) {
     buf[0] = 0;
   }
}



static char * chkparm(char * str, char *id, int numparms)
{
   char *cptr;

   cptr = strtok(str," \t\n");
   if (cptr == NULL || *cptr == 0)  return NULL;
   if (cptr[strlen(cptr)-1] != ':') return NULL;
   cptr[strlen(cptr)-1] = 0;
   if (NOT STREQUAL(cptr,id)) return NULL;
   return cptr;
}

static int intparm(void)
{
   char *cptr;

   cptr = strtok(NULL," \t\n");
   if (cptr == NULL) {bdformat(); return 0;}
   if (*cptr == '-') {
     if (str_is_int(cptr+1,0))
       return strtol(cptr,NULL,10);
     else
       {bdformat(); return 0;}
   }else{
     if (str_is_int(cptr,0))
       return strtol(cptr,NULL,10);
     else
       {bdformat();  return 0;}
   }
}


static double dblparm(void)
{
   char *cptr;

   cptr = strtok(NULL," \t\n");
   if (cptr == NULL) {bdformat(); return 0.0;}
   return strtod(cptr,NULL);
}


static void bdformat(void)
{
   wprintf("Unexpected formatting found in file being read.\n");
}

