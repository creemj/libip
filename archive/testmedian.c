#include <stdio.h>
#include <stdlib.h>

static int median(int *arr, int size);
static void allperms(int *, int, int);
void dumparr(int *arr, int size);

int arr[10];
int marr[10];
int med;
int ok;

main()
{
   int list[10];
   int lsize;
   int i;

   printf("Array size (<10):");
   scanf("%d",&lsize);
   if (lsize <= 0 || lsize > 10) exit(0);
   for (i=0; i<lsize; i++)
      list[i] = i+1;
   med = (lsize+1)/2;
   ok = 1;
   allperms(list,lsize,0);
   if (! ok) printf("*** SOME RESULTS INCORRECT ***\n");
}


static void allperms(int *list, int lsize, int depth)
{
   int i,j;
   int newlist[10];
   int newsize;
   int nmed;

   if (lsize == 1) {
      arr[depth++] = list[0];
      for (i=0; i<depth; i++) marr[i] = arr[i];
/*      dumparr(marr,depth); */
      nmed = median(marr,depth);
/*      printf(" : median = %d.",nmed); */
      if (nmed != med) {
	 printf("  INCORRECT");
	 ok = 0;
      }
/*      printf("\n"); */
   }else{
      for (i=0; i<lsize; i++) {
	 arr[depth] = list[i];
	 newsize = 0;
	 for (j=0; j<lsize; j++) {
	    if (j != i) 
	       newlist[newsize++] = list[j];
	 }
	 allperms(newlist,newsize,depth+1);
      }
   }
}



void dumparr(int *arr, int size)
{
   int i;

   printf("%d:",size);
   for (i=0; i<size; i++)
      printf("%d ",arr[i]);
}



static int median(int *arr, int size)
{
   long  left,right;		/* positions in 1-D array             */
   long  i,j,k;			/* loop variables for sorting         */
   short val;			/* value holding current middle value */
   short  t;			/* temporary storage of array values  */

/*   dumparr(arr,size);
   printf("\n");
*/
   left  = 0;
   right = size - 1;
   k = (right+1)/2;

   while (right > left) {
			        /* To prevent j-- loop going off array start */
      if (arr[right] < arr[left]) {
	 t = arr[right];
	 arr[right] = arr[left];
	 arr[left] = t;
      }

      val = arr[right];
      i = left - 1;
      j = right;

      do {
	 do i++; while (arr[i] < val);
	 do j--; while (arr[j] > val);
	 t = arr[i];
	 arr[i] = arr[j]; 
	 arr[j] = t;
      } while (j > i);

      arr[j] = arr[i];
      arr[i] = arr[right];
      arr[right] = t;

      if (j < left || i > right) {
	 printf("Out of bounds");
	 printf(" : l=%d, r=%d, i=%d, j=%d\n",left,right,i,j);
      }

      if (i >= k) right = i - 1;
      if (i <= k) left  = i + 1;

   }
   return arr[k];
}



#if 0

   for (;;) {
      printf("Size (<=10): ");
      gets(buf);
      size = atoi(buf);
      if (size < 1 || size > 10) break;
      printf("Values (space separated): ");
      gets(buf);
      pos = 0;
      for (i=0; i<size; i++) {
	 sscanf(&buf[pos],"%d%n",&arr[i],&npos);
	 pos += npos;
      }
      printf("Median = %d\n\n",median(arr,size));
   }
#endif
