#include <stdio.h>
#include <stdlib.h>

#include <ip/ip.h>
#include <ip/png.h>
#include <ip/text.h>

void bombout(logstack *ipr, int error, char *extra, va_list ap);


main()
{
   image *im;
   ipfont *ft;
   coord pos;

   register_error_handler(bombout);

   /*   debug_messages(0x0f); */

   ft = alloc_font("/usr/lib/X11/fonts/Type1/c0648bt_.pfb",16);
   if (ft == NULL) {
      printf("Failed to allocate font face\n");
   }else{
      printf("Apparantly allocated font face!\n");
   }

   im = alloc_image(IM_BYTE,256,256);
   im_clear(im);

   pos.x = pos.y = 20;
   im_text(im, ft, "Hello World!\nThis some text to exercise the text routine.\nABCDEFGHIJKLMNOPQRSTUVWXYZ\nabcdefghijklmnopqrstuvwxyz", pos, 255, 0);

   pos.y = 200;
   im_text(im, ft, "Goodbye", pos, 0, 255);

   write_png_image("freddy.png", im);

   free_image(im);

}


void bombout(logstack *ipr, int error, char *extra, va_list ap)
{
   static int inhere = 0;
   char name[14];
   int mem;
 
   if (!inhere)
      print_error("test",ipr,error,extra,ap);
   if (error == ERR_SIGFPE) {
      inhere = TRUE;
   }else{
      exit(1);
   }
}
