#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <ip/internal.h>

void bombout(logstack *ipr, int error, char *extra, va_list ap);
double dist_euclidean(colour *p1, colour *p2);
double dist_angle(colour *p1, colour *p2);
double dist_simil(colour *p1, colour *p2);
double dist_L1(colour *p1, colour *p2);

main(argc,argv)
int argc;
char *argv[];
{
   char *fname;
   int msize;
   int i,j,l,k;
   int hwidth;
   int arrsize;
   double *dist, *dptr;
   double sum;
   double min,max,cval,cf,m,c;
   image *im;
   double (*dfunc)(colour *,colour *);

   if (argc != 6) {
      fprintf(stderr,"Usage: testrgb <rgbimage> <l|e|a|s> <mask_size> <i_point> <j_point>\n");
      exit(1);
   }

   switch (*argv[2]) {
    case 'e':
      dfunc = dist_euclidean;
      break;
    case 'l':
      dfunc = dist_L1;
      break;
    case 'a':
      dfunc = dist_angle;
      break;
    case 's':
      dfunc = dist_simil;
      break;
    default:
      fprintf(stderr,"Second arg must be one of e, l, s or a.\n");
      exit(1);
   }

   msize = strtol(argv[3],NULL,10);
   i = strtol(argv[4],NULL,10);
   j = strtol(argv[5],NULL,10);

   register_error_handler(bombout);

   im = ip_read_image(argv[1],IM_RGB,-1);

   log_routine("argument_checks");

   ip_parm_greatereq("msize", msize, 3.0);
      
   ip_parm_odd("msize", msize);

   hwidth = msize / 2;          /* half-msize of filter */
   arrsize = msize*msize;

   ip_parm_inrange("i",i,hwidth,im->size.x-hwidth-1);
   ip_parm_inrange("j",j,hwidth,im->size.y-hwidth-1);

   pop_routine();

   /* Dump pixel values in mask at point (i,j) in image */

   printf("Image values:\n");
   for (l=-hwidth; l<=hwidth; l++) {
      colour *pix;

      pix = (colour *)im_rowadr(im,l+j) + (i-hwidth);
      printf(" %2d:",l);
      for (k=-hwidth; k<=hwidth; k++) {
	 printf("  (%03d,%03d,%03d)",pix->r,pix->g,pix->b);
	 pix++;
      }
      printf("\n");
   }
   printf("\n");

   /* Calculate distances wrt to pixel (1,1) of mask */

   sum = 0.0;
   printf("Distance wrt (%d,%d):\n",-hwidth,-hwidth);
   for (l=-hwidth; l<=hwidth; l++) {
      colour *pix,*rpix;
      
      pix = (colour *)im_rowadr(im,l+j) + (i-hwidth);
      rpix = (colour *)im_rowadr(im,j-hwidth) + (i-hwidth);
      printf(" %2d:",l);
      for (k=-hwidth; k<=hwidth; k++) {
	 double dist;

	 dist = dfunc(pix,rpix);
	 sum += dist;
	 printf("  %8.5f",dist);
	 pix++;
      }
      printf("\n");
   }
   printf("Sum = %g\n\n",sum);

   sum = 0.0;
   printf("Distance wrt (0,0) [ie (%d,%d) in image coords]:\n",i,j);
   for (l=-hwidth; l<=hwidth; l++) {
      colour *pix,*rpix;
      
      pix = (colour *)im_rowadr(im,l+j) + (i-hwidth);
      rpix = (colour *)im_rowadr(im,j) + i;
      printf(" %2d:",l);
      for (k=-hwidth; k<=hwidth; k++) {
	 double dist;

	 dist = dfunc(pix,rpix);
	 sum += dist;
	 printf("  %8.5f",dist);
	 pix++;
      }
      printf("\n");
   }
   printf("Sum = %g\n\n",sum);

   /* Calculate full distance matrix */

   dist = ip_malloc(arrsize*sizeof(double));
   
   dptr = dist;
   for (l=0;l<arrsize;l++) *dptr++=0;

   dptr = dist;
   for (l=-hwidth; l<=hwidth; l++) {
      colour *rpix;
      
      rpix = (colour *)im_rowadr(im,l+j) + (i-hwidth);
      for (k=-hwidth; k<=hwidth; k++) {
	 colour *pix;
	 double *dptr2;
	 int kk,ll;

	 dptr2 = dptr+1;
	 if (k < hwidth) {
	    ll = l;
	    kk = k+1;
	 }else{
	    ll = l+1;
	    kk=-hwidth;
	 }
	 for (; ll<=hwidth; ll++) {
	    pix = IM_ROWADR(im, j+ll, r) + (i+kk);
	    for (; kk<=hwidth; kk++) {
	       double dist;
	       dist = dfunc(rpix,pix);
	       *dptr += dist;
	       *dptr2 += dist;
	       pix++; dptr2++;
	    }
	    kk = -hwidth;
	 }	 
	 rpix++; dptr++;
      }
   }
   
   printf("Distance matrix: (%d,%d)\n",i,j);
   dptr = dist;
   max = 0.0;
   min = 1e30;
   for (l=-hwidth; l<=hwidth; l++) {
      printf(" %2d:",l);
      for (k=-hwidth; k<=hwidth; k++) {
	 if (*dptr > max) max = *dptr;
	 if (*dptr < min) min = *dptr;
	 printf("  %8.5f",*dptr++);
      }
      printf("\n");
   }
   printf("Min = %g,   max = %g\n\n",min,max);

   printf("Centre Bias Matrix: (%d,%d)\n",i,j);
   dptr = dist;
   cval = dist[arrsize/2]; 
   printf("cval=%g\n",cval);
   for (l=-hwidth; l<=hwidth; l++) {
      printf(" %2d:",l);
      for (k=-hwidth; k<=hwidth; k++) {
	 printf("  %8.5f",fabs(*dptr++-cval)+min);
      }
      printf("\n");
   }
   
   printf("\nCentre Bias 2 Matrix: (%d,%d)\n",i,j);
   dptr = dist;
   cf = (max + min) / 2;
   cval = dist[arrsize/2];
   m = -1;
   c = max + min;
   printf("cval=%g    cf=%g    m=%g    c=%g\n",cval,cf,m,c);
   if (cval > cf) cval = m*cval + c;
   printf("New cval reference = %g\n",cval);
   for (l=-hwidth; l<=hwidth; l++) {
      printf(" %2d:",l);
      for (k=-hwidth; k<=hwidth; k++) {
	 printf("  %8.5f",fabs(*dptr++-cval)+min);
      }
      printf("\n");
   }
   

   ip_free(dist);
   free_image(im);
}





void bombout(logstack *ipr, int error, char *extra, va_list ap)
{
   static int inhere = 0;
   char name[14];
   int mem;

   if (!inhere)
      print_error("test",ipr,error,extra,ap);
   if (error == ERR_SIGFPE) {
      inhere = TRUE;
   }else{
      exit(1);
   }
}



double dist_L1(colour *p1, colour *p2)
{
   return (fabs(((double)p1->r-p2->r))
	   + fabs(((double)p1->g-p2->g))
	   + fabs(((double)p1->b-p2->b)));
}


double dist_euclidean(colour *p1, colour *p2)
{
   return sqrt(((double)p1->r-p2->r)*((double)p1->r-p2->r)
	       + ((double)p1->g-p2->g)*((double)p1->g-p2->g)
	       + ((double)p1->b-p2->b)*((double)p1->b-p2->b));
}


double dist_angle(colour *p1, colour *p2)
{
   double m1, m2;

   m1 = sqrt((double)p1->r*p1->r + (double)p1->g*p1->g + (double)p1->b*p1->b);
   m2 = sqrt((double)p2->r*p2->r + (double)p2->g*p2->g + (double)p2->b*p2->b);
   if (m1 == 0 || m2 == 0) {
      return (m1 == m2 ? 0.0 : 1.0);      
   }else{
      return acos(((double)p1->r*p2->r 
		   + (double)p1->g*p2->g +(double)p1->b*p2->b) / ( m1 * m2));
   }
}



double dist_simil(colour *p1, colour *p2)
{
   double m1,m2;

   m1 = sqrt((double)p1->r*p1->r + (double)p1->g*p1->g + (double)p1->b*p1->b);
   m2 = sqrt((double)p2->r*p2->r + (double)p2->g*p2->g + (double)p2->b*p2->b);
   if (m1 == 0 || m2 == 0) {
      return (m1 == m2 ? 0.0 : 1.0);      
   }else{
      return (1.0 - (((double)p1->r*p2->r + (double)p1->g*p2->g 
		      + (double)p1->b*p2->b) / (m1*m2))
	      * (1 - fabs((m1-m2))/ (m1>m2 ? m1 : m2)));
   }
}
