/* $Id$ */
/*

   Module: ip/rgbmedian.c
   Author: M.J.Cree

   Vector Median and related filtering of RGB images

*/

#include <stdlib.h>
#include <math.h>

#include <ip/internal.h>

#include <debug/debug.h>

static char rcsid[] = "$Id$";



/*

NAME:

   im_rgbmedian() \- Median filter an RGB image

PROTOTYPE:

   #include <ip/ip.h>

   int  im_median( im, width, flag )
   image *im;
   int width;
   int flag;

DESCRIPTION:

   Median filter an RGB image with 'width'*'width' kernel.  'Width' must
   be odd.  By default, the border region where the mask goes off the
   edge of the image is left unprocessed.  This can be modified with
   the use of 'flag', which may be:

\&      FLG_ZERO   Zero border region of image where mask doesn''t cover
                 properly.

\&      FLG_EXTEND Extend the median function to the edge of the image
                 using as many pixels as are available to calculate
		 the median.

LIMITATIONS:

\-   Only RGB images supported
\-   The kernel mask size must be odd.

ERRORS:

   ERR_UNSUPPORTED_TYPE
   ERR_BAD_PARAMETER
   ERR_BAD_FLAG
   ERR_OUT_OF_MEM

*/

int im_rgbmedian( image *im, int width, int flag )
{

   int hwidth;			/* half-width of widthxwidth median filter */
   int i,j;			/* loop variables                          */
   double  *arr;		/* 1-d array for working space */
   size_t arrsize;
   image *tmp, *mag;

   log_routine("im_rgbmedian");

   if (im->type!=IM_RGB) {
      log_error(ERR_UNSUPPORTED_TYPE);
      goto exit;
   }

   if (NOT flag_ok(flag,FLG_ZERO,FLG_EXTEND,LASTFLAG))
      return ip_error;

   if (NOT ip_parm_greatereq("width", width, 3.0))
      return ip_error;
      
   if (NOT ip_parm_odd("width", width))
      return ip_error;

   if ((tmp = alloc_image(im->type,im->Nx,im->Ny)) == NULL)
      goto exit;

   hwidth = width / 2;		/* half-width of filter */
   arrsize = width*width;

   /* initialise array */

   if ((arr = ip_malloc(sizeof(double) * arrsize)) == NULL) {
      free_image(tmp);
      return ip_error;
   }

   /* Initialise an image with the vector magnitude of each pixel. */

   if ((mag = im_convert(im, IM_DOUBLE, FLG_MAG)) == NULL) {
      free_image(tmp);
      ip_free(arr);
      return ip_error;
   }

   /* for each pixel in image find median over width*width mask */

   for( j = hwidth; j < (im->Ny - hwidth); j++ ) {
      colour *tptr;		/* Result image */
      colour *cptr;		/* Centre pixel */
      double *mcptr;		/* Magnitude of centre pixel */
      
      tptr = (colour *)im_rowadr(tmp, j) + hwidth;
      cptr = (colour *)im_rowadr(im, j) + hwidth;
      mcptr = (double *)im_rowadr(mag, j) + hwidth;

      for( i = hwidth; i < (im->Nx - hwidth); i++ ) {
	 int k,l;
	 double *aptr;
	 double max, red, green , blue;

	 /* Load up array with distance measure of pixels
	    in neighbourhood of (i,j):

	    Distance = arccos( x_ij . x_kl / (|x_ij| * |x_kl|))

	    where x_ij is colour value of (i,j) pixel,
	    |x_ij| is magnitude thereof,
	    and '.' is vector dot-product.
	 */

#ifdef DEBUG
	 if (i==128 && j==128) {
	   D(bug("RGBMEDIAN:  point 128x128 debug.\n"));
	   D(bug("Dumping Image and Distance Matrix:\n"));
	 }
#endif

	 aptr = arr;
	 for (l=-hwidth; l<=hwidth; l++) {
	    colour *sptr;
	    double *mptr;

	    sptr = (colour *)im_rowadr(im, j+l) + (i - hwidth);
	    mptr = (double *)im_rowadr(mag, j+l) + (i - hwidth);
	    for (k=-hwidth; k<=hwidth; k++) {
	       *aptr = acos(((double)cptr->r*sptr->r 
			       + (double)cptr->g*sptr->g 
			       + (double)cptr->b*sptr->b) / (*mcptr * *mptr));
#ifdef DEBUG
	       if (i==128 && j==128) {
		 if (k==-hwidth) D(bug(" %2d:",l));
		 D(bug(" %2x%2x%2x-%-7.5f",sptr->r,sptr->g,sptr->b,*aptr));
		 if (k==hwidth) D(bug("\n"));
	       }
#endif
	       sptr++; mptr++; aptr++;
	    }
	 }

	 /* Identify max distance in array */

	 aptr = arr;
	 max = 0.0;
	 for (k=0; k<arrsize; k++) {
	    if (*aptr > max) max = *aptr;
	    aptr++;
	 }

#ifdef DEBUG
	 if (i==128 && j==128) {
	   D(bug("Max distance value = %g\n",max));
	 }
#endif

	 /* Replace array values with weights calculated by
               <newvalue> = (max - <oldvalue>)
	 */

	 aptr = arr;
	 for (k=0; k<arrsize; k++) {
	    *aptr = (max - *aptr);
	    aptr++;
	 }

	 /* Find sum of array values for normalisation to follow. */

	 aptr = arr;
	 max = 0;
	 for (k=0; k<arrsize; k++) {
	    max += *aptr++;
	 }

#ifdef DEBUG
	 if (i==128 && j==128) {
	   aptr = arr;
	   D(bug("Weights:\n"));
	   for (l=-hwidth; l<=hwidth; l++) {
	     D(bug(" %2d:",l));
	     for (k=-hwidth; k<=hwidth; k++) {
	       D(bug(" %8.6f",*aptr++));
	     }
	     D(bug("\n"));
	   }
	 }
#endif

	 /* Calculate new colour value for pixel (i,j) by vector summation
	    over pixels in neighbourhood each weighted by appropriate
	    value in array.
	 */

	 aptr = arr;
	 red = green = blue = 0.0;
	 for (l=-hwidth; l<=hwidth; l++) {
	    colour *sptr;

	    sptr = (colour *)im_rowadr(im, j+l) + (i - hwidth);
	    for (k=-hwidth; k<=hwidth; k++) {
	       red += *aptr * sptr->r;
	       green += *aptr * sptr->g;
	       blue += *aptr * sptr->b;
	       sptr++; aptr++;
	    }
	 }
	 tptr->r = (UBYTE)(red/max);
	 tptr->g = (UBYTE)(green/max);
	 tptr->b = (UBYTE)(blue/max);

#ifdef DEBUG
	 if (i==128 && j==128) {
	   D(bug("Final result: %2x%2x%2x  (%d,%d,%d)\n",
		 tptr->r, tptr->g, tptr->b, tptr->r, tptr->g, tptr->b));
	 }
#endif

	 tptr++; cptr++; mcptr++;
      }
   }

   if (flag == FLG_ZERO) {
      /* Clear edges of tmp */
      im_clear_border(tmp,hwidth);
   }

   if (flag == FLG_EXTEND) {

   }

   if (flag == FLG_ZERO || flag == FLG_EXTEND) {
      memcpy(im->image.v, tmp->image.v, im->Nx*im->Ny*ip_typesize[im->type]);
   }else{
      coord org,size;

      org.x = org.y = hwidth;
      size.x = im->Nx - 2*hwidth;
      size.y = im->Ny - 2*hwidth;

      im_insert(im, org, tmp, org, size);
   }
   free_image(tmp);
   ip_free(arr);

 exit:
   pop_routine();
   return ip_error;
}


