/* $Id$ */
/*
   Module: ip/zoom.c
   Author: M.J. Cree

Routines:

   Image resizing/zooming.

*/


#include <stdlib.h>
#include <ip/internal.h>
#include <jhh/interp.h>

static char rcsid[] = "$Id$";


image *old_im_reduce(image *src, int skip, int flag);
image *old_im_reducex(image *src, int xskip, int yskip, int flag);
image * old_im_expand( image * src, int factor , int flag );
image *old_im_zoom( image * src, double xfactor, double yfactor , int flag );
int im_warpx1(image *dest, image *src, poly2d *poly, int flag);
int im_warpx2(image *dest, image *src, poly2d *poly, int flag);
int im_warpx3(image *dest, image *src, poly2d *poly, int flag);
int old_im_warp(image *im, poly2d *poly, int flag);
int old_im_fft(image *, int);


/* To indicate that internal routines to this module are calling up
   im_reducex() - this allows BINARY images to be treated as BYTE
   images and should be returned with values in between 0 and 255 to
   keep precision. */

#define FLG_INTERNAL 0x4000


static void zoom_bilinear(image *dest, image *src);



image *old_im_reduce(image *src, int skip, int flag)
{
   image *dest;
   int j,i;
   coord size;
   int l,k;
   long int lval;
   double val;
   complex cval;
   long int i_skipsqr;
   double f_skipsqr;
   void *dp, *sp;
   
   log_routine("im_reduce");

   if (NOT flag_ok(flag,FLG_AVERAGE,FLG_SAMPLE,LASTFLAG))
      return NULL;

   dest = NULL;

   if (NOT ip_parm_inrange("skip", skip, 1, MIN(src->size.x, src->size.y))) {
      return NULL;
   }

   if (flag == FLG_AVERAGE && NOT ip_parm_mult2("skip", skip)) {
      return NULL;
   }

   if ((src->type == IM_BINARY || image_type_clrim(src))
						 && flag != FLG_SAMPLE) {
      log_error(ERR_BAD_FLAG);
      goto exit_s;
   }      

   size.x = (src->size.x + skip - 1) / skip;
   size.y = (src->size.y + skip - 1) / skip;
   if ((dest = alloc_image(src->type, size.x, size.y)) == NULL)
      goto exit_s;

   if (dest->type == IM_PALETTE)
      im_palette_copy(dest,src);

   if (flag == FLG_SAMPLE) {
      dp = dest->image.v;
      for (j=0; j<size.y; j++) {
	 switch (src->type) {
	  case IM_BINARY:
	  case IM_BYTE:
	  case IM_PALETTE:
	    sp = &src->image.b[Xs(0,j*skip)];
	    for (i=0; i<size.x; i++) {
	       *((UBYTE *)dp)++ = *(UBYTE *)sp;
	       ((UBYTE *)sp) += skip;
	    }
	    break;
	  case IM_SHORT:
	    sp = &src->image.s[Xs(0,j*skip)];
	    for (i=0; i<size.x; i++) {
	       *((SHORT *)dp)++ = *(SHORT *)sp;
	       ((SHORT *)sp) += skip;
	    }
	    break;
	  case IM_LONG:
	    sp = &src->image.l[Xs(0,j*skip)];
	    for (i=0; i<size.x; i++) {
	       *((LONG *)dp)++ = *(LONG *)sp;
	       ((LONG *)sp) += skip;
	    }
	    break;
	  case IM_FLOAT:
	    sp = &src->image.f[Xs(0,j*skip)];
	    for (i=0; i<size.x; i++) {
	       *((float *)dp)++ = *(float *)sp;
	       ((float *)sp) += skip;
	    }
	    break;
	  case IM_DOUBLE:
	    sp = &src->image.d[Xs(0,j*skip)];
	    for (i=0; i<size.x; i++) {
	       *((double *)dp)++ = *(double *)sp;
	       ((double *)sp) += skip;
	    }
	    break;
	  case IM_COMPLEX:
	    sp = &src->image.c[Xs(0,j*skip)];
	    for (i=0; i<size.x; i++) {
	       *((complex *)dp)++ = *(complex *)sp;
	       ((complex *)sp) += skip;
	    }
	    break;
	  case IM_RGB:
	    sp = &src->image.b[Xs(0,j*skip)*3];
	    for (i=0; i<size.x; i++) {
	       *((UBYTE *)dp)++ = *((UBYTE *)sp)++;
	       *((UBYTE *)dp)++ = *((UBYTE *)sp)++;
	       *((UBYTE *)dp)++ = *((UBYTE *)sp)++;
	       ((UBYTE *)sp) += (skip-1)*3;
	    }
	    break;
	 }
      }
   }

   if (flag == FLG_AVERAGE) {
      i_skipsqr = skip*skip;
      f_skipsqr = (double)skip*skip;
      for (j=0; j<dest->Ny; j++) {
	 for (i=0; i<dest->Nx; i++) {
	    val = 0.0;
	    lval = 0;
	    cval.i = cval.r = 0.0;	   
	    for (l=j*skip; l<j*skip+skip; l++) {
	       for (k=i*skip; k<i*skip+skip; k++) {
		 if (k >= src->size.x || l >= src->size.y) {
		   fprintf(stderr,"EOB: (%d,%d)\n",k,l);
		 }
		  switch (src->type) {
		   case IM_BYTE:
		     lval += src->image.b[Xs(k,l)];
		     break;
		   case IM_SHORT:
		     lval += src->image.s[Xs(k,l)];
		     break;
		   case IM_LONG:
		     lval += src->image.l[Xs(k,l)];
		     break;
		   case IM_FLOAT:
		     val += src->image.f[Xs(k,l)];
		     break;
		   case IM_COMPLEX:
		     cval.r += src->image.c[Xs(k,l)].r;
		     cval.i += src->image.c[Xs(k,l)].i;		     
		     break;
		   case IM_DOUBLE:
		     val += src->image.d[Xs(k,l)];
		     break;
		  }
	       }
	    }
	    switch (dest->type) {
	     case IM_BYTE:
	       dest->image.b[Xd(i,j)] = lval / i_skipsqr;
	       break;
	     case IM_SHORT:
	       dest->image.s[Xd(i,j)] = lval / i_skipsqr;
	       break;
	     case IM_LONG:
	       dest->image.l[Xd(i,j)] = lval / i_skipsqr;
	       break;
	     case IM_FLOAT:
	       dest->image.f[Xd(i,j)] = val / f_skipsqr;
	       break;
	     case IM_COMPLEX:
	       dest->image.c[Xd(i,j)].r = cval.r / f_skipsqr;
	       dest->image.c[Xd(i,j)].i = cval.i / f_skipsqr;
	       break;
	     case IM_DOUBLE:
	       dest->image.d[Xd(i,j)] = val / f_skipsqr;
	       break;
	    }
	 }
      }
   }

 exit_s:
   pop_routine();
   return dest;
}



image *old_im_reducex(image *src, int xskip, int yskip, int flag)
{
   image *dest;
   int internal;
   int dx, dy;

   log_routine("im_reducex");

   internal = FALSE;		/* Internal routines to this module can set */
   if (flag & FLG_INTERNAL) {	/*   FLG_INTERNAL to say that FLG_AVERAGE */
      internal = TRUE;		/*   may be used on BINARY images - an */
      flag &= ~FLG_INTERNAL;	/*   invalid BINARY image is returned!  */
   }

   if (NOT flag_ok(flag,FLG_SAMPLE,FLG_AVERAGE,LASTFLAG))
      return NULL;

   if (NOT ip_parm_inrange("x_skip", xskip, 1, src->size.x))
      return NULL;

   if (NOT ip_parm_inrange("y_skip", yskip, 1, src->size.y))
      return NULL;

   dest = NULL;

   if (xskip == 1 && yskip == 1) {
      dest = im_copy(src);
      goto exit_rx;
   }

   if (image_type_clrim(src) && flag != FLG_SAMPLE) {
      log_error(ERR_BAD_FLAG);
      goto exit_rx;
   }

   dx = src->size.x / xskip;
   dy = src->size.y / yskip;

   if ((dest = alloc_image(src->type, dx, dy)) == NULL)
      goto exit_rx;

#define LAVERAGE(type,t) \
   long average;                      \
   average = 0;                       \
   for (l=0; l<yskip; l++) {          \
      sp = IM_ROWADR(src,j*yskip+l,t);\
      (type *)sp += i*xskip;          \
      for (k=0; k<xskip; k++) {       \
         average += (long)(*((type *)sp)++); \
      }                               \
   }                                  \

#define AVERAGE(type,t) \
   {                                  \
      double average;                    \
      average = 0.0;                     \
      for (l=0; l<yskip; l++) {          \
         sp = IM_ROWADR(src,j*yskip+l,t);\
         (type *)sp += i*xskip;          \
         for (k=0; k<xskip; k++) {       \
            average += (double)(*((type *)sp)++); \
         }                               \
      }                                  \
      *((type *)dp)++ = (type)ROUND(average/npixels); \
   }

   if (flag == FLG_AVERAGE) {
      int i,j,k,l;
      void *dp, *sp;
      double npixels = xskip*yskip;
      int imtype = src->type;

      if (imtype == IM_BINARY && internal) imtype = IM_BYTE;

      for (j=0; j<dest->Ny; j++) {
	 dp = im_rowadr(dest, j);
	 for (i=0; i<dest->size.x; i++) {
	    switch (imtype) {
	     case IM_BINARY:
	       {
		  LAVERAGE(UBYTE,b);
		  *((UBYTE *)dp)++ = (average/npixels) < 128 ? 0 : 255;
	       }
	       break;
	     case IM_BYTE:
	       {
		  LAVERAGE(UBYTE,b);
		  *((UBYTE *)dp)++ = (UBYTE)ROUND(average/npixels);
	       }
	       break;
	     case IM_SHORT:
	       {
		  LAVERAGE(SHORT,s);
		  *((SHORT *)dp)++ = (SHORT)ROUND(average/npixels);
	       }
	       break;
	     case IM_LONG:
	       AVERAGE(LONG,l);
	       break;
	     case IM_FLOAT:
	       AVERAGE(float,f);
	       break;
	     case IM_DOUBLE:
	       AVERAGE(double,d);
	       break;
	     case IM_COMPLEX:
	       {
		  double average, iaverage;
		  average = iaverage = 0.0;
		  for (l=0; l<yskip; l++) {
		     sp = IM_ROWADR(src,j*yskip+l,c);
		     (complex *)sp += i*xskip;
		     for (k=0; k<xskip; k++) {
			average += (double)((complex *)sp)->r;
			iaverage += (double)((complex *)sp)->i;
			((complex *)sp)++;
		     }
		  }
		  ((complex *)dp)->r = (float)ROUND(average/npixels);
		  ((complex *)dp)->i = (float)ROUND(iaverage/npixels);
		  ((complex *)dp)++;
	       }
	       break;
	    }
	 }
      }
   }

#define SAMPLE(type) \
   (type *)sp += xoffset; \
   for (i=0; i<dest->size.x; i++) { \
      *((type *)dp)++ = *(type *)sp; \
      (type *)sp += xskip; \
   }

   if (flag == FLG_SAMPLE) {
      void *dp, *sp;
      int xoffset, yoffset;
      int i,j;

      xoffset = (xskip - 1) / 2;
      yoffset = (yskip - 1) / 2;
      for (j=0; j<dest->Ny; j++) {
	 dp = im_rowadr(dest,j);
	 sp = im_rowadr(src,j*yskip+yoffset);

	 switch (src->type) {
	  case IM_BINARY:
	  case IM_BYTE:
	  case IM_PALETTE:
	    SAMPLE(UBYTE);
	    break;
	  case IM_SHORT:
	    SAMPLE(SHORT);
	    break;
	  case IM_LONG:
	    SAMPLE(LONG);
	    break;
	  case IM_FLOAT:
	    SAMPLE(float);
	    break;
	  case IM_DOUBLE:
	    SAMPLE(double);
	    break;
	  case IM_COMPLEX:
	    SAMPLE(complex);
	    break;
	  case IM_RGB:
	    SAMPLE(colour);
	    break;
	 }
      }
   }

 exit_rx:
   pop_routine();
   return dest;
}

#undef LAVERAGE
#undef AVERAGE
#undef SAMPLE


image * old_im_expand( image * src, int factor , int flag )
{
   image *dest;
   int j,i;
   int l,k,lr,kr;
   int x1,y1;
   long int lval;
   long int tl_l,tr_l,bl_l,br_l;
   double val;
   complex cval;
   double dtl,dtr,dbl,dbr,td;

   log_routine("im_expand");

   if (NOT flag_ok(flag,FLG_ZOOM,FLG_INTERP,LASTFLAG))
      return NULL;

   if (NOT ip_parm_greatereq("factor", factor, 1)) {
      return NULL;
   }

   if (NOT ip_parm_mult2("factor", factor)) {
      return NULL;
   }

   dest = NULL;

   if ((src->type == IM_BINARY || src->type == IM_PALETTE) 
						&& flag != FLG_ZOOM) {
      log_error(ERR_BAD_FLAG);
      goto exit_e;
   }

   if ((dest = alloc_image(src->type,src->Nx*factor,src->Ny*factor)) == NULL)
      goto exit_e;

   if (dest->type == IM_PALETTE)
      im_palette_copy(dest,src);

   if (flag == FLG_ZOOM) {
      for (j=0; j<dest->Ny; j++) {
	 for (i=0; i<dest->Nx; i++) {
	    k = i / factor;
	    l = j / factor;
	    switch (src->type) {
	     case IM_BINARY:
	     case IM_BYTE:
	     case IM_PALETTE:
	       dest->image.b[Xd(i,j)] = src->image.b[Xs(k,l)];
	       break;
	     case IM_SHORT:
	       dest->image.s[Xd(i,j)] = src->image.s[Xs(k,l)];
	       break;
	     case IM_LONG:
	       dest->image.l[Xd(i,j)] = src->image.l[Xs(k,l)];
	       break;
	     case IM_FLOAT:
	       dest->image.f[Xd(i,j)] = src->image.f[Xs(k,l)];
	       break;
	     case IM_COMPLEX:
	       dest->image.c[Xd(i,j)] = src->image.c[Xs(k,l)];
	       break;
	     case IM_DOUBLE:
	       dest->image.d[Xd(i,j)] = src->image.d[Xs(k,l)];
	       break;
	    }
	 }
      }
   }

   /* Do fast routine for this special case */
   if (flag == FLG_INTERP && factor == 2 && src->type == IM_BYTE) {

      /* Most of image except last row and last column */

      for (j=0; j<src->Ny-1; j++) {
	 for (i=0; i<src->Nx-1; i++) {
	    k = i * factor;
	    l = j * factor;
	    tl_l = (dest->image.b[Xd(k,l)] = src->image.b[Xs(i,j)]);
	    dest->image.b[Xd(k+1,l)] = 
			(tl_l + (tr_l = src->image.b[Xs(i+1,j)])) / 2;
	    dest->image.b[Xd(k,l+1)] = 
			(tl_l + (bl_l = src->image.b[Xs(i,j+1)])) / 2;
	    dest->image.b[Xd(k+1,l+1)] = 
			(tl_l + tr_l + bl_l + src->image.b[Xs(i+1,j+1)]) / 4;
	 }
      }
      y1 = src->Ny-1;
      x1 = src->Nx-1;
      
      /* Do last column */

      k = x1 * factor;
      for (j=0; j<y1; j++) {
	 l = j * factor;
	 tl_l = (dest->image.b[Xd(k,l)] = src->image.b[Xs(i,j)]);
	 dest->image.b[Xd(k+1,l)] = tl_l;
	 dest->image.b[Xd(k,l+1)] = bl_l =
				(tl_l + src->image.b[Xs(i,j+1)]) / 2;
	 dest->image.b[Xd(k+1,l+1)] = bl_l;
      }

      /* Do last row */

      l = y1 * factor;
      for (i=0; i<x1; i++) {
	 k = i * factor;
	 l = j * factor;
	 tl_l = (dest->image.b[Xd(k,l)] = src->image.b[Xs(i,j)]);
	 dest->image.b[Xd(k,l+1)] = tl_l;
	 dest->image.b[Xd(k+1,l)] = tr_l =
				(tl_l + src->image.b[Xs(i+1,j)]) / 2;
	 dest->image.b[Xd(k+1,l+1)] = tr_l;
      }

      /* Do last few pixels in bottom right corner */

      k = y1 * factor;
      dest->image.b[Xd(k,l)] = dest->image.b[Xd(k+1,l)] =
		dest->image.b[Xd(k,l+1)] = dest->image.b[Xd(k+1,l+1)] =
						     src->image.b[Xs(x1,y1)];
   }else if (flag == FLG_INTERP) {
      zoom_bilinear(dest, src);
   }

 exit_e:
   pop_routine();
   return dest;

}




image * old_im_zoom( image * src, double xfactor, double yfactor , int flag )
{
   image *dest, *tmp;
   int xredf, yredf;
   int dx, dy;
   
   log_routine("im_zoom");

   if (NOT flag_ok(flag,FLG_BILINEAR,LASTFLAG))
      return NULL;

   dest = NULL;

   if (NOT ip_parm_greater("x_factor", xfactor, 0.0)) {
      return NULL;
   }

   if (NOT ip_parm_greater("y_factor", yfactor, 0.0)) {
      return NULL;
   }

   if (NOT (src->type == IM_BINARY ||
	        image_type_integer(src) || image_type_real(src))) {
      log_error(ERR_BAD_TYPE);
      goto exit_z;
   }

   dx = src->size.x * xfactor;
   dy = src->size.y * yfactor;

   if (dx == 0 || dy == 0) {
      log_error(ERR_BAD_PARAMETER);
      goto exit_z;
   }

   if (dx == src->size.x && dy == src->size.y) {
      dest = im_copy(src);
      goto exit_z;
   }

   xredf = 1.0 / xfactor;
   if (xredf == 0) xredf = 1.0;
   yredf = 1.0 / yfactor;
   if (yredf == 0) yredf = 1.0;

   if (xredf == 1 && yredf == 1) {

      if ((dest = alloc_image(src->type, dx, dy)) == NULL)
	 goto exit_z;

      zoom_bilinear(dest, src);

   }else{

      if ((tmp = im_reducex(src, xredf, yredf,
			    FLG_AVERAGE|FLG_INTERNAL)) == NULL)
	 goto exit_z;

      if (tmp->size.x == dx && tmp->size.y == dy) {
	 dest = tmp;
      }else{
	 if ((dest = alloc_image(src->type, dx, dy)) == NULL) {
	    free_image(tmp);
	    goto exit_z;
	 }
	 zoom_bilinear(dest, tmp);
	 free_image(tmp);
      }
   }

   /* If BINARY image and doing interpolation, threshold image
      to get back to a true BINARY image */

   if (dest->type == IM_BINARY && flag == FLG_BILINEAR) {
      int i,j;
      UBYTE *pixptr;
 
      for (j=0; j<dest->size.y; j++) {
         pixptr = IM_ROWADR(dest, j, b);
         for (i=0; i<dest->size.x; i++) {
            *pixptr = *pixptr>=128 ? 255 : 0;
            pixptr++;
         }
      }
   }

 exit_z:
   pop_routine();
   return dest;
}



static void zoom_bilinear(image *dest, image *src)
{
   int i,j;
   float xgap, ygap, xoffset, yoffset, xpos, ypos, x, y;

   xgap = (float)src->size.x / dest->size.x;
   ygap = (float)src->size.y / dest->size.y;
   xoffset = xgap / 2.0f - 0.5f;
   yoffset = ygap / 2.0f - 0.5f;

/* Get a particular pixel or zero if outside image bounds */
#define GetSrcPixel(im, xx, yy, type,t)        \
        (  (xx) >= im->size.x || (yy) >= im->size.y ) \
        ? (type) 0                              \
        : IM_PIXEL(im,xx,yy,t)


/* Find four nearest neighbours to pixel (i,j) and interpolate. */
#define Interpolation(type,t)                                \
        {                                                        \
            type tl_pixel, tr_pixel, bl_pixel, br_pixel;         \
                                                                 \
            tl_pixel = GetSrcPixel(src, (int)xpos, (int)ypos, type,t);    \
            tr_pixel = GetSrcPixel(src, ((int)xpos+1), (int)ypos, type,t);  \
            bl_pixel = GetSrcPixel(src, (int)xpos, ((int)ypos+1), type,t);  \
            br_pixel = GetSrcPixel(src, ((int)xpos+1),((int)ypos+1), type,t); \
            IM_PIXEL(dest,i,j,t) = (type)                        \
		       ( x * y * tl_pixel +              \
                               (1 - x) * y * tr_pixel +          \
                               x * (1 - y) * bl_pixel +          \
                               (1 - x) * (1 - y) * br_pixel );   \
	}


   for (j=0; j<dest->size.y; j++) {
      for (i=0; i<dest->size.x; i++) {

	 xpos = i * xgap + xoffset;
	 if (xpos < 0) xpos = 0;
	 if (xpos >= src->size.x) xpos = src->size.x-1;

	 ypos = j * ygap + yoffset;
	 if (ypos < 0) ypos = 0;
	 if (ypos >= src->size.y) ypos = src->size.y-1;

	 x = ((int)xpos + 1) - xpos;
	 y = ((int)ypos + 1) - ypos;

	 switch (src->type) {
	  case IM_BINARY:
	  case IM_BYTE:
	    Interpolation(UBYTE,b);
	    break;
	  case IM_SHORT:
	    Interpolation(SHORT,s);
	    break;
	  case IM_LONG:
	    Interpolation(LONG,l);
	    break;
	  case IM_FLOAT:
	    Interpolation(float,f);
	    break;
	  case IM_DOUBLE:
	    Interpolation(double,d);
	    break;
	 }
      }
   }
}




/* Tests of warp routine. */



#define IMROW_NEAREST_2(t,b)\
{\
   int i;\
   t *dp;\
   double E[5];\
   dp = im_rowadr(dest,j);\
   E[0] = 1.0;\
   for (i=1; i<poly->order; i++) {\
      E[i] = E[i-1] * (double)j;\
   }\
   for (i=0; i<dsize.x; i++) {\
      int k,l,m,count;\
      vector2d pt;\
      count = 0;\
      pt.x = pt.y = 0.0;\
      for (k=0; k<=poly->order; k++) {\
         for (l=0,m=k; l<=k; l++,m--) {\
            pt.x += poly->X[count]*D[m][i]*E[l];\
            pt.y += poly->Y[count]*D[m][i]*E[l];\
            count++;\
         }\
      }\
      if ((pt.x > -0.5) && (pt.x < rhs) && (pt.y > -0.5) && (pt.y < bhs)) {\
	 *dp++ = IM_PIXEL(src,(int)ROUND(pt.x),(int)ROUND(pt.y),b);\
      }else{\
	 *dp++ = 0;\
      }\
   }\
}


int im_warpx2(image *dest, image *src, poly2d *poly, int flag)
{
   int j;
   coord ssize, dsize;
   double rhs, bhs;
   double *D[5];
   double E[5];

   log_routine("im_warpx2");

   if (NOT images_same_type(dest, src)) {
      return ip_error;
   }

   if (NOT (image_type_integer(src) || image_type_real(src)
	     || src->type == IM_BINARY)) {
      log_error(ERR_UNSUPPORTED_TYPE);
      pop_routine();
      return ip_error;
   }

   if (NOT flag_ok(flag, FLG_BILINEAR, FLG_NEAREST, LASTFLAG))
      return ip_error;

   ssize = src->size;
   dsize = dest->size;

   rhs = ssize.x - 0.5;
   bhs = ssize.y - 0.5;

   D[0] = D[1] = D[2] = D[3] = D[4] = NULL;

   for (j=0; j<=poly->order; j++) {
      D[j] = ip_malloc(sizeof(double) * dsize.x);
   }
   for (j=0; j<dsize.x; j++) {
      int i;
      D[0][j] = 1.0;
      for (i=1; i<=poly->order; i++) {
	 D[i][j] = D[i-1][j] * (double)j;
      }
   }

   if (flag == FLG_NEAREST) {

     for (j=0; j<dsize.y; j++) {
        int i;
	E[0] = 1.0;
	for (i=1; i<=poly->order; i++) {
	   E[i] = E[i-1] * (double)j;
	}
	switch(src->type) {
         case IM_BINARY:
         case IM_BYTE:
	 case IM_PALETTE:
	   {
	     int i;
	     UBYTE *dp;
	     dp = im_rowadr(dest,j);
	     for (i=0; i<dsize.x; i++) {
	       int k,l,m,count;
	       vector2d pt;
	       count = 0;
	       pt.x = pt.y = 0.0;
	       for (k=0; k<=poly->order; k++) {
		 for (l=0,m=k; l<=k; l++,m--) {
		   pt.x += poly->X[count]*D[m][i]*E[l];
		   pt.y += poly->Y[count]*D[m][i]*E[l];
		   count++;
		 }
	       }
	       if ((pt.x > -0.5) && (pt.x < rhs) && (pt.y > -0.5) && (pt.y < bhs)) {
		 *dp++ = IM_PIXEL(src,(int)ROUND(pt.x),(int)ROUND(pt.y),b);
	       }else{
		 *dp++ = 0;
	       }
	     }
	   }
	   break;
         case IM_SHORT:
	   IMROW_NEAREST_2(SHORT,s);
	   break;
	 case IM_LONG:
	   IMROW_NEAREST_2(LONG,l);
	   break;
         case IM_FLOAT:
	   IMROW_NEAREST_2(float,f);
	   break;
         case IM_DOUBLE:
	   IMROW_NEAREST_2(double,d);
	   break;
	}
     }

   }
   for (j=0; j<=poly->order; j++) {
      ip_free(D[j]);
   }

   pop_routine();
   return OK;
}




#define IMROW_NEAREST_1(t,b)\
{\
   int i;\
   t *dp;\
   double y;\
   dp = im_rowadr(dest,j);\
   y = j;\
   for (i=0; i<dsize.x; i++) {\
   double x;\
   vector2d pt;\
   int count, k, l;\
   double pow1_x, pow2_x, pow_y;\
      x = i;\
      pt.x = pt.y = 0.;\
      count = 0;\
      pow1_x = 1;\
      for (l=1; l<=poly->order+1; l++) {\
         pow_y = 1;\
         pow2_x = pow1_x;\
         for (k=1; k<=l; k++) {\
            if (i == 0) pow2_x = (k < l) ? 0. : 1.;\
            pt.x += poly->X[count]*pow2_x*pow_y;\
            pt.y += poly->Y[count]*pow2_x*pow_y;\
            if (i != 0) pow2_x /= (double) x;\
            pow_y *= y;\
            count++;\
         }\
         pow1_x *= x;\
      }\
      if ((pt.x > -0.5) && (pt.x < rhs) && (pt.y > -0.5) && (pt.y < bhs)) {\
	 *dp++ = IM_PIXEL(src,(int)ROUND(pt.x),(int)ROUND(pt.y),b);\
      }else{\
	 *dp++ = 0;\
      }\
   }\
}


int im_warpx1(image *dest, image *src, poly2d *poly, int flag)
{
   int j;
   coord ssize, dsize;
   double rhs, bhs;

   log_routine("im_warpx1");

   if (NOT images_same_type(dest, src)) {
      return ip_error;
   }

   if (NOT (image_type_integer(src) || image_type_real(src)
	     || src->type == IM_BINARY)) {
      log_error(ERR_UNSUPPORTED_TYPE);
      pop_routine();
      return ip_error;
   }

   if (NOT flag_ok(flag, FLG_BILINEAR, FLG_NEAREST, LASTFLAG))
      return ip_error;

   ssize = src->size;
   dsize = dest->size;

   rhs = ssize.x - 0.5;
   bhs = ssize.y - 0.5;

   if (flag == FLG_NEAREST) {

     for (j=0; j<dsize.y; j++) {
        switch(src->type) {
         case IM_BINARY:
         case IM_BYTE:
	 case IM_PALETTE:
	   IMROW_NEAREST_1(UBYTE,b);
	   break;
         case IM_SHORT:
	   IMROW_NEAREST_1(SHORT,s);
	   break;
	 case IM_LONG:
	   IMROW_NEAREST_1(LONG,l);
	   break;
         case IM_FLOAT:
	   IMROW_NEAREST_1(float,f);
	   break;
         case IM_DOUBLE:
	   IMROW_NEAREST_1(double,d);
	   break;
	}
     }

   }

   pop_routine();
   return OK;
}



#define IMROW_NEAREST_3(t,b)\
{\
   t *dp;\
   dp = im_rowadr(dest,j);\
   for (i=0; i<dsize.x; i++) {\
      int k,l,m,count;\
      if ((pt.x > -0.5) && (pt.x < rhs) && (pt.y > -0.5) && (pt.y < bhs)) {\
	 *dp++ = IM_PIXEL(src,(int)ROUND(pt.x),(int)ROUND(pt.y),b);\
      }else{\
	 *dp++ = 0;\
      }\
      count = 1;\
      for (k=0; k<poly->order; k++) {\
	 for (l=0,m=k; l<=k; l++,m--) {\
	    pt.x += poly->X[count]*D[m][i]*E[l];\
	    pt.y += poly->Y[count]*D[m][i]*E[l];\
	    count++;\
	 }\
	 count++;\
      }\
   }\
}

#define IMROW_BILINEAR_INT(t,b)\
{\
   t *dp;\
   dp = im_rowadr(dest,j);\
   for (i=0; i<dsize.x; i++) {\
      int k,l,m,count;\
      double kfrac,lfrac,e,f;\
      if ((pt.x > 0.0) && (pt.x < rhsm1) && (pt.y > 0.0) && (pt.y < bhsm1)) {\
	 k = (int)pt.x;\
	 l = (int)pt.y;\
	 kfrac = pt.x - k;\
	 lfrac = pt.y - l;\
	 e = (1.0-lfrac)*(double)IM_PIXEL(src,k,l,b)\
	           + lfrac*(double)IM_PIXEL(src,k,l+1,b);\
	 f = (1.0-lfrac)*(double)IM_PIXEL(src,k+1,l,b)\
	           + lfrac*(double)IM_PIXEL(src,k+1,l+1,b);\
	 *dp++ = (t)ROUND((1-kfrac)*e + kfrac*f);\
      }else if ((pt.x > -0.5) \
		       && (pt.x < rhs) && (pt.y > -0.5) && (pt.y < bhs)) {\
	 *dp++ = IM_PIXEL(src,(int)ROUND(pt.x),(int)ROUND(pt.y),b);\
      }else{\
	 *dp++ = 0;\
      }\
      count = 1;\
      for (k=0; k<poly->order; k++) {\
	 for (l=0,m=k; l<=k; l++,m--) {\
	    pt.x += poly->X[count]*D[m][i]*E[l];\
	    pt.y += poly->Y[count]*D[m][i]*E[l];\
	    count++;\
	 }\
	 count++;\
      }\
   }\
}

#define IMROW_BILINEAR_REAL(t,b)\
{\
   t *dp;\
   dp = im_rowadr(dest,j);\
   for (i=0; i<dsize.x; i++) {\
      int k,l,m,count;\
      double kfrac,lfrac,e,f;\
      if ((pt.x > 0.0) && (pt.x < rhsm1) && (pt.y > 0.0) && (pt.y < bhsm1)) {\
	 k = (int)pt.x;\
	 l = (int)pt.y;\
	 kfrac = pt.x - k;\
	 lfrac = pt.y - l;\
	 e = (1.0-lfrac)*(double)IM_PIXEL(src,k,l,b)\
	           + lfrac*(double)IM_PIXEL(src,k,l+1,b);\
	 f = (1.0-lfrac)*(double)IM_PIXEL(src,k+1,l,b)\
	           + lfrac*(double)IM_PIXEL(src,k+1,l+1,b);\
	 *dp++ = (t)((1-kfrac)*e + kfrac*f);\
      }else if ((pt.x > -0.5) \
		       && (pt.x < rhs) && (pt.y > -0.5) && (pt.y < bhs)) {\
	 *dp++ = IM_PIXEL(src,(int)ROUND(pt.x),(int)ROUND(pt.y),b);\
      }else{\
	 *dp++ = 0;\
      }\
      count = 1;\
      for (k=0; k<poly->order; k++) {\
	 for (l=0,m=k; l<=k; l++,m--) {\
	    pt.x += poly->X[count]*D[m][i]*E[l];\
	    pt.y += poly->Y[count]*D[m][i]*E[l];\
	    count++;\
	 }\
	 count++;\
      }\
   }\
}

int im_warpx3(image *dest, image *src, poly2d *poly, int flag)
{
   int j;
   coord ssize, dsize;
   double rhs, bhs;
   double rhsm1, bhsm1;
   double *D[4];
   double E[4];

   log_routine("im_warpx3");

   if (NOT images_same_type(dest, src)) {
      return ip_error;
   }

   if (NOT (image_type_integer(src) || image_type_real(src)
	     || src->type == IM_BINARY)) {
      log_error(ERR_UNSUPPORTED_TYPE);
      pop_routine();
      return ip_error;
   }

   if (NOT flag_ok(flag, FLG_BILINEAR, FLG_NEAREST, LASTFLAG))
      return ip_error;

   ssize = src->size;
   dsize = dest->size;

   rhs = ssize.x - 0.5;
   bhs = ssize.y - 0.5;
   rhsm1 = ssize.x - 1.0;
   bhsm1 = ssize.y - 1.0;

   D[0] = D[1] = D[2] = D[3] = NULL;

   for (j=0; j<poly->order; j++) {
      D[j] = ip_malloc(sizeof(double) * dsize.x);
   }
   if (poly->order > 0) {
      for (j=0; j<dsize.x; j++) {
	 D[0][j] = 1.0;
	 if (poly->order > 1) {
	    double x = j;
	    D[1][j] = 2*x + 1;
	    if (poly->order > 2) {
	       D[2][j] = (3*x+3)*x + 1;
	       if (poly->order > 3) {
		  D[3][j] = ((4*x+6)*x+4)*x + 1;
	       }
	    }
	 }
      }
   }

   if (flag == FLG_NEAREST) {

     for (j=0; j<dsize.y; j++) {
        int i;
	vector2d pt;
	E[0] = 1.0;
	for (i=1; i<poly->order; i++) {
	   E[i] = E[i-1] * (double)j;
	}
	pt = poly2d_evaluate(poly,0,j);
	switch(src->type) {
         case IM_BINARY:
         case IM_BYTE:
	 case IM_PALETTE:
	   IMROW_NEAREST_3(UBYTE,b);
	   break;
         case IM_SHORT:
	   IMROW_NEAREST_3(SHORT,s);
	   break;
	 case IM_LONG:
	   IMROW_NEAREST_3(LONG,l);
	   break;
         case IM_FLOAT:
	   IMROW_NEAREST_3(float,f);
	   break;
         case IM_DOUBLE:
	   IMROW_NEAREST_3(double,d);
	   break;
	}
     }

   }else{

     for (j=0; j<dsize.y; j++) {
        int i;
	vector2d pt;
	E[0] = 1.0;
	for (i=1; i<poly->order; i++) {
	   E[i] = E[i-1] * (double)j;
	}
	pt = poly2d_evaluate(poly,0,j);
	switch(src->type) {
         case IM_BINARY:
         case IM_BYTE:
	 case IM_PALETTE:
	   IMROW_BILINEAR_INT(UBYTE,b);
	   break;
         case IM_SHORT:
	   IMROW_BILINEAR_INT(SHORT,s);
	   break;
	 case IM_LONG:
	   IMROW_BILINEAR_INT(LONG,l);
	   break;
         case IM_FLOAT:
	   IMROW_BILINEAR_REAL(float,f);
	   break;
         case IM_DOUBLE:
	   IMROW_BILINEAR_REAL(double,d);
	   break;
	}
     }

   }


   for (j=0; j<poly->order; j++) {
      ip_free(D[j]);
   }

   pop_routine();
   return OK;
}

static long int convert_type[IM_TYPEMAX] = {
   0x14L,                       /* IM_BYTE to I_CHRU */
   0x02L,                       /* IM_SHORT to I_SHORT */
   0x01L,                       /* IM_LONG to I_LONG */
   0x00L,                       /* IM_FLOAT to I_FLOAT */
   -1L,                         /* IM_DOUBLE not supported */
   -1L,                         /* IM_COMPLEX not supported */
   -1L,                         /* IM_PALETTE not supported */
   -1L,                         /* IM_RGB not supported */
   0x14L,                       /* IM_BINARY to I_CHRU */
};



#if 0
int old_im_warp(image *im, poly2d *poly, int flag)
{
   image *dest;
   long int vtype;
   int numcoeffs;
   int i, j;
   float *xpoly, *ypoly;

   log_routine("old_im_warp");
   dest = NULL;
   xpoly = NULL;
   
   if (NOT image_type_integer(im) && 
          im->type != IM_FLOAT && im->type != IM_BINARY) {
      log_error(ERR_UNSUPPORTED_TYPE);
      goto exit;
   }

   if (NOT flag_ok(flag, FLG_BILINEAR, FLG_NEAREST, LASTFLAG))
      return ip_error;

   vtype = convert_type[im->type];
   numcoeffs = poly2d_numcoeffs(poly);

   if ((xpoly = ip_malloc(2*numcoeffs * sizeof(float))) == NULL)
      return ip_error;

   ypoly = xpoly + numcoeffs;

   for (i=0; i<numcoeffs; i++) {
      xpoly[i] = poly->X[i];
      ypoly[i] = poly->Y[i];
   }

   if ((dest = alloc_image(im->type,im->size.x,im->size.y)) == NULL) {
      goto exit;
   }

   interp(im->image.v, dest->image.v, vtype,
          xpoly, ypoly, &poly->order,
          im->size.x, im->size.y, flag==FLG_BILINEAR);

   memcpy(im->image.v, dest->image.v,
          (size_t)ip_typesize[im->type] * im->size.x * im->size.y);
   if (im->type==IM_BINARY && flag==FLG_BILINEAR) {
      UBYTE *pixptr;

      for (j=0; j<im->size.y; j++) {
         pixptr = im_rowadr(im, j);
         for (i=0; i<im->size.x; i++) {
            *pixptr = *pixptr>=128 ? 255 : 0;
            pixptr++;
         }
      }
   }

 exit:
   if (dest)
      free_image(dest);
   if (xpoly)
      ip_free(xpoly);
   pop_routine();
   return ip_error;
}
#endif


/* $Id$ */
/*
   Module: ip/fft.c
   Author: M.J.Cree

   Fast Fourier transform images.

   � 1995-2001 Michael Cree 
   FFT routine � Numerical Recipes Software

ENTRY POINTS:

   im_fftshift()
   im_fft()

*/


#include <math.h>


static void fft(float *, int *, int, int);

/* Defn's used by im_fftshift */

#define FFTSHIFT(type,b) { \
   int i,j; \
   type *topquad, *botquad; \
   type tmp; \
   for (j=0; j<y2; j++) { \
      topquad = im_rowadr(im, j); \
      botquad = im_rowadr(im, j+y2); \
      for (i=0; i<x2; i++) { \
	 tmp = *topquad; \
         *topquad = *(botquad+x2); \
	 *(botquad+x2) = tmp; \
	 tmp = *botquad; \
         *botquad = *(topquad+x2); \
	 *(topquad+x2) = tmp; \
	 topquad++; botquad++; \
      } \
   } \
}


int old_im_fft(image *im, int flag)
{
   int size[2];
   int arsize;
   register int i;
   int isign, normalise;
   float divisor;
   register float *fp;

   log_routine("im_fft");

   if (im->type != IM_COMPLEX) {
      log_error(ERR_UNSUPPORTED_TYPE);
      goto exit_fft;
   }

   if (NOT ip_parm_powof2("image_size_x", im->size.x))
      return ip_error;
   if (NOT ip_parm_powof2("image_size_y", im->size.y))
      return ip_error;

   if (flag == -1) {		/* For backwards compatability */
      isign = -1;
      normalise = FALSE;
   }else if (flag == 1) {
      isign = 1;
      normalise = FALSE;
   }else{			/* New method of specifying operation */
      isign = (flag & FLG_INVERSE) ? -1 : 1;
      normalise = (flag & FLG_NORM);
   }

   /* Do FFT */

   size[0] = im->size.y;
   size[1] = im->size.x;
   fft(im->image.v, size, 2, isign);

   /* Normalise if requested (only on inverse FFT) */

   if (normalise && isign == -1) {
      arsize = 2 * im->size.x * im->size.y;
      divisor = im->size.x * im->size.y;
      fp = im->image.v;
      for (i=0; i<arsize; i++)
	 *fp++ /= divisor;
   }

 exit_fft:
   pop_routine();
   return ip_error;
}



/*

   Code following copied from Numerical Recipes. The University where I
   first developed this had a site licence to use Numerical Recipe code.

   The following code � Numerical Recipes Software

   I think it is their fourn() routine modified by:

      - Name is now fft()
      - Argument nn was long nn[], now int *nn
      - Arguments data and nn decremented at start of routine
        so that fft() can be called with conventinoal C arrays.

*/

#define SWAP(a,b) tempr=(a);(a)=(b);(b)=tempr

static void fft(float *data, int *nn, int ndim, int isign)
{
   int idim;
   unsigned long i1,i2,i3,i2rev,i3rev,ip1,ip2,ip3,ifp1,ifp2;
   unsigned long ibit,k1,k2,n,nprev,nrem,ntot;
   float tempi,tempr;
   double theta,wi,wpi,wpr,wr,wtemp;

   data--;     /* Some stupid prat wrote this with array indexing from 1 !!! */
   nn--;

   ntot = 1;
   for (idim=1; idim<=ndim; idim++)
      ntot *= nn[idim];
   nprev=1;
   for (idim=ndim; idim>=1; idim--) {
      n = nn[idim];
      nrem = ntot/(n*nprev);
      ip1 = nprev << 1;
      ip2 = ip1*n;
      ip3 = ip2*nrem;
      i2rev = 1;
      for (i2=1; i2<=ip2; i2+=ip1) {
	 if (i2 < i2rev) {
	    for (i1=i2; i1<=i2+ip1-2; i1+=2) {
	       for (i3=i1; i3<=ip3; i3+=ip2) {
		  i3rev = i2rev+i3-i2;
		  SWAP(data[i3],data[i3rev]);
		  SWAP(data[i3+1],data[i3rev+1]);
	       }
	    }
	 }
	 ibit = ip2 >> 1;
	 while (ibit >= ip1 && i2rev > ibit) {
	    i2rev -= ibit;
	    ibit >>= 1;
	 }
	 i2rev += ibit;
      }
      ifp1 = ip1;
      while (ifp1 < ip2) {
	 ifp2 = ifp1 << 1;
	 theta = isign*6.28318530717959/(ifp2/ip1);
	 wtemp = sin(0.5*theta);
	 wpr = -2.0*wtemp*wtemp;
	 wpi = sin(theta);
	 wr = 1.0;
	 wi = 0.0;
	 for (i3=1; i3<=ifp1; i3+=ip1) {
	    for (i1=i3; i1<=i3+ip1-2; i1+=2) {
	       for (i2=i1; i2<=ip3; i2+=ifp2) {
		  k1 = i2;
		  k2 = k1+ifp1;
		  tempr = wr*data[k2]-wi*data[k2+1];
		  tempi = wr*data[k2+1]+wi*data[k2];
		  data[k2] = data[k1]-tempr;
		  data[k2+1] = data[k1+1]-tempi;
		  data[k1] += tempr;
		  data[k1+1] += tempi;
	       }
	    }
	    wr = (wtemp = wr)*wpr-wi*wpi+wr;
	    wi = wi*wpr+wtemp*wpi+wi;
	 }
	 ifp1 = ifp2;
      }
      nprev *= n;
   }
}

#undef SWAP

