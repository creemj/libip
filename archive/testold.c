/* $Id$ */
/*
   Module: ip/testold.c
   Author: M.J.Cree

   Contains links for test programme to test old routines
   This file regularly changes as I install/remove checks.

*/


#include <stdlib.h>

#include <ip/internal.h>
#include <ip/stats.h>
#include <common/common.h>


#include "old.c"

/* All from test.c */

extern void dispim(image *im);
extern void chk_modified(int modified, image *im);
extern image *get_memory_image(void);

extern void timeit_I(int (*proc)(image *), image *);
extern void timeit_II(int (*proc)(image *, image *), image *, image *);

extern void timeit_If(int (*proc)(image *,int), image *, int f);
extern void timeit_IIf(int (*proc)(image *, image *,int), image *dest, image *src, int 
f);
extern void timeit_Iif(int (*proc)(image *,int,int), image *, int, int f);
extern void timeit_Iiif(int (*proc)(image *,int,int,int), image *, int, int, int f);
extern void timeit_Ivif(int (*proc)(image *,void *,int,int), image *, void *, int, int);
extern image * timeit_xIif(image * (*proc)(image *,int,int), image *, int, int f);
extern image * timeit_xIiif(image * (*proc)(image *,int,int,int), image *, int, int, int f);
extern image * timeit_xIddf(image * (*proc)(image *,double,double,int), image *, double, double, int f);

extern void timeit_ISR(int (*proc)(image *, stats *, ROI *), image *, stats *, ROI *);

#define timeit_Ii(p,i,c) timeit_If(p,i,c)
#define timeit_Iiii(p,i,c,d,e) timeit_Iiif(p,i,c,d,e)

extern void test_old(void);


extern image *gim;
extern image *rim;



void test_old(void)
{
   image *im, *memim;
   image *tmp;
   stats st;
   int option;
   int modified;
   int i,j;
   double xs,ys;
   char *options[]= {
      "Re-load local image from global image.",
      "Put local image into global image.",
      "Put local image into reference image.",
      "Run im_reduce() with FLG_SAMPLE.",
      "Run im_reduce() with FLG_AVERAGE.",
      "Run im_expand() with FLG_ZOOM.",
      "Run im_expand() with FLG_INTERP.",
      "Run im_reducex() with FLG_SAMPLE.",
      "Run im_reducex() with FLG_AVERAGE.",
      "Run im_zoom() with FLG_BILINEAR.",
      "Run im_flip() with FLG_HORIZ.",
      "Run im_flip() with FLG_VERT."
   };

   im = im_copy(gim);
   modified = FALSE;

   if (im->type != IM_COMPLEX)
      dispim(im);

   do {
      option = ask_choice(options, 10, 1);

      if (option >= 0) {
	 modified = TRUE;
      }

      memset(&st, 0, sizeof(st));

      switch (option) {
       case 0:
	 free_image(im);
	 im = im_copy(gim);
	 dispim(im);
	 break;
       case 1:
	 free_image(gim);
	 gim = im_copy(im);
	 modified = FALSE;
	 break;
       case 2:
	 free_image(rim);
	 rim = im_copy(im);
	 modified = FALSE;
	 break;
#if 0
       case 2:
	 free_image(im);
	 im = im_convert(gim, IM_COMPLEX, FLG_REAL);
	 if (rim->size.x == im->size.x && rim->size.y == im->size.y) {
	    /* Add reference into imaginary part */
	    int i,j;
	    complex *dst;
	    float *src;

	    if (rim->type == IM_FLOAT)
	       tmp = rim;
	    else
	       tmp = im_convert(rim, IM_FLOAT, NO_FLAG);
	    for (j=0; j<tmp->size.y; j++) {
	       src = IM_ROWADR(tmp,j,f);
	       dst = IM_ROWADR(im,j,c);
	       for (i=0; i<tmp->size.x; i++) {
		  dst->i = *src++;
		  dst++;
	       }
	    }
	    if (rim->type != IM_FLOAT)
	       free_image(tmp);
	 }
	 break;
#endif

       case 3:
         i = ask_double("Sample by", 1, 100);
         tmp = timeit_xIif(old_im_reduce, im, i, FLG_SAMPLE);
         if (tmp != NULL) {
           free_image(im);
           im = tmp;
           dispim(im);
         }
         break;
       case 4:
         i = ask_double("Reduce by", 1, 100);
         tmp = timeit_xIif(old_im_reduce, im, i, FLG_AVERAGE);
         if (tmp != NULL) {
           free_image(im);
           im = tmp;
           dispim(im);
         }
         break;
       case 5:
         i = ask_double("Expand by", 1, 100);
         tmp = timeit_xIif(old_im_expand, im, i, FLG_ZOOM);
         if (tmp != NULL) {
           free_image(im);
           im = tmp;
           dispim(im);
         }
         break;
       case 6:
         i = ask_double("Expand by", 1, 100);
         tmp = timeit_xIif(old_im_expand, im, i, FLG_INTERP);
         if (tmp != NULL) {
           free_image(im);
           im = tmp;
           dispim(im);
         }
         break;
       case 7:
         i = ask_double("Sample x by", 1, 100);
         j = ask_double("Sample y by", 1, 100);
         tmp = timeit_xIiif(old_im_reducex, im, i, j, FLG_SAMPLE);
         if (tmp != NULL) {
           free_image(im);
           im = tmp;
           dispim(im);
         }
         break;
       case 8:
         i = ask_double("Reduce x by", 1, 100);
         j = ask_double("Reduce y by", 1, 100);
         tmp = timeit_xIiif(old_im_reducex, im, i, j, FLG_AVERAGE);
         if (tmp != NULL) {
           free_image(im);
           im = tmp;
           dispim(im);
         }
         break;
       case 9:
         xs = ask_double("X skip", 0, 100);
         ys = ask_double("Y skip", 0, 100);
         tmp = timeit_xIddf(old_im_zoom, im, xs, ys, FLG_BILINEAR);
         if (tmp != NULL) {
           free_image(im);
           im = tmp;
           dispim(im);
         }
         break;
      }
   } while (option >= 0);

   chk_modified(modified, im);

   free_image(im);
}

