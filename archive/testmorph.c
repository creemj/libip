/*
   testmorph.c

   Stubs for etsting morphological routines. 

*/




#include <stdlib.h>
#include <stdio.h>
#include <ip/ip.h>
#include <debug/malloc.h>

extern void ip_dilate1d(image *im, unsigned int *line, void *g, void *h,
			int linelen, int selen);

extern void ip_erode1d(image *im, unsigned int *line, void *g, void *h,
			int linelen, int selen);

void bombout(logstack *ipr, int error, char *extra, va_list ap);
void dump_im(image *im);


main(argc,argv)
int argc;
char *argv[];
{
   image *im;
   void *g,*h;
   unsigned int *line;
   int i;
   int len;
   int selen;

   if (argc != 2)  {
      fprintf(stderr,"Usage: testmorph <selen>\n");
      exit(1);
   }

   register_error_handler(bombout);

   selen = atoi(argv[1]);
   len = 30;

   im = alloc_image(IM_BYTE,len,1);

   alloc_lgh(im,&line,&g,&h,selen);

   for (i=0; i<len; i++) {
      IM_PIXEL(im, i, 0, b) = (i > 11 && i < 19) ? 255 : 0;
      line[i] = i;
   }

   printf("Initial image:");
   dump_im(im);

   ip_dilate1d(im,line,g,h,len,selen);

   printf("Dilated image:");
   dump_im(im);

   for (i=0; i<len; i++) {
      IM_PIXEL(im, i, 0, b) = (i > 11 && i < 19) ? 255 : 0;
   }

   ip_erode1d(im,line,g,h,len,selen);

   printf("Eroded image:");
   dump_im(im);

   free_lgh(im, line, g, h, selen);

   return 0;
}




void bombout(logstack *ipr, int error, char *extra, va_list ap)
{
   print_error("testmorph",ipr,error,extra,ap);
   exit(1);
}


void dump_im(image *im)
{
   int i;

   printf("\n    ");
   for (i=0; i<im->size.x; i++) {
      printf("%3d%s",IM_PIXEL(im,i,0,b), i%16==15 ? "\n    " : " ");
   }
   printf("\n");
}

	 
