/* $Id$ */
/*
   Programme: testdisp.c
   Author: M.J.Cree


*/
/*
 * $Log$
 * Revision 1.6  1998/02/04 14:19:50  mph489
 * ???
 *
 * Revision 1.5  1997/09/15  15:23:00  mph489
 * Extra error generation checks.
 *
 * Revision 1.4  1997/09/03  09:17:50  mph489
 * Added dnmalloc debugging stuff.
 *
 * Revision 1.3  1997/08/20  15:38:50  mph489
 * Test for some error conditions.
 *
 * Revision 1.2  1997/08/15  10:15:50  mph489
 * Mods to infostr test.
 *
 * Revision 1.1  1997/08/13  13:05:25  mph489
 * Initial revision
 *
 */


#include <stdlib.h>
#include <string.h>

#include <ip/ip.h>
#include <ip/button.h>
#include <ip/infostr.h>

#include <common/common.h>


#include <dbmalloc/malloc.h>

static char rcsid[] = "$Id$";


/* Button declartion */

button_decl bdecl[4] = {
   {0, "Quit", NULL},
   {0, "Left", NULL},
   {0, "Play", NULL},
   {0, "Changes", NULL}
};

void bombout(logstack *ipr, int error, char *extra, va_list ap);
void optupd(char *str, char *new);
void test_label(void);


int dispid;


main(argc,argv)
int argc;
char *argv[];
{
   image *im;
   event_info *ms;
   button *bt;
   infostr *ifs;
   int i;
   int playing;
   int option;
   int buttoning;
   int chr;
   char *str;
   char buf[10];
   static char *choices[] = {
      "Test mouse no wait",
      "Test mouse with wait",
      "Test last mouse click no wait",
      "Test last mouse click with wait",
      "Test keypress no wait",
      "Test keypress with wait",
      "Test label routines",
      "Test buttons",
      "Test information requester",
      "Test palettes",
      "Wait for quit",
      "Test some errors"
   };

#ifdef _DEBUG_MALLOC_INC     /* Malloc debugging (see: dbmalloc) */
   unsigned long  histid1, histid2, orig_size, current_size;
   union dbmalloptarg  m;

   m.i = M_HANDLE_IGNORE;
   dbmallopt(MALLOC_WARN, &m);

   m.i = M_HANDLE_EXIT;
   dbmallopt(MALLOC_FATAL, &m);

   m.i = 1;
   dbmallopt(MALLOC_CKCHAIN, &m);
#endif  

#ifdef _DEBUG_MALLOC_INC   
   orig_size = malloc_inuse(&histid1);
#endif

   register_error_handler(bombout);

   im = alloc_image(IM_BYTE, 512, 512);

   im_create(im, (coord){256,256}, 1.0, 0.0, 1.0, TI_CONE | FLG_CLIP);
   im_drawbox(im, 255, (box){0,0,512,512});

   display_init(&argc, argv, (coord){640,580},
		"Display Test Programme", NO_FLAG);
   sleep(1);
   dispid = display_image(FLG_NEWIMAGE, im, (coord){0,20});

   printf("Image ID is %d.\n\n",dispid);

   free_image(im);

   playing = TRUE;
   while (playing) {
      option = ask_choice(choices, 12, 1);

      switch (option) {
       case -1:
	 playing = FALSE;
	 break;

       case 0:
	 printf("\ndisplay_mouse() with NOWAIT\n\n");

	 for (i=0; i<25; i++) {
	    ms = display_mouse(FLG_NOWAIT);
	    if (ms) {
	       printf("%3d: Id=%d, at (%d,%d)",i,ms->id,ms->pos.x,ms->pos.y);
	       if (ms->left) printf(" LEFT");
	       if (ms->middle) printf(" MIDDLE");
	       if (ms->right) printf(" RIGHT");
	       printf("\n");
	    }else{
	       printf("%3d: --\n",i);
	    }
	    sleep(1);
	 }
	 break;

       case 1:
	 printf("\ndisplay_mouse() with WAIT\n\n");
	 
	 for (i=0; i<25; i++) {
	    ms = display_mouse(FLG_WAIT);
	    if (ms) {
	       printf("%3d: Id=%d, at (%d,%d)",i,ms->id,ms->pos.x,ms->pos.y);
	       if (ms->left) printf(" LEFT");
	       if (ms->middle) printf(" MIDDLE");
	       if (ms->right) printf(" RIGHT");
	       printf("\n");
	    }
	 }
	 break;

       case 2:
	 printf("\ndisplay_click() with NOWAIT\n\n");

	 for (i=0; i<25; i++) {
	    ms = display_click(FLG_MOUSELEFT | FLG_MOUSERIGHT | FLG_NOWAIT);
	    if (ms) {
	       printf("%3d: Id=%d, at (%d,%d)",i,ms->id,ms->pos.x,ms->pos.y);
	       if (ms->left) printf(" LEFT");
	       if (ms->middle) printf(" MIDDLE");
	       if (ms->right) printf(" RIGHT");
	       printf("\n");
	    }else{
	       printf("%3d: --\n",i);
	    }
	    sleep(1);
	 }
	 break;

       case 3:
	 printf("\ndisplay_click() with WAIT\n\n");

	 for (i=0; i<25; i++) {
	    ms = display_click(FLG_MOUSELEFT | FLG_WAIT);
	    if (ms) {
	       printf("%3d: Id=%d, at (%d,%d)",i,ms->id,ms->pos.x,ms->pos.y);
	       if (ms->left) printf(" LEFT");
	       if (ms->middle) printf(" MIDDLE");
	       if (ms->right) printf(" RIGHT");
	       printf("\n");
	    }
	 }
	 break;

       case 4:
	 printf("\ndisplay_keypress() with NOWAIT\n\n");

	 for (i=0; i<25; i++) {
	    chr = display_keypress(FLG_NOWAIT);
	    if (chr != -1) {
	       printf("%3d: Char = %c (%d)\n",i,chr,chr);
	    }else{
	       printf("%3d: --\n",i);
	    }
	    sleep(1);
	 }
	 break;

       case 5:
	 printf("\ndisplay_keypress() wait WAIT\n\n");

	 for (i=0; i<25; i++) {
	    chr = display_keypress(FLG_WAIT);
	    if (chr != -1) {
	       printf("%3d: Char = %c (%d)\n",i,chr,chr);
	    }else{
	       printf("%3d: --\n",i);
	    }
	 }
	 break;

       case 6:
	 test_label();
	 break;

       case 7:
	 bt = alloc_button(4, bdecl, (coord){0,0});
	 button_set(bt, BT_TEXTPEN, OVL_RED);
	 button_display(bt);
	 buttoning = TRUE;
	 while (buttoning) {
	    ms = display_click(FLG_MOUSELEFT | FLG_WAIT);
	    i = button_press(bt, ms);
	    if (i>0) printf("Button <%s> pressed.\n", bdecl[i-1].uptext);
	    if (i == 1) buttoning = FALSE;
	    if (i == 4) {
	       button_set(bt, BT_STRING, 4, "Ooops");
	       button_display(bt);
	    }
	 }
	 free_button(bt);
	 break;

       case 8:
	 ifs = alloc_infostr((coord){0,0}, 400, 1, 12);
	 dump_infostr(ifs);
	 infostr_display(ifs);
	 sleep(3);
	 infostr_set(ifs, IFS_STRING, "Wait another three seconds please...");
	 infostr_display(ifs);
	 sleep(3);
	 infostr_set(ifs, IFS_TEXTPEN, OVL_RED);
	 infostr_set(ifs, IFS_BGPEN, 140);
	 infostr_set(ifs, IFS_ORIGIN, (coord){20,20});
	 dump_infostr(ifs);
	 str = infostr_getstr(ifs, "Input some text: ");	 
	 infostr_string(ifs, str);
	 infostr_display(ifs);
	 sleep(3);
	 free_infostr(ifs);
	 break;

       case 9:
	 printf("Press any key in display window to step onwards.\n");
	 chr = 0;
	 for (i=0; i<16; i++) {
	    sprintf(buf, "%d",i*16); 
	    display_label(dispid,buf,  OVL_WHITE, 6, (coord){i*16, 262});
	 }
	 im = alloc_image(IM_BYTE, 2, 2);
	 im_clear(im);
	 display_overlay(dispid, im, (coord){0,0}, NO_FLAG);
	 free_image(im);
	 for (i=0; chr!='q' && i<9; i++) {
	    display_palette(i);
	    chr = display_keypress(FLG_WAIT);
	 }
	 for (i=0; chr!='q' && i<9; i++) {
	    display_gamma(0.5 + i/10.0);
	    display_palette(PAL_GREY | PAL_GAMCORR);
	    chr = display_keypress(FLG_WAIT);
	 }
	 for (i=0; chr!='q' && i<9; i++) {
	    display_gamma(0.5 + i/10.0);
	    display_palette(PAL_HOTBODY | PAL_GAMCORR);
	    chr = display_keypress(FLG_WAIT);
	 }
	 display_gamma(1.0);
	 display_palette(PAL_GREY | PAL_GAMCORR);
	 display_clear(dispid, FLG_OVERLAY);
	 break;

       case 10:
	 ip_wait_for_quit();
	 break;

       case 11:
	 printf("\nTesting ip_parm_greater\n\n"); fflush(stdout);
	 log_routine("Testing");
	 if (NOT ip_parm_greater("test", -0.1, 0)) log_routine("Testing");
	 if (NOT ip_parm_greater("test", 0, 0)) log_routine("Testing");
	 if (NOT ip_parm_greater("test", 0.1, 0)) log_routine("Testing");

	 printf("\nTesting ip_parm_greatereq\n\n"); fflush(stdout);
	 if (NOT ip_parm_greatereq("test", -0.1, 0)) log_routine("Testing");
	 if (NOT ip_parm_greatereq("test", 0, 0)) log_routine("Testing");
	 if (NOT ip_parm_greatereq("test", 0.1, 0)) log_routine("Testing");

	 printf("\nTesting ip_parm_less\n\n"); fflush(stdout);
	 if (NOT ip_parm_less("test", -0.1, 0)) log_routine("Testing");
	 if (NOT ip_parm_less("test", 0, 0)) log_routine("Testing");
	 if (NOT ip_parm_less("test", 0.1, 0)) log_routine("Testing");

	 printf("\nTesting ip_parm_lesseq\n\n"); fflush(stdout);
	 if (NOT ip_parm_lesseq("test", -0.1, 0)) log_routine("Testing");
	 if (NOT ip_parm_lesseq("test", 0, 0)) log_routine("Testing");
	 if (NOT ip_parm_lesseq("test", 0.1, 0)) log_routine("Testing");

	 printf("\nTesting ip_parm_mult2\n\n"); fflush(stdout);
	 if (NOT ip_parm_mult2("test", 1)) log_routine("Testing");
	 if (NOT ip_parm_mult2("test", 2)) log_routine("Testing");
	 if (NOT ip_parm_mult2("test", 6)) log_routine("Testing");

	 printf("\nTesting ip_parm_powof2\n\n"); fflush(stdout);
	 if (NOT ip_parm_powof2("test", 1)) log_routine("Testing");
	 if (NOT ip_parm_powof2("test", 2)) log_routine("Testing");
	 if (NOT ip_parm_powof2("test", 6)) log_routine("Testing");
	 if (NOT ip_parm_powof2("test", 8)) pop_routine();

	 printf("Testing others\n"); fflush(stdout);
	 /* Trying to open already opened display */
	 display_init(NULL, NULL, (coord){0,0}, NULL, NO_FLAG);
	 im = alloc_image(IM_BYTE, 1, 1);
	 im_rowadr(im, 0);
	 im_rowadr(im, 1);
	 /* OOR parameter */
	 im_set(im, 100000);
	 /* Bad ID */
	 display_image(-1, im, (coord){0,0});
	 /* Unused ID */
	 display_image(dispid+1,  im, (coord){0,0});
	 free_image(im);
	 /* Bad parameters... */
	 display_gamma(0.0);
	 display_contrast(10000.0);
	 /* Too many palette entries */
	 load_palette(100000000, NULL);
	 break;
      }
   }

#ifdef _DEBUG_MALLOC_INC
   current_size = malloc_inuse(&histid2);
 
   if( current_size != orig_size )
      malloc_list(2, histid1, histid2);
#endif

   return 0;
}


void bombout(logstack *ipr, int error, char *extra, va_list ap)
{
   print_error("test",ipr,error,extra,ap);
   fflush(stdout);
   if (error == ERR_FILE_OPEN) {
      fprintf(stderr,"\n");
      return;
   }
   if (NOT ask_yesno("Should I continue testing"))
      exit(1);
}


void optupd(char *str, char *new)
{
   char *cptr;

   cptr = strrchr(str, '[');
   cptr++;
   sprintf(cptr, "%s]", new);
}


void test_label(void)
{
   int playing;
   int option;
   int width;
   char buf[80];
   static char str[70] = "Text";
   static char font[70] = "screen";
   static int size = 12;
   static coord pos = (coord){50,50};
   static int pen = 255;
   static char c_str[80] = "Set string []";
   static char c_font[80] = "Set font descriptor []";
   static char c_size[40] = "Set point size []";
   static char c_pos[40] = "Set position []";
   static char c_pen[40] = "Set pen colour []";
   char *choices[8] = {
      c_str,
      c_font,
      c_size,
      c_pos,
      c_pen,
      "Place string",
      "Get text width",
      "Clear overlays"
   };

   optupd(c_str, str);
   optupd(c_font, font);
   sprintf(buf, "%d", size);
   optupd(c_size, buf);
   sprintf(buf, "%d,%d", pos.x, pos.y);
   optupd(c_pos, buf);
   sprintf(buf, "%d", pen);
   optupd(c_pen, buf);

   playing = TRUE;
   while (playing) {
      option = ask_choice(choices, 8, 1);

      switch (option) {
       case -1:
	 playing = FALSE;
	 break;

       case 0:
	 strcpy(str, ask_string("New string:"));
	 optupd(c_str, str);
	 break;

       case 1:
	 printf("Not implemented\n");
	 break;

       case 2:
	 size = ask_double("Font size:",1,100);
	 sprintf(buf, "%d", size);
	 optupd(c_size, buf);
	 break;

       case 3:
	 pos = ask_coord("New position:", (coord){0,0}, (coord){640,512});
	 sprintf(buf, "%d,%d", pos.x, pos.y);
	 optupd(c_pos, buf);
	 break;

       case 4:
	 pen = ask_double("Pen colour:",0,255);
	 sprintf(buf, "%d", pen);
	 optupd(c_pen, buf);
	 break;

       case 5:
	 {
	 image *im;
	 display_label(dispid, str, pen, size, pos);
	 im = alloc_image(IM_BYTE, 2, 2);
	 im_clear(im);
	 display_overlay(dispid, im, (coord){0,0}, NO_FLAG);
	 free_image(im);
	 break;
         }
       case 6:
	 width = display_labelwidth(str, size);
	 printf("Label width = %d\n",width);
	 break;

       case 7:
	 display_clear(dispid, FLG_OVERLAY);
	 break;
      }
   }
}



