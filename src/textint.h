/* $Id$ */
/*

   Header: ip/textint.h
   Author: M. J. Cree

   Private structures to for text writing.

*/

#ifndef IP_TEXTINT_H
#define IP_TEXTINT_H

typedef struct {
   FT_Face face;
   int pixsize;
   int leading;
} ipfont;


#endif

