/*

   Module: ip/object.c
   Author: M.J.Cree

   Code related to object detection and measurement that is fundamental
   to other modules.

   Copyright (C) 1995-2002, 2018 Michael J. Cree

*/


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>
#include <math.h>

#include <ip/ip.h>
#include <ip/object.h>

#include "internal.h"
#include "utils.h"
#include "intobject.h"



/* Default allocations for row and rle (used in obj_get_row()) */

#define DEFROW 40		/* Default number of rows given to new object */
#define ATLROW 20		/* Allocate at least this number of rows */
#define DEFRLE 14		/* Default number of rle entries */


/*

NAME:

   ip_alloc_object_list() \- Allocate an (empty) object list

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/object.h>

   ip_objlist * ip_alloc_object_list(image)
   ip_image *image;

DESCRIPTION:

   Allocate an object list header. A pointer to the supplied image is
   put into the header, so an object list can be associated with an image.
   However, none of the supplied ip routines actually use the image field
   at present. Returns pointer to object list or NULL if allocation fails.

   Objects can be inserted into the list with ip_append_object() and
   ip_insert_object().  They can be removed with ip_remove_object() and
   ip_delete_object().  An object list can be walked in the following
   manner:

\|   ip_object *obj, *next;
\|
\|   for (obj = objlist->firstobj; obj; obj=next) {
\|      next = obj->next;
\|      // Do whatever with obj.
\|   }

   The use of the extra 'next' variable is only necessary if the
   object being looked at can be deleted as part of `Do whatever with
   obj''.

ERRORS:

   ERR_OUT_OF_MEM

SEE ALSO:

   ip_copy_object_list()
   ip_free_object_list()
   ip_insert_object()     ip_append_object()
   ip_remove_object()     ip_delete_object()
   im_create_object_list()

*/

ip_objlist *ip_alloc_object_list(ip_image *im)
{
    ip_objlist *ol;

    ip_log_routine(__func__);

    /* Allocate object list header */

    if ((ol = ip_malloc(sizeof(ip_objlist))) == NULL) {
	return NULL;
    }

    /* Initialise values */

    ol->image = im;		/* Image this object list is attached to */
    ol->numobjs = 0;		/* No objects found in image (so far) */
    ol->firstobj = NULL;		/* Ditto */

    ip_pop_routine();
    return ol;
}



/*

NAME:

   ip_alloc_object() \- Allocate an isolated object

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/object.h>

   ip_object * alloc_object(origin)
   ip_coord2d origin;

DESCRIPTION:

   Allocate an object structure. Returns an allocated object structure or
   NULL if the allocation fails. The origin is the top-left pixel point of
   the object. If unknown, a tempory value can be passed to this
   routine. The object can be considered to be the NULL object, that is, it
   contains no actual points, which means it is no use to the calling
   program until some points are entered into it, and it is ''completed''.
   Ip_alloc_object() is therefore practically useless to the calling
   program, but is used by internal ip routines.

   It is much more useful to call im_create_object_list() which creates an
   object list of ''completed'' objects from image data.

   Objects can be arranged into linked lists. See ip_alloc_object_list() for
   example code.  A number of useful measures can be made on objects, see
   ip_object_get().

LIMITATIONS:

   You must have an error handler installed when using object creation
   routines.  It is not always safe to return from the error handler
   when creating objects using some of the object creation routines.
   That is, the error handler must call exit() (or it''s equivalent).

   track_object() and region_grow() have not been updated to the new library
   paradigm and, thus, are unavailable for use.

ERRORS:

   ERR_OUT_OF_MEM

SEE ALSO:

   im_create_object_list()
   ip_copy_object()       ip_free_object()       ip_dump_object()
   ip_image_coord()       ip_object_coord()
   ip_object_set()        ip_object_modify()     ip_object_get()
   ip_point_in_object()
   ip_set_object_into_image()     ip_mask_object_into_image()
   ip_alloc_object_list()
   ip_obj_get_row()       ip_obj_insert_run()    ip_obj_insert_run_entry()
   ip_obj_get_run_end()   ip_obj_complete()
   ip_run_adjacent()      ip_run_overlap()

*/

ip_object *ip_alloc_object(ip_coord2d origin)
{
    ip_object *o;

    ip_log_routine(__func__);

    /* Allocate memory */

    if ((o = ip_malloc(sizeof(ip_object))) == NULL) {
	return NULL;
    }

    /* Initliase values */

    o->origin = origin;		/* Origin of top-left point wrt image */
				/* Don't know bounding box yet */
    o->bbox.br.x = o->bbox.br.y = 0;
    o->bbox.tl.x = origin.x;
    o->bbox.tl.y = origin.y;	/* except for top which is obvious */
    o->toprow = o->numrows = o->numpixels = 0; /* Unkown as yet */
    o->userdata = 0L;
    o->row = NULL;		/* Object contains no pixels  */
    o->next = NULL;		/* This object at end of list */

    SET_PARTIAL(o);		/* An uncompleted object */
    SET_RUNLEN(o);		/* Default status values */
    SET_OBJCOORD(o);

    ip_pop_routine();
    return o;
}




/*

NAME:

   ip_free_object() \- Free an object

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/object.h>

   void ip_free_object(object)
   ip_object *object;

DESCRIPTION:

   Frees an object (and all it''s memory) up. The object pointer is not
   valid to use after a call to ip_free_object(). This routine is safe to
   call with a NULL pointer. Do not call this routine for an object in a
   list, but rather use ip_delete_object(), which removes the object from
   the list first.

ERRORS:

   ERR_BAD_MEM_FREE

*/

void ip_free_object(ip_object *o)
{
    int i;

    ip_log_routine(__func__);
    if (o) {
	if (o->row) {
	    if (OBJ_PARTIAL(o)) {
		for (i=0; i <= o->botrow - o->toprow; i++) {
		    if (o->row[i].rle)
			ip_free(o->row[i].rle);
		}
	    }else{
		if (o->row->rle)
		    ip_free(o->row->rle);
	    }
	    ip_free(o->row);
	}
	ip_free(o);
    }
    ip_pop_routine();
}



/*

NAME:

   ip_free_object_list() \- Free up a complete list of objects

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/object.h>

   void ip_free_object_list(objlist)
   ip_objlist *objlist;

DESCRIPTION:

   Frees up an entire object list. All objects in the list are freed as is
   the list header. The objlist pointer is not valid for use after calling
   ip_free_object_list(). This routine is safe to call with a NULL pointer.

ERRORS:

   ERR_BAD_MEM_FREE

*/

void ip_free_object_list(ip_objlist *ol)
{
    ip_object *o, *next;

    ip_log_routine(__func__);
    if (ol) {
	for (o=ol->firstobj; o; o=next) {
	    next = o->next;
	    ip_free_object(o);
	}
	ip_free(ol);
    }
    ip_pop_routine();
}



/*

NAME:

   ip_insert_object() \- Insert an object into an object list

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/object.h>

   ip_object * ip_insert_object( objlist, position, object )
   ip_objlist *objlist;
   ip_object *position;
   ip_object *object;

DESCRIPTION:

   Inserts 'object' into the object list 'objlist'.  The object list must
   previously have been allocated (see ip_alloc_object_list()).  'Position'
   must point to the object in the object list where the 'object' should be
   inserted. To insert 'object' after the end of a list, position should be
   NULL.

   Note that inserting an object modifies it''s 'next' field. If the
   inserted object happens to be part of another list then the tail of that
   list (the part after 'object') will be chopped and probably lost for
   ever. To play safe, only insert individual objects that are not part of a
   list.

   To shift an object from one list to another, first call
   ip_remove_object() to take the object out of the first list then call
   ip_insert_object() or ip_append_object() to put it into the second list.

   This routine returns a pointer to the inserted object (identical to the
   argument 'object') if the insertion is successful. If the object pointed
   to by 'position' is not found in the object list, then no insertion
   occurs and NULL is returned.  This is not considered an error condition.

*/

ip_object *ip_insert_object(ip_objlist *ol, ip_object *pos, ip_object *obj)
{
    ip_object *o,*prev;

    if (pos == NULL) {		/* Insertion after end of list */
	o = ip_append_object(ol,obj);
	o->next = NULL;
    }else{
	prev = NULL;		/* General insertion case */
	for (o=ol->firstobj; o && o != pos; o=o->next)
	    prev = o;		/* Find position to insert at */
	if (o) {			/* If found, do insertion */
	    if (prev) {
		prev->next = obj;
	    }else{
		ol->firstobj = obj;
	    }
	    obj->next = o;
	    ol->numobjs++;
	}
    }
    return o ? obj : NULL;
}



/*

NAME:

   ip_append_object() \- Append object to end of object list

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/object.h>

   ip_object * ip_append_object( objlist, object )
   objlist *objlist;
   ip_object *object;

DESCRIPTION:

   Append 'object' to the end of the object list 'objlist'. This routine
   always returns a pointer to the object appended. Note that the object
   appended does not have its 'next' field modified. Therefore this routine
   can be used to append a number of objects in one go onto the object
   list. However, if you only mean to append one object, it is up to the
   calling programme to ensure the 'next' field is set to NULL, thus
   terminating the list. (All object creation routines set the 'next' field
   to NULL automatically, therefore one should have do to do it manually
   only rarely.)

*/

ip_object *ip_append_object(ip_objlist *ol, ip_object *obj)
{
    ip_object *o, *last;

    last = NULL;
    for (o=ol->firstobj; o; o=o->next) /* Go to end of list */
	last = o;
    if (last)			      /* Attach new object */
	last->next = obj;
    else
	ol->firstobj = obj;
    ol->numobjs++;
    return obj;
}



/*

NAME:

   ip_remove_object() \- Remove an object from an object list

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/object.h>

   ip_object * ip_remove_object( objlist , object )
   objlist *objlist;
   ip_object *object;

DESCRIPTION:

   Remove 'object' from list 'objlist' without destroying 'object'.  The
   object pointer 'object' remains valid.  If the removal is succesful the
   object pointer is returned. If the removal is unsuccesful (the object is
   not in this list) then NULL is returned.  It is not considered an error
   if the object is not in the list.

*/

ip_object *ip_remove_object(ip_objlist *ol, ip_object *obj)
{
    ip_object *o,*prev;

    prev = NULL;			/* Find object */
    for (o=ol->firstobj; o && o != obj; o=o->next)
	prev = o;
    if (o) {			/* If found, detach it from list */
	if (prev) {
	    prev->next = o->next;
	}else{
	    ol->firstobj = o->next;
	}
	ol->numobjs--;
	o->next = NULL;
    }
    return o;
}



/*

NAME:

   ip_delete_object() \- Delete object in object list

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/object.h>

   void ip_delete_object( objlist, obj )
   objlist *objlist;
   ip_object *obj;

DESCRIPTION:


   Remove 'object' from the object list 'objlist' and then free object space
   up. The object pointer 'object' is not valid for use after calling this
   routine.  If 'object' is not in the list then nothing is done.

*/

void ip_delete_object(ip_objlist *ol, ip_object *obj)
{
    ip_object *o;

    o = ip_remove_object(ol,obj);
    if (o)
	ip_free_object(obj);
}



/*

NAME:

   ip_copy_object() \- Copy an object into a new object structure

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/object.h>

   ip_object * ip_copy_object( srcobj )
   ip_object *srcobj;

DESCRIPTION:


   Make a complete copy of the object 'srcobj' and return the pointer to the
   new object. The new object''s 'next' field is set to NULL, but otherwise,
   the new object is identical to the 'srcobj' in every respect.

   If memory isn''t available to make the new object then NULL is returned
   and ERR_OUT_OF_MEM is flagged in the normal way.

ERRORS:

   ERR_OUT_OF_MEM

*/

ip_object *ip_copy_object(ip_object *srcobj)
{
    ip_object *obj;
    rowtype *row;
    rletype *rle;
    int i, numrle, rleidx;

    ip_log_routine(__func__);

    /* Allocate object and row array */

    if ((obj = ip_malloc(sizeof(ip_object))) == NULL) {
	return NULL;
    }

    memcpy(obj, srcobj, sizeof(ip_object) );
    obj->next = NULL;

    if ((obj->row = row = ip_malloc(srcobj->numrows * sizeof(rowtype))) == NULL) {
	ip_free(obj);
	return NULL;
    }

    /* Now copy object internal details across. */

    numrle = 0;
    for (i=0; i<srcobj->numrows; i++)
	numrle += srcobj->row[i].numentries;

    if ((rle = ip_malloc(numrle * sizeof(rletype))) == NULL) {
	ip_free(obj);
	ip_free(row);
	return NULL;
    }
    memcpy(rle, srcobj->row[0].rle, sizeof(rletype)*numrle );

    rleidx = 0;
    for (i=0; i<srcobj->numrows; i++) {
	row[i].rle = &rle[rleidx];
	rleidx += (row[i].numentries = srcobj->row[i].numentries);
    }

    ip_pop_routine();
    return obj;
}



/*

NAME:

   ip_copy_object_list() \- Copy a complete object list

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/object.h>

   ip_objlist * ip_copy_object_list( objlist )
   ip_objlist *objlist;

DESCRIPTION:

   Make a complete copy of the object list 'objlist'.  The new object list
   pointer is returned.  If the copy fails, a NULL pointer is returned.

ERRORS:

   ERR_OUT_OF_MEM

*/

ip_objlist *ip_copy_object_list(ip_objlist *olist)
{
    ip_objlist *newlist;
    ip_object *o, *newobj;

    ip_log_routine(__func__);

    /* Allocate new header */

    if ((newlist = ip_alloc_object_list(olist->image)) == NULL) {
	ip_pop_routine();
	return NULL;
    }

    /* Make a copy of each object and append to the new object list. */

    for (o=olist->firstobj; o; o=o->next) {
	if ((newobj = ip_copy_object(o)) == NULL) {
	    ip_free_object_list(newlist);
	    ip_pop_routine();
	    return NULL;
	}
	ip_append_object(newlist, newobj);
    }

    ip_pop_routine();
    return newlist;
}



/*

NAME:

   ip_object_set() \- Set attributes of an ip_object object.

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/object.h>

   int ip_object_set( action, obj, ... )
   unsigned int action;
   ip_object *obj;

DESCRIPTION:

   Set an attribute of an the ip_object 'obj'.  Currently the only 'action'
   possible is to set the object's origin.  Therefore one can do, for
   example,

\|   ip_object_set(OBJ_ORIGIN, obj, (coord){0,0});

   to set the object's origin to correspond exactly with the origin of the
   image.  The object's origin is defined to be the coordinate of the
   top-left corner of the smallest rectangle that encloses the object
   entirely.

   (TBD: I think the above specification of the origin is incorrect. The
   origin is that point on the object's boundary that is first encountered
   when doing a raster scan, thus the y-coordinate of the origin is the same
   as the bounding box top, but the x-coordinate of the origin is greater
   than or equal to the bounding box left edge and less than or equal to the
   bounding box right edge.)

ERRORS:

   ERR_BAD_ACTION

SEE ALSO:

   ip_object_modify()   ip_object_get()

*/

int ip_object_set(unsigned int action, ip_object *o, ...)
{
    va_list ap;
    ip_coord2d origin, shift;

    va_start(ap, o);
    ip_log_routine(__func__);

    switch (action) {
    case OBJ_ORIGIN:
	origin = va_arg(ap, ip_coord2d);
	shift.x = origin.x - o->origin.x;
	shift.y = origin.y - o->origin.y;
	o->origin = origin;
	o->bbox.tl.x += shift.x;
	o->bbox.tl.y += shift.y;
	o->bbox.br.x += shift.x;
	o->bbox.br.y += shift.y;
	break;
    default:
	ip_log_error(ERR_BAD_ACTION);
	break;
    }

    va_end(ap);
    ip_pop_routine();
    return ip_error;
}



/*

NAME:

   ip_object_modify() \- Modify an object's attributes.

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/object.h>

   int ip_object_modify( action, obj, ... )
   unsigned int action;
   ip_object *obj;

DESCRIPTION:

   Modify an attribute of the object 'obj'.  Only attribute that can
   be modified at present is the object's origin.  So, for example, to
   move the object down and right by one pixel do:

\|   ip_object_modify(OBJ_ORIGIN, obj, (coord){1,1});

ERRORS:

   ERR_BAD_ACTION

SEE ALSO:

   ip_object_set()   ip_object_get()

*/


int ip_object_modify(unsigned int action, ip_object *o, ...)
{
    va_list ap;
    ip_coord2d shift;

    va_start(ap, o);
    ip_log_routine(__func__);

    switch (action) {
    case OBJ_ORIGIN:
	shift = va_arg(ap, ip_coord2d);
	o->origin.x += shift.x;
	o->origin.y += shift.y;
	o->bbox.tl.x += shift.x;
	o->bbox.tl.y += shift.y;
	o->bbox.br.x += shift.x;
	o->bbox.br.y += shift.y;
	break;
    default:
	ip_log_error(ERR_BAD_ACTION);
	break;
    }

    va_end(ap);
    ip_pop_routine();
    return ip_error;
}



/*

NAME:

   ip_object_coord() \- Convert the image space coordinate into object space

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/object.h>

   ip_coord2d  ip_object_coord( obj, x )
   ip_object *obj;
   ip_coord2d x;

DESCRIPTION:

   Calculates the corresponding coordinate to the passed point 'x' in the
   object coordinate system. ('x' is assumed to be in image coordinates)

SEE ALSO:

   image_coord()

*/
ip_coord2d ip_object_coord(ip_object *o, ip_coord2d x)
{
    ip_coord2d f;

    f.x = x.x - o->origin.x;
    f.y = x.y - o->origin.y;
    return f;
}



/*

NAME:

   ip_image_coord() \- Convert the object space coordinate into image space

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/object.h>

   ip_coord2d  ip_image_coord( obj, x )
   ip_object *obj;
   ip_coord2d x;

DESCRIPTION:

   Calculates the corresponding coordinate to the passed point 'x' in the
   image coordinate system. ('x' is assumed to be in object coordinates)

SEE ALSO:

   ip_object_coord()

*/

ip_coord2d ip_image_coord(ip_object *o, ip_coord2d x)
{
    ip_coord2d f;

    f.x = x.x + o->origin.x;
    f.y = x.y + o->origin.y;
    return f;
}




/*

NAME:

   ip_point_in_object() \- Is the point within the object?

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/object.h>

   int  ip_point_in_object( obj, pt )
   ip_object *obj;
   ip_coord2d pt;

DESCRIPTION:

   Returns TRUE if the point 'pt' (which is in image coordinates) is located
   somewhere in the object 'obj'.  Note that this is an exact fit; it is not
   enough for the 'pt' to be within the object bounding box.  It must
   correspond to an actual object pixel before TRUE is returned.

   If this routine is called on a 'completed' object (as it should only be),
   then no error conditions can occur, despite the fact that obj_get_row()
   is called.

*/

int ip_point_in_object(ip_object *o, ip_coord2d pt)
{
    ip_coord2d opt;
    rowtype *row;
    rletype *rle;
    int i;

    /* Check first whether point is within the bounding-box of the object */

    if (pt.x < o->bbox.tl.x || pt.x > o->bbox.br.x ||
	pt.y < o->bbox.tl.y || pt.y > o->bbox.br.y)
	return FALSE;

    /* Possibly in object, so now check through corresponding row of object,
       to see if point really lies on an object point */

    ip_log_routine(__func__);

    opt = ip_object_coord(o, pt);

    row = ip_obj_get_row(o, opt.y);
    rle = row->rle;

    for (i=0; i<row->numentries; i++)
	if (opt.x >= rle[i].start && (opt.x < rle[i].start + rle[i].length))
	    break;

    /* If i < row->numentries then an rle entry was found that contains pt */

    ip_pop_routine();
    return (i < row->numentries);
}




/*

NAME:

   ip_set_object_into_image() \- Draw object into image

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/object.h>

   int  ip_set_object_into_image( image, object, value )
   ip_image *image;
   ip_object *object;
   double value;

DESCRIPTION:

   This routine ''sets'' 'object' into 'image' with the pixel value 'value'.
   That is, wherever 'object' has set pixels, then the corresponding pixel
   in 'image' is set to 'value'.

LIMITATIONS:

   RGB and COMPLEX images not supported.

ERRORS:

   ERR_BAD_PARAMETER

SEE ALSO:

   ip_set_object_into_imagex()       ip_mask_object_into_image()

*/

typedef void (*set_run_function)(void *, int, int, double, int);

#define generate_set_run_into_image_function(tt, vv)			\
    static void set_run_into_image_##tt(void *adr, int x, int len, double val, int ival) \
    {									\
	tt##_type *row = (tt##_type *)adr;				\
	row += x;							\
	for (int k = 0; k<len; k++)					\
	    *row++ = vv;						\
    }

generate_set_run_into_image_function(UBYTE, ival)
generate_set_run_into_image_function(USHORT, ival)
generate_set_run_into_image_function(ULONG, ival)
generate_set_run_into_image_function(FLOAT, val)
generate_set_run_into_image_function(DOUBLE, val)

set_run_function set_run_into_image_bytype[IM_TYPEMAX] = {
    [IM_BINARY] = set_run_into_image_UBYTE,
    [IM_BYTE] = set_run_into_image_UBYTE,
    [IM_UBYTE] = set_run_into_image_UBYTE,
    [IM_PALETTE] = set_run_into_image_UBYTE,
    [IM_USHORT] = set_run_into_image_USHORT,
    [IM_SHORT] = set_run_into_image_USHORT,
    [IM_ULONG] = set_run_into_image_ULONG,
    [IM_LONG] = set_run_into_image_ULONG,
    [IM_FLOAT] = set_run_into_image_FLOAT,
    [IM_DOUBLE] = set_run_into_image_DOUBLE
};

int ip_set_object_into_image(ip_image *im, ip_object *obj, double val)
{
    int x, y;
    int ival;
    rowtype *row;
    rletype *rle;
    void *ptr;

    ip_log_routine(__func__);

    if (NOT image_valid(im))
	return ip_error;

    if (NOT ip_box_in_image("obj.box", ip_object_get(OBJ_BBOX, obj).box, im))
	return ip_error;

    if (NOT ip_value_in_image("val", val, im))
	return ip_error;

    ival = ROUND(val);
    if (im->type == IM_BINARY)
	ival = ival ? 255 : 0;

    if (set_run_into_image_bytype[im->type] == NULL) {
	ip_log_error(ERR_UNSUPPORTED_TYPE);
	ip_pop_routine();
	return ip_error;
    }

    /* For each row in object */

    for (int i=0; i<obj->numrows; i++) {
	row = ip_obj_get_row(obj, i);
	rle = row->rle;
	y = i + obj->origin.y;

	/* For each run in row */

	for (int j=0; j<row->numentries; j++) {
	    x = rle[j].start + obj->origin.x;

	    /* set run into image */

	    ptr = IM_ROWADR(im, y, v);
	    set_run_into_image_bytype[im->type](ptr, x, rle[j].length, val, ival);
	}
    }

    ip_pop_routine();
    return ip_error;
}




/*

NAME:

   ip_set_object_into_imagex() \- Draw object into image

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/object.h>

   int  ip_set_object_into_imagex( image, object, value1, value2, value3 )
   ip_image *image;
   ip_object *object;
   double value1, value2, value3;

DESCRIPTION:

   This routine ''sets'' 'object' into 'image' with the pixel value
   specified by 'value1', 'value2' and 'value3'.  That is, wherever 'object'
   has set pixels, then the corresponding pixel in 'image' is set according
   to the three value parameters.

   If the image has a one-dimensional range (eg, BYTE, SHORT, DOUBLE images)
   then only the parameter 'value1' is used in setting the pixel value.

   If the image has a two-dimensional range (COMPLEX images) then 'value1'
   as sued as the real part and 'value2' is used as the imaginary part of the
   complex number to set into the image.

   If the image has a three-dimensional range (RGB and RGB16) images then
   the 'value1' is used as the red component, 'value2' as the green
   component and 'value3' as the blue component of a colour to set into the
   image.

ERRORS:

   ERR_BAD_PARAMETER

SEE ALSO:

   mask_object_into_image()

*/


typedef void (*set_into_im_function)(ip_image *, ip_object *, double, double, double);

#define generate_set_oneval_into_im(tt)					\
    static void set_oneval_into_im_##tt(ip_image *im, ip_object *obj,	\
					double v1, double v2, double v3) \
    {									\
	tt##_type val;							\
	val = (tt##_type)v1;						\
	for (int i=0; i<obj->numrows; i++) {				\
	    rowtype *row = ip_obj_get_row(obj,i);			\
	    rletype *rle = row->rle;					\
	    int y = i + obj->origin.y;					\
	    for (int j=0; j<row->numentries; j++) {			\
		int x = rle[j].start + obj->origin.x;			\
		tt##_type *ptr = IM_ROWADR(im, y, v);			\
		ptr += x;						\
		for (int k=0 ; k < rle[j].length ; k++)			\
		    *ptr++ = val;					\
	    }								\
	}								\
    }

#define generate_set_complex_into_im(tt, bt)				\
    static void set_complex_into_im_##tt(ip_image *im, ip_object *obj,	\
					double v1, double v2, double v3) \
    {									\
	tt##_type val;							\
	val.r = (bt)v1;							\
	val.i = (bt)v2;							\
	for (int i=0; i<obj->numrows; i++) {				\
	    rowtype *row = ip_obj_get_row(obj,i);			\
	    rletype *rle = row->rle;					\
	    int y = i + obj->origin.y;					\
	    for (int j=0; j<row->numentries; j++) {			\
		int x = rle[j].start + obj->origin.x;			\
		tt##_type *ptr = IM_ROWADR(im, y, v);			\
		ptr += x;						\
		for (int k=0 ; k < rle[j].length ; k++)			\
		    *ptr++ = val;					\
	    }								\
	}								\
    }

#define generate_set_rgb_into_im(tt, bt)				\
    static void set_rgb_into_im_##tt(ip_image *im, ip_object *obj,	\
				 double v1, double v2, double v3)	\
    {									\
	tt##_type val;							\
	val.r = (bt)v1;							\
	val.g = (bt)v2;							\
	val.b = (bt)v3;							\
	for (int i=0; i<obj->numrows; i++) {				\
	    rowtype *row = ip_obj_get_row(obj, i);			\
	    rletype *rle = row->rle;					\
	    int y = i + obj->origin.y;					\
	    for (int j=0; j<row->numentries; j++) {			\
		int x = rle[j].start + obj->origin.x;			\
		tt##_type *ptr = IM_ROWADR(im, y, v);			\
		ptr += x;						\
		for (int k=0 ; k < rle[j].length ; k++)			\
		    *ptr++ = val;					\
	    }								\
	}								\
    }


generate_set_oneval_into_im(UBYTE)
generate_set_oneval_into_im(BYTE)
generate_set_oneval_into_im(USHORT)
generate_set_oneval_into_im(SHORT)
generate_set_oneval_into_im(ULONG)
generate_set_oneval_into_im(LONG)
generate_set_oneval_into_im(FLOAT)
generate_set_oneval_into_im(DOUBLE)
generate_set_complex_into_im(COMPLEX, float)
generate_set_complex_into_im(DCOMPLEX, double)
generate_set_rgb_into_im(RGB, uint8_t)
generate_set_rgb_into_im(RGB16, uint16_t)


set_into_im_function set_object_into_image_bytype[IM_TYPEMAX] = {
    [IM_BINARY] = set_oneval_into_im_UBYTE,
    [IM_PALETTE] = set_oneval_into_im_UBYTE,
    [IM_BYTE] = set_oneval_into_im_BYTE,
    [IM_UBYTE] = set_oneval_into_im_UBYTE,
    [IM_SHORT] = set_oneval_into_im_SHORT,
    [IM_USHORT] = set_oneval_into_im_USHORT,
    [IM_LONG] = set_oneval_into_im_LONG,
    [IM_ULONG] = set_oneval_into_im_ULONG,
    [IM_FLOAT] = set_oneval_into_im_FLOAT,
    [IM_DOUBLE] = set_oneval_into_im_DOUBLE,
    [IM_COMPLEX] = set_complex_into_im_COMPLEX,
    [IM_DCOMPLEX] = set_complex_into_im_DCOMPLEX,
    [IM_RGB] = set_rgb_into_im_RGB,
    [IM_RGB16] = set_rgb_into_im_RGB16
};

int ip_set_object_into_imagex(ip_image *im, ip_object *obj,
			      double val1, double val2, double val3)
{
    ip_log_routine(__func__);

    if (NOT image_valid(im))
	return ip_error;

    if (NOT ip_box_in_image("obj.box", ip_object_get(OBJ_BBOX,obj).box, im))
	return ip_error;

    if (NOT ip_value_in_image("val1", val1, im))
	return ip_error;

    if (im->type == IM_COMPLEX || im->type == IM_DCOMPLEX
	|| im->type == IM_RGB || im->type == IM_RGBA || im->type == IM_RGB16) {
	if (NOT ip_value_in_image("val2", val2, im))
	    return ip_error;
    }

    if (im->type == IM_RGB || im->type == IM_RGBA || im->type == IM_RGB16) {
	if (NOT ip_value_in_image("val3", val3, im))
	    return ip_error;
    }

    if (im->type == IM_BINARY) {
	val1 = (val1 != 0 ? 255 : 0);
    }

    if (set_object_into_image_bytype[im->type])
	set_object_into_image_bytype[im->type](im, obj, val1, val2, val3);
    else
	ip_log_error(ERR_UNSUPPORTED_TYPE);

    ip_pop_routine();
    return ip_error;
}



/*

NAME:

   ip_mask_object_into_image() \- Mask object into image

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/object.h>

   int  ip_mask_object_into_image( image, object, immin, immax, objmin, objmax )
   ip_image *image;
   ip_object *object;
   int immin, immax, objmin, objmax;

DESCRIPTION:

   Like ip_set_object_into_mask(), except a more complicated formula is used to
   put the object pixel into the image. For every pixel in the object, the
   corresponding pixel in the image is changed to

\|       Pf = (Pi - immin)scale + objmin

   where scale is defn by

\|       scale = (objmax - objmin) / (immax - immin)

   and Pi is the initial image pixel value and Pf is the final image pixel
   value. Real arithmetic is used for the calculation which is then rounded
   to give Pf.

   If this routine is called on a 'completed' object (as it should
   only be), then no error conditions can occur, despite the fact that
   ip_obj_get_row() is called.

LIMITATIONS:

   Currently only works with UBYTE images.

ERRORS:

   ERR_BAD_PARAMETER

SEE ALSO:

   ip_set_object_into_image()

*/

int ip_mask_object_into_image(ip_image *im, ip_object *obj,
			      int immin, int immax, int objmin, int objmax)
{
    double scale;

    ip_log_routine(__func__);
   
    if (NOT ip_box_in_image("obj.box", ip_object_get(OBJ_BBOX,obj).box, im))
	return ip_error;

    scale = (objmax - objmin) / ((double)immax - immin);
    for (int i=0; i<obj->numrows; i++) {
	rowtype *row = ip_obj_get_row(obj,i);
	rletype *rle = row->rle;
	int y = i + obj->origin.y;
	for (int j=0; j<row->numentries; j++) {
	    int x = rle[j].start + obj->origin.x;
	    uint8_t *ptr = IM_ROWADR(im, y, v);
	    ptr += x;
	    for (int k=0 ; k < rle[j].length ; k++) {
		int val = ptr[k];
		val = ROUND((val - immin) * scale + objmin);
		ptr[k] = val;
	    }
	}
    }
    ip_pop_routine();
    return ip_error;
}




/*

NAME:

   ip_dump_object() \- Dump a pretty picture of an object

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/object.h>

   void  ip_dump_object( file, obj, flags )
   FILE *file;
   ip_object *obj;
   int flags;

DESCRIPTION:

   Dumps the object definition to the specified file. If 'flags' includes
   DBG_PICTORIAL then dumps a pretty picture representation of the object to
   the file.  Useful for debugging purposes, if one needs to know the exact
   form of the object.

   An example pictorial output is:

\|   Object at (10,9)
\|    bounding box = (9,9) to (11,11)
\|    Number of rows = 3, total number of pixels = 5
\|    user data = 0
\|     1:  1 -X-
\|     2:  1 XXX
\|     3:  1 -X-

   In the above, an ''X'' means an object pixel and a ''-'' is used for a
   filler.  The numbers at the beginning of each row indicate the row number
   (terminated with '':'') followed by the number of rle (runs, usually
   encoded with run-length encoding, hence ''rle'') entries in the row.

   This routine can go hay-wire if a badly formed object structure is passed
   to it with DBG_PICTORIAL.  Pictorial dump mode is only guarenteed to work
   with ''completed'' objects. In normal mode the rle entries are printed as
   coordinate pairs, and this routine will have a lot more success at
   handling ''partial'' or badly formed objects without crashing.

   Ip_dump_object() does not use ip_obj_get_row() to access an object row,
   rather, the access is hard coded. This is to ensure the object structure
   is not modified in any way, and to provide more robustness against
   corrupted objects.

BUGS:

   At present, the support for ''partial'' objects is not complete. It will
   probably (but not necessarily) crash on such objects.

SEE ALSO:

   ip_alloc_object()

*/

void ip_dump_object(FILE *f, ip_object *obj, int flags)
{
    int ll, lr;

    ip_log_routine(__func__);

    /* Dump some stats on object */

    fprintf(f, "Object at (%d,%d)\n", obj->origin.x, obj->origin.y);
    fprintf(f, " bounding box = (%d,%d) to (%d,%d)\n",
	    obj->bbox.tl.x, obj->bbox.tl.y,
	    obj->bbox.br.x, obj->bbox.br.y);
    fprintf(f, " number of rows = %d, total number of pixels = %d\n",
	    obj->numrows, obj->numpixels);
    fprintf(f, " user data = %ld\n", obj->userdata);

    /* Number of points to left and right of origin */

    ll = obj->bbox.tl.x - obj->origin.x;
    lr = obj->bbox.br.x - obj->origin.x + 1;

    /* Dump pretty picture of object */

    for (int i=0; i<obj->numrows; i++) {
	rowtype *row = &obj->row[i];
	rletype *rle = row->rle;
	if (!rle) {
	    fprintf(f, "%3d: rle NULL!!!\n",i);
	    continue;
	}
	int pos = ll;
	fprintf(f, "%3d:%3d ", i, row->numentries);
	for (int j=0; j<row->numentries; j++) {
	    if (flags & DBG_PICTORIAL) {
		int k;
		for ( ; pos < rle[j].start; pos++)
		    fputc('-', f);
		for (k=0 ; k < rle[j].length; k++)
		    fputc('#', f);
		pos += k;
	    }else{
		fprintf(f, " (%d,%d)", rle[j].start, rle[j].length);
	    }
	}
	if (flags & DBG_PICTORIAL) {
	    for ( ; pos < lr ; pos++)
		fputc('-', f);
	}
	fputc('\n', f);
    }
    fflush(f);		/* Do this to synchronise with stderr */

    ip_pop_routine();
}




/*

NAME:

   ip_dump_object_list() \- Dump the complete object list

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/object.h>

   void  ip_dump_object_list( file, ol, flags )
   FILE *file;
   ip_objlist *ol;
   int flags;

DESCRIPTION:

   Walks thorugh an object list, calling ip_dump_object() on every object in
   the list. This may result in a LOT of messages being sent to file if the
   object list is large!

   See ip_dump_object().

BUGS:

   See ip_dump_object().

*/

void ip_dump_object_list(FILE *f, ip_objlist *ol, int flags)
{
    ip_object *o;

    ip_log_routine(__func__);

    fprintf(f, "Object list dump (head at %p, image %p, num of objs %d)\n",
	    ol, ol->image, ol->numobjs);

    for (o=ol->firstobj; o; o=o->next)
	ip_dump_object(f, o, flags);

    fprintf(f, "\n");
    fflush(f);		/* To synchronise with stderr */

    ip_pop_routine();
}



/*
 * The following are internal routines to the ip library. The user program
 * should never have any occasion to call these. Only use them if you are
 * adding new routines to the ip object library.
 */



/*

NAME:

   ip_obj_get_row() \- Get row address within object

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/object.h>

   rowtype * ip_obj_get_row( obj, rowidx )
   ip_object *obj;
   int rowidx;

DESCRIPTION:

   This routine is only intended for use within the ip library.

   This routine MUST be called before accessing a row of pixels within an
   object. It handles both ''partial'' and ''completed'' objects. It is
   called with the object to be accessed/modified and with the row index,
   which is the y-coordinate (in object coordinates) of the row to be
   accessed.  It returns a pointer to the row structure of the particular
   row required.

   If the object is a ''completed'' object it just returns the pointer to
   the row. It is only safe to read the information in the row.  It is NOT
   safe to modify any data in the row. The only error possible is
   ERR_OBJ_INVALID_ROW if rowidx is ridiculous. In that case a NULL pointer
   is returned.

   If the object is a ''partial'' object, then ip_obj_get_row() does any
   necessary memory allocation and reorganisations of the object structure
   to ensure that the requested row (and its rle array) exist and are valid
   to access. It quarantees that TWO free spaces exist in the rle
   array. That is, it guarantees that is is safe to call
   ip_obj_insert_run_entry() a maximum of two times on the row, without
   calling ip_obj_get_row() again. If any memory allocations are necessary
   for the row, and the memory allocation fails, then a NULL pointer is
   returned and ERR_OUT_OF_MEM is flagged. Note that an invalid row is only
   possible with a negative rowidx. A large positive rowidx just causes the
   object structure to grow to encompass the new row.

   Every attempt is made to ensure the object remains valid as a ''partial''
   object even when a memory allocation fails.

   All dynamic row and rle allocations for objects are done in this
   routine. They should occur in no other routine at all (I hope).
   Ip_dump_object() should be the only other routine that has hard coded access
   to the row addresses (but see notes below).

NOTES:

   The object creation routine im_create_object_list() in module objgrow.c
   has hardcoded accesses to the object rows and runs, and does not use this
   routine.

ERRORS:

   ERR_OUT_OF_MEM
   ERR_BAD_MEM_FREE
   ERR_OBJ_INVALID_ROW
   ERR_RLE_ARRAY_OVERFLOW

BUGS:

   Negative rowidx for ''partial'' objects currently doesn''t generate an
   error.

SEE ALSO:

   ip_alloc_object()

*/

rowtype *ip_obj_get_row(ip_object *obj, int rowidx)
{
    rowtype *row, *nrow;
    rletype *nrle;
    int ntop,nbot,nsize;
    int i;

    ip_log_routine(__func__);

    row = NULL;			/* In anticipation of memory allocation fail */

    if (OBJ_COMPLETE(obj)) {	/* Do completed object */

	if (rowidx < 0 || rowidx >= obj->numrows) {
	    ip_log_error(ERR_OBJ_INVALID_ROW);
	}else{
	    row = &obj->row[rowidx];
	}

    }else{			/* Do partial object */

	/* Ensure row array is allocated */

	if (obj->row == NULL) {
	    if ((obj->row = ip_malloc(DEFROW * sizeof(rowtype))) == NULL)
		return NULL;

	    for (i=0; i<DEFROW; i++)
		obj->row[i].rle = NULL;

	    /* Top row and bottom row of allocated row array */

	    obj->toprow = (rowidx > 5*DEFROW/8) ? rowidx - DEFROW/2 : 0;
	    obj->botrow = obj->toprow + DEFROW - 1;

	}

	/* Ensure requested row is in allocated row array (will be if row
	   array just allocated on this call with above) */

	if (rowidx < obj->toprow || rowidx > obj->botrow) {

	    /* rowidx outside array - must extend row array */

	    ntop = obj->toprow;	/* Expand toprow to include rowidx plus a few more */
	    if (rowidx < obj->toprow)
		ntop = (rowidx > 3*ATLROW/2) ? rowidx - ATLROW : 0;
				/* Expand botrow to include rowidx plus a few more */
	    nbot = (rowidx > obj->botrow) ? rowidx + ATLROW : obj->botrow;

	    /* Allocate new row structure, copying old across into new */

	    if ((nrow = ip_malloc((nbot - ntop + 1) * sizeof(rowtype))) == NULL)
		return NULL;

	    /* Got memory, initialise and  hook into object structure */

	    memcpy(nrow + (obj->toprow - ntop), obj->row,
				(obj->botrow-obj->toprow+1)*sizeof(rowtype));
	    for (i=ntop; i<obj->toprow; i++) /* No rle arrays allocated yet for new rows */
		nrow[i-ntop].rle = NULL;
	    for (i=obj->botrow+1; i<=nbot; i++)
		nrow[i-ntop].rle = NULL;
	    obj->toprow = ntop;
	    obj->botrow = nbot;
	    ip_free(obj->row);
	    obj->row = nrow;

	}

	/* Calculate row address */

	row = &obj->row[rowidx - obj->toprow];

	/* Check that there is a free entry in the rle array for the row */

	if (row->rle == NULL) {

	    /* No rle array - must allocate it */

	    if ((row->rle = ip_malloc(DEFRLE * sizeof(rletype))) == NULL)
		return NULL;

	    row->numentries = 0;
	    row->allocated = DEFRLE;

	}else{

	    /* rle array exists - is it big enough? if not - expand it! */

	    if (row->allocated-row->numentries < 2) {

		if (row->numentries > row->allocated)
		    ip_log_error(ERR_RLE_ARRAY_OVERFLOW,
			      "numentries %d, allocated %d",
			      row->numentries,row->allocated);

		/* Need to expand rle array */

		nsize = row->allocated + DEFRLE;
		if ((nrle = ip_realloc(row->rle, nsize*sizeof(rletype))) == NULL) {
		    return NULL;
		}else{
	       			/* Got expanded rle */
		    row->allocated = nsize;
		    row->rle = nrle;
		}

	    }
	}
    }

    ip_pop_routine();
    return row;
}




/*

NAME:

   ip_obj_insert_run() \- Insert a run into an object

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/object.h>

   int  ip_obj_insert_run( obj, rowidx, start, length )
   ip_object *obj;
   int rowidx;
   int start, length;

DESCRIPTION:

   This routine is only intended for use within the ip library.

   Insert the run ('start','length') into the rle array on row 'rowidx' of
   the object 'obj'. This routine calls ip_obj_get_row() before doing the
   insertion so it is safe to call at any time on a ''partial'' object.
   This routine should not be called on a ''completed'' object.

   It returns OK if the operation succeeded and the error code if an error
   is flagged.

ERRORS:

   ERR_OUT_OF_MEM
   ERR_OBJ_INVALID_ROW

*/

int ip_obj_insert_run(ip_object *obj, int rowidx, int start, int length)
{
    rowtype *row;
    rletype *rle;
    int i;

    /* Get row to operate on */

    if ((row = ip_obj_get_row(obj, rowidx))) {

	/* Find position to insert entry and insert it */

	rle = row->rle;
	for (i=0; i<row->numentries && start > (rle[i].start + rle[i].length); i++)
	    ;
	ip_obj_insert_run_entry(row,i,start,length);
    }

    return ip_error;
}



/*

NAME:

   ip_obj_insert_run_entry() \- Insert run into object

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/object.h>

   void  ip_obj_insert_run_entry( row, pos, start, length )
   rowtype *row;
   int pos;
   int start, length;

DESCRIPTION:

   This routine is only intended for use within the ip library.

   Inserts the run ('start','length') into the rle array of the specified
   'row' at position 'pos' in the array. All following rle entries are
   shifted along by one position in the array. It is assumed the rle array
   is large enough to accomadate this operation. This call normally follows
   a call to ip_obj_get_row(), which guarantees the rle array is
   sufficiently large to accomodate the entry.

   This routine now checks to see if the array is large enough for the
   insertion, if not it flags the ERR_RLE_ARRAY_OVERFLOW error. No insertion
   is performed and nothing is returned to indicate the failure. Thus an
   error handler is necessary to catch this error. If any ip routine causes
   this error, it would very strongly indicate a bug in the ip code.

ERRORS:

   ERR_RLE_ARRAY_OVERFLOW

*/

void ip_obj_insert_run_entry(rowtype *row, int pos, int start, int end)
{
    int i;

    if (row->allocated <= row->numentries) {

	ip_log_routine(__func__);
	ip_log_error(ERR_RLE_ARRAY_OVERFLOW);
	ip_pop_routine();

    }else{

	/* Make space in array for new entry */

	if (pos < row->numentries) {
	    for (i=row->numentries; i>pos; i--)
		row->rle[i] = row->rle[i-1];
	}

	/* Put in new entry */

	row->rle[pos].start = start;
	row->rle[pos].length = end;

	/* And update count of entries */

	row->numentries++;
    }
}



/*

NAME:

   ip_obj_get_run_end() \- Find end of run in object

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/object.h>

   int  ip_obj_get_run_end( obj, rowidx, xpt )
   ip_object *obj;
   int rowidx;
   int xpt;

DESCRIPTION:

   This routine is only intended for use within the ip library.

   Searches the object 'obj' to see if the coord ('xpt','rowidx') lies in
   the object. If so, it returns the end point of the run that
   ('xpt','rowidx') lies on else it returns -1.

ERRORS:

*/

int ip_obj_get_run_end(ip_object *obj, int rowidx, int xpt)
{
    int i;
    rowtype *row;
    rletype *rle;

    row = ip_obj_get_row(obj,rowidx);
    rle = row->rle;
    if (rle == NULL) {
	return -1;
    }
    for (i=0; i<row->numentries; i++) {
	if ( xpt >= rle[i].start && xpt < (rle[i].start + rle[i].length))
	    break;
    }
    if (i == row->numentries)
	return -1;
    else
	return (rle[i].start + rle[i].length - 1);
}



/*

NAME:

   ip_run_adjacent() \- Returns true if two runs adjacent

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/object.h>

   int  ip_run_adjacent( rle1, rle2 )
   rletype *rle1;
   rletype *rle2;

DESCRIPTION:

   This routine is only intended for use within the ip library.

   Returns TRUE if the two runs 'rle1' and 'rle2' are adjacent. Assumes rle
   encoding.

*/

int ip_run_adjacent(rletype *rle1, rletype *rle2)
{
    if (rle1->start - rle2->start - rle2->length > 0)
	return FALSE;
    if (rle2->start - rle1->start - rle1->length > 0)
	return FALSE;
    return TRUE;
}



/*

NAME:

   ip_run_overlap() \- Returns overlap of adjacent runs

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/object.h>

   int  ip_run_overlap( rle1, rle2 )
   rletype *rle1;
   rletype *rle2;

DESCRIPTION:

   This routine is only intended for use within the ip library.

   If the two runs 'rle1' and 'rle2' are adjacent, then this returns the
   amount by which they overlap. Assumes rle encoding.

*/

int ip_run_overlap(rletype *rle1, rletype *rle2)
{
    int end1,end2;

    end1 = rle1->start + rle1->length - 1;
    end2 = rle2->start + rle2->length - 1;
    if (rle1->start >= rle2->start && end1 <= end2)
	return rle1->length;
    if (rle2->start >= rle1->start && end2 <= end1)
	return rle2->length;
    if (rle2->start > rle1->start)
	return (end1 - rle2->start + 1);
    else
	return (end2 - rle1->start + 1);
}




/*

NAME:

   ip_obj_complete() \- Complete an object

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/object.h>

   int  ip_obj_complete( obj )
   ip_object *obj;

DESCRIPTION:

   This routine is only intended for use within the ip library.

   Accepts a ''partial'' object as its argument and ''completes'' it. The
   process of completing involves the following: New fixed size row and rle
   arrays are allocated and the object pixel data are put into the new
   arrays. Because the arrays are now fixed in size, no more modifications
   can be made to the object pixel data. The object data are converted to
   object coordinates and run-length encoding (if it wasn''t before). User
   program accessible fields 'numrows' and 'numpixels' are set up.

   With the object creation and rle insertion routines it is possible to
   create a ''partial'' object that is not eight-connected. If
   ip_obj_complete() detects this (don''t assume that it always will), it flags
   ERR_SPLIT_OBJECT. Completion of the object continues, but the completed
   object will only consist of the top eight-connected region.

   Ip_obj_complete() returns OK if the object completion succeeded and an error
   code otherwise. If memory allocation fails then the ''partial'' object
   remains untouched.

ERRORS:

   ERR_OUT_OF_MEM
   ERR_BAD_MEM_FREE
   ERR_SPLIT_OBJECT
   ERR_BADLY_FORMED_OBJECT

*/

int ip_obj_complete(ip_object *obj)
{
    int i,j,k;
    int numrows;
    int top,bot;
    int flagged;
    rowtype *srcrow,*destrow,*newrowlist;
    rletype *srcrle,*destrle,*newrlelist;

    ip_log_routine(__func__);

    numrows = obj->botrow - obj->toprow + 1;

    /* Add up how much room object really takes */

    flagged = FALSE;
    k = 0;
    for (i=0; i<numrows && obj->row[i].rle == NULL; i++)
	;				   /* Skip unused rows at top */
    top = i;
    for (   ; i<numrows && obj->row[i].rle != NULL; i++) {
	k += obj->row[i].numentries; /* Count up number of runs */
				     /* Check rle entries have valid nums */
	for (j=0; j<obj->row[i].numentries; j++) {
	    if ((obj->row[i].rle[j].start == UNSTARTED)
		|| (obj->row[i].rle[j].length == UNENDED)) {
		ip_log_error(ERR_BADLY_FORMED_OBJECT);
		ip_pop_routine();
		return ip_error;
	    }
	}
    }
    bot = i;
    for (   ; i<numrows; i++) {	   /* Check that no more rows used at bottom */
	if (obj->row[i].rle != NULL) {
	    if (NOT flagged) {	   /* Only need to flag the error once */
		ip_log_error(ERR_SPLIT_OBJECT);
		flagged = TRUE;
	    }
	    ip_free(obj->row[i].rle);	   /* Best we can do under exceptional circumstance! */
	}
    }

    /* Check that boundaries of object pixel data match that given in
       bounding box and origin. */

    if ((OBJ_OBJCOORD(obj) && (top != 0 || obj->toprow != 0
			     || (bot+obj->origin.y-1 != obj->bbox.br.y)))
	|| (OBJ_IMCOORD(obj) && (top+obj->toprow!=obj->origin.y
				|| bot+obj->toprow-1 != obj->bbox.br.y))) {
	ip_log_error(ERR_BADLY_FORMED_OBJECT);
	ip_pop_routine();
	return ip_error;
    }

    numrows = bot - top;

    /* Allocate new row and rle structures for object that
       are exactly the correct size */

    if ((newrlelist = ip_malloc(sizeof(rletype) * k)) == NULL) {
	return ip_error;
    }
    if ((newrowlist = ip_malloc(sizeof(rowtype) * numrows)) == NULL) {
	ip_free(newrlelist);
	return ip_error;
    }

    /* Shift old allocations to new allocations while freeing old
       memory. */

    obj->numpixels = k = 0;
    for (i=0; i<numrows; i++) {
	srcrow = OBJ_OBJCOORD(obj) ? &obj->row[i] : &obj->row[i+top];
	destrow = &newrowlist[i];
	destrow->numentries = srcrow->numentries;
	srcrle = srcrow->rle;
	destrle = destrow->rle = &newrlelist[k];
	for (j=0; j<srcrow->numentries; j++) {
	    destrle->start = OBJ_OBJCOORD(obj)
			     ? srcrle[j].start
			     : srcrle[j].start-obj->origin.x;
	    destrle->length = OBJ_RUNLEN(obj)
			     ? srcrle[j].length
			     : srcrle[j].length-srcrle[j].start+1;
	    obj->numpixels += destrle->length;
	    k++;
	    destrle++;
	}
	ip_free(srcrle);
    }
    ip_free(obj->row);


    /* Link new row/rle structure into object */

    obj->row = newrowlist;
    obj->toprow = 0;
    obj->numrows = numrows;
    SET_OBJCOORD(obj);
    SET_RUNLEN(obj);
    SET_COMPLETE(obj);

    ip_pop_routine();
    return ip_error;
}
