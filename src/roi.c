/*
   Module: ip/roi.c
   Author: M.J.Cree

   Code to allocate, modify, etc. ROI object.

   Copyright (C) 1996-1997, 1999-2000, 2007-2008 Michael J. Cree

ENTRY POINTS:

   ip_alloc_roi()      ip_free_roi()
   roi_set()        roi_modify()    roi_get()
   roi_in_image()

*/

#include <stdlib.h>
#include <stdarg.h>
#include <limits.h>

#include <ip/ip.h>

#include "internal.h"
#include "utils.h"


static ip_roi *roi_rect(ip_coord2d o, ip_coord2d s);
static void imrowset(ip_image *im, int row, int start, int end, double val);



/*

NAME:

   ip_alloc_roi() \- Allocate a region-of-interest object

PROTOTYPE:

   #include <ip/ip.h>

   ip_roi * ip_alloc_roi( type, ... )
   int type;

DESCRIPTION:

   Allocate a region-of-interest (ROI) object.  Possible type of ROI
   are (including the arguments to pass to ip_alloc_roi()):

\|   TYPE           ARGUMENTS       DESCRIPTION
\|
\|   ROI_IMAGE      ip_image *         ROI covering whole image.
\|   ROI_RECTANGLE  ip_box             Rectangular ROI.
\|   ROI_RECTOS     ip_coord2d, ip_coord2d    Rectangular ROI.

   The ROI_RECTANGLE and ROI_RECTOS are both the same type of ROI but
   one specifies the reactangle by an argument of type ip_box, and the
   other by two arguments, one giving the origin and the second the
   size.

   The ROI object is used by the analysis routines im_stats(),
   im_extrema() and im_hist() to specify a certain part of the image
   for the analysis to be performed on.

ERRORS:

   ERR_BAD_ROI_TYPE
   ERR_OUT_OF_MEM

SEE ALSO:

   ip_free_roi()
   roi_set()      roi_modify()    roi_get()
   im_extrema()   im_hist()       im_stats()

*/

ip_roi *ip_alloc_roi(int type, ...)
{
   va_list ap;
   ip_image *im;
   ip_coord2d origin,size;
   ip_box bbox;
   ip_roi *roi;

   va_start(ap,type);
   ip_log_routine("ip_alloc_roi");

   switch (type) {
    case ROI_IMAGE:
      im = va_arg(ap, ip_image *);
      origin.x = origin.y = 0;
      size = im->size;
      roi = roi_rect(origin,size);
      break;
    case ROI_RECTANGLE:
      bbox = va_arg(ap, ip_box);
      roi = roi_rect(bbox.origin, bbox.size);
      break;
    case ROI_RECTOS:
      origin = va_arg(ap, ip_coord2d);
      size   = va_arg(ap, ip_coord2d);
      roi = roi_rect(origin, size);
      break;
    default:
      ip_log_error(ERR_BAD_ROI_TYPE);
      roi = NULL;
      break;
   }

   ip_pop_routine();
   va_end(ap);
   return roi;
}



/*

NAME:

   ip_free_roi() \- Free up a region-of-interest object

PROTOTYPE:

   #include <ip/ip.h>

   void  ip_free_roi( roi )
   ip_roi *roi;

DESCRIPTION:

   Free previously allocated ROI.

SEE ALSO:

   ip_alloc_roi()
   roi_set()      roi_modify()    roi_get()

*/

void ip_free_roi(ip_roi *roi)
{
   if (roi)
      ip_free(roi);
}



/*

NAME:

   roi_set() \- Set an attribute of a region-of-interest object

PROTOTYPE:

   #include <ip/ip.h>

   int roi_set( action, roi, ... )
   int action;
   ip_roi *roi;

DESCRIPTION:

   Set some parameter of an ROI.  The 'action' should be one of:

\|   ACTIONS    ARGUMENTS DESCRIPTION
\|
\|   ROI_ORIGIN   ip_coord2d   Set origin (keeping shape of ROI).
\|   ROI_SIZE     ip_coord2d   Set size of rectangular ROI.
\|   ROI_BOTRIGHT ip_coord2d   Set bottom-right coordinate.

   The setting of the bottom-right coordinate of a rectangular ROI
   effectively sets the size since it keeps the origin the same.

ERRORS:

   ERR_BAD_ROI_ACTION

SEE ALSO:

   ip_alloc_roi()    ip_free_roi()
   roi_modify()    roi_get()

*/


int roi_set(int type, ip_roi *roi, ...)
{
   va_list ap;
   ip_coord2d botr;

   va_start(ap, roi);
   ip_log_routine("ROI_set");

   switch (type) {
    case ROI_ORIGIN:
      roi->rbox.origin = va_arg(ap,ip_coord2d);
      break;
    case ROI_SIZE:
      roi->rbox.size = va_arg(ap,ip_coord2d);
      break;
    case ROI_BOTRIGHT:
      botr = va_arg(ap,ip_coord2d);
      roi->rbox.size.x = botr.x - roi->rbox.origin.x + 1;
      roi->rbox.size.y = botr.y - roi->rbox.origin.y + 1;
      break;
    default:
      ip_log_error(ERR_BAD_ROI_ACTION);
      break;
   }

   va_end(ap);
   ip_pop_routine();
   return ip_error;
}



/*

NAME:

   roi_modify() \- Modify an attribute of a ROI object

PROTOTYPE:

   #include <ip/ip.h>

   int roi_modify( action, roi )
   int action;
   ip_roi *roi;

DESCRIPTION:

   Modify by an incremental amount some parameter of an ROI.  The
   'action' should be one of:

\|   ACTIONS    ARGUMENTS DESCRIPTION
\|
\|   ROI_ORIGIN   ip_coord2d  Add coord to origin (shifts ROI).
\|   ROI_SIZE     ip_coord2d  Add coord to size of rectangular ROI.
\|   ROI_BOTRIGHT ip_coord2d  Add coord to bottom-right coordinate.
\|   ROI_REDUCE   int    Reduce ROI by int pixels all around.
\|   ROI_INCREASE int    Add to ROI int pixels all around.

   For example, to allocate a ROI that is one pixel less than the
   size of an image all around, one could do:

\|   ip_roi *roi;
\|
\|   roi = ip_alloc_roi(ROI_IMAGE, im);
\|   roi_modify(ROI_REDUCE, roi, 1);

ERRORS:

   ERR_BAD_ROI_ACTION

SEE ALSO:

   ip_alloc_roi()    ip_free_roi()
   roi_set()      roi_get()

*/

int roi_modify(int type, ip_roi *roi, ...)
{
   va_list ap;
   int t;
   ip_coord2d ofs;

   va_start(ap, roi);
   ip_log_routine("ROI_modify");

   switch (type) {
    case ROI_ORIGIN:
      ofs = va_arg(ap,ip_coord2d);
      roi->rbox.origin.x += ofs.x;
      roi->rbox.origin.y += ofs.y;
      break;
    case ROI_SIZE:
    case ROI_BOTRIGHT:
      ofs = va_arg(ap,ip_coord2d);
      roi->rbox.size.x += ofs.x;
      roi->rbox.size.y += ofs.y;
      break;
    case ROI_REDUCE:
    case ROI_INCREASE:
      t = va_arg(ap,int);
      if (type == ROI_REDUCE) t = -t;
      ofs.x = ofs.y = -t;
      roi_modify(ROI_ORIGIN,roi,ofs);
      ofs.x = ofs.y = 2*t;
      roi_modify(ROI_SIZE,roi,ofs);
      break;
    default:
      ip_log_error(ERR_BAD_ROI_ACTION);
      break;
   }

   va_end(ap);
   ip_pop_routine();
   return ip_error;
}



/*

NAME:

   roi_get() \- Read an attribute of a ROI object

PROTOTYPE:

   #include <ip/ip.h>

   ip_datum  roi_get( type, roi )
   int type;
   ip_roi *roi;

DESCRIPTION:

   Return some parameter of a ROI.  The possible types of action
   (and the field in ip_datum set) are:

\|   ACTIONS     DATUM TYPE   DESCRIPTION
\|
\|   ROI_ORIGIN     ip_coord2d     Origin
\|   ROI_SIZE       ip_coord2d     Size of rectangular ROI.
\|   ROI_BOTRIGHT   ip_coord2d     Bottom-right coordinate.

ERRORS:

   ERR_BAD_ROI_ACTION

SEE ALSO:

   ip_alloc_roi()    ip_free_roi()
   roi_set()      roi_modify()

*/

ip_datum roi_get(int type, ip_roi *roi)
{
   ip_datum info;

   ip_log_routine("ROI_get");

   switch (type) {
    case ROI_ORIGIN:
      info.pt = roi->rbox.origin;
      break;
    case ROI_SIZE:
      info.pt = roi->rbox.size;
      break;
    case ROI_BOTRIGHT:
      info.pt.x = roi->rbox.origin.x + roi->rbox.size.x - 1;
      info.pt.y = roi->rbox.origin.y + roi->rbox.size.y - 1;
      break;
    default:
      ip_log_error(ERR_BAD_ROI_ACTION);
      info.pt.x = info.pt.y = 0;
      break;
   }

   ip_pop_routine();
   return info;
}



/*

NAME:

   roi_in_image() \- Check whether ROI is in image bounds

PROTOTYPE:

   #include <ip/ip.h>

   int  roi_in_image( im, roi )
   ip_image *im;
   ip_roi *roi;

DESCRIPTION:

   Check ROI is entirely in image bounds.  Generates error
   ERR_BAD_PARAMETER if not and pops calling routine off routine
   stack.

   Return value is OK if ROI is within image bounds.

ERRORS:

   ERR_BAD_PARAMETER

*/

int roi_in_image( ip_image *im, ip_roi *roi)
{
    return ip_box_in_image("ROI", roi->rbox, im);
}


/*

NAME:

   im_roiset() \- Set pixels outside ROI in image to a value

PROTOTYPE:

   #include <ip/ip.h>

   int  im_roiset( im, roi, val, flag )
   ip_image *im;
   ip_roi *roi;
   double val;
   int flag;

DESCRIPTION:

   Sets all pixels outside the region-of-interest 'roi' to the value of
   'val'.  Useful, for example, to set all pixels outside the ROI in the
   image to zero.  'Flag' currently not supported and should be set to
   NO_FLAG.

*/


int im_roiset(ip_image *im, ip_roi *roi, double val, int flag)
{
   int j;
   ip_box *rbox;

   ip_log_routine("im_roiset");

   if (NOT ip_value_in_image("value", val, im))
      return ip_error;

   if (NOT roi_in_image(im,roi))
      return ip_error;

   if (image_type_integer(im) || image_type_clrim(im))
      val = ROUND(val);
   if (im->type == IM_BINARY)
      val = (val == 0.0 ? 0.0 : 255.0);

   rbox = &roi->rbox;

   for (j=0; j<rbox->origin.y; j++)
      imrowset(im,j,0,im->size.x-1,val);
   for (; j<rbox->origin.y+rbox->size.y-1; j++) {
      imrowset(im,j, 0, rbox->origin.x-1, val);
      imrowset(im,j, rbox->origin.x+rbox->size.x, im->size.x-1, val);
   }
   for (; j<im->size.y; j++)
      imrowset(im,j,0,im->size.x-1,val);

   ip_pop_routine();
   return OK;
}

      


static ip_roi *roi_rect(ip_coord2d o, ip_coord2d s)
{
   ip_roi *roi;

   if ((roi = ip_mallocx(sizeof(ip_roi))) == NULL) {
      return NULL;
   }

   roi->type = ROI_RECTANGLE;
   roi->rbox.origin = o;
   roi->rbox.size = s;

   return roi;
}


#define PROW_SET(t)\
{\
   t *ip;\
   t sval = (t)val;\
   ip = im_rowadr(im,row);\
   ip += start;\
   for (i=start; i<=end; i++)\
      *ip++ = sval;\
}

static void imrowset(ip_image *im, int row, int start, int end, double val)
{
   int i;

   switch (im->type) {
    case IM_UBYTE: 
    case IM_BINARY: 
    case IM_PALETTE:
      PROW_SET(uint8_t);
      break;
    case IM_USHORT:
      PROW_SET(uint16_t);
      break;
    case IM_SHORT:
      PROW_SET(int16_t);
      break;
    case IM_LONG:
      PROW_SET(int32_t);
      break;
    case IM_FLOAT:
      PROW_SET(float);
      break;
    case IM_DOUBLE:
      PROW_SET(double);
      break;
    case IM_COMPLEX: {
      ip_complex *ip;
      ip_complex sval;
      sval.r = val;
      sval.i = val;
      ip = im_rowadr(im,row);
      ip += start;
      for (i=start; i<=end; i++)
         *ip++ = sval;
    }break;
    case IM_DCOMPLEX: {
      ip_dcomplex *ip;
      ip_dcomplex sval;
      sval.r = val;
      sval.i = val;
      ip = im_rowadr(im,row);
      ip += start;
      for (i=start; i<=end; i++)
         *ip++ = sval;
    }break;
    case IM_RGB: {
      ip_rgb *ip;
      ip_rgb sval;
      sval.r = sval.g = sval.b = (uint8_t)val;
      ip = im_rowadr(im,row);
      ip += start;
      for (i=start; i<=end; i++)
         *ip++ = sval;
    }break;
    case IM_RGB16: {
      ip_lrgb *ip;
      ip_lrgb sval;
      sval.r = sval.g = sval.b = (uint16_t)val;
      ip = im_rowadr(im,row);
      ip += start;
      for (i=start; i<=end; i++)
         *ip++ = sval;
    }break;
   default:
      ;
   }
}


