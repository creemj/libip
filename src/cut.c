/*
   Module: ip/cut.c
   Author: M. J. Cree

   Copyright (C) 1995-1997, 1999, 2002, 2007, 2008, 2014 Michael J. Cree 

   Routines to cut out regions of images and to insert an image into a
   region of another image.

ENTRY POINTS:

   im_cut()
   im_insert()

*/


#include <stdlib.h>
#include <string.h>

#include <ip/types.h>
#include <ip/image.h>
#include <ip/error.h>
#include <ip/proto.h>


/*

NAME:

   im_cut() \- Cut out a rectangular part of an image

PROTOTYPE:

   #include <ip/ip.h>

   ip_image * im_cut( im, origin, size )
   ip_image * im;
   ip_coord2d origin, size;

DESCRIPTION:

   Makes a new image which is cut from part of the existing image 'im'.  The
   rectangular region to cut is specified by parameters 'origin' and 'size'.
   The new image can be freed with ip_free_image() in the normal way.
   Returns NULL if image allocation fails.

ERRORS:

   ERR_OUT_OF_MEM
   ERR_BAD_PARAMETER

SEE ALSO:

   im_insert()    ip_free_image()

*/

ip_image *im_cut(ip_image *old, ip_coord2d origin, ip_coord2d size)
{
    ip_image *new;

    ip_log_routine("im_cut");

    if (NOT ip_box_in_image("origin&size", (ip_box){origin, size}, old))
	return NULL;

    if ((new = ip_alloc_image(old->type, size)) != NULL) {
	int skip = ip_image_type_info[old->type].typesize;

	for (int j=0; j<size.y; j++) {
	    uint8_t *sp = IM_ROWADR(old, j+origin.y, v);
	    memcpy(IM_ROWADR(new, j, v), sp+origin.x*skip, size.x*skip);
	}
	if (old->type == IM_PALETTE)
	    im_palette_copy(new,old);
    }
    ip_pop_routine();
    return new;
}



/*

NAME:

   im_insert() \- Insert part of an image into another image

PROTOTYPE:

   #include <ip/ip.h>

   int  im_insert( dest, dorigin, src, sorigin, ssize )
   ip_image *dest;
   ip_coord2d dorigin;
   ip_image *src;
   ip_coord2d sorigin, ssize;

DESCRIPTION:

   Inserts part of the image 'src' into the image 'dest'. The
   rectangular piece cut from 'src' is specified by the origin
   'sorigin' and has size 'ssize' and is put into 'dest' at the point
   'dorigin'.  The two images must be of the same data type.

   If 'sorigin' and 'ssize' are such that the cut piece is out of
   bounds of the image 'ssrc', then the error ERR_BAD_PARAMETER is
   flagged. The point 'dorigin' must be within 'dest''s' bounds. If
   the piece cut from 'src' is too large to fit into 'dest', then it
   is clipped (at the bottom-right) to fit 'dest'.

   Note that for IM_PALETTE images the palette is not copied.

ERRORS: 

   ERR_NOT_SAME_TYPE
   ERR_BAD_PARAMETER

SEE ALSO:

   im_cut()

*/

int im_insert(ip_image *dest, ip_coord2d dorg,
	      ip_image *src, ip_coord2d sorg, ip_coord2d size)
{
    int skip;

    ip_log_routine("im_insert");

    if (NOT images_same_type(dest,src))
	return ip_error;

    if (NOT ip_box_in_image("src_origin&size", (ip_box){sorg, size}, src))
	return ip_error;

    if (NOT ip_point_in_image("dest_origin", dorg, dest))
	return ip_error;

    /* Clip region to fit into destination image if necessary */
    if (dorg.x + size.x > dest->size.x)
	size.x = dest->size.x - dorg.x;
    if (dorg.y + size.y > dest->size.y)
	size.y = dest->size.y - dorg.y;

    skip = ip_image_type_info[src->type].typesize;

    /* Do insert row by row*/
    for (int j=0; j<size.y; j++) {
	uint8_t *sp = IM_ROWADR(src, j+sorg.y, v);
	uint8_t *dp = IM_ROWADR(dest, j+dorg.y, v);
	memcpy(dp + dorg.x*skip, sp + sorg.x*skip, size.x*skip);
    }

    ip_pop_routine();
    return OK;
}

