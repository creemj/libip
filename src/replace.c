/*
   Module: replace.c
   Author: M. J. Cree

   (C) 1996-2013 Michael J. Cree

   Routines to replace certain values in an image with another.

ENTRY POINT:

   im_replace()

*/

#include <stdlib.h>
#include <ip/types.h>
#include <ip/flags.h>
#include <ip/image.h>
#include <ip/error.h>
#include <ip/proto.h>



#define IMROW_REPLACE(t) {			\
	t *pix;					\
	t pnew, pold;				\
	pnew = (t)new;				\
	pold = (t)old;				\
	pix = im_rowadr(im,j);			\
	for (i=0; i<size.x; i++) {		\
	    if (*pix == pold)			\
		*pix = pnew;			\
	    pix++;				\
	}					\
    }


/*

NAME:

   im_replace() \- Replace a value in an image with another value

PROTOTYPE:

   #include <ip/ip.h>

   int  im_replace( im, new, old )
   image *im;
   double new;
   double old;

DESCRIPTION:

   Replace every pixel in the image 'im' that has the value 'old' with
   the value 'new'.  Handy for getting zeros out of an image before
   performing a division.

LIMITATIONS:

   Doesn''t work on COMPLEX or RGB images. Also refuses to work on
   BINARY images since that would leave them either as completely TRUE or
   completely FALSE, which is better achieved with im_set().

ERRORS:

   ERR_UNSUPPORTED_TYPE
   ERR_BAD_TYPE
   ERR_BAD_PARAMETER

SEE ALSO:

   im_div()    im_set()

*/

int im_replace(ip_image *im, double new, double old)
{
   int i,j;
   ip_coord2d size;

   ip_log_routine("im_replace");

   if (im->type == IM_COMPLEX || im->type == IM_RGB) {
      ip_log_error(ERR_UNSUPPORTED_TYPE);
      ip_pop_routine();
      return ip_error;
   }

   if (im->type == IM_BINARY) {
      ip_log_error(ERR_BAD_TYPE);
      ip_pop_routine();
      return ip_error;
   }

   if (NOT in_image_val(im, old, new)) {
      ip_log_error(ERR_BAD_PARAMETER);
      ip_pop_routine();
      return ip_error;
   }

   size = im->size;

   for (j=0; j<size.y; j++) {

      switch (im->type) {

       case IM_UBYTE:
       case IM_PALETTE:
	 IMROW_REPLACE(uint8_t)
	 break;

       case IM_BYTE:
	 IMROW_REPLACE(int8_t)
	 break;

       case IM_USHORT:
	 IMROW_REPLACE(uint16_t)
	 break;

       case IM_SHORT:
	 IMROW_REPLACE(int16_t)
	 break;

       case IM_ULONG:
	 IMROW_REPLACE(uint32_t)
	 break;

       case IM_LONG:
	 IMROW_REPLACE(int32_t)
	 break;

      case IM_FLOAT:
	 IMROW_REPLACE(float)
	 break;

       case IM_DOUBLE: {
	 double *pix;
	 pix = im_rowadr(im,j);
	 for (i=0; i<size.x; i++) {
	    if (*pix == old)
	       *pix = new;
	    pix++;
	 }}
         break;

      default:
	  break;
      }
   }

   ip_pop_routine();
   return OK;
}


