/* $Id$ */
/*
   Module: ip/display.c
   Author: M.J.Cree

   Display routines for displaying images, etc.
   Relies on the BD library for actual display of images.

   � 1995-2001 Michael Cree 

ENTRY POINTS:

   display_canvas() 
   display_clear()
   display_click()
   display_contrast()
   display_event()
   display_free()
   display_gamma()
   display_get()
   display_getpal()
   display_image()
   display_imagex()
   display_init()
   display_keypress()
   display_label()
   display_labelwidth()
   display_mouse()
   display_overlay()
   display_palentries()
   display_palette()
   display_pen()
   display_set()
   load_palette()

*/


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <ip/internal.h>
#include <ip/dispint.h>
#include <bd/bd.h>

#ifdef CAPTUREIMAGES
#include <ip/stats.h>
#include <ip/tiff.h>
#endif

/* #define DEBUG */
/* #define IN_DEBUG */
#include <debug/debug.h>


/* Define DISPDEBUG to get a debugging version of this module. */
/* If DISPDEBUG is defined then no connection to BitDisp is made and
   thus the source level debuggers can be run on code calling this
   module more successfully.  If course no images will actually be
   displayed but the calling code is none the wiser about this!
*/
#ifdef DISPDEBUG
#include <common/common.h>
#endif


static char rcsid[] = "$Id$";



/*** Internal list structure ***/

struct image_node_type {
   struct image_node_type *next;
   int id;
   int type;
   coord origin;
   coord size;
};

typedef struct image_node_type image_node;

static image_node *image_list=NULL; /* List of currently displayed IDs. */
static image_node *free_list=NULL;  /* List of currently free IDs.  */

#ifdef DISPDEBUG
static int next_id;
#endif


/* Buffer for storing mouse information */

static event_info disp_mouse;

/* Default Gamma correction factor is set to unity.  Use
   display_gamma() to change. Same with contrast, but
   use display_contrast() to change.
*/

double ip_disp_gamma = 1.0;	/* Current gamma correction value. */
double ip_disp_contrast = 1.0;	/* Current contrast value */

/* pen selection from palettes
   1st index: Palette. 
   2nd index: 0 = black, 1=white, 2=red, 3=green, 4=blue, 5=yellow
              6 = cyan,  7=magenta
   Return:   -1 = No close approximation in palette.
*/

static int pen_select[9][8] = {
   { 0 , 255,  -1,  -1,  -1,  -1,  -1,  -1 }, /* PAL_GREY */
   { 0 , 255, 140,  -1,  -1, 205,  -1,  -1 }, /* PAL_HOTBODY */
   { 0 , 127, 159, 191, 223, 255,  -1,  -1 }, /* PAL_GREYOBJ */
   { 0 ,  -1, 192, 100,  68,  -1,  90, 200 }, /* PAL_MRI */
   { 0 ,  -1, 220, 164, 130, 184,  54, 100 }, /* PAL_GEOGRAPHIC */
   { 0 , 255,  64,  -1,  -1, 220,  -1,  -1 }, /* PAL_HBEQUAL */
   { 0 ,  -1, 255, 132,  40,  -1,  -1,  -1 }, /* PAL_BGR */
   { 0 ,  -1, 255,  -1,  -1,  -1,  -1,  -1 }, /* PAL_OMR */
   { 0 ,  -1, 255, 112,   1,  -1,  -1,  -1 }  /* PAL_RGB */
};

int ip_disp_pal;		/* Currently selected palette */

/* Default display width, height... */

static int disp_width = 256;
static int disp_height = 256;

#define DISP_MAXSIZE_X 1024
#define DISP_MAXSIZE_Y 800

/* Translation table from ip image type to bitdisp image types. */

static int convtype[IM_TYPEMAX] = {
   BD_UBYTE,			/* IM_BYTE */
   BD_SSHORT,			/* IM_SHORT */
   BD_SLONG,			/* IM_LONG */
   BD_FLOAT,			/* IM_FLOAT */
   BD_FLOAT,			/* IM_DOUBLE - convert to FLOAT */
   0,				/* IM_COMPLEX - no support */
   BD_UBYTE,			/* IM_PALETTE */
   0,				/* IM_RGB - no support */
   BD_UBYTE			/* IM_BINARY */
};

/* Translation table from ip overlay colours to bitdisp overlays */

static int convovl[8] = {
   BD_BLACK,			/* OVL_BLACK   = 0 */
   BD_WHITE,			/* OVL_WHITE   = 1 */
   BD_RED,			/* OVL_RED     = 2 */
   BD_GREEN,			/* OVL_GREEN   = 3 */
   BD_BLUE,			/* OVL_BLUE    = 4 */
   BD_YELLOW,			/* OVL_YELLOW  = 5 */
   BD_CYAN,			/* OVL_CYAN    = 6 */
   BD_MAGENTA,			/* OVL_MAGENTA = 7 */
};

/* Translation table from ip palette selections to bitdisp palettes */

static int convpal[9] = {
   BD_GREY,			/* PAL_GREY */
   BD_HOT_BODY,			/* PAL_HOTBODY */
   0,				/* PAL_GREYOBJ */
   BD_MRI,			/* PAL_MRI */
   BD_GEOGRAPHIC,		/* PAL_GEOGRAPHIC */
   BD_HB_EQUAL,			/* PAL_HBEQUAL */
   BD_BGR,			/* PAL_BGR */
   BD_OMR,			/* PAL_OMR */
   BD_RGB			/* PAL_RGB */
};



static int connected = FALSE;
static int connect(void);

static byte *alloc_palette(int pal);
static int cadj(int pen);


static image_node *in_insert(image_node **list, int id);
static image_node *in_find(int id);
static int in_delete(image_node **list, int id);

#ifdef DISPDEBUG
static int ask_imageid(void);
static coord ask_imagepos(int id);
#endif

#ifdef IN_DEBUG
static void dump_in(int id);
#endif





/*

NAME:

   display_init() \- Initialise the display canvas

PROTOTYPE:

   #include <ip/ip.h>

   int  display_init( argc, argv, size, title, flag )
   int *argc;
   char **argv;
   coord size;
   char *title;
   int flag;

DESCRIPTION:

   Initialise the display canvas.  'Argc' and 'argv' can either be
   NULL (in which case they are ignored) or as passed into main() by
   the C startup code.  The X Toolkit command line options are
   recognised, parsed and stripped out of 'argc' and 'argv'.  Note
   that a pointer to 'argc' is passed, not 'argc' itself.

   The required size of the canvas is specified by 'size', which may
   be set to (0,0) if a default size is acceptable.  'Title' is a
   string which is displayed in the title bar of the display window.
   If it is NULL then a default and rather ineloquent message is
   displayed in the title bar.

   'Flag' can be one of:

\&   NO_FLAG      Default operation.

\&   FLG_DRAGABLE Images can be dragged around display canvas
      by user.

\&-   Normally this routine is called before calling any other display
   routine, though this is not absolutely necessary.  If this routine
   has not been called when some other display routine is called then
   a default initialisation is done as if display_init() was called
   with NULL for 'argc', 'argv', and 'title', and with 'size' set to
   (0,0).

   Example of using display:

\|   // Open the display 
\|
\|   size.x = size.y = 512;
\|   display_init(&argc, argv, size, 
\|                "Display Window", NO_FLAG);
\|
\|   // Display the image im
\|
\|   origin.x = origin.y = 0;
\|   id = display_image(FLG_NEWIMAGE, im, origin);
\|      ...

ERRORS:

   ERR_OPEN_DISPLAY_FAIL
   ERR_DISPLAY_INIT
   ERR_BAD_PARAMETER
   ERR_BAD_FLAG
   ERR_DISPLAY_CANVAS

SEE ALSO:

   display_set()       display_get()
   display_canvas()
   display_image()     display_imagex()    display_clear()
   display_overlay()
   display_palette()   load_palette()      display_palentries()
   display_getpal()
   display_label()     display_labelwidth()
   display_mouse()     display_click()     display_keypress()
   display_event()

*/

int display_init(int *argc, char **argv, coord size, char *title, int flag)
{
   static char buf1[10] = "-geometry";
   static char buf2[24] = "";
   char *def_argv[3] = { buf1, buf2, NULL };
   int def_argc = 2;

   log_routine("display_init");

   D(bug("display_init:  size=(%d,%d)  title=%s\n",size.x,size.y,title));
   
   if (connected) {
      log_error(ERR_DISPLAY_OPENED);
      pop_routine();
      return ip_error;
   }

   if (NOT flag_ok(flag, FLG_DRAGABLE, LASTFLAG))
      return ip_error;

#ifndef DISPDEBUG
   if (argc) {
      if (bd_Init(argc, argv) != BD_SUCCESS) {
	 return log_error(ERR_OPEN_DISPLAY_FAIL);
      }
   }else{
#if 0 
      /* This currently doesn't work ! */
      /* Couple more '#if 0's below too */
      if (size.x != 0 && size.y != 0) {
	 if (size.x <= 0 || size.y <= 0 
	     || size.x > DISP_MAXSIZE_X || size.y > DISP_MAXSIZE_Y) {
	    log_error(ERR_BAD_PARAMETER);
	    pop_routine();
	    return ip_error;
	 }
	 sprintf(buf2, "%dx%d", size.x, size.y);
      }
#endif
      strcat(buf2, "+20+20");
      D(bug("  buf2=%s\n",buf2));
      if (bd_Init(&def_argc, def_argv) != BD_SUCCESS) {
	 return log_error(ERR_OPEN_DISPLAY_FAIL);
      }
   }

   if (flag != FLG_DRAGABLE) {
      bd_DragImage(FALSE);
   }

#endif

   connected = TRUE;
   ip_disp_pal = 0;
   ip_disp_gamma = ip_disp_contrast = 1.0;

#if 0
   if (argc) {
#endif
      if (size.x != 0 && size.y != 0)
	 display_canvas(size, NO_FLAG);
#if 0
   }
#endif

#ifndef DISPDEBUG
   if (title) bd_CanvasTitle(title);
#endif

   D(bug("   ...display_init/OK\n"));

   pop_routine();
   return ip_error;
}





/*

NAME:

   display_set() \- Set/change display attribute

PROTOTYPE:

   #include <ip/ip.h>

   int display_set( action, ... )
   int action;

DESCRIPTION:

   Set a display attribute.  The following actions are possible:

\|   Action    Extra Args       Description
\| DSP_TITLE     char *   Set title in title bar.
\| DSP_SIZE      coord    Set canvas size.
\| DSP_DRAGABLE  boolean  Set whether images are dragable.

ERRORS:

   ERR_DISPLAY_NOT_OPEN
   ERR_BAD_ACTION
   ERR_BAD_PARAMETER
   ERR_BAD_FLAG
   ERR_DISPLAY_CANVAS

SEE ALSO:

   display_init()   display_canvas()   display_get()

*/

int display_set(int action, ...)
{
   va_list ap;
   char *title;
   coord size;
   int flag;

   va_start(ap,action);
   log_routine("display_set");

   if (NOT connected) {
      log_error(ERR_DISPLAY_NOT_OPEN);
      pop_routine();
      return ip_error;
   }

   switch (action) {
    case DSP_TITLE:

      title = va_arg(ap, char *);
#ifndef DISPDEBUG
      bd_CanvasTitle(title);
#endif

      break;

    case DSP_SIZE:

      size = va_arg(ap, coord);
      display_canvas(size, NO_FLAG);
      break;

    case DSP_DRAGABLE:

      flag = va_arg(ap, int);
      flag = flag ? TRUE : FALSE;
#ifndef DISPDEBUG
      bd_DragImage(flag);
#endif
      break;

    default:
 
      log_error(ERR_BAD_ACTION);
      break;
 
   }


   pop_routine();
   return ip_error;
}




/*

NAME:

   display_get() \- Get back some attribute of display canvas

PROTOTYPE:

   #include <ip/ip.h>

   datum  display_get( action )
   int action;

DESCRIPTION:

   Get back an attribute of the display canvas. Valid 'actions' are:

\|    Action     Return type     Description
\|  DSP_SIZE        coord      Current canvas size.
\|  DSP_MAXSIZE     coord      Maximum possible canvas size.
\|  DSP_PALENTRIES   int       Optimum number of palette pens.

ERRORS:

   ERR_BAD_ACTION

SEE ALSO:

   display_set()   display_palentries()
   display_init()

*/

datum display_get(int action)
{
   datum res;

   log_routine("display_get");
   
   switch(action) {
     case DSP_SIZE:
      res.pt.x = disp_width;
      res.pt.x = disp_height;
      break;

    case DSP_MAXSIZE:
      res.pt.x = DISP_MAXSIZE_X;
      res.pt.x = DISP_MAXSIZE_Y;
      break;

    case DSP_PALENTRIES:
      res.i = display_palentries();
      break;

    default:
      log_error(ERR_BAD_ACTION);
      res.d = 0;
      break;
   }
 
   pop_routine();
   return res;
}



/*

NAME:

   display_canvas() \- Set display canvas size

PROTOTYPE:

   #include <ip/ip.h>

   int  display_canvas( size, flag )
   coord size;
   int flag;

DESCRIPTION:

   Set display canvas size.  Flag must currently be set to NO_FLAG.
   This routine can be re-called to change canvas size.

ERRORS:

   ERR_BAD_PARAMETER
   ERR_BAD_FLAG
   ERR_OPEN_DISPLAY_FAIL
   ERR_DISPLAY_CANVAS

SEE ALSO:

   display_init()

*/

int display_canvas(coord size, int flag)
{
   log_routine("display_canvas");

   D(bug("display_canvas: size=(%d,%d), flag=%d.\n",size.x,size.y,flag));

   if (size.x <= 0 || size.y <= 0 
       || size.x > DISP_MAXSIZE_X || size.y > DISP_MAXSIZE_Y) {
      log_error(ERR_BAD_PARAMETER);
      pop_routine();
      return ip_error;
   }

   if (flag != NO_FLAG) {
      log_error(ERR_BAD_FLAG);
      pop_routine();
      return ip_error;
   }

   disp_height = size.y;
   disp_width = size.x;

   if (connect() != OK) {
      pop_routine();
      return ip_error;
   }

#ifndef DISPDEBUG 
   if (bd_WindowSize(disp_width, disp_height) != BD_SUCCESS) {
      log_error(ERR_DISPLAY_CANVAS);
   }
#endif

   D(bug("  ...display_canvas/OK\n"));

   pop_routine();
   return ip_error;
}




/*

NAME:

   display_image() \- Display an image on display canvas

PROTOTYPE:

   #include <ip/ip.h>

   int  display_image( id, im, origin )
   int id;
   image *im;
   coord origin;

DESCRIPTION:

   Display the image 'im' at the point 'origin' in the display window.
   Only BINARY, BYTE, SHORT, LONG, FLOAT and DOUBLE images properly
   supported.  An attempt is made at displaying PALETTE images.
   If the palette of the PALETTE image has display_palentries()
   or less entries then it is displayed correctly.  If it has more
   entries it is liable to appear with incorrect colours.  Note that
   displaying a PALETTE image causes a call load_palette() and the
   palette of the whole display is changed.

   'Id' can be the ID of a previously displayed image or FLG_NEWIMAGE
   if a new image is being displayed.  If the image display is
   successful then an image ID is returned which should be used
   whenever redisplaying this image or adding overlays to it.  All
   image IDs are positive integers greater than zero.  A negative
   number on return indicates that the image displayed failed.

   Note that if you change either the size or the type of an image
   when re-displaying it with an old image ID, then the image ID can
   change.  If only image pixel data are modified, then it is
   guaranteed that the ID will remain the same.

   It is assumed that the image data (when displaying non-colour
   images) are scaled as 0-255, which when the PAL_GREY colour palette
   is selected, are displayed as a grey-scale with 0 as black and 255
   as white.  Any pixels outside that range (for SHORT and FLOAT
   images, for example) are displayed either as 255 (white) or as 0
   (black).

   This routine is implemented as a macro to call display_imagex()
   with the 'min' value of 0, and the 'max' value of 255.

LIMITATIONS:

   DOUBLE images are internally converted to FLOAT to display them.

   You have to re-display the complete image to change any part of it
   (alternatively use display_overlay() - however that technically
   only adds an overlay, it doesn''t change the underlying image.)

   There is a maximum number of images you can have displayed at once,
   which depends on the configuration of the workstation you run this
   on.

ERRORS:

   ERR_BAD_FLAG
   ERR_NOT_IMPLEMENTED  - COMPLEX and RGB types.
   ERR_OUT_OF_MEM
   ERR_OPEN_DISPLAY_FAIL
   ERR_DISPLAY_IMAGE

SEE ALSO:

   display_canvas()    display_imagex()    display_clear()
   display_overlay()
   load_palette()      display_palentries()

*/

int display_image(int id, image *im, coord origin)
{
   return display_imagex(id, im, origin, 0, 255);
}




/*

NAME:

   display_imagex() \- Display an image on display canvas

PROTOTYPE:

   #include <ip/ip.h>

   int  display_imagex( id, im, origin, min, max )
   int id;
   image *im;
   coord origin;
   double min, max;

DESCRIPTION:

   Display the image 'im' at the point 'origin' in the display window.
   Only BINARY, BYTE, SHORT, LONG, FLOAT and DOUBLE images properly
   supported.  An attempt is made at displaying PALETTE images.  If
   the palette of the PALETTE image has display_palentries() or less
   entries then it is displayed correctly.  If it has more entries it
   is liable to appear with incorrect colours.  Note that displaying a
   PALETTE image causes a call to load_palette() and the palette of
   the whole display is changed.

   'Id' can be the ID of a previously displayed image or FLG_NEWIMAGE
   if a new image is being displayed.  If the image display is
   successful then an image ID is returned which should be used
   whenever redisplaying this image or adding overlays to it.  All
   image IDs are positive integers greater than zero.  A negative
   number on return indicates that the image displayed failed.

   Note that if you change either the size or the type of an image
   when re-displaying it with an old image ID, then the image ID can
   change.  If only image pixel data is modified, then it is
   guaranteed that the ID will remain the same.

   'Min' and 'max' specify the image scaling range.  For non-colour
   images any pixel having value 'min' or less is displayed in palette
   colour 0 (normally black).  Any pixel having value 'max' or more is
   is displayed in palette colour 255 (normally white).  All pixels
   lying between 'min' and 'max' are linearly scaled to be displayed
   with the appropriate palette colour between 0 and 255.  The
   exception to this is when both 'min' and 'max' are set to zero,
   then the minimum and maximum of the image are found and these are
   then used for 'min' and 'max'.  For PALETTE images 'min' and 'max'
   are ignored.

LIMITATIONS:

   DOUBLE images are internally converted to FLOAT to display them.

   You have to re-display the complete image to change any part of it
   (alternatively use display_overlay() - however that technically
   only adds an overlay, it doesn''t change the underlying image.)

   There is a maximum number of images you can have displayed at once,
   which depends on the configuration of the workstation you run this
   on.

BUGS:

   Multiple displies of an image with 'min' and 'max' both set to zero
   use the scaling of the image calculated on the first time it was
   displayed with 'min' and 'max' set to zero, unless the image size
   or type changes, in which case the scaling is recalculated.  The
   intended behaviour is that the scaling should be recalculated with
   any redisplay with 'min' and 'max' set to zero.  Unfortunately this
   doesn''t happen at present.

ERRORS:

   ERR_BAD_FLAG
   ERR_NOT_IMPLEMENTED  - For COMPLEX and RGB images.
   ERR_OUT_OF_MEM
   ERR_OPEN_DISPLAY_FAIL
   ERR_DISPLAY_IMAGE

SEE ALSO:

   display_init()
   display_canvas()    display_image()    display_clear()
   display_overlay()
   load_palette()      display_palentries()

*/



int display_imagex(int oldid, image *im, coord origin,
		   double minval, double maxval)
{
   int free_im;
   int id;
   image_node *imnode, *oldnode;
#ifdef CAPTUREIMAGES
   static int imagecnt = 0;
#endif

   log_routine("display_image");

   D(bug("display_image: id=%d  [%s size=(%d,%d)] origin=(%d,%d)\n",
	 oldid, ipmsg_type[im->type], im->size.x, im->size.y,
	 origin.x, origin.y));

   /* Can't display RGB or COMPLEX images at present. */

   if (im->type == IM_COMPLEX || im->type == IM_RGB) {
      log_error(ERR_NOT_IMPLEMENTED);
      pop_routine();
      return -1;
   }

   if (NOT ip_parm_greatereq("image_id", oldid, 0)) {
      return -1;
   }

   if (oldid > 0 && ((imnode = in_find(oldid)) == NULL)) {
      log_error(ERR_DISPLAY_BAD_ID);
      pop_routine();
      return -1;
   }

   oldnode = NULL;
   if (oldid == 0 && free_list) {
      oldnode = free_list;
      oldid = oldnode->id;
      D(bug("   using old window ID %d.\n",oldid));
   }

   /* Open display if not opened yet. */

   if (connect() != OK) {
      pop_routine();
      return -1;
   }

#ifdef CAPTUREIMAGES
   /* This is a hook to save all displayed images if need be. 
      Don't normally compile this in. */
   {
      image *disp, *tmp;
      char fname[256];
      stats *st;
      double imin, imax;

      if (minval == 0 && maxval == 0) {
	 st = alloc_stats(NO_FLAG);
	 im_extrema(im,st,NULL);
	 imin = st->min;
	 imax = st->max;
	 free_stats(st);
      }else{
	 imin = minval;
	 imax = maxval;
      }

      if (im->type == IM_BYTE) {
	 disp = im_copy(im);
	 if (imin != 0 || imax != 255) {
	    im_norm(disp,imin,imax,0,255,FLG_CLIP);
	 }
      }else if (type_real(im->type) || type_integer(im->type)) {
	 tmp = im_copy(im);
	 im_norm(tmp,imin,imax,0,255,FLG_CLIP);
	 disp = im_convert(tmp, IM_BYTE, FLG_CLIP);
	 free_image(tmp);
      }else
	 disp = im;

      sprintf(fname, "capture%03d.tif", ++imagecnt);
      write_tiff_image(fname, disp);

      if (type_real(im->type) || type_integer(im->type)) {
	 free_image(disp);
      }
   }
#endif

   /* Convert DOUBLE images to FLOAT for displaying */

   free_im = FALSE;
   if (im->type == IM_DOUBLE) {
      if ((im = im_convert(im, IM_FLOAT, NO_FLAG)) == NULL) {
	 pop_routine();
	 return -1;
      }
      free_im = TRUE;
   }

   /* Process PALETTE image palette. */

   if (im->type == IM_PALETTE && BD_MAXPALETTE < 256) {
      UBYTE *pix;
      int i,j;
      int min,max;
      int numcols;

      /* Quickly find image min/max */

      min = 255;
      max = 0;
      for (j=0; j<im->size.y; j++) {
	 pix = IM_ROWADR(im, j, b);
	 for (i=0; i<im->size.x; i++) {
	    if (*pix > max) max = *pix;
	    if (*pix < min) min = *pix;
	    pix++;
	 }
      }
      numcols = max - min + 1;	/* Number of colours in image */
      if (numcols > BD_MAXPALETTE) {
	 /* Too many colours - this will be a mess :-( */
	 load_palette(256,im->palette);
	 minval = 0;
	 maxval = 255;
      }else{
	 /* Not too many colours - try to design and use optimum palette */
	 if (min > (256-BD_MAXPALETTE))
	    min = 256-BD_MAXPALETTE;
	 load_palette(PAL_OPTIMUM, im->palette + min);
	 minval = min;
	 maxval = min+BD_MAXPALETTE-1;
      }
   }

   /* Display image */

#ifdef DISPDEBUG
   if (oldid == FLG_NEWIMAGE ||
       (oldnode && (oldnode->type != im->type ||
		    oldnode->size.x != im->size.x ||
		    oldnode->size.y != im->size.y)) ||
       (imnode->type != im->type ||
	imnode->size.x != im->size.x ||
	imnode->size.y != im->size.y) ) {
      id = ++next_id;
   }else{
      id = oldid;
   }
#else

   D(bug("   bd_DisplayImage..."));
   if ((id = bd_DisplayImage(im->image.v, convtype[im->type],
			     minval, maxval, im->size.x, im->size.y,
			     origin.x, origin.y, oldid)) < BD_SUCCESS) {
      log_error(ERR_DISPLAY_IMAGE);
      goto dix_exit;
   }
   D(bug("  id %d,   dataptr %p.\n", id, im->image.v));

#endif

   /* Update list of image node IDs */

   if (oldnode) {
      /* We were using a previously freed up ID */
      if ((imnode = in_insert(&image_list, id)) == NULL)
	 return -1;
      in_delete(&free_list, oldid);
     D(bug("  Moved old ID %d from free list to image list ID %d\n",oldid,id));
   }else{
      if (oldid > 0 && oldid != id) {
	 /* ID has changed */
	 /* This may occur if image has changed size or type */
	 in_delete(&image_list, oldid);
	 if ((imnode = in_insert(&image_list, id)) == NULL) {
	    return -1;
	 }
	 D(bug("  exchanged old ID for new window ID\n"));
      }else if (oldid == 0) {
	 /* This is a completely new ID */
	 if ((imnode = in_insert(&image_list, id)) == NULL) {
	    return -1;
	 }
	 D(bug("  inserted new window ID\n"));
      }
      /* Else oldid and id are identical and have been previously used. */
   }

   imnode->type = im->type;
   imnode->origin = origin;
   imnode->size = im->size;


   D(bug("   ...display_image/OK\n"));

 dix_exit:

   if (free_im) {
      free_image(im);
   }

   pop_routine();
   return id;
}




/*

NAME:

   display_overlay() \- Display an overlay on top of an image

PROTOTYPE:

   #include <ip/ip.h>

   int  display_overlay( id, overlay, pos, flag )
   int id;
   image *overlay;
   coord pos;
   int flag;

DESCRIPTION:

   Place an overlay over a previously displayed image identified by
   the image ID 'id'.  The 'overlay' image can be a BYTE image (in which
   case 'flag' should be set to NO_FLAG) or a BINARY image (in which
   case 'flag' should be set to one of OVL_BLACK, OVL_RED, etc.)

   Partial updating of overlays is supported.  The overlay size need
   not be exactly the same as the original image displayed, however it
   must not be larger.  'Pos' specifies where 'overlay' should be
   placed in the image identified by 'id'.

   An example of use is:

\|   // Display an image pointed to by im.
\|   id = display_image(FLG_NEWIMAGE, im, (coord){0,0});
\|
\|   // Add the overlay (which is smaller than the image im)
\|   //   at the position pos in the image im.  The overlay
\|   //   is a BYTE image.
\|   display_overlay(id, overlay, pos, NO_FLAG);
\|      ...
\|
\|   // Clear the overlay from the image.
\|   display_clear(id, FLG_OVERLAY);

LIMITATIONS:

   The overlaying of BINARY images is not yet implemented.   Only
   overlaying BYTE images is supported.

ERRORS:

   ERR_DISPLAY_BAD_ID
   ERR_DISPLAY_BAD_TYPE
   ERR_DISPLAY_BAD_UPDATE
   ERR_DISPLAY_UPDATE

SEE ALSO:

   display_init()
   display_image()   display_clear()

*/

int display_overlay(int id, image *im, coord pos, int flag)
{
   image_node *imnode;

   log_routine("display_overlay");

   D(bug("display_overlay: id=%d  [%s size=(%d,%d)]  pos=(%d,%d)\n",
	 id, ipmsg_type[im->type], im->size.x, im->size.y, pos.x, pos.y));

   if ((imnode = in_find(id)) == NULL) {
      log_error(ERR_DISPLAY_BAD_ID);
      pop_routine();
      return ip_error;
   }

   if (im->type != IM_BYTE) {
      log_error(ERR_DISPLAY_BAD_TYPE);
      pop_routine();
      return ip_error;
   }

   if (pos.x < 0 || pos.y < 0 || 
       pos.x + im->size.x > imnode->size.x ||
       pos.y + im->size.y > imnode->size.y) {
      log_error(ERR_DISPLAY_BAD_UPDATE);
      pop_routine();
      return ip_error;
   }

#ifndef DISPDEBUG

   if (bd_OverlaySubimage(im->image.v, BD_UBYTE, im->size.x, im->size.y, 
			  pos.x, pos.y, id) < BD_SUCCESS) {
      log_error(ERR_DISPLAY_UPDATE);
   }

#endif

   D(bug("   ...display_overlay/OK\n"));

   pop_routine();
   return ip_error;
}



/*

NAME:

   display_free() \- Free up a previously displayed image and its ID

PROTOTYPE:

   #include <ip/ip.h>

   int  display_free( id )
   int id;

DESCRIPTION:

   Free a previously displayed image and its image ID and remove it
   from the display canvas.

LIMITATIONS:

   Doesn't work properely yet.  Just clears the image but it still
   hangs around in the display canvas at present.  However, if
   subsequently you do do another image display requesting a new image
   ID, then the freed up image ID can be reused and at this point the
   old image will disappear completely.

ERRORS:

   ERR_DISPLAY_BAD_ID
   ERR_DISPLAY_FREE

SEE ALSO:

   display_init()
   display_image()   display_imagex()

*/

int display_free(int id)
{
   image_node *imnode;
   image *dummy;
   int newid;

   log_routine("display_free");

   D(bug("display_free:: id=%d\n",id));

   if ((imnode = in_find(id)) == NULL) {
      log_error(ERR_DISPLAY_BAD_ID);
      pop_routine();
      return ip_error;
   }

#ifndef DISPDEBUG

   dummy = alloc_image(IM_BYTE,imnode->size.x,imnode->size.y);
   im_clear(dummy);

#endif
   
   D(bug("  shifting to free list.\n"));

   in_delete(&image_list, id);
   if (in_insert(&free_list, id) == NULL)
      return ip_error;

#ifndef DISPDEBUG

   D(bug("  displaying dummy small image.\n"));

   if ((newid = bd_DisplayImage(dummy->image.v, BD_UBYTE, 0, 255,
				dummy->size.x, dummy->size.y,
				0, 0, id)) < BD_SUCCESS) {
      log_error(ERR_DISPLAY_FREE);
   }

   free_image(dummy);
   
#endif

   D(bug("   .../display_free/OK\n"));

   pop_routine();
   return ip_error;
}




/*

NAME:

   display_clear() \- Clear display canvas or part thereof

PROTOTYPE:

   #include <ip/ip.h>

   int  display_clear( id, flag, ... )
   int id;
   int flag;

DESCRIPTION:

   Clears images in the display canvas.  Presently only the following
   two operations supported:

\|   display_clear( id, FLG_OVERLAY );

   which clears the overlays from the image, and

\|   display_clear( id, FLG_IMAGE );

   which clears the image data itself and blanks the image, but the
   image remains on the display canvas, and the image 'id' is still
   valid.

ERRORS:

   ERR_DISPLAY_BAD_ID
   ERR_OPEN_DISPLAY_FAIL
   ERR_DISPLAY_CLEAR

SEE ALSO:

   display_init()
   display_image()    display_imagex()    display_overlay()

*/

int display_clear(int id, int flag, ...)
{
   va_list ap;
   image *im;
   image_node *imnode, *next;
   int error;

   va_start(ap,flag);

   log_routine("display_clear");

   D(bug("display_clear: id=%d   flag=%d\n",id,flag));

   /* Open display if not opened yet. */

   if (connect() != OK) {
      goto exit_dc;
   }

   switch (flag) {
    case FLG_CANVAS:
      /* Clear whole display */

      error = BD_SUCCESS;

#ifndef DISPDEBUG
      for (imnode=image_list; error<BD_SUCCESS && imnode; imnode=next) {
	 next = imnode;
	 
	 im = alloc_image(imnode->type, imnode->size.x, imnode->size.y);
	 im_clear(im);

	 if ((error = bd_DisplayImage(im->image.v, convtype[imnode->type],
				      0, 255, im->size.x,im->size.y,
				      0, 0, imnode->id)) < BD_SUCCESS) {
	    log_error(ERR_DISPLAY_CLEAR);
	 }
      
	 free_image(im);
 
	 if (error < BD_SUCCESS) {
	    /*	 bd_dispose(imnode->id); */
	    /*	 in_delete(imnode->id); */
	 }
      }
#endif
      break;

    case FLG_IMAGE:
      
      if ((imnode = in_find(id)) == NULL) {
	 log_error(ERR_DISPLAY_BAD_ID);
	 goto exit_dc;
      }

#ifndef DISPDEBUG
      if ((im = alloc_image(imnode->type, 
			    imnode->size.x, imnode->size.y)) == NULL) {
	 goto exit_dc;
      }
      im_clear(im);

      if ((error = bd_DisplayImage(im->image.v, convtype[imnode->type],
				   0, 255, im->size.x,im->size.y,
				   0, 0, id)) < BD_SUCCESS) {
	 log_error(ERR_DISPLAY_CLEAR);
      }
      free_image(im);
#else
      error = BD_SUCCESS;
#endif
      break;

    case FLG_OVERLAY:
      
      if ((imnode = in_find(id)) == NULL) {
	 log_error(ERR_DISPLAY_BAD_ID);
	 goto exit_dc;
      }

#ifndef DISPDEBUG
      if (bd_ClearOverlays(id, id) != BD_SUCCESS) {
	 log_error(ERR_DISPLAY_CLEAR);
      }
#endif	 

      break;

    default:
      log_error(ERR_BAD_FLAG);
      break;

   }

#ifdef E_SNAPSHOT
   if (error >= BD_SUCCESS) 
      clear_view();
#endif

   D(bug("   ...display_clear/OK\n"));

 exit_dc:

   pop_routine();
   va_end(ap);
   return ip_error;
}



/*

NAME:

   display_label() \- Display a label (string) over image

PROTOTYPE:

   #include <ip/ip.h>

   int display_label( id, str, pen, size, pt )
   int id;
   char *str;
   int pen;
   int size;
   coord pt;

DESCRIPTION:

   Print label 'str' to the display overlayed the image identified by
   'id', in the specified overlay colour 'pen' and of point size
   'size', at the point 'pt' in the image.

   If 'size' is set to one, then a default point size of 12 points is
   selected.  This is to ensure backwards compatibility with old code.

   'Pen' must be one of:

\|  OVL_BLACK
\|  OVL_WHITE
\|  OVL_RED
\|  OVL_GREEN
\|  OVL_BLUE
\|  OVL_YELLOW
\|  OVL_CYAN
\|  OVL_MAGENTA

ERRORS:

   ERR_OOR_PARAMETER
   ERR_BAD_DISPLAY_ID
   ERR_DISPLAY_LABEL

*/

int display_label(int id, char *s, int colour, int size, coord pt)
{
   image_node *imnode;

   log_routine("display_label");

   D(bug("display_label: id=%d  col=%d  size=%d  pt=(%d,%d)\n",
	 id,colour,size, pt.x, pt.y));
   D(bug("   str=%s\n",s));

   if (NOT ip_parm_inrange("colour_pen", colour, OVL_START, OVL_END))
      return ip_error;

   /* Check that image ID is OK */

   if ((imnode = in_find(id)) == NULL) {
      log_error(ERR_DISPLAY_BAD_ID);
      goto exit_dl;
   }      

   if (size == 1) size = 12;

#ifndef DISPDEBUG

   /* Display label*/

   if (bd_OverlayText(s, pt.x, pt.y, "screen", size, convovl[colour], id) 
       != BD_SUCCESS) {
      log_error(ERR_DISPLAY_LABEL);
   }

#endif

 exit_dl:

   D(bug("   ...display_label/OK\n"));

   pop_routine();
   return ip_error;
}
   


/*

NAME:

   display_labelwidth() \- Calculate pixel width of label (string)

PROTOTYPE:

   #include <ip/ip.h>

   int  display_labelwidth( str, ptsize )
   char *str;
   int ptsize;

DESCRIPTION:

   Return the width of the text 'str' in pixels when it is to be
   displayed at point size 'ptsize'.

ERRORS:

   ERR_DISPLAY_BAD_ID

*/

int display_labelwidth(char *s, int size)
{
   int width;

   log_routine("display_labelwidth");

   D(bug("display_labelwidth:  size=%d  str=%s\n",size,s));

   /* Open display if not opened yet. */

   if (connect() != OK) {
      pop_routine();
      return -1;
   }

#ifdef DISPDEBUG
   width = (7*size*strlen(s)) / 10;
#else

   width =  bd_TextWidth(s, "screen", size, 255);

   if (width == 0 && *s) {
      fprintf(stderr, "WARNING: BitDisp returned label length of zero.\n");
      width = (7*size*strlen(s)) / 10;
   }

#endif

   D(bug("   ...display_labelwidth/OK  (width=%d)\n",width));

   pop_routine();
   return width;
}


/*

NAME:

   display_palette() \- Set display palette to internal palette

PROTOTYPE:

   #include <ip/ip.h>

   int  display_palette( pal )
   int pal;

DESCRIPTION:

   Select a predefined palette for the display canvas. Options are:

\&   PAL_GREY     Grey scale
\&   PAL_HOTBODY  Hot-body scale
\&   PAL_MRI
\&   PAL_GEOGRAPHIC
\&   PAL_HBEQUAL
\&   PAL_BGR
\&   PAL_OMR
\&   PAL_RGB
\&   PAL_GREYOBJ  Lower 128 colours grey scale. Upper 128 used for object
                  display (have red, green, blue and yellow scales).

\&-   OR one of the PAL_* flags with PAL_GAMCORR if you want gamma correction
   (and contrast correction) applied to all displayed images.

   Note that the palette applies to the whole display canvas.  It
   cannot be selected for just one image.

ERRORS:

   ERR_UNKNOWN_PALETTE
   ERR_OPEN_DISPLAY_FAIL
   ERR_OUT_OF_MEM
   ERR_PALETTE

BUGS:

   Gamma correction is only applied to PAL_GREY, PAL_HOTBODY or
   PAL_GREYOBJ palette selections when requested.

SEE ALSO:

   display_init()
   load_palette()    display_gamma()

*/

int display_palette(int pal)
{
   byte *colourpal;
   int gamma_correct;
   int bdpal;
   int i;

   log_routine("display_palette");

   D(bug("display_palette: pal 0x%x\n",pal));

   /* Open display if not opened yet. */

   if (connect() != OK) {
      goto exit_sp;
   }

   gamma_correct = (pal & PAL_GAMCORR);

   pal &= ~PAL_GAMCORR;

   if (pal < PAL_START || pal > PAL_END) {
      log_error(ERR_UNKNOWN_PALETTE);
      goto exit_sp;
   }

#ifdef DISPDEBUG
   ip_disp_pal = pal;
#else

   if (pal == PAL_GREYOBJ || (gamma_correct && pal < 2)) {

      /* Design our own palette */

      bdpal = BD_CUSTOM;

      if ((colourpal = alloc_palette(pal)) == NULL)
	 goto exit_sp;

      if (gamma_correct && ip_disp_gamma != 1.0) {
	 for (i=0; i<256*3; i++) {
	    colourpal[i] = ROUND(255.0*pow(colourpal[i]/255.0, ip_disp_gamma));
	 }
      }

      D(bug("  designed own palette.\n"));

   }else{

      /* Use predefined palette */

      bdpal = convpal[pal];
      colourpal = NULL;

      D(bug("  selected palette number %d\n",bdpal));

   }

   if (bd_Palette(bdpal, colourpal) != BD_SUCCESS) {
      log_error(ERR_PALETTE);
   }else{
      ip_disp_pal = pal;
   }
   
   if (colourpal)
      ip_free(colourpal);

#endif

   D(bug("   ...display_palette/OK\n"));

 exit_sp:

   pop_routine();
   return ip_error;
}



/*

NAME:

   load_palette() \- Load a custom palette

PROTOTYPE:

   #include <ip/ip.h>

   int  load_palette( numentries, pal )
   int numentries;
   colour *pal;

DESCRIPTION:

   Load the palette with a custom palette. 'Pal' is an array of rgb
   colour triplets with 'numentries' entries. At present, 'numentries'
   should be 256 (except for the case described below).

   If 'numentries' is set to PAL_OPTIMUM, then 'pal' should be a
   palette with exactly display_palentries() entries.  It is
   guaranteed that no interpolation will be performed on the palette
   (since it fits exactly the available display palette size) and that
   if a BYTE image with pixel values in the range 0 to
   display_palentries()-1 (inclusive) is displayed via
   display_imagex() with 'min' set to 0 and 'max' set to
   display_palentries() (see display_imagex()) then pixel value 0 will
   map exactly to palette entry 0, pixel value 1 to palette value 1,
   etc.

   If display_gamma() has been called to modify the gamma correction
   factor then load_palette() will automatically modify the custom
   palette according to the gamma correction required. There is no way
   to disable this feature except by setting the gamma correction back
   to 1.0.

ERRORS:

   ERR_OPEN_DISPLAY_FAIL
   ERR_TOOBIG_PARAMETER
   ERR_PALETTE
   ERR_OUT_OF_MEM

SEE ALSO:

   display_init()
   display_palette()    display_gamma()
   display_palentries() display_imagex()

*/

int load_palette(int numentries, colour *pal)
{
   int i;
   byte *clrpal;
   int bdflag;

   log_routine("load_palette");

   D(bug("load_palette: numentries=%d   first=(%d,%d,%d)\n",
	 numentries, pal[0].r, pal[0].g, pal[0].b));

   if (NOT ip_parm_inrange("number_of_entries", numentries, 0, 256))
      return ip_error;

   /* Open display if not opened yet. */

   if (connect() != OK) {
      pop_routine();
      return ip_error;
   }

#ifndef DISPDEBUG

   if (numentries == PAL_OPTIMUM) { /* PAL_OPTIMUM == 0 */
      bdflag = BD_FIXED;
      numentries = BD_MAXPALETTE;
   }else{
      bdflag = BD_CUSTOM;
   }

   if ((clrpal = ip_malloc(768 *sizeof(byte))) == NULL)
      return ip_error;

   for (i=0; i<numentries; i++) {
      clrpal[3*i] = ROUND(255.0 * pow( pal[i].r / 255.0, ip_disp_gamma));
      clrpal[3*i+1] = ROUND(255.0 * pow( pal[i].g / 255.0, ip_disp_gamma));
      clrpal[3*i+2] = ROUND(255.0 * pow( pal[i].b / 255.0, ip_disp_gamma));
   }

   if (bdflag == BD_CUSTOM) {
      for (i=numentries; i<256; i++) {
	 clrpal[3*i] = 0;
	 clrpal[3*i+1] = 0;
	 clrpal[3*i+2] = 0;
      }
   }

   if (bd_Palette(bdflag, clrpal) != BD_SUCCESS) {
      log_error(ERR_PALETTE);
   }

   ip_free(clrpal);

#endif

   ip_disp_pal = -1;

#ifdef DEBUG
   if (ip_error == OK) D(bug("  ...load_palette/OK\n"));
#endif

   pop_routine();
   return ip_error;
}




/*

NAME:

   display_palentries() \- Optimum number of entries in palette

PROTOTYPE:

   #include <ip/ip.h>

   int  display_palentries( void )

DESCRIPTION:

   Returns the number of palette entries the display routines have
   available on the display hardware.  Note that this need not be 256
   entries even if the display hardware is capable of 256 colours.

   This value is useful for designing a palette so that a one-to-one
   relationship exists between pixel values in BYTE images to palette
   entries.  See load_palette() for more info.

SEE ALSO:

   load_palette()

*/

int display_palentries(void)
{
   return BD_MAXPALETTE;
}




/*

NAME:

   display_getpal() \- Get the current display palette

PROTOTYPE:

   #include <ip/ip.h>

   int  display_getpal( palette )
   colour *palette;

DESCRIPTION:

   Returns the current palette installed in the display, as long as it
   is a pre-selected palette loaded with display_palette().  The palette
   returned contains the contrast correction (of display_contrast())
   but not the gamma correction.  There must be room for 256 entries
   in the colour table pointed to by 'palette'.

   Returns -1 if the routine fails; this doesn''t necessarily flag an
   error so you need to check whether the return value is -1 or OK to
   see whether a palette was returned or not.

ERRORS:

   ERR_OUT_OF_MEM

BUGS:

   Currently it is only possible to get back the palette of the PAL_GREY,
   PAL_GREYOBJ and PAL_HOTBODY palettes.

SEE ALSO:

   display_palette()

*/

int display_getpal(colour *palette)
{
   int i;
   byte *intpal;

   log_routine("display_getpal");

   if (ip_disp_pal > PAL_GREYOBJ)
      return -1;

   if ((intpal = alloc_palette(ip_disp_pal)) == NULL) {
      pop_routine();
      return -1;
   }

   for (i=0; i<256; i++) {
      palette[i].r = intpal[i*3];
      palette[i].g = intpal[i*3+1];
      palette[i].b = intpal[i*3+2];
   }

   ip_free(intpal);

   pop_routine();
   return OK;
}




/*

NAME:

   display_pen() \- Return pen number of colour in palette

PROTOTYPE:

   #include <ip/ip.h>

   int  display_pen( colour )
   int colour;

DESCRIPTION:

   Select the pen number of a particular colour in the currently
   selected palette.  Intended primarily for selecting a colour for
   displaying objects. Colours you can ask for are:

\|   OVL_BLACK
\|   OVL_WHITE
\|   OVL_RED
\|   OVL_GREEN
\|   OVL_BLUE
\|   OVL_YELLOW
\|   OVL_CYAN
\|   OVL_MAGENTA

   It is more normal to use the macros:

   PEN_WHITE, PEN_BLACK, PEN_RED, ... , than to call this routine
   directly.

   Note that this routine is only valid to call when pre-selected
   palette is used via display_palette(), not when a custom palette is
   loaded.

ERRORS:

   ERR_OOR_PARAMETER
   ERR_UNAVAILABLE_PEN

SEE ALSO:

   display_palette()

*/

int display_pen(int colour)
{
   int pen;

   log_routine("display_pen");

   if (NOT ip_parm_inrange("colour_request", colour, OVL_START, OVL_END)) {
      pen = 0;
      goto exit_dp;
   }
   pen = (ip_disp_pal >= 0) ? pen_select[ip_disp_pal][colour] : -1;
   if (pen == -1) {
      log_error(ERR_UNAVAILABLE_PEN);
      pen = 0;
   }
   pop_routine();

 exit_dp:

   return pen;
}



/*

NAME:

   display_gamma() \- Set display canvas gamma correction

PROTOTYPE:

   #include <ip/ip.h>

   int  display_gamma( gamma )
   double gamma;

DESCRIPTION:

   Set the gamma correction value to be applied to all images when
   displayed.  Note that the gamma correction is not applied until you
   have called one of display_palette() or load_palette().

SEE ALSO:

   display_palette()    load_palette()    display_contrast()

*/


int display_gamma(double g)
{
   log_routine("display_gamma");

   if (NOT ip_parm_inrange("gamma", g, 0.05, 10.0)) {
      return ip_error;
   }

   ip_disp_gamma = 1.0 / g;

   pop_routine();
   return ip_error;
}




/*

NAME:

   display_contrast() \- Set display canvas contrast correction

PROTOTYPE:

   #include <ip/ip.h>

   int  display_contrast( contrast )
   double contrast;

DESCRIPTION:

   Set the contrast correction value to be applied to all images when
   displayed with a palette selected with display_palette().  Note
   that the contrast correction is not applied until you have called
   display_palette().  The contrast correction is 'not' applied to
   any custom palettes loaded with load_palette().

SEE ALSO:

   display_palette()   load_palette()   display_gamma()

*/


int display_contrast(double c)
{
   log_routine("display_contrast");

   if (NOT ip_parm_inrange("contrast", c, 0.05, 10.0)) {
      return ip_error;
   }

   ip_disp_contrast = c;

   pop_routine();
   return ip_error;
}




/*

NAME:

   display_mouse() \- Get mouse movement in display canvas

PROTOTYPE:

   #include <ip/ip.h>

   event_info * display_mouse( flag )
   int flag;

DESCRIPTION:

   Returns mouse movement events in the event_info structure.  'Flag'
   should be one of:

\&   FLG_NOWAIT Always returns immediately and if a mouse movement has
   occured since the last call to display_mouse() then a valid
   event_info structure is returned, else NULL is returned.

\&   FLG_WAIT If a mouse movement has occurred then returns immediately
   else it waits until a mouse movement occurs then returns.

\&-   At present only the 'type', 'id' and 'pos' fields are filled in the
   event_info structure.  'Id' contains the ID of the image the mouse
   pointer was last over.  'Id' is set to -1 if the mouse pointer was
   over a part of the canvas where no images are displayed.  'Pos'
   contains the coordinates of the mouse pointer with reference to the
   image coordinates of the image identified by 'id'.  If 'id' is -1
   then the coordinates are in the canvas coordinates.  The 'type' field
   is set to EVT_MOUSEMOVE.

ERRORS:

   ERR_DISPLAY_NOT_OPEN
   ERR_BAD_FLAG

SEE ALSO:

   display_init()
   display_event()   display_click()   display_keypress()

*/

event_info * display_mouse(int flag)
{
   mouse *ms;
#ifdef DISPDEBUG
   int ask;
#endif

   log_routine("display_mouse");

   D(bug("display_mouse: flag=0x%x\n",flag));

   if (NOT connected) {
      log_error(ERR_DISPLAY_NOT_OPEN);
      pop_routine();
      return NULL;
   }

   if (NOT flag_ok(flag, FLG_WAIT, FLG_NOWAIT, LASTFLAG)) {
      return NULL;
   }

#ifdef DISPDEBUG
   if (flag == FLG_WAIT) {
      printf("display_mouse: Mouse move required.\n");
      ask = TRUE;
   }else{
      ask = ask_yesno("display_mouse: Issue mouse move");
   }
   if (ask) {
      disp_mouse.type = EVT_MOUSEMOVE;
      disp_mouse.id = ask_imageid();
      disp_mouse.pos = ask_imagepos(disp_mouse.id);
      disp_mouse.left = 0;
      disp_mouse.right = 0;
      disp_mouse.middle = 0;
      disp_mouse.drag = 0;
   }else{
      pop_routine();
      return NULL;
   }

#else

   ms = bd_MouseCoords(flag == FLG_WAIT);

   if (ms == NULL || (size_t)ms == (size_t)-1) {
      pop_routine();
      return NULL;
   }

   disp_mouse.type = EVT_MOUSEMOVE;
   disp_mouse.id = ms->id;
   disp_mouse.pos.x = ms->x;
   disp_mouse.pos.y = ms->y;
   disp_mouse.left = ms->leftbutton;
   disp_mouse.middle = ms->middlebutton;
   disp_mouse.right = ms->rightbutton;
   disp_mouse.drag = ms->drag;

#endif

   D(bug("   ...display_mouse/OK\n"));

   pop_routine();
   return &disp_mouse;
}




/*

NAME:

   display_click() \- Get mouse click in display canvas

PROTOTYPE:

   #include <ip/ip.h>

   event_info * display_click( flag )
   int flag;

DESCRIPTION:

   Get mouse button presses in the display canvas.  'Flag' may be one of:

\&   FLG_NOWAIT Always returns immediately and if a mouse button press has
   occured since the last call to display_mouse() then a valid
   event_info structure is returned, else NULL is returned.

\&   FLG_WAIT If a mouse button press has occurred then returns
   immediately else it waits until a mouse button press occurs then
   returns.
 
\&-   One of the two flags above should be ORed with at least one of (and
   more if desired) of the mouse button specifiers below:

\&   FLG_MOUSELEFT  Consider left button presses
\&   FLG_MOUSEMIDDLE  Consider middle button presses
\&   FLG_MOUSERIGHT  Consider right button presses

\&-   The 'id' field of the event_info structure contains the ID of the image
   in which the mouse click occurred, or -1 if the click occured in a part
   of the canvas where no image is dislpayed. 'Pos' contains the position
   of the mouse click and one of 'left', 'middle' or 'right' will be
   TRUE reflecting which button press satisfied the request.  The 'type'
   field is set to EVT_MOUSEBUTTON.

ERRORS:

   ERR_BAD_FLAG
   ERR_DISPLAY_NOT_OPEN

SEE ALSO:

   display_init()
   display_event()   display_mouse()   display_keypress()

*/

event_info *display_click(int flag)
{
   int left, right, middle;
   int got;
   lastclick *click;
   event_info *res;

   log_routine("display_click");

   D(bug("display_click: flag=0x%x\n",flag));

   if (NOT connected) {
      log_error(ERR_DISPLAY_NOT_OPEN);
      pop_routine();
      return NULL;
   }

   left = flag & FLG_MOUSELEFT;
   right = flag & FLG_MOUSERIGHT;
   middle = flag & FLG_MOUSEMIDDLE;

   if (NOT (left || right || middle)) {
      log_error(ERR_BAD_FLAG);
      pop_routine();
      return NULL;
   }

   flag &= ~(FLG_MOUSELEFT | FLG_MOUSERIGHT | FLG_MOUSEMIDDLE);

   if (NOT flag_ok(flag, FLG_WAIT, FLG_NOWAIT, LASTFLAG)) {
      return NULL;
   }

#ifdef DISPDEBUG
   if (flag == FLG_WAIT) {
      printf("display_click: Mouse click required.\n");
      got = TRUE;
   }else{
      got = ask_yesno("display_click: Issue mouse click");
   }
   if (got) {
      static char *strs[3] = {
	 "Left button",
	 "Middle button",
	 "Right button"
      };

      got = ask_choice(strs, 3, 0);
      disp_mouse.type = EVT_MOUSEBUTTON;
      disp_mouse.id = ask_imageid();
      disp_mouse.pos = ask_imagepos(disp_mouse.id);
      disp_mouse.left = (got == 0);
      disp_mouse.right = (got == 2);
      disp_mouse.middle = (got == 1);
      disp_mouse.drag = 0;
      res = &disp_mouse;
   }else{
      res = NULL;
   }

#else

   if (flag == FLG_WAIT) {
      got = FALSE;
      while (NOT got) {
	 click = bd_LastMouseClick(1);
	 if (click == NULL || (size_t)click == (size_t)-1) {
	    log_error(ERR_ALGORITHM_FAULT,
		      "NULL return from bd_lastclick().\n");
	 }else{
	    disp_mouse.type = EVT_MOUSEBUTTON;
	    disp_mouse.id = click->id;
	    disp_mouse.pos.x = click->x;
	    disp_mouse.pos.y = click->y;
	    disp_mouse.left = (click->whichbutton == BD_LEFT && left);
	    disp_mouse.right = (click->whichbutton == BD_RIGHT && right);
	    disp_mouse.middle = (click->whichbutton == BD_MIDDLE && middle);
	    if (disp_mouse.left || disp_mouse.right || disp_mouse.middle) {
	       got = TRUE;
	    }
	 }
      }
      res = &disp_mouse;
   }else{
      click = bd_LastMouseClick(0);
      if (click != NULL && (size_t)click != (size_t)-1) {
	 disp_mouse.type = EVT_MOUSEBUTTON;
	 disp_mouse.id = click->id;
	 disp_mouse.pos.x = click->x;
	 disp_mouse.pos.y = click->y;
	 disp_mouse.left = (click->whichbutton == BD_LEFT && left);
	 disp_mouse.right = (click->whichbutton == BD_RIGHT && right);
	 disp_mouse.middle = (click->whichbutton == BD_MIDDLE && middle);
	 res = &disp_mouse;
      }else{
	 res = NULL;
      }
   }
#endif

   D(bug("   ...display_click/OK\n"));

   pop_routine();
   return res;
}



/*

NAME:

   display_keypress() \- Get key presses in display window

PROTOTYPE:

   #include <ip/ip.h>

   int  display_keypress( flag )
   int flag;

DESCRIPTION:

   Get a keypress event from the display window.  'Flag' may be one of:

\&   FLG_NOWAIT Always returns immediately and if a key press has
   occured since the last call to display_keypres() then a valid
   ascii character is returned, else -1 is returned.

\&   FLG_WAIT If a key press has occurred then returns immediately else
   it waits until a key press occurs then returns.

ERRORS:

   ERR_BAD_FLAG
   ERR_DISPLAY_NOT_OPEN

SEE ALSO:

   display_init()
   display_event()   display_click()   display_mouse()

*/

int display_keypress(int flag)
{
   int chr;

   log_routine("display_keypress");

   D(bug("display_keypress:  flag=0x%x\n",flag));

   if (NOT connected) {
      log_error(ERR_DISPLAY_NOT_OPEN);
      pop_routine();
      return -1;
   }

   if (NOT flag_ok(flag, FLG_WAIT, FLG_NOWAIT, LASTFLAG)) {
      return -1;
   }

#ifdef DISPDEBUG

   if (flag == FLG_WAIT) {
      printf("display_keypress: Key press required.\n");
      chr = TRUE;
   }else{
      chr = ask_yesno("display_keypress: Issue key press");
   }
   if (chr) {
      printf("Type in a character folowed by a carriage-return.\n");
      chr = getchar();
      while (getchar() != '\n') ;
   }else
      chr = -1;
#else

   chr = bd_GetChar(flag == FLG_WAIT);

#endif

   D(bug("   ...display_keypress/OK\n"));

   pop_routine();
   return chr;
}

   

/*

NAME:

   display_event() \- Get an event from display window

PROTOTYPE:

   #include <ip/ip.h>

   event_info * display_event( flag )
   int flag;

DESCRIPTION:

   Get any sort of event, whether it be mouse move, button click or
   key press, from the display window.  'Flag' should be one of:

\&   FLG_NOWAIT Always returns immediately and if an event has
   occured since the last call to display_event() then a valid
   event_info structure is returned, else NULL is returned.

\&   FLG_WAIT If an event has occurred then returns immediately else it
   waits until a event occurs then returns.

\&-   The 'type' field of the event_info structure is set to reflect the
   type of event that occurred.  See display_mouse() and
   display_click() to see how the event_info structure is filled in
   for mouse moves and mouse button clicks.   For key presses the 'type'
   field is set to EVT_KEYPRESS and the 'chr' field contains the
   key pressed.

ERRORS:

   ERR_DISPLAY_NOT_OPEN
   ERR_BAD_FLAG

SEE ALSO:

   display_init()
   display_mouse()   display_click()   display_keypress()

*/

event_info *display_event(int flag)
{
   event_info *ms;
   int chr;

   log_routine("display_event");

   if (NOT connected) {
      log_error(ERR_DISPLAY_NOT_OPEN);
      pop_routine();
      return NULL;
   }

   if (NOT flag_ok(flag, FLG_WAIT, FLG_NOWAIT, LASTFLAG)) {
      return NULL;
   }

   ms = NULL;
   while (flag == FLG_WAIT && ms == NULL) {
      if ((ms = display_mouse(FLG_NOWAIT)) == NULL) {
	 if ((ms = display_click(FLG_MOUSELEFT | FLG_MOUSERIGHT 
				 | FLG_MOUSEMIDDLE | FLG_NOWAIT)) == NULL) {
	    if ((chr = display_keypress(FLG_NOWAIT)) >= 0) {
	       ms = &disp_mouse;
	       ms->type = EVT_KEYPRESS;
	       ms->chr = chr;
	    }
	 }
      }
   }

   pop_routine();
   return ms;
}





/*** Internal routines folow ****/


/* If not connected to BitDisp then do display_init() to connect to
   BitDisp */
static int connect(void)
{
   int err;
   coord pos;

   if (NOT connected) {
      pos.x = pos.y = 0;
      err = display_init(NULL, NULL, pos, NULL, NO_FLAG);
   }else{
      err = OK;
   }

   return err;
}


/* Allocated an internal standard palette with contrast incorporated
   but not gamma */
static byte *alloc_palette(int pal)
{
   byte *colourpal;
   int i, ni;

   if ((colourpal = ip_mallocx(768 * sizeof(byte))) == NULL) {
      return NULL;
   }

   switch (pal) {
    case PAL_GREY:
      for (i=0; i<256; i++) {
	 ni = cadj(i);
	 colourpal[3*i] = ni;
	 colourpal[3*i + 1] = ni;
	 colourpal[3*i + 2] = ni;
      }
      break;

    case PAL_HOTBODY:
      for (i=0; i<256; i++) {
         ni = (int)ROUND(i * 2.0 * ip_disp_contrast);
	 if (ni < 0) ni = 0;
	 if (ni > 511) ni = 511;
         if (ni < 128) {
            colourpal[3*i] = ni;
            colourpal[3*i + 1] = 0;
            colourpal[3*i + 2] = 0;
         }else if (ni < 256) {
            colourpal[3*i] = ni;
            colourpal[3*i + 1] = ni - 128;
            colourpal[3*i + 2] = 0;
         }else if (ni < 384) {
            colourpal[3*i] = 255;
            colourpal[3*i + 1] = ni-128;
            colourpal[3*i + 2] = ni-256;
         }else{ /* ni < 512 */
            colourpal[3*i] = 255;
            colourpal[3*i + 1] = 255;
            colourpal[3*i + 2] = ni-256;
         }
      }
      break;

    case PAL_GREYOBJ:
      for (i=0; i<128; i++) {
	 ni = cadj(i * 2);
	 colourpal[3*i] = ni;
	 colourpal[3*i + 1] = ni;
	 colourpal[3*i + 2] = ni;
      }
      for (i=128; i<160; i++) {
	 colourpal[3*i] = cadj((i-128) * 8);
	 colourpal[3*i + 1] = 0;
	 colourpal[3*i + 2] = 0;
      }
      for (i=160; i<192; i++) {
	 colourpal[3*i] = 0;
	 colourpal[3*i + 1] = cadj((i-160) * 8);
	 colourpal[3*i + 2] = 0;
      }
      for (i=192; i<224; i++) {
	 colourpal[3*i] = 0;
	 colourpal[3*i + 1] = 0;
	 colourpal[3*i + 2] = cadj((i-192) * 8);
      }
      for (i=224; i<256; i++) {
	 ni = cadj((i-224) * 8);
	 colourpal[3*i] = ni;
	 colourpal[3*i + 1] = ni;
	 colourpal[3*i + 2] = 0;
      }
      break;
   }
   return colourpal;
}


/* Contrast adjustment routine used by alloc_palette() */
static int cadj(int pen)
{
   int tmp;

   tmp = (int)ROUND((double)pen * ip_disp_contrast);
   if (tmp > 255) tmp = 255;
   if (tmp < 0) tmp = 0;
   return tmp;
}
   


/*** Routines to manipulate image_node list ***/

/* Insert new image node - list is sorted by node ID. */
static image_node *in_insert(image_node **list, int id)
{
   image_node *ptr, *prev, *this;

#ifdef IN_DEBUG
   D(bug("** insert_node:\n"));
#endif

   if ((this = ip_mallocx(sizeof(image_node))) != NULL) {

      this->id = id;

      prev = NULL;
      for (ptr=*list; ptr && ptr->id < id; ptr=ptr->next)
	 prev = ptr;

      if (prev) {
	 this->next = prev->next;
	 prev->next = this;
      }else{
	 this->next = *list;
	 *list = this;
      }

   }

#ifdef IN_DEBUG
   dump_in(0);
#endif

   return this;
}


/* Find node in image node list with image ID id. */
static image_node *in_find(int id)
{
   image_node *ptr;

   for (ptr=image_list; ptr && ptr->id<id; ptr=ptr->next)
      ;

   if (NOT (ptr && ptr->id == id)) {
      return NULL;
   }

   return ptr;
}


/* Delete an image node from an image node list */
static int in_delete(image_node **list, int id)
{
   image_node *ptr, *prev;

#ifdef IN_DEBUG
   D(bug("** in_delete:"));
#endif

   prev = NULL;
   for (ptr=*list; ptr && ptr->id<id; ptr=ptr->next)
      prev = ptr;

   if (ptr && ptr->id == id) {
      if (prev) {
	 prev->next = ptr->next;
      }else{
	 *list = ptr->next;
      }

      ip_free(ptr);

      return TRUE;
   }

#ifdef IN_DEBUG
   dump_in(0);
#endif

   return FALSE;
}



#ifdef IN_DEBUG
static void dump_in(int id)
{
   image_node *ptr;

   for (ptr = image_list; ptr; ptr=ptr->next) {
      fprintf(stderr, "    image node: %d", ptr->id);
      if (ptr->type >= 0 && ptr->type < IM_TYPEMAX)
	 fprintf(stderr,"  [%s]", ipmsg_type[ptr->type]);
      else
	 fprintf(stderr,"  [? %d]", ptr->type);
      fprintf(stderr,"  size=(%d,%d)\n",ptr->size.x, ptr->size.y);
   }
   if (NOT image_list) fprintf(stderr,"    image_list [empty]\n");
   if (id != 0) fprintf(stderr,"    Was looking for id %d.\n",id);
}
#endif





#ifdef DISPDEBUG

static int ask_imageid(void)
{
   image_node *imn;
   static char canvas[] = "Background canvas";
   char **msgs;
   char *strs;
   int i, ni, id;

   for (imn = image_list, ni=0; imn; imn=imn->next,ni++) ;
   msgs = ip_mallocx(sizeof(char *)*(ni+1));
   strs = ip_mallocx(128*ni);
   if (msgs && strs) {
      msgs[0] = canvas;
      for (imn = image_list, i=0; imn; imn=imn->next,i++) {
	 sprintf(strs+i*128, "%s [%dx%d] id=%d",
		 ipmsg_type[imn->type], imn->size.x, imn->size.y, imn->id);
	 msgs[i+1] = strs+i*128;
      }
      id = ask_choice(msgs, ni+1, 0)-1;
      if (id > 0) {
	 for (imn = image_list, i=0; i<id; imn=imn->next,i++) ;
	 id = imn->id;
      }else{
	 id = -1;
      }
      ip_free(msgs);
      ip_free(strs);
   }else{
      id = -1;
   }
   return id;
}


static coord ask_imagepos(int id)
{
   image_node *imn;
   coord min,max, pos;

   imn = in_find(id);
   if (imn) {
      max = imn->size;
   }else{
      max.x = disp_width;
      max.y = disp_height;
   }
   min.x = min.y = 0;
   max.x--;
   max.y--;
   pos = ask_coord("Position in image", min, max);
   return pos;
}

#endif
