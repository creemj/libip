/*
   Module: ip/graphic.c
   Author: M.J.Cree

   Graphic drawing routines.

   Copyright (C) 1995-1997, 1999, 2002, 2008, 2015-2016 Michael J. Cree.

ENTRY POINTS:

   im_drawpoint()
   im_drawline()      im_drawbox()
   im_drawcircle()    im_drawellipse()       im_drawellipsex()
*/


#include <stdlib.h>
#include <math.h>

#include <ip/ip.h>

#include "internal.h"
#include "utils.h"

static int draw_ellipse(ip_image *im, double val, double fillval,
			ip_coord2d origin, int a, int b, int flag);


/*

NAME:

   im_drawpoint() \- Draw a point into an image

PROTOTYPE:

   #include <ip/image.h>
   #include <ip/proto.h>

   int  im_drawpoint( im, val, pt )
   ip_image *im;
   double val;
   ip_coord2d pt;

DESCRIPTION:

   Plot a point into the image 'im' at position 'pt' with value 'val'.
   This routine does all necessary error checking so is not
   particulary fast, but handy for plotting a few points.  It will
   return an error for either 'pt' being outside the image bounds
   or for the 'val' being outside the image type range.

   For a much quicker routine when drawing lots and lots of points
   into an image the macro IM_PIXEL() is available.

ERRORS:

   ERR_BAD_COORD
   ERR_BAD_VALUE
   ERR_UNSUPPORTED_TYPE

SEE ALSO:

   im_drawline()     im_drawbox()
   im_drawcircle()  im_drawellipse()  im_drawarrow()

*/

int im_drawpoint(ip_image *im, double val, ip_coord2d pt)
{
    ip_log_routine("im_drawpoint");

    if (NOT ip_point_in_image("point", pt, im))
	return ip_error;
    if (NOT ip_value_in_image("value", val, im))
	return ip_error;

    switch (im->type) {
    case IM_BYTE:
	IM_PIXEL(im,pt.x,pt.y,b) = (int8_t)round(val);
	break;
    case IM_BINARY:
	val = val ? 255: 0;
    case IM_PALETTE:
    case IM_UBYTE:
	IM_PIXEL(im,pt.x,pt.y,ub) = (uint8_t)round(val);
	break;
    case IM_USHORT:
	IM_PIXEL(im,pt.x,pt.y,us) = (uint16_t)round(val);
	break;
    case IM_SHORT:
	IM_PIXEL(im,pt.x,pt.y,s) = (int16_t)round(val);
	break;
    case IM_LONG:
	IM_PIXEL(im,pt.x,pt.y,l) = (int32_t)round(val);
	break;
    case IM_ULONG:
	IM_PIXEL(im,pt.x,pt.y,ul) = (uint32_t)round(val);
	break;
    case IM_FLOAT:
	IM_PIXEL(im,pt.x,pt.y,f) = val;
	break;
    case IM_DOUBLE:
	IM_PIXEL(im,pt.x,pt.y,d) = val;
	break;
    case IM_COMPLEX:
	IM_PIXEL(im,pt.x,pt.y,c).r = val;
	IM_PIXEL(im,pt.x,pt.y,c).i = val;
	break;
    case IM_DCOMPLEX:
	IM_PIXEL(im,pt.x,pt.y,dc).r = val;
	IM_PIXEL(im,pt.x,pt.y,dc).i = val;
	break;
    case IM_RGB:
	IM_PIXEL(im,pt.x,pt.y,r).r =
	    IM_PIXEL(im,pt.x,pt.y,r).g =
	    IM_PIXEL(im,pt.x,pt.y,r).b = (uint8_t)round(val);
	break;
    case IM_RGB16:
	IM_PIXEL(im,pt.x,pt.y,r16).r =
	    IM_PIXEL(im,pt.x,pt.y,r16).g =
	    IM_PIXEL(im,pt.x,pt.y,r16).b = (uint16_t)round(val);
	break;
    default:
	ip_log_error(ERR_NOT_IMPLEMENTED);
	break;
    }
    ip_pop_routine();
    return OK;
}


/*

NAME:

   im_drawline() \- Draw a line into an image

PROTOTYPE:

   #include <ip/ip.h>

   int  im_drawline( im, val, start, end )
   ip_image *im;
   double val;
   ip_coord2d start, end;

DESCRIPTION:

   Draws a line into the image 'im' with the value 'val' starting at
   the point 'start' and ending at the point 'end'.  Checks first that
   'val', 'start' and 'end' are all valid before drawing the line.

ERRORS:

   ERR_BAD_COORD
   ERR_BAD_VALUE
   ERR_UNSUPPORTED_TYPE

SEE ALSO:

   im_drawpoint()   im_drawbox()
   im_drawcircle()  im_drawellipse()  im_drawarrow()

*/

#define DRAW_POINT(im,pt)			\
    switch (im->type) {				\
    case IM_BINARY:				\
    case IM_PALETTE:				\
    case IM_UBYTE:				\
    case IM_BYTE:				\
	IM_PIXEL(im,pt.x,pt.y,ub) = bval;	\
	break;					\
    case IM_SHORT:				\
    case IM_USHORT:				\
	IM_PIXEL(im,pt.x,pt.y,us) = sval;	\
	break;					\
    case IM_LONG:				\
    case IM_ULONG:				\
	IM_PIXEL(im,pt.x,pt.y,ul) = lval;	\
	break;					\
    case IM_FLOAT:				\
	IM_PIXEL(im,pt.x,pt.y,f) = fval;	\
	break;					\
    case IM_DOUBLE:				\
	IM_PIXEL(im,pt.x,pt.y,d) = val;		\
	break;					\
    default:					\
	break;					\
    }


int im_drawline(ip_image *im, double val, ip_coord2d start, ip_coord2d end)
{
    uint8_t bval;
    uint16_t sval;
    uint32_t lval;
    float fval;

    ip_log_routine(__func__);

    /* Check parameters okay */

    if (NOT ip_point_in_image("start_pt", start, im))
	return ip_error;
    if (NOT ip_point_in_image("end_pt", end, im))
	return ip_error;
    if (NOT ip_value_in_image("pixel_val", val, im))
	return ip_error;

    if (im->type == IM_COMPLEX || im->type == IM_RGB || im->type == IM_RGB16) {
	ip_log_error(ERR_UNSUPPORTED_TYPE);
	ip_pop_routine();
	return ip_error;
   }

    bval = (uint8_t)ROUND(val);
    sval = (uint16_t)ROUND(val);
    lval = (uint32_t)ROUND(val);
    fval = (float)val;
    if (im->type == IM_BINARY) {
	bval = (bval == 0) ? 0 : 255;
    }

    if (end.x != start.x && end.y != start.y) {
	ip_coord2d pt, diff;
	ip_coord2d step1, step2;
	int error;
	if (start.x > end.x) {
	    ip_coord2d tmp = start;
	    start = end;
	    end = tmp;
	}
	pt = start;
	DRAW_POINT(im, pt);
	diff.y = end.y - start.y;
	diff.x = end.x - start.x;
	if (abs(diff.y) < diff.x) {
	    step2.y = step2.x = step1.x = 1;
	    step1.y = 0;
	    if (diff.y < 0) {
		diff.y = -diff.y;
		step1.y = -step1.y;
		step2.y = -step2.y;
	    }
	}else{
	    int tmp;
	    step2.y = step2.x = step1.y = 1;
	    step1.x = 0;
	    tmp = diff.x;
	    diff.x = diff.y;
	    diff.y = tmp;
	    if (diff.x < 0) {
		diff.x = -diff.x;
		step1.y = -step1.y;
		step2.y = -step2.y;
	    }
	}
	error = 0;
	do {
	    if (2*(error + diff.y) < diff.x) {
		pt.x += step1.x;
		pt.y += step1.y;
		error += diff.y;
	    }else{
		pt.x += step2.x;
		pt.y += step2.y;
		error += diff.y - diff.x;
	    }
	    DRAW_POINT(im,pt);
	} while (pt.x != end.x || pt.y != end.y);

	ip_pop_routine();
	return OK;
    }

    if (start.x > end.x || start.y > end.y ) {
	ip_coord2d tmp = start;
	start = end;
	end = tmp;
    }

    if (end.y == start.y) {
	switch (im->type) {
	case IM_BINARY:
	case IM_BYTE:
	case IM_UBYTE:
	case IM_PALETTE:
	    for (int i=start.x; i<=end.x; i++)
		IM_PIXEL(im, i,start.y, ub) = bval;
	    break;
	case IM_SHORT:
	case IM_USHORT:
	    for (int i=start.x; i<=end.x; i++)
		IM_PIXEL(im, i,start.y, us) = sval;
	    break;
	case IM_LONG:
	case IM_ULONG:
	    for (int i=start.x; i<=end.x; i++)
		IM_PIXEL(im, i,start.y, ul) = lval;
	    break;
	case IM_FLOAT:
	    for (int i=start.x; i<=end.x; i++)
		IM_PIXEL(im, i,start.y, f) = fval;
	    break;
	case IM_DOUBLE:
	    for (int i=start.x; i<=end.x; i++)
		IM_PIXEL(im, i,start.y, f) = val;
	    break;
	default:
	    break;
	}
    }

    if (end.x == start.x) {
	switch (im->type) {
	case IM_BINARY:
	case IM_BYTE:
	case IM_UBYTE:
	case IM_PALETTE:
	    for (int i=start.y; i<=end.y; i++)
		IM_PIXEL(im, start.x,i, ub) = bval;
	    break;
	case IM_SHORT:
	case IM_USHORT:
	    for (int i=start.y; i<=end.y; i++)
		IM_PIXEL(im, start.x,i, us) = sval;
	    break;
	case IM_LONG:
	case IM_ULONG:
	    for (int i=start.y; i<=end.y; i++)
		IM_PIXEL(im, start.x,i, ul) = lval;
	    break;
	case IM_FLOAT:
	    for (int i=start.y; i<=end.y; i++)
		IM_PIXEL(im, start.x,i, f) = fval;
	    break;
	case IM_DOUBLE:
	    for (int i=start.y; i<=end.y; i++)
		IM_PIXEL(im, start.x,i, d) = val;
	    break;
	default:
	    break;
	}
    }

    ip_pop_routine();
    return OK;
}

#undef DRAW_POINT


/*

NAME:

   im_drawbox() \- Draw a box into an image

PROTOTYPE:

   #include <ip/ip.h>

   int  im_drawbox( im, val, box )
   ip_image *im;
   double val;
   ip_box box;

DESCRIPTION:

   Draw the rectangular box 'box' into the image 'im' with pixel
   value 'val'.  Should any part of 'box' extend outside the image
   boundaries an error is flagged.

ERRORS:

   ERR_BAD_BOX
   ERR_BAD_VALUE
   ERR_UNSUPPORTED_TYPE

SEE ALSO:

   im_drawpoint()   im_drawline()
   im_drawcircle()  im_drawellipse()  im_drawarrow()

*/

int im_drawbox(ip_image *im, double val, ip_box tbox)
{
    ip_coord2d str,end;

    ip_log_routine("im_drawbox");

    if (NOT ip_box_in_image("box", tbox, im)) {
	return ip_error;
    }

    str = tbox.origin;
    end.x = tbox.size.x + tbox.origin.x - 1;
    end.y = tbox.origin.y;
    if (im_drawline(im, val, str, end) != OK)
	goto exit;
    end.x = tbox.origin.x;
    end.y = tbox.size.y + tbox.origin.y - 1;
    if (im_drawline(im, val, str, end) != OK)
	goto exit;
    str.x = end.x = tbox.size.x + tbox.origin.x - 1;
    if (im_drawline(im, val, str, end) != OK)
	goto exit;
    str.x = tbox.origin.x;
    str.y = tbox.origin.y + tbox.size.y - 1;
    if (im_drawline(im, val, str, end) != OK)
	goto exit;

 exit:
    ip_pop_routine();
    return ip_error;
}


/*

NAME:

   im_drawcircle() \- Draw a circle into the image

PROTOTYPE:

   #include <ip/ip.h>

   int im_drawcircle( im, val, origin, radius )
   ip_image *im;
   double val;
   ip_coord2d origin;
   int radius;

DESCRIPTION:

   This routine is implemented as a call to im_drawellipsex() with the
   parameters 'a' and 'b' both set to 'radius'.

ERRORS:

   ERR_BAD_VALUE
   ERR_BAD_PARAMETER
   ERR_UNSUPPORTED_TYPE

SEE ALSO:

   im_drawellipse()

*/

int im_drawcircle(ip_image *im, double val, ip_coord2d origin, int r)
{
    ip_log_routine(__func__);
    return draw_ellipse(im, val, 0.0, origin, r, r, NO_FLAG);
}


/*

NAME:

   im_drawellipse() \- Draw an ellipse into an image

PROTOTYPE:

   #include <ip/ip.h>

   int  im_drawellipse( im, val, origin, a, b )
   ip_image *im;
   double val;
   ip_coord2d origin;
   int a, b;

DESCRIPTION:

   Draw an ellipse into the image 'im' with pixel value 'val'.  The ellipse
   is centred on 'origin' and has 'x'-axis length of 'a' and 'y'-axis length
   of 'b'.  It is not possible to draw an arbitrarily orientated ellipse
   with this routine, since the major/minor axes must lie along the 'x' and
   'y' axes.

   All the ellipse must lie completely within the image boundaries else
   an error is flagged.

ERRORS:

   ERR_BAD_VALUE
   ERR_BAD_PARAMETER
   ERR_UNSUPPORTED_TYPE

SEE ALSO:

   im_drawpoint()   im_drawline()     im_drawbox()
   im_drawcircle()  im_drawellipse()  im_drawarrow()

*/

int im_drawellipse(ip_image *im, double val, ip_coord2d origin, int a, int b)
{
    ip_log_routine(__func__);
    return draw_ellipse(im, val, 0.0, origin, a, b, NO_FLAG);
}



/*

NAME:

   im_drawellipsex() \- Draw an ellipse into an image

PROTOTYPE:

   #include <ip/ip.h>

   int  im_drawellipse( im, val, fillval, origin, a, b, flag )
   ip_image *im;
   double val,fillval;
   ip_coord2d origin;
   int a, b;
   int flag;

DESCRIPTION:

   Draw an ellipse into the image 'im' with pixel value 'val'.  The ellipse
   is centred on 'origin' and has 'x'-axis length of 'a' and 'y'-axis length
   of 'b'.  It is not possible to draw an arbitrarily orientated ellipse
   with this routine, since the major/minor axes must lie along the 'x' and
   'y' axes.

   All the ellipse must lie completely within the image boundaries else an
   error is flagged.

ERRORS:

   ERR_BAD_VALUE
   ERR_BAD_PARAMETER
   ERR_UNSUPPORTED_TYPE

SEE ALSO:

   im_drawpoint()   im_drawline()     im_drawbox()
   im_drawcircle()  im_drawellipse()  im_drawarrow()

*/




#define DRAW_FOUR_POINTS(im, origin, pt)	\
    do {					\
	int opx, opy, omx, omy;			\
	opx = origin.x + pt.x;			\
	opy = origin.y + pt.y;			\
	omx = origin.x - pt.x;			\
	omy = origin.y - pt.y;			\
	if (fillit) {				\
	    ip_coord2d str, end;		\
	    str.x = omx+1;			\
	    str.y = end.y = omy;		\
	    end.x = opx-1;			\
	    im_drawline(im, fillval, str, end);	\
	    str.y = end.y = opy;		\
	    im_drawline(im, fillval, str, end);	\
	}					\
	switch (im->type) {			\
	case IM_BINARY:				\
	case IM_PALETTE:			\
	case IM_BYTE:				\
	case IM_UBYTE:				\
	    IM_PIXEL(im,opx,opy,ub) = bval;	\
	    IM_PIXEL(im,opx,omy,ub) = bval;	\
	    IM_PIXEL(im,omx,opy,ub) = bval;	\
	    IM_PIXEL(im,omx,omy,ub) = bval;	\
	    break;				\
	case IM_SHORT:				\
	case IM_USHORT:				\
	    IM_PIXEL(im,opx,opy,us) = sval;	\
	    IM_PIXEL(im,opx,omy,us) = sval;	\
	    IM_PIXEL(im,omx,opy,us) = sval;	\
	    IM_PIXEL(im,omx,omy,us) = sval;	\
	    break;				\
	case IM_LONG:				\
	case IM_ULONG:				\
	    IM_PIXEL(im,opx,opy,ul) = lval;	\
	    IM_PIXEL(im,opx,omy,ul) = lval;	\
	    IM_PIXEL(im,omx,opy,ul) = lval;	\
	    IM_PIXEL(im,omx,omy,ul) = lval;	\
	    break;				\
	case IM_FLOAT:				\
	    IM_PIXEL(im,opx,opy,f) = fval;	\
	    IM_PIXEL(im,opx,omy,f) = fval;	\
	    IM_PIXEL(im,omx,opy,f) = fval;	\
	    IM_PIXEL(im,omx,omy,f) = fval;	\
	    break;				\
	case IM_DOUBLE:				\
	    IM_PIXEL(im,opx,opy,d) = val;	\
	    IM_PIXEL(im,opx,omy,d) = val;	\
	    IM_PIXEL(im,omx,opy,d) = val;	\
	    IM_PIXEL(im,omx,omy,d) = val;	\
	    break;				\
	default:				\
	    break;				\
	}					\
    } while (0)



int im_drawellipsex(ip_image *im, double val, double fillval,
		    ip_coord2d origin, int a, int b, int flag)
{
    ip_log_routine(__func__);
    return draw_ellipse(im, val, fillval, origin, a, b, flag);
}


static int draw_ellipse(ip_image *im, double val, double fillval,
			ip_coord2d origin, int a, int b, int flag)
{
    int error;
    ip_coord2d pt;
    int change;
    int a2,b2;
    uint8_t bval;
    uint16_t sval;
    uint32_t lval;
    float fval;
    int dofill;
    int fillit;

    if (origin.x - a < 0 || origin.x + a >= im->size.x ||
	            origin.y - b < 0 || origin.y + b >= im->size.y) {
	ip_log_error(ERR_BAD_PARAMETER);
	ip_pop_routine();
	return ip_error;
    }

    if (im->type == IM_COMPLEX || im->type == IM_RGB || im->type == IM_RGB16) {
	ip_log_error(ERR_UNSUPPORTED_TYPE);
	ip_pop_routine();
	return ip_error;
    }

    if (NOT ip_flag_ok(flag, FLG_FILLIN, LASTFLAG))
	return ip_error;

    dofill = (flag == FLG_FILLIN);

    if (NOT ip_value_in_image("pixel_val", val, im))
	return ip_error;
    if (dofill) {
	if (NOT ip_value_in_image("fill_val", fillval, im))
	    return ip_error;
    }

    bval = (uint8_t)ROUND(val);
    sval = (uint16_t)ROUND(val);
    lval = (uint32_t)ROUND(val);
    fval = (float)val;
    if (im->type == IM_BINARY) {
	bval = (bval == 0) ? 0 : 255;
    }

    a2 = a*a;
    b2 = b*b;
    change = ROUND((double)a2 / ((double)b * sqrt(1.0 + (double)a2/b2)));

    error = 0;
    pt.x = 0;
    pt.y = b;
    fillit = FALSE;

    while (pt.x < change) {
	int decide, xdiff, ydiff;

	DRAW_FOUR_POINTS(im, origin, pt);
	xdiff = b2 * (2*pt.x + 1);
	ydiff = a2 * (2*pt.y - 1);
	decide = 2*error - 2*xdiff + ydiff;
	if (decide >= 0) {
	    pt.x++;
	    error -= xdiff;
	    fillit = FALSE;
	}else{
	    pt.x++;
	    pt.y--;
	    error -= xdiff - ydiff;
	    fillit = dofill;
	}
    }

    error = a2*(b2 - pt.y*pt.y) - b2*pt.x*pt.x;

    while (pt.y > 0) {
	int decide, xdiff, ydiff;

	DRAW_FOUR_POINTS(im, origin, pt);
	xdiff = b2 * (2*pt.x + 1);
	ydiff = a2 * (2*pt.y - 1);
	decide = 2*error + 2*ydiff - xdiff;
	fillit = dofill;
	if (decide >= 0) {
	    pt.x++;
	    pt.y--;
	    error += ydiff - xdiff;
	}else{
	    pt.y--;
	    error += ydiff;
	}
    }

    DRAW_FOUR_POINTS(im, origin, pt);

    ip_pop_routine();
    return OK;
}
