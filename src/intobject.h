/*
   Header: ip/intobject.h
   Author: M.J.Cree

   Header files for object definitions internal to the image processing
   routines.

*/

#ifndef IP_INTOBJECT_H
#define IP_INTOBJECT_H

#include <ip/object.h>


/* Extra definitions for the object type, that we do not want to be
   public */

/* Flags used in status field of object */

#define OSB_COMPLETE 0x0001U
#define OSB_ENCODING 0x0002U
#define OSB_COORDSYS 0x0004U

/* Whether the object is 'complete' or 'partial' */
#define OBJ_PARTIAL(o)	(NOT ((o)->status & OSB_COMPLETE))
#define OBJ_COMPLETE(o)	((o)->status & OSB_COMPLETE)
#define SET_PARTIAL(o)	((o)->status &= ~OSB_COMPLETE)
#define SET_COMPLETE(o)	((o)->status |= OSB_COMPLETE)


/* Whether run-length encoding or run-stop encoding is used. Only
   valid in 'partial' objects. 'Complete' objects always use
   run-length encoding. */
#define OBJ_RUNLEN(o) ((o)->status & OSB_ENCODING)
#define OBJ_RUNSTOP(o) (NOT ((o)->status & OSB_ENCODING))
#define SET_RUNLEN(o)	((o)->status |= OSB_ENCODING)
#define SET_RUNSTOP(o)	((o)->status &= ~OSB_ENCODING)

/* Coordinate system used in object. 'Complete' objects can only be in
   object coordinates. */
#define OBJ_OBJCOORD(o) ((o)->status & OSB_COORDSYS)
#define OBJ_IMCOORD(o) (NOT ((o)->status & OSB_COORDSYS))
#define SET_OBJCOORD(o)	((o)->status |= OSB_COORDSYS)
#define SET_IMCOORD(o)	((o)->status &= ~OSB_COORDSYS)


/* So can reuse numrows field of 'partial' object for different
   meaning, while constructing object. */

#define botrow numrows

/* rle special codes */

/* Choose an invalid rle start position and end position
   (well actually, there is no invalid value, so choose the most
   unlikely one) to indicate a row with as yet unknown start position
   or unkown end position. The code depends on UNSTARTED being greater
   then any posible start and UNENDED being less than any possible
   end point.
*/
#define UNSTARTED SHRT_MAX
#define UNENDED SHRT_MIN


/* Some internal call points in object.c that are not intended to be public
   for calling programs, but are defined here for ip internal routines. */

extern rowtype *ip_obj_get_row(ip_object *o, int row);
extern int ip_obj_insert_run(ip_object *o, int row,int start,int length);
extern void ip_obj_insert_run_entry(rowtype *, int pos, int start, int end);
extern int ip_obj_get_run_end(ip_object *obj, int rowidx, int xpt);
extern int ip_obj_complete(ip_object *);
extern int ip_run_adjacent(rletype *rle1, rletype *rle2);
extern int ip_run_overlap(rletype *rle1, rletype *rle2);


#endif
