/*

   Module: ip/morphrecon.c
   Author: M.J.Cree

   Copyright (C) 2016, 2018 Michael J. Cree

   Morphological reconstruction.

ENTRY POINTS:

   im_reconstruct()

*/

#include <stdlib.h>
#include <string.h>

#include <ip/ip.h>
#include <ip/divide.h>
#include <ip/stats.h>
#include <ip/morph.h>

#include "internal.h"
#include "function_ops.h"

typedef int (*improc_function_i_II)(ip_image *d, ip_image *s);

/*
 * Counts the number of iterations of progressive reconstruction.
 */

int ip_reconstruct_iterations;


/*
 * Morphological reconstruction by progressive reconstruction by
 * dilation/erosion. This implementation writes a result pixel back into the
 * marker image as soon as it has the result, thus the iteration gets
 * propagated in the raster scan direction.  We proceed in normal raster
 * scan direction which propagates the iteration towards the bottom-right of
 * the image, then perform a raster scan direction in reverse direction
 * which propagates the iteration back to the top-left of the image.  For
 * many images this converges far faster than the naive algorithm.
 */

#define generate_recon_progressive_n4(type, name, opmax, opmin)		\
    static int im_##name##_progressive_n4_##type(ip_image *marker, ip_image *mask) \
    {									\
	type##_type *r_down = IM_ROWADR(marker, 0, v);			\
	type##_type *r_this = r_down;					\
	type##_type *r_up;						\
	int differs = 0;						\
	/* Dilate/erode down-right, i.e., normal raster scan */		\
	for (int j=0; j<marker->size.y; j++) {				\
	    type##_type *r_mask;					\
	    type##_type prev, this, next, new;				\
	    int x = marker->size.x-1;					\
	    r_up = r_this;						\
	    r_this = r_down;						\
	    r_down = IM_ROWADR(marker, (j<marker->size.y-1) ? (j+1) : j, v); \
	    r_mask = IM_ROWADR(mask, j, v);				\
	    next = this = r_this[0];					\
	    for (int i=0; i<marker->size.x-1; i++) {			\
		prev = this;						\
		this = next;						\
		next = r_this[i+1];					\
		new = opmax(this, next);				\
		new = opmax(new, prev);					\
		new = opmax(new, r_up[i]);				\
		new = opmax(new, r_down[i]);				\
		new = opmin(new, r_mask[i]);				\
		differs |= (new != this);				\
		this = new;						\
		r_this[i] = this;					\
	    }								\
	    prev = this;						\
	    this = next;						\
	    next = r_this[x];						\
	    new = opmax(this, next);					\
	    new = opmax(new, prev);					\
	    new = opmax(new, r_up[x]);					\
	    new = opmax(new, r_down[x]);				\
	    new = opmin(new, r_mask[x]);				\
	    differs |= (new != this);					\
	    this = new;							\
	    r_this[x] = this;						\
	}								\
	/* Dilate/erode up-left, i.e., reverse raster scan */		\
	r_up = IM_ROWADR(marker, marker->size.y-1, v);			\
	r_this = r_up;							\
									\
	for (int j=marker->size.y-1; j>=0; j--) {			\
	    type##_type *r_mask;					\
	    type##_type prev, this, next, new;				\
	    r_down = r_this;						\
	    r_this = r_up;						\
	    r_up = IM_ROWADR(marker, (j>0) ? (j-1) : j, v);		\
	    r_mask = IM_ROWADR(mask, j, v);				\
	    prev = this = r_this[marker->size.x-1];			\
	    for (int i=marker->size.x-1; i>0; i--) {			\
		next = this;						\
		this = prev;						\
		prev = r_this[i-1];					\
		new = opmax(this, next);				\
		new = opmax(new, prev);					\
		new = opmax(new, r_up[i]);				\
		new = opmax(new, r_down[i]);				\
		new = opmin(new, r_mask[i]);				\
		differs |= (new != this);				\
		this = new;						\
		r_this[i] = this;					\
	    }								\
	    next = this;						\
	    this = prev;						\
	    prev = r_this[0];						\
	    new = opmax(this, prev);					\
	    new = opmax(new, next);					\
	    new = opmax(new, r_up[0]);					\
	    new = opmax(new, r_down[0]);				\
	    new = opmin(new, r_mask[0]);				\
	    differs |= (new != this);					\
	    this = new;							\
	    r_this[0] = this;						\
	}								\
	return differs;							\
    }


generate_recon_progressive_n4(UBYTE, dilate, MAX, MIN)
generate_recon_progressive_n4(UBYTE, erode, MIN, MAX)
generate_recon_progressive_n4(BYTE, dilate, MAX, MIN)
generate_recon_progressive_n4(BYTE, erode, MIN, MAX)
generate_recon_progressive_n4(USHORT, dilate, MAX, MIN)
generate_recon_progressive_n4(USHORT, erode, MIN, MAX)
generate_recon_progressive_n4(SHORT, dilate, MAX, MIN)
generate_recon_progressive_n4(SHORT, erode, MIN, MAX)
generate_recon_progressive_n4(ULONG, dilate, MAX, MIN)
generate_recon_progressive_n4(ULONG, erode, MIN, MAX)
generate_recon_progressive_n4(LONG, dilate, MAX, MIN)
generate_recon_progressive_n4(LONG, erode, MIN, MAX)
generate_recon_progressive_n4(FLOAT, dilate, MAX, MIN)
generate_recon_progressive_n4(FLOAT, erode, MIN, MAX)
generate_recon_progressive_n4(DOUBLE, dilate, MAX, MIN)
generate_recon_progressive_n4(DOUBLE, erode, MIN, MAX)

static improc_function_i_II im_dilate_progressive_n4_bytype[IM_TYPEMAX] = {
    [IM_BINARY] = im_dilate_progressive_n4_UBYTE,
    [IM_UBYTE] = im_dilate_progressive_n4_UBYTE,
    [IM_BYTE] = im_dilate_progressive_n4_BYTE,
    [IM_USHORT] = im_dilate_progressive_n4_USHORT,
    [IM_SHORT] = im_dilate_progressive_n4_SHORT,
    [IM_ULONG] = im_dilate_progressive_n4_ULONG,
    [IM_LONG] = im_dilate_progressive_n4_LONG,
    [IM_FLOAT] = im_dilate_progressive_n4_FLOAT,
    [IM_DOUBLE] = im_dilate_progressive_n4_DOUBLE
};


static improc_function_i_II im_erode_progressive_n4_bytype[IM_TYPEMAX] = {
    [IM_BINARY] = im_erode_progressive_n4_UBYTE,
    [IM_UBYTE] = im_erode_progressive_n4_UBYTE,
    [IM_BYTE] = im_erode_progressive_n4_BYTE,
    [IM_USHORT] = im_erode_progressive_n4_USHORT,
    [IM_SHORT] = im_erode_progressive_n4_SHORT,
    [IM_ULONG] = im_erode_progressive_n4_ULONG,
    [IM_LONG] = im_erode_progressive_n4_LONG,
    [IM_FLOAT] = im_erode_progressive_n4_FLOAT,
    [IM_DOUBLE] = im_erode_progressive_n4_DOUBLE
};


/*
 * Do the eight-neighbourhood case.
 *
 * We now maintain previous, this and next values in CPU registers for each
 * row to save on memory accessess.  Thus nine CPU registers are used, plus
 * another three for the row pointers, plus one for the mask row pointer,
 * plus one for counting iterations, which is fourteen registers, so we are
 * at risk of CPU register spill on register starved architectures.  And now
 * variable differs adds one more register!
 */

#define generate_recon_progressive_n8(type, name, opmax, opmin)		\
    static int im_##name##_progressive_n8_##type(ip_image *marker, ip_image *mask) \
    {									\
	type##_type *r_down = IM_ROWADR(marker, 0, v);			\
	type##_type *r_this = r_down;					\
	type##_type *r_up;						\
	int differs = 0;						\
	/* Dilate/erode down-right, i.e., normal raster scan */		\
	for (int j=0; j<marker->size.y; j++) {				\
	    type##_type *r_mask;					\
	    type##_type prev, this, next, new;				\
	    type##_type u_prev, u_this, u_next;				\
	    type##_type d_prev, d_this, d_next;				\
	    int x = marker->size.x-1;					\
	    r_up = r_this;						\
	    r_this = r_down;						\
	    r_down = IM_ROWADR(marker, (j<marker->size.y-1) ? (j+1) : j, v); \
	    r_mask = IM_ROWADR(mask, j, v);				\
	    next = this = r_this[0];					\
	    u_next = u_this = r_up[0];					\
	    d_next = d_this = r_down[0];				\
	    for (int i=0; i<marker->size.x-1; i++) {			\
		prev = this;						\
		this = next;						\
		next = r_this[i+1];					\
		u_prev = u_this;					\
		u_this = u_next;					\
		u_next = r_up[i+1];					\
		d_prev = d_this;					\
		d_this = d_next;					\
		d_next = r_down[i+1];					\
		new = opmax(this, prev);				\
		new = opmax(new, next);					\
		new = opmax(new, d_prev);				\
		new = opmax(new, d_this);				\
		new = opmax(new, d_next);				\
		new = opmax(new, u_prev);				\
		new = opmax(new, u_this);				\
		new = opmax(new, u_next);				\
		new = opmin(new, r_mask[i]);				\
		differs |= (new != this);				\
		this = new;						\
		r_this[i] = this;					\
	    }								\
	    prev = this;						\
	    this = next;						\
	    next = r_this[x];						\
	    u_prev = u_this;						\
	    u_this = u_next;						\
	    u_next = r_up[x];						\
	    d_prev = d_this;						\
	    d_this = d_next;						\
	    d_next = r_down[x];						\
	    new = opmax(this, prev);					\
	    new = opmax(new, next);					\
	    new = opmax(new, d_prev);					\
	    new = opmax(new, d_this);					\
	    new = opmax(new, d_next);					\
	    new = opmax(new, u_prev);					\
	    new = opmax(new, u_this);					\
	    new = opmax(new, u_next);					\
	    new = opmin(new, r_mask[x]);				\
	    differs |= (new != this);					\
	    this = new;							\
	    r_this[x] = this;						\
	}								\
	/* Dilate/erode up-left, i.e., reverse raster scan */		\
	r_up = IM_ROWADR(marker, marker->size.y-1, v);			\
	r_this = r_up;							\
									\
	for (int j=marker->size.y-1; j>=0; j--) {			\
	    type##_type *r_mask;					\
	    type##_type prev, this, next, new;				\
	    type##_type u_prev, u_this, u_next;				\
	    type##_type d_prev, d_this, d_next;				\
	    r_down = r_this;						\
	    r_this = r_up;						\
	    r_up = IM_ROWADR(marker, (j>0) ? (j-1) : j, v);		\
	    r_mask = IM_ROWADR(mask, j, v);				\
	    prev = this = r_this[marker->size.x-1];			\
	    u_prev = u_this = r_up[marker->size.x-1];			\
	    d_prev = d_this = r_down[marker->size.x-1];			\
	    for (int i=marker->size.x-1; i>0; i--) {			\
		next = this;						\
		this = prev;						\
		prev = r_this[i-1];					\
		d_next = d_this;					\
		d_this = d_prev;					\
		d_prev = r_down[i-1];					\
		u_next = u_this;					\
		u_this = u_prev;					\
		u_prev = r_up[i-1];					\
		new = opmax(this, prev);				\
		new = opmax(new, next);					\
		new = opmax(new, d_prev);				\
		new = opmax(new, d_this);				\
		new = opmax(new, d_next);				\
		new = opmax(new, u_prev);				\
		new = opmax(new, u_this);				\
		new = opmax(new, u_next);				\
		new = opmin(new, r_mask[i]);				\
		differs |= (new != this);				\
		this = new;						\
		r_this[i] = this;					\
	    }								\
	    next = this;						\
	    this = prev;						\
	    d_next = d_this;						\
	    d_this = d_prev;						\
	    u_next = u_this;						\
	    u_this = u_prev;						\
	    new = opmax(this, r_this[0]);				\
	    new = opmax(new, next);					\
	    new = opmax(new, r_down[0]);				\
	    new = opmax(new, d_this);					\
	    new = opmax(new, d_next);					\
	    new = opmax(new, r_up[0]);					\
	    new = opmax(new, u_this);					\
	    new = opmax(new, u_next);					\
	    new = opmin(new, r_mask[0]);				\
	    differs |= (new != this);					\
	    this = new;							\
	    r_this[0] = this;						\
	}								\
	return differs;							\
    }

generate_recon_progressive_n8(UBYTE, dilate, MAX, MIN)
generate_recon_progressive_n8(UBYTE, erode, MIN, MAX)
generate_recon_progressive_n8(BYTE, dilate, MAX, MIN)
generate_recon_progressive_n8(BYTE, erode, MIN, MAX)
generate_recon_progressive_n8(USHORT, dilate, MAX, MIN)
generate_recon_progressive_n8(USHORT, erode, MIN, MAX)
generate_recon_progressive_n8(SHORT, dilate, MAX, MIN)
generate_recon_progressive_n8(SHORT, erode, MIN, MAX)
generate_recon_progressive_n8(ULONG, dilate, MAX, MIN)
generate_recon_progressive_n8(ULONG, erode, MIN, MAX)
generate_recon_progressive_n8(LONG, dilate, MAX, MIN)
generate_recon_progressive_n8(LONG, erode, MIN, MAX)
generate_recon_progressive_n8(FLOAT, dilate, MAX, MIN)
generate_recon_progressive_n8(FLOAT, erode, MIN, MAX)
generate_recon_progressive_n8(DOUBLE, dilate, MAX, MIN)
generate_recon_progressive_n8(DOUBLE, erode, MIN, MAX)

static improc_function_i_II im_dilate_progressive_n8_bytype[IM_TYPEMAX] = {
    [IM_BINARY] = im_dilate_progressive_n8_UBYTE,
    [IM_UBYTE] = im_dilate_progressive_n8_UBYTE,
    [IM_BYTE] = im_dilate_progressive_n8_BYTE,
    [IM_USHORT] = im_dilate_progressive_n8_USHORT,
    [IM_SHORT] = im_dilate_progressive_n8_SHORT,
    [IM_ULONG] = im_dilate_progressive_n8_ULONG,
    [IM_LONG] = im_dilate_progressive_n8_LONG,
    [IM_FLOAT] = im_dilate_progressive_n8_FLOAT,
    [IM_DOUBLE] = im_dilate_progressive_n8_DOUBLE
};


static improc_function_i_II im_erode_progressive_n8_bytype[IM_TYPEMAX] = {
    [IM_BINARY] = im_erode_progressive_n8_UBYTE,
    [IM_UBYTE] = im_erode_progressive_n8_UBYTE,
    [IM_BYTE] = im_erode_progressive_n8_BYTE,
    [IM_USHORT] = im_erode_progressive_n8_USHORT,
    [IM_SHORT] = im_erode_progressive_n8_SHORT,
    [IM_ULONG] = im_erode_progressive_n8_ULONG,
    [IM_LONG] = im_erode_progressive_n8_LONG,
    [IM_FLOAT] = im_erode_progressive_n8_FLOAT,
    [IM_DOUBLE] = im_erode_progressive_n8_DOUBLE
};


static int im_reconstruct_progressive(ip_image *marker, ip_image *mask, int flag)
{
    ip_image *mask_T;
    int iterating;
    int from_above;
    improc_function_i_II recon_progressive;

    from_above = flag & FLG_RECON_FROM_ABOVE;
    ip_error = OK;
    mask_T = NULL;
    recon_progressive = NULL;

    if (from_above) {
	if ((flag & FLG_BITS_STREL) == IP_STREL_N8)
	    recon_progressive = im_erode_progressive_n8_bytype[marker->type];
	else
	    recon_progressive = im_erode_progressive_n4_bytype[marker->type];
    }else{
	if ((flag & FLG_BITS_STREL) == IP_STREL_N8)
	    recon_progressive = im_dilate_progressive_n8_bytype[marker->type];
	else
	    recon_progressive = im_dilate_progressive_n4_bytype[marker->type];
    }
    if (! recon_progressive) {
	ip_log_error(ERR_NOT_IMPLEMENTED);
	goto irp_exit;
    }

    iterating = TRUE;
    ip_image *copy = NULL;
    while (iterating) {
	ip_reconstruct_iterations++;
	if ((ip_reconstruct_iterations & 0x3ff) == 0) {
	    copy = im_copy(marker);
	}
	iterating = recon_progressive(marker, mask);
	if (iterating && copy) {
	    /*
	     * TBD:
	     * Currently here to terminate this loop while bugs might still
	     * be present.
	     */
	    if (!im_differ(copy, marker)) {
		ip_log_error(ERR_ALGORITHM_FAULT,
			     "WARNING: Reconstruction converged but not detected; aborting.\n");
		iterating = 0;
	    }
	    ip_free_image(copy);
	    copy = NULL;
	}
    }

 irp_exit:
    if (mask_T)
	ip_free_image(mask_T);
    return ip_error;
}



/*

NAME:

   im_reconstruct()

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/morph.h>

   int im_reconstruct( marker, mask , flags )
   ip_image *marker;
   ip_image *mask;
   int flags;

DESCRIPTION:

   Geodesic reconstruction of the image 'marker' from above or below subject
   to the image 'mask'.  By default reconstruction from below is performed
   (the conventional morphological reconstruction) but if
   FLG_RECON_FROM_ABOVE is past then reconstruction from above is performed.

   For reconstruction from below the image 'mask' must be superior to
   'marker', namely for each pixel $f_i$ in 'marker', the corresponding
   pixel $g_i$ in 'mask' must satisfy $g_i \ge p_i$ for correct operation.

   For reconstruction from above the image 'mask' must be inferior to
   'marker', namely for each pixel $f_i$ in 'marker', the corresponding
   pixel $g_i$ in 'mask' must satisfy $g_i \le p_i$ for correct operation.

   No check is made that 'mask' is superior (or inferior if reconstructing
   from above) to 'marker' and the result is undefined if this condition is
   not satisfied.

   The argument 'flags' should be one of FLG_NONE (for the default
   reconstructon from below) or FLG_RECON_FROM_ABOVE.

   The structuring element for the (effective) repeated dilation (or erosion
   if from above) defaults to the four nearest neighbours of the pixel.
   Bitwise OR 'flags' with FLG_EIGHT_NEIGHBOUR (or FLG_N8 for short) to use
   the eight neighbours in the (effective) repeated dilations (or erosions).

   On normal exit 'marker' contains the morphological reconstruction.

ERRORS:

   ERR_NOT_SAME_SIZE
   ERR_NOT_SAME_TYPE
   ERR_BAD_FLAG

*/

int im_reconstruct(ip_image *marker, ip_image *mask, int flag)
{
    int retcode;
    int strel;

    ip_log_routine(__func__);

    if (! image_valid(marker) || ! image_valid(mask))
	return ip_error;

    if (! images_compatible(marker, mask))
	return ip_error;

    ip_reconstruct_iterations = 0;

    strel = flag & FLG_BITS_STREL;
    flag &= ~FLG_BITS_STREL;

    if (NOT ip_flag_ok(flag, FLG_RECON_FROM_ABOVE, LASTFLAG))
	return ip_error;

    if (NOT ip_flag_ok(strel, IP_STREL_N4, IP_STREL_N8, LASTFLAG))
	return ip_error;

    flag |= strel;

    retcode = im_reconstruct_progressive(marker, mask, flag);

    ip_pop_routine();
    return retcode;
}
