/*
   Module: ip/arith.c
   Author: M.J.Cree

   Binary arithmetic operators between two images.
   See arithc.c for binary arithmetic between an image and a constant.

   Copyright (C) 1995-2013, 2015, 2017 Michael J. Cree

ENTRY POINTS:

   im_add()
   im_sub()
   im_mul()
   im_div()

*/

#include <stdlib.h>
#include <math.h>

#include <ip/ip.h>
#include <ip/complex.h>

#include "internal.h"
#include "utils.h"
#include "function_ops.h"

#ifdef HAVE_SIMD
#include "simd/arith_simd.h"
#endif


/*
 * Macros to implement poor man's SIMD.  These provide faster addition and
 * subtraction on BYTE and SHORT orientated images particularly on 64 bit
 * arches.  Performance gains may be less impressive (maybe even losses?) on
 * 32 bit arches.
 *
 * Method one inserts zero bits at the MSB of each pixel and calculates the
 * result without the sign bit to avoid the carry bit polluting the
 * neighbouring pixel, then ORs back at the end the result of calculating
 * the sign bit.
 *
 * Method two splits the calculation into two calculations, one over the odd
 * pixels and the other over even pixels.  There are plenty of zeros between
 * pixels to avoid pollution due to a carry bit.  The two calculations are
 * ORed back to gether at the end.

 * The first method seems to be the fastest on most architectures (but not
 * so on PPC G4).  We therefore default to Method 1.
 */
#define USE_FAST_CODE_METHOD_1


#ifdef USE_FAST_CODE_METHOD_1
/* First method */
#define IMROW_FAST_ADD(d, s, row, len)		\
    unsigned long *sptr, *dptr;			\
    sptr = IM_ROWADR(s, row, v);		\
    dptr = IM_ROWADR(d, row, v);		\
    for (int i=0; i<len; i++) {			\
	unsigned long sc = *sptr++, dc = *dptr;	\
	unsigned long signs = (sc ^ dc) & m1;	\
	dc &= m2;				\
	sc &= m2;				\
        *dptr++ = (dc + sc) ^ signs;		\
    }

#define IMROW_FAST_SUB(d, s, row, len)		\
    unsigned long *sptr, *dptr;			\
    sptr = IM_ROWADR(s, row, v);		\
    dptr = IM_ROWADR(d, row, v);		\
    for (int i=0; i<len; i++) {			\
	unsigned long sc = *sptr++, dc = *dptr;	\
	unsigned long signs = (sc ^ dc ^ m1) & m1;	\
	dc |= m1;				\
	sc &= m2;				\
        *dptr++ = (dc - sc) ^ signs;		\
    }

#else
/* Second method */
#define IMROW_FAST_ADD(d, s, row, len)		\
    unsigned long *sptr, *dptr;			\
    unsigned long w1, w2;			\
    unsigned long a1, a2;			\
    sptr = IM_ROWADR(s, row, v);		\
    dptr = IM_ROWADR(d, row, v);		\
    for (int i=0; i<len; i++) {			\
        w1 = w2 = *dptr;			\
	a1 = a2 = *sptr++;			\
	w1 &= m1;				\
	w2 &= m2;				\
	a1 &= m1;				\
	a2 &= m2;				\
	w1 += a1;				\
	w2 += a2;				\
	w1 &= m1;				\
	w2 &= m2;				\
	*dptr++ = w1 | w2;			\
    }

#define IMROW_FAST_SUB(d, s, row, len)		\
    unsigned long *sptr, *dptr;			\
    unsigned long w1, w2;			\
    unsigned long a1, a2;			\
    sptr = IM_ROWADR(s, row, v);		\
    dptr = IM_ROWADR(d, row, v);		\
    for (int i=0; i<len; i++) {			\
        w1 = w2 = *dptr;			\
	a1 = a2 = *sptr++;			\
	w1 |= m2;				\
	w2 |= m1;				\
	a1 &= m1;				\
	a2 &= m2;				\
	w1 -= a1;				\
	w2 -= a2;				\
	w1 &= m1;				\
	w2 &= m2;				\
	*dptr++ = w1 | w2;			\
    }
#endif


/*
 * Macro to process image pixelwise (PROCESS_IMAGE).
 *
 * The length of a row is calculated from the dest image (argument d) type
 * and the processing type (argument t).  This allows, for example, to
 * calculate COMPLEX addition by doing a FLOAT addition on image rows of
 * doubled length as they are the same thing.
 */

#define PROCESS_IMAGE(d, s, t, op) {					\
	int rowlen = ip_type_size(d->type) / ip_type_size(IM_ ## t);	\
	rowlen *= d->size.x;						\
	for (int j=0; j<d->size.y; j++) {				\
	    t ## _type *dptr = IM_ROWADR(d, j, v);			\
	    t ## _type *sptr = IM_ROWADR(s, j, v);			\
	    for (int i=0; i<rowlen; i++) {				\
		*dptr = pixel_ ## op ## _ ## t(*dptr, *sptr);		\
		dptr++; sptr++;						\
	    }								\
	}								\
    }


/*
 * Defines a calleable function that completely processes an image pair of
 * the same type (i.e. destination and source with result left in
 * destination) with a specified pixel operator.
 */
#define generate_image_same_function(type, operator)			\
    static void								\
    im_ ## operator ## _same_ ## type(ip_image *d, ip_image *s, int f)	\
    {									\
	PROCESS_IMAGE(d, s, type, operator);				\
    }

/*
 * For defining a static inline function being a pixel operator to use in
 * generate_image_same_function() above.
 */
#define generate_pixel_op(type, opname, op)			    \
    static inline type ## _type					    \
    pixel_ ## opname ## _ ## type(type ## _type d, type ## _type s) \
    {								    \
	return (op);						    \
    }



/*
 * Support functions/macros used.  First the pixel operators.
 */

/* Addition operator for each pixel type */
generate_pixel_op(UBYTE, add, d + s)
generate_pixel_op(USHORT, add, d + s)
generate_pixel_op(ULONG, add, d + s)
generate_pixel_op(FLOAT, add, d + s)
generate_pixel_op(DOUBLE, add, d + s)


/* Clipped (saturated) addition operator for each pixel type */
static inline uint8_t pixel_add_clipped_UBYTE(uint8_t d, uint8_t s)
{
    int res = (int)d + (int)s;
    return (uint8_t)((res > 255) ? 255 : res);
}

static inline int8_t pixel_add_clipped_BYTE(int8_t d, int8_t s)
{
    int res = (int)d + (int)s;
    if (res < INT8_MIN) res = INT8_MIN;
    if (res > INT8_MAX) res = INT8_MAX;
    return (int8_t)res;
}

static inline uint16_t pixel_add_clipped_USHORT(uint16_t d, uint16_t s)
{
    unsigned int res = (unsigned int)d + (unsigned int)s;
    return (uint16_t)((res > UINT16_MAX) ? UINT16_MAX : res);
}

static inline int16_t pixel_add_clipped_SHORT(int16_t d, int16_t s)
{
    int res = (int)d + (int)s;
    if (res < INT16_MIN) res = INT16_MIN;
    if (res > INT16_MAX) res = INT16_MAX;
    return (int16_t)res;
}

/* Subtraction operator for each pixel type */
generate_pixel_op(UBYTE, sub, d - s)
generate_pixel_op(USHORT, sub, d - s)
generate_pixel_op(ULONG, sub, d - s)
generate_pixel_op(FLOAT, sub, d - s)
generate_pixel_op(DOUBLE, sub, d - s)


/* Clipped (saturated) subtraction operator for each pixel type */
static inline uint8_t pixel_sub_clipped_UBYTE(uint8_t d, uint8_t s)
{
    int res = (int)d - (int)s;
    return (uint8_t)((res < 0) ? 0 : res);
}

static inline int8_t pixel_sub_clipped_BYTE(int8_t d, int8_t s)
{
    int res = (int)d - (int)s;
    if (res < INT8_MIN) res = INT8_MIN;
    if (res > INT8_MAX) res = INT8_MAX;
    return (int8_t)res;
}

static inline uint16_t pixel_sub_clipped_USHORT(uint16_t d, uint16_t s)
{
    int res = (int)d - (int)s;
    return (uint16_t)((res < 0) ? 0 : res);
}

static inline int16_t pixel_sub_clipped_SHORT(int16_t d, int16_t s)
{
    int res = (int)d - (int)s;
    if (res < INT16_MIN) res = INT16_MIN;
    if (res > INT16_MAX) res = INT16_MAX;
    return (int16_t)res;
}


/* Multiplication operators for each image type */
generate_pixel_op(UBYTE, mul, d * s)
generate_pixel_op(USHORT, mul, d * s)
generate_pixel_op(ULONG, mul, d * s)
generate_pixel_op(FLOAT, mul, d * s)
generate_pixel_op(DOUBLE, mul, d * s)
generate_pixel_op(COMPLEX, mul, ip_complex_mul(d,s))
generate_pixel_op(DCOMPLEX, mul, ip_dcomplex_mul(d,s))


/* Clipped (saturated) multiplication operator for each pixel type */

static inline uint8_t pixel_mul_clipped_UBYTE(uint8_t d, uint8_t s)
{
    int res = (int)d * (int)s;
    return (uint8_t)((res > 255) ? 255 : res);
}

static inline int8_t pixel_mul_clipped_BYTE(int8_t d, int8_t s)
{
    int res = (int)d * (int)s;
    if (res < INT8_MIN) res = INT8_MIN;
    if (res > INT8_MAX) res = INT8_MAX;
    return res;
}

static inline uint16_t pixel_mul_clipped_USHORT(uint16_t d, uint16_t s)
{
    uint32_t res = (uint32_t)d * (uint32_t)s;
    return (uint16_t)((res > UINT16_MAX) ? UINT16_MAX : res);
}

static inline int16_t pixel_mul_clipped_SHORT(int16_t d, int16_t s)
{
    int res = (int)d * (int)s;
    if (res < INT16_MIN) res = INT16_MIN;
    if (res > INT16_MAX) res = INT16_MAX;
    return (int16_t)res;
}

/* Division operators for each pixel type */
generate_pixel_op(UBYTE, div, d / s)
generate_pixel_op(BYTE, div, d / s)
generate_pixel_op(USHORT, div, d / s)
generate_pixel_op(SHORT, div, d / s)
generate_pixel_op(ULONG, div, d / s)
generate_pixel_op(LONG, div, d / s)
generate_pixel_op(FLOAT, div, d / s)
generate_pixel_op(DOUBLE, div, d / s)
generate_pixel_op(COMPLEX, div, ip_complex_div(d,s))
generate_pixel_op(DCOMPLEX, div, ip_dcomplex_div(d,s))


/* division protected (i.e. avoids divide by zero) pixel operators */
generate_pixel_op(UBYTE, div_protected, (s==0) ? (d==0 ? 0 : 255) : d/s)
generate_pixel_op(BYTE, div_protected, (s==0) ? (d==0 ? 0 : (d>0 ? INT8_MAX : INT8_MIN)) : d/s)
generate_pixel_op(USHORT, div_protected, (s==0) ? (d==0 ? 0 : UINT16_MAX) : d/s)
generate_pixel_op(SHORT, div_protected, (s==0) ? (d==0 ? 0 : (d>0 ? INT16_MAX : INT16_MIN)) : d/s)
generate_pixel_op(ULONG, div_protected, (s==0) ? (d==0 ? 0 : UINT32_MAX) : d/s)
generate_pixel_op(LONG, div_protected, (s==0) ? (d==0 ? 0 : (d>0 ? INT32_MAX : INT32_MIN)) : d/s)
generate_pixel_op(FLOAT, div_protected, (s==0) ? (d==0 ? NAN : (d>0 ? INFINITY : -INFINITY)) : d/s)
generate_pixel_op(DOUBLE, div_protected, (s==0) ? (d==0 ? NAN : (d>0 ? INFINITY : -INFINITY)) : d/s)


/* Helpers for mixed operator types */
#define DORED(f) ((f) & FLG_SRED)
#define DOGREEN(f) ((f) & FLG_SGREEN)
#define DOBLUE(f) ((f) & FLG_SBLUE)
#define DOALPHA(f) ((f) & FLG_SALPHA)
#define DOREAL(f) ((f) & FLG_SREAL)
#define DOIMAG(f) ((f) & FLG_SIMAG)


static int process_images_by_operator(ip_image *dest, ip_image *src, int flag,
				      struct function_list *ops)
{
    int done = 0;

    if (dest->type == src->type) {
	/* Same typed arguments */
	if (flag & FLG_CLIP) {
	    /* saturated operator */
	    if (ops->op_clipped_same[dest->type]) {
		ops->op_clipped_same[dest->type](dest, src, flag);
		done = 1;
	    }
	}else{
	    /* operator */
	    if (ops->op_same[dest->type]) {
		ops->op_same[dest->type](dest, src, flag);
		done = 1;
	    }
	}
    }else{
	/* Mixed type arguments */
	if (flag & FLG_CLIP) {
	    /* saturated operator */
	    if (ops->op_clipped_mixed[dest->type]) {
		ops->op_clipped_mixed[dest->type](dest, src, flag);
		done = 1;
	    }
	}else{
	    /* operator */
	    if (ops->op_mixed[dest->type]) {
		ops->op_mixed[dest->type](dest, src, flag);
		done = 1;
	    }
	}
    }
    return done;
}


/*
 * im_add() helper functions.
 */

/*
 * Generate the functions:
 *    im_add_same_BYTE()
 *    im_add_same_SHORT()
 *    im_add_same_LONG()
 *    im_add_same_FLOAT()
 *    im_add_same_DOUBLE()
 *
 * Note that unsigned binary arithmetic is the same as signed two's
 * complement arithmetic so we don't need special functions for BYTE, etc.
 * We implement the unsigned variants in preference as C standards give a
 * guarantee that integer types wrap on overflow only for unsigned
 * arithmetic.
 */
generate_image_same_function(UBYTE, add)
generate_image_same_function(USHORT, add)
generate_image_same_function(ULONG, add)
generate_image_same_function(FLOAT, add)
generate_image_same_function(DOUBLE, add)

static improc_function_IIf add_same[IM_TYPEMAX] = {
    [IM_BYTE] = im_add_same_UBYTE,
    [IM_UBYTE] = im_add_same_UBYTE,
    [IM_SHORT] = im_add_same_USHORT,
    [IM_USHORT] = im_add_same_USHORT,
    [IM_LONG] = im_add_same_ULONG,
    [IM_ULONG] = im_add_same_ULONG,
    [IM_FLOAT] = im_add_same_FLOAT,
    [IM_DOUBLE] = im_add_same_DOUBLE,
    [IM_COMPLEX] = im_add_same_FLOAT,
    [IM_DCOMPLEX] = im_add_same_DOUBLE,
    [IM_RGB] = im_add_same_UBYTE,
    [IM_RGBA] = im_add_same_UBYTE,
    [IM_RGB16] = im_add_same_USHORT,
};


/*
 * Generate the functions:
 *    im_add_clipped_same_BYTE()
 *    im_add_clipped_same_UBYTE()
 *    im_add_clipped_same_SHORT()
 *    im_add_clipped_same_USHORT()
 */
generate_image_same_function(UBYTE, add_clipped)
generate_image_same_function(BYTE, add_clipped)
generate_image_same_function(USHORT, add_clipped)
generate_image_same_function(SHORT, add_clipped)

static improc_function_IIf add_clipped_same[IM_TYPEMAX] = {
    [IM_BYTE] = im_add_clipped_same_BYTE,
    [IM_UBYTE] = im_add_clipped_same_UBYTE,
    [IM_SHORT] = im_add_clipped_same_SHORT,
    [IM_USHORT] = im_add_clipped_same_USHORT,
    [IM_RGB] = im_add_clipped_same_UBYTE,
    [IM_RGBA] = im_add_clipped_same_UBYTE,
    [IM_RGB16] = im_add_clipped_same_USHORT,
};


/*
 * mixed image type additions
 * - as image type specific manually coded.
 */

static void im_add_mixed_RGB_with_UBYTE(ip_image *d, ip_image *s, int flag)
{
    for (int j=0; j<d->size.y; j++) {
	ip_rgb *dp;
	uint8_t *sp;
	dp = IM_ROWADR(d, j, v);
	sp = IM_ROWADR(s, j, v);
	for (int i=0; i<d->size.x; i++) {
	    if (DORED(flag))
		dp->r += *sp;
	    if (DOGREEN(flag))
		dp->g += *sp;
	    if (DOBLUE(flag))
		dp->b += *sp;
	    dp++; sp++;
	}
    }
}


static void im_add_mixed_RGBA_with_UBYTE(ip_image *d, ip_image *s, int flag)
{
    for (int j=0; j<d->size.y; j++) {
	ip_rgba *dp;
	uint8_t *sp;
	dp = IM_ROWADR(d, j, v);
	sp = IM_ROWADR(s, j, v);
	for (int i=0; i<d->size.x; i++) {
	    if (DORED(flag))
		dp->r += *sp;
	    if (DOGREEN(flag))
		dp->g += *sp;
	    if (DOBLUE(flag))
		dp->b += *sp;
	    if (DOALPHA(flag))
		dp->a += *sp;
	    dp++; sp++;
	}
    }
}


static void im_add_mixed_RGB16_with_USHORT(ip_image *d, ip_image *s, int flag)
{
    for (int j=0; j<d->size.y; j++) {
	ip_lrgb *dp;
	uint16_t *sp;
	dp = IM_ROWADR(d, j, v);
	sp = IM_ROWADR(s, j, v);
	for (int i=0; i<d->size.x; i++) {
	    if (DORED(flag))
		dp->r += *sp;
	    if (DOGREEN(flag))
		dp->g += *sp;
	    if (DOBLUE(flag))
		dp->b += *sp;
	    dp++; sp++;
	}
    }
}


static void im_add_mixed_COMPLEX_with_FLOAT(ip_image *d, ip_image *s, int flag)
{
    for (int j=0; j<d->size.y; j++) {
	float *sptr;
	ip_complex *dptr;
	dptr = IM_ROWADR(d,j,v);
	sptr = IM_ROWADR(s,j,v);
	for (int i=0; i<d->size.x; i++) {
	    if (DOREAL(flag))
		dptr->r += *sptr;
	    if (DOIMAG(flag))
		dptr->i += *sptr;
	    dptr++; sptr++;
	}
    }
}


static void im_add_mixed_DCOMPLEX_with_DOUBLE(ip_image *d, ip_image *s, int flag)
{
    for (int j=0; j<d->size.y; j++) {
	double *sptr;
	ip_dcomplex *dptr;
	dptr = IM_ROWADR(d,j,v);
	sptr = IM_ROWADR(s,j,v);
	for (int i=0; i<d->size.x; i++) {
	    if (DOREAL(flag))
		dptr->r += *sptr;
	    if (DOIMAG(flag))
		dptr->i += *sptr;
	    dptr++; sptr++;
	}
    }
}


static improc_function_IIf add_mixed[IM_TYPEMAX] = {
    [IM_RGB] = im_add_mixed_RGB_with_UBYTE,
    [IM_RGBA] = im_add_mixed_RGBA_with_UBYTE,
    [IM_RGB16] = im_add_mixed_RGB16_with_USHORT,
    [IM_COMPLEX] = im_add_mixed_COMPLEX_with_FLOAT,
    [IM_DCOMPLEX] = im_add_mixed_DCOMPLEX_with_DOUBLE,
};


/*
 * mixed image type additions with saturation
 * -  as rather image type specific manually coded.
 */

static void  im_add_clipped_mixed_RGB_with_UBYTE(ip_image *d, ip_image *s, int flag)
{
    for (int j=0; j<d->size.y; j++) {
	ip_rgb *dp;
	uint8_t *sp;
	dp = IM_ROWADR(d, j, v);
	sp = IM_ROWADR(s, j, v);
	for (int i=0; i<d->size.x; i++) {
	    if (DORED(flag))
		dp->r = pixel_add_clipped_UBYTE(dp->r, *sp);
	    if (DOGREEN(flag))
		dp->g = pixel_add_clipped_UBYTE(dp->g, *sp);
	    if (DOBLUE(flag))
		dp->b = pixel_add_clipped_UBYTE(dp->b, *sp);
	    dp++; sp++;
	}
    }
}

static void im_add_clipped_mixed_RGBA_with_UBYTE(ip_image *d, ip_image *s, int flag)
{
    for (int j=0; j<d->size.y; j++) {
	ip_rgba *dp;
	uint8_t *sp;
	dp = IM_ROWADR(d, j, v);
	sp = IM_ROWADR(s, j, v);
	for (int i=0; i<d->size.x; i++) {
	    if (DORED(flag))
		dp->r = pixel_add_clipped_UBYTE(dp->r, *sp);
	    if (DOGREEN(flag))
		dp->g = pixel_add_clipped_UBYTE(dp->g, *sp);
	    if (DOBLUE(flag))
		dp->b = pixel_add_clipped_UBYTE(dp->b, *sp);
	    if (DOALPHA(flag))
		dp->a = pixel_add_clipped_UBYTE(dp->a, *sp);
	    dp++; sp++;
	}
    }
}

static void im_add_clipped_mixed_RGB16_with_USHORT(ip_image *d, ip_image *s, int flag)
{
    for (int j=0; j<d->size.y; j++) {
	ip_lrgb *dp;
	uint16_t *sp;
	dp = IM_ROWADR(d, j, v);
	sp = IM_ROWADR(s, j, v);
	for (int i=0; i<d->size.x; i++) {
	    if (DORED(flag))
		dp->r = pixel_add_clipped_USHORT(dp->r, *sp);
	    if (DOGREEN(flag))
		dp->g = pixel_add_clipped_USHORT(dp->g, *sp);
	    if (DOBLUE(flag))
		dp->b = pixel_add_clipped_USHORT(dp->b, *sp);
	    dp++; sp++;
	}
    }
}


static improc_function_IIf add_clipped_mixed[IM_TYPEMAX] = {
    [IM_RGB] = im_add_clipped_mixed_RGB_with_UBYTE,
    [IM_RGBA] = im_add_clipped_mixed_RGBA_with_UBYTE,
    [IM_RGB16] = im_add_clipped_mixed_RGB16_with_USHORT
};




/*

NAME:

   im_add() \- Add two images together

PROTOTYPE:

   #include <ip/ip.h>

   int  im_add( dest, src, flag )
   ip_image *dest;
   ip_image *src;
   unsigned int flag;

DESCRIPTION:

   Add the image 'src' to the image 'dest' and return the result in 'dest'.
   The addition is carried out (effectively) in the same type as the
   image. So, for example, if the image type is IM_UBYTE and the pixels to
   be added are 10 and 250, then the result is 4 (if FLG_CLIP not set).
   Unsigned integer arithmetic is guaranteed to wrap on overflow (as
   illustrated in the previous sentence), however no such guarantee is given
   for signed arithmetic (due to the conventions of the C programming
   language).

   For BYTE and SHORT images, if 'flag' is set to FLG_CLIP then results are
   clipped (saturated) to the appropriate range. For example, if the pixel
   values 10 and 250 are added in an IM_UBYTE image the result is 255.

   Normally, images must be of the same type to be added, but exceptions
   exist for this routine.  The first is that 'dest' can be COMPLEX and
   'src' FLOAT, and 'flag' must be set to FLG_SREAL or FLG_SIMAG to specify
   whether the float image should be treated as the real or imaginary part
   of a complex image (and the other part is treated as zero).

   The second is that 'dest' can be a RGB type image (i.e. one of RGB, RGB16
   or RGBA) and 'src' a UBYTE or USHORT image.  Then flag must be set to one
   (or more by ORing) of FLG_SRED, FLG_SGREEN, FLG_SBLUE or FLG_SALPHA.  The
   UBYTE or USHORT image is added to the specified components of the colour
   image.

   This routine does not support addition of BINARY or PALETTE images.

ERRORS:

   ERR_NOT_SAME_SIZE
   ERR_NOT_SAME_TYPE
   ERR_BAD_TYPE
   ERR_BAD_FLAG

SEE ALSO:

   im_sub()     im_mul()     im_div()
   im_addc()    im_subc()    im_mulc()    im_divc()

*/

int im_add(ip_image *dest, ip_image *src, int flag)
{
    int retcode;
    struct function_list ops;
    int done = 0;

    ip_log_routine(__func__);

    if (NOT (image_valid(dest) && image_valid(src)))
	return ip_error;

    if ((dest->type == IM_COMPLEX && src->type == IM_FLOAT) ||
	(dest->type == IM_DCOMPLEX && src->type == IM_DOUBLE) ||
	((dest->type == IM_RGB || dest->type==IM_RGBA) && src->type == IM_UBYTE) ||
	(dest->type == IM_RGB16 && src->type == IM_USHORT) ) {
	if (NOT images_same_size(dest,src))
	    return ip_error;
    }else{
	if (NOT images_compatible(dest,src))
	    return ip_error;
    }

    if (image_bad_type(src, IM_PALETTE, IM_BINARY, -1))
	return ip_error;

    if ((flag & FLG_CLIP) && (NOT (type_clrim(dest->type) || dest->type == IM_BYTE
				   || dest->type == IM_UBYTE || dest->type == IM_SHORT
				   || dest->type == IM_USHORT))) {
	ip_log_error(ERR_BAD_FLAG);
	ip_pop_routine();
	return ip_error;
    }

    if (type_complex(dest->type) && NOT type_complex(src->type)) {

	/* COMPLEX or DCOMPLEX with a floating point addend */
	if (NOT ip_flag_bits_unused(flag,  ~(FLG_SREAL|FLG_SIMAG)))
	    return ip_error;
	/* At least one relevant flag must be set */
	if (!DOREAL(flag) && !DOIMAG(flag)) {
	    ip_log_error(ERR_BAD_FLAG);
	    ip_pop_routine();
	    return ip_error;
	}

    }else if (type_clrim(dest->type) && dest->type != IM_PALETTE) {

	/* RGB, RGBA (or RGB16) with a UBYTE (or USHORT) addend */
	if (NOT ip_flag_bits_unused(flag, ~(FLGPRT_COLOUR|FLG_CLIP)))
	    return ip_error;
	/* At least one relevant flag must be set */
	if ((src->type == IM_UBYTE || src->type == IM_USHORT)
	    && (! (DOALPHA(flag) | DORED(flag) | DOGREEN(flag) | DOBLUE(flag))
		|| (dest->type != IM_RGBA && DOALPHA(flag)))) {
	    ip_log_error(ERR_BAD_FLAG);
	    ip_pop_routine();
	    return ip_error;
	}

    }else{

	/* scalar image types including palette */
	if (NOT ip_flag_ok(flag, FLG_CLIP, LASTFLAG))
	    return ip_error;

    }

    /* Flags and image types have been verified correct at this stage */

#ifdef HAVE_SIMD

    if (IP_USE_HWACCEL) {
	ops.op_same = ip_simd_add_same;
	ops.op_clipped_same = ip_simd_add_clipped_same;
	ops.op_mixed = ip_simd_add_mixed;
	ops.op_clipped_mixed = ip_simd_add_clipped_mixed;

	done = process_images_by_operator(dest, src, flag, &ops);
    }

    if (done) {
	ip_pop_routine();
	return OK;
    }

#endif

    /*
     * If get here then no hardware/SIMD acceleration for this image type.
     * Try packing multiple bytes/shorts into a long with some zero bits in
     * between to protect against spilling of the carry bit.  In this way we
     * can do a poor man's SIMD add, which, particularly for byte and short
     * images on a 64 bit arch, gives some speed up.
     */

    if (IP_USE_FAST_CODE && flag == FLG_NONE
	     && (dest->type == src->type) && fast_code_masks[dest->type].mask_even) {
	int length;
	unsigned long m1, m2;

#ifdef USE_FAST_CODE_METHOD_1
	m1 = fast_code_masks[src->type].sign_bits;
	m2 = ~m1;
#else
	m1 = fast_code_masks[src->type].mask_even;
	m2 = fast_code_masks[src->type].mask_odd;
#endif

	length = containers_in_padded_row(dest, sizeof(unsigned long));

	for (int j=0; j<dest->size.y; j++) {
	    IMROW_FAST_ADD(dest, src, j, length);
	}

	ip_pop_routine();
	return OK;
    }

    /*
     * We are running out of clever tricks to speed up processing so we
     * resort to basic pixel by pixel processing.
     */

    ops.op_same = add_same;
    ops.op_clipped_same = add_clipped_same;
    ops.op_mixed = add_mixed;
    ops.op_clipped_mixed = add_clipped_mixed;

    done = process_images_by_operator(dest, src, flag, &ops);

    retcode = OK;
    if (! done) {
	ip_log_error(ERR_NOT_IMPLEMENTED);
	retcode = ip_error;
    }

    ip_pop_routine();
    return retcode;
}


/*
 * im_sub() helper functions
 */

/*
 * Generate the functions:
 *    im_sub_same_UBYTE()
 *    im_sub_same_USHORT()
 *    im_sub_same_ULONG()
 *    im_sub_same_FLOAT()
 *    im_sub_same_DOUBLE()
 */
generate_image_same_function(UBYTE, sub)
generate_image_same_function(USHORT, sub)
generate_image_same_function(ULONG, sub)
generate_image_same_function(FLOAT, sub)
generate_image_same_function(DOUBLE, sub)

static improc_function_IIf sub_same[IM_TYPEMAX] = {
    [IM_BYTE] = im_sub_same_UBYTE,
    [IM_UBYTE] = im_sub_same_UBYTE,
    [IM_SHORT] = im_sub_same_USHORT,
    [IM_USHORT] = im_sub_same_USHORT,
    [IM_LONG] = im_sub_same_ULONG,
    [IM_ULONG] = im_sub_same_ULONG,
    [IM_FLOAT] = im_sub_same_FLOAT,
    [IM_DOUBLE] = im_sub_same_DOUBLE,
    [IM_COMPLEX] = im_sub_same_FLOAT,
    [IM_DCOMPLEX] = im_sub_same_DOUBLE,
    [IM_RGB] = im_sub_same_UBYTE,
    [IM_RGBA] = im_sub_same_UBYTE,
    [IM_RGB16] = im_sub_same_USHORT,
};


/*
 * Generate the functions:
 *    im_sub_clipped_same_BYTE()
 *    im_sub_clipped_same_UBYTE()
 *    im_sub_clipped_same_SHORT()
 *    im_sub_clipped_same_USHORT()
 */
generate_image_same_function(UBYTE, sub_clipped)
generate_image_same_function(BYTE, sub_clipped)
generate_image_same_function(USHORT, sub_clipped)
generate_image_same_function(SHORT, sub_clipped)

static improc_function_IIf sub_clipped_same[IM_TYPEMAX] = {
    [IM_BYTE] = im_sub_clipped_same_BYTE,
    [IM_UBYTE] = im_sub_clipped_same_UBYTE,
    [IM_SHORT] = im_sub_clipped_same_SHORT,
    [IM_USHORT] = im_sub_clipped_same_USHORT,
    [IM_RGB] = im_sub_clipped_same_UBYTE,
    [IM_RGBA] = im_sub_clipped_same_UBYTE,
    [IM_RGB16] = im_sub_clipped_same_USHORT,
};


/*
 * mixed image type additions
 * - as image type specific manually coded.
 */

static void im_sub_mixed_RGB_with_UBYTE(ip_image *d, ip_image *s, int flag)
{
    for (int j=0; j<d->size.y; j++) {
	ip_rgb *dp;
	uint8_t *sp;
	dp = IM_ROWADR(d, j, v);
	sp = IM_ROWADR(s, j, v);
	for (int i=0; i<d->size.x; i++) {
	    if (DORED(flag))
		dp->r -= *sp;
	    if (DOGREEN(flag))
		dp->g -= *sp;
	    if (DOBLUE(flag))
		dp->b -= *sp;
	    dp++; sp++;
	}
    }
}


static void im_sub_mixed_RGBA_with_UBYTE(ip_image *d, ip_image *s, int flag)
{
    for (int j=0; j<d->size.y; j++) {
	ip_rgba *dp;
	uint8_t *sp;
	dp = IM_ROWADR(d, j, v);
	sp = IM_ROWADR(s, j, v);
	for (int i=0; i<d->size.x; i++) {
	    if (DORED(flag))
		dp->r -= *sp;
	    if (DOGREEN(flag))
		dp->g -= *sp;
	    if (DOBLUE(flag))
		dp->b -= *sp;
	    if (DOALPHA(flag))
		dp->a -= *sp;
	    dp++; sp++;
	}
    }
}


static void im_sub_mixed_RGB16_with_USHORT(ip_image *d, ip_image *s, int flag)
{
    for (int j=0; j<d->size.y; j++) {
	ip_lrgb *dp;
	uint16_t *sp;
	dp = IM_ROWADR(d, j, v);
	sp = IM_ROWADR(s, j, v);
	for (int i=0; i<d->size.x; i++) {
	    if (DORED(flag))
		dp->r -= *sp;
	    if (DOGREEN(flag))
		dp->g -= *sp;
	    if (DOBLUE(flag))
		dp->b -= *sp;
	    dp++; sp++;
	}
    }
}


static void im_sub_mixed_COMPLEX_with_FLOAT(ip_image *d, ip_image *s, int flag)
{
    for (int j=0; j<d->size.y; j++) {
	float *sptr;
	ip_complex *dptr;
	dptr = IM_ROWADR(d,j,v);
	sptr = IM_ROWADR(s,j,v);
	for (int i=0; i<d->size.x; i++) {
	    if (DOREAL(flag))
		dptr->r -= *sptr;
	    if (DOIMAG(flag))
		dptr->i -= *sptr;
	    dptr++; sptr++;
	}
    }
}


static void im_sub_mixed_DCOMPLEX_with_DOUBLE(ip_image *d, ip_image *s, int flag)
{
    for (int j=0; j<d->size.y; j++) {
	double *sptr;
	ip_dcomplex *dptr;
	dptr = IM_ROWADR(d,j,v);
	sptr = IM_ROWADR(s,j,v);
	for (int i=0; i<d->size.x; i++) {
	    if (DOREAL(flag))
		dptr->r -= *sptr;
	    if (DOIMAG(flag))
		dptr->i -= *sptr;
	    dptr++; sptr++;
	}
    }
}


static improc_function_IIf sub_mixed[IM_TYPEMAX] = {
    [IM_RGB] = im_sub_mixed_RGB_with_UBYTE,
    [IM_RGBA] = im_sub_mixed_RGBA_with_UBYTE,
    [IM_RGB16] = im_sub_mixed_RGB16_with_USHORT,
    [IM_COMPLEX] = im_sub_mixed_COMPLEX_with_FLOAT,
    [IM_DCOMPLEX] = im_sub_mixed_DCOMPLEX_with_DOUBLE,
};


/*
 * mixed image type additions with saturation
 * -  as rather image type specific manually coded.
 */

static void im_sub_clipped_mixed_RGB_with_UBYTE(ip_image *d, ip_image *s, int flag)
{
    for (int j=0; j<d->size.y; j++) {
	ip_rgb *dp;
	uint8_t *sp;
	dp = IM_ROWADR(d, j, v);
	sp = IM_ROWADR(s, j, v);
	for (int i=0; i<d->size.x; i++) {
	    if (DORED(flag))
		dp->r = pixel_sub_clipped_UBYTE(dp->r, *sp);
	    if (DOGREEN(flag))
		dp->g = pixel_sub_clipped_UBYTE(dp->g, *sp);
	    if (DOBLUE(flag))
		dp->b = pixel_sub_clipped_UBYTE(dp->b, *sp);
	    dp++; sp++;
	}
    }
}

static void im_sub_clipped_mixed_RGBA_with_UBYTE(ip_image *d, ip_image *s, int flag)
{
    for (int j=0; j<d->size.y; j++) {
	ip_rgba *dp;
	uint8_t *sp;
	dp = IM_ROWADR(d, j, v);
	sp = IM_ROWADR(s, j, v);
	for (int i=0; i<d->size.x; i++) {
	    if (DORED(flag))
		dp->r = pixel_sub_clipped_UBYTE(dp->r, *sp);
	    if (DOGREEN(flag))
		dp->g = pixel_sub_clipped_UBYTE(dp->g, *sp);
	    if (DOBLUE(flag))
		dp->b = pixel_sub_clipped_UBYTE(dp->b, *sp);
	    if (DOALPHA(flag))
		dp->a = pixel_sub_clipped_UBYTE(dp->a, *sp);
	    dp++; sp++;
	}
    }
}

static void im_sub_clipped_mixed_RGB16_with_USHORT(ip_image *d, ip_image *s, int flag)
{
    for (int j=0; j<d->size.y; j++) {
	ip_lrgb *dp;
	uint16_t *sp;
	dp = IM_ROWADR(d, j, v);
	sp = IM_ROWADR(s, j, v);
	for (int i=0; i<d->size.x; i++) {
	    if (DORED(flag))
		dp->r = pixel_sub_clipped_USHORT(dp->r, *sp);
	    if (DOGREEN(flag))
		dp->g = pixel_sub_clipped_USHORT(dp->g, *sp);
	    if (DOBLUE(flag))
		dp->b = pixel_sub_clipped_USHORT(dp->b, *sp);
	    dp++; sp++;
	}
    }
}


static improc_function_IIf sub_clipped_mixed[IM_TYPEMAX] = {
    [IM_RGB] = im_sub_clipped_mixed_RGB_with_UBYTE,
    [IM_RGBA] = im_sub_clipped_mixed_RGBA_with_UBYTE,
    [IM_RGB16] = im_sub_clipped_mixed_RGB16_with_USHORT
};




/*

NAME:

   im_sub() \- Subtract an image fron amother image

PROTOTYPE:

   #include <ip/ip.h>

   int  im_sub( dest, src, flag )
   ip_image *dest;
   ip_image *src;
   int flag;

DESCRIPTION:

   Subtract the image 'src' from the image 'dest' and return the result in
   'dest'.  The subtraction is carried out (effectively) in the same type as
   the image. So, for example, if the image type is IM_UBYTE and if 20 is to
   be subtracted from 10, then the result is 246.

   For UBYTE and USHORT images, if 'flag' is set to FLG_CLIP then integer
   subtraction is used and and the result is clipped (saturated) to the
   limits of the pixel data type. For example, if 20 is subtracted from 10
   in a UBYTE image the result is clipped to 0.

   Normally, images must be of the same type to be subtracted, but an
   exception is that 'dest' can be COMPLEX (or DCOMPLEX) and 'src' FLOAT (or
   respectively DOUBLE), and 'flag' must be set to FLG_SREAL and/or
   FLG_SIMAG to specify whether the float image should be treated as the
   real or imaginery part of a complex image (with the other part treated as
   zero).

   Also the subtraction of a UBYTE image from RGB and RGBA images is
   supported, as is the subtraction of a USHORT image from an RGB16 image.
   Use the flags FLG_SRED, FLG_SGREEN, FLG_SBLUE and FLG_SALPHA to specify
   components of RGB image to be subtracted from.

ERRORS:

   ERR_NOT_SAME_SIZE
   ERR_NOT_SAME_TYPE
   ERR_BAD_TYPE
   ERR_BAD_FLAG
   ERR_NO_*_SUPPORT

SEE ALSO:

   im_add()     im_mul()     im_div()
   im_addc()    im_subc()    im_mulc()    im_divc()

*/

int im_sub(ip_image *dest, ip_image *src, int flag)
{
    int retcode;
    struct function_list ops;
    int done = 0;

    ip_log_routine(__func__);

    if (NOT (image_valid(dest) && image_valid(src)))
	return ip_error;

    if ((dest->type == IM_COMPLEX && src->type == IM_FLOAT) ||
	(dest->type == IM_DCOMPLEX && src->type == IM_DOUBLE) ||
	((dest->type == IM_RGB || dest->type==IM_RGBA) && src->type == IM_UBYTE) ||
	(dest->type == IM_RGB16 && src->type == IM_USHORT) ) {
	if (NOT images_same_size(dest,src))
	    return ip_error;
    }else{
	if (NOT images_compatible(dest,src))
	    return ip_error;
    }

    if (image_bad_type(src, IM_PALETTE, IM_BINARY, -1))
	return ip_error;

    if ((flag & FLG_CLIP) && (NOT (type_clrim(dest->type) || dest->type == IM_BYTE
				   || dest->type == IM_UBYTE || dest->type == IM_SHORT
				   || dest->type == IM_USHORT))) {
	ip_log_error(ERR_BAD_FLAG);
	ip_pop_routine();
	return ip_error;
    }

    if (type_complex(dest->type) && NOT type_complex(src->type)) {

	/* COMPLEX or DCOMPLEX with a floating point addend */
	if (NOT ip_flag_bits_unused(flag,  ~(FLG_SREAL|FLG_SIMAG)))
	    return ip_error;
	/* At least one relevant flag must be set */
	if (!DOREAL(flag) && !DOIMAG(flag)) {
	    ip_log_error(ERR_BAD_FLAG);
	    ip_pop_routine();
	    return ip_error;
	}

    }else if (type_clrim(dest->type) && dest->type != IM_PALETTE) {

	/* RGB, RGBA (or RGB16) with a UBYTE (or USHORT) addend */
	if (NOT ip_flag_bits_unused(flag, ~(FLGPRT_COLOUR|FLG_CLIP)))
	    return ip_error;
	/* At least one relevant flag must be set */
	if ((src->type == IM_UBYTE || src->type == IM_USHORT)
	    && (! (DOALPHA(flag) | DORED(flag) | DOGREEN(flag) | DOBLUE(flag))
		|| (dest->type != IM_RGBA && DOALPHA(flag)))) {
	    ip_log_error(ERR_BAD_FLAG);
	    ip_pop_routine();
	    return ip_error;
	}

    }else{

	/* scalar image types including palette */
	if (NOT ip_flag_ok(flag, FLG_CLIP, LASTFLAG))
	    return ip_error;

    }


    /* Flags have been verified correct at this stage */

#ifdef HAVE_SIMD

    if (IP_USE_HWACCEL) {
	ops.op_same = ip_simd_sub_same;
	ops.op_clipped_same = ip_simd_sub_clipped_same;
	ops.op_mixed = ip_simd_sub_mixed;
	ops.op_clipped_mixed = ip_simd_sub_clipped_mixed;

	done = process_images_by_operator(dest, src, flag, &ops);
    }

    if (done) {
	ip_pop_routine();
	return OK;
    }

#endif

    /*
     * If get here then no hardware/SIMD acceleration for this image type.
     * Try packing multiple bytes/shorts into a long with some set bits in
     * between to protect against subtraction underflow spilling a carry
     * bit.  In this way we can do a poor man's SIMD subtraction, which,
     * particularly for byte and short images on a 64 bit arch, gives some
     * speed up.
     */

    if (IP_USE_FAST_CODE && fast_code_masks[dest->type].mask_even) {
	int length;
	unsigned long m1, m2;

#ifdef USE_FAST_CODE_METHOD_1
	m1 = fast_code_masks[src->type].sign_bits;
	m2 = ~m1;
#else
	m1 = fast_code_masks[src->type].mask_even;
	m2 = fast_code_masks[src->type].mask_odd;
#endif

	length = containers_in_padded_row(dest, sizeof(unsigned long));

	for (int j=0; j<dest->size.y; j++) {
	    IMROW_FAST_SUB(dest, src, j, length);
	}

	ip_pop_routine();
	return OK;
    }

    /*
     * We are running out of clever tricks to speed up processing so we
     * now resort to basic pixel by pixel processing.
     */

    ops.op_same = sub_same;
    ops.op_clipped_same = sub_clipped_same;
    ops.op_mixed = sub_mixed;
    ops.op_clipped_mixed = sub_clipped_mixed;

    done = process_images_by_operator(dest, src, flag, &ops);

    retcode = OK;
    if (! done) {
	ip_log_error(ERR_NOT_IMPLEMENTED);
	retcode = ip_error;
    }

    ip_pop_routine();
    return retcode;
}



/*
 * im_mul() helper routines
 */

generate_image_same_function(UBYTE, mul)
generate_image_same_function(USHORT, mul)
generate_image_same_function(ULONG, mul)
generate_image_same_function(FLOAT, mul)
generate_image_same_function(DOUBLE, mul)
generate_image_same_function(COMPLEX, mul)
generate_image_same_function(DCOMPLEX, mul)

static improc_function_IIf mul_same[IM_TYPEMAX] = {
    [IM_BYTE] = im_mul_same_UBYTE,
    [IM_UBYTE] = im_mul_same_UBYTE,
    [IM_SHORT] = im_mul_same_USHORT,
    [IM_USHORT] = im_mul_same_USHORT,
    [IM_LONG] = im_mul_same_ULONG,
    [IM_ULONG] = im_mul_same_ULONG,
    [IM_FLOAT] = im_mul_same_FLOAT,
    [IM_DOUBLE] = im_mul_same_DOUBLE,
    [IM_COMPLEX] = im_mul_same_COMPLEX,
    [IM_DCOMPLEX] = im_mul_same_DCOMPLEX,
    [IM_RGB] = im_mul_same_UBYTE,
    [IM_RGBA] = im_mul_same_UBYTE,
    [IM_RGB16] = im_mul_same_USHORT,
};


generate_image_same_function(UBYTE, mul_clipped)
generate_image_same_function(BYTE, mul_clipped)
generate_image_same_function(USHORT, mul_clipped)
generate_image_same_function(SHORT, mul_clipped)

static improc_function_IIf mul_clipped_same[IM_TYPEMAX] = {
    [IM_BYTE] = im_mul_clipped_same_BYTE,
    [IM_UBYTE] = im_mul_clipped_same_UBYTE,
    [IM_SHORT] = im_mul_clipped_same_SHORT,
    [IM_USHORT] = im_mul_clipped_same_USHORT,
    [IM_RGB] = im_mul_clipped_same_UBYTE,
    [IM_RGBA] = im_mul_clipped_same_UBYTE,
    [IM_RGB16] = im_mul_clipped_same_USHORT,
};

static void im_mul_mixed_RGB_with_UBYTE(ip_image *d, ip_image *s, int flag)
{
    for (int j=0; j<d->size.y; j++) {
	ip_rgb *dp;
	uint8_t *sp;
	dp = IM_ROWADR(d, j, v);
	sp = IM_ROWADR(s, j, v);
	for (int i=0; i<d->size.x; i++) {
	    if (DORED(flag))
		dp->r *= *sp;
	    if (DOGREEN(flag))
		dp->g *= *sp;
	    if (DOBLUE(flag))
		dp->b *= *sp;
	    dp++; sp++;
	}
    }
}


static void im_mul_mixed_RGBA_with_UBYTE(ip_image *d, ip_image *s, int flag)
{
    for (int j=0; j<d->size.y; j++) {
	ip_rgba *dp;
	uint8_t *sp;
	dp = IM_ROWADR(d, j, v);
	sp = IM_ROWADR(s, j, v);
	for (int i=0; i<d->size.x; i++) {
	    if (DORED(flag))
		dp->r *= *sp;
	    if (DOGREEN(flag))
		dp->g *= *sp;
	    if (DOBLUE(flag))
		dp->b *= *sp;
	    if (DOALPHA(flag))
		dp->a *= *sp;
	    dp++; sp++;
	}
    }
}


static void im_mul_mixed_RGB16_with_USHORT(ip_image *d, ip_image *s, int flag)
{
    for (int j=0; j<d->size.y; j++) {
	ip_lrgb *dp;
	uint16_t *sp;
	dp = IM_ROWADR(d, j, v);
	sp = IM_ROWADR(s, j, v);
	for (int i=0; i<d->size.x; i++) {
	    if (DORED(flag))
		dp->r *= *sp;
	    if (DOGREEN(flag))
		dp->g *= *sp;
	    if (DOBLUE(flag))
		dp->b *= *sp;
	    dp++; sp++;
	}
    }
}


static void im_mul_mixed_COMPLEX_with_FLOAT(ip_image *d, ip_image *s, int flag)
{
    for (int j=0; j<d->size.y; j++) {
	float *sptr;
	ip_complex *dptr;
	dptr = IM_ROWADR(d,j,v);
	sptr = IM_ROWADR(s,j,v);
	for (int i=0; i<d->size.x; i++) {
	    if (DOREAL(flag))
		dptr->r *= *sptr;
	    if (DOIMAG(flag))
		dptr->i *= *sptr;
	    dptr++; sptr++;
	}
    }
}


static void im_mul_mixed_DCOMPLEX_with_DOUBLE(ip_image *d, ip_image *s, int flag)
{
    for (int j=0; j<d->size.y; j++) {
	double *sptr;
	ip_dcomplex *dptr;
	dptr = IM_ROWADR(d,j,v);
	sptr = IM_ROWADR(s,j,v);
	for (int i=0; i<d->size.x; i++) {
	    if (DOREAL(flag))
		dptr->r *= *sptr;
	    if (DOIMAG(flag))
		dptr->i *= *sptr;
	    dptr++; sptr++;
	}
    }
}


static improc_function_IIf mul_mixed[IM_TYPEMAX] = {
    [IM_RGB] = im_mul_mixed_RGB_with_UBYTE,
    [IM_RGBA] = im_mul_mixed_RGBA_with_UBYTE,
    [IM_RGB16] = im_mul_mixed_RGB16_with_USHORT,
    [IM_COMPLEX] = im_mul_mixed_COMPLEX_with_FLOAT,
    [IM_DCOMPLEX] = im_mul_mixed_DCOMPLEX_with_DOUBLE,
};


static void im_mul_clipped_mixed_RGB_with_UBYTE(ip_image *d, ip_image *s, int flag)
{
    for (int j=0; j<d->size.y; j++) {
	ip_rgb *dp;
	uint8_t *sp;
	dp = IM_ROWADR(d, j, v);
	sp = IM_ROWADR(s, j, v);
	for (int i=0; i<d->size.x; i++) {
	    if (DORED(flag))
		dp->r = pixel_mul_clipped_UBYTE(dp->r, *sp);
	    if (DOGREEN(flag))
		dp->g = pixel_mul_clipped_UBYTE(dp->g, *sp);
	    if (DOBLUE(flag))
		dp->b = pixel_mul_clipped_UBYTE(dp->b, *sp);
	    dp++; sp++;
	}
    }
}

static void im_mul_clipped_mixed_RGBA_with_UBYTE(ip_image *d, ip_image *s, int flag)
{
    for (int j=0; j<d->size.y; j++) {
	ip_rgba *dp;
	uint8_t *sp;
	dp = IM_ROWADR(d, j, v);
	sp = IM_ROWADR(s, j, v);
	for (int i=0; i<d->size.x; i++) {
	    if (DORED(flag))
		dp->r = pixel_mul_clipped_UBYTE(dp->r, *sp);
	    if (DOGREEN(flag))
		dp->g = pixel_mul_clipped_UBYTE(dp->g, *sp);
	    if (DOBLUE(flag))
		dp->b = pixel_mul_clipped_UBYTE(dp->b, *sp);
	    if (DOALPHA(flag))
		dp->a = pixel_mul_clipped_UBYTE(dp->a, *sp);
	    dp++; sp++;
	}
    }
}

static void im_mul_clipped_mixed_RGB16_with_USHORT(ip_image *d, ip_image *s, int flag)
{
    for (int j=0; j<d->size.y; j++) {
	ip_lrgb *dp;
	uint16_t *sp;
	dp = IM_ROWADR(d, j, v);
	sp = IM_ROWADR(s, j, v);
	for (int i=0; i<d->size.x; i++) {
	    if (DORED(flag))
		dp->r = pixel_mul_clipped_USHORT(dp->r, *sp);
	    if (DOGREEN(flag))
		dp->g = pixel_mul_clipped_USHORT(dp->g, *sp);
	    if (DOBLUE(flag))
		dp->b = pixel_mul_clipped_USHORT(dp->b, *sp);
	    dp++; sp++;
	}
    }
}


static improc_function_IIf mul_clipped_mixed[IM_TYPEMAX] = {
    [IM_RGB] = im_mul_clipped_mixed_RGB_with_UBYTE,
    [IM_RGBA] = im_mul_clipped_mixed_RGBA_with_UBYTE,
    [IM_RGB16] = im_mul_clipped_mixed_RGB16_with_USHORT
};

/*

NAME:

   im_mul() \- Multiply two images together pixel by pixel.

PROTOTYPE:

   #include <ip/ip.h>

   int  im_mul( dest, src, flag )
   ip_image *dest;
   ip_image *src;
   int flag;

DESCRIPTION:

   Multiply the images 'src' and 'dest' pixelwise and return the result in
   'dest'.  The multiplication is carried out (effectively) in the same type
   as the image. So, for example, if the image type is IM_UBYTE and if 20 is
   to be multiplied with 15, then the result is 44.

   For BYTE, UBYTE, SHORT and USHORT images, if 'flag' is set to FLG_CLIP
   then integer multiplication is used and the result is clipped (saturated)
   to the limits of the respective pixel data type. For example, if pixel
   values 20 and 15 are multiplied together in a UBYTE image then the
   resultant pixel would be 255.

   Normally, images must be of the same type to be multiplied, but one
   exception is that 'dest' can be COMPLEX and 'src' FLOAT, and 'flag'
   must be set to FLG_REAL or FLG_IMAG to specify whether the float
   image should be treated as the real or imaginery part of a complex
   image (with the other part treated as zero).  Useful for windowing
   in complex Fourier space, for example.

LIMITATIONS:

   At present the FLG_CLIP flag only works on BYTE, UBYTE, SHORT and USHORT
   images, but should really be implemented for other image types.

ERRORS:

   ERR_NOT_SAME_SIZE
   ERR_NOT_SAME_TYPE
   ERR_BAD_FLAG
   ERR_BAD_TYPE

SEE ALSO:

   im_add()     im_sub()     im_div()
   im_addc()    im_subc()    im_mulc()    im_divc()

*/

int im_mul(ip_image *dest, ip_image *src, int flag)
{
    int retcode;
    struct function_list ops;
    int done = 0;

    ip_log_routine(__func__);

    if (NOT (image_valid(dest) && image_valid(src)))
	return ip_error;

    if ((dest->type == IM_COMPLEX && src->type == IM_FLOAT) ||
	(dest->type == IM_DCOMPLEX && src->type == IM_DOUBLE) ||
	((dest->type == IM_RGB || dest->type==IM_RGBA) && src->type == IM_UBYTE) ||
	(dest->type == IM_RGB16 && src->type == IM_USHORT) ) {
	if (NOT images_same_size(dest,src))
	    return ip_error;
    }else{
	if (NOT images_compatible(dest,src))
	    return ip_error;
    }

    if (image_bad_type(src, IM_PALETTE, IM_BINARY, -1))
	return ip_error;

    if ((flag & FLG_CLIP) && (NOT (type_clrim(dest->type) || dest->type == IM_BYTE
				   || dest->type == IM_UBYTE || dest->type == IM_SHORT
				   || dest->type == IM_USHORT))) {
	ip_log_error(ERR_BAD_FLAG);
	ip_pop_routine();
	return ip_error;
    }

    if (type_complex(dest->type) && NOT type_complex(src->type)) {

	/* COMPLEX or DCOMPLEX with a floating point addend */
	if (NOT ip_flag_bits_unused(flag,  ~(FLG_SREAL|FLG_SIMAG)))
	    return ip_error;
	/* At least one relevant flag must be set */
	if (!DOREAL(flag) && !DOIMAG(flag)) {
	    ip_log_error(ERR_BAD_FLAG);
	    ip_pop_routine();
	    return ip_error;
	}

    }else if (type_clrim(dest->type) && dest->type != IM_PALETTE) {

	/* RGB, RGBA (or RGB16) with a UBYTE (or USHORT) addend */
	if (NOT ip_flag_bits_unused(flag, ~(FLGPRT_COLOUR|FLG_CLIP)))
	    return ip_error;
	/* At least one relevant flag must be set */
	if ((src->type == IM_UBYTE || src->type == IM_USHORT)
	    && (! (DOALPHA(flag) | DORED(flag) | DOGREEN(flag) | DOBLUE(flag))
		|| (dest->type != IM_RGBA && DOALPHA(flag)))) {
	    ip_log_error(ERR_BAD_FLAG);
	    ip_pop_routine();
	    return ip_error;
	}

    }else{

	/* scalar image types including palette */
	if (NOT ip_flag_ok(flag, FLG_CLIP, LASTFLAG))
	    return ip_error;

    }

    /* Flags have been verified correct at this stage */

#ifdef HAVE_SIMD

    if (IP_USE_HWACCEL) {
	ops.op_same = ip_simd_mul_same;
	ops.op_clipped_same = ip_simd_mul_clipped_same;
	ops.op_mixed = ip_simd_mul_mixed;
	ops.op_clipped_mixed = ip_simd_mul_clipped_mixed;

	done = process_images_by_operator(dest, src, flag, &ops);
    }

    if (done) {
	ip_pop_routine();
	return OK;
    }

#endif

    /* No SIMD operator available; use pixelwise processing. */

    ops.op_same = mul_same;
    ops.op_clipped_same = mul_clipped_same;
    ops.op_mixed = mul_mixed;
    ops.op_clipped_mixed = mul_clipped_mixed;

    done = process_images_by_operator(dest, src, flag, &ops);

    retcode = OK;
    if (! done) {
	ip_log_error(ERR_NOT_IMPLEMENTED);
	retcode = ip_error;
    }

    ip_pop_routine();
    return retcode;
}



/*
 * im_div() helper routines
 */

generate_image_same_function(UBYTE, div)
generate_image_same_function(BYTE, div)
generate_image_same_function(USHORT, div)
generate_image_same_function(SHORT, div)
generate_image_same_function(ULONG, div)
generate_image_same_function(LONG, div)
generate_image_same_function(FLOAT, div)
generate_image_same_function(DOUBLE, div)
generate_image_same_function(COMPLEX, div)
generate_image_same_function(DCOMPLEX, div)

static improc_function_IIf div_same[IM_TYPEMAX] = {
    [IM_BYTE] = im_div_same_BYTE,
    [IM_UBYTE] = im_div_same_UBYTE,
    [IM_SHORT] = im_div_same_SHORT,
    [IM_USHORT] = im_div_same_USHORT,
    [IM_LONG] = im_div_same_LONG,
    [IM_ULONG] = im_div_same_ULONG,
    [IM_FLOAT] = im_div_same_FLOAT,
    [IM_DOUBLE] = im_div_same_DOUBLE,
    [IM_COMPLEX] = im_div_same_COMPLEX,
    [IM_DCOMPLEX] = im_div_same_DCOMPLEX,
    [IM_RGB] = im_div_same_UBYTE,
    [IM_RGBA] = im_div_same_UBYTE,
    [IM_RGB16] = im_div_same_USHORT,
};

generate_image_same_function(UBYTE, div_protected)
generate_image_same_function(BYTE, div_protected)
generate_image_same_function(USHORT, div_protected)
generate_image_same_function(SHORT, div_protected)
generate_image_same_function(ULONG, div_protected)
generate_image_same_function(LONG, div_protected)
generate_image_same_function(FLOAT, div_protected)
generate_image_same_function(DOUBLE, div_protected)

static improc_function_IIf div_protected_same[IM_TYPEMAX] = {
    [IM_BYTE] = im_div_protected_same_BYTE,
    [IM_UBYTE] = im_div_protected_same_UBYTE,
    [IM_SHORT] = im_div_protected_same_SHORT,
    [IM_USHORT] = im_div_protected_same_USHORT,
    [IM_LONG] = im_div_protected_same_LONG,
    [IM_ULONG] = im_div_protected_same_ULONG,
    [IM_FLOAT] = im_div_protected_same_FLOAT,
    [IM_DOUBLE] = im_div_protected_same_DOUBLE,
    [IM_RGB] = im_div_protected_same_UBYTE,
    [IM_RGBA] = im_div_protected_same_UBYTE,
    [IM_RGB16] = im_div_protected_same_USHORT,
};


static void im_div_mixed_COMPLEX_with_FLOAT(ip_image *d, ip_image *s, int flag)
{
    for (int j=0; j<d->size.y; j++) {
	float *sptr;
	ip_complex *dptr;
	dptr = IM_ROWADR(d,j,v);
	sptr = IM_ROWADR(s,j,v);
	for (int i=0; i<d->size.x; i++) {
	    if (DOREAL(flag))
		dptr->r /= *sptr;
	    if (DOIMAG(flag))
		dptr->i /= *sptr;
	    dptr++; sptr++;
	}
    }
}


static void im_div_mixed_DCOMPLEX_with_DOUBLE(ip_image *d, ip_image *s, int flag)
{
    for (int j=0; j<d->size.y; j++) {
	double *sptr;
	ip_dcomplex *dptr;
	dptr = IM_ROWADR(d,j,v);
	sptr = IM_ROWADR(s,j,v);
	for (int i=0; i<d->size.x; i++) {
	    if (DOREAL(flag))
		dptr->r /= *sptr;
	    if (DOIMAG(flag))
		dptr->i /= *sptr;
	    dptr++; sptr++;
	}
    }
}


static improc_function_IIf div_mixed[IM_TYPEMAX] = {
    [IM_COMPLEX] = im_div_mixed_COMPLEX_with_FLOAT,
    [IM_DCOMPLEX] = im_div_mixed_DCOMPLEX_with_DOUBLE,
};

static improc_function_IIf div_protected_mixed[IM_TYPEMAX] = {};

/*

NAME:

   im_div() \- Divide an image by another image

PROTOTYPE:

   #include <ip/ip.h>

   int  im_div( dest, src, flag )
   ip_image *dest;
   ip_image *src;
   int flag;

DESCRIPTION:

   Divide the image 'dest' by the image 'src', pixel by pixel, and return
   the result in 'dest'.  The division is carried out completely in the same
   type of the image. For example, if the image type is IM_UBYTE (or any
   other integral type) then if 5 is to be divided by 26, the result is 0.

   The result of division by zero is hardware dependent and may (or may not)
   raise a ERR_SIGFPE error.  For this reason the use of FLG_PROTECT
   (described below) is offered.

   Because hardware implementations are so inconsistent in the way they
   handle division by zero, one can set the flag FLG_PROTECT and then
   im_div() will check before division whether the divisor is zero, and if
   so sets the destination pixel to a suitable value.  For integer images
   division of a non-zero number by zero results in the maximum possible
   (positive) integral value or the minimum (i.e. negative) integral value
   depending on the sign of the numerator.  For zero divided by zero the
   destination pixel is replaced with zero.  For floating point arithmetic
   division by zero results in plus or minus infinity depending on the sign
   of the numerator, and division of zero by zero results in
   not-a-number. Obviously there is a performance hit using FLG_PROTECT,
   however the certainty in the result may be worth it.

   Since the result of division is always within the range of the type
   except for division by zero, FLG_CLIP only implements the behaviour of
   FLG_PROTECT.

   Normally, images must be of the same type to be divided. There is an
   exception: 'dest' can be IM_COMPLEX and 'src' IM_FLOAT, and 'flag' must
   be set to FLG_REAL or FLG_IMAG to specify whether the float image should
   be treated as the real or the imaginary part of a complex image (with the
   other part treated as unity for the purposes of the division).  This
   behaviour is also implemented for an IM_DOUBLE divisor and IM_DCOMPLEX
   numerator.

LIMITATIONS:

   Don''t rely on ERR_SIGFPE being flagged for divisions by zero.  It is
   rather machine dependent. On some platforms it is a show stopper (if no
   error handler is installed) and will cause the program to exit.  Even if
   SIGFPE is trapped (which the IP library attempts to do), returning from
   the error handler is unsafe on some platforms as it leads to infinite
   looping!

   The colour image mixed types (e.g. dividing IM_RGB by a UBYTE image and
   only operating on selected colour planes) is not yet
   implemented. Diddums.

ERRORS:

   ERR_NOT_SAME_SIZE
   ERR_NOT_SAME_TYPE
   ERR_BAD_TYPE
   ERR_BAD_FLAG
   ERR_NO_*_SUPPORT

SEE ALSO:

   im_add()     im_sub()     im_mul()
   im_addc()    im_subc()    im_mulc()    im_divc()
   im_replace()

*/

int im_div(ip_image *dest, ip_image *src, int flag)
{
    int retcode;
    struct function_list ops;
    int done = 0;
    int type = dest->type;

    ip_log_routine(__func__);

    if (NOT (image_valid(dest) && image_valid(src)))
	return ip_error;

    if ((type == IM_COMPLEX && src->type == IM_FLOAT) ||
	(type == IM_DCOMPLEX && src->type == IM_DOUBLE) ||
	((type == IM_RGB || type==IM_RGBA) && src->type == IM_UBYTE) ||
	(type == IM_RGB16 && src->type == IM_USHORT) ) {
	if (NOT images_same_size(dest,src))
	    return ip_error;
    }else{
	if (NOT images_compatible(dest,src))
	    return ip_error;
    }

    if (image_bad_type(src, IM_PALETTE, IM_BINARY, -1))
	return ip_error;

    if (type_complex(dest->type)  && !type_complex(src->type)) {
	if (NOT ip_flag_ok(flag,  FLG_SREAL, FLG_SIMAG, LASTFLAG))
	    return ip_error;
	/* At least one relevant flag must be set */
	if (!DOREAL(flag) && !DOIMAG(flag)) {
	    ip_log_error(ERR_BAD_FLAG);
	    ip_pop_routine();
	    return ip_error;
	}

    }else if (type_clrim(dest->type) && dest->type != IM_PALETTE) {
	if (NOT ip_flag_ok(flag, FLG_SRED, FLG_SGREEN, FLG_SBLUE,
			   FLG_SALPHA, LASTFLAG))
	    return ip_error;
	/* At least one relevant flag must be set */
	if (! (DOALPHA(flag) | DORED(flag) | DOGREEN(flag) | DOBLUE(flag))
	    || (dest->type != IM_RGBA && DOALPHA(flag))) {
	    ip_log_error(ERR_BAD_FLAG);
	    ip_pop_routine();
	    return ip_error;
	}

    }else{
	/* scalar image types including palette */
	if (NOT ip_flag_ok(flag, FLG_PROTECT, FLG_CLIP, LASTFLAG))
	    return ip_error;
    }

    if (flag & FLG_PROTECT) {
	/* We use FLG_CLIP to flag FLG_PROTECT in the following */
	flag |= FLG_CLIP;
    }

    /* Flags have been verified correct at this stage */

#ifdef HAVE_SIMD

    if (IP_USE_HWACCEL) {
	ops.op_same = ip_simd_div_same;
	ops.op_clipped_same = ip_simd_div_protected_same;
	ops.op_mixed = ip_simd_div_mixed;
	ops.op_clipped_mixed = ip_simd_div_protected_mixed;

	done = process_images_by_operator(dest, src, flag, &ops);
    }

    if (done) {
	ip_pop_routine();
	return OK;
    }

#endif

    /* No SIMD operator available; use pixelwise processing. */

    ops.op_same = div_same;
    ops.op_clipped_same = div_protected_same;
    ops.op_mixed = div_mixed;
    ops.op_clipped_mixed = div_protected_mixed;

    done = process_images_by_operator(dest, src, flag, &ops);

    retcode = OK;
    if (! done) {
	ip_log_error(ERR_NOT_IMPLEMENTED);
	retcode = ip_error;
    }

    ip_pop_routine();
    return retcode;
}
