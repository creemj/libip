/*
   Module: ip/text.c
   Author: M.J.Cree

   Copyright (C) 2002, 2008, 2015 by Michael J. Cree

   Text drawing routines.

   Uses the freeware freetype library.

ENTRY POINTS:

   ip_alloc_font()
   ip_free_font()
   im_text()

   The following routines currently remain unimplemented and are just stubs;
   indeed, I am not sure now what I intended some of them for :-)

   ip_inittext()
   ip_writetext()


*/

#include <stdlib.h>

#include <ft2build.h>
#include FT_FREETYPE_H
#include FT_GLYPH_H

#define IP_TEXTINT
#include <ip/ip.h>
#include <ip/text.h>

#include "internal.h"
#include "utils.h"

static int initftlib(void);


FT_Library ftlib = NULL;

ip_font *ip_alloc_font(char *font, int pixsize, int leading)
{
    ip_font *ft;
    int err;

    ip_log_routine("ip_alloc_font");

    if (initftlib()) return NULL;

    if ((ft = ip_malloc(sizeof(ip_font))) == NULL) {
	return NULL;
    }

    err = FT_New_Face(ftlib, font, 0, &ft->face);

    if (err) {
	if (err == FT_Err_Unknown_File_Format)
	    ip_log_error(ERR_FTLIB_UNKNOWN_FORMAT,font);
	else
	    ip_log_error(ERR_FTLIB_FONT_NOT_FOUND,font);
	ip_free(ft);
	ft = NULL;
    }else{
	err = FT_Set_Pixel_Sizes(ft->face, 0, pixsize);
	ft->pixsize = pixsize;
	ft->leading = (leading == 0) ? (int)round(pixsize*1.2) : leading;
	if (err) ip_log_error(ERR_FTLIB_ERROR,err);
    }

    ip_pop_routine();
    return ft;
}


/* Not implemented yet */
void ip_free_font(ip_font *ft)
{
    if (ft == NULL) return;

    ip_log_routine("free_font");

    if (initftlib()) return;

    FT_Done_Face(ft->face);
    ip_free(ft);

    ip_pop_routine();
    return;
}


int im_text(ip_image *im, ip_font *font, char *str, ip_coord2d pos, double fg, double bg)
{
    FT_Face face;
    FT_GlyphSlot slot;
    FT_UInt glyph_index, previous;
    FT_Bool use_kerning;
    int pen_x, pen_y;
    char *cptr;
    int err;

    ip_log_routine("im_text");

    if (NOT ip_point_in_image("pos", pos, im)) return ip_error;
    if (NOT ip_value_in_image("fg_pen", fg, im)) return ip_error;
    if (NOT ip_value_in_image("bg_pen", bg, im)) return ip_error;

    if (initftlib()) return ip_error;

    face = font->face;
    slot = face->glyph;
 
    pen_x = pos.x;
    pen_y = pos.y;

    use_kerning = FT_HAS_KERNING(face);
    previous = 0;

    for (cptr = str; *cptr && pen_y < im->size.y; cptr++) {

	if (*cptr == '\n') {
	    pen_x = pos.x;
	    pen_y += font->leading;
	    previous = 0;
	    continue;
	}

	if (*cptr == '\r') {
	    pen_x = pos.x;
	    previous = 0;
	    continue;
	}

	if (*cptr == '\t') {
	    pen_x += (6 * font->pixsize);
	    previous = 0;
	    continue;
	}

	if (*cptr == '\b') {
	    if (previous) {
		pen_x -= slot->advance.x >> 6;
	    }
	    previous = 0;
	    continue;
	}

	if (pen_x > im->size.x) {
	    previous = 0;
	    continue;
	}

	/* Get glyph corresponding to character */

	glyph_index = FT_Get_Char_Index(face, *cptr);

	/* Do kerning if possible */

	if (use_kerning && previous && glyph_index) {
	    FT_Vector delta;

	    FT_Get_Kerning(face, previous, glyph_index, 
			   ft_kerning_default, &delta);
	    pen_x += delta.x >> 6;
	}

	/* Render glyph into Freetype library bitmap */

	err = FT_Load_Glyph(face, glyph_index, FT_LOAD_RENDER);
	if (err) continue;

	/* Put rendered glyph into image */

#if 0
	printf("Rendering char %c (0x%x)\n",*cptr,*cptr);
	printf("Position: pen=(%d,%d)  with slot (%d,%d)\n",
	       pen_x,pen_y,pen_x+slot->bitmap_left,pen_y-slot->bitmap_top);
	printf("Bitmap: size=(%d,%d), pitch=%d,   greys=%d\n",
	       slot->bitmap.width, slot->bitmap.rows, slot->bitmap.pitch,
	       slot->bitmap.num_grays);
#endif

	/* Inefficient drawing code follows (because of the use of im_drawpoint()). */
	{
	    ip_coord2d pt;
	    double slope;

	    slope = (fg - bg) / (double)(slot->bitmap.num_grays-1);
	    pt.x = pen_x+slot->bitmap_left;
	    pt.y = pen_y-slot->bitmap_top;

	    for (int j=0; j<slot->bitmap.rows; j++) {
		unsigned char *bptr = slot->bitmap.buffer + j*slot->bitmap.pitch;

		for (int i=0; i<slot->bitmap.width; i++) {
		    if (pt.x < im->size.x && pt.y < im->size.y) {
			im_drawpoint(im, (double)*bptr * slope + bg, pt);
			bptr++;
		    }
		    pt.x++;
		}
		pt.y ++;
		pt.x = pen_x+slot->bitmap_left;
	    }
	}

	/* Increment pen positions */

	pen_x += slot->advance.x >> 6;

	previous = glyph_index;
    }

    ip_pop_routine();
    return OK;
}


#if 0
/* Not implemented yet */
int ip_inittext(char *str, ip_font *font)
{
    int len;

    ip_log_routine("im_text");

    if (initftlib()) return ip_error;

    len = 0;

    ip_pop_routine();
    return len;
}


/* Not implemented yet */
int ip_writetext(ip_image *im, ip_font *font, ip_coord2d pos, double fg, double bg)
{
    ip_log_routine("ip_writetext");

    if (initftlib()) return ip_error;

    ip_pop_routine();
    return OK;
}
#endif


static int initftlib(void)
{
   ip_log_routine("initftlib");

   if (ftlib == NULL) {
      if (FT_Init_FreeType(&ftlib)) {
	 ip_log_error(ERR_FTLIB_INIT);
	 ip_pop_routine();
	 ip_pop_routine();
	 return 1;
      }
   }
   ip_pop_routine();
   return 0;
}
