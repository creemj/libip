/*
   Module: ip/error.c
   Author: M. J. Cree

   Copyright (C) 1995-2007, 2014, 2017 Michael J. Cree

   Manages error reporting for the IP library.

ENTRY POINTS:

   ip_debug_messages()
   ip_deregister_error_handler()
   ip_printlog()
   ip_log_error()
   ip_log_routine()
   ip_pop_routine()
   ip_print_error()
   ip_register_error_handler()

*/

/* To get sigaction(), etc., stuff */
#define _POSIX_C_SOURCE 199309L

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <errno.h>
#include <signal.h>


#define IPERROR_INTERNAL
#include <ip/image.h>
#include <ip/error.h>

#ifdef IP_USE_GSL
#include <gsl/gsl_errno.h>
#endif

#ifdef IP_DEBUG
#include <mjc/malloc.h>
#endif


/*** Globals ***/

int ip_error;			/* Last error occured */
const char *ip_errmsg;		/* Error message (printf string) */
va_list ip_errargs;		/* Arguments of message */

int ip_verbose;			/* Bits mapped to certain debugging messages */

static int gsl_error_handler_installed = FALSE;

static ip_error_logstack ip_log = { 0 };

static int error_handler_registered;

static void (*error_handler)(ip_error_logstack *, int, const char *, va_list);
static void log_fpe_error(int sig, siginfo_t *, void *);
static void ip_printlogx(ip_error_logstack *ipr);

#ifdef IP_USE_GSL
static void log_gsl_error(const char *reason, const char *file, int line,
			  int gerrno);
#endif

/* The error messages array */
struct error_msg {
    int err_code;			/* The error code */
    const char *err_code_str;		/*   and the error code as a string. */
    const char *msg;			/* A corresponding error message. */
};

/*** Table (array) of error messages ***/

#define ERR_MSG(id, msg) { id, #id, msg }

struct error_msg ip_error_msg[] = {
   ERR_MSG( OK, "Hmmmm, there seems to be no error, actually..." ),
   ERR_MSG( ERR_OUT_OF_MEM, "Out of memory" ),
   ERR_MSG( ERR_NOT_IMPLEMENTED, "Sorry, not implemented yet" ),
   ERR_MSG( ERR_TOO_DEEP_LOG, "The routine log has overflowed" ),
   ERR_MSG( ERR_POPPED_TOO_MANY, "More routines have exited than entered!!!" ),
   ERR_MSG( ERR_ALGORITHM_FAULT, "OHHH SHIT - THIS IS AN ALGORITHM FAULT" ),
   ERR_MSG( ERR_BAD_MEM_FREE, "Tried to free memory with a NULL pointer" ),
   ERR_MSG( ERR_OUT_OF_CONTROL, "Ohhh, I'm getting all hung up and frazzled" ),

   ERR_MSG( ERR_INVALID_IMAGE, "The image object is not valid or is corrupted!" ),
   ERR_MSG( ERR_INVALID_IMAGE_TYPE, "Invalid image type requested" ),
   ERR_MSG( ERR_INVALID_IMAGE_SIZE, "Invalid image size requested" ),

   ERR_MSG( ERR_FILE_OPEN, "Failed to open file %s" ),
   ERR_MSG( ERR_SHORT_FILE, "%s terminated unexpectantly" ),
   ERR_MSG( ERR_FILEIO, "File I/O error in %s" ),
   ERR_MSG( ERR_FILEIO_OR_TOO_SHORT, "File %s has I/O error or file terminated"
	" unexpectantly" ),
   ERR_MSG( ERR_FILE_CLOSE, "Error closing file %s" ),
   ERR_MSG( ERR_NO_STDIN, "%s files cannot be read from stdin" ),
   ERR_MSG( ERR_NO_STDOUT, "%s files cannot be written to stdout" ),
   ERR_MSG( ERR_BAD_DATABASE, "Bad database file %s, can't read it" ),

   ERR_MSG( ERR_NOT_VISILOG, "%s is not a visilog image file" ),
   ERR_MSG( ERR_INVALID_VISILOG_TYPE, "%s - Can't read visilog image file of that"
	" type" ),
   ERR_MSG( ERR_UNSUPPORTED_VISILOG_WRITE, "%s - Can't write this type of image as a"
	" visilog file" ),

   ERR_MSG( ERR_NOT_CIMAGES, "%s is not a c_images image file" ),
   ERR_MSG( ERR_INVALID_CIMAGES_TYPE, "%s - Can't read c_images image file of that"
	" type" ),
   ERR_MSG( ERR_UNSUPPORTED_CIMAGES_WRITE, "%s - Can't write this type of image as a"
	" c_images file" ),

   ERR_MSG( ERR_NOT_TIFF, "%s is not a TIFF image file" ),
   ERR_MSG( ERR_INVALID_TIFF_TYPE, "%s - Can't read TIFF image file of that type" ),
   ERR_MSG( ERR_UNSUPPORTED_TIFF_WRITE, "%s - Can't write this type of image as a"
	" TIFF file"),
   ERR_MSG( ERR_TIFF, "TIFF routines gave internal error (file is %s)" ),
   ERR_MSG( ERR_BAD_TIFF_FILE, "TIFF file is bad - doesn't comply to TIFF"
	" specification.\n" ),

   ERR_MSG( ERR_NOT_PNG, "%s is not a PNG image file" ),
   ERR_MSG( ERR_INVALID_PNG_TYPE, "%s - Can't read PNG image file of that type" ),
   ERR_MSG( ERR_UNSUPPORTED_PNG_WRITE, "%s - Can't write this type of image as a"
	" PNG file"),
   ERR_MSG( ERR_PNG, "PNG routines gave internal error (file is %s)" ),
   ERR_MSG( ERR_BAD_PNG_FILE, "PNG file is bad - doesn't comply to PNG"
	" specification.\n" ),
   ERR_MSG( ERR_NOT_JPEG, "%s is not a JPEG image file" ),
   ERR_MSG( ERR_INVALID_JPEG_TYPE, "%s - Can't read JPEG image file of that type" ),
   ERR_MSG( ERR_JPEG, "JPEG routines gave internal error (file is %s)" ),
   ERR_MSG( ERR_BAD_JPEG_FILE, "JPEG file is bad - doesn't comply to JPEG"
	" specification.\n" ),

   ERR_MSG( ERR_NOT_SUNRASTER, "%s is not a sunraster image file" ),
   ERR_MSG( ERR_INVALID_SUNRASTER_TYPE, "%s - Can't read sunraster image file of that type" ),
   ERR_MSG( ERR_UNSUPPORTED_SUNRASTER_WRITE, "%s - Can't write this type of image as"
	" a SUN raster file"),
   ERR_MSG( ERR_BAD_SUNRASTER_FILE, "Sunraster file is bad - doesn't comply to"
	" specifications.\n" ),

   ERR_MSG( ERR_UNSUPPORTED_FILETYPE, "%s is not a recognisable image [filetype=%s]" ),
   ERR_MSG( ERR_WRONG_IMAGE_TYPE, "%s is of type %s, which is not permitted here" ),

   ERR_MSG( ERR_UNSUPPORTED_TYPE, "Don't support required operation on that image"
	" type" ),
   ERR_MSG( ERR_SAME_TYPE, "Tried to convert image to same type as original" ),
   ERR_MSG( ERR_BAD_CONVERSION, "" ),
   ERR_MSG( ERR_BAD_FLAG, "Unrecognised flag" ),
   ERR_MSG( ERR_BAD_FLAG2, "Unrecognised flag [value = 0x%x (%d)]" ),
   ERR_MSG( ERR_BAD_TYPE, "Bad image type for this operation" ),

   ERR_MSG( ERR_BAD_PARAMETER, "Bad or out of range parameter" ),
   ERR_MSG( ERR_PARM_BAD, "Parameter <%s> has a bad value of %g.\n"),
   ERR_MSG( ERR_PARM_BAD_VALUE, "Parameter <%s> not within valid image pixel values\n"
			 "INFO: Image type is %s and the parameter has value %g" ),
   ERR_MSG( ERR_PARM_BAD_COORD, "Coord2d parameter <%s> isn't within image bounds\n"
			 "INFO: Image size=(%d,%d),  parameter=(%d,%d)" ),
   ERR_MSG( ERR_PARM_BAD_BOX,   "Box parameter <%s> extends outside image bounds\n"
			 "INFO: Image size=(%d,%d),  box at (%d,%d) of size (%d,%d)" ),
   ERR_MSG( ERR_PARM_BAD_ROI,   "ROI parameter <%s> extends outside image bounds\n"
			 "INFO: Image size=(%d,%d)\n"),
   ERR_MSG( ERR_PARM_NOT_MULT_2, "Parameter <%s> should be even\n"
			"INFO:  It however has value %d" ),
   ERR_MSG( ERR_PARM_NOT_POWER_2, "Parameter <%s> should be a power of 2\n"
			"INFO:  It however has value %d" ),
   ERR_MSG( ERR_PARM_NOT_ODD, "Parameter <%s> should be odd\n"
			"INFO:  It however has value %d" ),
   ERR_MSG( ERR_PARM_OOR, "Parameter <%s> (value %g) is out of allowable range\n"
			"INFO:  It must be between %g and %g" ),
   ERR_MSG( ERR_PARM_TOOSMALL, "Parameter <%s> (value %g) is too small\n"
			"INFO:  It must be greater than %s%g" ),
   ERR_MSG( ERR_PARM_TOOBIG, "Parameter <%s> (value %g) is too big\n"
			"INFO:  It must be less than %s%g" ),
   ERR_MSG( ERR_PARM_NOT_IN_BOX, "Parameter <%s> (value (%d,%d)) not in allowable range\n"
			"INFO: It must be in the box at (%d,%d) and of size (%d,%d)\n" ),
   ERR_MSG( ERR_PARM_NULL, "Parameter <%s> is NULL but must point to something" ),

   ERR_MSG( ERR_NOT_SAME_TYPE, "Images must be the same type for this operation" ),
   ERR_MSG( ERR_NOT_SAME_SIZE, "Images must be the same size for this operation" ),
   ERR_MSG( ERR_BAD_BOX, "The parameter of type box isn't within image bounds\n"
		 "INFO: Image size (%d,%d),  box at (%d,%d) of size (%d,%d)" ),
   ERR_MSG( ERR_BAD_COORD, "The parameter of type coord isn't within image bounds\n"
		  "INFO: Image size=(%d,%d),  coord=(%d,%d)" ),
   ERR_MSG( ERR_BAD_ROI, "The ROI parameter extends outside image boundaries" ),

   ERR_MSG( ERR_BAD_ACTION, "Bad action requested" ),
   ERR_MSG( ERR_BAD_TAG, "Bad tag passed" ),

   ERR_MSG( ERR_BAD_ROWADR, "Bad row number (%d) requested in image (of %d rows)" ),
   ERR_MSG( ERR_BAD_VALUE, "Parameter passed not within valid image pixel values\n"
		    "INFO:  Image type is %s and the parameter has value %g" ),

   ERR_MSG( ERR_RLE_ARRAY_OVERFLOW, "Overflowed rle array for this object."
	" PROBABLE FATAL BUG" ),
   ERR_MSG( ERR_OBJ_INVALID_ROW, "Invalid row to object requested."
	" PROBABLE FATAL BUG" ),
   ERR_MSG( ERR_SPLIT_OBJECT, "The object to be completed is not eight-connected" ),
   ERR_MSG( ERR_BADLY_FORMED_OBJECT, "Object to be completed is badly formed." ),


   ERR_MSG( ERR_BAD_ROI_TYPE, "Unknown/invalid ROI type specified." ),
   ERR_MSG( ERR_BAD_ROI_ACTION, "Unknown/invalid action applied to ROI." ),

   ERR_MSG( ERR_SVD_NO_CONVERGENCE, "SVD didn't converge within 30 iterations." ),
   ERR_MSG( ERR_POLY_NOT_SAME_ORIGIN, "Polynomials must have same origin." ),
   ERR_MSG( ERR_POLY_NOT_SAME_DIMENSION,"Polynomials must have same dimensionality."),

   ERR_MSG( ERR_INVALID_KERNEL, "The kernel object is not valid or is corrupted."),

   ERR_MSG( ERR_FTLIB_ERROR, "FreeType font library returned error code %d." ),
   ERR_MSG( ERR_FTLIB_INIT, "Error in opening FreeType font library." ),
   ERR_MSG( ERR_FTLIB_FONT_NOT_FOUND, "FreeType library failed to find font %s."),
   ERR_MSG( ERR_FTLIB_UNKNOWN_FORMAT, "FreeType library doesn't know format of font %s."),

   ERR_MSG( ERR_GSLLIB, "GNU Scientific Library error - %s." ),

   ERR_MSG( ERR_OPEN_DISPLAY_FAIL, "Failed to open display window" ),
   ERR_MSG( ERR_DISPLAY_NOT_OPEN, "Display must be opened for this function" ),
   ERR_MSG( ERR_DISPLAY_CANVAS, "Failed to resize display canvas" ),
   ERR_MSG( ERR_DISPLAY_IMAGE, "Failed to display image" ),
   ERR_MSG( ERR_DISPLAY_UPDATE, "Failed to update image" ),
   ERR_MSG( ERR_DISPLAY_CLEAR, "Failed to clear display" ),
   ERR_MSG( ERR_DISPLAY_LABEL, "Failed to display label" ),
   ERR_MSG( ERR_DISPLAY_MOUSE, "Failed to get mouse position" ),
   ERR_MSG( ERR_DISPLAY_FREE,  "Failed to free window ID" ),
   ERR_MSG( ERR_DISPLAY_OPENED, "Attempt to open already opened display window" ),

   ERR_MSG( ERR_DISPLAY_BAD_ID, "Bad display window ID" ),
   ERR_MSG( ERR_DISPLAY_BAD_TYPE, "Mis-matching image type for update" ),
   ERR_MSG( ERR_DISPLAY_BAD_UPDATE, "Bad image size/position for update" ),

   ERR_MSG( ERR_UNKNOWN_PALETTE, "Didn't recognise colour palette requested" ),
   ERR_MSG( ERR_PALETTE, "Failed to set new palette" ),
   ERR_MSG( ERR_UNAVAILABLE_PEN, "Pen colour not available in current palette" ),


   ERR_MSG( ERR_SIGNAL, "Unrecognised exception signal received" ),
   ERR_MSG( ERR_SIGFPE, "Arithmetic exception" ),
   ERR_MSG( ERR_MATHS_EXCEPTION, "Mathematics exception" ),
   ERR_MSG( ERR_NR_FAULT, "Numerical routine exception" ),

   ERR_MSG( ERR_PPC_ALTIVEC, "PowerPC AltiVec returned error code %d."),
   ERR_MSG( ERR_OSX_VIMAGE, "Apple vImage library returned error code %d."),
   ERR_MSG( ERR_OSX_VDSP, "Apple vDSP library returned error code %d."),

   ERR_MSG( IMPOSSIBLE_ERROR, "Don't recognise error code" )
};


/* Find the error code in the Error Table above; if not found returns -1. */

static int ip_error_msg_array_idx(int error_code)
{
    int idx;

    for (idx=0; ip_error_msg[idx].err_code != IMPOSSIBLE_ERROR
	     && ip_error_msg[idx].err_code != error_code; idx++)
	; /* Do nothing */

    if (ip_error_msg[idx].err_code == IMPOSSIBLE_ERROR)
	idx = -1;

    return idx;
}



/*

NAME:

   ip_error_code_as_str() \- Get IP library error code as a string

PROTOTYPE:

   #include <ip/ip.h>

   const char *ip_error_code_as_str( error_code )
   int error_code;

DESCRIPTION:

   Returns the error code as a string.  For example, passing ERR_BAD_TYPE
   as 'error_code' results in the string "ERR_BAD_TYPE" being returned.

   If the error_code does not appear as a true error then NULL is returned.

ALSO SEE:

   ip_error_code_msg()		ip_register_error_handler()

*/

const char *ip_error_code_as_str(int error_code)
{
    const char *str = NULL;
    int idx = ip_error_msg_array_idx(error_code);

    if (idx >= 0) {
	str = ip_error_msg[idx].err_code_str;
    }

    return str;
}


/*

NAME:

   ip_error_code_msg() \- Return a useful message corresponding to the error code

PROTOTYPE:

   #include <ip/ip.h>

   const char *ip_error_code_msg( error_code )
   int error_code;

DESCRIPTION:

   Returns a message describing the error corresponding to the passed error
   code.  For example, passing ERR_BAD_TYPE as 'error_code' results in the
   string "Bad image type for this operation" being returned.  Be aware that
   some strings returned are printf formatted strings that contain "%s",
   "%d" or the like, and expect extra arguments when passed to printf().

   If the error_code does not appear as a true error then NULL is returned.

SEE ALSO:

   ip_error_code_as_str()		ip_register_error_handler()

*/

const char *ip_error_code_msg(int error_code)
{
    const char *str = NULL;
    int idx = ip_error_msg_array_idx(error_code);

    if (idx >= 0) {
	str = ip_error_msg[idx].msg;
    }

    return str;
}



/*

NAME:

   ip_register_error_handler() \- Register an ip library error handler

PROTOTYPE:

   #include <ip/ip.h>

   void ip_register_error_handler( error_handler )
   void (*error_handler)(ip_error_logstack *ipr, int error, const char *msg, va_list args);

DESCRIPTION:

   Register an error handler to which any error detected by the ip
   library will be passed.  The parameters of the error handler are:

\&      ipr	  A log of routines that have been called.  Don''t
		access this ever, but it may be passed to print_error().
\&      error     The error code (see error.h)
\&      msg	A printf() formatted message string describing the error.
\&      args	A list of arguments for the string (suitable for use
		with vprintf()).

\&-   The simplest error handler is the following:

\|   void error_handler(ip_error_logstack *ipr, int error, char *msg,
\|                      va_list args)
\|   {
\|      print_error(ipr,error,msg,args);
\|      exit(1);
\|   }

   For some error types including ERR_ALGORITHM_FAULT and
   ERR_RLE_ARRAY_OVERFLOW (and some others) the arguments 'msg' and
   'args' have a different interpretation than that described above.
   'Msg' is a straight string (no printf() formatting) and the first
   argument of 'args' is a printf() formatted string with the rest of
   the arguments following in 'args'.  Note that in any case it is
   okay to do vprintf(msg,args). You will just lose out on printing
   the extra messages that come with the two errors listed above.
   (print_error() knows about this and can print the extra bits!)

   Be wary of calling any IP library routines from within the error
   handler.  If the same error gets raised again then the error
   handler can be called recursively - potentially leading to a
   dead-lock - but should ultimately be detected with an
   ERR_TOO_DEEP_LOG error being flagged.

   Also if you call any IP library routines from the error handle be
   aware that the global variable 'ip_error' will be changed.  This
   has consequences if you return from the error handler to the
   calling routine (rather than exiting the programme) since the error
   handler is assumed not to change 'ip_error'.  Therefore you need to
   restore it when returning from the error handler. An example is
   (for a file failing to be opened):

\|   void error_handler(ip_error_logstack *ipr, int error, char *msg,
\|                      va_list args)
\|   {
\|      if (error == ERR_FILE_OPEN || error == ERR_TIFF) {
\|         // Call IP routines here to display error in
\|         // image display window, then restore ip_error.
\|            .
\|            .
\|            .
\|         ip_error = error;
\|      }else{
\|         // All other errors are fatal.
\|         print_error(ipr,error,msg,args);
\|         exit(1);
\|      }
\|   }

   The vast majority of routines in the IP library can recover/tidyup
   behind themselves and report the error if the error handler returns
   back into the routine.  Note however that there are a few routines
   in the IP library which cannot cope with a return from the error
   handler.  For example, the object generation routines (eg
   track_object() and region_grow()) cannot recover from an error
   occuring within them.

SEE ALSO:

   ip_deregister_error_handler()    ip_print_error()

*/


void ip_register_error_handler(void (*f)(ip_error_logstack *,
					 int, const char *, va_list))
{
   struct sigaction sa;

   error_handler = f;

   sa.sa_sigaction = log_fpe_error;
   sa.sa_flags = SA_SIGINFO;
   sigemptyset(&sa.sa_mask);
   if (sigaction(SIGFPE, &sa, NULL) != 0) {
      fprintf(stderr,
	      "WARNING: Coulnd't establish arithmetic exception handler.\n");
   }
   error_handler_registered = TRUE;
}



/*

NAME:

   ip_deregister_error_handler() \- Deregister a registered error handler.

PROTOTYPE:

   #include <ip/ip.h>

   void  ip_deregister_error_handler( void )

DESCRIPTION:

   Detach the error_handler that was previously installed with
   ip_register_error_handler().

SEE ALSO:

   ip_register_error_handler()    ip_print_error()

*/

void ip_deregister_error_handler(void)
{
   error_handler_registered = FALSE;
}



/*

NAME:

   ip_debug_messages() \- Turn on debugging messages in the ip library

PROTOTYPE:

   #include <ip/ip.h>

   void  ip_debug_messages( flag )
   int flag;

DESCRIPTION:

   Set 'flag' to 0x01 to turn on getting a message every time a routine
   enters and exits.

   Set 'flag' to 0x02 to get reports of memory allocations/frees.  In
   debugging versions of the IP library setting 'flag' to 0x04 forces a
   memory wall check to be done on routine entry and exit.

   Bits can be ORed together.

*/

void ip_debug_messages(int flag)
{
   ip_verbose = flag;
}




/*

NAME:

   ip_log_routine() \- Log the name of a routine just entered

PROTOTYPE:

   #include <ip/ip.h>

   void  ip_log_routine( name )
   char *name;

DESCRIPTION:

   Log a new routine''s 'name' with the error handling code.
   To be used with error flagging code. An example of use is:

\|   void a_routine(int argument)
\|   {
\|
\|      ip_log_routine("a_routine");
\|
\|      // Check argument positive
\|      if (argument < 0) {
\|         ip_log_error(BAD_PARAMETER);
\|         ip_pop_routine();
\|         return;
\|      }
\|
\|      // a_routine()'s code follows
\|          ...
\|
\|      // Done, now exit
\|      ip_pop_routine()
\|   }

   Note that you must carefully do one ip_pop_routine() for each routine
   logged.  Also the ip_pop_routine() always corresponds to the last
   ip_log_routine(), thus the use of setjmp() and longjmp() are prohibited.
   Since I don''t use longjmp() I''ve never found this a problem.

LIMITATIONS:

   The routine name stack size is finite.  In fact it is quite small.  This
   is intentional -- it helps to track down bugs.  Should the stack be
   overflowed the error ERR_TOO_DEEP_LOG is flagged.  The name of the
   function is not logged, but the number of logs and pops are counted and
   matched so it is safe to ignore the error ERR_TOO_DEEP_LOG and continue
   processing.  Any error messages printed may have the wrong routine name
   in this situation.

ERRORS:

   ERR_TOO_DEEP_LOG - Too many routines have logged on!

SEE ALSO:

   ip_pop_routine()   ip_log_error()
   ip_register_error_handler()   ip_print_error()
   ip_printlog()

*/

void ip_log_routine(const char *name)
{
    ip_error = OK;
    if (ip_log.index >= MAX_LOG) {
	ip_log_error(ERR_TOO_DEEP_LOG);
	ip_log.index++;
    }else{
	ip_log.routine[ip_log.index++] = name;
    }
    if (ip_verbose & 0x01) {
	fprintf(stderr,"%s:  ",name);
    }
#ifdef IP_DEBUG
    dbg_enter(name);
    if (ip_verbose & 0x04) {
	dbg_checkmem();
    }
#endif
}



/*

NAME:

   ip_pop_routine() \- Pop routine name off routine name stack

PROTOTYPE:

   #include <ip/ip.h>

   void  ip_pop_routine( void )

DESCRIPTION:

   A routine that was previously logged on with ip_log_routine() must call
   ip_pop_routine() before exiting.  See ip_log_routine() for an example of
   use.

ERRORS:

   ERR_POPPED_TOO_MANY - There were no routines logged!

SEE ALSO:

   ip_log_routine()   ip_log_error()
   ip_register_error_handler()   ip_print_error()
   ip_printlog()

*/

void ip_pop_routine(void)
{
   ip_log.index--;
#ifdef IP_DEBUG
   dbg_leave();
   if (ip_verbose & 0x04) {
      dbg_checkmem();
   }
#endif
   if (ip_log.index < MAX_LOG && ip_log.index >= 0) {
      if (ip_verbose & 0x01) {
	 fprintf(stderr,"  Exiting %s\n",ip_log.routine[ip_log.index]);
      }
      ip_log.routine[ip_log.index] = NULL;
   }
   if (ip_log.index < 0) {
      if (ip_verbose & 0x01) {
	 fprintf(stderr,"Exiting a routine...logstack underflow!\n");
      }
      ip_log.index = 0;
      ip_log_error(ERR_POPPED_TOO_MANY);
   }
}


void ip_reset_routine_stack(void)
{
    while (ip_log.index)
	ip_pop_routine();
}


static void log_fpe_error(int sig, siginfo_t *si, void *v)
{
   if (sig == SIGFPE) {
      ip_log_error(ERR_SIGFPE);
   }else
      ip_log_error(ERR_SIGNAL);
}



#ifdef IP_USE_GSL
static void log_gsl_error(const char *reason, const char *file, int line,
			  int gerrno)
{
   if (gerrno == GSL_ENOMEM) {
      ip_log_error(ERR_OUT_OF_MEM);
   }else{
      ip_log_error(ERR_GSLLIB, gerrno, reason);
   }
}
#endif


void ip_register_gsl_error_handler(void)
{
   if (NOT gsl_error_handler_installed) {
#ifdef IP_USE_GSL
      gsl_set_error_handler(log_gsl_error);
#else
   ;
#endif
   }
}


/*

NAME:

   ip_log_error() \- Log an error condition

PROTOTYPE:

   #include <ip/ip.h>

   int ip_log_error( error, ... )
   int error;

DESCRIPTION:

   Flag an error condition.  Most errors just need to be flagged so, for
   example, to flag a bad parameter passed to a routine do:

\|      ip_log_error(ERR_BAD_PARAMETER);

   The following errors take a filename as the variable argument:

\|   ERR_FILE_OPEN                  ERR_SHORT_FILE
\|   ERR_FILEIO                     ERR_FILEIO_OR_TOO_SHORT
\|   ERR_FILE_CLOSE
\|   ERR_NO_STDIN                   ERR_NO_STDOUT
\|   ERR_BAD_DATABASE
\|   ERR_NOT_VISILOG                ERR_INVALID_VISILOG_TYPE
\|   ERR_UNSUPPORTED_VISILOG_WRITE
\|   ERR_NOT_CIMAGES                ERR_INVALID_CIMAGES_TYPE
\|   ERR_UNSUPPORTED_CIMAGES_WRITE
\|   ERR_NOT_TIFF                   ERR_INVALID_TIFF_TYPE
\|   ERR_UNSUPPORTED_TIFF_WRITE     ERR_TIFF
\|   ERR_BAD_TIFF_FILE
\|   ERR_UNSUPPORTED_SUNRASTER_WRITE
\|   ERR_FTLIB_FONT_NOT_FOUND       ERR_FTLIB_UNKNOWN_FORMAT

   An example of use is:

\|   ip_log_error(ERR_FILE_OPEN,filename);

   The following errors take a filename and another string which describes
   image file or data type, depending on error.

\|   ERR_UNSUPPORTED_FILETYPE       ERR_WRONG_IMAGE_TYPE

   Examples of use are:

\|   ip_log_error(ERR_UNSUPPORTED_FILETYPE, filename,
\|                          ipmsg_filetype[filetype]);
\|   ip_log_error(ERR_WRONG_IMAGE_TYPE, filename,
\|                          ip_image_type_info[imagetype].name);

   The following errors take an integer as an extra argument.

\|   ERR_FTLIB_ERROR

   The following take a fprintf() formatted string as extra arguments:

\|   ERR_ALGORITHM_FAULT            ERR_RLE_ARRAY_OVERFLOW
\|   ERR_OUT_OF_CONTROL

   An example of use is:

\|   if (x > 10) {
\|     ip_log_error(ERR_ALGORITHM_FAULT,
\|       "Variable x should be less than 10 but is %d!\n", x);
\|   }

   The error ERR_OOR_PARAMETER should be used as:

\|   if (x_pos < 0 || x_pos > 10) {
\|     ip_log_error(ERR_OOR_PARAMETER,
\|       "x_position", (double)x_pos, 0.0, 10.0);
\|   }

   That is, it takes the name of the variable, the actual value of it as a
   double and its permitted minimum value and maximum values (also as
   doubles).

   Note that this routine calls any error handler established by a call to
   ip_register_error_handler().  If the error handler doesn''t return then
   this routine will never return.  To read more on how to write error
   handlers see ip_register_error_handler().

LIMITATIONS:

   This description is not complete.  There are other errors not listed
   above that must be called with special arguments to ip_log_error().  Best
   to examine the error.h and error.c source code...

SEE ALSO:

   ip_register_error_handler()   ip_print_error()

*/

int ip_log_error(int error, ...)
{
    va_list ap;
    int idx;

    va_start(ap,error);
    ip_error = error;
    ip_errmsg = NULL;

    /* Find corresponding error message. */

    idx = ip_error_msg_array_idx(error);

    /* Set up message pointers */

    if (idx > 0) {
	ip_errmsg = ip_error_msg[idx].msg;
#if 0
	if ((ip_error >= LOWER_FILE_ERR_LIMIT && ip_error <HIGHER_FILE_ERR_LIMIT)
	    || ip_error == ERR_ALGORITHM_FAULT
	    || ip_error == ERR_RLE_ARRAY_OVERFLOW
	    || ip_error == ERR_WRONG_IMAGE_TYPE
	    || ip_error == ERR_UNSUPPORTED_FILETYPE
	    || ip_error == ERR_OUT_OF_CONTROL
	    || ip_error == ERR_BAD_FLAG2
	    || ip_error == ERR_OOR_PARAMETER
	    || ip_error == ERR_TOOBIG_PARAMETER
	    || ip_error == ERR_TOOSMALL_PARAMETER
	    || ip_error == ERR_PARM_NOT_MULT_2
	    || ip_error == ERR_PARM_NOT_POWER_2
	    || ip_error == ERR_PARM_NOT_ODD
	    || ip_error == ERR_BAD_ROWADR
	    || ip_error == ERR_BAD_VALUE
	    || ip_error == ERR_BAD_COORD
	    || ip_error == ERR_BAD_BOX
	    || ip_error == ERR_MATHS_EXCEPTION
	    || ip_error == ERR_NR_FAULT
	    || ip_error == ERR_FTLIB_ERROR
	    || ip_error == ERR_FTLIB_FONT_NOT_FOUND
	    || ip_error == ERR_FTLIB_UNKNOWN_FORMAT
	    || ip_error == ERR_GSLLIB
	    || ip_error == ERR_PPC_ALTIVEC
	    || ip_error == ERR_OSX_VIMAGE
	    || ip_error == ERR_OSX_VDSP) {
	    ip_errargs = ap;
	}
#endif
    }else{
	ip_errmsg = "Unrecognised error.";
    }

   /* Call error handler if established */

   if (error_handler_registered) {
       error_handler(&ip_log, ip_error, ip_errmsg, ap);
   }

   va_end(ap);
   return ip_error;
}



/*

NAME:

   ip_print_error() \- Print an error condition in a nice format

PROTOTYPE:

   #include <ip/ip.h>

   void  ip_print_error( prg, ipr, error, msg, args )
   const char *prg;
   ip_error_logstack *ipr;
   int error;
   const char *msg;
   va_list args;

DESCRIPTION:

   Pretty print an error message.  Usually called from within an error
   handler.  'Prg' is the programme name.  The other arguments are as passed
   to the error handler.

   Prints the message in the format:

 \|  <programme_name>:(<routine_generating_error>)
 \|  ERROR: <error_message>
 \|  [<Extra info for certain errors>]

SEE ALSO:

   ip_register_error_handler()

*/

void ip_print_error(const char *prg, ip_error_logstack *ipr,
		    int error, const char *msg, va_list ap)
{
   char *extrmsg;

   /* Print programme name */

   fprintf(stderr,"%s:",prg);

   /* Print name of routine that caused error */

   ip_printlogx(ipr);
   fputc('\n',stderr);

   /* Print error message */

   if (msg) {
      if (ip_error == ERR_GSLLIB) {
#ifdef IP_USE_GSL
	 int gerrno;

	 gerrno = va_arg(ap, int);
#endif
	 fprintf(stderr,"ERROR: ");
#ifdef IP_USE_GSL
	 fprintf(stderr,msg,gsl_strerror(gerrno));
#else
	 fprintf(stderr,msg,0);
#endif
	 extrmsg = va_arg(ap, char *);
	 fprintf(stderr, "\nINFO:  ");
	 vfprintf(stderr,extrmsg,ap);
      }else if (ip_error == ERR_ALGORITHM_FAULT
		|| ip_error == ERR_RLE_ARRAY_OVERFLOW
		|| ip_error == ERR_OUT_OF_CONTROL
		|| ip_error == ERR_MATHS_EXCEPTION
		|| ip_error == ERR_NR_FAULT) {
	 fprintf(stderr,"ERROR: %s\n",msg);
	 extrmsg = va_arg(ap, char *);
	 fprintf(stderr, "INFO:  ");
	 vfprintf(stderr,extrmsg,ap);
      }else{
	 fprintf(stderr,"ERROR: ");
	 vfprintf(stderr,msg,ap);
      }
   }else{
      fprintf(stderr,"ERROR: Unregonised error code??? (error code = %d)",error);
   }

   fprintf(stderr,"\n");

   if (ip_error >= LOWER_FILE_ERR_LIMIT && ip_error <= ERRNO_LIMIT && errno) {
      perror("INFO");
   }
}




static void ip_printlogx(ip_error_logstack *ipr)
{
   if (ipr->routine[0]) {
      fprintf(stderr," (%s",ipr->routine[0]);
      if (ipr->index > 1) {
	 if (ipr->index == 2)
	    fprintf(stderr,"/");
	 else
	    fprintf(stderr,"/.../");
	 fprintf(stderr,"%s",
		 ipr->index > MAX_LOG ? "???" : ipr->routine[ipr->index-1]);
      }
      fprintf(stderr,")");
   }else
      fprintf(stderr," (routine not logged)");
}


/*

NAME:

   ip_printlog() \- Print out the logged routines

PROTOTYPE:

   #include <ip/ip.h>

   void  ip_printlog( void )

DESCRIPTION:

   This is the routine that ip_print_error() uses to print out the logged
   routine names.  It only prints out the first routine name logged and the
   last routine name logged to stderr. If there are more than two then it
   prints an ellipsis to represent the extra routine names.

   Example outputs follow:

   No routine logged:

\|   (routine not logged)

   One routine logged (has name function1()):

\|   (function1)

   Two routines logged (second has name function2()):

\|   (function1/function2)

   Three or more routines logged where last logged routine has the
   name functionN()

\|   (function1/.../functionN)

SEE ALSO:

   ip_log_routine()    ip_pop_routine()
   ip_print_error()

*/

void ip_printlog(void)
{
   ip_printlogx(&ip_log);
}
