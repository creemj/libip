/*
   Header: ip/utils.h
   Author: M.J.Cree

   Copyright (C) 1995-2013, 2016 Michael J. Cree

   Utility definitions and functions for IP library.

*/

#ifndef IP_UTILS_H
#define IP_UTILS_H

#include <math.h>
#include <ip/types.h>
#include "internal.h"

#ifdef WIN32
extern int isnan(double);
extern int isfinite(double);
#endif

/* Fix up missing definitions in some compilers */
#ifndef M_SQRT2
#define M_SQRT2 1.4142135623730950488E0
#endif

#ifndef M_PI
#define M_PI 3.1415926535897932385E0
#endif


/* Mathematical support */

/* #define ROUND(x) (floor((x)+0.5))*/
#define ROUND(x) (round(x))

static inline double eucldist(ip_coord2d a, ip_coord2d b)
{
   return hypot(b.x-a.x, b.y-a.y);
}

/* Memory allocation/deallocation */

extern void *ip_malloc(size_t size);
extern void *ip_mallocx(size_t size);
extern void *ip_mallocx_align(size_t size, size_t alignment);
extern void *ip_realloc(void *ptr, size_t size);
extern void *ip_reallocx(void *ptr, size_t size);
extern void ip_free(void *ptr);

static inline void *ip_mallocx_with_row_alignment(size_t size)
{
#ifdef HAVE_SIMD
    return ip_mallocx_align(size, ROW_ALIGNMENT);
#else
    return ip_mallocx(size);
#endif
}


/* String manipulation */

extern char *ip_strdup(char *str);
extern char *ip_strdupx(char *str);

/* Code in random.c at present */
double ip_rand_linear(void);
double ip_rand_gaussian(void);

/* Splat a value into an unsigned long */
int ip_value_splat(int type, double val, unsigned long *val_splat);

#endif
