/*
   Module: tranpose.c
   Author: M. J. Cree

   Copyright (C) 2015, 2016 Michael J. Cree

   Transpose an image
*/

#include <stdlib.h>
#include <ip/ip.h>
#include "internal.h"
#include "function_ops.h"

#ifdef HAVE_SIMD
#include "simd/transpose_simd.h"
#endif


/*
 * Straightforward walk through the image transposing pixel by pixel.  Not
 * very efficient wrt cache, but here for testing purposes.
 */

#define generate_image_pixel_transpose(imtype, tt)\
    static void image_transpose_pixel_##imtype(ip_image *dest, ip_image *src) \
    {									\
	for (int j=0; j<src->size.y; j++) {				\
	    imtype##_type *spix = IM_ROWADR(src, j, v);			\
	    for (int i=0; i<src->size.x; i++) {				\
		IM_PIXEL(dest, j, i, tt) = *spix++;			\
	    }								\
	}								\
    }

#if 0
generate_image_pixel_transpose(UBYTE, ub)
generate_image_pixel_transpose(USHORT, us)
generate_image_pixel_transpose(ULONG, ul)
#endif


/*
 * We transpose images at two levels: first by tiles then by small 2x2 or
 * 4x4 pixel blocks.
 *
 * The tiles are roughly of size 32x32 pixels, i.e. a good part of a cache
 * way along an image row without going down too many columns on the
 * transpose thus risking too many cache misses.  Thus the tiles are an
 * attempt to improve cache locality.
 *
 * Each tile is then processed by blocks which are 2x2 or 4x4 pixels blocks
 * if a few pixels can fit in an uint32_t or uint64_t, or 1x1 pixel blocks
 * (thus back to single pixels) if the pixels are the machine natural size
 * or larger.  The 2x2 or 4x4 blocks provide a speedup in that we incur only
 * two or four memory accesses (respectively) to read the block, instead of
 * four or sixteen (respectively) memory accesses if it were done by
 * individual pixels.  The principle is that memory accesses are costly and
 * we want to reduce them as much as possible.
 */



/*
 * Use blocks based on multiple uint32_t memory accesses (if that is the
 * machine's natural memory access size or if the machine is 64-bit but
 * limited in CPU registers).  If the pixel base type is 1-byte in size then
 * we get 4x4 pixel blocks, and if the pixel base type is 2-bytes in size
 * then get 2x2 pixel blocks.  Anything larger (on a 32-bit machine) is
 * processed as individual pixels.
 */

struct uint32x2 {
    uint32_t a, b;
};

struct uint32x4 {
    uint32_t a, b, c, d;
};

static inline struct uint32x2 transpose_u32_2(uint32_t a, uint32_t b)
{
    struct uint32x2 res;

#ifdef HAVE_LITTLE_ENDIAN
    res.a = (a & 0x00ff00ff) | ((b & 0x00ff00ff)<<8);
    res.b = ((a & 0xff00ff00)>>8) | (b & 0xff00ff00);
#else
    res.a = (a & 0xff00ff00) | ((b & 0xff00ff00)>>8);
    res.b = ((a & 0x00ff00ff)<<8) | (b & 0x00ff00ff);
#endif
    return res;
}

static inline struct uint32x2 transpose_u32_h4(uint32_t a, uint32_t b)
{
    struct uint32x2 res;

#ifdef HAVE_LITTLE_ENDIAN
    res.a = (a & 0x0000ffff) | ((b & 0x0000ffff)<<16);
    res.b = ((a & 0xffff0000)>>16) | (b & 0xffff0000);
#else
    res.a = (a & 0xffff0000) | ((b & 0xffff0000)>>16);
    res.b = ((a & 0x0000ffff)<<16) | (b & 0x0000ffff);
#endif

    return res;
}

static inline struct uint32x4 transpose_uint32_4(struct uint32x4 in)
{
    struct uint32x2 t;

    /* 2x2 tranpose within each quadrant */
    t = transpose_u32_2(in.a, in.b);
    in.a = t.a; in.b = t.b;

    t = transpose_u32_2(in.c, in.d);
    in.c = t.a; in.d = t.b;

    /* 2x2 transpose of the quadrants */
    t = transpose_u32_h4(in.a, in.c);
    in.a = t.a; in.c = t.b;

    t = transpose_u32_h4(in.b, in.d);
    in.b = t.a; in.d = t.b;

    return in;
}

/*
 * Use blocks based on uint64_t accesses.
 *
 * These are only used on 64-bit machines.  A 2-byte sized pixel leads to
 * 4x4 blocks and a 4-byte pixel leads to 2x2 blocks.  A 1-byte pixel leads
 * to an 8x8 block however that could lead to register spill to the stack
 * and defeat any gains in performance on architectures with a small number
 * of CPU registers.
 */

struct uint64x2 {
    uint64_t a, b;
};

struct uint64x4 {
    uint64_t a, b, c, d;
};

static inline struct uint64x2 transpose_u64_2(uint64_t a, uint64_t b)
{
    struct uint64x2 res;

#ifdef HAVE_LITTLE_ENDIAN
    res.a = (a & 0x0000ffff0000ffffUL) | ((b & 0x0000ffff0000ffffUL)<<16);
    res.b = ((a & 0xffff0000ffff0000UL)>>16) | (b & 0xffff0000ffff0000UL);
#else
    res.a = (a & 0xffff0000ffff0000UL) | ((b & 0xffff0000ffff0000UL)>>16);
    res.b = ((a & 0x0000ffff0000ffffUL)<<16) | (b & 0x0000ffff0000ffffUL);
#endif
    return res;
}

static inline struct uint64x2 transpose_u64_h4(uint64_t a, uint64_t b)
{
    struct uint64x2 res;

#ifdef HAVE_LITTLE_ENDIAN
    res.a = (a & 0x00000000ffffffffUL) | ((b & 0x00000000ffffffff)<<32);
    res.b = ((a & 0xffffffff00000000UL)>>32) | (b & 0xffffffff00000000UL);
#else
    res.a = (a & 0xffffffff00000000UL) | ((b & 0xffffffff00000000)>>32);
    res.b = ((a & 0x00000000ffffffffUL)<<32) | (b & 0x00000000ffffffffUL);
#endif

    return res;
}

static inline struct uint64x4 transpose_uint64_4(struct uint64x4 in)
{
    struct uint64x2 t;

    /* 2x2 tranpose within each quadrant */
    t = transpose_u64_2(in.a, in.b);
    in.a = t.a; in.b = t.b;

    t = transpose_u64_2(in.c, in.d);
    in.c = t.a; in.d = t.b;

    /* 2x2 transpose of the quadrants */
    t = transpose_u64_h4(in.a, in.c);
    in.a = t.a; in.c = t.b;

    t = transpose_u64_h4(in.b, in.d);
    in.b = t.a; in.d = t.b;

    return in;
}



/*
 * This routine processes a single tile by pixel pumping of size xsize by
 * ysize at location (xofs,yofs) in the source image.
 */

#define generate_transpose_tile_pixel(type, tt)			\
    static inline void transpose_tile_pixel_##type(ip_image *dest, ip_image *src, \
					     int xofs, int yofs, int xsize, int ysize) \
    {									\
	for (int j=0; j<ysize; j++) {					\
	    int y = yofs + j;						\
	    type##_type *spix = IM_ROWADR(src, y, tt);			\
	    spix += xofs;						\
	    for (int i=0; i<xsize; i++) {				\
		IM_PIXEL(dest, y, xofs+i, tt) = *spix++;		\
	    }								\
	}								\
    }

/* Cannot use blocks for these types so do by pixel pumping */
#ifndef HAVE_64BIT
generate_transpose_tile_pixel(ULONG, ul)
#endif
generate_transpose_tile_pixel(DOUBLE, d)
generate_transpose_tile_pixel(DCOMPLEX, dc)


/*
 * Transpose a tile of xsize by ysize at location (xofs,yofs) in the source
 * image by transposing each 4x4 pixel block within the tile and saving that
 * back to its transposed position in the destination image.
 *
 * We rely on the compiler inlining and optimising this code particularly if
 * xsize and/or ysize are known at compile time.
 */

#define generate_transpose_tile_x4(type, proctt, tt)			\
    static inline void transpose_tile_x4_##type(ip_image *dest, ip_image *src, \
					     int xofs, int yofs, int xsize, int ysize) \
    {									\
	/* For each 4x4 pixel block in tile of xsize by ysize do. */	\
	for (int j=0; j<ysize; j+=4) {					\
	    int x = xofs;						\
	    int y = yofs + j;						\
	    proctt##_t *spix1 = IM_ROWADR(src, y, tt) + xofs/4;		\
	    proctt##_t *spix2 = IM_ROWADR(src, y+1, tt) + xofs/4;	\
	    proctt##_t *spix3 = IM_ROWADR(src, y+2, tt) + xofs/4;	\
	    proctt##_t *spix4 = IM_ROWADR(src, y+3, tt) + xofs/4;	\
	    for (int i=0; i<xsize; i+=4) {				\
		struct proctt##x4 block4x4;				\
		proctt##_t *dpix;					\
		/* Read the 4x4 pixel block from the source image. */	\
		block4x4.a = *spix1++;					\
		block4x4.b = *spix2++;					\
		block4x4.c = *spix3++;					\
		block4x4.d = *spix4++;					\
		/* Tranpose the 4x4 pixel block. */			\
		block4x4 = transpose_##proctt##_4(block4x4);		\
		/* Write to transposed position in the destination image. */ \
		dpix = IM_ROWADR(dest, x++, v);				\
		dpix[y/4] = block4x4.a;					\
		dpix = IM_ROWADR(dest, x++, v);				\
		dpix[y/4] = block4x4.b;					\
		dpix = IM_ROWADR(dest, x++, v);				\
		dpix[y/4] = block4x4.c;					\
		dpix = IM_ROWADR(dest, x++, v);				\
		dpix[y/4] = block4x4.d;					\
	    }								\
	}								\
    }


generate_transpose_tile_x4(UBYTE, uint32, ul)
#ifdef HAVE_64BIT
generate_transpose_tile_x4(USHORT, uint64, ull)
#endif

#define generate_transpose_tile_x4h(type, proctt, tt, hh)			\
    static inline void transpose_tile_x4h_##type(ip_image *dest, ip_image *src, \
					     int xofs, int yofs, int xsize, int ysize) \
    {									\
	/* For each 2x2 pixel block in tile of xsize by ysize do. */	\
	for (int j=0; j<ysize; j+=2) {					\
	    int y = yofs + j;						\
	    proctt##_t *spix1 = IM_ROWADR(src, y, tt) + xofs/2;		\
	    proctt##_t *spix2 = IM_ROWADR(src, y+1, tt) + xofs/2;	\
	    for (int i=0; i<xsize; i+=2) {				\
		int x = xofs + i;					\
		struct proctt##x2 block2x2;				\
		proctt##_t *dpix;					\
		/* Read the 2x2 pixel block from the source image. */	\
		block2x2.a = *spix1++;					\
		block2x2.b = *spix2++;					\
		/* Tranpose the 2x2 pixel block. */			\
		block2x2 = transpose_##hh##_h4(block2x2.a, block2x2.b);	\
		/* Write to transposed position in the destination image. */ \
		dpix = IM_ROWADR(dest, x, tt) + y/2;			\
		*dpix = block2x2.a;					\
		dpix = IM_ROWADR(dest, x+1, tt) + y/2;			\
		*dpix = block2x2.b;					\
	    }								\
	}								\
    }

#ifndef HAVE_64BIT
generate_transpose_tile_x4h(USHORT, uint32, ul, u32)
#endif
generate_transpose_tile_x4h(ULONG, uint64, ull, u64)



/*
 * Transpose image by tiles.
 *
 * This is the main routine used for processing an image by tiles.
 *
 * Calculates the number of tiles of size tile_size*tile_size in the
 * x-direction and in the y-direction.  The x-direction will always safely
 * fit because of row padding in the source image and will always fit in the
 * y-direction of the transposed image.  The y-direction will always safely
 * fit in the source image but when transposed to the x-direction may spill
 * over the row padding of the destination image therefore specially check
 * for this and process only as many tiles that safely fit, then finish off
 * any remaining rows at the bottom of the image by pixel-based tiles.
 */

#define generate_image_transpose_tiled(type, proctt, tile_size, fn)	\
    static void image_transpose_tiled_##fn##_##type(ip_image *dest, ip_image *src) \
    {									\
	int xlen = contained_pixels_in_padded_row(src, sizeof(proctt)); \
	int ylen = contained_pixels_in_padded_row(dest, sizeof(proctt)); \
	int xblks = (xlen + tile_size - 1) / tile_size;			\
	int yblks = (ylen + tile_size - 1) / tile_size;			\
	/* For each tile do */						\
	for (int m=0; m<yblks; m++) {					\
	    int yofs = m*tile_size;					\
	    int ysize = (ylen - yofs > tile_size) ? tile_size : (ylen - yofs); \
	    for (int k=0; k<xblks; k++) {				\
		int xofs = k*tile_size;					\
		int xsize = (xlen - xofs > tile_size) ? tile_size : (xlen - xofs); \
		/* Transpose tile */					\
		transpose_tile_##fn##_##type(dest, src, xofs, yofs, xsize, ysize); \
	    }								\
	}								\
    }

#if 0
/* Faster options exist. */
generate_image_transpose_tiled(UBYTE, uint32_t, 32, pixel)
generate_image_transpose_tiled(USHORT, uint32_t, 16, pixel)
#endif

generate_image_transpose_tiled(UBYTE, uint32_t, 32, x4)
#ifdef HAVE_64BIT
/* 64 bit machines use uint64_t to process smaller types */
generate_image_transpose_tiled(USHORT, uint64_t, 32, x4)
generate_image_transpose_tiled(ULONG, uint64_t, 32, x4h)
#else
/* 32 bit machines use uint32_t to process smaller types */
generate_image_transpose_tiled(USHORT, uint32_t, 32, x4h)
generate_image_transpose_tiled(ULONG, uint32_t, 16, pixel)
#endif

generate_image_transpose_tiled(DOUBLE, double, 8, pixel)
generate_image_transpose_tiled(DCOMPLEX, ip_dcomplex, 4, pixel)


improc_function_II image_transpose_bytype[IM_TYPEMAX] = {
    [IM_PALETTE] = image_transpose_tiled_x4_UBYTE,
    [IM_BINARY] = image_transpose_tiled_x4_UBYTE,
    [IM_UBYTE] = image_transpose_tiled_x4_UBYTE,
    [IM_BYTE] = image_transpose_tiled_x4_UBYTE,
#ifdef HAVE_64BIT
    [IM_USHORT] = image_transpose_tiled_x4_USHORT,
    [IM_SHORT] = image_transpose_tiled_x4_USHORT,
    [IM_ULONG] = image_transpose_tiled_x4h_ULONG,
    [IM_LONG] = image_transpose_tiled_x4h_ULONG,
    [IM_FLOAT] = image_transpose_tiled_x4h_ULONG,
    [IM_RGBA] = image_transpose_tiled_x4h_ULONG,
#else
    [IM_USHORT] = image_transpose_tiled_x4h_USHORT,
    [IM_SHORT] = image_transpose_tiled_x4h_USHORT,
    [IM_ULONG] = image_transpose_tiled_pixel_ULONG,
    [IM_LONG] = image_transpose_tiled_pixel_ULONG,
    [IM_FLOAT] = image_transpose_tiled_pixel_ULONG,
    [IM_RGBA] = image_transpose_tiled_pixel_ULONG,
#endif
    [IM_DOUBLE] = image_transpose_tiled_pixel_DOUBLE,
    [IM_COMPLEX] = image_transpose_tiled_pixel_DOUBLE,
    [IM_DCOMPLEX] = image_transpose_tiled_pixel_DCOMPLEX
};




ip_image * im_transpose(ip_image *tr, ip_image *im)
{
    int own_tr = FALSE;

    ip_log_routine(__func__);

    if (NOT image_valid(im))
	return NULL;

    if (tr) {
	if (NOT image_valid(tr))
	    return NULL;

	if (tr->size.x != im->size.y || tr->size.y != im->size.x) {
	    ip_log_error(ERR_NOT_SAME_SIZE);
	    ip_pop_routine();
	    return NULL;
	}

	if (NOT images_same_type(tr, im))
	    return NULL;
    }

    if (NOT image_transpose_bytype[im->type]) {
	ip_log_error(ERR_NOT_IMPLEMENTED);
	ip_pop_routine();
	return NULL;
    }

    if (tr == NULL) {
	tr = ip_alloc_image(im->type, (ip_coord2d){im->size.y, im->size.x});
	own_tr = TRUE;
    }

    if (tr) {
	if (ip_im_alloc_extra_rows(im, 1) && ip_im_alloc_extra_rows(tr, 1)) {
	    ip_im_set_rowptr_padding(im, IP_ROWPTRPAD_EXTRAROW1);
	    ip_im_set_rowptr_padding(tr, IP_ROWPTRPAD_EXTRAROW1);

#ifdef HAVE_SIMD
	    if (IP_USE_HWACCEL) {
		if (ip_simd_transpose[im->type]) {
		    ip_simd_transpose[im->type](tr, im);
		    ip_pop_routine();
		    return tr;
		}
	    }
#endif

	    if (image_transpose_bytype[im->type])
		image_transpose_bytype[im->type](tr, im);
	    else
		ip_log_error(ERR_NOT_IMPLEMENTED);

	}else{
#if 1
	    if (own_tr) {
#endif
		ip_free_image(tr);
#if 1
		tr = NULL;
	    }
#endif
	}
    }

    ip_pop_routine();
    return tr;
}


