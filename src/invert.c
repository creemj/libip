/*

   Module: ip/invert.c
   Author: M.J.Cree

   For inverting images.

   Copyright (C) 1995-1997, 2007, 2015 Michael J. Cree

ENTRY POINTS:

   im_invert()

*/


#include <stdlib.h>
#include <stdint.h>

#include <ip/ip.h>

#include "internal.h"
#include "utils.h"

#define IMROW_INVERT_SIGNED(tt,t)		\
    tt *pix = IM_ROWADR(im, j, t);		\
    for (int i=0; i<im->size.x; i++) {		\
	*pix = -*pix;				\
	pix++;					\
    }

#define IMROW_INVERT_UNSIGNED(tt,t,m)		\
    tt *pix = IM_ROWADR(im, j, t);		\
    for (int i=0; i<im->size.x; i++) {		\
	*pix = m - *pix;			\
	pix++;					\
    }


/*

NAME:

   im_invert() \- Invert an image

PROTOTYPE:

   #include <ip/ip.h>

   int  im_invert( im )
   ip_image *im;

DESCRIPTION:

   Invert a BINARY, integer or real type image.  Inversion is defined
   for signed image types to be negation so this just means a sign
   change.  For BINARY and BYTE images, which are unsigned, inversion
   is taken to be the same as the NOT operator.  Hence for BINARY and
   BYTE images only, this routine is exactly the same as im_not().

LIMITATION:

   This routine is currently not defined for COMPLEX or colour images.

ERRORS:

   ERR_BAD_TYPE

SEE ALSO:

   im_not()

*/

int im_invert(ip_image *im)
{
    ip_log_routine("im_invert");

    /* Check have valid image to work with */

    if (NOT (image_type_real(im) || 
	     image_type_integer(im) ||
	     im->type == IM_BINARY)) {
	ip_log_error(ERR_BAD_TYPE);
	ip_pop_routine();
	return ip_error;
    }

    /* Calculate inversion */

    switch (im->type) {
    case IM_BINARY:
    case IM_UBYTE:
	for (int j=0; j<im->size.y; j++) {
	    uint8_t *pix = IM_ROWADR(im, j, ub);
	   
	    for (int i=0; i<im->size.x; i++) {
		*pix = ~*pix;
		pix++;
	    }
	}
	break;
    case IM_SHORT:
	for (int j=0; j<im->size.y; j++) {
	    IMROW_INVERT_SIGNED(int16_t,s);
	}
	break;
    case IM_USHORT:
	for (int j=0; j<im->size.y; j++) {
	    IMROW_INVERT_UNSIGNED(uint16_t,us,UINT16_MAX);
	}
	break;
    case IM_LONG:
	for (int j=0; j<im->size.y; j++) {
	    IMROW_INVERT_SIGNED(int32_t,l);
	}
	break;
    case IM_ULONG:
	for (int j=0; j<im->size.y; j++) {
	    IMROW_INVERT_UNSIGNED(uint32_t,ul,UINT32_MAX);
	}
	break;
    case IM_FLOAT:
	for (int j=0; j<im->size.y; j++) {
	    IMROW_INVERT_SIGNED(float,f);
	}
	break;
    case IM_DOUBLE:
	for (int j=0; j<im->size.y; j++) {
	    IMROW_INVERT_SIGNED(double,d);
	}
	break;
    default:
	;
    }

    ip_pop_routine();
    return OK;
}



