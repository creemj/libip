/*

   Module: ip/logical.c
   Author: M.J.Cree

   Bitwise logical operations on images.
   Image must be of an integer type or IM_BINARY.

   (C) 1995-2012, 2015, 2017 Michael J. Cree

ENTRY POINTS:

   im_or()
   im_and()
   im_xor()
   im_not()

*/

#include <ip/ip.h>

#include "internal.h"
#include "utils.h"

#ifdef HAVE_SIMD
#include "function_ops.h"
#include "simd/binops_simd.h"
#endif


/* Macro to generate the image processing operators */

#define generate_process_image_function(opname, op)			\
    static void process_image_ ## opname(ip_image *d, ip_image *s)	\
    {									\
	int rowlen = containers_in_padded_row(d, sizeof(unsigned long)); \
	for (int j=0; j<s->size.y; j++) {				\
	    unsigned long *dp = IM_ROWADR(d, j, v);			\
	    unsigned long *sp = IM_ROWADR(s, j, v);			\
	    for (int i=0; i<rowlen; i++) {				\
		*dp++ op *sp++;						\
	    }								\
	}								\
    }

generate_process_image_function(or, |= )
generate_process_image_function(and, &= )
generate_process_image_function(xor, ^= )

static void process_image_not(ip_image *d)
{
    int rowlen = containers_in_padded_row(d, sizeof(unsigned long));
    for (int j=0; j<d->size.y; j++) {
	unsigned long *dp = IM_ROWADR(d, j, v);
	for (int i=0; i<rowlen; i++) {
	    *dp = ~*dp;
	    dp++;
	}
    }
}


/*

NAME:

   im_or() \- Logical OR between two images

PROTOTYPE:

   #include <ip/ip.h>

   int  im_or( dest, src )
   ip_image *dest;
   ip_image *src;

DESCRIPTION:

   Perform the pixelwise logical OR between the two images 'dest' and 'src'
   and store the result in 'dest'.  Both images must be BINARY or integer
   type images.  The signed image types (IM_BYTE, IM_SHORT and IM_LONG) are
   treated as unsigned for the purposes of performing the OR operation.

ERRORS:

   ERR_NOT_SAME_TYPE
   ERR_NOT_SAME_SIZE
   ERR_BAD_TYPE

SEE ALSO:

   im_and()    im_xor()    im_not()

*/

int im_or(ip_image *dest, ip_image *src)
{
    ip_log_routine(__func__);

    if (!image_valid(src) || !image_valid(dest))
	return ip_error;

    if (NOT images_compatible(dest, src))
	return ip_error;

    if (NOT (image_type_integer(src) || src->type == IM_BINARY)) {
	ip_log_error(ERR_BAD_TYPE);
	ip_pop_routine();
	return ip_error;
    }

#ifdef HAVE_SIMD

    if (IP_USE_HWACCEL) {
	ip_simd_or(dest, src);
	ip_pop_routine();
	return OK;
    }

#endif

    process_image_or(dest, src);

    ip_pop_routine();
    return OK;
}



/*

NAME:

   im_and() \- Logical AND between two images

PROTOTYPE:

   #include <ip/ip.h>

   int  im_and( dest, src )
   ip_image *dest;
   ip_image *src;

DESCRIPTION:

   Perform the bitwise logical AND between the two images 'dest' and 'src'
   and store the result in 'dest'.  Both images must be BINARY or integer
   type images.  The signed image types (IM_BYTE, IM_SHORT and IM_LONG) are
   treated as unsigned for the purposes of performing the AND operation.

ERRORS:

   ERR_NOT_SAME_TYPE
   ERR_NOT_SAME_SIZE
   ERR_BAD_TYPE

SEE ALSO:

   im_or()    im_xor()    im_not()

*/

int im_and(ip_image *dest, ip_image *src)
{
    ip_log_routine(__func__);

    if (!image_valid(src) || !image_valid(dest))
	return ip_error;

    if (NOT images_compatible(dest, src))
	return ip_error;

    if (NOT (image_type_integer(src) || src->type == IM_BINARY)) {
	ip_log_error(ERR_BAD_TYPE);
	ip_pop_routine();
	return ip_error;
    }

#ifdef HAVE_SIMD

    if (IP_USE_HWACCEL) {
	ip_simd_and(dest, src);
	ip_pop_routine();
	return OK;
    }

#endif

    process_image_and(dest, src);

    ip_pop_routine();
    return OK;
}



/*

NAME:

   im_eor() \- Logical exclusive-OR between two images

PROTOTYPE:

   #include <ip/ip.h>

   int  im_eor( dest, src )
   ip_image *dest;
   ip_image *src;

DESCRIPTION:

   Perform the bitwise logical exclusive OR between the two images 'dest'
   and 'src' and store the result in 'dest'.  Both images must be BINARY or
   integer type images.  The signed image types (IM_BYTE, IM_SHORT and
   IM_LONG) are treated as unsigned for the purposes of performing the XOR
   operation.

   This routine is defined as a macro to use im_xor().

ERRORS:

   ERR_NOT_SAME_TYPE
   ERR_NOT_SAME_SIZE
   ERR_BAD_TYPE

SEE ALSO:

   im_or()     im_and()    im_xor()    im_not()

*/


/*

NAME:

   im_xor() \- Logical exclusive-OR between two images

PROTOTYPE:

   #include <ip/ip.h>

   int  im_xor( dest, src )
   ip_image *dest;
   ip_image *src;

DESCRIPTION:

   Perform the bitwise logical exclusive OR between the two images 'dest'
   and 'src' and store the result in 'dest'.  Both images must be BINARY or
   integer type images.  The signed image types (IM_BYTE, IM_SHORT and
   IM_LONG) are treated as unsigned for the purpose of performing the XOR
   operation.

ERRORS:

   ERR_NOT_SAME_TYPE
   ERR_NOT_SAME_SIZE
   ERR_BAD_TYPE

SEE ALSO:

   im_or()     im_and()    im_not()

*/

int im_xor(ip_image *dest, ip_image *src)
{
    ip_log_routine(__func__);

    if (!image_valid(src) || !image_valid(dest))
	return ip_error;

    if (NOT images_compatible(dest, src))
	return ip_error;

    if (NOT (image_type_integer(src) || src->type == IM_BINARY)) {
	ip_log_error(ERR_BAD_TYPE);
	ip_pop_routine();
	return ip_error;
    }

#ifdef HAVE_SIMD

    if (IP_USE_HWACCEL) {
	ip_simd_xor(dest, src);
	ip_pop_routine();
	return OK;
    }

#endif

    process_image_xor(dest, src);

    ip_pop_routine();
    return OK;
}





/*

NAME:

   im_not() \- Logical NOT on an image

PROTOTYPE:

   #include <ip/ip.h>

   int  im_not( im )
   ip_image *im;

DESCRIPTION:

   Perform a bitwise logical NOT (ones complement) on a BINARY or an integer
   type image.  The signed image types (IM_BYTE, IM_SHORT and IM_LONG) are
   treated as unsigned for the purposes of performing the NOT operation.
   For a signed inversion see im_invert().

ERRORS:

   ERR_BAD_TYPE

SEE ALSO:

   im_or()     im_and()    im_xor()    im_invert()

*/

int im_not(ip_image *im)
{
    ip_log_routine(__func__);

    /* Check have valid image to work with */

    if (NOT (image_type_integer(im) || im->type == IM_BINARY)) {
	ip_log_error(ERR_BAD_TYPE);
	ip_pop_routine();
	return ip_error;
    }

    process_image_not(im);

    ip_pop_routine();
    return OK;
}
