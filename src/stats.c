/*

   Module: ip/stats.c
   Author: M.J.Cree

   Copyright (C) 1993, 1995-1998, 2000, 2002, 2007, 2015-2016 Michael J. Cree

DESCRIPTION:

   Calculate various statistics on image.

*/

#include <stdlib.h>
#include <math.h>
#include <float.h>

#include <ip/ip.h>
#include <ip/stats.h>

#include "internal.h"
#include "function_ops.h"
#include "utils.h"

#ifdef HAVE_SIMD
#include "simd/stats_simd.h"
#endif


typedef void (*improc_function_Isxx)(ip_image *d, ip_stats *s, ip_coord2d tl, ip_coord2d br);


/*

NAME:

   ip_alloc_stats() \- Allocate a statistics object

PROTOTYPE:

   #include <ip/ip.h>

   ip_stats *  ip_alloc_stats( flag )
   int flag;

DESCRIPTION:

   Allocate an ip_stats structure.  Must be done before calling either
   im_stats() or im_extrema().  The allocated ip_stats structure can be
   reused with multiple calls to im_stats() or im_extrema().

\|      ip_stats *st;
\|      st = ip_alloc_stats(NO_FLAG);
\|      im_stats(im, st, NULL);
\|      // Can read data out of ip_stats structure.
\|         ...
\|      //  ip_stats structure can be reused.
\|      im_stats(another_im, st, NULL);
\|      // Read out data from ip_stats structure.
\|         ...
\|      ip_free_stats(st);

ERRORS:

   ERR_OUT_OF_MEM

SEE ALSO:

   ip_free_stats()
   im_stats()          im_extrema()

*/



ip_stats * ip_alloc_stats(int flag)
{
    ip_stats * st;

    ip_log_routine(__func__);

    st = ip_mallocx(sizeof(ip_stats));

    /* These two fields currently unused, but provided for potential future
     * expansion.
     */

    st->id = 0;
    st->ptr = NULL;

    st->type = -1;
    st->size = (ip_coord2d){0, 0};
    st->box = (ip_box){{0, 0}, {0, 0}};

    ip_pop_routine();
    return st;
}



/*

NAME:

   ip_free_stats() \- Free a previously allocated statistics object

PROTOTYPE:

   #include <ip/ip.h>

   void  ip_free_stats( st )
   ip_stats *st;

DESCRIPTION:

   Free a previously allocated ip_stats structure.

*/


void ip_free_stats(ip_stats *st)
{
    ip_log_routine(__func__);

    if (st)
	ip_free(st);

    ip_pop_routine();
}



/*
 * Helper functions for im_stats().
 */

/* Helper macro to update main stats (intensity or magnitude) */
#define update_main(val)			\
    do {					\
	st->sum += (val);			\
	st->sumsq += (val) * (val);		\
	if ((val) < st->min) {			\
	    st->min = (val);			\
	    st->minpt.x = i; st->minpt.y = j;	\
	}					\
	if ((val) > st->max) {			\
	    st->max = (val);			\
	    st->maxpt.x = i; st->maxpt.y = j;	\
	}					\
    } while (0)


/* Helper macro to update a single component of a complex or RGB image */
#define update_field(xx, ff, val)					\
    do {								\
	st->xx ## sum.ff += (val);					\
	st->xx ## sumsq.ff += (val) * (val);				\
	if ((val) < st->xx ## min.ff) {					\
	    st->xx ## min.ff = (val);					\
	    st->xx ## minpt_ ## ff.x = i; st->xx ## minpt_ ## ff.y = j;	\
	}								\
	if ((val) > st->xx ## max.ff) {					\
	    st->xx ## max.ff = (val);					\
	    st->xx ## maxpt_ ## ff.x = i; st->xx ## maxpt_ ## ff.y = j;	\
	}								\
    } while (0)


#define generate_stats_int_function(type)				\
    static void								\
    im_stats_ ## type(ip_image *im, ip_stats *st, ip_coord2d tl, ip_coord2d br) \
    {									\
	for (int j=tl.y; j<=br.y; j++) {				\
	    type ## _type *ip = IM_ROWADR(im, j, v);			\
	    ip += tl.x;							\
	    for (int i=tl.x; i<=br.x; i++) {				\
		double tmp = (double)*ip++;				\
		update_main(tmp);					\
	    }								\
	}								\
    }


#define generate_stats_real_function(type)				\
    static void								\
    im_stats_ ## type(ip_image *im, ip_stats *st, ip_coord2d tl, ip_coord2d br) \
    {									\
	for (int j=tl.y; j<=br.y; j++) {				\
	    type ## _type *ip = IM_ROWADR(im, j, v);			\
	    ip += tl.x;							\
	    for (int i=tl.x; i<=br.x; i++) {				\
		double tmp = (double)*ip++;				\
		if (NOT isfinite(tmp)) {				\
		    st->infinities++;					\
		}else{							\
		    update_main(tmp);					\
		}							\
	    }								\
	}								\
    }



#define generate_stats_complex_function(type)				\
    static void								\
    im_stats_ ## type(ip_image *im, ip_stats *st, ip_coord2d tl, ip_coord2d br) \
    {									\
	for (int j=tl.y; j<=br.y; j++) {				\
	    type ## _type *ip = IM_ROWADR(im, j, v);			\
	    ip += tl.x;							\
	    for (int i=tl.x; i<=br.x; i++) {				\
		double mag, real, imag;					\
		real = (double)ip->r;					\
		imag = (double)ip->i;					\
		if ((NOT isfinite(real)) || (NOT isfinite(imag)))	\
		    st->infinities++;					\
		else{							\
		    mag = hypot(real, imag);				\
		    update_main(mag);					\
		}							\
		if (isfinite(real)) {					\
		    update_field(c, r, real);				\
		}							\
		if (isfinite(imag)) {					\
		    update_field(c, i, imag);				\
		}							\
		ip++;							\
	    }								\
	}								\
    }


#define generate_stats_rgb_function(type)				\
    static void								\
    im_stats_ ## type(ip_image *im, ip_stats *st, ip_coord2d tl, ip_coord2d br) \
    {									\
	for (int j=tl.y; j<=br.y; j++) {				\
	    type ## _type *ip = IM_ROWADR(im, j, v);			\
	    ip += tl.x;							\
	    for (int i=tl.x; i<=br.x; i++) {				\
		double tmp_r, tmp_g, tmp_b, tmp;			\
		tmp_r = ip->r;						\
		tmp_g = ip->g;						\
		tmp_b = ip->b;						\
		tmp = 0.299 * tmp_r + 0.587 * tmp_g + 0.114 * tmp_b;	\
		update_main(tmp);					\
		update_field(r, r, tmp_r);				\
		update_field(r, g, tmp_g);				\
		update_field(r, b, tmp_b);				\
		ip++;							\
	    }								\
	}								\
    }


#define generate_stats_rgba_function(type)				\
    static void								\
    im_stats_ ## type(ip_image *im, ip_stats *st, ip_coord2d tl, ip_coord2d br) \
    {									\
	for (int j=tl.y; j<=br.y; j++) {				\
	    type ## _type *ip = IM_ROWADR(im, j, v);			\
	    ip += tl.x;							\
	    for (int i=tl.x; i<=br.x; i++) {				\
		double tmp_r, tmp_g, tmp_b, tmp_a, tmp;			\
		tmp_r = ip->r;						\
		tmp_g = ip->g;						\
		tmp_b = ip->b;						\
		tmp_a = ip->a;						\
		tmp = 0.299 * tmp_r + 0.587 * tmp_g + 0.114 * tmp_b;	\
		update_main(tmp);					\
		update_field(r, r, tmp_r);				\
		update_field(r, g, tmp_g);				\
		update_field(r, b, tmp_b);				\
		update_field(r, a, tmp_a);				\
		ip++;							\
	    }								\
	}								\
    }

#define generate_stats_palette_function(type)				\
    static void								\
    im_stats_ ## type(ip_image *im, ip_stats *st, ip_coord2d tl, ip_coord2d br) \
    {									\
	for (int j=tl.y; j<=br.y; j++) {				\
	    uint8_t *ip = IM_ROWADR(im, j, v);				\
	    ip += tl.x;							\
	    for (int i=tl.x; i<=br.x; i++) {				\
		ip_rgb ctmp;						\
		double tmp_r, tmp_g, tmp_b, tmp;			\
		uint8_t pen = *ip++;					\
		if (pen > st->penmax)					\
		    st->penmax = pen;					\
		if (pen < st->penmin)					\
		    st->penmin = pen;					\
		ctmp = im->palette[pen];				\
		tmp_r = (double)ctmp.r;					\
		tmp_g = (double)ctmp.g;					\
		tmp_b = (double)ctmp.b;					\
		tmp = 0.299 * tmp_r + 0.587 * tmp_g + 0.114 * tmp_b;	\
		update_main(tmp);					\
		update_field(r, r, tmp_r);				\
		update_field(r, g, tmp_g);				\
		update_field(r, b, tmp_b);				\
	    }								\
	}								\
    }


generate_stats_int_function(BYTE)
generate_stats_int_function(UBYTE)
generate_stats_int_function(SHORT)
generate_stats_int_function(USHORT)
generate_stats_int_function(LONG)
generate_stats_int_function(ULONG)

generate_stats_real_function(FLOAT)
generate_stats_real_function(DOUBLE)

generate_stats_complex_function(COMPLEX)
generate_stats_complex_function(DCOMPLEX)

generate_stats_rgb_function(RGB)
generate_stats_rgb_function(RGB16)
generate_stats_rgba_function(RGBA)
generate_stats_palette_function(PALETTE)


static improc_function_Isxx im_stats_bytype[IM_TYPEMAX] = {
    [IM_BYTE] = im_stats_BYTE,
    [IM_UBYTE] = im_stats_UBYTE,
    [IM_SHORT] = im_stats_SHORT,
    [IM_USHORT] = im_stats_USHORT,
    [IM_LONG] = im_stats_LONG,
    [IM_ULONG] = im_stats_ULONG,
    [IM_FLOAT] = im_stats_FLOAT,
    [IM_DOUBLE] = im_stats_DOUBLE,
    [IM_COMPLEX] = im_stats_COMPLEX,
    [IM_DCOMPLEX] = im_stats_DCOMPLEX,
    [IM_BINARY] = im_stats_UBYTE,
    [IM_PALETTE] = im_stats_PALETTE,
    [IM_RGB] = im_stats_RGB,
    [IM_RGB16] = im_stats_RGB16,
    [IM_RGBA] = im_stats_RGBA
};


static void calc_stats(double *mean, double *sd, double *rms, double sum, double sumsq, double size)
{
    *mean = sum / size;
    *sd= sqrt( (sumsq - (sum*sum)/size) / (size-1) );
    *rms = sqrt(sumsq / size);
}



/*

NAME:

   im_stats() \- Calculate statistics on an image

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/stats.h>

   int  im_stats( im, statistics, roi )
   ip_image *im;
   ip_stats *statistics;
   ip_roi *roi;

DESCRIPTION:

   Calculate image statistics including minimum, maximum, summation of all
   pixels, mean, standard deviation, RMS, and whether (for floating-point
   types) the image contains any NaNs or infinities.  The statistics
   structure must be allocated with alloc_stats() before calling im_stats().

   Any pixels that are NaN or infinite are excluded from the calculation of
   the statistics.

   If 'roi' is specified then calculates image statistics only within the
   ROI.  If 'roi' is NULL, then uses the whole image.

   The ip_stats object 'statistics' for receiving the statistics must be
   allocated by the calling programme before calling im_stats().  See
   ip/stats.h for description of fields returned in 'statistics'.

   For COMPLEX images stats are performed on the magnitude part of the image
   and saved in the standard results area.  Statistics are also performed on
   the separate real and imaginary parts and stored in the special complex
   area of the ip_stats structure.

   For PALETTE, RGB and RGB16 images stats are performed over the intensity
   of the image given by 'Y' = 0.299'R' + 0.587'G' + 0.114'B' and stored in
   the standard results area.  Also analyses are performed on the separate
   red, green and blue fields and results are stored in the special colour
   area of the ip_stats structure.  For PALETTE images the minimum pen
   number and maximum pen number used in the palette are also calculated.

   See stats.h for a description of the ip_stats object.

   Note that the min and max field of the ip_stats object is treated
   differently by im_extrema() than im_stats() for multi-component image
   types.  In im_stats() the magnitude of the complex number, or the
   intensity of the colour, is used for calculating the min and max fields
   (and the mean, etc., sub-fields).

ERRORS:

   ERR_OUT_OF_MEM
   ERR_UNSUPPORTED_TYPE
   ERR_BAD_ROI

SEE ALSO:

   ip_alloc_stats()     ip_free_stats()        ip_alloc_roi()
   im_extrema()         im_hist()

*/


int im_stats(ip_image *im, ip_stats *st, ip_roi *roi)
{
    ip_coord2d topl, botr;
    int ownroi;
    double size;

    ip_log_routine(__func__);

    if (NOT image_valid(im)) {
	ip_pop_routine();
	return ip_error;
    }

    ownroi = FALSE;
    if (roi == NULL) {
	if ((roi = ip_alloc_roi(ROI_IMAGE,im)) == NULL) {
	    ip_pop_routine();
	    return ip_error;
	}
	ownroi = TRUE;
    }

    if (NOT roi_in_image(im,roi)) {
	/* Don't need to free own roi here since it can't cause this error */
	return ip_error;
    }

    st->max = -DBL_MAX;
    st->min = DBL_MAX;
    st->sum = st->sumsq = 0.0;
    st->infinities = 0;

    if (type_complex(im->type)) {
	st->cmax.i = st->cmax.r = -DBL_MAX;
	st->cmin.i = st->cmin.r = DBL_MAX;
	st->csum.r = st->csum.i = st->csumsq.r = st->csumsq.i = 0.0;
    }

    if (image_type_clrim(im)) {
	st->rmax.r = st->rmax.g = st->rmax.b = st->rmax.a = 0;
	st->rmin.r = st->rmin.g = st->rmin.b = st->rmin.a = UINT16_MAX;
	st->rsum.r = st->rsum.g = st->rsum.b = st->rsum.a = 0.0;
	st->rsumsq.r = st->rsumsq.g = st->rsumsq.b = st->rsumsq.a = 0.0;
	st->penmax = 0;
	st->penmin = 255;
    }

    /*
     * Set these to zero just in case image is uniformly one value at the
     * base type minimum or maximum value, since stats loop misses setting
     * these in that instance.
     */
    st->minpt = st->maxpt = (ip_coord2d){0, 0};
    st->cminpt_r = st->cminpt_i = st->cmaxpt_r = st->cmaxpt_i = (ip_coord2d){0, 0};
    st->rminpt_r = st->rminpt_g = st->rminpt_b = st->rminpt_a = (ip_coord2d){0, 0};
    st->rmaxpt_r = st->rmaxpt_g = st->rmaxpt_b = st->rmaxpt_a = (ip_coord2d){0, 0};

    topl = roi_get(ROI_ORIGIN,roi).pt;
    botr = roi_get(ROI_BOTRIGHT,roi).pt;

    /* Calculate base stats which are dependent on image type */

    if (im_stats_bytype[im->type])
	im_stats_bytype[im->type](im, st, topl, botr);
    else
	ip_log_error(ERR_NOT_IMPLEMENTED);

    /* Get number of pixels in ROI */

    size = (double)ip_size_numpixels(ip_botr_size(topl, botr));

    /* General image characteristics */

    st->type = im->type;
    st->size = im->size;
    st->box = ip_botr_box(topl, botr);

    /* Image statistics */

    if (im->type == IM_BINARY) {
	st->min = st->min ? 1.0 : 0.0;
	st->max = st->max ? 1.0 : 0.0;
	st->sum /= 255.0;
	st->sumsq /= (255.0*255.0);
    }

    /* Scalar image stats, or complex mag stats, or rgb intensity stats. */
    calc_stats(&st->mean, &st->stddev, &st->rms, st->sum, st->sumsq, size);

    if (image_type_complex(im)) {
	/* Complex image statistics */
	calc_stats(&st->cmean.r, &st->cstddev.r, &st->crms.r, st->csum.r, st->csumsq.r, size);
	calc_stats(&st->cmean.i, &st->cstddev.i, &st->crms.i, st->csum.i, st->csumsq.i, size);
    }

    if (image_type_clrim(im)) {
	/* Colour image stats */
	calc_stats(&st->rmean.r, &st->rstddev.r, &st->rrms.r, st->rsum.r, st->rsumsq.r, size);
	calc_stats(&st->rmean.g, &st->rstddev.g, &st->rrms.g, st->rsum.g, st->rsumsq.g, size);
	calc_stats(&st->rmean.b, &st->rstddev.b, &st->rrms.b, st->rsum.b, st->rsumsq.b, size);
	if (im->type == IM_RGBA) {
	    calc_stats(&st->rmean.a, &st->rstddev.a, &st->rrms.a, st->rsum.a, st->rsumsq.a, size);
	}
    }

    if (ownroi) ip_free_roi(roi);

    ip_pop_routine();
    return ip_error;
}



/*
 * Helper functions for im_extrema()
 */


/* Helper macro to update main extrema (intensity or magnitude) */
#define update_minmax(val)			\
    do {					\
	if ((val) < lmin) {			\
	    lmin = (val);			\
	    st->minpt.x = i; st->minpt.y = j;	\
	}					\
	if ((val) > lmax) {			\
	    lmax = (val);			\
	    st->maxpt.x = i; st->maxpt.y = j;	\
	}					\
    } while (0)

/* And to update extrema of a particular component of a complex or rgb type. */
#define update_minmax_field(xx, ff, val)	\
    do {					\
	if ((val) < lmin.ff) {			\
	    lmin.ff = (val);			\
	    st->xx ## minpt_ ## ff.x = i;	\
	    st->xx ## minpt_ ## ff.y = j;	\
	}					\
	if ((val) > lmax.ff) {			\
	    lmax.ff = (val);			\
	    st->xx ## maxpt_ ## ff.x = i;	\
	    st->xx ## maxpt_ ## ff.y = j;	\
	}					\
    } while (0)


#define generate_extrema_int_function(type, tmin, tmax)			\
static void im_extrema_ ## type(ip_image *im, ip_stats *st, ip_coord2d tl, ip_coord2d br)\
{									\
    type ## _type lmin = tmax;						\
    type ## _type lmax = tmin;						\
    for (int j=tl.y; j<=br.y; j++) {					\
	type ## _type *ip = IM_ROWADR(im, j, v);			\
	ip += tl.x;							\
	for (int i=tl.x; i<=br.x; i++) {				\
	    update_minmax(*ip);						\
	    ip++;							\
	}								\
    }									\
    st->min = (double)lmin;						\
    st->max = (double)lmax;						\
}


#define generate_extrema_real_function(type, tmin, tmax)		\
static void im_extrema_ ## type(ip_image *im, ip_stats *st, ip_coord2d tl, ip_coord2d br)\
{									\
    type ## _type lmin = tmax;						\
    type ## _type lmax = tmin;						\
    for (int j=tl.y; j<=br.y; j++) {					\
	type ## _type *ip = IM_ROWADR(im, j, v);			\
	ip += tl.x;							\
	for (int i=tl.x; i<=br.x; i++) {				\
	    if (NOT isfinite(*ip))					\
		st->infinities = TRUE;					\
	    update_minmax(*ip);						\
	    ip++;							\
	}								\
    }									\
    st->min = (double)lmin;						\
    st->max = (double)lmax;						\
}


#define generate_extrema_complex_function(type, tmin, tmax)		\
static inline void im_extrema_ ## type(ip_image *im, ip_stats *st, ip_coord2d tl, ip_coord2d br)\
{									\
    type ## _type lmin;							\
    type ## _type lmax;							\
    lmin.r = lmin.i = tmax;						\
    lmax.r = lmax.i = tmin;						\
    for (int j=tl.y; j<=br.y; j++) {					\
	type ## _type *ip = IM_ROWADR(im,j,v);				\
	ip += tl.x;							\
	for (int i=tl.x; i<=br.x; i++) {				\
	    if ((NOT isfinite(ip->r)) || (NOT isfinite(ip->i)))		\
		st->infinities = TRUE;					\
	    update_minmax_field(c, r, ip->r);				\
	    update_minmax_field(c, i, ip->i);				\
	    ip++;							\
	}								\
    }									\
    st->cmin.r = (double)lmin.r;					\
    st->cmin.i = (double)lmin.i;					\
    st->cmax.r = (double)lmax.r;					\
    st->cmax.i = (double)lmax.i;					\
    st->min = MIN(st->cmin.r, st->cmin.i);				\
    st->max = MAX(st->cmax.r, st->cmax.i);				\
}


#define generate_extrema_rgb_function(type, tmin, tmax)			\
static void im_extrema_ ## type(ip_image *im, ip_stats *st, ip_coord2d tl, ip_coord2d br)\
{									\
    type ## _type lmin;							\
    type ## _type lmax;							\
    lmin.r = lmin.g = lmin.b = tmax;					\
    lmax.r = lmax.g = lmax.b = tmin;					\
    for (int j=tl.y; j<=br.y; j++) {					\
	type ## _type *ip = IM_ROWADR(im,j,v);				\
	ip += tl.x;							\
	for (int i=tl.x; i<=br.x; i++) {				\
	    update_minmax_field(r, r, ip->r);				\
	    update_minmax_field(r, g, ip->g);				\
	    update_minmax_field(r, b, ip->b);				\
	    ip++;							\
	}								\
    }									\
    st->rmin.r = (uint16_t)lmin.r;					\
    st->rmin.g = (uint16_t)lmin.g;					\
    st->rmin.b = (uint16_t)lmin.b;					\
    st->rmax.r = (uint16_t)lmax.r;					\
    st->rmax.g = (uint16_t)lmax.g;					\
    st->rmax.b = (uint16_t)lmax.b;					\
    st->min = MIN(st->rmin.r, st->rmin.g);				\
    st->min = MIN(st->min, st->rmin.b);					\
    st->max = MAX(st->rmax.r, st->rmax.g);				\
    st->max = MAX(st->max, st->rmax.b);					\
}


generate_extrema_int_function(BYTE, INT8_MIN, INT8_MAX)
generate_extrema_int_function(UBYTE, 0, UINT8_MAX)
generate_extrema_int_function(SHORT, INT16_MIN, INT16_MAX)
generate_extrema_int_function(USHORT, 0, UINT16_MAX)
generate_extrema_int_function(LONG, INT32_MIN, INT32_MAX)
generate_extrema_int_function(ULONG, 0, UINT32_MAX)

generate_extrema_real_function(FLOAT, -FLT_MAX, FLT_MAX)
generate_extrema_real_function(DOUBLE, -DBL_MAX, DBL_MAX)

generate_extrema_complex_function(COMPLEX, -FLT_MAX, FLT_MAX)
generate_extrema_complex_function(DCOMPLEX, -DBL_MAX, DBL_MAX)

generate_extrema_rgb_function(RGB, 0, UINT8_MAX)
generate_extrema_rgb_function(RGB16, 0, UINT16_MAX)


static void im_extrema_RGBA(ip_image *im, ip_stats *st, ip_coord2d tl, ip_coord2d br)
{
    ip_rgba lmin;
    ip_rgba lmax;
    lmin.r = lmin.g = lmin.b = lmin.a = UINT8_MAX;
    lmax.r = lmax.g = lmax.b = lmax.a = 0;
    for (int j=tl.y; j<=br.y; j++) {
	ip_rgba *ip = IM_ROWADR(im, j, v);
	ip += tl.x;
	for (int i=tl.x; i<=br.x; i++) {
	    update_minmax_field(r, r, ip->r);
	    update_minmax_field(r, g, ip->g);
	    update_minmax_field(r, b, ip->b);
	    update_minmax_field(r, a, ip->a);
	}
    }
    st->rmin.r = (double)lmin.r;
    st->rmin.g = (double)lmin.g;
    st->rmin.b = (double)lmin.b;
    st->rmin.a = (double)lmin.a;
    st->rmax.r = (double)lmax.r;
    st->rmax.g = (double)lmax.g;
    st->rmax.b = (double)lmax.b;
    st->rmax.a = (double)lmax.a;
    st->min = MIN(st->rmin.r, st->rmin.g);
    st->min = MIN(st->min, st->rmin.b);
    st->min = MIN(st->min, st->rmin.a);
    st->max = MAX(st->rmax.r, st->rmax.g);
    st->max = MAX(st->max, st->rmax.b);
    st->max = MAX(st->max, st->rmax.a);
}



static improc_function_Isxx im_extrema_bytype[IM_TYPEMAX] = {
    [IM_BYTE] = im_extrema_BYTE,
    [IM_UBYTE] = im_extrema_UBYTE,
    [IM_SHORT] = im_extrema_SHORT,
    [IM_USHORT] = im_extrema_USHORT,
    [IM_LONG] = im_extrema_LONG,
    [IM_ULONG] = im_extrema_ULONG,
    [IM_FLOAT] = im_extrema_FLOAT,
    [IM_DOUBLE] = im_extrema_DOUBLE,
    [IM_COMPLEX] = im_extrema_COMPLEX,
    [IM_DCOMPLEX] = im_extrema_DCOMPLEX,
    [IM_BINARY] = im_extrema_UBYTE,
    [IM_PALETTE] = im_extrema_UBYTE,
    [IM_RGB] = im_extrema_RGB,
    [IM_RGB16] = im_extrema_RGB16,
    [IM_RGBA] = im_extrema_RGBA
};


/*

NAME:

   im_extrema() \- Find the minimum and maximum value in an image

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/stats.h>

   int  im_extrema( im, stats, roi )
   ip_image *im;
   ip_stats *stats;
   ip_roi *roi;

DESCRIPTION:

   Find the extrema, that is the minimum and maximum pixel values, within
   the image 'im'.  Im_extrema() stores the results into the ip_stats object
   pointed to by 'stats'. It should be allocated by the ip_alloc_stats()
   function.  Only the fields type, size, infinities, min, max, minpt and
   maxpt are updated on scalar images.  For COMPLEX images the fields
   infinities, cmin, cmax, cminpt_r, cminpt_i, cmaxpt_r and cmaxpt_i are
   updated.  For RGB/RGB16/RGBA images the fields rmin, rmax, rminpt_r,
   rminpt_g, rminpt_b, rmaxpt_r, rmaxpt_g and rmaxpt_b (also rmaxpt_a and
   rminpt_a if RGBA) are updated.

   For multi-component images (complex and colour images) the min and max
   field is set to the minimum (respectively maximum) of all the separate
   components.  Thus, for example, for COMPLEX images min is set to the
   minimum of cmin.r and cmin.i.  Note that this is different behaviour than
   im_stats().

   If 'roi' is NULL then the extrema are calculated over the whole image.
   If a ROI is passed the extrema are calculated only over the ROI.

   This primary benefit of this routine is that it is faster than
   im_stats(), as fewer statistics are calculated and the min and max
   comparisions on each field of the image type are calculated at image
   type, not at double type, thus saving a conversion to double on each
   pixel.  In real and complex images if any infinities or NaNs are present
   in the image then infinities is set true.  Also the infinities are
   considered in the min and max reckoning so min and max can be returned
   with the value infinity or minus infinity.  This is a different behaviour
   than the im_stats() routine.

ERRORS:

   ERR_BAD_PARAMETER    - If ROI extends out of image bounds.
   ERR_OUT_OF_MEM

SEE ALSO:

   ip_alloc_stats()      ip_free_stats()           ip_alloc_roi()
   im_stats()            im_hist()

*/

int im_extrema(ip_image *im, ip_stats *st, ip_roi *roi)
{
    ip_coord2d topl, botr;     /* Top-left and bottom-right points of ROI */
    int ownroi = FALSE;	       /* If we allocated ROI internally */

    ip_log_routine(__func__);

    if (NOT image_valid(im)) {
	ip_pop_routine();
	return ip_error;
    }

    if (roi == NULL) {
	if ((roi = ip_alloc_roi(ROI_IMAGE, im)) == NULL) {
	    ip_pop_routine();
	    return ip_error;
	}
	ownroi = TRUE;
    }

    if (NOT roi_in_image(im,roi)) {
	/* Don't need to free own roi here since it can't cause this error */
	return ip_error;
    }

    st->type = im->type;
    st->size = im->size;
    st->infinities = FALSE;

    /*
     * Set these to zero just in case image is completely constant at either
     * base type minimum or maximum value, since extrema loop misses setting
     * these in that instance.
     */
    st->minpt = st->maxpt = (ip_coord2d){0, 0};

    topl = roi_get(ROI_ORIGIN,roi).pt;
    botr = roi_get(ROI_BOTRIGHT,roi).pt;

    if (im_extrema_bytype[im->type])
	im_extrema_bytype[im->type](im, st, topl, botr);
    else{
	ip_log_error(ERR_NOT_IMPLEMENTED);
	ip_pop_routine();
	return ERR_NOT_IMPLEMENTED;
    }

    if (im->type == IM_BINARY) {
	st->min /= 255;
	st->max /= 255;
    }

    if (im->type == IM_PALETTE) {
	st->penmin = (uint8_t)st->min;
	st->penmin = (uint8_t)st->max;
    }

    if (ownroi) ip_free_roi(roi);

    ip_pop_routine();

    return OK;
}



/*
 * im_differ() implementation follows
 */


#if MACHINE_NATURAL_SIZE == 4
#define NAT_TYPE uint32_t
#define HIGH_BYTE 0xff000000U
#elif MACHINE_NATURAL_SIZE == 8
#define NAT_TYPE uint64_t
#define HIGH_BYTE 0xff00000000000000UL
#else
#error Only support 32-bit and 64-bit targets (has 'make config' been run?)
#endif


/*
 * Analyse images by the largest natural type available; uint32_t on 32-bit
 * machines and uint64_t on 64-bit machines.  If the bit pattern is
 * different in position on an image row, except in the row padding, we
 * determine that the image data of the two images differ and return true.
 * The routine short-cuts and exits as soon as a difference is found.
 *
 * Because we look at the bit pattern this means that the various types of
 * NaN in floating point images are each considered different to all other
 * NaNs, even if they are a NaN of the same type.
 */

static int image_differ_generic(ip_image *d, ip_image *s)
{
    /*
     * Calculate the number uint32_t/uint64_t that fit in the image
     * row. Also calculate the extra pixels at the end that do not full a
     * full uint32_t/uint64_t --- this determines mask to use for last
     * uint32_t/uint64_t access in row.
     */

    int same = 1;
    size_t rowlen = ip_type_size(d->type) * (size_t)d->size.x;
    int steps = rowlen / MACHINE_NATURAL_SIZE;
    int extra = rowlen - steps * MACHINE_NATURAL_SIZE;
    NAT_TYPE mask;

    /*
     * The mask for last access in row depends on endianess.
     */
#ifdef HAVE_LITTLE_ENDIAN
    mask = 0;
    for (int i=0; i<extra; i++) {
	mask <<= 8;
	mask |= 0xff;
    }
#else
    mask = 0;
    for (int i=0; i<extra; i++) {
	mask >>= 8;
	mask |= HIGH_BYTE;
    }
#endif

    /*
     * Efficient search for any difference in image data in image.
     */
    for (int j=0; same && j<d->size.y; j++) {
	NAT_TYPE *dptr = IM_ROWADR(d, j, v);
	NAT_TYPE *sptr = IM_ROWADR(s, j, v);
	for (int i=0; i<steps; i++) {
	    if (*dptr++ != *sptr++) {
		same = 0;
		break;
	    }
	}
	/* Finish off last few entries */
	if (same && mask) {
	    NAT_TYPE dval, sval;

	    dval = *dptr & mask;
	    sval = *sptr & mask;
	    if (dval != sval)
		same = 0;
	}
    }
    return 1 - same;
}



/*

NAME:

   im_differ() \- true if two images differ

PROTOTYPE:

   #include <ip/ip.h>

   int  im_differ( im1, im2 )
   ip_image *im1;
   ip_image *im2;

DESCRIPTION:

   Compare two images together and return -1 if an error occurs (i.e. they
   are not compatible), 1 if they differ, and 0 if they are the same type,
   same size and exactly the same pixel data.

   Shorts circuits and exits as soon as it sees a difference in the image.
   Considers the various types of floating point NaNs to all be different
   from one another, that is, the check is that the bit pattern of each
   pixel in image 'im1' exactly matches the bit pattern of the corresponding
   pixel in image 'im2'.

ERRORS:

   ERR_NOT_SAME_SIZE
   ERR_NOT_SAME_TYPE
   ERR_BAD_TYPE

*/


int im_differ(ip_image *im1, ip_image *im2)
{
    int differ;

    ip_log_routine(__func__);

    if (!image_valid(im1) || !image_valid(im2))
	return -1;

    if (NOT images_compatible(im1, im2))
	return -1;

#ifdef HAVE_SIMD
    if (IP_USE_HWACCEL && ip_simd_image_differ) {
	differ = ip_simd_image_differ(im1, im2);
	ip_pop_routine();
	return differ;
    }
#endif

    differ = image_differ_generic(im1, im2);

    ip_pop_routine();
    return differ;
}
