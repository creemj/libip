/*
 * Module: ip/objgrow.c
 * Author: Michael J. Cree
 *
 * Copyright (C) 2008, 2018 by Michael J. Cree
 *
 * Routines to create lists of objects found in images.
 *
 * ENTRY POINTS:
 *
 * im_create_object_list()
 */

#include <stdlib.h>
#include <string.h>

#include <ip/ip.h>
#include <ip/object.h>

#include "internal.h"
#include "intobject.h"
#include "utils.h"

/* Define to turn on printing out debugging info */
/* #define DEBUG */

#define DEFAULT_RLE_ARRAY_SIZE 1000
#define DEFAULT_LM_ARRAY_SIZE 256


/*
 * The following RLE types are not the same as used in the object code, but
 * are useful in this module for the process of analysing an image for runs
 * and collecting runs into objects.
 */

/* The runlength encoding */
typedef struct rle {
    int row;
    int label;
    int start;
    int length;
    double weight;
} rle_t;


/* An array of rle elements */
typedef struct rle_array {
    int allocated;	        /* Max number of elements the array can hold  */
    int num;			/* Number of elements in array */
    rle_t *rle;			/* The rle array itself */
} rle_array_t;


/* The label match element */
typedef struct lm {
    int l1;
    int l2;
} lm_t;


/* An array of label match elements */
typedef struct lm_array {
    int allocated;	        /* Max number of elements the array can hold  */
    int num;			/* Number of elements in array */
    lm_t *lm;			/* The label match array itself */
} lm_array_t;



/* Allocate a run-length array with room for size elements */
static rle_array_t *alloc_run_length_array(int size)
{
    rle_array_t *ra;

    ra = ip_mallocx(sizeof(rle_array_t));
    if (ra == NULL) return NULL;

    ra->rle = ip_mallocx(sizeof(rle_t) * size);
    if (ra->rle == NULL) {
	ip_free(ra);
	return NULL;
    }

    ra->allocated = size;
    ra->num = 0;

    return ra;
}


/*
 * Extend a run-length array to hold size more objects.
 * Returns NULL if extension of array unsuccessful, possible reasons are:
 *     - out of memory
 *     - passed size is negative implying a reduction in size!
 * The old array remains intact if NULL returned.
 */

static rle_array_t *extend_run_length_array(rle_array_t *ra, int size)
{
    rle_t *nptr;

    if (size < 0) return NULL;
    if (size == 0) return ra;

    if ((nptr = ip_reallocx(ra->rle, sizeof(rle_t)*(ra->allocated+size))) == NULL)
	return NULL;

    ra->rle = nptr;
    ra->allocated = size;

    return ra;
}


/* Free an allocated run length array */
static void free_run_length_array(rle_array_t *ra)
{
    if (ra) {
	if (ra->rle) ip_free(ra->rle);
	free(ra);
    }
}

/* Insert a run into the run length array; returns TRUE if successful */
static int rla_insert_run(rle_array_t *ra, int label, int x, int y, int length)
{
    if (ra->num >= ra->allocated) {
	ra = extend_run_length_array(ra, ra->allocated+DEFAULT_RLE_ARRAY_SIZE);
	if (ra == NULL)
	    return 0;
    }
    rle_t *rle = ra->rle + ra->num;
    rle->row = y;
    rle->start = x;
    rle->length = length;
    rle->label = label;
    ra->num++;
    return 1;
}


/*
 * Allocate a label match array.
 * Holds elements that make a link from label 1 (l1) to label 2 (l2)
 */

static lm_array_t *alloc_label_match_array(int size)
{
    lm_array_t *lma;

    if ((lma = ip_mallocx(sizeof(lm_array_t))) == NULL)
	return NULL;

    if ((lma->lm = ip_mallocx(sizeof(lm_t) * size)) == NULL) {
	ip_free(lma);
	return NULL;
    }

    lma->allocated = size;
    lma->num = 0;

    return lma;
}


#ifdef DEBUG
/* Helper function for debugging */

static void dump_lma(char *msg, lm_array_t *lma)
{
    printf("%s: lma array of size %d at %p.\n", msg, lma->num, lma);
    for (int j=0; j<lma->num; j++) {
	printf("   %2d -> %d\n", lma->lm[j].l1, lma->lm[j].l2);
    }
    printf("\n");
}
#define Debug(x) x
#else
#define Debug(x)
#endif



/*
 * Extend a match array to hold size more objects.
 * Returns NULL if extension of array unsuccessful, possible reasons are:
 *    - out of memory
 *    - passed size is negative implying a reduction in size!
 * The old array remains intact if NULL returned.
 */

static lm_array_t *extend_label_match_array(lm_array_t *lma, int size)
{
    lm_t *nptr;

    if (size < 0) return NULL;
    if (size == 0) return lma;

    if ((nptr = ip_reallocx(lma->lm, sizeof(lm_t)*(lma->allocated+size))) == NULL)
	return NULL;

    lma->lm = nptr;
    lma->allocated = size;

    return lma;
}



static void free_label_match_array(lm_array_t *lma)
{
    if (lma) {
	if (lma->lm) ip_free(lma->lm);
	ip_free(lma);
    }
}

/* Append a label match to the end of the label match array */
static int append_label_match(lm_array_t *lma, int l1, int l2)
{
    if (lma->num == lma->allocated) {
	lma = extend_label_match_array(lma, DEFAULT_LM_ARRAY_SIZE);
	if (lma == NULL) return FALSE;
    }
    if (l1 < l2) {
	int tmp = l1;
	l1 = l2;
	l2 = tmp;
    }
    lma->lm[lma->num].l1 = l1;
    lma->lm[lma->num].l2 = l2;
    lma->num++;
    return TRUE;
}


/* Compare label match first entry against key */
static int cmp_label_match_key(const void *entry, const void *key_p)
{
    const lm_t *lm = entry;
    int key = *(int *)key_p;

    if (lm->l1 < key)
	return -1;
    if (lm->l1 > key)
	return 1;
    else
	return 0;
}


static lm_t * bsearch_label_match_array(lm_array_t *lma, int key)
{
    return bsearch(&key, lma->lm, lma->num, sizeof(lm_t), cmp_label_match_key);
}


/* Find item or return index of next item in sorted array */
static int bfind_label_match_array(lm_array_t *lma, int key)
{
    int low = 0;
    int high = lma->num;
    int idx = 0;		/* STFCU */

    while (low < high) {
	idx = (high + low) / 2;
	if (key > lma->lm[idx].l1) {
	    low = idx+1;
	}else if (key < lma->lm[idx].l1) {
	    high = idx;
	}else
	    break;
    }

    if (low == high) 
	idx = low;

    while (idx>=1 && lma->lm[idx-1].l1 == key)
	idx--;
    return idx;
}



/* Chase through a chain to its end in a sorted array */
static lm_t *chain_end_label_match_array(lm_array_t *lma, int key)
{
    lm_t *this, *next;

    next = bsearch_label_match_array(lma, key);
    this = next;
    while (next) {
	next = bsearch_label_match_array(lma, this->l2);
	if (next) this = next;
    }
    return this;
}

/* Chase through a chain to its end in a sorted array; return idx of last link */
static int chain_end_label_match_array_idx(lm_array_t *lma, int key)
{
    lm_t *next;

    next = chain_end_label_match_array(lma, key);
    if (next)
	return next->l2;
    else
	return key;
}


/* Insert label match to keep array sorted */
static int insert_label_match(lm_array_t *lma, int l1, int l2)
{
    if (lma->num >= lma->allocated
			&& !extend_label_match_array(lma, lma->num < 1024 ? lma->num : 1024))
	return -1;

    if (l1 < l2) {
	int tmp = l1;
	l1 = l2;
	l2 = tmp;
    }

    Debug(printf("Insert: (%d,%d)\n", l1, l2); fflush(stdout));
    /*
     * Usually label match to be inserted is actually biggest value thus
     * should go at end of array.  Check for that case first.
     */
    if ((lma->num == 0) || (lma->lm[lma->num-1].l1 < l1)) {
	append_label_match(lma, l1, l2);
	return lma->num-1;
    }

    /* Doesn't go at end; find insertion point */
    Debug(printf("... searching for %d in lma...", l1); fflush(stdout));
    int k = bfind_label_match_array(lma, l1);
    Debug(printf(" found at %d.\n", k); fflush(stdout));

    memmove(lma->lm+k+1, lma->lm+k, (lma->num - k)*sizeof(lm_t));
    lma->lm[k].l1 = l1;
    lma->lm[k].l2 = l2;
    lma->num++;

    return k;
}


/*
 * Is a run adjacent to another run?
 *    - As long as they touch at ends (even if no overlap)
 *    - ignores row coordinates
 */
static int run_adjacent_x(rle_t *rle1, rle_t *rle2)
{
    if (rle1->start - rle2->start > rle2->length)
	return FALSE;
    if (rle2->start - rle1->start > rle1->length)
	return FALSE;
    return TRUE;
}

/*
 * An array to hold info gathered from the rle array, one entry for each
 * object.
 */

struct obj_info {
    int first_run_idx;		/* idx into rle array (origin run) */
    int num_runs;
    ip_coord2d tl;
    ip_coord2d br;
    ip_object *obj;
};


/* 
 * Analyse run-length array for 8-connected objects, and label each
 * run-length entry according to the object it is a part of.  Returns
 * complete object list constructed from the un array.
 */
static ip_objlist * connect_and_label_runs(rle_array_t *ra) 
{
    int label = 1;
    int row_below, curr;
    lm_array_t *lma;
    int *obj_label;

    /* Protect against empty list */

    if (ra->num == 0) return 0;

    /* Allocate an array to hold matching labels within one object. */

    if ((lma = alloc_label_match_array(DEFAULT_LM_ARRAY_SIZE)) == NULL)
	return 0;

    /*
     * Walk through runs from start to end.  Label a run never seen before
     * then propagate label down to all runs adjacent and below it.
     *
     * If objects have two or more spikes poking upwards (at some stage)
     * then some spikes may be labelled with wrong number. Will fix in a
     * second pass through the run array.
     */
    curr = 0;

    while (curr < ra->num) {
	row_below = ra->rle[curr].row+1;
	if (ra->rle[curr].label == 0) {
	    ra->rle[curr].label = label++;
	}

	/*
	 * Walk further through rows that are same image row or one below.
	 * If adjacent to current run and not labelled then propagate label.
	 * If adjacent to current run and labelled note presence of multiple
	 * labels for one object.
	 */
	for (int i=curr+1; i<ra->num && ra->rle[i].row <= row_below; i++) {
	    if (run_adjacent_x(ra->rle+curr, ra->rle+i)) {
		if (ra->rle[i].label > 0) {
		    /* Alreay labelled so add into label match array */
		    if (ra->rle[i].label != ra->rle[curr].label) {
			insert_label_match(lma, ra->rle[curr].label,
					   ra->rle[i].label);
		    }
		}else{
		    /* Unlabelled run, but same object so propagate label. */
		    ra->rle[i].label = ra->rle[curr].label;
		}
	    }
	}
       
	curr++;
    }

    Debug(dump_lma("original", lma));

    /* Find entries that have multiple matches */
    for (int k=0; k<lma->num-1; k++) {
	if (lma->lm[k].l1 == lma->lm[k+1].l1) {
	    int low;
	    int high;
	    if (lma->lm[k].l2 < lma->lm[k+1].l2) {
		low = lma->lm[k].l2;
		high = lma->lm[k+1].l2;
	    }else{
		low = lma->lm[k+1].l2;
		high = lma->lm[k].l2;
	    }
	    high = chain_end_label_match_array_idx(lma, high);
	    low = chain_end_label_match_array_idx(lma, low);
	    if (high != low) {
		insert_label_match(lma, high, low);
		k = k + 1;
	    }
	}
    }

    Debug(dump_lma("extended", lma));

    /* Allocate array that has correct number of entries */
    label--;
    if ((obj_label = ip_mallocx(label * sizeof(int))) == NULL) {
	/* Pooh - no more memory at a bad point */
	free_label_match_array(lma);
	return 0;
    }
    memset(obj_label, 0, label * sizeof(int));

    /* Initialise label-match array */
    obj_label[0] = 1;		/* First object must have label 1 */
    int new_label = 1;
    for (int i=1; i<label; i++) {
	lm_t *lm;

	lm = chain_end_label_match_array(lma, i+1);
	if (lm) {
	    obj_label[i] = obj_label[lm->l2-1];
	}else{
	    obj_label[i] = ++new_label;
	}
    }

#ifdef DEBUG
    printf("final: match array of size %d with %d objects.\n", label, new_label);
    for (int i=0; i<label; i++) {
	printf("  %2d -> %2d\n", i+1, obj_label[i]);
    }
#endif
    free_label_match_array(lma);

    /*
     * Array to hold the number of runs and the row of the first run of each
     * object.  new_label holds the number of objects plus one but because
     * labels start at one we allocate one extra spot in array.
     */

    struct obj_info *oinfo = ip_mallocx(new_label * sizeof(struct obj_info));
    if (!oinfo) {
	ip_free(obj_label);
	return 0;
    }
    memset(oinfo, 0, new_label * sizeof(struct obj_info));

    /* Now walk through rle array re-labelling all runs */
    for (int i=0; i<ra->num; i++) {
	/* Update label of run */
	int objlabel = obj_label[ra->rle[i].label-1] - 1;
	ra->rle[i].label = objlabel;
	/* Record index of first run with that label */ 
	if (!oinfo[objlabel].num_runs) {
	    oinfo[objlabel].first_run_idx = i;
	    oinfo[objlabel].tl.y = ra->rle[i].row;
	}
	/* Record number of runs with that label */
	oinfo[objlabel].num_runs++;
	/* Record row of last run */
	oinfo[objlabel].br.y = ra->rle[i].row;
	/* Update left and right edge of object */
	int end_x = ra->rle[i].start + ra->rle[i].length - 1;
	int start_x = ra->rle[i].start + 1; /* intentionally off by one */
	if (end_x > oinfo[objlabel].br.x)
	    oinfo[objlabel].br.x = end_x;
	if (oinfo[objlabel].tl.x==0 || start_x < oinfo[objlabel].tl.x)
	    oinfo[objlabel].tl.x = start_x;				     
    }
    ip_free(obj_label);

    /* Now can allocate object list */
    ip_objlist *objlist = ip_alloc_object_list(NULL);
    for (int j=0; j<new_label; j++) {
	int start_y = ra->rle[oinfo[j].first_run_idx].row;
	int start_x = ra->rle[oinfo[j].first_run_idx].start;
	int end_y = oinfo[j].br.y;
	int num_runs = oinfo[j].num_runs;
	Debug(printf("obj %d: (%d,%d) - (--,%d) of %d runs.\n",
		     j, start_x, start_y, end_y, num_runs));

	ip_object *obj = ip_alloc_object((ip_coord2d){start_x, start_y});
	if (!obj) goto clr_disaster;
	oinfo[j].obj = obj;

	obj->numrows = end_y - start_y + 1;
	if ((obj->row = ip_mallocx(obj->numrows * sizeof(rowtype))) == NULL) {
	    ip_free_object(obj);
	    goto clr_disaster;
	};
	memset(obj->row, 0, obj->numrows*sizeof(rowtype));
	SET_COMPLETE(obj);	/* to ensure ip_free_object on error does right thing */

	if ((obj->row[0].rle = ip_mallocx(num_runs * sizeof(rletype))) == NULL) {
	    ip_free_object(obj);
	    goto clr_disaster;
	};

	obj->bbox.tl = oinfo[j].tl;
	obj->bbox.br = oinfo[j].br;
	obj->bbox.tl.x--;	/* Subtract off one added above */
	obj->origin.x = start_x;
	obj->origin.y = obj->bbox.tl.y;
	SET_RUNLEN(obj);
	SET_OBJCOORD(obj);
	SET_COMPLETE(obj);

	ip_append_object(objlist, obj);	
    }

    /* Now walk rle array for the last time; fill in the objects */
    for (int i=0; i<ra->num; i++) {
	ip_object *obj = oinfo[ra->rle[i].label].obj;
	int row_idx = ra->rle[i].row - obj->origin.y;
	int rle_idx = obj->row[0].allocated++;
	int run_start = ra->rle[i].start - obj->origin.x;
	int run_len = ra->rle[i].length;
	rowtype *row = obj->row + row_idx;
	if (!row->rle) row->rle = obj->row[0].rle + rle_idx;
	row->rle[row->numentries].start = run_start;
	row->rle[row->numentries].length = run_len;
	row->numentries++;
	obj->numpixels += run_len;
    }

    ip_free(oinfo);
    return objlist;

 clr_disaster:
    /* Got error; try to unravel and return; hope no more shitty errors occur! */
    if (objlist)
	ip_free_object_list(objlist);
    ip_free(oinfo);

    return NULL;
}



/*

NAME:

   im_create_object_list() \- Create an object list from an image

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/object.h>

   im_create_object_list(im, in_object(), flag)
   ip_image *image;
   int (*in_object)(ip_image *im, void *rowptr, int x);
   int flag;

DESCRIPTION:

   Finds all objects in the image---as defined by provided function
   in_object() or by non-zero pixels if in_object() is NULL---and return
   them in an object list.  If in_object() is NULL can be called on a
   BINARY, UBYTE or BYTE image (only) and will determine the presence of
   objects by non-zero pixels.  If in_object() is provided it must be a
   function that given the image 'im', the row of the image as the pointer
   'rowptr' to the row of image data and the x-coordinate (i.e. the index
   into the row) returns TRUE if the specified pixel is an object pixel, and
   FALSE otherwise.

   An object is defined to be an eight-connected region of object pixels.

   This routine is improved over the old im_create_object_list() from the
   obsolete trackobj.c module.  It does not modify the passed image 'im' and
   it does not fill in the holes in objects.  It returns all objects even
   those entirely enclosed by another object.

ERRORS:

   ERR_UNSUPPORTED_TYPE (if in_object() is NULL and image is not (U)BYTE/BINARY)
   ERR_OUT_OF_MEM

SEE ALSO:

   ip_alloc_object_list()                   ip_alloc_object()
   ip_free_object_list()                    ip_free_object()
   All the object measure routines.

*/


ip_objlist *im_create_object_list(ip_image *im, int (*in_object)(ip_image *, void *rowptr, int idx), int flag)
{
    ip_objlist *ol;
    rle_array_t *ra;

    ip_log_routine(__func__);

    if (in_object == NULL) {
	if (NOT (im->type == IM_BINARY || im->type == IM_BYTE || im->type != IM_UBYTE)) {
	    ip_log_error(ERR_UNSUPPORTED_TYPE);
	    ip_pop_routine();
	    return NULL;
	}
    }

    if ((ra = alloc_run_length_array(DEFAULT_RLE_ARRAY_SIZE)) == NULL) {
	ip_pop_routine();
	return NULL;
    }

    if (in_object == NULL) {
	/* Special case BINARY/UBYTE images by detecting non-zero pixels */

	for (int j=0; j<im->size.y; j++) {
	    int i = 0;
	    uint8_t *row = IM_ROWADR(im, j, v);

	    while (i < im->size.x) {
		while ((i < im->size.x) && !row[i])
		    i++;
		if (i < im->size.x) {
		    /* Have start of a run */
		    int start = i;
		    while ((i < im->size.x) && row[i])
			i++;
		    /* i is now one past the end of the run (even if i==im->size.x) */
		    if (NOT rla_insert_run(ra, 0, start, j, i - start))
			goto iol_outofmem;
		}
	    }
	}

    }else{
	/*
	 * General purpose routine; can run on all image types; provided
	 * function in_object() determines whether the pixel is in an object
	 * or not.
	 */

	for (int j=0; j<im->size.y; j++) {
	    int i = 0;
	    void *row = IM_ROWADR(im, j, v);

	    while (i < im->size.x) {
		while ((i < im->size.x) && !in_object(im, row, i))
		    i++;
		if (i < im->size.x) {
		    /* Have start of a run */
		    int start = i;
		    while ((i < im->size.x) && in_object(im, row, i))
			i++;
		    /* i is now one past the end of the run (even if i==im->size.x) */
		    if (NOT rla_insert_run(ra, 0, start, j, i - start))
			goto iol_outofmem;
		}
	    }
	}
    }

    ol =  connect_and_label_runs(ra);

    free_run_length_array(ra);
    ip_pop_routine();
    return ol;

 iol_outofmem:
    free_run_length_array(ra);
    ip_pop_routine();
    return NULL;
}
