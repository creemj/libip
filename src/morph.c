/*

   Module: ip/morph.c
   Author: M.J.Cree

   Copyright (C) 1996, 1997, 2000, 2015, 2017 Michael J. Cree

   Morphological operators with arbitrary flat structuring elements.

   This code calls into routines from morphchord.c or morph1d.c to do its
   job depending on the structuring element.

ENTRY POINTS:

   im_dilate()
   im_erode()
   im_open()
   im_close()

*/

#include <stdlib.h>

#include <ip/ip.h>
#include <ip/morph.h>

#include "internal.h"

/*

NAME:

   im_dilate() \- Dilation with an arbitrary structuring element

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/morph.h>

   int  im_dilate( im, strel )
   ip_image *im;
   ip_strel *strel;

DESCRIPTION:

   Dilate the image 'im' with the structuring element 'strel'.  The
   structuring element must first be allocated with ip_alloc_strel().

   This routine either call into either im_morphchd_dilate() (for general
   structuring elements) or im_morph1d_*_dilate() (for line and square
   structuring elements of BINARY, UBYTE or USHORT type).

LIMITATIONS:

   Only scalar image types supported.

ERRORS:

   ERR_OUT_OF_MEM
   ERR_NOT_IMPLEMENTED

SEE ALSO:

   ip_alloc_strel()     ip_free_strel()
   im_erode()           im_open()          im_close()

*/

int im_dilate(ip_image *im, ip_strel *se)
{
    int suptype, status;

    ip_log_routine(__func__);

    /* Analyse structuring element for which algorithm to use */

    /* The special line code only supports these types */
    suptype = (im->type == IM_BINARY || type_integer(im->type) || type_real(im->type));

    if (suptype && se->num_chords == 1) {
	/* A single connected line */
	int se_len = se->chords[0].len;

	if (se->direction) {
	    status = im_morph1d_vert_dilate(im, se_len, -se->bbox.origin.y);
	}else{
	    status = im_morph1d_horiz_dilate(im, se_len, -se->bbox.origin.x);
	}

    }else if (suptype && se->rectangular) {
	int st1;

	st1 = im_morph1d_horiz_dilate(im, se->bbox.size.x, -se->bbox.origin.x);
	status = im_morph1d_vert_dilate(im, se->bbox.size.y, -se->bbox.origin.y);
	if (st1 != OK)
	    status = st1;

    }else{

	status = im_morph_chord_dilate(im, se);

    }

    ip_pop_routine();
    return status;
}





/*

NAME:

   im_erode() \- Erosion with an arbitrary structuring element

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/morph.h>

   int  im_erode( im, strel )
   ip_image *im;
   ip_strel *strel;

DESCRIPTION:

   Erode the image 'im' with the structuring element 'strel'.  The
   structuring element must first be allocated with ip_alloc_strel().

   This routine either call into either im_morph_chord_erode() (for general
   structuring elements) or im_morph1d_*_erode() (for line and square
   structuring elements of BINARY, UBYTE or USHORT type).

LIMITATIONS:

   Only scalar image types supported.

ERRORS:

   ERR_OUT_OF_MEM
   ERR_NOT_IMPLEMENTED

SEE ALSO:

   ip_alloc_strel()     ip_free_strel()
   im_dilate()          im_open()       im_close()

*/

int im_erode(ip_image *im, ip_strel *se)
{
    int suptype, status;

    ip_log_routine(__func__);

    /* Analyse structuring element for which algorithm to use */

    /* The special line code only supports these types */
    suptype = (im->type == IM_BINARY || type_integer(im->type) || type_real(im->type));

    if (suptype && se->num_chords == 1) {
	/* A single connected line */
	int se_len = se->chords[0].len;

	if (se->direction) {
	    status = im_morph1d_vert_erode(im, se_len, -se->bbox.origin.y);
	}else{
	    status = im_morph1d_horiz_erode(im, se_len, -se->bbox.origin.x);
	}

    }else if (suptype && se->rectangular) {
	int st1;

	st1 = im_morph1d_horiz_erode(im, se->bbox.size.x, -se->bbox.origin.x);
	status = im_morph1d_vert_erode(im, se->bbox.size.y, -se->bbox.origin.y);
	if (st1 != OK)
	    status = st1;

    }else{

	status = im_morph_chord_erode(im, se);

    }

    ip_pop_routine();
    return status;
}




/*

NAME:

   im_open() \- Opening with an arbitrary structuring element

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/morph.h>

   int  im_open( im, strel )
   ip_image *im;
   ip_strel *stre;

DESCRIPTION:

   Perform an opening with the structuring element 'strel'.

LIMITATIONS:

   Only scalar image types supported.

ERRORS:

   ERR_OUT_OF_MEM
   ERR_NOT_IMPLEMENTED

SEE ALSO:

   ip_alloc_strel()   ip_free_strel()
   im_erode()         im_dilate()     im_close()

*/

int im_open(ip_image *im, ip_strel *se)
{
    int status;

    if ((status = im_erode(im, se)) != OK)
	return status;
    return im_dilate(im, se);
}



/*

NAME:

   im_close() \- Close an image with an arbitrary structuring element

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/morph.h>

   int  im_close( im, strel )
   ip_image *im;
   ip_strel *strel;

DESCRIPTION:

   Close the image 'im' with a 'width'*'width' square structuring
   element.

LIMITATIONS:

   Only scalar image types are supported.

ERRORS:

   ERR_OUT_OF_MEM
   ERR_NOT_IMPLEMENTED

SEE ALSO:

   ip_alloc_strel()   ip_free_strel()
   im_erode()         im_dilate()     im_open()

*/

int im_close(ip_image *im, ip_strel *se)
{
    int status;

    if ((status = im_dilate(im, se)) != OK)
	return status;
    return im_erode(im, se);
}

