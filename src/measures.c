/*

   Module: ip/measures.c
   Author: M.J.Cree

   Perform measurement on objects.

   Copyright (C) 1995-2003, 2018 Michael Cree

ENTRY POINTS:

   ip_object_get()
   ip_measure_object_area()
   ip_measure_object_bbox()
   ip_measure_object_coa()
   ip_measure_object_length()
   ip_measure_object_breadth()
   ip_measure_object_perimeter()
   ip_measure_object_max()
   ip_measure_object_max_point()
   ip_measure_object_min()
   ip_measure_object_min_point()
   ip_measure_object_integral()
   ip_measure_object_variance()
   ip_measure_object_mean()
   ip_measure_object_overlap()
   ip_measure_object_farpoint()

*/


#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include <math.h>
#include <ip/ip.h>
#include <ip/object.h>

#include "internal.h"
#include "utils.h"
#include "intobject.h"


/* Return the triangular number of x */

static int trinum(int n)
{
    return (n * (n+1)) / 2;
}



/* Find perpendicular distance from line (xf - xi) to point p.
   NB.   u = xf - xi
         udivusqr = u / (u.u)
   Both of the above values must be precalculated for this function.
   It is easy to demonstrate that p has a perpendicular distance
   from the line (xf - xi) of

   |              u.(xi-p)   |
   |  p - xi -  ---------- u |
   |                 u.u     |

*/

static double distfromline(ip_coord2d p, ip_coord2d xi, ip_coord2d u, ip_vector2d udivusqr)
{
    ip_vector2d pminxi;
    ip_vector2d tmp;
    float dotprod;

    pminxi.x = (double)(p.x - xi.x);
    pminxi.y = (double)(p.y - xi.y);
    dotprod = udivusqr.x * pminxi.x + udivusqr.y * pminxi.y;
    tmp.x = pminxi.x - dotprod * u.x;
    tmp.y = pminxi.y - dotprod * u.y;
    return hypot(tmp.x, tmp.y);
}



/*
   Find out whether the point p is over or under (or on) the
   line given by (xf - xi).
   Returns 1 for "over", -1 for "under", and 0 for on.
   NB.   cross = z-comp [ (xf padded to 3d) x (xi padded to 3d) ]
	       = xf.x * xi.y - xf.y * xi.x
   This value must be precalculated before calling overunder.

   overunder is calculated by taking the croos-product of
   the lines (xf-xi) and (p-xi) where the two vectors have
   zero z-comp. The resultant vector always lies in the z-direction,
   thus having zero x- and y-comp. Its sign of the z-comp
   provides the defn of "over" and "under".
   Note that it is nice in that it only requires integer arithmetic.
*/

static int overunder(ip_coord2d p, ip_coord2d xi, ip_coord2d xf, int cross)
{
    return (xf.x - xi.x)*p.y - (xf.y-xi.y)*p.x - cross;
}


typedef double (*run_integral_function)(ip_image *, ip_coord2d *, rletype *);

#define generate_run_integral_function(tt, it)				\
    static double run_integral_##tt(ip_image *im, ip_coord2d *origin, rletype *run) \
    {									\
	it integral = 0;						\
									\
	tt##_type *iptr = IM_ROWADR(im, origin->y, v);			\
	iptr += origin->x + run->start;					\
	for (int i=0; i<run->length; i++) {				\
	    integral += iptr[i];					\
	}								\
	return (double)integral;					\
    }

static double run_integral_BINARY(ip_image *im, ip_coord2d *origin, rletype *run)
{
    int integral = 0;

    int *iptr = IM_ROWADR(im, origin->y, v);
    iptr += origin->x + run->start;
    for (int i=0; i<run->length; i++) {
	integral += iptr[i];
    }
    return (double)(integral / 255);
}

generate_run_integral_function(BYTE, int)
generate_run_integral_function(UBYTE, int)
generate_run_integral_function(SHORT, int)
generate_run_integral_function(USHORT, int)
generate_run_integral_function(LONG, int64_t)
generate_run_integral_function(ULONG, int64_t)
generate_run_integral_function(FLOAT, float)
generate_run_integral_function(DOUBLE, double)

run_integral_function ip_run_integral_bytype[IM_TYPEMAX] = {
    [IM_BINARY] = run_integral_BINARY,
    [IM_UBYTE] = run_integral_UBYTE,
    [IM_BYTE] = run_integral_BYTE,
    [IM_SHORT] = run_integral_SHORT,
    [IM_USHORT] = run_integral_USHORT,
    [IM_LONG] = run_integral_LONG,
    [IM_ULONG] = run_integral_ULONG,
    [IM_FLOAT] = run_integral_FLOAT,
    [IM_DOUBLE] = run_integral_DOUBLE
};

static inline double ip_run_integral(ip_image *im, ip_coord2d *origin, rletype *run)
{
    return ip_run_integral_bytype[im->type](im, origin, run);
}

static inline ip_drgba ip_run_integral_rgb(ip_image *im, ip_coord2d *origin, rletype *run)
{
    ip_drgba res;
    unsigned int rint = 0, gint = 0, bint = 0, aint = 0;

    switch (im->type) {
    case IM_PALETTE: {
	uint8_t *iptr = IM_ROWADR(im, origin->y, v);
	iptr += origin->x + run->start;
	for (int i=0; i<run->length; i++) {
	    ip_rgb val = im->palette[iptr[i]];
	    rint += val.r;
	    gint += val.g;
	    bint += val.b;
	}
    }   break;
    case IM_RGB: {
	ip_rgb *iptr = IM_ROWADR(im, origin->y, v);
	iptr += origin->x + run->start;
	for (int i=0; i<run->length; i++) {
	    rint += iptr[i].r;
	    gint += iptr[i].g;
	    bint += iptr[i].b;
	}
    }   break;
    case IM_RGB16: {
	ip_lrgb *iptr = IM_ROWADR(im, origin->y, v);
	iptr += origin->x + run->start;
	for (int i=0; i<run->length; i++) {
	    rint += iptr[i].r;
	    gint += iptr[i].g;
	    bint += iptr[i].b;
	}
    }   break;
    case IM_RGBA: {
	ip_rgba *iptr = IM_ROWADR(im, origin->y, v);
	iptr += origin->x + run->start;
	for (int i=0; i<run->length; i++) {
	    rint += iptr[i].r;
	    gint += iptr[i].g;
	    bint += iptr[i].b;
	    aint += iptr[i].a;
	}
    }   break;
    default:
	break;
    }

    res.r = rint;  res.g = gint;  res.b = bint;  res.a = aint;

    return res;
}


/* Used for calculating variance */

typedef void (*run_sqrintegral_function)(ip_image *, ip_coord2d *, rletype *, double *, double *);

#define generate_run_sqrintegral_function(tt, it)			\
    static void run_sqrintegral_##tt(ip_image *im, ip_coord2d *origin, rletype *run, \
				     double *rsum, double *rsumsqr)	\
    {									\
	it sum = 0, sumsqr = 0;						\
									\
	tt##_type *iptr = IM_ROWADR(im, origin->y, v);			\
	iptr += origin->x + run->start;					\
	for (int i=0; i<run->length; i++) {				\
	    sum += iptr[i];						\
	    sumsqr += iptr[i] * iptr[i];				\
	}								\
	*rsum = (double)sum;						\
	*rsumsqr = (double)sumsqr;					\
    }

static void run_sqrintegral_BINARY(ip_image *im, ip_coord2d *origin, rletype *run,
				     double *rsum, double *rsumsqr)
{
    int sum = 0, sumsqr = 0;

    int *iptr = IM_ROWADR(im, origin->y, v);
    iptr += origin->x + run->start;
    for (int i=0; i<run->length; i++) {
	sum += iptr[i];
	sumsqr += iptr[i] * iptr[i];
    }
    *rsum = (double)(sum / 255);
    *rsumsqr = (double)(sumsqr / (255*255));
}

generate_run_sqrintegral_function(BYTE, int)
generate_run_sqrintegral_function(UBYTE, int)
generate_run_sqrintegral_function(SHORT, int64_t)
generate_run_sqrintegral_function(USHORT, int64_t)
generate_run_sqrintegral_function(LONG, int64_t)
generate_run_sqrintegral_function(ULONG, int64_t)
generate_run_sqrintegral_function(FLOAT, double)
generate_run_sqrintegral_function(DOUBLE, double)

run_sqrintegral_function ip_run_sqrintegral_bytype[IM_TYPEMAX] = {
    [IM_BINARY] = run_sqrintegral_BINARY,
    [IM_UBYTE] = run_sqrintegral_UBYTE,
    [IM_BYTE] = run_sqrintegral_BYTE,
    [IM_SHORT] = run_sqrintegral_SHORT,
    [IM_USHORT] = run_sqrintegral_USHORT,
    [IM_LONG] = run_sqrintegral_LONG,
    [IM_ULONG] = run_sqrintegral_ULONG,
    [IM_FLOAT] = run_sqrintegral_FLOAT,
    [IM_DOUBLE] = run_sqrintegral_DOUBLE
};

static inline void ip_run_sqrintegral(ip_image *im, ip_coord2d *origin, rletype *run,
					double *rsum, double *rsumsqr)
{
    ip_run_sqrintegral_bytype[im->type](im, origin, run, rsum, rsumsqr);
}

static inline void ip_run_sqrintegral_rgb(ip_image *im, ip_coord2d *origin, rletype *run,
					  ip_drgba *rsum, ip_drgba *rsumsqr)
{
    unsigned int rint = 0, gint = 0, bint = 0, aint = 0;
    unsigned int rsqr = 0, gsqr = 0, bsqr = 0, asqr = 0;

    switch (im->type) {
    case IM_PALETTE: {
	uint8_t *iptr = IM_ROWADR(im, origin->y, v);
	iptr += origin->x + run->start;
	for (int i=0; i<run->length; i++) {
	    ip_rgb val = im->palette[iptr[i]];
	    rint += val.r;    rsqr += val.r*val.r;
	    gint += val.g;    gsqr += val.g*val.g;
	    bint += val.b;    bsqr += val.b*val.b;
	}
    }   break;
    case IM_RGB: {
	ip_rgb *iptr = IM_ROWADR(im, origin->y, v);
	iptr += origin->x + run->start;
	for (int i=0; i<run->length; i++) {
	    rint += iptr[i].r;   rsqr += iptr[i].r * iptr[i].r;
	    gint += iptr[i].g;   gsqr += iptr[i].g * iptr[i].g;
	    bint += iptr[i].b;   bsqr += iptr[i].b * iptr[i].b;
	}
    }   break;
    case IM_RGB16: {
	ip_lrgb *iptr = IM_ROWADR(im, origin->y, v);
	iptr += origin->x + run->start;
	for (int i=0; i<run->length; i++) {
	    rint += iptr[i].r;   rsqr += iptr[i].r * iptr[i].r;
	    gint += iptr[i].g;   gsqr += iptr[i].g * iptr[i].g;
	    bint += iptr[i].b;   bsqr += iptr[i].b * iptr[i].b;
	}
    }   break;
    case IM_RGBA: {
	ip_rgba *iptr = IM_ROWADR(im, origin->y, v);
	iptr += origin->x + run->start;
	for (int i=0; i<run->length; i++) {
	    rint += iptr[i].r;   rsqr += iptr[i].r * iptr[i].r;
	    gint += iptr[i].g;   gsqr += iptr[i].g * iptr[i].g;
	    bint += iptr[i].b;   bsqr += iptr[i].b * iptr[i].b;
	    aint += iptr[i].a;   asqr += iptr[i].a * iptr[i].a;
	}
    }   break;
    default:
	break;
    }

    rsum->r = rint;  rsum->g = gint;  rsum->b = bint;  rsum->a = aint;
    rsumsqr->r = rsqr;  rsumsqr->g = gsqr;  rsumsqr->b = bsqr;  rsumsqr->a = asqr;
}


/*

NAME:

   ip_object_get() \- Get some attribute of an object

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/object.h>

   ip_datum ip_object_get( action, obj, ... )
   int action;
   ip_object *obj;

DESCRIPTION:

   Return some info about an object 'obj'. The 'action' to perform
   must be one of:

\|   Action	    Returns    Return Type  Arguments
\|
\|   OBJ_ORIGIN    origin           ip_coord2d
\|   OBJ_AREA      area             double
\|   OBJ_BBOX      bounding-box     box
\|   OBJ_COA       centre-of-area   double
\|   OBJ_LENGTH    length           double
\|   OBJ_BREADTH   breadth          double
\|   OBJ_PERIMETER perimeter        double  ip_image *
\|   OBJ_MAX       maximum          double  ip_image *
\|   OBJ_MIN       minimum          double  ip_image *
\|   OBJ_MAXPT     point-of-maximum ip_coord2d   ip_image *
\|   OBJ_MINPT     point-of-minimum ip_coord2d   ip_image *
\|   OBJ_INTEGRAL  integral         double  ip_image *
\|   OBJ_VARIANCE  variance         double  ip_image *
\|   OBJ_BNDMEAN   boundary-mean    double  ip_image *
\|   OBJ_OVERLAP   overlap          double  ip_object *
\|   OBJ_FARPOINT  far-point        ip_coord2d   ip_coord2d

   The return type is listed above which must be accessed from the union
   ip_datum. Some actions require extra arguments, which are also listed in
   column arguments. The routines which require an image pointer find the
   specified quantity, for the image, under the object only.

   This is the prefered entry point for getting attributes of an object.

   This routine calls the underlying ip_measure_object_*() routines. If
   preferred, they may be called directly, since in a few cases, calls to
   the ip_measure_object_*() can be rearranged to optimise performance.

LIMITATIONS:

\&   - Doesn''t handle complex image types.
\&   - Only can flag an error (when an invalid action is tried) through
       an error-handler and not through the return value.

ERRORS:

   ERR_BAD_PARAMETER

SEE ALSO:

   ip_object_set()     ip_object_modify()

*/


ip_datum ip_object_get(int type, ip_object *o, ...)
{
    ip_datum info;
    ip_coord2d dummy,dummy2;
    ip_image *im;
    ip_object *obj;
    va_list ap;

    va_start(ap,o);
    ip_log_routine(__func__);

    switch (type) {
    case OBJ_ORIGIN:
	info.pt = o->origin;
	break;
    case OBJ_AREA:
	info.d = ip_measure_object_area(o);
	break;
    case OBJ_BBOX:
	info.box = ip_measure_object_bbox(o);
	break;
    case OBJ_COA:
    case OBJ_LENGTH:
    case OBJ_BREADTH:
	info.v2d = ip_measure_object_coa(o);
	if (type == OBJ_COA)
	    break;
	info.d = ip_measure_object_length(o,info.v2d,&dummy,&dummy2);
	if (type == OBJ_LENGTH)
	    break;
	info.d = ip_measure_object_breadth(o,dummy,dummy2);
	break;
    case OBJ_PERIMETER:
	info.d = ip_measure_object_perimeter(o);
	break;
    case OBJ_MAX:
	im = va_arg(ap, ip_image *);
	info.d = ip_measure_object_max(im,o,&dummy);
	break;
    case OBJ_MIN:
	im = va_arg(ap, ip_image *);
	info.d = ip_measure_object_min(im,o,&dummy);
	break;
    case OBJ_MAXPT:
	im = va_arg(ap, ip_image *);
	info.pt = ip_measure_object_max_point(im,o);
	break;
    case OBJ_MINPT:
	im = va_arg(ap, ip_image *);
	info.pt = ip_measure_object_min_point(im,o);
      break;
    case OBJ_INTEGRAL:
	im = va_arg(ap, ip_image *);
	info.d = ip_measure_object_integral(im,o);
	break;
    case OBJ_VARIANCE:
	im = va_arg(ap, ip_image *);
	info.d = ip_measure_object_variance(im,o);
	break;
    case OBJ_BNDMEAN:
	im = va_arg(ap, ip_image *);
	info.d = ip_measure_boundary_mean(im,o);
	break;
    case OBJ_OVERLAP:
	obj = va_arg(ap, ip_object *);
	info.d = ip_measure_object_overlap(o, obj);
	break;
    case OBJ_FARPOINT:
	dummy = va_arg(ap, ip_coord2d);
	info.pt = ip_measure_object_farpoint(o, dummy);
	break;
    default:
	ip_log_error(ERR_BAD_PARAMETER);
	break;
    }
    ip_pop_routine();
    va_end(ap);
    return info;
}



/*
*/
double ip_measure_object_area(ip_object *o)
{
    return (double)o->numpixels;
}



/*
*/
ip_box ip_measure_object_bbox(ip_object *o)
{
   ip_box bbox;

   bbox.origin = o->bbox.tl;
   bbox.size = ip_botr_size(o->bbox.tl, o->bbox.br);

   return bbox;
}



/*
*/
ip_vector2d ip_measure_object_coa(ip_object *o)
{
    ip_vector2d coa;

    coa.x = coa.y = 0;
    for (int i=0; i<o->numrows; i++) {
	rowtype *row = ip_obj_get_row(o,i);
	rletype *rle = row->rle;
	for (int j=0; j<row->numentries; j++) {
	    coa.x += rle[j].length * (o->origin.x + rle[j].start)
						  + trinum(rle[j].length-1);
	    coa.y += rle[j].length * (o->origin.y + i);
	}
    }
    coa.x /= o->numpixels;
    coa.y /= o->numpixels;
    return coa;
}



/*
*/
double ip_measure_object_length(ip_object *obj,
				ip_vector2d coapt, ip_coord2d *pt1, ip_coord2d *pt2)
{
    ip_coord2d prev,this,next;

    /* Seed with centre of area measurement and shift to object coord */

    prev.x = ROUND(coapt.x) - obj->origin.x;
    prev.y = ROUND(coapt.y) - obj->origin.y;
    this = ip_measure_object_farpoint(obj, prev);
    next = ip_measure_object_farpoint(obj, this);

    /*
     * Keep searching for point further away than current point (while
     * updating current point with furtherest point) until get convergence
     * to two points. These will be two furtherest apart points in object.
     */

    while (prev.x != next.x || prev.y != next.y) {
	prev = this;
	this = next;
	next = ip_measure_object_farpoint(obj,next);
    }

    /* Put back to image coord */

    *pt1 = ip_image_coord(obj, this);
    *pt2 = ip_image_coord(obj, next);

    /*
     * Return object length as distance between two furtherest apart points.
     * Add 2 * 0.5 to distance to correct for pixel width at each end of
     * length calculation.
     */

    return eucldist(this, next) + 1.0;
}



/*
*/
double ip_measure_object_breadth(ip_object *obj, ip_coord2d pt1, ip_coord2d pt2)
{
    double maxunder,maxover;
    float tmp;
    ip_coord2d u;
    ip_vector2d udivusqr;
    int crosspt,ou;
    double dist;
    ip_coord2d pt;

    /* Shift to object coordinates */

    pt1 = ip_object_coord(obj, pt1);
    pt2 = ip_object_coord(obj, pt2);

    /* Initialise breadth to zero */

    maxunder = maxover = 0.0;

    /* u = pt2 - pt1,   ie the line directed from pt1 to pt2. */

    u.x = pt2.x - pt1.x;
    u.y = pt2.y - pt1.y;

    /* udivusqr = u / u.u */

    tmp = (float)(u.x*u.x+u.y*u.y);
    udivusqr.x = u.x / tmp;
    udivusqr.y = u.y / tmp;

    /*
     * crosspt = z-comp of pt1 x pt2, where x = cross product,
     * and pt1 and pt2 are padded out to 3d vectors with zero z-comp
     */

    crosspt = pt2.x*pt1.y - pt2.y*pt1.x;

    /* for each row in object */

    for (int i=0; i<obj->numrows; i++) {
	rowtype *row = ip_obj_get_row(obj,i);
	rletype *rle = row->rle;

	/* Consider start point of first line segment */

	pt.y = i;
	pt.x = rle[0].start;

	/* Find distance of pt from line defn by pt1 to pt2. */

	dist = distfromline(pt, pt1, u, udivusqr);

	/* Is the pt under or over the line ? */

	ou = overunder(pt, pt1, pt2, crosspt);

	/* Update maxunder or maxover, respectively */

	if (ou > 0) {
	    if (dist > maxover) maxover = dist;
	}
	if (ou < 0) {
	    if (dist > maxunder) maxunder = dist;
	}

	/* Repeat for end point of entire row. */

	pt.x = rle[row->numentries-1].start + rle[row->numentries-1].length - 1;
	dist = distfromline(pt, pt1, u, udivusqr);
	ou = overunder(pt, pt1, pt2, crosspt);
	/* D(bug("pt = (%d,%d), dist = %g, overunder = %d\n",pt.x,pt.y,dist,ou)); */
	if (ou > 0) {
	    if (dist > maxover) maxover = dist;
	}
	if (ou < 0) {
	    if (dist > maxunder) maxunder = dist;
	}
    }

    /*
     * Breadth is the max distance under line plus max distance above line
     * plus 2 * 0.5 for pixel width correction at both ends of line.
     */

    return maxunder + maxover + 1.0;
}




/*
 * The perimeter is calculated by stepping arround the boundary pixels of
 * the object and adding one for a horizontal or vertical step, and sqrt(2)
 * for a diagonal step.
 */
double ip_measure_object_perimeter(ip_object *obj)
{
    double perimeter;
    rowtype *row;
    rletype *rle;

    perimeter = 0.0;		 /* Initialisation */
    row = ip_obj_get_row(obj,0); /* Calculate 1st row in isolation */
    rle = row->rle;
    for (int i=0; i<row->numentries; i++)
	perimeter += 2 * (rle[i].length - 1);

    for (int j=1; j<obj->numrows; j++) { /* Add each row's contribution in */
	rowtype *abvrow = ip_obj_get_row(obj,j-1);
	rletype *abvrle = abvrow->rle;
	row = ip_obj_get_row(obj,j);
	rle = row->rle;

	for (int i=0; i<row->numentries; i++) {
	    perimeter += 2 * (rle[i].length - 1);

	    for (int k=0; k<abvrow->numentries; k++) {
		if (NOT ip_run_adjacent(&rle[i], &abvrle[k]))
		    continue;
		perimeter -= 2*ip_run_overlap(&abvrle[k], &rle[i]);
		perimeter += 2 * M_SQRT2;
		if (rle[i].start == abvrle[k].start)
		    perimeter += 2 - M_SQRT2;
		if (rle[i].start + rle[i].length == abvrle[k].start +abvrle[k].length)
		    perimeter += 2 - M_SQRT2;
	    }

	}
    }
    return perimeter;
}


typedef double (*object_extremum_function)(ip_image *, ip_object *, ip_coord2d *);

#define generate_object_max_function(tt)				\
    static double object_max_##tt(ip_image *im, ip_object *obj, ip_coord2d *maxpt)	\
    {									\
	tt##_type max = tt##_min;					\
	for (int j=0; j<obj->numrows; j++) {				\
	    rowtype *row = ip_obj_get_row(obj,j);			\
	    rletype *rle = row->rle;					\
	    ip_coord2d pt;						\
	    tt##_type *iptr;						\
	    pt.y = j + obj->origin.y;					\
	    iptr = IM_ROWADR(im, pt.y, v);				\
	    iptr += obj->origin.x;					\
	    for (int i=0; i<row->numentries; i++) {			\
		for (int k=rle[i].start; k<rle[i].start+rle[i].length; k++) { \
		    if (iptr[k] > max) {				\
			max = iptr[k];					\
			pt.x = obj->origin.x + k;			\
			*maxpt = pt;					\
		    }							\
		}							\
	    }								\
	}								\
	return max;							\
    }


generate_object_max_function(UBYTE)
generate_object_max_function(BYTE)
generate_object_max_function(USHORT)
generate_object_max_function(SHORT)
generate_object_max_function(ULONG)
generate_object_max_function(LONG)
generate_object_max_function(FLOAT)
generate_object_max_function(DOUBLE)

object_extremum_function ip_object_max_bytype[IM_TYPEMAX] = {
    [IM_BINARY] = object_max_UBYTE,
    [IM_UBYTE] = object_max_UBYTE,
    [IM_PALETTE] = object_max_UBYTE,
    [IM_BYTE] = object_max_BYTE,
    [IM_SHORT] = object_max_SHORT,
    [IM_USHORT] = object_max_USHORT,
    [IM_LONG] = object_max_LONG,
    [IM_ULONG] = object_max_ULONG,
    [IM_FLOAT] = object_max_FLOAT,
    [IM_DOUBLE] = object_max_DOUBLE
};


/*
*/
ip_coord2d ip_measure_object_max_point(ip_image *im,ip_object *obj)
{
    ip_coord2d maxpt;

    ip_measure_object_max(im, obj, &maxpt);
    return maxpt;
}



/*
*/
double ip_measure_object_max(ip_image *im, ip_object *obj, ip_coord2d *maxpt)
{
    double fmax;

    if (ip_object_max_bytype[im->type])
	fmax = ip_object_max_bytype[im->type](im, obj, maxpt);
    else
	fmax = NAN;

    return fmax;
}


#define generate_object_min_function(tt)				\
    static double object_min_##tt(ip_image *im, ip_object *obj, ip_coord2d *minpt)	\
    {									\
	tt##_type min = tt##_max;					\
	for (int j=0; j<obj->numrows; j++) {				\
	    rowtype *row = ip_obj_get_row(obj,j);			\
	    rletype *rle = row->rle;					\
	    ip_coord2d pt;						\
	    tt##_type *iptr;						\
	    pt.y = j + obj->origin.y;					\
	    iptr = IM_ROWADR(im, pt.y, v);				\
	    iptr += obj->origin.x;					\
	    for (int i=0; i<row->numentries; i++) {			\
		for (int k=rle[i].start; k<rle[i].start+rle[i].length; k++) { \
		    if (iptr[k] < min) {				\
			min = iptr[k];					\
			pt.x = obj->origin.x + k;			\
			*minpt = pt;					\
		    }							\
		}							\
	    }								\
	}								\
	return min;							\
    }


generate_object_min_function(UBYTE)
generate_object_min_function(BYTE)
generate_object_min_function(USHORT)
generate_object_min_function(SHORT)
generate_object_min_function(ULONG)
generate_object_min_function(LONG)
generate_object_min_function(FLOAT)
generate_object_min_function(DOUBLE)

object_extremum_function ip_object_min_bytype[IM_TYPEMAX] = {
    [IM_BINARY] = object_min_UBYTE,
    [IM_UBYTE] = object_min_UBYTE,
    [IM_PALETTE] = object_min_UBYTE,
    [IM_BYTE] = object_min_BYTE,
    [IM_SHORT] = object_min_SHORT,
    [IM_USHORT] = object_min_USHORT,
    [IM_LONG] = object_min_LONG,
    [IM_ULONG] = object_min_ULONG,
    [IM_FLOAT] = object_min_FLOAT,
    [IM_DOUBLE] = object_min_DOUBLE
};


/*
*/
ip_coord2d ip_measure_object_min_point(ip_image *im, ip_object *obj)
{
    ip_coord2d minpt;

    ip_measure_object_min(im, obj, &minpt);
    return minpt;
}



/*
*/
double ip_measure_object_min(ip_image *im, ip_object *obj, ip_coord2d *minpt)
{
    double fmin;

    if (ip_object_min_bytype[im->type])
	fmin = ip_object_min_bytype[im->type](im, obj, minpt);
    else
	fmin = NAN;

    return fmin;
}



double ip_measure_object_integral(ip_image *im, ip_object *obj)
{
    double integral;
    ip_coord2d pt;

    integral = 0.0;
    pt.x = obj->origin.x;

    for (int j=0; j<obj->numrows; j++) {
	rowtype *row = ip_obj_get_row(obj,j);
	rletype *run = row->rle;
	pt.y = j + obj->origin.y;
	for (int i=0; i<row->numentries; i++) {
	    integral += ip_run_integral(im, &pt, run++);
	}
    }
    return integral;
}

/*
 * Works on RGB images; returning sum of pixels in red, green and blue.  If
 * called on a scalar image treats the image as grey-scale thus having same
 * red, green and blue values at every pixel.  If called on a palette image
 * then uses the images palette to get the red, green and blue values to add
 * up.
 */

ip_drgba ip_measure_object_integral_rgb(ip_image *im, ip_object *obj)
{
    ip_drgba res = {0.0, 0.0, 0.0, 0.0};

    if (type_scalar(im->type)) {
	res.r = res.g = res.b = ip_measure_object_integral(im, obj);
    }else if (type_clrim(im->type)) {
	ip_coord2d pt;

	pt.x = obj->origin.x;

	for (int j=0; j<obj->numrows; j++) {
	    rowtype *row = ip_obj_get_row(obj,j);
	    rletype *run = row->rle;
	    pt.y = j + obj->origin.y;
	    for (int i=0; i<row->numentries; i++) {
		ip_drgba update = ip_run_integral_rgb(im, &pt, run++);
		res.r += update.r;
		res.g += update.g;
		res.b += update.b;
		res.a += update.a;
	    }
	}
    }
    return res;
}




double ip_measure_object_variance(ip_image *im, ip_object *obj)
{
    ip_coord2d pt;
    double sum = 0.0, sumsqr = 0.0;

    pt.x = obj->origin.x;
    for (int j=0; j<obj->numrows; j++) {
	rowtype *row = ip_obj_get_row(obj,j);
	rletype *run = row->rle;
	pt.y = j + obj->origin.y;

	for (int i=0; i<row->numentries; i++) {
	    double rsum, rsumsqr;
	    ip_run_sqrintegral(im, &pt, run++, &rsum, &rsumsqr);
	    sum += rsum;
	    sumsqr += rsumsqr;
	}
    }
    sum /= obj->numpixels;
    return sumsqr / obj->numpixels - sum*sum;
}


ip_drgba ip_measure_object_variance_rgb(ip_image *im, ip_object *obj)
{
    ip_drgba res = {0.0, 0.0, 0.0, 0.0};
    ip_drgba sqr = {0.0, 0.0, 0.0, 0.0};

    if (type_scalar(im->type)) {
	res.r = res.g = res.b = ip_measure_object_integral(im, obj);
    }else if (type_clrim(im->type)) {
	ip_coord2d pt;

	pt.x = obj->origin.x;

	for (int j=0; j<obj->numrows; j++) {
	    rowtype *row = ip_obj_get_row(obj,j);
	    rletype *run = row->rle;
	    pt.y = j + obj->origin.y;
	    for (int i=0; i<row->numentries; i++) {
		ip_drgba sum, sumsqr;
		ip_run_sqrintegral_rgb(im, &pt, run++, &sum, &sumsqr);
		res.r += sum.r;
		res.g += sum.g;
		res.b += sum.b;
		res.a += sum.a;
		sqr.r += sumsqr.r;
		sqr.g += sumsqr.g;
		sqr.b += sumsqr.b;
		sqr.a += sumsqr.a;
	    }
	}
    }
    res.r /= obj->numpixels;
    res.g /= obj->numpixels;
    res.b /= obj->numpixels;
    res.a /= obj->numpixels;
    res.r = sqr.r / obj->numpixels - res.r*res.r;
    res.g = sqr.g / obj->numpixels - res.g*res.g;
    res.b = sqr.b / obj->numpixels - res.b*res.b;
    res.a = sqr.a / obj->numpixels - res.r*res.a;
    return res;
}


double ip_measure_boundary_mean(ip_image *im, ip_object *obj)
{
    double bndint;
    ip_coord2d pt,below,above;
    int numbnd;
    rowtype *row;
    rletype *run;

    bndint = 0.0;
    numbnd = 0;

    /* Add in all first row since must be on boundary */

    pt = obj->origin;
    row = ip_obj_get_row(obj, 0);
    run = row->rle;

    for (int i=0; i<row->numentries; i++) {
	numbnd += run->length;
	bndint += ip_run_integral(im, &pt, run++);
    }

    /* Add in all of last row (if not first row) */

    if (obj->numrows > 1) {
	pt.y += obj->numrows-1;
	row = ip_obj_get_row(obj, obj->numrows-1);
	run = row->rle;
	for (int i=0; i<row->numentries; i++) {
	    numbnd += run->length;
	    bndint += ip_run_integral(im, &pt, run++);
	}
    }

    /*
     * Add in all in-between rows, checking to see if pixels are on
     * boundary.
     */

    for (int j=1; j<obj->numrows-1; j++) {
	pt.y = obj->origin.y + j;
	above.y = pt.y - 1;
	below.y = pt.y + 1;
	row = ip_obj_get_row(obj, j);
	run = row->rle;
	for (int i=0; i<row->numentries; i++,run++) {
	    pt.x = obj->origin.x + run->start;
	    bndint += im_value(im, pt).d[0];
	    numbnd++;
	    if (run->length > 1) {
		pt.x += run->length - 1;
		bndint += im_value(im, pt).d[0];
		numbnd++;
	    }
	    pt.x = obj->origin.x + run->start + 1;
	    for (int k=1; k<run->length-1; k++) {
		above.x = below.x = pt.x;
		if (NOT (ip_point_in_object(obj,above) && ip_point_in_object(obj,below))) {
		    bndint += im_value(im,pt).d[0];
		    numbnd++;
		}
		pt.x++;
	    }
	}
    }

    return bndint/numbnd;
}



double ip_measure_object_overlap(ip_object *ref, ip_object *other)
{
    int count;
    int rstart,rend;
    int xoffset,yoffset;

    /* If bounding boxes do not overlap then overlap is 0.0 */

    if (ref->bbox.br.x < other->bbox.tl.x || other->bbox.br.x < ref->bbox.tl.x
	|| ref->bbox.br.y < other->bbox.tl.y || other->bbox.br.y < ref->bbox.tl.y)
	return 0.0;

    /* Find which rows overlap in both objects */

    rstart = MAX(ref->bbox.tl.y, other->bbox.tl.y) - ref->bbox.tl.y;
    rend   = MIN(ref->bbox.br.y, other->bbox.br.y) - ref->bbox.tl.y + 1;
    yoffset = ref->bbox.tl.y - other->bbox.tl.y;
    xoffset = ref->origin.x - other->origin.x;

    /* For each overlapping row do */

    count = 0;
    for (int j=rstart; j<rend; j++) {
	rowtype *rrow = ip_obj_get_row(ref,j);
	rowtype *orow = ip_obj_get_row(other,j+yoffset);
	rletype *rrun = rrow->rle;
	rletype *orun;

	/* For each run in row in ref object do */

	for (int ri=0; ri<rrow->numentries; ri++) {

	    /* Add up any overlapping parts of run with runs in other */

	    rrun->start += xoffset;
	    orun = orow->rle;
	    for (int oi=0; oi<orow->numentries; oi++) {
		if (ip_run_adjacent(rrun,orun))
		    count += ip_run_overlap(rrun,orun);
		orun++;
	    }
	    rrun->start -= xoffset;
	    rrun++;
	}

    }

    /* Return fraction of count wrt ref total area */

    return ((double)count / (double)ref->numpixels);
}




ip_coord2d ip_measure_object_farpoint(ip_object *obj, ip_coord2d x)
{
    ip_coord2d f,pt;
    double dist,ptdist;

    f.x = x.x;
    f.y = x.y;
    dist = 0.0;
    for (int i=0; i<obj->numrows; i++) {
	rowtype *row = ip_obj_get_row(obj,i);
	rletype *rle = row->rle;

	/* Check start point of first line segment */

	pt.x = rle->start;
	pt.y = i;
	ptdist = eucldist(x,pt);
	if (ptdist > dist) {
	    f = pt;
	    dist = ptdist;
	}

	/* Check end point of last line segment */

	pt.x = rle[row->numentries-1].start + rle[row->numentries-1].length - 1;
	ptdist = eucldist(x,pt);
	if (ptdist > dist) {
	    f = pt;
	    dist = ptdist;
	}
    }
    return f;
}
