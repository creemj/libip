/*
   Module: ip/jpeg.c
   Author: M.J.Cree

   Copyright (C) 2005-2007, 2015 Michael J. Cree

   Routines for reading JPEG images.
   Writing JPEG images not implemented.
   Probably won't - JPEG images not suitable for scientific work.

*/


#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <setjmp.h>

#include <time.h>

#define IPINTERNAL
#include <ip/ip.h>
#include <ip/file.h>
#include <ip/jpeg.h>

#include "internal.h"
#include "utils.h"

static ip_image *rd_jpeg_image(ip_jpeg_handle *ph, int first_row, int nrows);


/* JPEG error handler */

static void ip_jpeg_flag_error(j_common_ptr dcinfo);
static void ip_jpeg_flag_error(j_common_ptr dcinfo)
{
   ip_jpeg_err *fred = (ip_jpeg_err *)dcinfo->err;
   /* Display message from jpeg library */
   (*dcinfo->err->output_message)(dcinfo);
   /* Return to setjmp point in calling routine. */
   longjmp(fred->sj_buffer,1);
}



/*

NAME:

   open_jpeg_image() \- Open JPEG image file ready for reading

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/jpeg.h>

   ip_jpeg_handle *open_jpeg_image( filename )
   char * filename

DESCRIPTION:

   Open JPEG image corresponding to 'filename'. Returns JPEG handle if
   successfully opened or alternatively NULL if any errors occur. One
   can check image type and size in the JPEG handle. This routine does
   not read in the actual image data, only the JPEG image header.

   Filename may be a NULL pointer, in which case stdin is used as the file
   handle.

   Example of reading in a JPEG image file:

\|   char *str;
\|   ip_jpeg_handle *ph;
\|
\|   ph = open_jpeg_image("fred");
\|   im = read_jpeg_image(ph);
\|   close_jpeg_image(ph);

ERRORS:

   ERR_NO_STDIN - Can't read from stdin
   ERR_OUT_OF_MEM
   ERR_JPEG - libjpeg generated error
   ERR_INVALID_JPEG_TYPE - Can't read that type of JPEG image

SEE ALSO:

   open_jpeg_image()   read_jpeg_image()   read_jpeg_imagex()
   read_jpeg_tag()     close_jpeg_image()
   write_jpeg_image()  write_jpeg_imagex()
   ip_read_image()

*/

ip_jpeg_handle *open_jpeg_image(const char * fname)
{
   ip_jpeg_handle *jh;

   ip_log_routine("open_jpeg_image");

   /* Allocate JPEG handle. */

   if ((jh = ip_mallocx(sizeof(ip_jpeg_handle))) == NULL) {
      goto exit_oji;
   }
   jh->f = NULL;
   jh->fname = fname;
   jh->dcinfo = NULL;
   jh->cinfo = NULL;
   jh->init = 0;
   if ((jh->dcinfo = 
             ip_mallocx(sizeof(struct jpeg_decompress_struct))) == NULL) {
      goto exit_oji;
   }
   if ((jh->jerr = 
             ip_mallocx(sizeof(ip_jpeg_err))) == NULL) {
      goto exit_oji;
   }

   /* Open file for reading */

   errno = 0;
   if (fname) {
      if ((jh->f = fopen(fname,"rb")) == NULL) {
	 ip_log_error(ERR_FILE_OPEN,fname);
	 goto exit_oji;
      }
   }else{
      jh->f = stdin;
      jh->fname = ip_msg_stdin;
   }

   /* Initialise error handler - we override error_exit */
   jh->dcinfo->err = jpeg_std_error((struct jpeg_error_mgr *)jh->jerr);
   jh->jerr->pub.error_exit = ip_jpeg_flag_error;

   /* Establish jump context for setjmp */
   if (setjmp(jh->jerr->sj_buffer)) {
      ip_log_error(ERR_JPEG, fname);
      goto exit_oji;
   }

   /* initialise jpeg decompression handle */
   jpeg_create_decompress(jh->dcinfo);
   jh->init = TRUE;

   /* Link jpeg decompression to opened file */

   jpeg_stdio_src(jh->dcinfo, jh->f);

   /* Read jpeg header and parse */

   jpeg_read_header(jh->dcinfo, TRUE);

   jh->size.x = jh->dcinfo->image_width;
   jh->size.y = jh->dcinfo->image_height;
   if (jh->dcinfo->num_components == 1) {
      /* grey-scale image. */
      jh->type = IM_BYTE;
   }else if (jh->dcinfo->num_components == 3) {
      /* Hopefully is three channel colour image */
      jh->type = IM_RGB;
   }else{
      /* Dunno what this image is! */
      ip_log_error(ERR_INVALID_JPEG_TYPE,jh->fname);
      goto exit_oji;
   }

   if (jh->dcinfo->data_precision != 8) {
      ip_log_error(ERR_INVALID_JPEG_TYPE,jh->fname);
      goto exit_oji;
   }

 exit_oji:			/* Error exit */
 
   if (ip_error != OK && jh != NULL) {
      if (jh->init) {
	 jpeg_destroy_decompress(jh->dcinfo);
      }
      if (jh->f && jh->f!=stdin) fclose(jh->f);
      if (jh->dcinfo) ip_free(jh->dcinfo);
      if (jh->jerr) ip_free(jh->jerr);
      ip_free(jh);
      jh = NULL;
   }

   ip_pop_routine();
   return jh;
}



ip_jpeg_handle *open_jpeg_imagex(FILE *f, const char *fname, unsigned char hdr[4],
                             int flag)
{
   ip_jpeg_handle *jh;

   ip_log_routine("open_jpeg_imagex");

   /* Allocate JPEG handle. */

   if ((jh = ip_mallocx(sizeof(ip_jpeg_handle))) == NULL) {
      goto exit_ojix;
   }
   jh->dcinfo = NULL;
   jh->cinfo = NULL;
   jh->init = 0;
   if ((jh->dcinfo = 
             ip_mallocx(sizeof(struct jpeg_decompress_struct))) == NULL) {
      goto exit_ojix;
   }
   if ((jh->jerr = 
             ip_mallocx(sizeof(ip_jpeg_err))) == NULL) {
      goto exit_ojix;
   }
   
   ungetc(hdr[3],f);
   ungetc(hdr[2],f);
   ungetc(hdr[1],f);
   ungetc(hdr[0],f);
   jh->f = f;

   if (fname) {
      jh->fname = fname;
   }else{
      jh->fname = ip_msg_stdin;
   }

   /* Initialise error handler - we override error_exit */
   jh->dcinfo->err = jpeg_std_error((struct jpeg_error_mgr *)jh->jerr);
   jh->jerr->pub.error_exit = ip_jpeg_flag_error;

   /* Establish jump context for setjmp */
   if (setjmp(jh->jerr->sj_buffer)) {
      ip_log_error(ERR_JPEG, fname);
      goto exit_ojix;
   }

   /* initialise jpeg decompression handle */
   jpeg_create_decompress(jh->dcinfo);
   jh->init = TRUE;

   /* Link jpeg decompression to opened file */

   jpeg_stdio_src(jh->dcinfo, jh->f);

   /* Read jpeg header and parse */

   jpeg_read_header(jh->dcinfo, TRUE);
   jh->size.x = jh->dcinfo->image_width;
   jh->size.y = jh->dcinfo->image_height;
   if (jh->dcinfo->num_components == 1) {
      /* grey-scale image. */
      jh->type = IM_BYTE;
   }else if (jh->dcinfo->num_components == 3) {
      /* Hopefully is three channel colour image */
      jh->type = IM_RGB;
   }else{
      /* Dunno what this image is! */
      ip_log_error(ERR_INVALID_JPEG_TYPE,jh->fname);
      goto exit_ojix;
   }

   if (jh->dcinfo->data_precision != 8) {
      ip_log_error(ERR_INVALID_JPEG_TYPE,jh->fname);
      goto exit_ojix;
   }

 exit_ojix:			/* Error exit */
 
   if (ip_error != OK && jh != NULL) {
      if (jh->init) {
	 jpeg_destroy_decompress(jh->dcinfo);
      }
      if (jh->dcinfo) ip_free(jh->dcinfo);
      if (jh->jerr) ip_free(jh->jerr);
      ip_free(jh);
      jh = NULL;
   }

   ip_pop_routine();
   return jh;
}
   



/*

NAME:

   read_jpeg_image() \- Read in image data from opened JPEG file

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/jpeg.h>

   ip_image * read_jpeg_image( handle )
   ip_jpeg_handle * handle;

DESCRIPTION:

   Reads in JPEG image corresponding to 'handle' from the
   disk. Returns the image data with image an opened image handle. One
   neads to call open_jpeg_image() to get a valid JPEG handle before
   calling this routine.  Returns NULL if an error occurs.

ERRORS:

   ERR_BAD_JPEG_FILE - JPEG file is not fully JPEG compliant
   ERR_INVALID_JPEG_TYPE - Unsupported JPEG image file type
   ERR_JPEG - libjpeg reported an error
   ERR_OUT_OF_MEM

SEE ALSO:

   open_jpeg_image()   read_jpeg_imagex()
   read_jpeg_tag()     close_jpeg_image()
   write_jpeg_image()  write_jpeg_imagex()
   ip_read_image()

*/

ip_image *read_jpeg_image(ip_jpeg_handle *ph)
{
   ip_log_routine("read_jpeg_image");
   return  rd_jpeg_image(ph, 0, ph->size.y);
}


   
/*

NAME:

   read_jpeg_imagex() \- Read in part/whole of a JPEG image

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/jpeg.h>

   ip_image * read_jpeg_imagex( handle, first_row, nrows )
   ip_jpeg_handle * handle;
   int first_row;
   int nrows;

DESCRIPTION:

   Reads in jpeg image rows corresponding to 'handle' from the
   disk. Returns an image. One needs to call open_jpeg_image() to get
   a valid 'handle' before calling this routine.  Returns NULL if an
   error occurs.

   The first row of an image is numbered zero. Hence to read only the
   first row of an image specify 'first_row'=0 and 'nrows'=1.

   The image returned has 'nrows' rows in it.

ERRORS:

   ERR_OOR_PARAMETER
   ERR_BAD_JPEG_FILE - JPEG file is not fully JPEG compliant
   ERR_INVALID_JPEG_TYPE - Unsupported JPEG image file type
   ERR_JPEG - libjpeg reported an error
   ERR_OUT_OF_MEM

SEE ALSO:

   open_jpeg_image()   read_jpeg_image()
   read_jpeg_tag()     close_jpeg_image()
   write_jpeg_image()  write_jpeg_imagex()
   ip_read_image()

*/


ip_image *read_jpeg_imagex(ip_jpeg_handle *ph, int first_row, int nrows)
{
   ip_log_routine("read_jpeg_imagex");
   return  rd_jpeg_image(ph, first_row, nrows);
}



static ip_image *rd_jpeg_image(ip_jpeg_handle *jh, int first_row, int nrows)
{
   ip_image *im;

   /* ip_log_routine() 'sposed to be done by calling routine.  */

   /* Check first_row and nrows are within range */

   if (NOT ip_parm_inrange("first_row", first_row, 0, jh->size.y-1))
      return NULL;

   if (NOT ip_parm_inrange("number_rows", nrows, 1, jh->size.y-first_row))
      return NULL;

   /* Allocate image */

   if ((im = ip_alloc_image(jh->type, (ip_coord2d){jh->size.x, nrows})) == NULL)
      goto exit_rji;

   /* In case of JPEG library error */

   if (setjmp(jh->jerr->sj_buffer)) {
      ip_log_error(ERR_JPEG, jh->fname);
      goto exit_rji;
   }

   jpeg_start_decompress(jh->dcinfo);
   while (jh->dcinfo->output_scanline < jh->dcinfo->output_height) {
      jpeg_read_scanlines(jh->dcinfo,
			  (JSAMPARRAY)&im->imrow.v[jh->dcinfo->output_scanline], 1);
   }

 exit_rji:

   if (ip_error != OK) {
      ip_free_image(im);
      im = NULL;
   }
   
   ip_pop_routine();
   return im;
}



/*

NAME:

   close_jpeg_image() \- Close previously opened JPEG image file

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/jpeg.h>

   void close_jpeg_image( handle ) 
   ip_jpeg_handle *handle;

DESCRIPTION:

   Closes jpeg file associated with 'handle'. 'Handle' is not valid
   anymore after calling close_jpeg_image(). Safe to call with NULL
   'handle'.

SEE ALSO:

   open_jpeg_image()

*/

void close_jpeg_image(ip_jpeg_handle *jh)
{
   ip_log_routine("close_jpeg_image");

   if (jh) {
      if (jh->dcinfo && jh->init) {
	 if (setjmp(jh->jerr->sj_buffer)) {
	    ip_log_error(ERR_JPEG,jh->fname);
	 }else{
	    jpeg_destroy_decompress(jh->dcinfo);
	 }
	 if (jh->jerr) ip_free(jh->jerr);
	 if (jh->dcinfo) ip_free(jh->dcinfo);
      }
      if (jh->f && jh->f != stdin) {
	 if (fclose(jh->f) != 0)
            ip_log_error(ERR_FILE_CLOSE,jh->fname);
      }
      ip_free(jh);
   }
   ip_pop_routine();
}

