/*
   Module: strel.c
   Author: M.J.Cree

   Copyright (C) 2015, 2017 Michael J. Cree

   Structuring elements for image morphology.

   Entry points:

      ip_alloc_strel()
      ip_free_strel()
      ip_strel_reflect()
      ip_strel_set_origin()
      ip_strel_shift_origin()
      ip_strel_dump()
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

#include <ip/ip.h>
#include <ip/morph.h>

#include "internal.h"
#include "utils.h"


/* For making a histogram of chord lengths in an image */
struct chord_freq {
    int freq;
    int ulidx;
};



/*
 * Helper functions.
 */

/* Allocate the chord and unique length arrays. */
static int alloc_strel_chords(ip_strel *se, int num_chords, int num_unique_len)
{
    se->chords = ip_mallocx(num_chords * sizeof(ip_chord));
    se->ulength = ip_mallocx(num_unique_len * sizeof(int));

    if (!se->chords || !se->ulength) {
	if (se->chords) ip_free(se->chords);
	if (se->ulength) ip_free(se->ulength);
	se->num_chords = se->num_unique = 0;
	se->chords = NULL;
	se->ulength = NULL;
	return FALSE;
    }

    se->num_chords = num_chords;
    se->num_unique = num_unique_len;

    return TRUE;
}


/* Count the number of horizontal chords in the image im */
static int count_h_chords(ip_image *im)
{
    int chord;
    int num_h_chords = 0;

    for (int j=0; j<im->size.y; j++) {
	uint8_t *iptr = IM_ROWADR(im, j, v);
	chord = 0;

	for (int i=0; i<im->size.x; i++) {
	    if (*iptr++)
		chord = 1;
	    else{
		if (chord) {
		    num_h_chords++;
		    chord = 0;
		}
	    }
	}
	if (chord) {
	    num_h_chords++;
	    chord = 0;
	}
    }

    return num_h_chords;
}


/* Count the number of vertical chords in the image im */
static int count_v_chords(ip_image *im)
{
    int chord;
    int num_v_chords = 0;

    for (int i=0; i<im->size.x; i++) {
	chord = 0;

	for (int j=0; j<im->size.y; j++) {
	    if (IM_PIXEL(im, i, j, ub))
		chord = 1;
	    else{
		if (chord) {
		    num_v_chords++;
		    chord = 0;
		}
	    }
	}
	if (chord) {
	    num_v_chords++;
	    chord = 0;
	}
    }

    return num_v_chords;
}


/*
 * Determine the number of unique lengths in the chord length histogram then
 * allocate the unique lengths array and complete the chords ulidx field
 * which is the link back into the unique lengths array.  Note that a unique
 * length in the unique length array cannot jump up by more than two times
 * the last entry in the array thus we also insert extra lengths into the
 * unique length array to satisfy this condition.
 */

static int calculate_unique_lengths(ip_strel *se, struct chord_freq *lengths, int num)
{
    int num_unique, last;

    /* We must have the length of one for starters */
    if (!lengths[0].freq) {
	lengths[0].freq = 1;
    }
    lengths[0].ulidx = 0;
    num_unique = 1;
    last = 1;
    /* Count the number of unique lengths and identify extras */
    for (int j=1; j<num; j++) {
	if (lengths[j].freq) {
	    while (2*last < (j+1)) {
		/* Too big a jump; need to insert new lengths */
		last *= 2;
		lengths[last-1].freq = 1;
		lengths[last-1].ulidx = num_unique;
		num_unique++;
	    }
	    lengths[j].ulidx = num_unique;
	    num_unique++;
	    last = j+1;
	}
    }

    /*
     * Now that we know the number of unique chord lengths we can allocate
     * and construct the unique length array.
     */
    if ((se->ulength = ip_mallocx(num_unique * sizeof(ip_chord))) == NULL)
	return 0;
    se->num_unique = num_unique;

    last = 0;
    for (int j=0; j<num; j++) {
	if (lengths[j].freq)
	    se->ulength[last++] = j+1;
    }

    /*
     * Put links back from the structuring element chords to the unique
     * length array.
     */
    for (int j=0; j<se->num_chords; j++) {
	se->chords[j].ulidx = lengths[se->chords[j].len-1].ulidx;
    }

    return 1;
}


/* Insert the horizontal chords from the image im into the structuring element se */
static int insert_h_chords(ip_strel *se, ip_image *im)
{
    int chord = 0;
    struct chord_freq *lengths;

    if ((lengths = ip_mallocx(im->size.x * sizeof(*lengths))) == NULL)
	return 0;
    memset(lengths, 0, im->size.x * sizeof(*lengths));

    for (int j=0; j<im->size.y; j++) {
	uint8_t *iptr = IM_ROWADR(im, j, v);
	int len = 0;

	for (int i=0; i<im->size.x; i++) {
	    if (*iptr++) {
		len++;
	    }else{
		if (len) {
		    se->chords[chord].x = i - len;
		    se->chords[chord].y = j;
		    se->chords[chord].len = len;
		    se->chords[chord].ulidx = 0;
		    se->chords[chord].ct_row = NULL;
		    lengths[len-1].freq++;
		    len = 0;
		    chord++;
		}
	    }
	}
	if (len) {
	    se->chords[chord].x = im->size.x - len;
	    se->chords[chord].y = j;
	    se->chords[chord].len = len;
	    se->chords[chord].ulidx = 0;
	    se->chords[chord].ct_row = NULL;
	    lengths[len-1].freq++;
	    len = 0;
	    chord++;
	}
    }

    chord = calculate_unique_lengths(se, lengths, im->size.x);
    ip_free(lengths);

    return chord;
}


/* Insert the vertical chords from the image im into the structuring element se */
static int insert_v_chords(ip_strel *se, ip_image *im)
{
    int chord = 0;
    struct chord_freq *lengths;

    if ((lengths = ip_mallocx(im->size.y * sizeof(*lengths))) == NULL)
	return 0;
    memset(lengths, 0, im->size.y * sizeof(*lengths));

    for (int i=0; i<im->size.x; i++) {
	int len = 0;

	for (int j=0; j<im->size.y; j++) {
	    if (IM_PIXEL(im, i, j, ub)) {
		len++;
	    }else{
		if (len) {
		    se->chords[chord].x = i;
		    se->chords[chord].y = j - len;
		    se->chords[chord].len = len;
		    se->chords[chord].ulidx = 0;
		    se->chords[chord].ct_row = NULL;
		    lengths[len-1].freq++;
		    len = 0;
		    chord++;
		}
	    }
	}
	if (len) {
	    se->chords[chord].x = i;
	    se->chords[chord].y = im->size.y - len;
	    se->chords[chord].len = len;
	    se->chords[chord].ulidx = 0;
	    se->chords[chord].ct_row = NULL;
	    lengths[len-1].freq++;
	    len = 0;
	    chord++;
	}
    }

    chord = calculate_unique_lengths(se, lengths, im->size.y);
    ip_free(lengths);

    return chord;
}


/*
 * Create a line structuring element
 *
 * ** TBD **
 *
 * Have only implemented 0, 90, 180 and 270 degree lines.
 */

static int create_line_strel(ip_strel *se, int length, double angle)
{
    int num_unique_lens;

    /* Check length */
    if (NOT ip_parm_greatereq("length", (double)length, 0.0))
	return 0;

    num_unique_lens = ceil(log2((double)length)) + 1;
    if (angle == 0.0 || angle == 90.0 || angle == 180.0 || angle == 270.0) {
        if (!alloc_strel_chords(se, 1, num_unique_lens))
	    return 0;
	se->direction = (angle == 0.0 || angle == 180.0) ? 0 : 1;
	if (angle == 0.0 || angle == 270.0) {
	    se->chords[0] = (ip_chord){0, 0, length, num_unique_lens-1, NULL};
	    se->bbox.origin.x = se->bbox.origin.y = 0;
	    if (angle == 0.0)
		se->bbox.size = (ip_coord2d){length, 1};
	    else
		se->bbox.size = (ip_coord2d){1, length};
	}else if (angle == 180.0) {
	    se->chords[0] = (ip_chord){-(length-1), 0, length, num_unique_lens-1, NULL};
	    se->bbox.origin = (ip_coord2d){-(length-1), 0};
	    se->bbox.size = (ip_coord2d){length, 1};
	}else{
	    se->chords[0] = (ip_chord){0, -(length-1), length, num_unique_lens-1, NULL};
	    se->bbox.origin = (ip_coord2d){0, -(length-1)};
	    se->bbox.size = (ip_coord2d){1, length};
	}
	for (int j=0, len=1; j<num_unique_lens-1; j++, len<<=1) {
	    se->ulength[j] = len;
	}
	se->ulength[num_unique_lens-1] = length;
    }else{
	ip_log_error(ERR_NOT_IMPLEMENTED);
	return 0;
    }

    return 1;
}



static int create_rectangular_strel(ip_strel *se, int width, int height)
{
    int num_unique_lengths;
    int smallest, largest;
    ip_coord2d origin;

    if (NOT ip_parm_greatereq("width", (double)width, 0.0))
	return 0;
    if (NOT ip_parm_greatereq("height", (double)height, 0.0))
	return 0;

    origin = (ip_coord2d){-width/2, -height/2};
    se->bbox.size = (ip_coord2d){width, height};
    se->bbox.origin = origin;
    se->rectangular = 1;

    /*
     * The above set fields are all that are used by im_erode(), etc., to
     * effect the morphology with a rectangular structuring element,
     * nevertheless we construct the chord and unique length arrays just in
     * case this structuring element is used in calling
     * im_morph_chord_erode(), etc., routines directly from outside the IP
     * library.
     */
    if (width >= height) {
	/* Use horizontal chords */
	se->direction = 0;
	largest = width;
	smallest = height;
    }else{
	/* Use vertical chords */
	se->direction = 1;
	largest = height;
	smallest = width;
    }

    num_unique_lengths = ceil(log2((double)largest)) + 1;
    if (NOT alloc_strel_chords(se, smallest, num_unique_lengths))
	return 0;

    for (int j=0; j<smallest; j++)
	if (se->direction)
	    se->chords[j] = (ip_chord){origin.x+j, origin.y, largest, num_unique_lengths-1, NULL};
	else
	    se->chords[j] = (ip_chord){origin.x, origin.y+j, largest, num_unique_lengths-1, NULL};

    for (int j=0, len=1; j<num_unique_lengths-1; j++, len<<=1) {
	se->ulength[j] = len;
    }
    se->ulength[num_unique_lengths-1] = largest;

    return 1;
}



static int create_diamond_strel(ip_strel *se, int size)
{
    if (NOT ip_parm_greatereq("size", (double)size, 0.0))
	return 0;

    ip_log_error(ERR_NOT_IMPLEMENTED);
    return 0;
}



static int create_image_strel(ip_strel *se, ip_image *im)
{
    int num_h_chords = 0;
    int num_v_chords = 0;
    int num_chords;
    int success;

    if (NOT (im->type == IM_BINARY || im->type == IM_UBYTE)) {
	ip_log_error(ERR_BAD_TYPE);
	return 0;
    }

    se->bbox.origin = (ip_coord2d){0,0};
    se->bbox.size = im->size;

    num_h_chords = count_h_chords(im);
    num_v_chords = count_v_chords(im);

    if (num_v_chords >= num_h_chords) {
	/* Fewer horizontal chords so use horizontal chords */
	se->direction = 0;
	num_chords = num_h_chords;
    }else{
	/* Fewer vertical chords so use vertical chords */
	se->direction = 1;
	num_chords = num_v_chords;
    }

    se->num_chords = num_chords;
    se->chords = ip_mallocx(num_chords * sizeof(ip_chord));
    if (!se->chords)
	return 0;

    if (se->direction)
	success = insert_v_chords(se, im);
    else
	success = insert_h_chords(se, im);

    if (NOT success) {
	ip_free(se->chords);
	se->chords = NULL;
	se->num_chords = 0;
    }

    return success;
}



static int create_disc_strel(ip_strel *se, int radius)
{
    ip_image *im;
    int size;
    int retcode;

    if (NOT ip_parm_greatereq("radius", (double)radius, 0.0))
	return 0;

    /* Laziness prevails; we create an image because it is easier. */
    size = 2*radius + 1;
    im = ip_alloc_image(IM_BINARY, (ip_coord2d){size, size});
    if (!im)
	return 0;

    /* and set those pixels whose centre lies within radius of the centre */
    for (int j=0; j<size; j++) {
	uint8_t *iptr = IM_ROWADR(im, j, v);
	for (int i=0; i<size; i++) {
	    if (hypot((double)(i-radius), (double)(j-radius)) <= (double)radius)
		*iptr = 255;
	    else
		*iptr = 0;
	    iptr++;
	}
    }

    /* ... then convert the image to a structuring element. */
    retcode = create_image_strel(se, im);
    if (retcode) {
	ip_strel_set_origin(se, (ip_coord2d){radius, radius});
    }

    ip_free_image(im);

    return retcode;
}



static int create_n4_strel(ip_strel *se)
{
    if (NOT alloc_strel_chords(se, 3, 3))
	return 0;

    se->bbox.size = (ip_coord2d){3, 3};
    se->bbox.origin = (ip_coord2d){-1, -1};
    se->rectangular = 0;
    se->direction = 0;
    se->chords[0] = (ip_chord){0, -1, 1, 0, NULL};
    se->chords[1] = (ip_chord){-1, 0, 3, 2, NULL};
    se->chords[2] = (ip_chord){0, 1, 1, 0, NULL};
    se->ulength[0] = 1;
    se->ulength[1] = 2;
    se->ulength[2] = 3;

    return 1;
}


/*

NAME:

   ip_alloc_strel()

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/morph.h>

   void  ip_alloc_strel( type, ... )
   int type;

DESCRIPTION:

   Allocate a flat structuring element for performing mathematical
   morphology on an image.  The types are:

\|     IP_STREL_LINE
\|     IP_STREL_SQUARE
\|     IP_STREL_RECTANGLE
\|     IP_STREL_DISC
\|     IP_STREL_DIAMOND
\|     IP_STREL_IMAGE
\|     IP_STREL_N4
\|     IP_STREL_N8

   The line type takes the arguments:

\|   int length, double angle;

   where the angle is measured from x-axis anticlockwise.  The square type
   takes the argument:

\|   int size

   and creates a square structuring element of size x size pixels.  The
   rectangular type takes the arguments:

\|   int width, int height;

   The disc structuring element takes the argument:

\|   int radius

   and creates a disc with each pixel whose centre is within radius pixels
   of the origin as part of the structuring element.  The diamond
   structuring element is not implemented.  The image structuring element
   takes as its argument:

\|   ip_image *image;

   and can be used to create an arbitrary structuring element.  The image
   must be of BINARY or UBYTE type.  The origin is placed at the top-left
   corner of the image (whether that pixel is set or not) and it is up to
   the caller to shift the origin to somewhere meaningful with
   ip_strel_{shift|set}_origin().

ERRORS:

   ERR_OUT_OF_MEM
   ERR_BAD_TYPE
   ERR_PARM_BAD
   ERR_PARM_TOO_SMALL

SEE ALSO:

   ip_free_strel()    ip_strel_dump()
   ip_strel_reflect() ip_strel_set_origin()  ip_strel_shift_origin()
   im_dilate()        im_erode()             im_open()               im_close()

*/

ip_strel *ip_alloc_strel(int type, ...)
{
    va_list ap;
    ip_strel *se;
    int success = TRUE;

    ip_log_routine(__func__);

    if ((se = ip_malloc(sizeof(*se))) == NULL)
	return NULL;
    se->rectangular = 0;
    se->chords = NULL;
    se->ulength = NULL;

    va_start(ap, type);

    switch (type) {
    case IP_STREL_LINE: {
	int length = va_arg(ap, int);
	double angle = va_arg(ap, double);
	success = create_line_strel(se, length, angle);
	break;
    }
    case IP_STREL_SQUARE: {
	int width = va_arg(ap, int);
	success = create_rectangular_strel(se, width, width);
	break;
    }
    case IP_STREL_RECTANGLE: {
	int width = va_arg(ap, int);
	int height = va_arg(ap, int);
	success = create_rectangular_strel(se, width, height);
	break;
    }
    case IP_STREL_DISC: {
	int radius = va_arg(ap, int);
	success = create_disc_strel(se, radius);
	break;
    }
    case IP_STREL_DIAMOND: {
	int size = va_arg(ap, int);
	success = create_diamond_strel(se, size);
	break;
    }
    case IP_STREL_IMAGE: {
	ip_image *im = va_arg(ap, ip_image *);
	success = create_image_strel(se, im);
	break;
    }
    case IP_STREL_N4:
	success = create_n4_strel(se);
	break;
    case IP_STREL_N8:
	success = create_rectangular_strel(se, 3, 3);
	break;
    default:
	ip_log_error(ERR_PARM_BAD, "type", type);
	success = 0;
	break;
    }

    if (NOT success) {
	ip_free(se);
	se = NULL;
    }

    va_end(ap);

    ip_pop_routine();
    return se;
}



/*

NAME:

   ip_free_strel()

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/morph.h>

   void  ip_free_strel( se )
   ip_strel *se;

DESCRIPTION:

   Free a previously allocated structuring element.

SEE ALSO:

   ip_alloc_strel()

*/

void ip_free_strel(ip_strel *se)
{
    ip_log_routine(__func__);

    if (se) {
	if (se->chords) ip_free(se->chords);
	if (se->ulength) ip_free(se->ulength);
	ip_free(se);
    }

    ip_pop_routine();
}



/*

NAME:

   ip_strel_reflect()

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/morph.h>

   void  ip_strel_reflect( se )
   ip_strel *se;

DESCRIPTION:

   Reflect the structuring element about the origin.  Used in the dilation
   routines, for example, so that an erosion followed by a dilation will
   give an opening.

SEE ALSO:

   ip_alloc_strel()     ip_strel_set_origin()    ip_strel_shift_origin()

*/

void ip_strel_reflect(ip_strel *se)
{
    if (se->direction) {
	/* Vertical chords */
	for (int j=0; j<se->num_chords; j++) {
	    se->chords[j].x = -se->chords[j].x;
	    se->chords[j].y = -se->chords[j].y - (se->chords[j].len-1);
	}
    }else{
	/* Horizontal chords */
	for (int j=0; j<se->num_chords; j++) {
	    se->chords[j].x = -se->chords[j].x - (se->chords[j].len-1);
	    se->chords[j].y = -se->chords[j].y;
	}
    }
    se->bbox.origin.x = -(se->bbox.size.x-1) - se->bbox.origin.x;
    se->bbox.origin.y = -(se->bbox.size.y-1) - se->bbox.origin.y;
}



/*

NAME:

   ip_strel_set_origin()

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/morph.h>

   int  ip_strel_set_origin( se, origin )
   ip_strel *se;
   ip_coord2d origin;

DESCRIPTION:

   Set the origin of the structuring element to a new position.  The
   top-left of the structuring element is (0,0) thus any new origin must
   have positive coordinates and must not exceed the structuring element
   size, that is, the origin *must* be within the structuring element
   bounding box.

ERRORS:

   ERR_PARM_NOT_IN_BOX

SEE ALSO:

   ip_alloc_strel()     ip_strel_shift_origin()
   ip_strel_reflect()

*/

int ip_strel_set_origin(ip_strel *se, ip_coord2d origin)
{
    ip_coord2d shift;

    ip_log_routine(__func__);

    if (NOT ip_parm_point_in_box("strel_origin", origin, (ip_box){{0,0}, se->bbox.size}))
	return ip_error;

    shift.x = -se->bbox.origin.x - origin.x;
    shift.y = -se->bbox.origin.y - origin.y;

    se->bbox.origin.x = -origin.x;
    se->bbox.origin.y = -origin.y;

    for (int j=0; j<se->num_chords; j++) {
	se->chords[j].x += shift.x;
	se->chords[j].y += shift.y;
    }

    ip_pop_routine();
    return OK;
}



/*

NAME:

   ip_strel_shift_origin()

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/morph.h>

   int  ip_strel_shift_origin( se, shift )
   ip_strel *se;
   ip_coord2d shift;

DESCRIPTION:

   Shift the structuring element origin to a new position. The shift is
   relative to the current origin.  The resultant origin must remain within
   the structuring element bounding box.

ERRORS:

   ERR_PARM_NOT_IN_BOX

SEE ALSO:

   ip_alloc_strel()     ip_strel_set_origin()
   ip_strel_reflect()

*/

int ip_strel_shift_origin(ip_strel *se, ip_coord2d shift)
{
    ip_coord2d norigin;

    ip_log_routine(__func__);

    norigin.x = se->bbox.origin.x + shift.x;
    norigin.y = se->bbox.origin.y + shift.y;

    ip_strel_set_origin(se, norigin);

    ip_pop_routine();
    return ip_error;
}



/*

NAME:

   ip_strel_dump()

PROTOTYPE:

   #include <stdio.h>
   #include <ip/ip.h>
   #include <ip/morph.h>

   void  ip_strel_dump( f, se )
   FILE *f;
   ip_strel *se;

DESCRIPTION:

   Dump an ascii representation of the structuring element 'se' to the
   opened file 'f'.  Primarily intended for debugging purposes.

SEE ALSO:

   ip_alloc_strel()

*/

void ip_strel_dump(FILE *f, ip_strel *se)
{
    ip_image *im;

    static char *dir_str[] = {
	"horizontal", "vertical"
    };

    fprintf(f, "Structuring element at %p:\n", se);
    fprintf(f, "  origin=(%d,%d) and of size (%d,%d) thus bottom-right at (%d,%d).\n",
	    se->bbox.origin.x, se->bbox.origin.y, se->bbox.size.x, se->bbox.size.y,
	    ip_box_botr(se->bbox).x, ip_box_botr(se->bbox).y);
    if (se->direction >= 0 && se->direction <= 1)
	fprintf(f, "  %d chord(s) in the %s direction.\n", se->num_chords, dir_str[se->direction]);
    else
	fprintf(f, "  %d chord(s) in the invalid direction %d.\n", se->num_chords, se->direction);
    fprintf(f, "  with %d unique chord lengths of:", se->num_unique);
    for (int j=0; j<se->num_unique; j++) {
	fprintf(f, " %2d", se->ulength[j]);
    }

    for (int j=0; j<se->num_chords; j++) {
	if (j % 8 == 0)
	    fprintf(f, "\n    %2d:", j);
	fprintf(f, " (%3d,%3d)%d-%d", se->chords[j].x, se->chords[j].y,
				      se->chords[j].len, se->chords[j].ulidx);
    }
    fputc('\n',f);

    im = ip_alloc_image(IM_BINARY, se->bbox.size);
    im_clear(im);

    for (int j=0; j<se->num_chords; j++) {
	ip_coord2d startpos;

	startpos.x = se->chords[j].x - se->bbox.origin.x;
	startpos.y = se->chords[j].y - se->bbox.origin.y;
	for (int l=0; l<se->chords[j].len; l++) {
	    if (se->direction)
		IM_PIXEL(im, startpos.x, startpos.y + l, ub) = 255;
	    else
		IM_PIXEL(im, startpos.x + l, startpos.y, ub) = 255;
	}
    }

    for (int j=0; j<im->size.y; j++) {
	uint8_t *iptr = IM_ROWADR(im, j, v);

	printf("%2d: ", j);
	for (int i=0; i<im->size.x; i++) {
	    if (j == -se->bbox.origin.y && i == -se->bbox.origin.x) {
		fprintf(f, "%c", ".O"[*iptr++ != 0]);
	    }else{
		fprintf(f, "%c", " X"[*iptr++ != 0]);
	    }
	}
	fputc('\n', f);
    }
    ip_free_image(im);
}
