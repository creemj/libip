/*
   Header: ip/internal.h
   Author: M. J. Cree

   (C) 1995-1998, 2013, 2015-2016 Michael J. Cree

   Some guff private to the IP library.

*/

#ifndef IP_INTERNAL_H
#define IP_INTERNAL_H

#include <stdint.h>
#include <float.h>
#include <ip/types.h>
#include <ip/image.h>
#include <ip/error.h>
#include "../config.h"

#ifdef HAVE_SIMD
#define ROW_ALIGNMENT SIMD_VECTOR_SIZE
#else
#define ROW_ALIGNMENT MACHINE_NATURAL_SIZE
#endif

/* Compile time detection of endianess if need be */
#if !defined(HAVE_LITTLE_ENDIAN) && !defined(HAVE_BIG_ENDIAN)
#  ifdef __APPLE__
#    include <machine/endian.h>
#    if BYTE_ORDER == LITTLE_ENDIAN
#      define HAVE_LITTLE_ENDIAN
#    else
#      define HAVE_BIG_ENDIAN
#    endif
#  else
#    include <endian.h>
#    if __BYTE_ORDER == __LITTLE_ENDIAN
#      define HAVE_LITTLE_ENDIAN
#    else
#      define HAVE_BIG_ENDIAN
#    endif
#  endif
#endif


#define MIN(a,b) ((a)<(b)?(a):(b))
#define MAX(a,b) ((a)>(b)?(a):(b))

#ifdef HAVE_64BIT
#define IROUND(x) lround(x)
#define LROUND(x) lround(x)
#else
#define IROUND(x) lround(x)
#define LROUND(x) llround(x)
#endif

/* internal global variable */
extern int ip_global_flags;
extern int ip_l1_cache_size;
extern int ip_l2_cache_size;

#define IP_USE_FAST_CODE (!!(ip_global_flags & IP_FAST_CODE))
#define IP_USE_HWACCEL (!!(ip_global_flags & IP_HWACCEL))

/* Used by the Fast Code pixelwise processing paths. */
typedef struct {
    unsigned long mask_even;
    unsigned long mask_odd;
    unsigned long sign_bits;
} fast_code_masks_t;

extern fast_code_masks_t fast_code_masks[IM_TYPETOTAL+1];

typedef uint8_t  BINARY_type;
typedef uint8_t  UBYTE_type;
typedef int8_t   BYTE_type;
typedef uint16_t USHORT_type;
typedef int16_t  SHORT_type;
typedef uint32_t ULONG_type;
typedef int32_t  LONG_type;
typedef float    FLOAT_type;
typedef double   DOUBLE_type;
typedef ip_complex  COMPLEX_type;
typedef ip_dcomplex  DCOMPLEX_type;
typedef ip_rgb   RGB_type;
typedef ip_rgba  RGBA_type;
typedef ip_lrgb  RGB16_type;

#define BYTE_min INT8_MIN
#define BYTE_max INT8_MAX
#define UBYTE_min 0
#define UBYTE_max UINT8_MAX
#define SHORT_min INT16_MIN
#define SHORT_max INT16_MAX
#define USHORT_min 0
#define USHORT_max UINT16_MAX
#define LONG_min INT32_MIN
#define LONG_max INT32_MAX
#define ULONG_min 0
#define ULONG_max UINT32_MAX
#define FLOAT_min -FLT_MAX
#define FLOAT_max FLT_MAX
#define DOUBLE_min -DBL_MAX
#define DOUBLE_max DBL_MAX

typedef uint64_t  ULONG64_type;
typedef int64_t  LONG64_type;


/*
 * Row size with padding so that row size is always an integer multiple of
 * an unsigned long (for the fast code "poor man's SIMD") or a SIMD vector
 * (if compiling with SIMD support).
 */

#define PADDED_SIZE(x, s) (((x)+((s)-1)) & (~((s)-1)))
#define PADDED_ROW_SIZE(x) PADDED_SIZE(x, (size_t)ROW_ALIGNMENT)

/*
 * Calculations on number of bytes and pixels in padded row of an image
 * (i.e. calculations with the padding included).
 *
 * sizeof_padded_row(im)
 *     number of bytes in the padded image row.  It is a multiple of
 *     ROW_ALIGNMENT and is sufficient to hold im->size.x pixels in the
 *     line.
 *
 * pixels_in_padded_row(im)
 *     number of pixels that fit (properly) into the padded row.  The number
 *     of pixels will be greater than or equal to im->size.x and equal to
 *     the maximum number of pixels that fit in the padded line without any
 *     possibility of access past the end of the row.  Note that the sizeof
 *     padded row may not be an integer times the sizeof(pixel) as some
 *     pixel types (e.g. RGB) are not a power of two in size.
 *
 * containers_in_padded_row(im, s)
 *     number of containers of size s that fit into the padded row.  This
 *     routine should only be called with a value of s less than or equal
 *     to the value of ROW_ALIGNMENT.  That is, s can only be
 *     sizeof(uint32_t) on a 32-bit machine running generic code,
 *     sizeof(uint32_t) or sizeof(uint64_t) on a 64-bit machine running
 *     generic code, or any multiple of sizeof(uint32_t) up to the SIMD
 *     vector size when compiled for SIMD.  Exceeding these conditions
 *     results in ERR_ALGORITHM_FAULT being logged.
 *
 * contained_pixels_in_padded_row(im, s)
 *     given the number of containers in the padded row, convert back to the
 *     number of pixels.  The result will be greater than or equal to
 *     im->size.x and less than or equal to pixels_in_padded_row().
 *
 * The idea of the containers is that sometimes processing multiple smaller
 * sized pixels (e.g. UBYTE or USHORT) as uint32_t or uint64_t gives a
 * useful speed up when there is no SIMD alternative.
 *
 * Note that these routines only return valid results when the image is
 * fully allocated (i.e. when im->row_size has been determined and set in
 * the image object).
 */

static inline size_t sizeof_padded_row(ip_image *im)
{
    return im->row_size;
}

static inline size_t pixels_in_padded_row(ip_image *im)
{
    return im->row_size / ip_type_size(im->type);
}

static inline size_t containers_in_padded_row(ip_image *im, size_t s)
{
    int num = PADDED_SIZE(im->size.x * ip_type_size(im->type), s) / s;
    if (num * s > im->row_size) {
	ip_log_error(ERR_ALGORITHM_FAULT,
		     "Too many elements (%d) of size %zu for image row sized %zu.",
		     num, s, im->row_size);
    }
    return num;
}

static inline size_t contained_pixels_in_padded_row(ip_image *im, size_t s)
{
    return containers_in_padded_row(im, s) * (s / ip_type_size(im->type));
}

#endif

