/*
   Module: ip/file.c
   Author: M.J.Cree

   (C) 1996-2007,2014

   General image file load and save routines.

*/

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <errno.h>

#include <ip/image.h>
#include <ip/error.h>
#include <ip/flags.h>
#include <ip/file.h>

#include <png.h>
#include <ip/png.h>
#include <ip/tiff.h>
#include <ip/jpeg.h>
#include <ip/raw.h>
#include <ip/sunraster.h>
#include <ip/file.h>

static int lcmp(unsigned char *b, unsigned long l);


/* Substitute these names for NULL file names in file handling routines */

const char *ip_msg_stdin = "<stdin>";
const char *ip_msg_stdout = "<stdout>";

/* File types recognised */

const char *ip_msg_filetype[] = {
   "Unrecognised",
   "Visilog",
   "C_Images",
   "TIFF",
   "PPM",
   "VIFF",
   "SUN Raster",
   "PNG",
   "JPEG",
   "BMP",
   "GIF",
   "RAW"
};


/*

NAME:

   ip_filetype() \- Determine image file type

PROTOTYPE:

   #include <ip/ip.h>

   int  ip_filetype( fn )
   const char *fn;

DESCRIPTION:

   Returns the image file type of the specified file.

   Returned value is one of:

\|   FT_ERROR          - I/O error, etc. occured.
\|   FT_UNKNOWN        - Didn't recognise file.
\|   FT_VISILOG        - Visilog image
\|   FT_TIFF           - TIFF image
\|   FT_CIMAGES        - Foster Findlay C-Images file.
\|   FT_PPM            - PBM, PGM or PPM file.
\|   FT_VIFF           - Khoros VIFF image file.
\|   FT_SUNRASTER      - SUN raster image.
\|   FT_PNG            - Portable Network Graphics File
\|   FT_JPEG           - JPEG

   Note that the recognition of a image file type in this routine does not
   guarantee that the ip library has code to support read/write that
   particular file type.

ERRORS:

   ERR_FILE_OPEN
   ERR_SHORT_FILE

SEE ALSO:

   ip_filetypex()
   open_visilog_file()    open_tiff_file()   open_cimages_file()

*/

int ip_filetype(const char *fn)
{
   FILE *f;
   int ft;
   unsigned char header[4];

   ip_log_routine("ip_filetype");

   ft = FT_ERROR;
   f = NULL;

   if ((f = fopen(fn,"rb")) == NULL) {
      ip_log_error(ERR_FILE_OPEN,fn);
      goto exit_ft;
   }

   if (fread(header,1,4,f) != 4) {
      ip_log_error(ERR_SHORT_FILE,fn);
      goto exit_ft;
   }

   ft = ip_filetypex(header);

 exit_ft:

   if (f) fclose(f);
   ip_pop_routine();

   return ft;
}




/*

NAME:

   ip_filetypex() \- Determine image file type

PROTOTYPE:

   #include <ip/ip.h>

   int  ip_filetypex(  header )
   unsigned char header[4];

DESCRIPTION:

   'Header' is the first four bytes of an image file.  Ip_filetypex()
   returns the image file type corresponding to 'header'.

   Return value is one of:

\|   FT_ERROR          - I/O error, etc. occured.
\|   FT_UNKNOWN        - Didn't recognise file.
\|   FT_VISILOG        - Visilog image
\|   FT_TIFF           - TIFF image
\|   FT_CIMAGES        - Foster Findlay C-Images file.
\|   FT_PPM            - PBM, PGM or PPM file.
\|   FT_VIFF           - Khoros VIFF image file.
\|   FT_SUNRASTER      - SUN raster image.
\|   FT_PNG            - Portable Network Graphics File
\|   FT_JPEG           - JPEG

   Note that the recognition of a image file type in this routine does not
   guarantee that the ip library has code to support read/write that
   particular file type.

   Useful for handling pipes or unseekable files of which you don''t
   know file type yet.

SEE ALSO:

   ip_filetypex()         ip_read_filehdr()
   open_visilog_file()    open_tiff_file()   open_cimages_file()

*/

int ip_filetypex(unsigned char buf[4])
{
   int ft;

   ft = FT_UNKNOWN;

   /* Visilog version 3 and 4 images */
   if (lcmp(buf,0x00006932) || lcmp(buf,0x32690000)
	    || lcmp(buf,0x00006931) || lcmp(buf,0x31690000)) {
      ft = FT_VISILOG;
      goto exit_ftx;
   }

   /* TIFF images */
   if (lcmp(buf,0x49492a00) || lcmp(buf,0x4d4d002a)) {
      ft = FT_TIFF;		/* "MM" or "II" followed by 42 */
      goto exit_ftx;
   }

   /* Foster Findlay C_Images format */
   if (lcmp(buf,0x46464102)) {	/* "FFA" followed by 0x02 */
      ft = FT_CIMAGES;
      goto exit_ftx;
   }

   /* SUN raster file */
   if (lcmp(buf,0x59a66a95)) {
      ft = FT_SUNRASTER;
      goto exit_ftx;
   }

   /* PBM, PGM or PPM images */
   if (buf[0] == 'P' && (buf[1] >= '1' && buf[1] <= '6')) {
      ft = FT_PPM;		/* "P1" upto "P6" */
      goto exit_ftx;
   }

   /* Khoros VIFF image file */
   if (buf[0] == 0xab && buf[1] == 1) {
      ft = FT_VIFF;
      goto exit_ftx;
   }

   /* PNG image */
   if (png_check_sig(buf, 4)) {
      ft = FT_PNG;
      goto exit_ftx;
   }

   /* JPEG Image */
   if (lcmp(buf, 0xffd8ffe0UL) || lcmp(buf, 0xffd8ffe1UL)) {
      ft = FT_JPEG;
      goto exit_ftx;
   }

   /* BMP Image */
   if (buf[0] == 'B' && buf[1] == 'M') {
      ft = FT_BMP;
      goto exit_ftx;
   }

   /* GIF Image */
   if (lcmp(buf, 0x47494638)) {
      ft = FT_GIF;
      goto exit_ftx;
   }

 exit_ftx:

   return ft;
}




/*

NAME:

   ip_read_filehdr() \- Open file and determine image file type

PROTOTYPE:

   #include <ip/ip.h>

   int ip_read_filehdr( fname, f, header )
   const char *fname;
   FILE **f;
   unsigned char header[4];

DESCRIPTION:

   Open file of filename 'fname' and read in the first 4 bytes into
   'header'.  Intrepret image file type based on header.  Returns
   filetype code.  Argument 'f' returns with the file handle and
   'header' with the four bytes read.  Useful for using with
   unseekable files to decide which file loading code to use.  A NULL
   'fname' means use stdin.  If an error occurs then neither 'f' nor
   'header' will have anything valid.

ERRORS:

   ERR_FILE_OPEN
   ERR_SHORT_FILE

SEE ALSO:

   ip_filetypex()    open_visilog_imagex()

*/

int ip_read_filehdr(const char *fname, FILE **f, unsigned char header[4])
{
   int ft;

   ip_log_routine("ip_read_filehdr");

   *f = NULL;
   ft = FT_ERROR;

   if (fname) {
      if ((*f = fopen(fname,"rb")) == NULL) {
	 ip_log_error(ERR_FILE_OPEN,fname);
	 goto exit_rf;
      }
   }else{
      *f = stdin;
      fname = ip_msg_stdin;
   }

   if (fread(header,1,4,*f) != 4) {
      ip_log_error(ERR_SHORT_FILE,fname);
      fclose(*f);
      *f = NULL;
      goto exit_rf;
   }

   ft = ip_filetypex(header);

 exit_rf:

   ip_pop_routine();
   return ft;
}




/*

NAME:

   ip_read_image() \- General read image routine

PROTOTYPE:

   #include <ip/ip.h>

   image * ip_read_image( fname, allowable_types, ... ,-1 )
   const char *fname;
   int allowable_types;

DESCRIPTION:

   Read in an image (either a TIFF image, Sunraster Image or a Visilog
   image) and check that is of one of the allowable types on the argument
   list (which should be terminated with a -1).  For example, to read either
   BYTE or SHORT images use:

\|      im = ip_read_image( fname, IM_BYTE, IM_SHORT, -1);

   Returns NULL if an error occurs.

   This routine is much simpler to use than the open, read, close
   procedure present in the basic TIFF and Visilog image handling
   code.

   'Fname' may be NULL, in which case input is taken from stdin.  Note
   that only Visilog images can be read from stdin.

ERRORS:

   ERR_FILE_OPEN
   ERR_SHORT_FILE
   ERR_UNSUPPORTED_FILETYPE
   ERR_WRONG_IMAGE_TYPE

   also errors that can be generated by TIFF or Visilog image
   reading code.

SEE ALSO:

   open_tiff_image()      read_tiff_image()      close_tiff_image()
   open_visilog_image()   read_visilog_image()   close_visilog_image()
   open_sunraster_image() read_sunraster_image() close_sunraster_image()

*/


ip_image *ip_read_image(const char *fname, ...)
{
   va_list ap;
   int fltype,type,allowable;
   FILE *f;
   ip_png_handle *ph;
   ip_tiff_handle *th;
   ip_jpeg_handle *jh;
   ip_sunraster_handle *srh;
#if 0
   vhandle *vh;
#endif
   ip_image *im;
   int ok;
   unsigned char header[4];

   va_start(ap,fname);
   ip_log_routine("ip_read_image");

   im = NULL;
   th = NULL;
   ph = NULL;
   jh = NULL;
   srh = NULL;
#if 0
   vh = NULL;
#endif
   f = NULL;

   if ((fltype = ip_read_filehdr(fname, &f, header)) == FT_ERROR) {
      goto exit_ri;
   }

   if (fname == NULL) fname = ip_msg_stdin;

   type = -1;
   switch (fltype) {
    case FT_VISILOG:
#if 0
      if ((vh = open_visilog_imagex(f,fname,header,NO_FLAG)) == NULL)
	 goto exit_ri;
      type = vh->type;
#else
      ip_log_error(ERR_NOT_IMPLEMENTED);
#endif
      break;

    case FT_TIFF:
      if (f != stdin) {
	 fclose(f);
	 if ((th = open_tiff_image(fname)) == NULL)
	    goto exit_ri;
      }else{
	 ip_log_error(ERR_NO_STDIN,"Tiff");
	 goto exit_ri;
      }
      type = th->type;
      break;
   case FT_SUNRASTER:
      if ((srh = open_sunraster_imagex(f,fname,header,NO_FLAG)) == NULL)
	 goto exit_ri;
      type = srh->type;
      break;
   case FT_PNG:
      if ((ph = open_png_imagex(f,fname,header,NO_FLAG)) == NULL)
	 goto exit_ri;
      type = ph->type;
      break;
   case FT_JPEG:
      if ((jh = open_jpeg_imagex(f,fname,header,NO_FLAG)) == NULL)
	 goto exit_ri;
      type = jh->type;
      break;
    default:
      ip_log_error(ERR_UNSUPPORTED_FILETYPE,fname,ip_msg_filetype[fltype]);
      goto exit_ri;
   }

   ok = FALSE;
   allowable = va_arg(ap,int);
   while (allowable >=0 && allowable < IM_TYPEMAX) {
      if (allowable == type) ok = TRUE;
      allowable = va_arg(ap,int);
   }
   if (allowable == IM_TYPE_ANY)
       ok = TRUE;

   if (NOT ok) {
      ip_log_error(ERR_WRONG_IMAGE_TYPE,fname,ip_image_type_info[type].name);
      goto exit_ri;
   }

   switch (fltype) {
#if 0
   case FT_VISILOG:
      im = read_visilog_image(vh);
      break;
#endif
   case FT_TIFF:
      im = read_tiff_image(th);
      break;
   case FT_SUNRASTER:
      im = read_sunraster_image(srh);
      break;
   case FT_PNG:
      im = read_png_image(ph);
      break;
   case FT_JPEG:
      im = read_jpeg_image(jh);
      break;
   }

 exit_ri:

#if 0
   if (vh) 
      close_visilog_image(vh);
#else
   if (th)
      close_tiff_image(th);
   else if (srh)
      close_sunraster_image(srh);
   else if (ph)
      close_png_image(ph);
   else if (jh)
      close_jpeg_image(jh);
   else if (f && f != stdin)
      fclose(f);
#endif
   ip_pop_routine();
   va_end(ap);
   return im;
}



static int lcmp(unsigned char *b, unsigned long l)
{
   unsigned long t;

   t = ((unsigned long)b[0] << 24) + ((unsigned long)b[1] << 16) + 
	((unsigned long)b[2] << 8) + (unsigned long)b[3];
   return (l == t);
}
