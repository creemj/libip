/*
   Module: ip/palette.c
   Author: M.J.Cree

   Routines for managing PALETTE type images.

   Copyright (C) 1996-1997, 2007, 2018 Michael J. Cree
*/


#include <stdlib.h>
#include <string.h>

#include <ip/ip.h>

#include "internal.h"
#include "utils.h"


/*

NAME:

   im_palette_set() \- Set the palette of a PALETTE image

PROTOTYPE:

   #include <ip/ip.h>

   void  im_palette_set( im, pal )
   ip_image *im;
   ip_rgb pal[256];

DESCRIPTION:

   Load the ip_rgb palette 'pal' into the PALETTE type image 'im'.
   The palette must have 256 entries.

ERRORS:

   ERR_BAD_TYPE

SEE ALSO:

   im_palette_get()   im_palette_copy()    im_palette_compress()

*/

int im_palette_set(ip_image *im, ip_rgb *pal)
{
    ip_log_routine("im_load_palette");

    if (im->type == IM_PALETTE)
	memcpy(im->palette, pal, 256*sizeof(ip_rgb));
    else
	ip_log_error(ERR_BAD_TYPE);

    ip_pop_routine();
    return ip_error;
}




/*

NAME:

   im_palette_copy() \- Copy palette of one image into another

PROTOTYPE:

   #include <ip/ip.h>

   int  im_palette_copy( dest, src )
   ip_image *dest;
   ip_image *src;

DESCRIPTION:

   Copy palette of image 'src' into image 'dest'.  Both images must be
   PALETTE images.

ERRORS:

   ERR_BAD_TYPE

SEE ALSO:

   im_palette_set()   im_palette_get()

*/

int im_palette_copy(ip_image *dest, ip_image *src)
{
   ip_log_routine("im_copy_palette");

   if (src->type != IM_PALETTE || dest->type != IM_PALETTE) {
      ip_log_error(ERR_BAD_TYPE);
      ip_pop_routine();
      return ip_error;
   }

   memcpy(dest->palette, src->palette, 256*sizeof(ip_rgb));

   ip_pop_routine();
   return OK;
}




/*

NAME:

   im_palette_get() \- Get the palette of a PALETTE image

PROTOTYPE:

   #include <ip/ip.h>

   int  im_palette_get( im, pal )
   ip_image *im;
   ip_rgb pal[256];

DESCRIPTION:

   Get the palette from the PALETTE image 'im' into the space pointed to by
   'pal'. 'Pal' must point to a 256 entry ip_rgb array which has been
   alocated by the caller.

ERRORS:

   ERR_BAD_TYPE

SEE ALSO:

   im_palette_set()   im_palette_copy()    im_palette_compress()

*/

int im_palette_get(ip_image *im, ip_rgb *pal)
{
    ip_log_routine("im_palette_get");

    if (im->type == IM_PALETTE)
	memcpy(pal, im->palette, 256*sizeof(ip_rgb));
    else
	ip_log_error(ERR_BAD_TYPE);

    ip_pop_routine();
    return ip_error;
}




/*

NAME:

   im_ubyte_to_palette() \- Convert UBYTE image to PALETTE in place

PROTOTYPE:

   #include <ip/ip.h>

   int  im_ubyte_to_palette( im, pal )
   ip_image *im;
   ip_rgb *pal;

DESCRIPTION:

   Convert UBYTE image 'im' into a PALETTE image without copying image.  If
   'pal' is non-NULL, then the 256 entry palette 'pal' is copied into the
   PALETTE image. If 'pal' is NULL, then the PALETTE is set to be
   grey-scale.

   Returns any error codes.

ERRORS:

   ERR_BAD_TYPE
   ERR_OUT_OF_MEM

SEE ALSO:

   im_palette_to_ubyte()    im_convert()

*/

int im_ubyte_to_palette(ip_image *im, ip_rgb *pal)
{
    ip_log_routine("im_ubyte_to_palette");

    if (im->type != IM_UBYTE) {
	ip_log_error(ERR_BAD_TYPE);
	ip_pop_routine();
	return ip_error;
    }

    if ((im->palette = ip_malloc(256*sizeof(ip_rgb))) == NULL) {
	return ip_error;
    }

    im->type = IM_PALETTE;

    if (pal) {
	im_palette_set(im, pal);
    }else{
	/* Set to a grey scale for default setup. */
	for (int i=0; i<256; i++) {
	    im->palette[i].r = (uint8_t)i;
	    im->palette[i].g = (uint8_t)i;
	    im->palette[i].b = (uint8_t)i;
	}
    }

    ip_pop_routine();
    return OK;
}



/*

NAME:

   im_palette_to_ubyte() \- Convert PALETTE image to UBYTE in place

PROTOTYPE:

   #include <ip/ip.h>

   int  im_palette_to_ubyte( im )
   ip_image *im;

DESCRIPTION:

   Convert the PALETTE image 'im' into a UBYTE image without copying whole
   image.  The palette associated with the image on entry is disposed of
   and cannot be recovered.

   This routine is limited.  It just dumps the palette and assumes that the
   "palette pen numbers" in the image data are now grey scale values.  This
   routine does not do a colour to grey-scale image conversion.

ERRORS:

   ERR_BAD_TYPE

SEE ALSO:

   im_ubyte_to_palette()    im_convert()

*/

int im_palette_to_ubyte(ip_image *im)
{
    ip_log_routine("im_palette_to_ubyte");

    if (im->type == IM_PALETTE) {

	ip_free(im->palette);
	im->palette = NULL;
	im->type = IM_UBYTE;

   }else{

	ip_log_error(ERR_BAD_TYPE);

    }

    ip_pop_routine();

    return ip_error;
}
