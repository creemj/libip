/*

   Module: ip/mean.c
   Author: M. J. Cree

   Copyright (C) 1997, 2002, 2013-2014 Michael J. Cree

   Mean filter images.  Currently only UBYTE images supported.

ENTRY POINTS:

   im_mean()

*/

#include <string.h>

#include <ip/ip.h>

#include "internal.h"
#include "utils.h"


/*

NAME:

   im_mean() \- Mean filter an image

PROTOTYPE:

   #include <ip/ip.h>

   int  im_mean( im, width, flag )
   ip_image *im;
   int width;
   int flag;

DESCRIPTION:

   Mean filter an image with 'width'*'width' kernel.  'Width' must be odd.
   By default, the border region where the mask goes off the edge of the
   image is left unprocessed (i.e. FLG_COPYBORDER behaviour) This can be
   modified with the use of 'flag', which may be:

\&      FLG_ZEROBORDER  Zero border region of image where mask doesn''t
                 cover properly.

\&      FLG_ZEROEXTEND Extend the mean function to the edge of the image
                 using as many pixels as are available to calculate
		 the mean.

\&-   This routine could equally be implemented by using a kernel with
   all values set to one and calling im_convolve() with the flag set
   to FLG_NORM.  However im_convolve() is of O('m'^2) operation (where
   'm' is the kernel size) and thus is slow.  Im_mean() is always
   faster than im_convolve(). In fact, im_mean() is of O('m')
   operation, thus is significantly faster for large values of 'm'.

LIMITATIONS:

\-   Only UBYTE type images supported.
\-   The kernel mask size must be odd.
\-   FLG_ZEROEXTEND not yet implemented - don''t use.

ERRORS:

   ERR_UNSUPPORTED_TYPE
   ERR_BAD_PARAMETER
   ERR_BAD_FLAG
   ERR_OUT_OF_MEM

*/

int im_mean(ip_image *im, int width, int flag)
{
    int hwidth;
    long arrsize;
    ip_image *tmp;

    /* Check the images type :      */
    /* this function supports only the following types : */
    /* UBYTE */

    ip_log_routine(__func__);

    if (im->type != IM_UBYTE) {
	ip_log_error(ERR_UNSUPPORTED_TYPE);
	goto exit;
    }

    if (NOT ip_flag_ok(flag, FLG_ZEROBORDER, FLG_ZEROEXTEND, LASTFLAG))
	return ip_error;

    if (NOT ip_parm_greatereq("kernel_size", width, 3))
	return ip_error;

    if (NOT ip_parm_odd("kernel_size", width))
	return ip_error;

    if ((tmp = ip_alloc_image(im->type, im->size)) == NULL)
	goto exit;

    hwidth = width / 2;		/* half-width of filter */
    arrsize = (long)width*width;

    /* for each pixel in image find mean over width*width mask */

    for (int j=hwidth; j<(im->size.y - hwidth); j++) {

	long mean;		/* Running mean value for row */

	/* Init running mean */

	mean = 0;
	for (int r=-hwidth; r<=hwidth; r++) {
	    for (int c=0; c<width; c++) {
		mean += IM_PIXEL(im, c, j+r, ub);
	    }
	}

	/* And store mean for left-most pixel. */

	IM_PIXEL(tmp, hwidth, j, b) = (uint8_t)(mean / arrsize);

	for (int i=hwidth+1; i<(im->size.x - hwidth); i++) {

	    int left, right;

	    /* Update running mean box */

	    left = i - hwidth - 1;
	    right = i + hwidth;
	    for (int r=-hwidth; r<=hwidth; r++) {
		mean -= IM_PIXEL(im, left, j+r, b);
		mean += IM_PIXEL(im, right, j+r, b);
	    }

	    /* Update mean value for this pixel */

	    IM_PIXEL(tmp, i, j, b) = (uint8_t)(mean / arrsize);

	}
    }

    if (flag == FLG_ZEROBORDER) {
	/* Clear edges of tmp */
	im_clear_border(tmp,hwidth);
    }

    if (flag == FLG_ZEROEXTEND) {
	ip_log_error(ERR_NOT_IMPLEMENTED);
    }

    if (flag == FLG_COPYBORDER) { /* same as flag == NO_FLAG */
	ip_coord2d org, size;

	org.x = org.y = hwidth;
	size.x = im->size.x - 2*hwidth;
	size.y = im->size.y - 2*hwidth;

	im_insert(im, org, tmp, org, size);
    }else{
	for (int j=0; j<im->size.y; j++)
	    memcpy(IM_ROWADR(im,j,v), IM_ROWADR(tmp,j,v), im->row_size);
    }
    ip_free_image(tmp);

 exit:
    ip_pop_routine();
    return ip_error;
}

