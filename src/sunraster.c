/*
   Module: ip/sunraster.c
   Author: M.J.Cree

   Copyright (C) 1996-1997, 2001, 2007, 2015 by Michael J. Cree.

   Routines for reading/saving Sun sunraster images.

*/


#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>

#define IPINTERNAL
#include <ip/ip.h>
#include <ip/file.h>
#include <ip/sunraster.h>

#include "internal.h"
#include "utils.h"

#include <rasterfile.h>

static int wr_sunraster_image(const char *fname, ip_image *im, int flag);
static ip_image *rd_sunraster_image(ip_sunraster_handle *srh, int first_row, int nrows);
static int seek_to_row(ip_sunraster_handle *srh, void * rowbuf, int skip, int rowsize);

#ifdef HAVE_LITTLE_ENDIAN
static void switch_int32(void *adr, int num);
#endif

/*

NAME:

   open_sunraster_image() \- Open Sunraster image file ready for reading

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/sunraster.h>

   ip_sunraster_handle *open_sunraster_image( filename )
   const char * filename;

DESCRIPTION:

   Open sunraster image corresponding to 'filename'. Returns sunraster handle
   or NULL if any errors occur. One can check image type and size in 
   the sunraster handle. This routine does not read in the actual image
   data, only the sunraster image header.  If 'filename' is NULL, reads
   from stdin.

   Example of reading in Sunraster image file:

\|   ip_sunraster_handle *srh;
\|   ip_image *im;
\|
\|   srh = open_sunraster_image("gertrude");
\|   im = read_sunraster_image(srh);
\|   close_sunraster_image(srh);


ERRORS:

   ERR_OUT_OF_MEMORY
   ERR_FILE_OPEN     - Error when opening file
   ERR_NOT_SUNRASTER   - File is not a Sunraster file
   ERR_SHORT_FILE    - File terminated unexpectantly

SEE ALSO:

   open_sunraster_image()   open_sunraster_imagex()
   read_sunraster_image()   read_sunraster_imagex()
   close_sunraster_image()
   write_sunraster_image()  write_sunraster_imagex()
   ip_read_image()

*/

ip_sunraster_handle *open_sunraster_image(const char * fname)
{
   unsigned char header[4];
   ip_sunraster_handle *srh;
   FILE *f;

   ip_log_routine("open_sunraster_image");

   srh = NULL;
   f = NULL;

   /* Open disk file and read in first part in anticipation 
      of sunraster image */

   if (fname) {
      if ((f = fopen(fname,"rb")) == NULL) {
	 ip_log_error(ERR_FILE_OPEN,fname);
	 goto exit_osri;
      }
   }else{
      f = stdin;
      fname = ip_msg_stdin;
   }

   if (fread(header,1,4,f) != 4) {
      ip_log_error(ERR_SHORT_FILE,fname);
      goto exit_osri;
   }

   srh = open_sunraster_imagex(f, fname, header, NO_FLAG);

 exit_osri:

   if ((srh == NULL) && f && (f != stdin))
      fclose(f);

   ip_pop_routine();
   return srh;
}




/*

NAME:

   open_sunraster_imagex() \- Open Sunraster image file ready for reading

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/sunraster.h>

   ip_sunraster_handle * open_sunraster_imagex( file, filename, header, flag )
   FILE *file;
   const char *filename;
   unsigned char header[4];
   int flag;

DESCRIPTION:

   Interpret header of a file that is believed to be a Sunraster image.
   This routine is primarily intended for internal ip library use,
   however it may be of use to design sophisticated image loaders
   that automatically detect image file type.

   It is assumed that 'file' of filename 'filename' has already been
   opened and the first four bytes have already been read.  Pass the
   first four bytes read through 'header'.  This routine reads more of
   the file and returns a sunraster handle describing the image file.
   The reason for doing it this way is so one can open a file that is
   a pipe (or otherwise non-seekable), determine the file type based
   on the first four bytes, then call the correct routine to load the
   image. The 'filename' is only for reference for printing any error
   messages.

   If 'file' is found not to be a Sunraster image (that is, the four header
   bytes do not match any known Sunraster image type) then this routine
   returns automatically (flagging the error ERR_NOT_SUNRASTER) without
   having touched the input file.  Thus the possibility of reading the file
   as some other image type (even if it is unseekable) is not lost.  Other
   errors possibly mean that some data have been read from the file and thus
   will be lost if the file is unseekable.  Returns NULL if any errors
   occur.  The file remains opened even if an error occurs.

ERRORS:

   ERR_OUT_OF_MEMORY
   ERR_NOT_SUNRASTER
   ERR_SHORT_FILE
   ERR_INVALID_SUNRASTER_TYPE

SEE ALSO:

   open_sunraster_image()
   read_sunraster_image()   read_sunraster_imagex()
   close_sunraster_image()
   write_sunraster_image()  write_sunraster_imagex()
   ip_read_image()

*/

ip_sunraster_handle *open_sunraster_imagex(FILE *f, const char *fname, 
			       unsigned char hdr[4], int flag)
{
   ip_sunraster_handle *srh;
   struct rasterfile srhead;
   int magic;

   ip_log_routine("open_sunraster_imagex");

   srh = NULL;

   /* Allocate sunraster handle. */

   if ((srh = ip_mallocx(sizeof(ip_sunraster_handle))) == NULL) {
      goto exit_osrix;
   }
   srh->f = f;
   srh->fname = fname;

   /* Read and interpret header */

   magic = (hdr[0] << 24) + (hdr[1] << 16) + (hdr[2] << 8) + hdr[3];

   if (magic == RAS_MAGIC) {

      if (fread(&srhead.ras_width,1,sizeof(struct rasterfile) - 4,srh->f)
					!= sizeof(struct rasterfile)-4) {
	 ip_log_error(ERR_SHORT_FILE,fname);
	 goto exit_osrix;
      }

#ifdef HAVE_LITTLE_ENDIAN
      switch_int32(&srhead,sizeof(struct rasterfile)/4);
#endif

#ifdef IP_DEBUG
      fprintf(stderr,"SR: type %d, width %d, height %d, depth %d, length %d\n",
	      srhead.ras_type, srhead.ras_width, srhead.ras_height,
	      srhead.ras_depth, srhead.ras_length);
      fprintf(stderr,"SR: maptype %d, maplength %d\n",
	      srhead.ras_maptype,srhead.ras_maplength); 
#endif

      srh->rtype = srhead.ras_type;
      srh->size.x = srhead.ras_width;
      srh->size.y = srhead.ras_height;
      srh->depth =  srhead.ras_depth;
      srh->imagelen = srhead.ras_length;
      srh->maptype = srhead.ras_maptype;
      srh->maplength = srhead.ras_maplength;
      srh->type = -1;

      if ((srh->rtype == RT_STANDARD) && (srh->depth == 1)) {
	 srh->type = IM_BINARY;
	 srh->rowlen =  ((srh->size.x+15)/16) * 2;
      }
      if ((srh->rtype == RT_STANDARD) && (srh->depth == 8)) {
	 srh->type =  (srh->maptype == RMT_EQUAL_RGB) ? IM_PALETTE : IM_BYTE;
	 srh->rowlen =  ((srh->size.x+1)/2) * 2;
      }
      if ((srh->rtype == RT_STANDARD || srh->rtype == RT_FORMAT_RGB) 
	  && (srh->depth == 24)) {
	 srh->type = IM_RGB;
	 srh->rowlen = ((srh->size.x*3+1)/2) * 2;
      }

      srh->imagelen = srh->imagelen ? srh->imagelen : srh->rowlen*srh->size.y;

      if (srh->type == -1) {
	 ip_log_error(ERR_INVALID_SUNRASTER_TYPE,fname);
	 goto exit_osrix;
      }
   }else{
      ip_log_error(ERR_NOT_SUNRASTER,fname);
   }

#ifdef IP_DEBUG
   fprintf(stderr,"SR: Chosen type = %s,  imagelen = %d   rowlen = %d\n",
	   ip_image_type_info[srh->type].name,srh->imagelen,srh->rowlen);
#endif

 exit_osrix:

   if (ip_error != OK && srh != NULL) {
      ip_free(srh);
      srh = NULL;
   }

   ip_pop_routine();
   return srh;
}




/*

NAME:

   write_sunraster_image() \- Write an image as a SunRaster file

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/sunraster.h>

   int write_sunraster_image( filename, im )
   char * filename;
   ip_image * im;

DESCRIPTION:

   Writes image 'im' as SunRaster file to 'filename'. Any error
   conditions returned.  If 'filename' is NULL, then writes to stdout.

LIMITATIONS:

   Only BINARY, BYTE, PALETTE and RGB image writes supported.

ERRORS:

   ERR_OUT_OF_MEM
   ERR_UNSUPPORTED_SUNRASTER_WRITE
   ERR_FILE_OPEN
   ERR_FILEIO
   ERR_FILE_CLOSE

SEE ALSO:

   write_sunraster_imagex()

*/

int write_sunraster_image(const char *fname, ip_image *im)
{
   ip_log_routine("write_sunraster_image");
   return wr_sunraster_image(fname, im, NO_FLAG);
}



/*

NAME:

   write_sunraster_imagex() \- Write an image as a SunRaster file

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/sunraster.h>

   int write_sunraster_imagex( filename, im, flag )
   char * filename;
   ip_image * im;
   int flag;

DESCRIPTION:

   Writes image 'im' as SunRaster file to 'filename'. Any error
   conditions returned.  If 'filename' is NULL, then writes to stdout.

LIMITATIONS:

   Only BINARY, BYTE, PALETTE and RGB image writes supported.

ERRORS:

   ERR_OUT_OF_MEM
   ERR_UNSUPPORTED_SUNRASTER_WRITE
   ERR_FILE_OPEN
   ERR_FILEIO
   ERR_FILE_CLOSE

SEE ALSO:

   write_sunraster_image()

*/

int write_sunraster_imagex(const char *fname, ip_image *im, int flag)
{
   ip_log_routine("write_sunraster_imagex");
   return wr_sunraster_image(fname, im, flag);
}



static int wr_sunraster_image(const char *fname, ip_image *im, int flag)
{
   struct rasterfile r;
   int rowlen, imrowlen;
   FILE *f;
   uint8_t *rowbuf;

   if (fname) {
      f = NULL;
   }else{
      f = stdout;
      fname = ip_msg_stdout;
   }
   rowbuf = NULL;

   /* Only support BINARY, BYTE, PALETTE and RGB image writes. */

   if (im->type != IM_BYTE && im->type != IM_BINARY
				&& (NOT image_type_clrim(im)) ) {
      ip_log_error(ERR_UNSUPPORTED_SUNRASTER_WRITE,fname);
      goto exit_wsri;
   }

   /* Initialise values in header */

   r.ras_magic = RAS_MAGIC;
   r.ras_width = im->size.x;
   r.ras_height = im->size.y;
   r.ras_type = RT_STANDARD;
   r.ras_maptype = RMT_NONE;
   r.ras_maplength = 0;

   /* Note: row length field calculated on basis that each scan line
      is padded out to a 16 bit word. */

   rowlen = 0;
   if (flag == FLG_BINARY || im->type == IM_BINARY) {
      r.ras_depth = 1;
      rowlen = ((im->size.x + 15) / 16) * 2;
   }else{
      switch (im->type) {
       case IM_BYTE:
	 r.ras_depth = 8;
	 rowlen = ((im->size.x + 1) / 2) * 2;
	 break;
       case IM_PALETTE:
	 r.ras_depth = 8;
	 r.ras_maptype = RMT_EQUAL_RGB;
	 r.ras_maplength = 256 * 3;
	 rowlen = ((im->size.x + 1) / 2) * 2;
	 break;
       case IM_RGB:
	 r.ras_type = RT_FORMAT_RGB;
	 r.ras_depth = 24;
	 rowlen = ((im->size.x*3 + 1) / 2) * 2;
	 break;
      default:
	 ip_log_error(ERR_NOT_IMPLEMENTED);
	 break;
      }
   }
   r.ras_length = rowlen * im->size.y;

   /* Open file. */

   errno = 0;
   if (f == NULL) {
      if ((f = fopen(fname,"wb")) == NULL) {
	 ip_log_error(ERR_FILE_OPEN,fname);
	 goto exit_wsri;
      }
   }

   /* Write header */

#ifdef HAVE_LITTLE_ENDIAN
   switch_int32(&r,sizeof(struct rasterfile)/4);
#endif

   if (fwrite(&r,1,sizeof(struct rasterfile),f) != sizeof(struct rasterfile)) {
      ip_log_error(ERR_FILEIO,fname);
      goto exit_wsri;
   }

   /* Write palette if PALETTE image. */

   if (im->type == IM_PALETTE) {
      if ((rowbuf = ip_mallocx(sizeof(uint8_t) * 256 * 3)) == NULL) {
	 goto exit_wsri;
      }

      for (int i=0; i<256; i++) {
	 rowbuf[i] = im->palette[i].r;
	 rowbuf[i+256] = im->palette[i].g;
	 rowbuf[i+512] = im->palette[i].b;
      }

      if (fwrite(rowbuf,sizeof(uint8_t),256*3,f) != (256*3)) {
	 ip_log_error(ERR_FILEIO,fname);
	 goto exit_wsri;
      }
      ip_free(rowbuf);
      rowbuf = NULL;
   }

   /* Write image data. */

   if ((rowbuf = ip_mallocx(rowlen * sizeof(uint8_t))) == NULL) {
      goto exit_wsri;
   }

   if (flag == FLG_BINARY || im->type == IM_BINARY) {
      uint8_t *impix, *rptr;	/* IM_BINARY writes */
      int mask;

      for (int j=0; j<im->size.y; j++) {
	 mask = 0x80;
	 impix = im_rowadr(im, j);
	 rptr = rowbuf;
	 *rptr = 0;
	 for (int i=0; i<im->size.x; i++) {
	    if (mask == 0x00) {
	       mask = 0x80;
	       *++rptr = 0;
	    }
	    *rptr |= (*impix++) ? (uint8_t)0 : (uint8_t)mask;
	    mask >>= 1;
	 }
	 if (fwrite(rowbuf, sizeof(uint8_t), rowlen, f) != rowlen) {
	    ip_log_error(ERR_FILEIO,fname);
	    goto exit_wsri;
	 }
      }
   }else{			/* IM_BYTE, IM_PALETTE, IM_RGB writes */
      imrowlen = ip_image_type_info[im->type].typesize * im->size.x;
      /* We have to use a buffer because rowlen larger than im->size.x */
      for (int j=0; j<im->size.y; j++) {
	 memcpy(rowbuf, im_rowadr(im, j), imrowlen);
	 if (fwrite(im_rowadr(im,j), sizeof(uint8_t), rowlen, f) != rowlen) {
	    ip_log_error(ERR_FILEIO,fname);
	    goto exit_wsri;
	 }
      }
   }

 exit_wsri:

   if (f && f != stdout) {
      if (fclose(f) != 0 && ip_error == OK)
	 ip_log_error(ERR_FILE_CLOSE,fname);
   }
   if (rowbuf) 
      ip_free(rowbuf);

   ip_pop_routine();

   return ip_error;
}





/*

NAME:

   read_sunraster_image() \- Read in image data from opened Sunraster file

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/sunraster.h>

   ip_image * read_sunraster_image( handle )
   ip_sunraster_handle *handle;

DESCRIPTION:

   Reads in sunraster image data corresponding to 'handle' from the
   disk. Returns the image data with image an opened image handle. One
   neads to call open_sunraster_image() to get a valid 'handle' before
   calling this routine.  Returns NULL if an error occurs.

   The reading of BINARY, BYTE, PALETTE and RGB images is supported.

ERRORS:

   ERR_OOR_PARAMETER
   ERR_OUT_OF_MEM
   ERR_NOT_IMPLEMENTED
   ERR_FILEIO_OR_TOO_SHORT

SEE ALSO:

   open_sunraster_image()   open_sunraster_imagex()
   read_sunraster_imagex()
   close_sunraster_image()
   write_sunraster_image()  write_sunraster_imagex()
   ip_read_image()

*/

ip_image *read_sunraster_image(ip_sunraster_handle *srh)
{
   ip_log_routine("read_sunraster_image");
   return rd_sunraster_image(srh, 0, srh->size.y);
}




/*

NAME:

   read_sunraster_imagex() \- Read in image data from opened Sunraster file

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/sunraster.h>

   ip_image * read_sunraster_imagex( handle, first_row, nrows )
   ip_sunraster_handle *handle;
   int first_row;
   int nrows;

DESCRIPTION:

   Reads in part sunraster image data corresponding to 'handle' from the
   disk. Returns the image data with image an opened image handle. One
   needs to call open_sunraster_image() to get a valid 'handle' before
   calling this routine.  Returns NULL if an error occurs.

   The first row of an image is numbered zero. Hence to read only the
   first row of an image specify 'first_row'=0 and 'nrows'=1.

   The image returned has 'nrows' rows in it.

ERRORS:

   ERR_OOR_PARAMETER
   ERR_OUT_OF_MEM
   ERR_NOT_IMPLEMENTED
   ERR_FILEIO_OR_TOO_SHORT

SEE ALSO:

   open_sunraster_image()   open_sunraster_imagex()
   read_sunraster_image()
   close_sunraster_image()
   write_sunraster_image()  write_sunraster_imagex()
   ip_read_image()

*/

ip_image *read_sunraster_imagex(ip_sunraster_handle *srh, int first_row, int nrows)
{
   ip_log_routine("read_sunraster_imagex");
   return rd_sunraster_image(srh, first_row, nrows);
}




static ip_image *rd_sunraster_image(ip_sunraster_handle *srh, int first_row, int nrows)
{
   ip_image *im;
   uint8_t *rowbuf;
   int imrowlen;

   /* Check first_row and nrows are within range */

   if (NOT ip_parm_inrange("first_row", first_row, 0, srh->size.y-1))
      return NULL;

   if (NOT ip_parm_inrange("number_rows", nrows, 1, srh->size.y-first_row))
      return NULL;


   rowbuf = NULL;
   im = NULL;

   imrowlen = srh->size.x * ip_image_type_info[srh->type].typesize;
   if (srh->rowlen < imrowlen) {
      /* Problem here! This means there are too few data per row
	 of image data.  We shit ourselves at this point... */
      ip_log_error(ERR_BAD_SUNRASTER_FILE,srh->fname);
      goto exit_rsri;
   }

   /* Allocate image  */

   if ((im = ip_alloc_image(srh->type, (ip_coord2d){srh->size.x, nrows})) == NULL)
      goto exit_rsri;

   /* Read in colour palette if present. */
   if (im->type == IM_PALETTE) {
      int mld3;
      int i;

      if ((rowbuf = ip_mallocx(srh->maplength)) == NULL) {
	 goto exit_rsri;
      }
      mld3 = srh->maplength/3;
      if (mld3 < 1 || (mld3 > 256)) {
	 ip_log_error(ERR_BAD_SUNRASTER_FILE,srh->fname);
	 goto exit_rsri;
      }

      fprintf(stderr,"SR: reading in palette of length %d [/3=%d]\n",
	      srh->maplength,mld3);

      if (fread(rowbuf,1,srh->maplength,srh->f) != srh->maplength) {
	 ip_log_error(ERR_FILEIO_OR_TOO_SHORT,srh->fname);
	 goto exit_rsri;
      }

      for (i=0; i<mld3; i++) {
	 im->palette[i].r = rowbuf[i];
	 im->palette[i].g = rowbuf[i+mld3];
	 im->palette[i].b = rowbuf[i+(2*mld3)];
      }
      for ( ; i<256; i++) {
	 im->palette[i].r = im->palette[i].g = im->palette[i].b = 0;
      }

      ip_free(rowbuf);
      rowbuf = NULL;
   }

   /* Allocate row buffer to read each row of image data in. */

   if ((rowbuf = ip_mallocx(srh->rowlen)) == NULL) {
      goto exit_rsri;
   }


   /* skip first rows ... */

   if (seek_to_row(srh, rowbuf, first_row, srh->rowlen) != OK) {
      goto exit_rsri;
   }

   /* ... and read nrows */

   switch (im->type) {
   case IM_BINARY:
      ip_log_error(ERR_NOT_IMPLEMENTED);

      break;
   case IM_BYTE:
   case IM_PALETTE:
      for (int j=0; j<im->size.y; j++) {
	 if (fread(rowbuf,1,srh->rowlen, srh->f) != srh->rowlen) {
	    ip_log_error(ERR_FILEIO_OR_TOO_SHORT,srh->fname);
	    goto exit_rsri;
	 }
	 memcpy(im_rowadr(im,j), rowbuf, imrowlen);
      }
      break;

   case IM_RGB:
      for (int j=0; j<im->size.y; j++) {
	 if (fread(rowbuf,1,srh->rowlen, srh->f) != srh->rowlen) {
	    ip_log_error(ERR_FILEIO_OR_TOO_SHORT,srh->fname);
	    goto exit_rsri;
	 }
	 if (srh->rtype == RT_FORMAT_RGB)
	    memcpy(im_rowadr(im,j), rowbuf, imrowlen);
	 else{
	    ip_rgb *iptr;
	    uint8_t *bptr;
	    iptr = im_rowadr(im,j);
	    bptr = rowbuf;
	    for (int i=0; i<im->size.x; i++) {
	       iptr->b = *bptr++;
	       iptr->g = *bptr++;
	       iptr->r = *bptr++;
	       iptr++;
	    }
	 }
      }
      break;

   default:
      ip_log_error(ERR_NOT_IMPLEMENTED);
      break;
   }

 exit_rsri:

   if (ip_error != OK && im) {
      ip_free_image(im);
      im = NULL;
   }
   if (rowbuf) ip_free(rowbuf);
   ip_pop_routine();
   return im;
}




/*

NAME:

   close_sunraster_image() \- Close previously opened Sunraster image file

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/sunraster.h>

   void close_sunraster_image( ip_sunraster_handle )
   ip_sunraster_handle *ip_sunraster_handle;

DESCRIPTION:

   Closes sunraster file associated with 'ip_sunraster_handle'. 'Ip_Sunraster_Handle' is not
   valid anymore after calling close_sunraster_image(). Safe to call
   with NULL 'ip_sunraster_handle' or on files attached to stdin.

ERRORS:

   ERR_FILE_CLOSE

SEE ALSO:

   open_sunraster_image()   open_sunraster_imagex()

*/

void close_sunraster_image(ip_sunraster_handle *srh)
{
   ip_log_routine("close_sunraster_image");

   if (srh) {
      if (srh->f && srh->f != stdin)
	 if (fclose(srh->f) != 0)
	    ip_log_error(ERR_FILE_CLOSE,srh->fname);
      ip_free(srh);
   }
   ip_pop_routine();
}



static int seek_to_row(ip_sunraster_handle *srh, void * rowbuf, int skip, int rowsize)
{
   int ownbuf;

   if (skip != 0) {		/* Only do seek if not first row of image */
      errno = 0;
      if (fseek(srh->f,(long)skip*rowsize, SEEK_CUR)) {

	 if (errno == ESPIPE) {
				/* If pipe, read through data consequtively */
	    ownbuf = FALSE;
	    if (rowbuf == NULL) {
	       if ((rowbuf = ip_mallocx(rowsize)) == NULL) {
		  goto exit_str;
	       }
	       ownbuf = TRUE;
	    }
	    for (int j=0; j<skip; j++) {
	       if (fread(rowbuf,1,rowsize,srh->f) != rowsize) {
		  ip_log_error(ERR_FILEIO_OR_TOO_SHORT,srh->fname);
		  break;
	       }
	    }
	    if (ownbuf) ip_free(rowbuf);

	 }else{
	    ip_log_error(ERR_FILEIO_OR_TOO_SHORT,srh->fname);
	 }

      }
   }

 exit_str:
   return ip_error;

}


#ifdef HAVE_LITTLE_ENDIAN
static void switch_int32(void *adr, int num)
{
   char *ptr;
   char ctmp;

   ptr = adr;
   for (int i=0; i<num; i++) {
      ctmp = ptr[0];
      ptr[0] = ptr[3];
      ptr[3] = ctmp;
      ctmp = ptr[1];
      ptr[1] = ptr[2];
      ptr[2] = ctmp;
      ptr += 4;
   }
}
#endif
