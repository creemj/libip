/*
  Module: ip/imtest.c
  Author: M.J.Cree

  For creating test images.

  Copyright (C) 1996-2013, 2016 Michael J. Cree

*/

#include <stdlib.h>
#include <limits.h>
#include <math.h>

#include <ip/ip.h>

#include "internal.h"
#include "utils.h"


/*

NAME:

   im_create() \- Create a test image

PROTOTYPE:

   #include <ip/ip.h>

   int im_create( im, origin, scale, offset, spread, type )
   ip_image *im;
   ip_coord2d origin;
   double scale;
   double offset;
   double spread;
   int type;

DESCRIPTION:

   Creates a test image. You pass an allocated image and im_create() fills
   it with the desired test image. The origin specifies where you want the
   origin of the resultant image to be (relative to the origin specified
   below for each test image). `Spread' controls the `spreading'' out of the
   test function from the origin. The test image is multiplied by `scale'
   and then has `offset' added to it. A precise definition is:

      %$r = cf((\mathbf{x}-\mathbf{o})/s) + t$%

   where %$f(\mathbf{x})$% is the test function and %$\mathbf{x}$% is a
   two-dimensional position vector. The origin is given by %$\mathbf{o}$%
   (also a vector), the spread by the scalar %$s$%, the scale by the scalar
   %$c$% and the offset by the scalar %$t$%.  The result put into pixel
   position %$\mathbf{x}$% is %$r$%.  Below, %$M$% refers to the minimum
   dimension of the image divided by 2.

   The test image type is one of:

\&   TI_CONST  	  Constant.
		%$f(\mathbf{x}) = 1$%
		[Pixel value equals 1]
		(This calls im_set() to do its job.)

\&   TI_HWEDGE 	  Horizontal wedge.
		%$f(\mathbf{x}) = \mathbf{x}\s{x}$%
		where %$\mathbf{x}\s{x}$% = %$x$%-component of %$\mathbf{x}$%.
		[Pixel value equals X coordinate]

\&   TI_VWEDGE 	  Vertical wedge.
		%$f(\mathbf{x}) = \mathbf{x}\s{y}$%
		where %$\mathbf{x}\s{y}$% = %$y$%-component of %$\mathbf{x}$%.
		[Pixel value equals Y coordinate]

\&   TI_CONE      Cone centred on origin
		%$f(\mathbf{x}) = 255 - x$% where %$x = |\mathbf{x}|.$%
		[Pixel value equals 255 - (distance from origin)]

\&   TI_GAUSSIAN   2D circularly symmetric Gaussian
		%$f(\mathbf{x}) = \exp(-x^2/2)$% where %$x = |\mathbf{x}|.$%

\&   TI_WDWCOSINE  2D cicularly symmetric cosine window.
		%$f(\mathbf{x}) = \cos(\pi x/3M)$% where %$x = |\mathbf{x}|.$%
		[Pixel value equals cosine(distance from origin)]

\&   TI_WDWBUTTW1 2D circularly symmetric first order Butterworth window.
		%$f(\mathbf{x} = 1 / (1 + (x/M)^2)$% where %$x = |\mathbf{x}|.$%

\&- The definitions of the windows are arranged so that the window goes to
   0.5 of maximum value (maximum value occurs at the origin) at the closest
   edge to the centre of the image, when %$s=1$%, %$t=0$%, and origin is at
   the centre of the image.

   Flags that can be ORed with the test image type are:

\|      FLG_CLIP
\|      FLG_TRANSPOSE

   The flag FLG_TRANSPOSE specifies to create an image with the four
   quadrants swapped around. This is useful for creating windows in Fourier
   space where the origin is (0,0) and the rightmost and bottommost quadrants
   represent negative frequencies.

   The flag FLG_CLIP is only relevant to integer type images, and specifies
   to clip the results to the image range.

LIMITATIONS:

   Only TI_GAUSSIAN, TI_WDWCOSINE and TI_WDWBUTTW1 supports use of
   FLG_TRANSPOSE so far...

ERRORS:

   ERR_UNSUPPORTED_TYPE	  (No complex, binary or colour image support)
   ERR_BAD_FLAG           (If don't recognise type parameter)
   ERR_PARM_BAD		  (If spread == 0.0)
   ERR_NOT_IMPLEMENTED    (If try to use TI_TRANSPOSE with certain types)

SEE ALSO:

   im_random()

*/


int im_create(ip_image *im,
	      ip_coord2d origin, double scale, double offset, double spread,
	      int flag)
{
    int Nxdiv2,Nydiv2;
    int type, transpose, clip;
    double scale_div_spread = scale / spread;
    double rcp_spread = 1.0 / spread;

    ip_log_routine(__func__);

    /*
     * We allow a PALETTE image through and just treat is as UBYTE ignoring
     * the palette.
     */

    if (NOT (image_type_integer(im) || image_type_real(im) || im->type == IM_PALETTE)) {
	ip_log_error(ERR_UNSUPPORTED_TYPE);
	goto exit;
    }

    type = flag & FLGPRT_TESTIM;
    transpose = flag & FLG_TRANSPOSE;
    clip = flag & FLG_CLIP;

    if (flag & FLGPRT_TESTIM_UNUSED) {
	ip_log_error(ERR_BAD_FLAG2, flag, flag);
	goto exit;
    }

    if (transpose && type != TI_WDWCOSINE && type != TI_WDWBUTTW1
					       && type != TI_GAUSSIAN) {
	ip_log_error(ERR_NOT_IMPLEMENTED);
	goto exit;
    }

    if (spread == 0.0 || spread == -0.0) {
	ip_log_error(ERR_PARM_BAD, "spread", spread);
	goto exit;
    }

    Nxdiv2 = im->size.x / 2;
    Nydiv2 = im->size.y / 2;

    switch (type) {
    case TI_CONST:		/* Constant image */
	im_set(im, scale+offset, NO_FLAG);
	break;

    case TI_HWEDGE:		/* Horizontal-wedge */
    case TI_VWEDGE:		/* Vertical-wedge */
    case TI_CONE:		/* Cone image */
	for (int j=0; j<im->size.y; j++) {
	    double ydiff = (double)(j - origin.y);
	    for (int i=0; i<im->size.x; i++) {
		double xdiff = (double)(i - origin.x);
		double val;
		if (type == TI_HWEDGE)
		    val = xdiff * scale_div_spread + offset;
		else if (type == TI_VWEDGE)
		    val = ydiff * scale_div_spread + offset;
		else /* type == TI_CONE */ {
		    val = sqrt(ydiff*ydiff + xdiff*xdiff) * rcp_spread;
		    val = scale*(255.0 - val) + offset;
		}
		switch(im->type) {
		case IM_BYTE:
		    if (clip) {
			if (val < INT8_MIN) val = INT8_MIN;
			if (val > INT8_MAX) val = INT8_MAX;
		    }
		    IM_PIXEL(im,i,j,b) = IROUND(val);
		    break;
		case IM_UBYTE:
		case IM_PALETTE:
		    if (clip) {
			if (val < 0) val = 0;
			if (val > UINT8_MAX) val = UINT8_MAX;
		    }
		    IM_PIXEL(im,i,j,ub) = IROUND(val);
		    break;
		case IM_SHORT:
		    if (clip) {
			if (val < INT16_MIN) val = INT16_MIN;
			if (val > INT16_MAX) val = INT16_MAX;
		    }
		    IM_PIXEL(im,i,j,s) = IROUND(val);
		    break;
		case IM_USHORT:
		    if (clip) {
			if (val < 0) val = 0;
			if (val > UINT16_MAX) val = UINT16_MAX;
		    }
		    IM_PIXEL(im,i,j,us) = IROUND(val);
		    break;
		case IM_LONG:
		    if (clip) {
			if (val < INT32_MIN) val = INT32_MIN;
			if (val > INT32_MAX) val = INT32_MAX;
		    }
		    IM_PIXEL(im,i,j,l) = (int32_t)LROUND(val);
		    break;
		case IM_ULONG:
		    if (clip) {
			if (val < 0) val = 0;
			if (val > UINT32_MAX) val = UINT32_MAX;
		    }
		    IM_PIXEL(im,i,j,ul) = (uint32_t)LROUND(val);
		    break;
		case IM_FLOAT:
		    IM_PIXEL(im,i,j,f) = (float)val;
		    break;
		case IM_DOUBLE:
		    IM_PIXEL(im,i,j,d) = val;
		    break;
		default:
		    /* Should not be possible */
		    if (i==0 && j==0)
			ip_log_error(ERR_NOT_IMPLEMENTED);
		    break;
		}
	    }
	}
	break;

    case TI_GAUSSIAN:		/* 2D Gaussian function */
    case TI_WDWCOSINE:		/* Cosine window */
    case TI_WDWBUTTW1:		/* 1st-order Butterworth window */

	switch (type) {
	case TI_GAUSSIAN:		/* 2D Gaussian function */
	    spread *= spread;
	    break;
	case TI_WDWCOSINE:		/* Cosine window */
	    spread *= (3.0 / M_PI);
	    /* Fall through */
	case TI_WDWBUTTW1:		/* 1st-order Butterworth window */
	    spread *= 0.5*(MIN(im->size.x,im->size.y));
	    break;
	}

	for (int j=0; j<im->size.y; j++) {
	    double val = 0;
	    double ydiff;
	    if (transpose) {
		ydiff = ((j+Nydiv2)%im->size.y) - origin.y;
	    }else{
		ydiff = j-origin.y;
	    }
	    ydiff *= ydiff;

	    for (int i=0; i<im->size.x; i++) {
		double xdiff;
		if (transpose) {
		    xdiff = ((i+Nxdiv2)%im->size.x) - origin.x;
		}else{
		    xdiff = i-origin.x;
		}

		switch (type) {
		case TI_WDWCOSINE:
		    val = sqrt(xdiff*xdiff + ydiff) * rcp_spread;
		    val = (val < M_PI/2) ? scale*cos(val) + offset : offset;
		    break;
		case TI_WDWBUTTW1:
		    val = scale / (1.0 + (xdiff*xdiff+ydiff)*(rcp_spread*rcp_spread))
			+ offset;
		    break;
		case TI_GAUSSIAN:
		    val = scale*exp(-(xdiff*xdiff+ydiff)*(0.5*rcp_spread)) + offset;
		    break;
		}

		switch(im->type) {
		case IM_BYTE:
		    if (clip) {
			if (val < INT8_MIN) val = INT8_MIN;
			if (val > INT8_MAX) val = INT8_MAX;
		    }
		    IM_PIXEL(im,i,j,b) = IROUND(val);
		    break;
		case IM_UBYTE:
		case IM_PALETTE:
		    if (clip) {
			if (val < 0) val = 0;
			if (val > UINT8_MAX) val = UINT8_MAX;
		    }
		    IM_PIXEL(im,i,j,ub) = IROUND(val);
		    break;
		case IM_SHORT:
		    if (clip) {
			if (val < INT16_MIN) val = INT16_MIN;
			if (val > INT16_MAX) val = INT16_MAX;
		    }
		    IM_PIXEL(im,i,j,s) = IROUND(val);
		    break;
		case IM_USHORT:
		    if (clip) {
			if (val < 0) val = 0;
			if (val > UINT16_MAX) val = UINT16_MAX;
		    }
		    IM_PIXEL(im,i,j,us) = IROUND(val);
		    break;
		case IM_LONG:
		    if (clip) {
			if (val < INT32_MIN) val = INT32_MIN;
			if (val > INT32_MAX) val = INT32_MAX;
		    }
		    IM_PIXEL(im,i,j,l) = (int32_t)LROUND(val);
		    break;
		case IM_ULONG:
		    if (clip) {
			if (val < 0) val = 0;
			if (val > INT32_MAX) val = INT32_MAX;
		    }
		    IM_PIXEL(im,i,j,ul) = (uint32_t)LROUND(val);
		    break;
		case IM_FLOAT:
		    IM_PIXEL(im,i,j,f) = (float)val;
		    break;
		case IM_DOUBLE:
		    IM_PIXEL(im,i,j,d) = val;
		    break;
		default:
		    if (i==0 && j==0)
			ip_log_error(ERR_NOT_IMPLEMENTED);
		    break;
		}
	    }
	}
	break;

    default:
	ip_log_error(ERR_BAD_PARAMETER);
	break;
    }

 exit:
    ip_pop_routine();
    return ip_error;
}
