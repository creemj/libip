/*
   Module: random.c
   Author: M. J. Cree

   (C) 1997-2013, 2015 Michael J. Cree

   Generation of random numbers and initialisation of an image
   with random numbers.

ENTRY POINTS:

   im_random()
   ip_rand_uniform()    ip_rand_normal()

*/

/* To get random() definition */
#define _XOPEN_SOURCE 500
#include <stdlib.h>
#include <math.h>

#include <ip/ip.h>
#include <ip/random.h>
#include "internal.h"


/*

NAME:

   ip_rand_uniform() \- Generate random number, uniform deviate.

PROTOTYPE:

   #include <ip/random.h>

   double  ip_rand_uniform( void )

DESCRIPTION:

   Return a uniform deviate, that is, a uniformly distributed random number
   in the range 0 (inclusive) to 1.0 (exclusive).

   Uses the C library random() function which can be seeded with srandom().

   Defined as static inline in random.h.

SEE ALSO:

   ip_rand_normal()	random(3)	srandom(3)

*/

#if 0
/* Defined as static inline in random.h */
double ip_rand_uniform(void)
{
    return (double)random() / RAND_MAXP1;
}
#endif


/*

NAME:

   ip_rand_normal() \- Generate random number, normal (Gaussian) distribution

PROTOTYPE:

   #include <ip/random.h>

   double  ip_rand_normal( void )

DESCRIPTION:

   Return a normal (Gaussian) distributed (mean = 0, standard deviation = 1)
   random number.

   Uses the standard C library random() function which can be seeded with
   srandom().

SEE ALSO:

   ip_rand_uniform()	random(3)	srandom(3)

*/

double ip_rand_normal(void)
{
    static int iset = 0;
    static double gset;
    double fac, r2, v1, v2;

    if  (iset == 0) {
	do {
	    v1 = 2.0 * ip_rand_uniform() - 1.0;
	    v2 = 2.0 * ip_rand_uniform() - 1.0;
	    r2 = v1*v1 + v2*v2;
	} while (r2 >= 1.0);
	fac = sqrt(-2.0 * log(r2) / r2);
	gset = v1*fac;
	iset = 1;
	return v2*fac;
    } else {
	iset = 0;
	return gset;
    }
}



/* Macros used in im_random() */

#define PROCESS_IMAGE(im, f, s, o, t) {				\
	int rowlen = ip_type_size(im->type) / ip_type_size(IM_ ## t);	\
	rowlen *= im->size.x;					\
	for (int j=0; j<im->size.y; j++) {			\
	    t ## _type *ptr;					\
	    ptr = IM_ROWADR(im, j, v);				\
	    for (int i=0; i<rowlen; i++) {			\
		*ptr++ = (t ## _type)(s*f()+o);			\
	    }							\
	}							\
    }

#define PROCESS_IMAGE_INT(im, f, s, o, t) {			\
	int rowlen = ip_type_size(im->type) / ip_type_size(IM_ ## t);	\
	rowlen *= im->size.x;					\
	for (int j=0; j<im->size.y; j++) {			\
	    t ## _type *ptr;					\
	    ptr = IM_ROWADR(im, j, v);				\
	    for (int i=0; i<rowlen; i++) {			\
		*ptr++ = (t ## _type)(int64_t)LROUND(s*f()+o);	\
	    }							\
	}							\
    }

#define PROCESS_IMAGE_BINARY(im, f, p) {	\
	for (int j=0; j<im->size.y; j++) {	\
	    uint8_t *ptr = IM_ROWADR(im, j, v);	\
	    for (int i=0; i<im->size.x; i++) {	\
		*ptr++ = (f() >= p) ? 255 : 0;	\
	    }					\
	}					\
    }


/*

NAME:

   im_random()

PROTOTYPE:

   #include <ip/ip.h>

   int  im_random( im, parm1, parm2, flag )
   ip_image *im;
   double parm1;
   double parm2;
   int flag;

DESCRIPTION:

   Initialise an image with random pixel values.  'flag' should be one of
   FLG_RANDOM_UNIFORM (to get uniformaly distributed random numbers) or
   FLG_RANDOM_NORMAL (to get normally distributed random numbers).

   The function of the two parameters 'parm1' and 'parm2' depends on the
   random distribution type.  For FLG_RANDOM_UNIFORM 'parm1' is the lowest
   possible random number and 'parm2' is the highest possible random number
   and pixel values are uniformly distirbuted between these two values.  For
   FLG_RANDOM_NORMAL 'parm1' is the mean and 'parm2' is the standard
   deviation.

   BINARY images are treated specially and only 'parm1' is used and it sets
   the fraction of points that are TRUE.  Set to 0.5 to get 50% FALSE and
   50% TRUE pixels.  The flag is ignored.

   Currently FLG_RANDOM_NORMAL may lead to pixel values that wrap outside
   the pixel type value limits.  No check is made for this.

ERRORS:

   ERR_BAD_FLAG2
   ERR_PARAM_BAD_VALUE

SEE ALSO:

   ip_random_uniform()     ip_random_normal()	random(3)	srandom(3)
   im_create()

*/

int im_random(ip_image *im, double parm1, double parm2, int flag)
{
    double scale = 0.0, offset = 0.0;
    double (*random_number)(void);

    ip_log_routine(__func__);

    if (NOT ip_flag_ok(flag, FLG_RANDOM_UNIFORM, FLG_RANDOM_NORMAL, LASTFLAG))
	return ip_error;

    if (im->type == IM_BINARY) {

	/* BINARY images treated specially */
	if (NOT ip_parm_inrange("parm1", parm1, 0.0, 1.0))
	    return ip_error;
	random_number = ip_rand_uniform;

    }else{

	if (flag == FLG_RANDOM_UNIFORM || flag == NO_FLAG) {
	    random_number = ip_rand_uniform;
	    if (NOT ip_value_in_image("parm1", parm1, im))
		return ip_error;
	    if (NOT ip_value_in_image("parm2", parm2, im))
		return ip_error;
	    if (type_integer(im->type) || type_clrim(im->type)) {
		parm2 = round(parm2) + 0.5;
		parm1 = round(parm1) - 0.499999999999;
	    }
	    scale = parm2 - parm1;
	    offset = parm1;
	}

	if (flag == FLG_RANDOM_NORMAL) {
	    random_number = ip_rand_normal;
	    scale = parm2;
	    offset = parm1;
	}
    }

    switch (im->type) {
    case IM_UBYTE:
    case IM_PALETTE:
    case IM_RGB:
    case IM_RGBA:
	PROCESS_IMAGE_INT(im, random_number, scale, offset, UBYTE);
	break;
    case IM_BYTE:
	PROCESS_IMAGE_INT(im, random_number, scale, offset, BYTE);
	break;
    case IM_USHORT:
    case IM_RGB16:
	PROCESS_IMAGE_INT(im, random_number, scale, offset, USHORT);
	break;
    case IM_SHORT:
	PROCESS_IMAGE_INT(im, random_number, scale, offset, SHORT);
	break;
    case IM_ULONG:
	PROCESS_IMAGE_INT(im, random_number, scale, offset, ULONG);
	break;
    case IM_LONG:
	PROCESS_IMAGE_INT(im, random_number, scale, offset, LONG);
	break;
    case IM_FLOAT:
    case IM_COMPLEX:
	PROCESS_IMAGE(im, random_number, scale, offset, FLOAT);
	break;
    case IM_DOUBLE:
    case IM_DCOMPLEX:
	PROCESS_IMAGE(im, random_number, scale, offset, DOUBLE);
	break;
    case IM_BINARY:
	PROCESS_IMAGE_BINARY(im, random_number, parm1);
	break;
    default:
	break;
    }

    ip_pop_routine();
    return OK;
}
