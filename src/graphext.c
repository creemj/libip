/*
   Module: ip/graphext.c
   Author: M.J.Cree

   Copyright (C) 1996-1997, 2016 Michael J. Cree.

   More graphic drawing routines that are less likely to be used.

*/


#include <stdlib.h>
#include <math.h>

#include <ip/ip.h>

#include "internal.h"
#include "utils.h"

#define X30DEG (30.0*M_PI/180)


static ip_coord2d best_endpoint(ip_image *im, ip_coord2d pt, double angle, int size);


/*

NAME:

   im_drawarrow() \- Draw an arrow into an image

PROTOTYPE:

   #include <ip/ip.h>

   int  im_drawarrow( im, val, pt, angle, size )
   ip_image *im;
   double val;
   ip_coord2d pt;
   double angle;
   int size;

DESCRIPTION:

   Draw an arrow into the image 'im'.  The arrow is drawn with value 'val',
   with its arrow point located at 'pt'.  The angle specifies the direction
   the arrow is pointing (0 radians is along the 'x'-axis and higher angles
   rotate clockwise since the 'y'-axis points downwards).  'Size' specifies
   the length of the arrow in pixels.

   The arrow point 'pt' must be within the image bounds, however if any
   other part of the arrow extends outside the image bounds no errors are
   flagged and the arrow is clipped at the image boundary.

ERRORS:

   ERR_BAD_COORD
   ERR_BAD_VALUE
   ERR_OOR_PARAMETER
   ERR_TOOSMALL_PARAMETER
   ERR_UNSUPPORTED_TYPE

SEE ALSO:

   im_drawpoint()   im_drawline()     im_drawbox()
   im_drawcircle()  im_drawellipse()

*/

int im_drawarrow(ip_image *im, double val, ip_coord2d pt, double angle, int size)
{
    ip_coord2d tail;

    ip_log_routine(__func__);
   
    if (NOT ip_point_in_image("arrow_point", pt, im))
	return ip_error;
    if (NOT ip_value_in_image("pixel_val", val, im))
	return ip_error;

    if (im->type == IM_COMPLEX || im->type == IM_RGB || im->type == IM_RGB16) {
	ip_log_error(ERR_UNSUPPORTED_TYPE);
	ip_pop_routine();
	return ip_error;
    }

    if (NOT ip_parm_inrange("angle", angle, 0.0, 2*M_PI)) {
	return ip_error;
    }

    if (NOT ip_parm_greatereq("arrow_size", size, 1)) {
	return ip_error;
    }
 
    tail = best_endpoint(im, pt, angle, size);
    im_drawline(im, val, pt, tail);

    size /= 5;

    if (size > 0) {
	tail = best_endpoint(im, pt, angle + X30DEG, size);
	im_drawline(im, val, pt, tail);

	tail = best_endpoint(im, pt, angle - X30DEG, size);
	im_drawline(im, val, pt, tail);
    }

    ip_pop_routine();
    return ip_error;
}




static ip_coord2d best_endpoint(ip_image *im, ip_coord2d pt, double angle, int size)
{
    ip_coord2d tail;
    int newsize, tmp;

    tail.x = (pt.x - (int)ROUND(size*cos(angle)));
    tail.y = (pt.y - (int)ROUND(size*sin(angle)));

    newsize = size;
    if (tail.x < 0) {
	newsize = pt.x / fabs(cos(angle));
    }
   
    if (tail.y < 0) {
	tmp = pt.y / fabs(sin(angle));
	if (tmp < newsize) newsize = tmp;
    }

    if (tail.x >= im->size.x) {
	tmp = (im->size.x-1-pt.x) / fabs(cos(angle));
	if (tmp < newsize) newsize = tmp;
    }

    if (tail.y >= im->size.y) {
	tmp = (im->size.y-1-pt.y) / fabs(sin(angle));
	if (tmp < newsize) newsize = tmp;
    }

    if (newsize != size) {
	tail.x = (pt.x - (int)ROUND(newsize*cos(angle)));
	tail.y = (pt.y - (int)ROUND(newsize*sin(angle)));
    }

    {  /* Only here for debugging purposes */
	int wrong;
	ip_coord2d wrtail;

	wrong = FALSE;
	wrtail = tail;
	if (tail.x < 0) {
	    wrong = TRUE;
	    tail.x = 0;
	}
	if (tail.y < 0) {
	    wrong = TRUE;
	    tail.y = 0;
	}
	if (tail.x >= im->size.x) {
	    wrong = TRUE;
	    tail.x = im->size.x-1;
	}
	if (tail.y >= im->size.y) {
	    wrong = TRUE;
	    tail.y = im->size.y-1;
	}
	if (wrong) {
	    ip_log_error(ERR_ALGORITHM_FAULT,
			"Tail is (%d,%d) with pt (%d,%d),"
			" angle %g, size %d, newsize %d\n",
			wrtail.x, wrtail.y, pt.x, pt.y, angle, size, newsize);
	}
    }

    return tail;
}

