/*

   Module: ip/hist.c
   Author: M.J.Cree

   Histogram calculations on images.

   Copyright (C) 1995-1997, 2000, 2002, 2013, 2015, 2017 Michael J. Cree

*/


#include <stdlib.h>
#include <string.h>
#include <float.h>
#include <math.h>

#include <ip/ip.h>
#include <ip/hist.h>
#include <ip/stats.h>

#include "internal.h"
#include "utils.h"

typedef void (*improc_function_Ih)(ip_hist *h, ip_image *s);

/*
 * A histogram part is a complete histogram in itself and is a sub-object to
 * the histogram object.  It is useful for constructing HIST_COMPONENT
 * (etc.) type histograms which have a histogram part per component of the
 * image data type.  (E.g. a histogram of the real part and a histogram of
 * the imaginary part of a complex image contained in one histogram object.)
 */

/*
 * ip_init_hist_part()
 *
 * Allocate memory for the bin array for a histogram part object.  Set bin
 * counts to zero. Returns true if successful.
 */

static int ip_init_hist_part(ip_hist_part *hp, int numbins, double low, double high, int layered)
{
    hp->low = low;
    hp->high = high;
    hp->width = (high - low) / (double)numbins;
    hp->under = hp->over = hp->nan = 0;
    hp->offset = 0;

    if (layered) {
	int totalbins;

	hp->numbins = high;
	hp->numlayers = 2;
	hp->layer_numbins = numbins;
	totalbins = hp->numbins + hp->layer_numbins;
	if ((hp->bins = ip_malloc(totalbins * sizeof(int))) == NULL)
	    return 0;
	memset(hp->bins, 0, totalbins*sizeof(int));
	hp->layer0 = hp->bins + hp->numbins;
    }else{
	hp->numbins = numbins;
	hp->layer_numbins = 0;
	if ((hp->bins = ip_malloc(numbins*sizeof(int))) == NULL)
	    return 0;

	memset(hp->bins, 0, numbins*sizeof(int));
    }

    return 1;
}



/*

NAME:

   ip_alloc_hist() \- Allocate a histogram object

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/hist.h>

   ip_hist *  ip_alloc_hist( htype, numparts, partspec )
   int htype;
   int numparts;
   struct ip_hist_part_spec *partspec;

DESCRIPTION:

   Allocate a histogram object ready for use. The histrogram type is
   specified by 'htype' and should be one of:

   HIST_COMPONENTS: The histogram is intended for use on an image and will
   have a histogram part for each component of the image data type (e.g. a
   real and imaginary histogram for complex data.)  The number of components
   (thus histogram parts) is set by 'numparts' and the number of bins for
   each part is given by the array (with 'numparts' entries) given by
   'numbinsp' (thus one can have a different number of bins for each
   histogram part), or if 'numbinsp' is NULL then every histogram part is
   allocated the same number of bins being 'numbins' bins.

   HIST_INTENSITY: The histogram has one histogram part ('numparts' must be
   set to one) with 'numbins' bins, unless 'numbinsp' is non-null then the
   integer value pointed to by 'numbinsp' is the number of bins. It is
   intended to be used on scalar images or to be a histogram of the
   magnitude/intensity of a complex/RGB image.

   A HIST_INTENSITY histogram on a scalar image is the same thing as a
   HIST_COMPONENTS histogram on a scalar image.  The im_hist() function
   prefers to make a HIST_COMPONENTS histogram.

   HIST_MAGPHASE: The histogram is intended for use on a complex image and
   has two histgram parts, the first being a histogram of the magnitude and
   the second a histogram of the phase.  Argument 'numparts' must be two.
   The number of bins is indicated by the two-element array given by
   'numbinsp' if it is non-null, otherwise both histogram parts have the
   same number of bins given by 'numbins' if 'numbinsp' is NULL.

   HIST_LAYERED: The histogram is intended for use on BYTE/UBYTE or
   SHORT/USHORT data and has two histgram parts, the first being a course
   histogram (large bins) and the second being fine having one bin per
   possible pixel value.  Set 'numparts' to one.  Set the top value to the
   maximum value (either 256 for 8-bit data or 65536 for 16-bit data) and
   the numbins to either 16 (for sixteen bins per layer) or 256 (for 256
   bins per layer).  Special bin update and reading functions must be used
   to update and read the histogram otherwise the layered histogram parts
   could become inconsistent with each other.

   A histogram part keeps track of the following counts: under, over, nan,
   and the histogram bin array with the number of bins as specified by
   'numbins' or 'numbinsp'.  The number of pixels that are less than low are
   counted in field under, the number of pixels that are equal to or greater
   than value high are counted in field over, the number of NaNs (if
   floating-point data) are counted in field nan, and all over pixel values
   greater than or equal to low and less than high are counted in one of the
   bins according to their pixel value.

   For creating histograms from an image use im_hist() or im_hist_a(), not
   this function.  A histogram is freed with ip_free_hist().

ERRORS:

   ERR_OUT_OF_MEM
   ERR_BAD_PARAMETER

SEE ALSO:

   ip_free_hist()
   im_hist()
   ip_hist_peak()                ip_hist_smooth()
   ip_hist_bin_left()            ip_hist_bin_centre()
   ip_hist_bin_width()           ip_hist_bin_count()

*/


ip_hist * ip_alloc_hist(ip_hist_type htype, int numparts, struct ip_hist_parts_spec *hps)
{
    int error;
    ip_hist *h;

    ip_log_routine(__func__);

    if (NOT ip_parm_inrange("htype", htype, 0, IP_HIST_MAX_TYPE))
	return NULL;

    /* Check number of parts, i.e., number of sub-histograms */
    if (htype == HIST_INTENSITY || htype == HIST_LAYERED) {
	if (NOT ip_parm_inrange("numparts", numparts, 1, 1))
	    return NULL;
    }else if (htype == HIST_MAGPHASE) {
	if (NOT ip_parm_inrange("numparts", numparts, 2, 2))
	    return NULL;
    }else{
	if (NOT ip_parm_greatereq("numparts", numparts, 1))
	    return NULL;
    }

    /* Histogram array sizes are defined by hist_parts_spec */
    for (int j=0; j<numparts; j++) {
	if (NOT ip_parm_greatereq("hps[.].numbins", hps[j].numbins, 2))
	    return NULL;
	if (NOT ip_parm_greater("hps[.].high", hps[j].high, hps[j].low))
	    return NULL;
    }

    if (htype == HIST_LAYERED) {
	/* Only a few restricted types are implemented so check inputs */
	if ((hps[0].high != 256 && hps[0].high != 65536)
	    || ((hps[0].high == 256 && hps[0].numbins != 16)
		|| (hps[0].high == 65536 && hps[0].numbins != 256))) {
	    ip_log_error(ERR_BAD_PARAMETER);
	    ip_pop_routine();
	    return NULL;
	}
    }

    /* Good to go; allocate main histogram object */
    if ((h = ip_malloc(sizeof(ip_hist))) == NULL)
	return NULL;

    memset(h, 0, sizeof(*h));

    h->imtype = IM_UNDEFINED;
    h->imsize = (ip_coord2d){0, 0};
    h->htype = htype;
    h->num_parts = numparts;

    if ((h->hp = ip_malloc(numparts * sizeof(ip_hist_part))) == NULL) {
	ip_free(h);
	return NULL;
    }

    /* Flag that bin arrays have not been allocated ... */
    for (int j=0; j<numparts; j++)
	h->hp[j].bins = NULL;

    /* ... and then allocate them. */
    if (htype == HIST_LAYERED) {
	/* Allocate first layer in layered histogram */
	if (NOT ip_init_hist_part(&h->hp[0], hps[0].numbins, hps[0].low, hps[0].high, 1))
	    goto exit_ah;
    }else{
	/* For other types allocate all layers */
	for (int j=0; j<numparts; j++) {
	    if (NOT ip_init_hist_part(&h->hp[j], hps[j].numbins, hps[j].low, hps[j].high, 0))
		goto exit_ah;
	}
    }

    ip_pop_routine();
    return h;

 exit_ah:
    error = ip_error;
    if (h->hp) {
	for (int j=0; j<h->num_parts; j++) {
	    if (h->hp[j].bins)
		ip_free(h->hp[j].bins);
	}
	ip_free(h->hp);
    }
    ip_free(h);
    ip_pop_routine();
    ip_error = error;
    return NULL;
}


/*

NAME:

   ip_free_hist() \- Free previously allocated histogram

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/hist.h>

   void  ip_free_hist( h )
   ip_hist *h;

DESCRIPTION:

   Free a histogram allocated by im_hist() or ip_alloc_hist().

SEE ALSO:

   im_hist()           ip_alloc_hist()

*/


void ip_free_hist(ip_hist *h)
{
    ip_log_routine(__func__);
    if (h) {
	if (h->hp) {
	    for (int j=0; j<h->num_parts; j++) {
		if (h->hp[j].bins) {
		    ip_free(h->hp[j].bins + h->hp[j].offset);
		}
	    }
	    ip_free(h->hp);
	}
	ip_free(h);
    }
    ip_pop_routine();
}


/*

NAME:

   ip_hist_reset() \- Reset histogram back to zero counts

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/hist.h>

   void ip_hist_reset( hist )
   ip_hist *hist;

DESCRIPTION:

    Reset the histogram counts of the histgram `hist' back to zero.

SEE ALSO:

    ip_alloc_hist()

*/

void ip_hist_reset(ip_hist *h)
{
    if (h->htype == HIST_LAYERED) {
	int totalbins = h->hp[0].numbins + h->hp[0].layer_numbins;
	if (h->hp[0].bins)
	    memset(h->hp[0].bins + h->hp[0].offset, 0, totalbins*sizeof(int));
    }else{
	for (int j=0; j<h->num_parts; j++) {
	    if (h->hp[j].bins)
		memset(h->hp[j].bins + h->hp[j].offset, 0, sizeof(int)*h->hp[j].numbins);
	    h->hp[j].under = 0;
	    h->hp[j].over = 0;
	    h->hp[j].nan = 0;
	}
    }
}



/*

NAME:

   im_hist() \- Generate histogram of an image

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/hist.h>

   ip_hist * im_hist( im, roi, low, high, numbins )
   ip_image *im;
   ip_roi *roi;

DESCRIPTION:

   Calculate HIST_COMPONENTS histogram of image 'im' within the ROI 'roi'.
   To calculate histogram over whole image pass 'roi' to NULL.  Histogram
   uses 'numbins' bins ranging from 'low' to 'high'.  For a UBYTE image to
   get a histogram with a bin for each pixel value do:

\|     hist *h;
\|     h = im_hist(im, NULL, 0, 255, 256);
\|        ...
\|     ip_free_hist(h);

   The value 'low' defines the left-edge (inclusive) of the lowest bin.

   For integer data 'high' defines the largest integer value to include in
   the histogram, that is, any pixels of value 'high'+1 or larger are
   counted in the "over" bin.  Any integer pixel values less than 'low' are
   counted in the "under" bin.

   For floating-point data (both real and complex) 'high' defines the
   right-edge (inclusive) of the highest bin, thus any pixel data that is of
   value nextafter('high') or larger goes to the "over" bin.  Any pixel data
   that is smaller than 'low' goes to the "under" bin. (In practice, the
   specification of the meaning of 'high' may not precisely occur if the
   width of a bin is not exactly representable within double precision,
   etc.)

   The above specification for 'low' and 'high' is chosen so that if 'low'
   is set from the minimum pixel value in the image, and 'high' is set from
   the maximum value in the image (such as provided by im_extrema()) then
   you get a sensible histogram.  Over ways of specifying 'low' and 'high'
   are provided for by im_hist_a().

   Returns result in newly allocated ip_hist object, or NULL if an error
   occurs.  The histogram is freed with the ip_free_hist() function.

   If you want some other histogram type (HIST_INTENSITY, HIST_MAGPHASE or
   HIST_LAYERED) on an image call im_hist_a() which has a more flexible,
   thus more complicated, interface.

ERRORS:

   ERR_BAD_TYPE
   ERR_BAD_ROI
   ERR_OUT_OF_MEM

SEE ALSO:

   im_hist_a()			 ip_free_hist()
   ip_hist_peak()                ip_hist_smooth()
   ip_hist_bin_left()            ip_hist_bin_centre()
   ip_hist_bin_width()           ip_hist_bin_count()

*/

/*
 * Use 32/32 fixed-point arithmetic to calculate bin on integer types
 *
 * The below macro works for all integer types except LONG because the
 * subtraction of ilow from val can be out of range of int32_t
 * (i.e. overflow which is underfined behaviour) when used for LONG. Hence
 * the specific macro for LONG.
 *
 * The bin calculation cannot overflow the histogram bins array.  Let M be
 * the number of histogram bins, N = high-low, then scaler is given by
 * scaler = floor(2^32M/N) <= 2^32(1+e)M/N, where e < 2^-53 is the relative
 * error in representing the division M/N by the nearest floating point
 * number. Thus the calculation of the bin b for the maximum integer pixel
 * value resulting from (val - ilow) = (N-1) is,
 *    b =  trunc(scaler * (N-1) / 2^32),
 *      <= 2^32(1+e)M(N-1)/(2^32N),
 *      <= M(1+e)(N-1)/N.
 * But (N-1)/N <= 1 - 2^-32 (for all values of all integer types supported)
 * and (1+e)(1-2^-32) = 1 - 2^-32 + e - e2^-32 which is strictly less than
 * one since e <= 2^-53, therefore the bin b calculated is strictly smaller
 * than M and since a truncation to int is the final act b is at most
 * M-1. (Of course this is assuming the function calling the IMROW_LIST()
 * macro sets up low, high and scaler correctly.)
 */

static inline uint64_t int_scaler(ip_hist_part *hp, int idx)
{
    double rcp_width = (double)hp[idx].numbins / (hp[idx].high - hp[idx].low);
    return (uint64_t)floor(pow(2, 32) * rcp_width);
}

static inline double real_scaler(ip_hist_part *hp, int idx)
{
    return (double)hp[idx].numbins / (hp[idx].high - hp[idx].low);;
}

static inline uint64_t rgb_int_scaler(ip_hist_part *hp, int idx)
{
    double rcp_width = (double)hp[idx].numbins / (1000.0*(hp[idx].high - hp[idx].low));
    return (uint64_t)floor(pow(2, 32) * rcp_width);
}


static inline void hp_int_count(ip_hist_part *hp, int val, int low, int high, uint64_t scaler)
{
    if (val < low)
	ip_hp_under_inc(hp);
    else if (val >= high)
	ip_hp_over_inc(hp);
    else{
	int bin = (int)(((uint64_t)(val - low) * scaler) >> 32);
	ip_hp_bin_inc(hp, bin);
    }
}


static inline void hp_real_count(ip_hist_part *hp, double val, double rcp_width)
{
    if (val < hp->low)
	ip_hp_under_inc(hp);
    else if (val >= hp->high)
	ip_hp_over_inc(hp);
    else if (isnan(val))
	ip_hp_nan_inc(hp);
    else{
	int bin = (val - hp->low)*rcp_width;
	if (bin >= hp->numbins) bin = hp->numbins-1;
	ip_hp_bin_inc(hp, bin);
    }
}



#define IMROW_HIST(type, ptype, scalar)			\
    {							\
	ptype ilow = low;				\
	ptype ihigh = high;				\
	type *ip = IM_ROWADR(im, j, v);			\
	ip += topl.x;					\
	for (int i=topl.x; i<=botr.x; i++) {		\
	    ptype val = *ip++;				\
	    if (val < ilow)				\
		ip_hist_under_inc(hg, 0);		\
	    else if (val >= ihigh)			\
		ip_hist_over_inc(hg, 0);		\
	    else{					\
		int bin = (int)(((uint64_t)(val - ilow) * scaler) >> 32); \
		ip_hist_bin_inc(hg, 0, bin);		\
	    }						\
	}						\
    }


#define IMROW_HIST_LONG(unused1, unused2, scaler)	\
    {							\
	int32_t ilow = low;				\
	int32_t ihigh = high;				\
	int32_t *ip = IM_ROWADR(im, j, v);		\
	ip += topl.x;					\
	for (int i=topl.x; i<=botr.x; i++) {		\
	    int32_t val = *ip++;			\
	    if (val < ilow)				\
		ip_hist_under_inc(hg, 0);		\
	    else if (val >= ihigh)			\
		ip_hist_over_inc(hg, 0);		\
	    else{					\
		int bin = (int)(((uint64_t)((uint32_t)val - (uint32_t)ilow) * scaler) >> 32); \
		ip_hist_bin_inc(hg, 0, bin);		\
	    }						\
	}						\
    }


#define IMROW_REAL_HIST(type, unused, rcp_width)	\
    {							\
	type *ip = IM_ROWADR(im, j, v);			\
	ip += topl.x;					\
	for (int i=topl.x; i<=botr.x; i++) {		\
	    hp_real_count(hg->hp, *ip, rcp_width);	\
	    ip++;					\
	}						\
    }

#define IMROW_BINARY_HIST()			\
    {						\
	uint8_t *ip;				\
	ip = IM_ROWADR(im, j, v);		\
	ip += topl.x;				\
	for (int i=topl.x; i<=botr.x; i++) {	\
	    if (*ip++)				\
		ip_hist_bin_inc(hg, 0, 1);	\
	    else				\
		ip_hist_bin_inc(hg, 0, 0);	\
	}					\
    }

#define IMROW_COMPLEX_HIST(type, btype)			\
    {							\
	type *ip;					\
	btype val;					\
	btype flow = low;				\
	btype fhigh = high;				\
	ip = IM_ROWADR(im, j, v);			\
	ip += topl.x;					\
	for (int i=topl.x; i<=botr.x; i++) {		\
	    val = ip->r;				\
	    if (val < flow)				\
		ip_hist_under_inc(hg, 0);		\
	    else if (val >= fhigh)			\
		ip_hist_over_inc(hg, 0);		\
	    else if (isnan(val))			\
		ip_hist_nan_inc(hg, 0);			\
	    else{					\
		int bin = (val - low)*rcp_width;	\
		ip_hist_bin_inc(hg, 0, bin);		\
	    }						\
	    val = ip->i;				\
	    if (val < flow)				\
		ip_hist_under_inc(hg, 1);		\
	    else if (val >= fhigh)			\
		ip_hist_over_inc(hg, 1);		\
	    else if (isnan(val))			\
		ip_hist_nan_inc(hg, 1);			\
	    else{					\
		int bin = (val - low)*rcp_width;	\
		ip_hist_bin_inc(hg, 1, bin);		\
	    }						\
	    ip++;					\
	}						\
    }

#define IMROW_RGB_HIST(type, ptype)			\
    {							\
	ptype ilow = low;				\
	ptype ihigh = high;				\
	type *ip;					\
	ip = IM_ROWADR(im, j, v);			\
	ip += topl.x;					\
	for (int i=topl.x; i<=botr.x; i++) {		\
	    ptype val = ip->r;				\
	    if (val < ilow)				\
		ip_hist_under_inc(hg, 0);		\
	    else if (val >= ihigh)			\
		ip_hist_over_inc(hg, 0);		\
	    else{					\
		int bin = (int)(((uint64_t)(val - ilow) * scaler) >> 32); \
		ip_hist_bin_inc(hg, 0, bin);		\
	    }						\
	    val = ip->g;				\
	    if (val < ilow)				\
		ip_hist_under_inc(hg, 1);		\
	    else if (val >= ihigh)			\
		ip_hist_over_inc(hg, 1);		\
	    else{					\
		int bin = (int)(((uint64_t)(val - ilow) * scaler) >> 32); \
		ip_hist_bin_inc(hg, 1, bin);		\
	    }						\
	    val = ip->b;				\
	    if (val < ilow)				\
		ip_hist_under_inc(hg, 2);		\
	    else if (val >= ihigh)			\
		ip_hist_over_inc(hg, 2);		\
	    else{					\
		int bin = (int)(((uint64_t)(val - ilow) * scaler) >> 32); \
		ip_hist_bin_inc(hg, 2, bin);		\
	    }						\
	    ip++;					\
	}						\
    }

#define IMROW_COMPLEX_MAG_HIST(type, btype, rcp_width)		   \
    {								   \
	type *ip;						   \
	ip = IM_ROWADR(im, j, v);				   \
	ip += topl.x;						   \
	for (int i=topl.x; i<=botr.x; i++) {			   \
	    hp_real_count(hg->hp, hypot(ip->r, ip->i), rcp_width); \
	    ip++;						   \
	}							   \
    }

#define IMROW_RGB_INTENSITY_HIST(type, btype, scaler)	\
    {							\
	type *ip;					\
	btype ilow = 1000*low;				\
	btype ihigh = 1000*high;			\
	ip = IM_ROWADR(im, j, v);			\
	ip += topl.x;					\
	for (int i=topl.x; i<=botr.x; i++) {		\
	    btype val = 299*ip->r + 587*ip->g + 114*ip->b;	\
	    if (val < ilow)				\
		ip_hist_under_inc(hg, 0);		\
	    else if (val >= ihigh)			\
		ip_hist_over_inc(hg, 0);		\
	    else{					\
		int bin = (int)(((uint64_t)(val - ilow) * scaler) >> 32); \
		ip_hist_bin_inc(hg, 0, bin);		\
	    }						\
	    ip++;					\
	}						\
    }



#define generate_hist_magnitude_1d(t, ptype, row_proc, rtype, rprep)	\
    static void hist_magnitude_##t(ip_hist *hg, ip_image * im)		\
    {									\
	ip_coord2d topl = hg->box.origin;				\
	ip_coord2d botr = ip_box_botr(hg->box);				\
	double low = hg->hp[0].low;					\
	double high = hg->hp[0].high;					\
	rtype scaler = rprep(hg->hp, 0);				\
	for (int j = topl.y; j<=botr.y; j++) {				\
	    row_proc(t##_type, ptype, scaler);				\
	}								\
    }

#define generate_hist_magnitude_r1d(t, ptype, row_proc, rtype, rprep)	\
    static void hist_magnitude_##t(ip_hist *hg, ip_image * im)		\
    {									\
	ip_coord2d topl = hg->box.origin;				\
	ip_coord2d botr = ip_box_botr(hg->box);				\
	rtype scaler = rprep(hg->hp, 0);				\
	for (int j = topl.y; j<=botr.y; j++) {				\
	    row_proc(t##_type, ptype, scaler);				\
	}								\
    }

#define generate_hist_component_complex(t)				\
    static void hist_component_##t(ip_hist *hg, ip_image * im)		\
    {									\
	ip_coord2d topl = hg->box.origin;				\
	ip_coord2d botr = ip_box_botr(hg->box);				\
	for (int j = topl.y; j<=botr.y; j++) {				\
	    t##_type *ip = IM_ROWADR(im, j, v);				\
	    double r_rcp_width = real_scaler(hg->hp, 0);		\
	    double i_rcp_width = real_scaler(hg->hp, 1);		\
	    ip += topl.x;						\
	    for (int i=topl.x; i<=botr.x; i++) {			\
		hp_real_count(hg->hp, ip->r, r_rcp_width);		\
		hp_real_count(hg->hp+1, ip->i, i_rcp_width);		\
		ip++;							\
	    }								\
	}								\
    }

#define generate_hist_magphase_complex(t)				\
    static void hist_magphase_##t(ip_hist *hg, ip_image * im)		\
    {									\
	ip_coord2d topl = hg->box.origin;				\
	ip_coord2d botr = ip_box_botr(hg->box);				\
	for (int j = topl.y; j<=botr.y; j++) {				\
	    t##_type *ip = IM_ROWADR(im, j, v);				\
	    double m_rcp_width = real_scaler(hg->hp, 0);		\
	    double p_rcp_width = real_scaler(hg->hp, 1);		\
	    ip += topl.x;						\
	    for (int i=topl.x; i<=botr.x; i++) {			\
		double val = hypot(ip->r, ip->i);			\
		hp_real_count(hg->hp, val, m_rcp_width);		\
		val = atan2(ip->i, ip->r);				\
		hp_real_count(hg->hp+1, val, p_rcp_width);		\
		ip++;							\
	    }								\
	}								\
    }

#define generate_hist_component_rgb(t)					\
    static void hist_component_##t(ip_hist *hg, ip_image * im)		\
    {									\
	ip_coord2d topl = hg->box.origin;				\
	ip_coord2d botr = ip_box_botr(hg->box);				\
	uint64_t r_scaler = int_scaler(hg->hp, 0);			\
	uint64_t g_scaler = int_scaler(hg->hp, 1);			\
	uint64_t b_scaler = int_scaler(hg->hp, 2);			\
	for (int j = topl.y; j<=botr.y; j++) {				\
	    int r_low = hg->hp[0].low;					\
	    int r_high = hg->hp[0].high;				\
	    int g_low = hg->hp[1].low;					\
	    int g_high = hg->hp[1].high;				\
	    int b_low = hg->hp[2].low;					\
	    int b_high = hg->hp[2].high;				\
	    t##_type *ip = IM_ROWADR(im, j, v);				\
	    ip += topl.x;						\
	    for (int i=topl.x; i<=botr.x; i++) {			\
		hp_int_count(hg->hp,   ip->r, r_low, r_high, r_scaler);	\
		hp_int_count(hg->hp+1, ip->g, g_low, g_high, g_scaler);	\
		hp_int_count(hg->hp+2, ip->b, b_low, b_high, b_scaler);	\
		ip++;							\
	    }								\
	}								\
    }

static void hist_magnitude_BINARY(ip_hist *hg, ip_image * im)
{
    ip_coord2d topl = hg->box.origin;
    ip_coord2d botr = ip_box_botr(hg->box);
    for (int j = topl.y; j<=botr.y; j++) {
	IMROW_BINARY_HIST();
    }
}

generate_hist_magnitude_1d(UBYTE, int, IMROW_HIST, uint64_t, int_scaler)
generate_hist_magnitude_1d(BYTE, int, IMROW_HIST, uint64_t, int_scaler)
generate_hist_magnitude_1d(USHORT, int, IMROW_HIST, uint64_t, int_scaler)
generate_hist_magnitude_1d(SHORT, int, IMROW_HIST, uint64_t, int_scaler)
generate_hist_magnitude_1d(ULONG, uint32_t, IMROW_HIST, uint64_t, int_scaler)
generate_hist_magnitude_1d(LONG, int32_t, IMROW_HIST_LONG, uint64_t, int_scaler)
generate_hist_magnitude_r1d(FLOAT, double, IMROW_REAL_HIST, double, real_scaler)
generate_hist_magnitude_r1d(DOUBLE, double, IMROW_REAL_HIST, double, real_scaler)
generate_hist_magnitude_r1d(COMPLEX, double, IMROW_COMPLEX_MAG_HIST, double, real_scaler)
generate_hist_magnitude_r1d(DCOMPLEX, double, IMROW_COMPLEX_MAG_HIST, double, real_scaler)

generate_hist_magnitude_1d(RGB, int, IMROW_RGB_INTENSITY_HIST, uint64_t, rgb_int_scaler)
generate_hist_magnitude_1d(RGB16, int, IMROW_RGB_INTENSITY_HIST, uint64_t, rgb_int_scaler)

generate_hist_component_complex(COMPLEX)
generate_hist_component_complex(DCOMPLEX)

generate_hist_component_rgb(RGB)
generate_hist_component_rgb(RGB16)

generate_hist_magphase_complex(COMPLEX)
generate_hist_magphase_complex(DCOMPLEX)


static void hist_layered_UBYTE(ip_hist *hg, ip_image * im)
{
    ip_coord2d topl = hg->box.origin;
    ip_coord2d botr = ip_box_botr(hg->box);
    for (int j = topl.y; j<=botr.y; j++) {
	uint8_t *ip = IM_ROWADR(im, j, v);
	ip += topl.x;
	for (int i=topl.x; i<=botr.x; i++)
	    ip_hist_lu8_bin_inc(hg, *ip++);
    }
}

static void hist_layered_BYTE(ip_hist *hg, ip_image * im)
{
    ip_coord2d topl = hg->box.origin;
    ip_coord2d botr = ip_box_botr(hg->box);
    for (int j = topl.y; j<=botr.y; j++) {
	int8_t *ip = IM_ROWADR(im, j, v);
	ip += topl.x;
	for (int i=topl.x; i<=botr.x; i++) {
	    int val = *ip++;
	    ip_hist_lu8_bin_inc(hg, val + 128);
	}
    }
}

improc_function_Ih ip_hist_component_bytype[IM_TYPEMAX] = {
    [IM_BINARY] = hist_magnitude_BINARY,
    [IM_UBYTE] = hist_magnitude_UBYTE,
    [IM_BYTE] = hist_magnitude_BYTE,
    [IM_USHORT] = hist_magnitude_USHORT,
    [IM_SHORT] = hist_magnitude_SHORT,
    [IM_ULONG] = hist_magnitude_ULONG,
    [IM_LONG] = hist_magnitude_LONG,
    [IM_FLOAT] = hist_magnitude_FLOAT,
    [IM_DOUBLE] = hist_magnitude_DOUBLE,
    [IM_COMPLEX] = hist_component_COMPLEX,
    [IM_DCOMPLEX] = hist_component_DCOMPLEX,
    [IM_RGB] = hist_component_RGB,
    [IM_RGB16] = hist_component_RGB16,
    [IM_PALETTE] = hist_magnitude_UBYTE
};

improc_function_Ih ip_hist_intensity_bytype[IM_TYPEMAX] = {
    [IM_BINARY] = hist_magnitude_BINARY,
    [IM_UBYTE] = hist_magnitude_UBYTE,
    [IM_BYTE] = hist_magnitude_BYTE,
    [IM_USHORT] = hist_magnitude_USHORT,
    [IM_SHORT] = hist_magnitude_SHORT,
    [IM_ULONG] = hist_magnitude_ULONG,
    [IM_LONG] = hist_magnitude_LONG,
    [IM_FLOAT] = hist_magnitude_FLOAT,
    [IM_DOUBLE] = hist_magnitude_DOUBLE,
    [IM_COMPLEX] = hist_magnitude_COMPLEX,
    [IM_DCOMPLEX] = hist_magnitude_DCOMPLEX,
    [IM_RGB] = hist_magnitude_RGB,
    [IM_RGB16] = hist_magnitude_RGB16,
    [IM_PALETTE] = hist_magnitude_UBYTE
};


ip_hist *im_hist(ip_image *im, ip_roi *roi, double low, double high, int numbins)
{
    struct ip_hist_args args = {
	.htype = HIST_COMPONENTS,
	.hflag = IP_HIST_HIGH_IS_HIGHBIN_RHS,
	.roi = roi,
	.low = low,
	.high = high,
	.numbins = numbins,
	.numparts = 0,
	.parts_spec = NULL
    };

    return im_hist_x(im, args);
}


ip_hist *im_hist_x(ip_image *im, struct ip_hist_args hargs)
{
    int failed = 0;
    int num_parts;
    ip_hist *h;
    struct ip_hist_parts_spec hps[3];

    ip_log_routine(__func__);

    /* Check have valid image to work with */

    if (NOT image_valid(im))
	return NULL;

    /* Currently don't support RGBA */

    if (im->type == IM_RGBA) {
	ip_log_error(ERR_BAD_TYPE);
	ip_pop_routine();
	return NULL;
    }

    /* Check hargs values */

    if (NOT ip_parm_inrange("htype", hargs.htype, 0, IP_HIST_MAX_TYPE))
	return NULL;

    /* Work out number of histogram parts required */
    switch (hargs.htype) {
    case HIST_COMPONENTS:
	num_parts = ip_type_size(im->type) / ip_type_basesize(im->type);
	break;
    case HIST_INTENSITY:
	num_parts = 1;
	break;
    case HIST_MAGPHASE:
	if (NOT image_type_complex(im)) {
	    ip_log_error(ERR_WRONG_IMAGE_TYPE, "im", ip_type_name(im->type));
	    ip_pop_routine();
	    return NULL;
	}
	num_parts = 2;
	break;
    case HIST_LAYERED:
	if (NOT (im->type == IM_BYTE || im->type == IM_UBYTE)) {
	    ip_log_error(ERR_BAD_TYPE);
	    ip_pop_routine();
	    return NULL;
	}
	num_parts = 1;
	break;
    default:
	ip_log_error(ERR_PARM_BAD, "hargs.htype", hargs.htype);
	ip_pop_routine();
	return NULL;
    }

    /*
     * Check that the number of histogram parts is reasonable even though it
     * is impossible at this stage for it not to be so. Maybe the compiler
     * will be smart enough to remove this code!
     */
    if (num_parts < 1 || num_parts > 3) {
	ip_log_error(ERR_ALGORITHM_FAULT);
	ip_pop_routine();
	return NULL;
    }

    /* Set histogram low and high parameters */

    if (im->type == IM_BINARY) {
	/*
	 * We ignore the passed low/high in the case of IM_BINARY as there is
	 * only one valid value for each.
	 */
	hps[0].low = 0;
	hps[0].high = 2;
	hps[0].numbins = 2;
    }else{
	if (hargs.parts_spec) {
	    /*
	     * If parts_spec array provided then that gives the
	     * specifications, but first check that the caller has
	     * initialised enough parts_spec entries.
	     */
	    if (hargs.numparts < num_parts) {
		ip_log_error(ERR_PARM_BAD, "hargs.numparts", hargs.numparts);
		ip_pop_routine();
		return NULL;
	    }
	    for (int j=0; j<num_parts; j++) {
		hps[j] = hargs.parts_spec[j];
	    }
	}else{
	    /* specs are given by args high, low and numbins */
	    for (int j=0; j<num_parts; j++) {
		hps[j].low = hargs.low;
		hps[j].high = hargs.high;
		hps[j].numbins = hargs.numbins;
	    }
	}
    }

    if (image_type_integer(im) || image_type_clrim(im)) {
	for (int j=0; j<num_parts; j++) {
	    hps[j].low = round(hps[j].low);
	    hps[j].high = round(hps[j].high);
	}
    }

    if (hargs.htype == HIST_MAGPHASE) {
	/* Bring limits back into a sensible range if needs be. */
	if (hps[0].low < 0) hps[0].low = 0.0;
	if (hps[1].low < 0) hps[1].low = 0.0;
	if (hps[1].high > 2*M_PI) hps[1].high = 2*M_PI;
    }

    /*
     * Default is to take high as specifying LHS of over bin.  But sometimes
     * it is more useful to specify high as the RHS of the highest valid
     * bin.  Provide that option.
     */
    if (NOT (hargs.hflag & IP_HIST_HIGH_IS_OVER_LHS)) {
	/* i.e. flags are IP_HIST_HIGH_IS_HIGHBIN_RHS */

	if (image_type_real(im) || image_type_complex(im))
	    for (int j=0; j<num_parts; j++)
		hps[j].high = nextafter(hps[j].high, HUGE_VAL);
	else if (im->type != IM_BINARY) /* i.e., if integer/colour image */
	    for (int j=0; j<num_parts; j++)
		hps[j].high += 1;
    }

    /* We don't need to check number of bins because ip_alloc_hist() does. */
    if ((h = ip_alloc_hist(hargs.htype, num_parts, hps)) == NULL) {
	ip_pop_routine();
	return NULL;
    }

    /* Initialise the box (ROI) in the image to be used */
    if (hargs.roi) {
	if (NOT roi_in_image(im, hargs.roi)) {
	    ip_free_hist(h);
	    return NULL;
	}
	h->box.origin = roi_get(ROI_ORIGIN, hargs.roi).pt;
	h->box.size = roi_get(ROI_SIZE, hargs.roi).pt;
    }else{
	h->box.origin = (ip_coord2d){0, 0};
	h->box.size = im->size;
    }

    /* Record in histogram image type and size used to create this histogram */
    h->imtype = im->type;
    h->imsize = im->size;

    /* Calculate the histogram */
    switch (h->htype) {
    case HIST_COMPONENTS:
	if (ip_hist_component_bytype[im->type])
	    ip_hist_component_bytype[im->type](h, im);
	else
	    failed = 1;
	break;
    case HIST_INTENSITY:
	if (ip_hist_intensity_bytype[im->type])
	    ip_hist_intensity_bytype[im->type](h, im);
	else
	    failed = 1;
	break;
    case HIST_MAGPHASE:
	if (im->type == IM_COMPLEX)
	    hist_magphase_COMPLEX(h, im);
	else
	    hist_magphase_DCOMPLEX(h, im);
	break;
    case HIST_LAYERED:
	if (im->type == IM_UBYTE)
	    hist_layered_UBYTE(h, im);
	else
	    hist_layered_BYTE(h, im);
	break;
    default:
	/* Not possible */
	failed = 1;
	break;
    }

    if (failed) {
	ip_log_error(ERR_NOT_IMPLEMENTED);
	ip_free_hist(h);
	h = NULL;
    }

    ip_pop_routine();
    return h;
}



/*

NAME:

   ip_hist_peak() \- Find peak value in histogram

PROTOTYPE:

   #include <ip/ip.h>
   #include <hist/hist.h>

   int  ip_hist_peak( h, hp, where, flag )
   ip_hist *h;
   int hp;
   int *where;
   int flag;

DESCRIPTION:

   Calculate the peak point in the histogram ignoring the under and over
   bins. Returns this value.  If 'where' is non-NULL, then stores location
   (bin number) of peak value in 'where'.  To convert the bin number into an
   actual value use ip_hist_bin_left() or ip_hist_bin_centre().

   'hp' is the histogram part to use for a multicomponent image.  If scalar
   then hp must be 0. If histogram of COMPLEX/DCOMPLEX image then set 'hp'
   to zero to find peak in real part histogram and 'hp' to 1 to find peak in
   imaginary part histogram.  If a MAGPHASE histogram then 'hp' 0 is over
   the complex magnitude and 'hp' is over the complex phase.  If RGB image
   'hp' can be one of 0, 1 or 2 for the red, green or blue part,
   respectively.  If a LAYERED histogram 'hp' must be 0.

   If 'flag' is FLG_SKIPZERO then doesn''t consider h[0] in calculation of
   peak value. In this way, a zero background value will not be included in
   calculation of peak value.  Also if FLG_SKIP255 is used as a flag then
   h[255] (if the histogram was calculated on a UBYTE image) is not used in
   the calculation.  FLG_SKIPZERO and FLAG_SKIP255 can be ORed together.
   These flags are most useful for histograms calculated on UBYTE images.

   If an error occurs then -1 is returned.

ERRORS:

   ERR_BAD_FLAG

SEE ALSO:

   im_hist()
   ip_hist_bin_left()            ip_hist_bin_centre()

*/

int ip_hist_peak(ip_hist *h, int p, int *where, int flag)
{
    int max;
    int loc;

    ip_log_routine(__func__);

    if (NOT ip_flag_ok(flag, FLG_SKIPZERO, FLG_SKIP255,
			     FLG_SKIPZERO | FLG_SKIP255, LASTFLAG))
	return -1;

    if (NOT ip_parm_inrange("hp", p, 0, h->num_parts))
	return -1;

    if (h->htype != HIST_LAYERED) {
	/* INTENSITY, COMPONENT or MAGPHASE histogram */
	int start = h->hp[p].offset;
	int end = h->hp[p].numbins + h->hp[p].offset;

	if (flag & FLG_SKIPZERO) start = 1;
	if ((flag & FLG_SKIP255) && (end == 256)) end = 255;

	loc = max = 0;
	for (int j=start; j<end; j++) {
	    int count = ip_hist_bin_count(h, p, j);
	    if (count > max) {
		max = count;
		loc = j;
	    }
	}
    }else{
	/* LAYERED histogram (p must be zero) */
	int start = h->hp[p].offset;
	int end = h->hp[0].layer_numbins + h->hp[0].offset;
	int shift = ip_hist_l_shift(h);
	int mask = ip_hist_l_mask(h);

	if (flag & FLG_SKIPZERO) start = 1;
	if ((flag & FLG_SKIP255) && (end == 256)) end = 255;

	int j = start;
	loc = max = 0;
	while (j < end) {
	    int bigcount = ip_hist_l_coarse_count(h, j, shift);
	    if (bigcount > max) {
		/* Max might be in one of this group of bins */
		int endsmall = j | mask;
		while (j < endsmall) {
		    int count = ip_hist_l_bin_count(h, j, shift);
		    if (count > max) {
			max = count;
			loc = j;
		    }
		    j++;
		}
	    }else{
		/* Max can't be in this group of bins; skip them */
		j = (j | mask) + 1;
	    }
	}
    }
    if (where) *where = loc;

    ip_pop_routine();
    return max;
}



/*

NAME:

   ip_hist_total_count()

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/hist.h>

   long int  ip_hist_total_count( h, p )
   ip_hist *h;
   int p;

DESCRIPTION:

   Returns the total number of counts in the valid bins of part 'p' of
   histogram 'h'.  The valid bins are all bins excluding the special under,
   over and nan bins.

   If an error occurs returns -1.

ERRORS:

   ERR_PARM_OOR

*/


long int ip_hist_total_count(ip_hist *h, int p)
{
    long int sum = 0;

    ip_log_routine(__func__);

    if (NOT ip_parm_inrange("part", p, 0, h->num_parts))
	return -1;

    if (h->htype == HIST_LAYERED) {
	/* Layered histogram; can sum top layer */
	int shift = ip_hist_l_shift(h);
	int start = 0 + h->hp[0].offset;
	int end = h->hp[0].layer_numbins + h->hp[0].offset;
	for (int j=start; j<end; j++) {
	    sum += ip_hist_l_coarse_count(h, j, shift);
	}
    }else{
	int start = 0 + h->hp[0].offset;
	int end = h->hp[0].numbins + h->hp[0].offset;
	for (int j=start; j<end; j++) {
	    sum += ip_hist_bin_count(h, p, j);
	}
    }

    ip_pop_routine();
    return sum;
}

/*

NAME:

   ip_hist_smooth() \- Smooth histogram

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/hist.h>

   int  ip_hist_smooth( h, hp, size )
   ip_hist *h;
   int hp;
   int size;

DESCRIPTION:

   Smooths the histogram part 'hp' of the histogram 'h' with a mean filter
   window of the specified size.  If 'hp' is -1 then smoothes all histogram
   parts of the histogram.

LIMITATIONS:

   Smoothing of a HIST_LAYERED histogram is not currently supported.

ERRORS:

   ERR_OUT_OF_MEM

SEE ALSO:

   im_hist()

*/

int ip_hist_smooth(ip_hist *hg, int p, int size)
{
    int *mem;
    int memidx;
    int hsize;
    int pstart, pend;

    /* Identity operator; nothing to do */
    if (size == 1)
	return OK;

    ip_log_routine(__func__);

    if (NOT ip_parm_inrange("wsize", size, 1, hg->hp[0].numbins))
	return ip_error;

    if (NOT ip_parm_inrange("hp", p, -1, hg->num_parts))
	return ip_error;

    if (hg->htype == HIST_LAYERED) {
	ip_log_error(ERR_NOT_IMPLEMENTED);
	ip_pop_routine();
	return ip_error;
    }

    /* Get temporary working space */

    if ((mem = ip_malloc(size*sizeof(int))) == NULL) {
	return ip_error;
    }

    memidx = 0;
    hsize = size/2;		/* half-length of array */

    if (p == -1) {
	pstart = 0;
	pend = hg->num_parts;
    }else{
	pstart = pend = p;
    }

    for (int p=pstart; p<pend; p++) {
	double sum;
	int i;

	/* Initialise working space */

	sum = 0.0;
	for (i = 0; i < size; i++) {	/* initialise array sum */
	    int bin = i + hg->hp[p].offset;
	    sum += (double)(mem[i] = ip_hist_bin_count(hg, p, bin));
	    if (i >= hsize) {
		ip_hist_bin_set(hg, p, bin-hsize, (int)ROUND(sum / (double)(i+1)));
	    }
	}

	/* Smooth histogram by averaging over a sliding size length window */

	for (i = (hsize+1); i <(hg->hp[p].numbins-hsize); i++) {
	    /* move summing array along ...*/
	    int bin = i + hg->hp[p].offset;
	    sum -= mem[memidx];
	    sum += (double)(mem[memidx] = ip_hist_bin_count(hg, p, bin+hsize));
	    ip_hist_bin_set(hg, p, bin, lround(sum / (double)size));
	    memidx++;
	    if (memidx == size) memidx = 0;
	}

	/* Last few values ... */

	for ( ; i<hg->hp[p].numbins; i++) {
	    int bin = i + hg->hp[p].offset;
	    sum -= mem[memidx];
	    ip_hist_bin_set(hg, p, bin, (int)ROUND(sum / (double)(hsize + (hg->hp[p].numbins-i))));
	    memidx++;
	    if (memidx == size) memidx = 0;
	}
    }

    ip_free(mem);
    ip_pop_routine();
    return ip_error;
}


/*

NAME:

   ip_hist_bin_left() \- Get left edge of histogram bin

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/hist.h>

   double  hist_bin_left( h, hp, bin )
   ip_hist *h;
   int hp;
   int bin;

DESCRIPTION:

   Returns the actual value of the left edge of bin number 'bin'.

ERRORS:

   None possible; this routine does not check arguments.

SEE ALSO:

   im_hist()
   ip_hist_bin_centre()             ip_hist_bin_width()
   IP_HIST_BIN_COUNT()

*/

#if 0
/* Defined as static inline in hist.h */
double ip_hist_bin_left(ip_hist *h, int p, int bin)
{
    double width;

    ip_log_routine(__func__);

    if (NOT ip_parm_inrange("bin_number",bin,0,h->hp[p].numbins-1))
	return 0.0;

    width = ip_hist_bin_width(h, p);
    ip_pop_routine();
    return h->hp[p].low + width * bin;
}
#endif

/*

NAME:

   ip_hist_bin_centre() \- Get centre coordinate of histogram bin

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/hist.h>

   double  ip_hist_bin_centre( h, hp, bin )
   ip_hist *h;
   int hp;
   int bin;

DESCRIPTION:

   Returns the actual value at the centre of bin number 'bin' or NAN if an
   error occurs.

ERRORS:

   None possible; this routine does not check arguments.

SEE ALSO:

   im_hist()
   ip_hist_bin_left()             ip_hist_bin_width()
   IP_HIST_BIN_COUNT()

*/

#if 0
/* Defined as static inline in hist.h */
double ip_hist_bin_centre(ip_hist *h, int p, int bin)
{
    double width, res;

    ip_log_routine(__func__);

    if (NOT ip_parm_inrange("hp", p, 0, h->num_parts))
	return nan("");

    if (NOT ip_parm_inrange("bin_number",bin,0,h->hp[p].numbins-1))
	return nan("");

    width = ip_hist_bin_width(h, p);
    res = h->hp[p].low + width * (bin + 0.5);

    ip_pop_routine();
    return res;
}
#endif


/*

NAME:

   ip_hist_bin_width() \- Get width of bins in histogram

PROTOTYPE:

   #include <ip/ip.h>

   double  ip_hist_bin_width( h, hp )
   ip_hist *h;
   int hp;

DESCRIPTION:

   Returns the width of bins in the histogram 'h'.

SEE ALSO:

   im_hist()
   ip_hist_bin_left()             ip_hist_bin_centre()

*/



/*

NAME:

   ip_hist_bin_count() \- Get count in bin of histogram

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/hist.h>

   int  ip_hist_bin_count( h, p, bin )
   ip_hist *h;
   int p;
   int bin;

DESCRIPTION:

   Returns the frequency count stored in bin number 'bin' of the histogram
   part 'p' of histogram 'h'.

SEE ALSO:

   im_hist()
   ip_hist_bin_left()             ip_hist_bin_centre()

*/


/*

NAME:

   ip_hist_stats() \- Calculate statistics from histogram

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/hist.h>
   #include <ip/stats.h>

   void  ip_hist_stats( h, st )
   ip_hist *h;
   ip_stats *st;

DESCRIPTION:

   Caculate statistics from a histogram.  Only uses the specified bins in
   the histogram, i.e., the under and over fields are ignored.  Calculates
   the minimum and maximum value present in the histgram, and the summation,
   mean, RMS and standard deviation of the the histogram.

   Uses all pixels (including the under and over) to calculate median and
   entropy.

   Calculating a histogram over an image, provided no pixels get assigned to
   the under or over bins, and then calculating statistics on the histogram
   gives the same or similar result as calculating the statistics directly
   on the image.  The same result is achieved if every pixel has its own
   unique histogram bin, otherwise only a similar result is achieved.

   The stats structure must be preallocated by calling ip_alloc_stats().

SEE ALSO:

   im_hist()
   im_stats()
   ip_alloc_stats()

*/


void ip_hist_stats(ip_hist *h, ip_stats *st)
{
    double value;
    double sum, sumsq;
    int  count, halfway, between, accum;
    double median, entropy;
    int have_hit_halfway;
    int64_t numpix;
    int p = 0;

    st->type = h->imtype;
    st->size = h->imsize;
    st->box = h->box;
    st->infinities = 0;

    /* TBD: Layered histogram not implemented */
    if (h->htype == HIST_LAYERED)
	return;

    /*
     * Calculate position for median.
     * halfway --- this count is the median.
     * between --- If true then median lies between halfway and halfway+1
     * have_hit_halfway --- 0 if haven't, bin number+2 if have.
     */
    numpix = ip_box_numpixels(h->box);
    halfway = (numpix + 1)/2;
    between = !(numpix & 0x01);
    accum = h->hp[p].under;
    have_hit_halfway = 0;
    median = -100;		/* Median not yet found */
    if (accum > halfway || (accum == halfway && !between))
	median = -1;		/* Found median at bin -1 (i.e. under) */
    else if (accum == halfway && between)
	have_hit_halfway = 1;	/* Have hit halfway and bin is -1 */

    /* Initialise stats min/max search and accumulators */
    st->max = -DBL_MAX;
    st->min = DBL_MAX;
    sum = sumsq = 0.0;
    entropy = 0.0;

    /* Calculate the histogram stats */
    for (int j=0; j<h->hp[0].numbins; j++) {
	double prob;

	value = type_integer(h->imtype) ? ip_hist_bin_left(h, p, j) : ip_hist_bin_centre(h, p, j);
	count = ip_hist_bin_count(h, p, j);

	/* Update min and max bin with counts */;
	if (count && (value > st->max))
	    st->max = value;
	if (count && (value < st->min))
	    st->min = value;

	/* Have we found median? */
	accum += count;
	if (median < -2 && accum >= halfway) {
	    if (count > 0 && have_hit_halfway)
		median = ((have_hit_halfway-2.0) + (double)j) / 2.0;
	    else if (accum > halfway || (accum == halfway && !between))
		median = j;
	    else
		have_hit_halfway = j+2;
	}

	/* Update sum/sumsq for calculating mean and stddev. */
	sum += value * count;
	sumsq += count * (value * value);

	/* Update entropy calculation */
	prob = (double)count / (double)numpix;
	if (prob > 0.0)
	    entropy += prob * log(prob);
    }
    if (median < -2) {
	if (have_hit_halfway) {
	    median = ((have_hit_halfway-2.0) + (double)h->hp[p].numbins) / 2.0;
	}else
	    median = h->hp[p].numbins;
    }
    st->median = median;
    st->entropy = -entropy;

    st->mean = sum / numpix;
    st->stddev = sqrt((sumsq - sum*sum/numpix) / numpix);
    st->rms = sqrt(sumsq / numpix);
    st->sum = sum;
}


/*

NAME:

   ip_hist_entropy()

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/hist.h>

   double  ip_hist_entropy( h, p )
   ip_hist *h;
   int p;

DESCRIPTION:

   Calculate the entropy of the part 'p' of the histrogram 'h' over the
   valid bins (i.e. excluding under, over and nan bins, and bins with zero
   counts).  The natural logarithm is used in the entropy calculation.

   Entropy = -Sum_i p_i ln(p_i) where p_i is the probability (estimated by
   the normalised frequency) of the ith bin.

   Bins with zero counts are excluded from the calculation.

   Returns -1.0 if an error occurs.

ERRORS:

   ERR_PARM_OOR

*/

double ip_hist_entropy(ip_hist *h, int p)
{
    double entropy = 0.0;
    double sum;

    ip_log_routine(__func__);

    if (h->htype == HIST_LAYERED) {
	ip_log_error(ERR_NOT_IMPLEMENTED);
	ip_pop_routine();
	return -1.0;
    }

    if ((sum = ip_hist_total_count(h, p)) <  0) {
	ip_pop_routine();
	return -1.0;
    }

    for (int j=0; j<h->hp[p].numbins; j++) {
	double probability = ip_hist_bin_count(h, p, j) / sum;
	if (probability != 0.0)
	    entropy += probability * log(probability);
    }

    ip_pop_routine();
    return -entropy;
}
