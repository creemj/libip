/*
   Module: norm.c
   Author: M. J. Cree

   Copyright (C) 1995-1998, 2014 by Michael J. Cree.

DESCRIPTION:

   Routines for normalisaton of images.

ENTRY POINTS:

   im_norm()

*/

#include <math.h>

#include <ip/types.h>
#include <ip/image.h>
#include <ip/error.h>
#include <ip/flags.h>
#include <ip/proto.h>


/*

NAME:

   im_norm() \- Normalise the pixel range in an image

PROTOTYPE:

   #include <ip/ip.h>

   int  im_norm( im, imin, imax, fmin, fmax, flag )
   ip_image *im;
   double imin, imax;
   double fmin, fmax;
   int flag;

DESCRIPTION:

   Normalise image by the rule:

\|   foreach pixel in image do {
\|
\|      result = ((double)pixel - imin) 
\|                     * (fmax-fmin)/(imax-imin) + fmin
\|
\|      if (flag == FLG_CLIP) then
\|	   if (result > fmax) then result = fmax
\|	   if (result < fmin) then result = fmin
\|      endif
\|
\|      if (result > MAX_VALUE(image type))
\|         result = MAX_VALUE(image type)
\|      if (result < MIN_VALUE(image type))
\|         result = MIN_VALUE(image type)
\|
\|      if (image type REAL)
\|         pixel = result
\|      else
\|         pixel = round(result)
\|
\|   }

   This routine has been implemented with precision in mind, rather
   than speed.  Floating point arithmetic is used throughout the
   calculation, whatever the image type. For integral image types, the
   result (for each pixel) is rounded to the nearest integer to give
   the most accurate result.

ERRORS:

   ERR_UNSUPPORTED_TYPE - No complex nor colour image support.
   ERR_BAD_PARAMETER

*/

#define IMNORM_ROW(im, row, t, tmin, tmax) {		  \
    t *ptr = IM_ROWADR(im, row, v);			  \
    for (int i=0; i<im->size.x; i++) {			  \
	double val;					  \
	val =  ((double)*ptr - imin) * scale + fmin;	  \
	if (val > tmax) val = tmax;			  \
	if (val < tmin) val = tmin;			  \
	*ptr++ = (t)round(val);				  \
    }							  \
}
	

int im_norm(ip_image *im, double imin, double imax, double fmin, double fmax,
	    int flag)
{
    double scale, tmin, tmax;

    ip_log_routine(__func__);

    if (type_complex(im->type) || type_clrim(im->type)) {
	ip_log_error(ERR_UNSUPPORTED_TYPE);
	goto exit;
    }

    if (NOT (image_type_integer(im) || image_type_real(im))) {
	ip_log_error(ERR_BAD_TYPE);
	goto exit;
    }

    if (NOT (in_image_val(im,imin,imax) && in_image_val(im,fmin,fmax))) {
	ip_log_error(ERR_BAD_PARAMETER);
	goto exit;
    }

    scale = (fmax - fmin) / (imax - imin);

    tmin = ip_image_type_info[im->type].minvalue;
    tmax = ip_image_type_info[im->type].maxvalue;
    tmin = (fmin > tmin) ? fmin : tmin;
    tmax = (fmax < tmax) ? fmax : tmax;

    for (int j=0; j<im->size.y; j++) {
	switch (im->type) {
	case IM_UBYTE:
	    IMNORM_ROW(im, j, uint8_t, tmin, tmax);
	    break;
	case IM_BYTE:
	    IMNORM_ROW(im, j, int8_t, tmin, tmax);
	    break;
	case IM_USHORT:
	    IMNORM_ROW(im, j, uint16_t, tmin, tmax);
	    break;
	case IM_SHORT:
	    IMNORM_ROW(im, j, int16_t, tmin, tmax);
	    break;
	case IM_ULONG:
	    IMNORM_ROW(im, j, uint32_t, tmin, tmax);
	    break;
	case IM_LONG:
	    IMNORM_ROW(im, j, int32_t, tmin, tmax);
	    break;
	case IM_FLOAT:
	    IMNORM_ROW(im, j, float, tmin, tmax);
	    break;
	case IM_DOUBLE:
	    IMNORM_ROW(im, j, double, tmin, tmax);
	    break;
	default:
	    ip_log_error(ERR_UNSUPPORTED_TYPE);
	    goto exit;
	    break;
	}
    }

 exit:

    ip_pop_routine();
    return ip_error;
}

