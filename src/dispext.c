/* $Id$ */
/*
   Module: ip/dispext.c
   Author: M.J.Cree

   Display extended routines.

   � 1997-2001 Michael Cree 

ENTRY_POINTS:

   ip_wait_for_quit()

*/


#include <stdlib.h>

#include <ip/internal.h>
#include <ip/dispint.h>

static char rcsid[] = "$Id$";


void ip_wait_for_quit(void)
{
   char c;
   int playing;
   int palette;
   double org_gamma, org_contrast;
   int org_palette;

   log_routine("ip_wait_for_quit");

   org_gamma = ip_disp_gamma;
   org_contrast = ip_disp_contrast;
   org_palette = ip_disp_pal;

   c = 0;
   playing = TRUE;
   while (playing) {
      c = display_keypress(FLG_WAIT);
      if (c=='q' || c=='Q' || c=='e' || c=='E' || c=='x' || c=='X') {
	 playing = FALSE;
      }else{

	 palette = ip_disp_pal;

	 switch (c) {
	  case 'g':
	  case 'G':
	    palette = PAL_GREY;
	    break;

	  case 'h':
	  case 'H':
	    palette = PAL_HOTBODY;
	    break;
	    
	  case 'o':
	  case 'O':
	    palette = PAL_GREYOBJ;
	    break;
	 }
	 
	 if (palette >= PAL_START) {
	    switch (c) {

	     case ']':
	     case '}':
	       ip_disp_gamma = 1.0 / ip_disp_gamma;
	       if (ip_disp_gamma < 5)
		  ip_disp_gamma += 0.1;
	       ip_disp_gamma = 1.0 / ip_disp_gamma;
	       break;

	     case '[':
	     case '{':
	       ip_disp_gamma = 1.0 / ip_disp_gamma;
	       if (ip_disp_gamma > 0.15)
		  ip_disp_gamma -= 0.1;
	       ip_disp_gamma = 1.0 / ip_disp_gamma;
	       break;

	     case ',':
	     case '<':
	       if (ip_disp_contrast > 0.15)
		  ip_disp_contrast -= 0.1;
	       break;

	     case '.':
	     case '>':
	       if (ip_disp_contrast < 8)
		  ip_disp_contrast += 0.1;
	       break;

	    }
	    display_palette(palette | PAL_GAMCORR);
	 }
      }
   }

   ip_disp_gamma = org_gamma;
   ip_disp_contrast = org_contrast;
   display_palette(org_palette | PAL_GAMCORR);

   pop_routine();
}
