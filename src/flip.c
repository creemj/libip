/*
   Module: ip/flip.c
   Author: M.J.Cree

   For flipping image rows or columns.

   Copyright (C) 1997, 2001, 2018 by Michael J. Cree

*/


#include <stdlib.h>

#include <ip/ip.h>
#include "internal.h"
#include "function_ops.h"


#define generate_horizontal_flip_function(tt)		\
    static void im_horiz_flip_##tt(ip_image *im)	\
    {							\
	for (int j=0; j<im->size.y; j++) {		\
	    tt##_type *ptr = IM_ROWADR(im, j, v);	\
	    for (int i=0; i<im->size.x/2; i++)	{	\
		tt##_type tmp = ptr[i];			\
		ptr[i] = ptr[im->size.x-i-1];		\
		ptr[im->size.x-i-1] = tmp;		\
	    }						\
	}						\
    }

generate_horizontal_flip_function(UBYTE)
generate_horizontal_flip_function(USHORT)
generate_horizontal_flip_function(ULONG)
generate_horizontal_flip_function(DOUBLE)
generate_horizontal_flip_function(DCOMPLEX)
generate_horizontal_flip_function(RGB)
generate_horizontal_flip_function(RGB16)

improc_function_I im_horiz_flip_bytype[IM_TYPEMAX] = {
    [IM_BINARY] = im_horiz_flip_UBYTE,
    [IM_BYTE] = im_horiz_flip_UBYTE,
    [IM_UBYTE] = im_horiz_flip_UBYTE,
    [IM_SHORT] = im_horiz_flip_USHORT,
    [IM_USHORT] = im_horiz_flip_USHORT,
    [IM_LONG] = im_horiz_flip_ULONG,
    [IM_ULONG] = im_horiz_flip_ULONG,
    [IM_FLOAT] = im_horiz_flip_ULONG,
    [IM_DOUBLE] = im_horiz_flip_DOUBLE,
    [IM_COMPLEX] = im_horiz_flip_DOUBLE,
    [IM_DCOMPLEX] = im_horiz_flip_DCOMPLEX,
    [IM_PALETTE] = im_horiz_flip_UBYTE,
    [IM_RGB] = im_horiz_flip_RGB,
    [IM_RGB16] = im_horiz_flip_RGB16,
    [IM_RGBA] = im_horiz_flip_ULONG
};


/*

NAME:

   im_flip() \- Flip an image.

PROTOTYPE:

   #include <ip/ip.h>

   int  im_flip( im , flag )
   image *im;
   int flag

DESCRIPTION:

   In-place flip the image 'im'.  'Flag' should be FLG_HORIZ to flip the
   horizontally, i.e., the columns of the image about the centre column, and
   FLG_VERT to flip image vertically about the centre row.

ERRORS:

   ERR_BAD_FLAG
   ERR_OUT_OF_MEM

SEE ALSO:

   im_transpose()          im_rot90()

*/

int im_flip(ip_image *im, int flag)
{
    ip_log_routine("im_flip");

    /* Check for correct flags */

    if (! ip_flag_ok(flag, FLG_HORIZ, FLG_VERT, LASTFLAG))
	return ip_error;

    if (!image_valid(im))
	return ip_error;

    if (flag == FLG_VERT) {
	/* For vertical flip can just flip row pointers */
	for (int j=0; j<im->size.y/2; j++) {
	    void *tmp = im->imrow.v[j];
	    im->imrow.v[j] = im->imrow.v[im->size.y-j-1];
	    im->imrow.v[im->size.y-j-1] = tmp;
	}
    }else{
	/* Horizontally flip image */
	if (im_horiz_flip_bytype[im->type])
	    im_horiz_flip_bytype[im->type](im);
	else
	    ip_log_error(ERR_UNSUPPORTED_TYPE);
    }

    ip_pop_routine();
    return OK;
}





/*

NAME:

   im_rot90() \- Rotate an image by 90 degrees

PROTOTYPE:

   #include <ip/ip.h>

   ip_image *  im_rot90( dest, src , flag)
   ip_image *dest;
   ip_image *src;
   int flag

DESCRIPTION:

   Rotate the image 'src' by 90 or -90 degrees.  'Flag' should be set to 90
   for an anticlockwise 90 deg rotation.  'Flag' should be set to -90 for a
   clockwise 90 deg rotation.  These are the only two valid values for
   'flag'.

   If 'dest' is NULL a new image is created for the result and returned.  If
   'dest' is non-null it must be of the same type as 'src' and of transposed
   dimensions.  The result of rotating 'src' is written to 'dest' and it is
   returned.

ERRORS:

   ERR_BAD_FLAG
   ERR_OUT_OF_MEM

SEE ALSO:

   im_flip()          im_transpose()

*/

ip_image * im_rot90(ip_image *rot, ip_image *orig, int flag)
{
    ip_log_routine("im_rot90");

    /* Check for correct flags */

    if (! ip_flag_ok(flag, 90, -90, LASTFLAG)) {
	ip_pop_routine();
	return NULL;
    }

    /* image_valid check is performed by im_transpose/im_flip below. */

    /*
     * We transpose and flip the image vertically to effect the 90 degree
     * rotations.  This should be quite efficient as we have good code for
     * performing the transpose and the vertically flip is extremely
     * efficient since it only involves modifying the image row pointers.
     */

    if (flag == 0 || flag == 90) {
	/* 90 degree anticlockwise rotation */
	if ((rot = im_transpose(rot, orig)) == NULL) {
	    ip_pop_routine();
	    return NULL;
	}
	im_flip(rot, FLG_VERT);
    }else{
	/* 90 degree clockwise rotation */
	if (im_flip(orig, FLG_VERT) == OK) {
	    rot = im_transpose(rot, orig);
	    /* Must put back orig to its original state! */
	    im_flip(orig, FLG_VERT);
	}else
	    rot = NULL;
	ip_free_image(orig);
    }

    ip_pop_routine();
    return rot;
}
