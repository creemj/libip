#include <ip/filter.h>

/*
 * The type field of the ip_kernel object takes these values
 *
 * KERNEL_HORIZONTAL: horizontal filter, use hfilter field.
 * KERNEL_VERTICAL: vertical filter, use vfilter field.
 * KERNEL_SEPARABLE: horizontal followed by vertical filter.
 * KERNEL_2D: use (non-separable) 2d matrix in kernel field.
 *
 * Some special kernels get their own type to avoid having to allocate
 * coeffs:
 *
 * KERNEL_MEAN:  mean filter specified by size parameter only.
 */

#define KERNEL_HORIZONTAL  1
#define KERNEL_VERTICAL    2
#define KERNEL_SEPARABLE   3
#define KERNEL_2D          4
#define KERNEL_MEAN        5
#define KERNEL_MAX_VALID_TYPE 5

/* Can be ORed with 2-pt 1D kernels to indicate a backwards difference */
#define KERNEL_BACKWARDS   0x100
#define KERNEL_OWN_KERNEL  0x200

typedef struct {
    ip_coord2d origin;
    ip_coord2d size;
    ip_image *tbuf;
} ip_tile;

/* Used in flag arg in calling ip_alloc_tile() */
#define TILE_ROWBUF 1
#define TILE_TILEBUF 2

typedef void (*improc_function_Ikt)(ip_image *d, ip_kernel *k, ip_tile *t);
