/*
   Module: ip/morph1d.c
   Author: M.J.Cree

   Copyright (C) 1996, 1997, 1999-2002, 2015, 2017 Michael J. Cree

   Linear morphological operators for use on a line through an image.  These
   routines are primarily intended for internal use within the ip library
   (by morphlin.c and morph.c), however an application programme could make
   use of them to make exotic morphological masks.

   Note that the method used is not entirely invariant to translations
   of the image frame (except for vertical/horizontal structuring elements).

   The arbitrary angled code has been removed (2015) and only horizontal and
   vertical lines are implemented.  Supports only UBYTE and USHORT
   images. See strel.c and morphchord.c for implementation of arbitrary flat
   structuring elements and extension to other image types.

REFERENCE:

   P. Soille, E. J. Breen and R. Jones (1996)
   "Recursive Implementation of Erosions and Dilations Along Discrete
   Lines at Arbitrary Angles"
   IEEE Trans Pat Anal Mach Int v18 562-567

   The above authors also describe a translational invariant method (not
   implemented in this code) in the paper:

   P. Soille and H. Talbot (2001)
   "Directional Morphological Filtering"
   IEEE Trans Pat Anal Mach Int v23 1313-1329


ENTRY POINTS:

  im_morph1d_horiz_erode()
  im_morph1d_horiz_dilate()
  im_morph1d_vert_erode()
  im_morph1d_vert_dilate()

Deprecated and removed:

   alloc_lgh()
   free_lgh()
   ip_dilate1d()
   ip_erode1d()

*/

#include <stdlib.h>

#include <ip/ip.h>
#include <ip/morph.h>

#include "internal.h"
#include "utils.h"
#include "morphint.h"

#ifdef HAVE_SIMD
#include "simd/morph_simd.h"
#endif


#define AND(x,y) ((x) & (y))
#define OR(x,y) ((x) | (y))



static int check_morph1d_parms(ip_image *im, int se_len, int se_origin)
{
    if (NOT image_valid(im))
	return ip_error;

    if (NOT ip_parm_inrange("se_length", se_len, 1, MAX(im->size.x, im->size.y)))
	return ip_error;

    if (NOT ip_parm_inrange("se_origin", se_origin, 0, se_len-1))
	return ip_error;

    if (im->type != IM_BINARY && !type_real(im->type) && !type_integer(im->type)) {
	ip_log_error(ERR_NOT_IMPLEMENTED);
	ip_pop_routine();
	return ip_error;
    }

    return OK;
}


#define generate_m1d_horiz_se2_function(type, opname, op)		\
    static void m1d_horiz_ ## opname ## _se2_ ## type(ip_image *im, int se_origin) \
    {									\
	for (int j=0; j<im->size.y; j++) {				\
	    type ## _type *iptr = IM_ROWADR(im, j, v);			\
	    type ## _type prev, next;					\
									\
	    if (se_origin == 0) {					\
		prev = iptr[0];						\
		for (int i=0; i<im->size.x-1; i++) {			\
		    next = iptr[i+1];					\
		    iptr[i] = op(prev, next);				\
		    prev = next;					\
		}							\
	    }else{							\
		prev = iptr[0];						\
		for (int i=1; i<im->size.x; i++) {			\
		    next = iptr[i];					\
		    iptr[i] = op(prev, next);				\
		    prev = next;					\
		}							\
	    }								\
	}								\
    }

generate_m1d_horiz_se2_function(BINARY, erode, AND)
generate_m1d_horiz_se2_function(BYTE, erode, MIN)
generate_m1d_horiz_se2_function(UBYTE, erode, MIN)
generate_m1d_horiz_se2_function(USHORT, erode, MIN)
generate_m1d_horiz_se2_function(SHORT, erode, MIN)
generate_m1d_horiz_se2_function(ULONG, erode, MIN)
generate_m1d_horiz_se2_function(LONG, erode, MIN)
generate_m1d_horiz_se2_function(FLOAT, erode, MIN)
generate_m1d_horiz_se2_function(DOUBLE, erode, MIN)

generate_m1d_horiz_se2_function(BINARY, dilate, OR)
generate_m1d_horiz_se2_function(BYTE, dilate, MAX)
generate_m1d_horiz_se2_function(UBYTE, dilate, MAX)
generate_m1d_horiz_se2_function(USHORT, dilate, MAX)
generate_m1d_horiz_se2_function(SHORT, dilate, MAX)
generate_m1d_horiz_se2_function(ULONG, dilate, MAX)
generate_m1d_horiz_se2_function(LONG, dilate, MAX)
generate_m1d_horiz_se2_function(FLOAT, dilate, MAX)
generate_m1d_horiz_se2_function(DOUBLE, dilate, MAX)


static improc_morphfun_se2 m1d_horiz_erode_se2_bytype[IM_TYPEMAX] = {
    [IM_BINARY] = m1d_horiz_erode_se2_BINARY,
    [IM_BYTE] = m1d_horiz_erode_se2_BYTE,
    [IM_UBYTE] = m1d_horiz_erode_se2_UBYTE,
    [IM_SHORT] = m1d_horiz_erode_se2_SHORT,
    [IM_USHORT] = m1d_horiz_erode_se2_USHORT,
    [IM_LONG] = m1d_horiz_erode_se2_LONG,
    [IM_ULONG] = m1d_horiz_erode_se2_ULONG,
    [IM_FLOAT] = m1d_horiz_erode_se2_FLOAT,
    [IM_DOUBLE] = m1d_horiz_erode_se2_DOUBLE,
};

static improc_morphfun_se2 m1d_horiz_dilate_se2_bytype[IM_TYPEMAX] = {
    [IM_BINARY] = m1d_horiz_dilate_se2_BINARY,
    [IM_BYTE] = m1d_horiz_dilate_se2_BYTE,
    [IM_UBYTE] = m1d_horiz_dilate_se2_UBYTE,
    [IM_SHORT] = m1d_horiz_dilate_se2_SHORT,
    [IM_USHORT] = m1d_horiz_dilate_se2_USHORT,
    [IM_LONG] = m1d_horiz_dilate_se2_LONG,
    [IM_ULONG] = m1d_horiz_dilate_se2_ULONG,
    [IM_FLOAT] = m1d_horiz_dilate_se2_FLOAT,
    [IM_DOUBLE] = m1d_horiz_dilate_se2_DOUBLE,
};


#define generate_m1d_horiz_function(type, opname, op)			\
    static void m1d_horiz_ ## opname ## _ ## type(ip_image *im, int se_len, int se_origin, ip_image *rb) \
    {									\
	int lofs, rofs;							\
	lofs = se_len - se_origin;					\
	rofs = (se_len-1) - se_origin;					\
									\
	for (int j=0; j<im->size.y; j++) {				\
	    type ## _type *iptr = IM_ROWADR(im, j, v);			\
	    type ## _type *gptr = IM_ROWADR(rb, 0, v);			\
	    type ## _type *hptr = IM_ROWADR(rb, 1, v);			\
	    type ## _type prev = iptr[0];				\
	    int cnt = 0;						\
									\
	    /* Set up right ordered array of minima (maxima) */		\
	    for (int i=0; i<im->size.x; i++) {				\
		if (cnt == se_len) {					\
		    cnt = 0;						\
		    prev = iptr[i];					\
		}							\
		prev = gptr[i] = op(prev, iptr[i]);			\
		cnt++;							\
	    }								\
	    /* Pad past end of line with last value */			\
	    for (int i=im->size.x; i<im->size.x+se_len; i++) {		\
		if (cnt == se_len) {					\
		    cnt = 0;						\
		    gptr[i] = iptr[im->size.x-1];			\
		}else{							\
		    gptr[i] = gptr[i-1];				\
		}							\
		cnt++;							\
	    }								\
									\
	    /* Set up left ordered array of minima (maxima) */		\
	    hptr += se_len;						\
	    prev = hptr[im->size.x-1] = iptr[im->size.x-1];		\
	    cnt = (im->size.x-2) % se_len;				\
	    for (int i=im->size.x-2; i>=0; i--) {			\
		if (cnt == 0) {						\
		    prev = iptr[i];					\
		    cnt = se_len;					\
		}							\
		prev = hptr[i] = op(prev, iptr[i]);			\
		cnt--;							\
	    }								\
	    /* Pad before start of line with first value */		\
	    hptr -= se_len;						\
	    for (int i=0; i<se_len; i++) {				\
		hptr[i] = iptr[0];					\
	    }								\
									\
	    /* Erode (dilate) by taking min (max) of two values in g and h. */ \
	    gptr += rofs;						\
	    hptr += lofs;						\
	    for (int i=0; i<im->size.x; i++) {				\
		iptr[i] = op(gptr[i], hptr[i]);				\
	    }								\
	}								\
    }

generate_m1d_horiz_function(BINARY, erode, AND)
generate_m1d_horiz_function(BYTE, erode, MIN)
generate_m1d_horiz_function(UBYTE, erode, MIN)
generate_m1d_horiz_function(USHORT, erode, MIN)
generate_m1d_horiz_function(SHORT, erode, MIN)
generate_m1d_horiz_function(ULONG, erode, MIN)
generate_m1d_horiz_function(LONG, erode, MIN)
generate_m1d_horiz_function(FLOAT, erode, MIN)
generate_m1d_horiz_function(DOUBLE, erode, MIN)

generate_m1d_horiz_function(BINARY, dilate, OR)
generate_m1d_horiz_function(BYTE, dilate, MAX)
generate_m1d_horiz_function(UBYTE, dilate, MAX)
generate_m1d_horiz_function(USHORT, dilate, MAX)
generate_m1d_horiz_function(SHORT, dilate, MAX)
generate_m1d_horiz_function(ULONG, dilate, MAX)
generate_m1d_horiz_function(LONG, dilate, MAX)
generate_m1d_horiz_function(FLOAT, dilate, MAX)
generate_m1d_horiz_function(DOUBLE, dilate, MAX)


static improc_morphfun_horiz m1d_horiz_erode_bytype[IM_TYPEMAX] = {
    [IM_BINARY] = m1d_horiz_erode_BINARY,
    [IM_BYTE] = m1d_horiz_erode_BYTE,
    [IM_UBYTE] = m1d_horiz_erode_UBYTE,
    [IM_SHORT] = m1d_horiz_erode_SHORT,
    [IM_USHORT] = m1d_horiz_erode_USHORT,
    [IM_LONG] = m1d_horiz_erode_LONG,
    [IM_ULONG] = m1d_horiz_erode_ULONG,
    [IM_FLOAT] = m1d_horiz_erode_FLOAT,
    [IM_DOUBLE] = m1d_horiz_erode_DOUBLE,
};

static improc_morphfun_horiz m1d_horiz_dilate_bytype[IM_TYPEMAX] = {
    [IM_BINARY] = m1d_horiz_dilate_BINARY,
    [IM_BYTE] = m1d_horiz_dilate_BYTE,
    [IM_UBYTE] = m1d_horiz_dilate_UBYTE,
    [IM_SHORT] = m1d_horiz_dilate_SHORT,
    [IM_USHORT] = m1d_horiz_dilate_USHORT,
    [IM_LONG] = m1d_horiz_dilate_LONG,
    [IM_ULONG] = m1d_horiz_dilate_ULONG,
    [IM_FLOAT] = m1d_horiz_dilate_FLOAT,
    [IM_DOUBLE] = m1d_horiz_dilate_DOUBLE,
};



/*
 * Morphological erosion of an image with a flat horizontal line.
 */

int im_morph1d_horiz_erode(ip_image *im, int se_len, int se_origin)
{
    ip_image *rb;
    int row_buff_size;
    int tmp;

    /* Structuring element of length 1 is the identity; nothing to do! */
    if (se_len == 1)
	return OK;

    ip_log_routine(__func__);

    if (check_morph1d_parms(im, se_len, se_origin) != OK)
	return ip_error;

    if (se_len == 2) {
	/* Handle length 2 specially to keep fast; try SIMD implementation first */
#ifdef HAVE_SIMD
	if (IP_USE_HWACCEL && ip_simd_m1d_horiz_se2_erode_bytype[im->type]) {
	    ip_simd_m1d_horiz_se2_erode_bytype[im->type](im, se_origin);
	    ip_pop_routine();
	    return OK;
	}
#endif
	/* Optimised structuring element of length two routines */
	if (m1d_horiz_erode_se2_bytype[im->type]) {
	    m1d_horiz_erode_se2_bytype[im->type](im, se_origin);
	    ip_pop_routine();
	    return ip_error;
	}
    }

    /* Need two row buffers for processing */
    ip_error = OK;
    row_buff_size = im->row_size + 2*PADDED_ROW_SIZE(se_len*ip_type_size(im->type));
    if ((rb = ip_alloc_image(im->type, (ip_coord2d){row_buff_size, 2})) == NULL)
	goto exit_mhe;

#ifdef HAVE_SIMD
    if (IP_USE_HWACCEL && ip_simd_m1d_horiz_erode_bytype[im->type]) {
	ip_simd_m1d_horiz_erode_bytype[im->type](im, se_len, se_origin, rb);
	goto exit_mhe;
    }
#endif

    if (m1d_horiz_erode_bytype[im->type])
	m1d_horiz_erode_bytype[im->type](im, se_len, se_origin, rb);
    else
	ip_log_error(ERR_NOT_IMPLEMENTED);

 exit_mhe:
    tmp = ip_error;
    ip_free_image(rb);

    ip_pop_routine();
    return tmp;
}



/*
 * Morphological dilation of an image with a flat horizontal line.
 */

int im_morph1d_horiz_dilate(ip_image *im, int se_len, int se_origin)
{
    ip_image *rb;
    int row_buff_size;
    int tmp;

    /* Structuring element of length 1 is the identity; nothing to do! */
    if (se_len == 1)
	return OK;

    ip_log_routine(__func__);

    if (check_morph1d_parms(im, se_len, se_origin) != OK)
	return ip_error;

    /* Dilation uses reflected structuring element */
    se_origin = (se_len-1) - se_origin;

    if (se_len == 2) {
	/* Optimised structuring element of length two routines */
#ifdef HAVE_SIMD
	if (IP_USE_HWACCEL && ip_simd_m1d_horiz_se2_dilate_bytype[im->type]){
	    ip_simd_m1d_horiz_se2_dilate_bytype[im->type](im, se_origin);
	    ip_pop_routine();
	    return OK;
	}
#endif

	if (m1d_horiz_dilate_se2_bytype[im->type]) {
	    m1d_horiz_dilate_se2_bytype[im->type](im, se_origin);
	    ip_pop_routine();
	    return OK;
	}
    }

    /*
     * Need two row buffers for processing:
     *    Scalar code needs an extra se_len elements only, whereas,
     *    SIMD code needs an extra se_len elements rounded up to the SIMD
     *    vector size on each end of the row.
     */
    ip_error = OK;
    row_buff_size = PADDED_ROW_SIZE(im->size.x)
	+ 2*PADDED_ROW_SIZE(se_len*ip_type_size(im->type));
    if ((rb = ip_alloc_image(im->type, (ip_coord2d){row_buff_size, 2})) == NULL)
	goto exit_mhd;

#ifdef HAVE_SIMD
    if (IP_USE_HWACCEL && ip_simd_m1d_horiz_dilate_bytype[im->type]) {
	ip_simd_m1d_horiz_dilate_bytype[im->type](im, se_len, se_origin, rb);
	goto exit_mhd;
    }
#endif

    if (m1d_horiz_dilate_bytype[im->type])
	m1d_horiz_dilate_bytype[im->type](im, se_len, se_origin, rb);
    else
	ip_log_error(ERR_NOT_IMPLEMENTED);

 exit_mhd:
    tmp = ip_error;
    ip_free_image(rb);

    ip_pop_routine();
    return tmp;
}


/*
 * Support functions for morphology on vertical lines
 */


#define generate_m1d_vert_se2_function(type, opname, op)		\
    static void m1d_vert_ ## opname ## _se2_ ## type(ip_image *im, int se_origin) \
    {									\
	if (se_origin == 0) {						\
	    for (int j=0; j<im->size.y-1; j++) {			\
		type ## _type *iptr = IM_ROWADR(im, j, v);		\
		type ## _type *iup = IM_ROWADR(im, j+1, v);		\
									\
		for (int i=0; i<im->size.x; i++)			\
		    iptr[i] = op(iptr[i], iup[i]);			\
	    }								\
	}else{								\
	    for (int j=im->size.y-1; j>0; j--) {			\
		type ## _type *iptr = IM_ROWADR(im, j, v);		\
		type ## _type *idown = IM_ROWADR(im, j-1, v);		\
									\
		for (int i=0; i<im->size.x; i++)			\
		    iptr[i] = op(iptr[i], idown[i]);			\
	    }								\
	}								\
    }

generate_m1d_vert_se2_function(BINARY, erode, AND)
generate_m1d_vert_se2_function(BYTE, erode, MIN)
generate_m1d_vert_se2_function(UBYTE, erode, MIN)
generate_m1d_vert_se2_function(USHORT, erode, MIN)
generate_m1d_vert_se2_function(SHORT, erode, MIN)
generate_m1d_vert_se2_function(ULONG, erode, MIN)
generate_m1d_vert_se2_function(LONG, erode, MIN)
generate_m1d_vert_se2_function(FLOAT, erode, MIN)
generate_m1d_vert_se2_function(DOUBLE, erode, MIN)

generate_m1d_vert_se2_function(BINARY, dilate, OR)
generate_m1d_vert_se2_function(BYTE, dilate, MAX)
generate_m1d_vert_se2_function(UBYTE, dilate, MAX)
generate_m1d_vert_se2_function(USHORT, dilate, MAX)
generate_m1d_vert_se2_function(SHORT, dilate, MAX)
generate_m1d_vert_se2_function(ULONG, dilate, MAX)
generate_m1d_vert_se2_function(LONG, dilate, MAX)
generate_m1d_vert_se2_function(FLOAT, dilate, MAX)
generate_m1d_vert_se2_function(DOUBLE, dilate, MAX)

static improc_morphfun_se2 m1d_vert_erode_se2_bytype[IM_TYPEMAX] = {
    [IM_BINARY] = m1d_vert_erode_se2_BINARY,
    [IM_BYTE] = m1d_vert_erode_se2_BYTE,
    [IM_UBYTE] = m1d_vert_erode_se2_UBYTE,
    [IM_SHORT] = m1d_vert_erode_se2_SHORT,
    [IM_USHORT] = m1d_vert_erode_se2_USHORT,
    [IM_LONG] = m1d_vert_erode_se2_LONG,
    [IM_ULONG] = m1d_vert_erode_se2_ULONG,
    [IM_FLOAT] = m1d_vert_erode_se2_FLOAT,
    [IM_DOUBLE] = m1d_vert_erode_se2_DOUBLE,
};

static improc_morphfun_se2 m1d_vert_dilate_se2_bytype[IM_TYPEMAX] = {
    [IM_BINARY] = m1d_vert_dilate_se2_BINARY,
    [IM_BYTE] = m1d_vert_dilate_se2_BYTE,
    [IM_UBYTE] = m1d_vert_dilate_se2_UBYTE,
    [IM_SHORT] = m1d_vert_dilate_se2_SHORT,
    [IM_USHORT] = m1d_vert_dilate_se2_USHORT,
    [IM_LONG] = m1d_vert_dilate_se2_LONG,
    [IM_ULONG] = m1d_vert_dilate_se2_ULONG,
    [IM_FLOAT] = m1d_vert_dilate_se2_FLOAT,
    [IM_DOUBLE] = m1d_vert_dilate_se2_DOUBLE,
};

/*
 * Morphology on vertical lines (i.e. processing down columns of the image)
 * requires some care in implementation to avoid an excessive number of
 * cache misses.
 *
 * We process in blocks of CACHE_LINE_SIZE width so that once we load an
 * element into the cache we process all the elements in the same line of
 * the cache as well.  This requires splitting up the x-loop into two: one
 * over the CACHE_LINE_SIZE blocks and one to process each pixel within the
 * block.  The y-loop is inserted in between these two loops.  This approach
 * requires CACHE_LINE_SIZE times the buffer memory that the horizontal
 * algorithm uses, but it is worth the extra memory overhead to avoid cache
 * misses on just about every pixel processed!
 */

#define NUMBER_OF_ELEMENTS(x, d) (((x) + (d) - 1) / (d))

#define generate_m1d_vert_function(type, opname, op)			\
    static void m1d_vert_ ## opname ## _ ## type(ip_image *im, int se_len, int se_origin, \
						 ip_image *gbuf, ip_image *hbuf)	\
    {									\
	int hofs, gofs, numsteps;					\
									\
	hofs = se_len - se_origin;					\
	gofs = (se_len-1) - se_origin;					\
									\
	/* Process the image in the x direction in steps of CACHE_LINE_SIZE */ \
	numsteps = NUMBER_OF_ELEMENTS(im->row_size, CACHE_LINE_SIZE);	\
	for (int k=0; k<numsteps; k++) {				\
	    int xofs, xsteps;						\
	    int cnt = se_len;						\
	    const int els_per_cline = CACHE_LINE_SIZE / sizeof(type ## _type); \
									\
	    xofs = k * els_per_cline;					\
	    if (k == numsteps-1)					\
		xsteps = NUMBER_OF_ELEMENTS(im->row_size-k*CACHE_LINE_SIZE, sizeof(type ## _type)); \
	    else							\
		xsteps = els_per_cline;					\
									\
	    /* Set up right ordered array of minima */			\
	    for (int j=0; j<im->size.y; j++) {				\
		type ## _type *iptr = IM_ROWADR(im, j, v);		\
		type ## _type *gptr = IM_ROWADR(gbuf, j, v);		\
									\
		iptr += xofs;						\
		if (cnt == se_len) {					\
		    cnt = 0;						\
		    for (int i=0; i<xsteps; i++)			\
			gptr[i] = iptr[i];				\
		}else{							\
		    type ## _type *gup = IM_ROWADR(gbuf, j-1, v);	\
		    for (int i=0; i<xsteps; i++)			\
			gptr[i] = op(gup[i], iptr[i]);			\
		}							\
		cnt++;							\
	    }								\
									\
	    /* Pad past end of line with last value */			\
	    for (int j=im->size.y; j<im->size.y+se_len; j++) {		\
		type ## _type *gptr = IM_ROWADR(gbuf, j, v);		\
									\
		if (cnt == se_len) {					\
		    type ## _type *iptr = IM_ROWADR(im, im->size.y-1, v); \
		    iptr += xofs;					\
		    cnt = 0;						\
		    for (int i=0; i<xsteps; i++)			\
			gptr[i] = iptr[i];				\
		}else{							\
		    type ## _type *gup = IM_ROWADR(gbuf, j-1, v);	\
		    for (int i=0; i<xsteps; i++)			\
			gptr[i] = gup[i];				\
		}							\
		cnt++;							\
	    }								\
									\
	    /* Set up left ordered array of minima */			\
	    {								\
		type ## _type *iptr = IM_ROWADR(im, im->size.y-1, v);	\
		type ## _type *hptr = IM_ROWADR(hbuf, im->size.y-1+se_len, v); \
		iptr += xofs;						\
		for (int i=0; i<xsteps; i++)				\
		    hptr[i] = iptr[i];					\
	    }								\
									\
	    cnt = (im->size.y-2) % se_len;				\
	    for (int j=im->size.y-2; j>=0; j--) {			\
		type ## _type *iptr = IM_ROWADR(im, j, v);		\
		type ## _type *hptr = IM_ROWADR(hbuf, j+se_len, v);	\
									\
		iptr += xofs;						\
		if (cnt == 0) {						\
		    for (int i=0; i<xsteps; i++)			\
			hptr[i] = iptr[i];				\
		    cnt = se_len;					\
		}else{							\
		    type ## _type *hdown = IM_ROWADR(hbuf, j+se_len+1, v); \
		    for (int i=0; i<xsteps; i++)			\
			hptr[i] = op(hdown[i], iptr[i]);		\
		}							\
		cnt--;							\
	    }								\
									\
	    /* Pad before start of line with first value */		\
	    for (int j=0; j<se_len; j++) {				\
		type ## _type *iptr = IM_ROWADR(im, 0, v);		\
		type ## _type *hptr = IM_ROWADR(hbuf, j, v);		\
									\
		iptr += xofs;						\
		for (int i=0; i<xsteps; i++)				\
		    hptr[i] = iptr[i];					\
	    }								\
									\
	    /* Perform the erosion by taking minimum of two values in g and h. */ \
	    for (int j=0; j<im->size.y; j++) {				\
		type ## _type *iptr = IM_ROWADR(im, j, v);		\
		type ## _type *gptr = IM_ROWADR(gbuf, j+gofs, v);	\
		type ## _type *hptr = IM_ROWADR(hbuf, j+hofs, v);	\
									\
		iptr += xofs;						\
		for (int i=0; i<xsteps; i++)				\
		    iptr[i] = op(gptr[i], hptr[i]);			\
	    }								\
	}								\
    }


generate_m1d_vert_function(BINARY, erode, AND)
generate_m1d_vert_function(BYTE, erode, MIN)
generate_m1d_vert_function(UBYTE, erode, MIN)
generate_m1d_vert_function(USHORT, erode, MIN)
generate_m1d_vert_function(SHORT, erode, MIN)
generate_m1d_vert_function(ULONG, erode, MIN)
generate_m1d_vert_function(LONG, erode, MIN)
generate_m1d_vert_function(FLOAT, erode, MIN)
generate_m1d_vert_function(DOUBLE, erode, MIN)

generate_m1d_vert_function(BINARY, dilate, OR)
generate_m1d_vert_function(BYTE, dilate, MAX)
generate_m1d_vert_function(UBYTE, dilate, MAX)
generate_m1d_vert_function(USHORT, dilate, MAX)
generate_m1d_vert_function(SHORT, dilate, MAX)
generate_m1d_vert_function(ULONG, dilate, MAX)
generate_m1d_vert_function(LONG, dilate, MAX)
generate_m1d_vert_function(FLOAT, dilate, MAX)
generate_m1d_vert_function(DOUBLE, dilate, MAX)

static improc_morphfun_vert m1d_vert_erode_bytype[IM_TYPEMAX] = {
    [IM_BINARY] = m1d_vert_erode_BINARY,
    [IM_BYTE] = m1d_vert_erode_BYTE,
    [IM_UBYTE] = m1d_vert_erode_UBYTE,
    [IM_SHORT] = m1d_vert_erode_SHORT,
    [IM_USHORT] = m1d_vert_erode_USHORT,
    [IM_LONG] = m1d_vert_erode_LONG,
    [IM_ULONG] = m1d_vert_erode_ULONG,
    [IM_FLOAT] = m1d_vert_erode_FLOAT,
    [IM_DOUBLE] = m1d_vert_erode_DOUBLE,
};

static improc_morphfun_vert m1d_vert_dilate_bytype[IM_TYPEMAX] = {
    [IM_BINARY] = m1d_vert_dilate_BINARY,
    [IM_BYTE] = m1d_vert_dilate_BYTE,
    [IM_UBYTE] = m1d_vert_dilate_UBYTE,
    [IM_SHORT] = m1d_vert_dilate_SHORT,
    [IM_USHORT] = m1d_vert_dilate_USHORT,
    [IM_LONG] = m1d_vert_dilate_LONG,
    [IM_ULONG] = m1d_vert_dilate_ULONG,
    [IM_FLOAT] = m1d_vert_dilate_FLOAT,
    [IM_DOUBLE] = m1d_vert_dilate_DOUBLE,
};



int im_morph1d_vert_erode(ip_image *im, int se_len, int se_origin)
{
    ip_image *gbuf, *hbuf;
    int tmp;

    /* Structuring element of length 1 is the identity; nothing to do! */
    if (se_len == 1)
	return OK;

    ip_log_routine(__func__);

    if (check_morph1d_parms(im, se_len, se_origin) != OK)
	return ip_error;

    if (se_len == 2) {
	/* Handle length 2 specially to keep fast; try SIMD implementation first */
#ifdef HAVE_SIMD
	if (IP_USE_HWACCEL && ip_simd_m1d_vert_se2_erode_bytype[im->type]) {
	    ip_simd_m1d_vert_se2_erode_bytype[im->type](im, se_origin);
	    ip_pop_routine();
	    return OK;
	}
#endif
	/* No SIMD implementation; do pixelwise */
	if (m1d_vert_erode_se2_bytype[im->type]) {
	    m1d_vert_erode_se2_bytype[im->type](im, se_origin);
	    ip_pop_routine();
	    return ip_error;
	}

	ip_pop_routine();
	return OK;
    }

    /*
     * Need two buffers of the column size by CACHE_LINE_SIZE.  We do this
     * rather than a column size by 1 buffer to better exploit the streaming
     * nature of modern memory subsystems.
     */
    gbuf = hbuf = NULL;
    ip_error = OK;
    gbuf = ip_alloc_image(im->type, (ip_coord2d){CACHE_LINE_SIZE, im->size.y+se_len});
    hbuf = ip_alloc_image(im->type, (ip_coord2d){CACHE_LINE_SIZE, im->size.y+se_len});
    if (gbuf == NULL || hbuf == NULL)
	goto exit_mve;

#ifdef HAVE_SIMD
    if (IP_USE_HWACCEL && ip_simd_m1d_vert_erode_bytype[im->type]) {
	ip_simd_m1d_vert_erode_bytype[im->type](im, se_len, se_origin, gbuf, hbuf);
	goto exit_mve;
    }
#endif

    if (m1d_vert_erode_bytype[im->type])
	m1d_vert_erode_bytype[im->type](im, se_len, se_origin, gbuf, hbuf);
    else
	ip_log_error(ERR_NOT_IMPLEMENTED);

 exit_mve:
    tmp = ip_error;
    ip_free_image(gbuf);
    ip_free_image(hbuf);

    ip_pop_routine();
    return tmp;
}



int im_morph1d_vert_dilate(ip_image *im, int se_len, int se_origin)
{
    ip_image *gbuf, *hbuf;
    int tmp;

    /* Structuring element of length 1 is the identity; nothing to do! */
    if (se_len == 1)
	return OK;

    ip_log_routine(__func__);

    if (check_morph1d_parms(im, se_len, se_origin) != OK)
	return ip_error;

    /* Dilation uses reflected structuring element */
    se_origin = (se_len-1) - se_origin;

    if (se_len == 2) {
	/* Handle length 2 specially to keep fast; try SIMD implementation first */

#ifdef HAVE_SIMD
	if (IP_USE_HWACCEL && ip_simd_m1d_vert_se2_dilate_bytype[im->type]) {
	    ip_simd_m1d_vert_se2_dilate_bytype[im->type](im, se_origin);
	    ip_pop_routine();
	    return OK;
	}
#endif
	/* No SIMD implementation; do pixelwise */
	if (m1d_vert_dilate_se2_bytype[im->type]) {
	    m1d_vert_dilate_se2_bytype[im->type](im, se_origin);
	    ip_pop_routine();
	    return ip_error;
	}
    }

    /*
     * Need two buffers of the column size by CACHE_LINE_SIZE.  We do this
     * rather than a column size by 1 buffer to better exploit the streaming
     * nature of modern memory subsystems.
     */
    ip_error = OK;
    gbuf = hbuf = NULL;
    gbuf = ip_alloc_image(im->type, (ip_coord2d){CACHE_LINE_SIZE, im->size.y+se_len});
    hbuf = ip_alloc_image(im->type, (ip_coord2d){CACHE_LINE_SIZE, im->size.y+se_len});
    if (gbuf == NULL || hbuf == NULL)
	goto exit_mvd;

#ifdef HAVE_SIMD
    if (IP_USE_HWACCEL && ip_simd_m1d_vert_dilate_bytype[im->type]) {
	ip_simd_m1d_vert_dilate_bytype[im->type](im, se_len, se_origin, gbuf, hbuf);
	goto exit_mvd;
    }
#endif

    if (m1d_vert_dilate_bytype[im->type])
	m1d_vert_dilate_bytype[im->type](im, se_len, se_origin, gbuf, hbuf);
    else
	ip_log_error(ERR_NOT_IMPLEMENTED);

 exit_mvd:
    tmp = ip_error;
    ip_free_image(gbuf);
    ip_free_image(hbuf);

    ip_pop_routine();
    return tmp;
}
