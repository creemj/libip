/*
   Module: ip/threshold.c
   Author: M. J. Cree

   Copyright (C) 1995-1997, 1999, 2008-2009, 2013-2015 Michael J. Cree

   Routines for thresholding images.

ENTRY POINTS:
   im_threshold()
   im_bthresh()

*/

#include <stdlib.h>
#include <math.h>

#include <ip/ip.h>

#include "internal.h"
#include "function_ops.h"

#ifdef HAVE_SIMD
#include "simd/threshold_simd.h"
#endif


#define generate_image_threshold_function(t, rnd)			\
    static void								\
    im_threshold_ ## t(ip_image *d, ip_image *s,			\
		       double low, double high, uint8_t tv)		\
    {									\
	t ## _type llow = (t ## _type)rnd(low);				\
	t ## _type lhigh = (t ## _type)rnd(high);			\
	uint8_t fv = ~tv;						\
	for (int j=0; j<d->size.y; j++) {				\
	    UBYTE_type *dp = IM_ROWADR(d, j, v);			\
	    t ## _type *sp = IM_ROWADR(s, j, v);			\
	    for (int i=0; i<d->size.x; i++) {				\
		*dp = ((llow <= *sp) && (*sp <= lhigh)) ? tv : fv;	\
		dp++; sp++;						\
	    }								\
	}								\
    }

generate_image_threshold_function(UBYTE, IROUND);
generate_image_threshold_function(BYTE, IROUND);
generate_image_threshold_function(USHORT, IROUND);
generate_image_threshold_function(SHORT, IROUND);
generate_image_threshold_function(ULONG, LROUND);
generate_image_threshold_function(LONG, LROUND);

#define ASIS(x) x
generate_image_threshold_function(FLOAT, ASIS);
generate_image_threshold_function(DOUBLE, ASIS);

static improc_function_IIccb im_threshold_by_type[IM_TYPEMAX] = {
    [IM_BYTE] = im_threshold_BYTE,
    [IM_UBYTE] = im_threshold_UBYTE,
    [IM_SHORT] = im_threshold_SHORT,
    [IM_USHORT] = im_threshold_USHORT,
    [IM_LONG] = im_threshold_LONG,
    [IM_ULONG] = im_threshold_ULONG,
    [IM_FLOAT] = im_threshold_FLOAT,
    [IM_DOUBLE] = im_threshold_DOUBLE
};


/*

NAME:

   im_threshold() \- Threshold an image.

PROTOTYPE:

   #include <ip/ip.h>

   ip_image * im_threshold( src, low, high )
   ip_image *src;
   double low;
   double high;

DESCRIPTION:

   Threshold image 'src' at values between 'low' and 'high' inclusive and
   return BINARY image of result (with values between 'low' and 'high'
   returned as BINARY TRUE).  That is, if l<=p and p<=h then result pixel
   set true else result pixel set false.  (Here p is source pixel value, l
   is low threshold and h is high threshold.)

   If 'high' is less than 'low' then 'high' and 'low' are swapped and
   resultant BINARY image is inverted.  That is, all values between
   'low' and 'high' inclusive are returned as BINARY FALSE.

   For integer image types 'high' and 'low' are rounded to the nearest
   integer before thresholding image.

ERRORS:

   ERR_OUT_OF_MEMORY
   ERR_BAD_TYPE
   ERR_PARM_BAD_VALUE

*/

ip_image *im_threshold(ip_image *src, double low, double high)
{
    ip_image *im;
    uint8_t tv;

    ip_log_routine(__func__);

    /* Check have valid image to work with */

    if (NOT image_valid(src))
	return NULL;

    if (NOT (image_type_integer(src) || image_type_real(src))) {
	ip_log_error(ERR_BAD_TYPE);
	ip_pop_routine();
	return NULL;
    }

    if (NOT ip_value_in_image("low", low, src))
	return NULL;

    if (NOT ip_value_in_image("high", high, src))
	return NULL;

    tv = 255;

    if (low > high) {
	double tmp = high;
	high = low;
	low = tmp;
	tv = 0;
    }

    if ((im = ip_alloc_image(IM_BINARY, src->size)) == NULL) {
	ip_pop_routine();
	return NULL;
    }

#ifdef HAVE_SIMD
    if (IP_USE_HWACCEL && ip_simd_threshold_bytype[src->type]) {
	ip_simd_threshold_bytype[src->type](im, src, low, high, tv);
	ip_pop_routine();
	return im;
    }
#endif

    if (im_threshold_by_type[src->type])
	im_threshold_by_type[src->type](im, src, low, high, tv);
    else
	ip_log_error(ERR_NOT_IMPLEMENTED);

    ip_pop_routine();
    return im;
}




/*

  NAME:

  im_bthresh() \- Threshold a UBYTE image in place

  PROTOTYPE:

  #include <ip/ip.h>

  int  im_bthresh( im, lower, higher )
  ip_image *im;
  int lower;
  int higher;

  DESCRIPTION:

  OBSOLETE - Don''t use :-)

  ERRORS:

      ERR_BAD_TYPE

*/


int im_bthresh(ip_image *im, int lower, int higher)
{
    uint8_t trueval, falseval;

    ip_log_routine(__func__);

    /* Check have valid image to work with */

    if (im->type != IM_UBYTE) {
	ip_log_error(ERR_BAD_TYPE);
    }else{

	trueval = 255;
	falseval = 0;

	if (lower > higher) {
	    int i = higher;
	    higher = lower;
	    lower = i;
	    trueval = 0;
	    falseval = 255;
	}

	/* Calculate threshold */
      
	for (int j=0; j<im->size.y; j++) {
	    uint8_t *sp = IM_ROWADR(im, j, ub);

	    for (int i=0; i<im->size.x; i++) {
		*sp = (lower <= *sp && *sp <= higher) ? trueval : falseval;
		sp++;
	    }
	}
    }

    ip_pop_routine();
    return ip_error;
}


