/*

   Module: ip/median.c
   Author: M.J.Cree

   Copyright (C) 1995-1997, 2018-2019 by Michael J. Cree

   The standard 2D Median filter.

*/

#include <stdlib.h>
#include <string.h>

#include <ip/ip.h>
#include <ip/hist.h>

#include "internal.h"
#include "utils.h"
#include "function_ops.h"

#ifdef HAVE_SIMD
#include "simd/median_simd.h"
#endif


/*
 * Finding the median on 8-bit and 16-bit image data is best performed by
 * forming a histogram of the kernel. The histgram is updated efficiently
 * when the kernel is shifted along the row, and the new median can be found
 * quickly by searching from the old median.  This leads to an O(k) per
 * pixel algorithm where k^2 is the size of the kernel.
 *
 * Perrault and Hebert (2007) describe an O(1) per pixel algorithm but it is
 * very memory intensive with a histogram per column of the image thus we
 * don't implement it.
 *
 * For 32-bit and larger data the size of the histogram is prohibitive for
 * two reasons: it uses far too much memory and searching for the new median
 * becomes inefficient. We therefore full back to searching for the median
 * in the kernel by the quickselect algorithm.  This is O(k^2) per pixel
 * and can be extremely slow for large kernels.
 *
 * The SIMD implementation, based on that described by Kolte, Smith and Su
 * (1999), currently only supports a 3x3 sized kernel for all scalar image
 * data types.  The algorithm is extremely fast at 50 to 300 times faster
 * (dependent on architecture and image data) than the scalar histogram
 * implementation.
 */

/*
 * A structure used in the histogram method to hold the histogram and extra
 * information for finding the median efficiently.
 */

struct hist_med {
    ip_hist *h;			/* histogram of kernel values */
    int ledge;			/* Acc count at left edge of median bin */
    int medbin;			/* Median bin value */
    int medcnt;			/* Acc count required for median */
};


/* Typedef of functions using histogram to implement median filter */
typedef void (*improc_function_IIhif)(ip_image *d, ip_image *s, struct hist_med *hm, int i, int f);

/* Typedef of functions using quickselect to implement median filter */
typedef void (*improc_function_IIpi)(ip_image *d, ip_image *s, void *v, int i);
typedef void (*improc_function_IIpif)(ip_image *d, ip_image *s, void *v, int i, int f);


/*
 * 8-bit data: use an intensity histogram.  (The overhead of maintaining a
 * layered histogram outweighs any advantage it gives in searching for the
 * new median.)
 */

static inline void hmed_insert_u8_value(struct hist_med *hm, int value)
{
    ip_hist_bin_inc(hm->h, 0, value);
    hm->ledge += value < hm->medbin;
}

static inline void hmed_remove_u8_value(struct hist_med *hm, int value)
{
    ip_hist_bin_dec(hm->h, 0, value);
    hm->ledge -= value < hm->medbin;
}

static inline void hmed_insert_u8_values(struct hist_med *hm, int value, int num)
{
    ip_hist_bin_add(hm->h, 0, value, num);
    hm->ledge += (value < hm->medbin) ? num : 0;
}

static inline void hmed_remove_u8_values(struct hist_med *hm, int value, int num)
{
    ip_hist_bin_sub(hm->h, 0, value, num);
    hm->ledge -= (value < hm->medbin) ? num : 0;
}

static inline int hmed_update_u8_median(struct hist_med *hm, int x, int y)
{
    while (hm->ledge >= hm->medcnt) {
	/* Median is now smaller */
	hm->ledge -= ip_hist_bin_count(hm->h, 0, --hm->medbin);
    }
    while ((hm->ledge + ip_hist_bin_count(hm->h, 0, hm->medbin)) < hm->medcnt) {
	/* Median is now bigger */
	hm->ledge += ip_hist_bin_count(hm->h, 0, hm->medbin++);
    }

    return hm->medbin;
}



/*
 * 16-bit data: use a layered histogram with the coarse layered indexed by
 * the high byte of the 16-bit datum.  This enables a more efficient search
 * for the median.
 */

static inline void hmed_insert_u16_value(struct hist_med *hm, int value)
{
    ip_hist_lu16_bin_inc(hm->h, value);
    hm->ledge += value < hm->medbin;
}

static inline void hmed_remove_u16_value(struct hist_med *hm, int value)
{
    ip_hist_lu16_bin_dec(hm->h, value);
    hm->ledge -= value < hm->medbin;
}

static inline void hmed_insert_u16_values(struct hist_med *hm, int value, int num)
{
    ip_hist_lu16_bin_add(hm->h, value, num);
    hm->ledge += (value < hm->medbin) ? num : 0;
}

static inline void hmed_remove_u16_values(struct hist_med *hm, int value, int num)
{
    ip_hist_lu16_bin_sub(hm->h, value, num);
    hm->ledge -= (value < hm->medbin) ? num : 0;
}

/* TBD this could be better optimised to make more use of coarse bins */
static inline int hmed_update_u16_median(struct hist_med *hm, int x, int y)
{
    while (hm->ledge >= hm->medcnt) {
	/* Median is now smaller; search backwards for it. */
	hm->medbin = ip_hist_lu16_bin_prev(hm->h, hm->medbin-1);
	hm->ledge -= ip_hist_lu16_bin_count(hm->h, hm->medbin);
    }
    while ((hm->ledge + ip_hist_lu16_bin_count(hm->h, hm->medbin)) < hm->medcnt) {
	/* Median is now bigger; search forwards for it. */
	hm->ledge += ip_hist_lu16_bin_count(hm->h, hm->medbin);
	hm->medbin = ip_hist_lu16_bin_next(hm->h, hm->medbin+1);
    }

    return hm->medbin;
}


/*
 * Median filter an image by using the histogram approach.  The histogram
 * approach here only supports the flags FLG_ZEROEXTEND and
 * FLG_EDGEEXTEND.
 *
 * One can get a 10% performance increase for larger kernel sizes (width >
 * ~15) by splitting the loop over i (the columns) up into three parts;
 * first process the kernel is off the left edge of the image, process the
 * row where the kernel is completely in the image, and then process the
 * final piece where the kernel extends off the right edge of the image.
 * The speed-up is achieved by removing a number of if statements from the
 * central loop.  This is not implemented below because I prefer the more
 * compact nature of the code.
 */

#define generate_process_image_medhist(tt, histt, hofs)			\
    static void process_image_medhist_##tt(ip_image *dest, ip_image *im, struct hist_med *hm, int width, int flag) \
    {									\
	int hwidth = width / 2;						\
	hm->medcnt = (width*width+1)/2;					\
	if (hofs)							\
	    ip_hist_set_bin_offset(hm->h, 0, hofs);			\
	for (int j=0; j<im->size.y; j++) {				\
	    tt##_type *trow = IM_ROWADR(dest, j, v);			\
	    /* Initalise histogram at start of each row */		\
	    hm->ledge = 0;						\
	    hm->medbin = hofs;						\
	    ip_hist_reset(hm->h);					\
	    for (int l=-hwidth; l<=hwidth; l++) {			\
		tt##_type *irow;					\
		int row = j + l;					\
		if (row < 0)						\
		    irow = (flag == FLG_ZEROEXTEND) ? im->extra_rows : IM_ROWADR(im, 0, v); \
		else if (row >= im->size.y)				\
		    irow = (flag == FLG_ZEROEXTEND) ? im->extra_rows : IM_ROWADR(im, im->size.y-1, v); \
		else							\
		    irow = IM_ROWADR(im, row, v);			\
		hmed_insert_##histt##_values(hm, (flag == FLG_ZEROEXTEND) ? 0 : irow[0], hwidth); \
		for (int k=0; k<=hwidth; k++) {				\
		    hmed_insert_##histt##_value(hm, irow[k]);		\
		}							\
	    }								\
	    /* Get median value of histogram */				\
	    trow[0] = hmed_update_##histt##_median(hm, hwidth, j);	\
	    /* Process along row with efficient update of median */	\
	    for (int i=1; i<im->size.x; i++) {				\
		for (int l = -hwidth; l<=hwidth; l++) {			\
		    tt##_type *irow;					\
		    int row = j + l;					\
		    if (row < 0)					\
			irow = (flag == FLG_ZEROEXTEND) ? im->extra_rows : IM_ROWADR(im, 0, v);	\
		    else if (row >= im->size.y)				\
			irow = (flag == FLG_ZEROEXTEND) ? im->extra_rows : IM_ROWADR(im, im->size.y-1, v); \
		    else						\
			irow = IM_ROWADR(im, row, v);			\
                    int left = i-hwidth-1;				\
                    int right = i+hwidth;				\
                    hmed_remove_##histt##_value(hm, ((left>=0) ? irow[left] : ((flag==FLG_ZEROEXTEND) ? 0 : irow[0]))); \
                    hmed_insert_##histt##_value(hm, ((right < im->size.x) ? irow[right] : ((flag==FLG_ZEROEXTEND) ? 0 : irow[im->size.x-1]))); \
		}							\
		trow[i] = hmed_update_##histt##_median(hm, i, j);	\
	    }								\
	}								\
    }

generate_process_image_medhist(BYTE, u8, -128)
generate_process_image_medhist(UBYTE, u8, 0)
generate_process_image_medhist(SHORT, u16, -32768)
generate_process_image_medhist(USHORT, u16, 0)

improc_function_IIhif process_image_medhist_bytype[IM_TYPEMAX] = {
    [IM_BYTE] = process_image_medhist_BYTE,
    [IM_UBYTE] = process_image_medhist_UBYTE,
    [IM_SHORT] = process_image_medhist_SHORT,
    [IM_USHORT] = process_image_medhist_USHORT
};



/*

NAME:

   im_median() \- Median filter an image

PROTOTYPE:

   #include <ip/ip.h>

   int  im_median( im, width, flag )
   image *im;
   int width;
   int flag;

DESCRIPTION:

   Median filter an image with 'width'*'width' kernel.  'Width' currently
   must be odd amd smaller than any linear dimensional of the image.  By
   default image edges where the kernel is partly off the image is processed
   with the flag FLG_EDGEEXTEND described below is active. This can be
   modified with the use of 'flag', which, in addition to the default
   NO_FLAG, may be:

\&      FLG_ZEROEXTEND Pad image boundaries with zeros.

\&      FLG_EDGEEXTEND Pad image boundaries with closest edge pixel.

   Different algorithms are used depending on the bit size of the image
   data.  For 8-bit and 16-bit image data an efficient histogram based
   method is used. For 32-bit or larger data the histogram method is no
   longer practical and a quickselect search for the median is performed
   over the kernel at each pixel location, and this can get extrememly slow
   for larger kernels.

LIMITATIONS:

\-   The kernel mask size must be odd.
\-   The median filter is only implemented on scalar images.

ERRORS:

   ERR_UNSUPPORTED_TYPE
   ERR_BAD_PARAMETER
   ERR_BAD_FLAG
   ERR_OUT_OF_MEM

*/



/*
 * Quickselect algorithm to find the median of the array arr of specified
 * size.  This is a little faster than a full quicksort.
 *
 * This is only needed for larger scalar types (32-bit and bigger).
 */

#define generate_median(tt)						\
    static tt##_type median_##tt(tt##_type *arr, int size)		\
    {									\
	int  left, right, k;						\
	left  = 0;							\
	right = size - 1;						\
	k = (right+1)/2;						\
									\
	while (right > left) {						\
	    int i, j;							\
	    tt##_type t, val;						\
									\
	    if (arr[right] < arr[left]) {				\
		t = arr[right];						\
		arr[right] = arr[left];					\
		arr[left] = t;						\
	    }								\
									\
	    val = arr[right];						\
	    i = left - 1;						\
	    j = right;							\
	    do {							\
		do i++; while (arr[i] < val);				\
		do j--; while (arr[j] > val);				\
		t = arr[i];						\
		arr[i] = arr[j];					\
		arr[j] = t;						\
	    } while (j > i);						\
									\
	    arr[j] = arr[i];						\
	    arr[i] = arr[right];					\
	    arr[right] = t;						\
									\
	    if (i >= k) right = i - 1;					\
	    if (i <= k) left  = i + 1;					\
									\
	}								\
	return arr[k];							\
    }

generate_median(LONG)
generate_median(ULONG)
generate_median(FLOAT)
generate_median(DOUBLE)


/*
 * Process the body of the image, that is, that part for which the median
 * kernel completely fits inside the image, with the median quickselect
 * algorithm.
 */

#define generate_process_body_median(tt)				\
    static void process_body_median_##tt(ip_image *res, ip_image *im, void *arr, int width) \
    {									\
	const int hwidth = width/2;					\
	const int arrsize = width*width;				\
	for (int j=hwidth; j<im->size.y-hwidth; j++) {			\
	    tt##_type *rptr = IM_ROWADR(res, j, v);			\
	    for (int i=hwidth; i<im->size.x-hwidth; i++) {		\
		tt##_type *ap = arr;					\
		for (int l=-hwidth; l<=hwidth; l++) {			\
		    tt##_type *irow = IM_ROWADR(im, j+l, v);		\
		    irow += i;						\
		    for (int k=-hwidth; k<=hwidth; k++) {		\
			*ap++ = irow[k];				\
		    }							\
		}							\
		rptr[i] = median_##tt(arr, arrsize);			\
	    }								\
	}								\
    }

generate_process_body_median(LONG)
generate_process_body_median(ULONG)
generate_process_body_median(FLOAT)
generate_process_body_median(DOUBLE)


/*
 * Process the edges of the image where the kernel only partly overlaps the
 * edge.  First the helper function fill_arr_X() which fills the array used
 * for sorting with the correct image pixels padded according to the flag.
 */

#define generate_fill_array(nn, tt, bb)					\
    static int fill_arr_##nn(tt *arr, ip_image *im, int i, int j, int hwidth, int flag)	\
    {									\
	const int width = 2*hwidth + 1;					\
	int asize = width*width;					\
									\
	/* Determine size of array so can't go off edge of image */	\
									\
	const int xstart = (hwidth <= i) ? -hwidth :  -i;		\
	const int xend = (hwidth <= im->size.x-i-1) ? hwidth : im->size.x-i-1; \
	const int xsize = xend - xstart + 1;				\
	const int ystart = (hwidth <= j) ? -hwidth : -j;		\
	const int yend = (hwidth <= im->size.y-j-1) ? hwidth : im->size.y-j-1; \
	const int ysize = yend - ystart + 1;				\
									\
	/* Insert zeros at start of array if requested */		\
									\
	if (flag == FLG_ZEROEXTEND) {					\
	    int Nzeros = asize - xsize*ysize;				\
	    memset(arr, 0, Nzeros * sizeof(tt));			\
	    arr += Nzeros;						\
	}								\
									\
	/* Fill array from pixels in image overlapping kernel */	\
									\
	for (int l=ystart; l<=yend; l++) {				\
	    tt *iptr = IM_ROWADR(im, j+l, v);				\
	    iptr += i;							\
	    for (int k=xstart; k<=xend; k++)				\
		*arr++ = iptr[k];					\
	}								\
									\
	/* Insert extra edge pixels if requested */			\
									\
	if (flag != FLG_ZEROEXTEND) {					\
	    const int left = (i < hwidth) ? (hwidth - i) : 0;		\
	    const int right = ((im->size.x-1 - i) < hwidth) ? (hwidth - (im->size.x-1-i)) : 0; \
	    const int top = (j < hwidth) ? (hwidth - j) : 0;		\
	    const int bottom = ((im->size.y-1 - j) < hwidth) ? (hwidth - (im->size.y-1-j)) : 0;	\
	    if (top > 0) {						\
		tt *iptr = IM_ROWADR(im, 0, v);				\
		for (int k=xstart; k<=xend; k++) {			\
		    for (int n=0; n<top; n++)				\
			*arr++ = iptr[k+i];				\
		}							\
		if (left > 0) {						\
		    for (int n=0; n<top*left; n++)			\
			*arr++ = iptr[0];				\
		}							\
		if (right > 0) {					\
		    for (int n=0; n<top*right; n++)			\
			*arr++ = iptr[im->size.x-1];			\
		}							\
	    }								\
	    if (bottom > 0) {						\
		tt *iptr = IM_ROWADR(im, im->size.y-1, v);		\
		for (int k=xstart; k<=xend; k++)			\
		    for (int n=0; n<bottom; n++)			\
			*arr++ = iptr[k+i];				\
		if (left > 0) {						\
		    for (int n=0; n<bottom*left; n++)			\
			*arr++ = iptr[0];				\
		}							\
									\
		if (right > 0) {					\
		    for (int n=0; n<bottom*right; n++)			\
			*arr++ = iptr[im->size.x-1];			\
		}							\
	    }								\
	    if (left > 0) {						\
		for (int k=ystart; k<=yend; k++) {			\
		    tt val = IM_PIXEL(im, 0, k+j, bb);			\
		    for (int n=0; n<left; n++)				\
			*arr++ = val;					\
		}							\
	    }								\
	    if (right > 0) {						\
		for (int k=ystart; k<=yend; k++) {			\
		    tt val = IM_PIXEL(im, im->size.x-1, k+j, bb);	\
		    for (int n=0; n<right; n++)				\
			*arr++ = val;					\
		}							\
	    }								\
	}								\
									\
	return asize;							\
    }									\

generate_fill_array(32, int32_t, l)
generate_fill_array(64, double, d)

#define generate_process_edge_median(tt, nn)				\
    static void process_edge_median_##tt(ip_image *res, ip_image *im, void *arr, int width, int flag) \
    {									\
	const int hwidth = width/2;					\
	/* Do top and bottom borders */					\
	for (int j=0; j<hwidth; j++) {					\
	    tt##_type *trow = IM_ROWADR(res, j, v);			\
	    tt##_type *brow = IM_ROWADR(res, im->size.y-j-1, v);	\
	    for (int i=0; i<im->size.x; i++) {				\
		int size;						\
		size = fill_arr_##nn(arr, im, i, j, hwidth, flag);	\
		trow[i] = median_##tt(arr, size);			\
		size = fill_arr_##nn(arr, im, i, im->size.y-1-j, hwidth, flag); \
		brow[i] = median_##tt(arr, size);			\
	    }								\
	}								\
	/* Do left and right borders */					\
	for (int j=hwidth; j<im->size.y-hwidth; j++) {			\
	    tt##_type *row = IM_ROWADR(res, j, v);			\
	    for (int i=0; i<hwidth; i++) {				\
		int size;						\
		/* Left border */					\
		size = fill_arr_##nn(arr, im, i, j, hwidth, flag);	\
		row[i] = median_##tt(arr, size);			\
		/* Right border */					\
		size = fill_arr_##nn(arr, im, im->size.x-i-1, j, hwidth, flag); \
		row[im->size.x-i-1] = median_##tt(arr, size);		\
	    }								\
	}								\
    }

generate_process_edge_median(LONG, 32)
generate_process_edge_median(ULONG, 32)
generate_process_edge_median(FLOAT, 32)
generate_process_edge_median(DOUBLE, 64)

improc_function_IIpi process_body_median_bytype[IM_TYPEMAX] = {
    [IM_LONG] = process_body_median_LONG,
    [IM_ULONG] = process_body_median_ULONG,
    [IM_FLOAT] = process_body_median_FLOAT,
    [IM_DOUBLE] = process_body_median_DOUBLE
};

improc_function_IIpif process_edge_median_bytype[IM_TYPEMAX] = {
    [IM_LONG] = process_edge_median_LONG,
    [IM_ULONG] = process_edge_median_ULONG,
    [IM_FLOAT] = process_edge_median_FLOAT,
    [IM_DOUBLE] = process_edge_median_DOUBLE
};



int im_median( ip_image *im, int width, int flag )
{
    ip_image *tmp;

    ip_log_routine(__func__);

    if (!type_scalar(im->type) || im->type == IM_BINARY) {
	ip_log_error(ERR_UNSUPPORTED_TYPE);
	goto exit;
    }

    if (NOT ip_flag_ok(flag, FLG_ZEROEXTEND, FLG_EDGEEXTEND, LASTFLAG))
	return ip_error;

    if (NOT ip_parm_greatereq("width", width, 3))
	return ip_error;

    if (NOT ip_parm_odd("width", width))
	return ip_error;

    /*
     * We don't support kernels larger than the image!
     * This also gives the guarantee that there are indeed hwidth worth of
     * pixels on each border of the image to process specially so no need
     * for checks for fewer than hwidth pixels in border region below.
     */
    if (NOT ip_parm_lesseq("width", width, MIN(im->size.x, im->size.y)))
	return ip_error;

    if ((tmp = ip_alloc_image(im->type, im->size)) == NULL)
	goto exit;

    if (flag == FLG_ZEROEXTEND) {
	/*
	 * Set up an extra row of zeros to use as edge pixels which some
	 * algorithms use to effect the zero boundary enclosing the image.
	 */
	if (NOT ip_im_alloc_extra_rows(im, 1)) {
	    int error = ip_error;
	    ip_free_image(tmp);
	    ip_pop_routine();
	    return error;
	}
	memset(im->extra_rows, 0, im->row_size);
    }

#ifdef HAVE_SIMD
    if (IP_USE_HWACCEL && im->size.x > SIMD_VECTOR_SIZE) {
	improc_function_IIf f = NULL;

	if (width == 3 && ip_simd_median_3x3_bytype[im->type])
	    f = ip_simd_median_3x3_bytype[im->type];
	if (width == 5 && ip_simd_median_5x5_bytype[im->type])
	    f = ip_simd_median_5x5_bytype[im->type];

	if (f) {
	    f(tmp, im, flag);
	    im_switch(im, tmp);
	    ip_free_image(tmp);
	    ip_pop_routine();
	    return OK;
	}
    }
#endif

    if (ip_type_size(im->type) >= 4) {
	void *arr;

	/* Use brute-force quickselect sort of the kernel */
	if ((arr = ip_malloc(ip_type_size(im->type) * width * width)) == NULL) {
	    ip_free_image(tmp);
	    ip_pop_routine();
	    return ip_error;
	}

	if (process_body_median_bytype[im->type]) {
	    process_body_median_bytype[im->type](tmp, im, arr, width);
	    process_edge_median_bytype[im->type](tmp, im, arr, width, flag);
	}else{
	    ip_free(arr);
	    ip_free_image(tmp);
	    ip_log_error(ERR_NOT_IMPLEMENTED);
	    return ip_error;
	}

	ip_free(arr);

    }else{
	/* Histogram approach */
	struct hist_med hm;
	struct ip_hist_parts_spec ubyte_hps = {0, 256, 256};
	struct ip_hist_parts_spec ushort_hps = {0, 65536, 256};

	if (im->type == IM_UBYTE || im->type == IM_BYTE) {
	    hm.h = ip_alloc_hist(HIST_INTENSITY, 1, &ubyte_hps);
	}else{
	    hm.h = ip_alloc_hist(HIST_LAYERED, 1, &ushort_hps);
	}

	if (process_image_medhist_bytype[im->type])
	    process_image_medhist_bytype[im->type](tmp, im, &hm, width, flag);
	else{
	    ip_free_hist(hm.h);
	    ip_free_image(tmp);
	    ip_log_error(ERR_NOT_IMPLEMENTED);
	    return ip_error;
	}

	ip_free_hist(hm.h);
    }

    im_switch(im, tmp);
    ip_free_image(tmp);

 exit:
    ip_pop_routine();
    return ip_error;
}
