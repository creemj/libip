/*
   Module: ip/image.c
   Author: M. J. Cree

   Copyright (C) 1995-2013, 2015, 2016, 2018 Michael J. Cree

   Code for handling images. Image allocation and deallocation.
   Image comparisions and checks.

ENTRY POINTS:

   ip_version()          ip_version_major()      ip_version_minor()
   ip_set_global_flags() ip_clear_global_flags()
   ip_get_global_flags() ip_set_cache_size()
   ip_alloc_image()      ip_free_image()         im_copy()
   im_clear()            im_set()                im_set_ptr()
   im_value()            im_value_ptr()
   im_rowadr()

   ip_alloc_imarray()    ip_imarray_rowsize()    ip_free_imarray()
   ip_alloc_image_using_imarray()      ip_alloc_image_from_array()
   ip_free_image_leaving_imarray()

PRIVATE to library:

   image_valid()
   images_same_size()   images_same_type()   images_compatible()
   image_not_clrim()    image_bad_type()

   type_compiled()      type_real()          type_integer()
   type_clrim()

   ip_value_in_image()  ip_point_in_image()   ip_box_in_image()

   ip_flag_ok()         ip_flag_bits_unused()

   ip_parm_inrange()    ip_parm_lesseq()     ip_parm_greatereq()
   ip_parm_less()       ip_parm_greater()

   ip_parm_powof2()     ip_parm_mult2()      ip_parm_odd()

*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <float.h>
#include <inttypes.h>

#include <ip/ip.h>
#include <ip/version.h>

#include "internal.h"
#include "function_ops.h"
#include "utils.h"

#ifdef HAVE_SIMD
#include "simd/image_simd.h"
#endif

static char ip_version_str[] = "lib" IP_LIBRARY ": version " IP_VERSION
			       " (" IP_DATE ")";


/* Image datatype sizes */
ip_image_type_info_t ip_image_type_info[IM_TYPETOTAL+1] = {
    {IM_UNDEFINED, IM_TYPEMAX, 0, 0, 0, 0, "undefined"},
    {IM_BINARY,    1, sizeof(uint8_t),  sizeof(uint8_t),  0,         UINT8_MAX,  "BINARY"},
    {IM_BYTE,      0, sizeof(int8_t),   sizeof(int8_t),   INT8_MIN,  INT8_MAX,   "BYTE"},
    {IM_UBYTE,     1, sizeof(uint8_t),  sizeof(uint8_t),  0,         UINT8_MAX,  "UBYTE"},
    {IM_SHORT,     1, sizeof(int16_t),  sizeof(int16_t),  INT16_MIN, INT16_MAX,  "SHORT"},
    {IM_USHORT,    1, sizeof(uint16_t), sizeof(uint16_t), 0,         UINT16_MAX, "USHORT"},
    {IM_LONG,      1, sizeof(int32_t),  sizeof(int32_t),  INT32_MIN, INT32_MAX,  "LONG"},
    {IM_ULONG,     0, sizeof(uint32_t), sizeof(uint32_t), 0,         UINT32_MAX, "ULONG"},
    {IM_FLOAT,     1, sizeof(float),    sizeof(float),    -FLT_MAX,  FLT_MAX,    "FLOAT"},
    {IM_DOUBLE,    1, sizeof(double),   sizeof(double),   -DBL_MAX,  DBL_MAX,    "DOUBLE"},
    {IM_COMPLEX,   1, sizeof(ip_complex),sizeof(float),   -FLT_MAX,  FLT_MAX,    "COMPLEX"},
    {IM_DCOMPLEX,  1, sizeof(ip_dcomplex),sizeof(double), -DBL_MAX,  DBL_MAX,    "DCOMPLEX"},
    {IM_PALETTE,   1, sizeof(uint8_t),  sizeof(uint8_t),  0,         UINT8_MAX,  "PALETTE"},
    {IM_RGB,	   1, sizeof(ip_rgb),   sizeof(uint8_t),  0,         UINT8_MAX,  "RGB"},
    {IM_RGBA,      0, sizeof(ip_rgba),  sizeof(uint8_t) , 0,         UINT8_MAX,  "RGBA"},
    {IM_RGB16,	   0, sizeof(ip_lrgb),  sizeof(uint16_t), 0,         UINT16_MAX, "RGB16"},
    {IM_LONG64,    0, sizeof(int64_t),  sizeof(int64_t),  INT64_MIN, INT64_MAX,  "LONG64"},
    {IM_ULONG64,   0, sizeof(uint64_t), sizeof(uint64_t), 0,         UINT64_MAX, "ULONG64"},
    {IM_UNDEFINED, 0, 0, 0, 0, 0, "undefined"}
};


/* Used to check typesize field in above array */
static size_t ip_image_typesize_check[IM_TYPETOTAL+1] = {
    0,  /* IM_UNDEFINED */
    1,	/* IM_BINARY */
    1,	/* IM_BYTE */
    1,	/* IM_UBYTE */
    2,	/* IM_SHORT */
    2,	/* IM_USHORT */
    4,	/* IM_LONG */
    4,	/* IM_ULONG */
    4,	/* IM_FLOAT */
    8,	/* IM_DOUBLE */
    8,	/* IM_COMPLEX */
    16,	/* IM_DCOMPLEX */
    1,	/* IM_PALETTE */
    3,	/* IM_RGB */
    4,	/* IM_RGBA */
    6,	/* IM_RGB16 */
    8,	/* IM_LONG64 */
    8,	/* IM_ULONG64 */
    0	/* Undefined */
};


/* Masks used in "Poor Man's SIMD" code. */
#if MACHINE_NATURAL_SIZE == 4
fast_code_masks_t fast_code_masks[IM_TYPETOTAL+1] = {
    { 0, 0, 0},				    /* IM_UNDEFINED */
    { 0x00ff00ffUL, 0xff00ff00UL, 0x80808080UL }, /* IM_BINARY */
    { 0x00ff00ffUL, 0xff00ff00UL, 0x80808080UL }, /* IM_BYTE */
    { 0x00ff00ffUL, 0xff00ff00UL, 0x80808080UL }, /* IM_UBYTE */
    { 0x0000ffffUL, 0xffff0000UL, 0x80008000UL }, /* IM_SHORT */
    { 0x0000ffffUL, 0xffff0000UL, 0x80008000UL }, /* IM_USHORT */
    { 0, 0, 0},				    /* IM_LONG */
    { 0, 0, 0},				    /* IM_ULONG */
    { 0, 0, 0},				    /* IM_FLOAT */
    { 0, 0, 0},				    /* IM_DOUBLE */
    { 0, 0, 0},				    /* IM_COMPLEX */
    { 0, 0, 0},				    /* IM_DCOMPLEX */
    { 0x00ff00ffUL, 0xff00ff00UL, 0x80808080UL }, /* IM_PALETTE */
    { 0x00ff00ffUL, 0xff00ff00UL, 0x80808080UL }, /* IM_RGB */
    { 0x00ff00ffUL, 0xff00ff00UL, 0x80808080UL }, /* IM_RGBA */
    { 0x0000ffffUL, 0xffff0000UL, 0x80008000UL }, /* IM_RGB16 */
    { 0, 0, 0},				    /* IM_LONG64 */
    { 0, 0, 0},				    /* IM_ULONG64 */
    { 0, 0, 0}				    /* Undefined */
};
#else

fast_code_masks_t fast_code_masks[IM_TYPETOTAL+1] = {
    { 0, 0, 0},							    /* IM_UNDEFINED */
    { 0x00ff00ff00ff00ffUL, 0xff00ff00ff00ff00UL, 0x8080808080808080UL }, /* IM_BINARY */
    { 0x00ff00ff00ff00ffUL, 0xff00ff00ff00ff00UL, 0x8080808080808080UL }, /* IM_BYTE */
    { 0x00ff00ff00ff00ffUL, 0xff00ff00ff00ff00UL, 0x8080808080808080UL }, /* IM_UBYTE */
    { 0x0000ffff0000ffffUL, 0xffff0000ffff0000UL, 0x8000800080008000UL }, /* IM_SHORT */
    { 0x0000ffff0000ffffUL, 0xffff0000ffff0000UL, 0x8000800080008000UL }, /* IM_USHORT */
    { 0x00000000ffffffffUL, 0xffffffff00000000UL, 0x8000000080000000UL }, /* IM_LONG */
    { 0x00000000ffffffffUL, 0xffffffff00000000UL, 0x8000000080000000UL }, /* IM_ULONG */
    { 0, 0, 0},							    /* IM_FLOAT */
    { 0, 0, 0},							    /* IM_DOUBLE */
    { 0, 0, 0},							    /* IM_COMPLEX */
    { 0, 0, 0},							    /* IM_DCOMPLEX */
    { 0x00ff00ff00ff00ffUL, 0xff00ff00ff00ff00UL, 0x8080808080808080UL }, /* IM_PALETTE */
    { 0x00ff00ff00ff00ffUL, 0xff00ff00ff00ff00UL, 0x8080808080808080UL }, /* IM_RGB */
    { 0x00ff00ff00ff00ffUL, 0xff00ff00ff00ff00UL, 0x8080808080808080UL }, /* IM_RGBA */
    { 0x0000ffff0000ffffUL, 0xffff0000ffff0000UL, 0x8000800080008000UL }, /* IM_RGB16 */
    { 0, 0, 0},							    /* IM_LONG64 */
    { 0, 0, 0},							    /* IM_ULONG64 */
    { 0, 0, 0}							    /* Undefined */
};
#endif



/*
 * Global flags for the IP library --- only used by hardware acceleration at
 *  time of writing this comment
 */
int ip_global_flags;
int ip_l1_cache_size;
int ip_l2_cache_size;


/**** Public function entry points follow ****/

/*

NAME:

   ip_version() \- Get iplib version string

PROTOTYPE:

   #include <ip/ip.h>

   char * ip_version( void )

DESCRIPTION:

   Return a string describing the ip library version.

SEE ALSO:

   ip_version_minor()    ip_version_major()

*/

char *ip_version(void)
{
    return ip_version_str;
}



/*

NAME:

   ip_version_major() \- Get iplib major version number

PROTOTYPE:

   #include <ip/ip.h>

   int  ip_version_major( void )

DESCRIPTION:

   Return the major version number of the ip library.

SEE ALSO:

   ip_version()    ip_version_minor()

*/

int ip_version_major(void)
{
    return IP_VERSION_MAJOR;
}



/*

NAME:

   ip_version_minor() \- Get iplib minor version number

PROTOTYPE:

   #include <ip/ip.h>

   int  ip_version_minor( void )

DESCRIPTION:

   Return the minor version number of the ip library.

SEE ALSO:

   ip_version()    ip_version_major()

*/

int ip_version_minor(void)
{
    return IP_VERSION_MINOR;
}


void ip_set_global_flags(int flag)
{
    ip_global_flags |= (flag & IP_FLAGS_MASK);
}

void ip_clear_global_flags(int flag)
{
    ip_global_flags &= ~(flag & IP_FLAGS_MASK);
}


int ip_get_global_flags(void)
{
    return ip_global_flags;
}


void ip_set_cache_size(int type, int size, int assoc)
{
    int l1_def_size = (1024*L1_CACHE_SIZE) * L1_CACHE_ASSOC;
    int l2_def_size = (1024*L2_CACHE_SIZE) * L2_CACHE_ASSOC;

#ifdef __linux__
    if (type == 0 || size == 0) {
	FILE *f;

	f = fopen("/sys/devices/system/cpu/cpu0/cache/index0/size", "r");
	if (f) {
	    int size;
	    fscanf(f, "%d", &size);
	    fclose(f);
	    if (size > 8 && size < 32768) {
		int assoc;
		f = fopen("/sys/devices/system/cpu/cpu0/cache/index0/ways_of_associativity", "r");
		fscanf(f, "%d", &assoc);
		fclose(f);
		if (assoc > 0 && assoc <= 16) {
		    l1_def_size = 1024 * size * assoc;
		}
	    }
	}
    }
#endif

    if (type == 0) {
	ip_l1_cache_size = l1_def_size;
	ip_l2_cache_size = l2_def_size;
    }else if (type == 1)
	if (size == 0)
	    ip_l1_cache_size = l1_def_size;
	else
	    ip_l1_cache_size = 1024 * size * assoc;
    else
	if (size == 0)
	    ip_l2_cache_size = l2_def_size;
	else
	    ip_l2_cache_size = 1024 * size * assoc;
}



/*
 * Helper function to check parameters to the ip_alloc_image() like
 * routines.
 */

static int ip_alloc_image_parms_ok(ip_image_type type, ip_coord2d size)
{
    int is_good = 1;

    /*
     * Note that the comparison to IM_TYPEMAX rejects the ULONG64 and LONG64
     * types as invalid if IP_INCLUDE_LONG64 is not defined.
     */
    if (type < IM_BINARY || type >= IM_TYPEMAX) {
	ip_log_error(ERR_INVALID_IMAGE_TYPE);
	is_good = 0;
    }

    if (size.x <= 0 || size.y <= 0) {
	ip_log_error(ERR_INVALID_IMAGE_SIZE);
	is_good = 0;
    }

    return is_good;
}



/*

NAME:

   ip_imarray_rowsize() \-  Row size of image including the row padding

PROTOTYPE:

   #include <ip/ip.h>

   size_t  ip_imarray_rowsize( type, size )
   int type;
   ip_coord2d size;

DESCRIPTION:

   Find the row size (in bytes) of an IP library compliant array allocated
   by the ip_alloc_imarray() routine.  This is the row skip (in bytes) from
   one row to the next one in the image array.  To get the row stride for
   indexing into the array divide the rowsize by sizeof pixel (provided the
   pixel size is a power of 2). For example, if 'type' is IM_FLOAT then the
   row stride to use in indexing the imarray is ip_imarray_rowsize(IM_FLOAT,
   size) / ip_type_size(IM_FLOAT).

   In the case of IM_RGB and IM_RGB16 where the pixel size does not divide
   rowsize evenly more care is needed in indexing the array.

SEE ALSO:

   ip_alloc_imarray()			ip_alloc_image()

*/

size_t ip_imarray_rowsize(int type, ip_coord2d size)
{
    return (size_t)PADDED_ROW_SIZE((size_t)size.x * ip_type_size(type));
}



/*

NAME:

   ip_alloc_imarray() \- Allocate array suitable for holding image data

PROTOTYPE:

   #include <ip/ip.h>

   void * ip_alloc_imarray( type, size )
   int type;
   ip_coord2d size;

DESCRIPTION:

   Allocate a C array that is compliant to the image data array allocated
   within the ip_image object returned by the ip_alloc_image() routine.
   This routine provides a means of allocating an array that can be used in
   C code then efficiently converted into an image used by the IP library.

   The image data array allocated by the IP library in the ip_alloc_image()
   routine must satisfy certain constraints regarding row alignment and
   padding.  A C array allocated with malloc() most likely will not met the
   constraints.  Ip_alloc_imarray() provides a means to allocate an array
   that is useable with the IP library.

   Call ip_alloc_imarray() with standard IP image types (IM_BYTE, IM_SHORT,
   etc.) and the size of the array, then call ip_imarray_rowsize() to find
   the row size (in bytes) from one row to the next. Provided the image type
   is not IM_RGB or IM_RGB2 (i.e. the pixel size divides the row size) you
   find the row stride for indexing by dividing the row size by the size of
   the pixel datum.  Use the returned array in your C code with the (i,j)
   element at the j*row_stride+i index in the array.

   To call IP library routines first obtain an image handle to the array
   with ip_alloc_image_using_imarray().  This routine is efficient; it does
   not copy data but uses the passed imarray by reference.  If the image
   handle is freed with ip_free_image() then the full image and the imarray
   is freed.  If the image handle is freed with
   ip_free_image_leaving_imarrray() then the image handle is freed but
   imarray is returned and left intact.  If the imarray is never attached to
   an image handle it can be freed with ip_free().  Once
   ip_alloc_image_using_imarray() has successfully been called on the
   imarray you must not free the imarray unless you have called
   ip_free_image_leaving_imarray() on the image handle first.

   Do not call ip_alloc_image_using_imarrray() twice on the same imarray.

   If you have an array that is not allocated with ip_alloc_imarray() you
   can still initialise an IP image from it. See
   ip_alloc_image_from_array().

   NULL is returned if the imarray allocation fails with an error.

EXAMPLE:

   An example of allocating a single precision float array, allocating data
   to it and converting into an IP library image.

\|   float *arr;
\|   size_t row_stride;
\|
\|   arr = ip_alloc_imarray(IM_FLOAT, size);
\|   row_stride = ip_imarray_rowsize(IM_FLOAT, size) / sizeof(float);
\|   if (arr) {
\|       // You can access elements in arr as arr[j*row_stride + i]
\|       // Assuming arr is now initialised do:
\|       ip_image *im = ip_alloc_image_using_imarray(IM_FLOAT, size, arr);
\|
\|       // You can now use im in IP library routines, e.g.,
\|       im_dilate(im, strel);
\|
\|       // Free image but keep array
\|       ip_free_image_leaving_imarrray(im);
\|
\|       // You can still use arr here in your code
\|       // When finished free it
\|       ip_free(arr);
\|    }

ERRORS:

   ERR_OUT_OF_MEM
   ERR_INVALID_IMAGE_TYPE
   ERR_INVALID_IMAGE_SIZE

SEE ALSO:

   ip_imarray_rowsize()               ip_alloc_image_using_imarray()
   ip_alloc_image_from_array()        ip_free_image_leaving_imarray()
   ip_alloc_image()                   ip_free_image()
*/

void *ip_alloc_imarray(int type, ip_coord2d size)
{
    void *array = NULL;
    size_t row_size;

    ip_log_routine(__func__);

    /*
     * We allocate rows with row padding so that they are aligned to
     * ROW_ALIGNMENT which is guaranteed to be at least MACHINE_NATURAL_SIZE
     * (the size of an unsigned long) and, if SIMD support is compiled in,
     * the size of the SIMD vector. This is to enable code to operate on
     * unsigned longs or SIMD vectors without special code for alignment or
     * detecting end of row.
     */

    if (ip_alloc_image_parms_ok(type, size)) {
	row_size = ip_imarray_rowsize(type, size);
	array = ip_mallocx_with_row_alignment(row_size*(size_t)size.y);
    }

    ip_pop_routine();
    return array;
}



static ip_image *ip_alloc_image_common(ip_image_type type, ip_coord2d size, void *imarray)
{
    ip_image *im;
    ip_image_type_info_t *info;
    size_t rowptr_size;

    if (ip_l1_cache_size == 0)
	ip_set_cache_size(1, 0, 0);
    if (ip_l2_cache_size == 0)
	ip_set_cache_size(2, 0, 0);

    if (NOT ip_alloc_image_parms_ok(type, size))
	return NULL;

    info = &ip_image_type_info[type];
    if (info->typesize != ip_image_typesize_check[type]) {
	ip_log_error(ERR_ALGORITHM_FAULT,
		     "Type %s size %" PRIu64 " incorrect. Should be %" PRIu64 ". Fix and recompile.",
		     info->name, info->typesize, ip_image_typesize_check[type]);
	return NULL;
    }

    /* Allocate image handle */

    if ((im = ip_mallocx(sizeof(ip_image))) == NULL)
	return NULL;

    im->type = type;
    im->size = size;
    im->status = 0;
    im->row_size = ip_imarray_rowsize(type, size);
    im->imbuff = NULL;
    im->imrow.v = NULL;
    im->num_extra_rows = 0;
    im->extra_rows = NULL;
    im->palette = NULL;
    im->fftw_plans[0] = im->fftw_plans[1] = NULL;

    /*
     * Currently allocate image data area as one contiguous memory block and
     * set the IP_IMDATA_ONE_ARRAY status flag, however code cannot rely
     * upon this.  All code must assume each row is an individual unit and
     * access data through the row pointers.
     */

    if (imarray) {
	im->imbuff = imarray;
    }else{
	im->imbuff = ip_alloc_imarray(type, size);
    }
    im->status |= IP_IMDATA_ONE_ARRAY;

    if (im->imbuff == NULL) {
	ip_free(im);
	return NULL;
    }

    /*
     * Allocate image row pointers with padding at the end of the rowptr
     * array.  The padding is sufficient that we can read the rowptr array
     * with SIMD vector reads (if compiled with SIMD) or with reads of
     * size_t (MACHINE_NATURAL_SIZE) if compiled without SIMD.  Note that if
     * the SIMD vector size is 16 bytes that means we need to be able read
     * blocks of 16 row pointers at a time (i.e. sizeof(size_t) * 16), not
     * 16 bytes at a time!
     *
     * Also allocate this array with row alignment as it allows SIMD vector
     * access to the array.
     */
    rowptr_size = sizeof(void *) * PADDED_ROW_SIZE(im->size.y);
    if ((im->imrow.v = ip_mallocx_with_row_alignment(rowptr_size)) == NULL)
	goto exit_aic;

    /*
     * Calculate the row pointers for each row of the image, however do not
     * initialise the padding at the end of the array; if some operator
     * needs to use the padding it must initialise it to something sensible
     * first.
     */
    for (int j=0; j<size.y; j++) {
	im->imrow.v[j] = (void *)(im->imbuff + (j*im->row_size));
    }

    /* If IM_PALETTE allocate palette */

    if (im->type == IM_PALETTE) {
	if ((im->palette = ip_mallocx(256*sizeof(ip_rgb))) == NULL)
	    goto exit_aic;

	/* Set to a grey scale for default setup. */
	for (int i=0; i<256; i++) {
	    im->palette[i].r = (uint8_t)i;
	    im->palette[i].b = (uint8_t)i;
	    im->palette[i].g = (uint8_t)i;
	}
    }

    return im;

 exit_aic:
    if (im->imrow.v)
	ip_free(im->imrow.v);
    if (NOT imarray && im->imbuff)
	ip_free(im->imbuff);
    ip_free(im);
    return NULL;
}



/*

NAME:

   ip_alloc_image() \- Allocate an image

PROTOTYPE:

   #include <ip/ip.h>

   ip_image * ip_alloc_image(type, size)
   ip_image_type type;
   ip_coord2d size;

DESCRIPTION:

   Allocates image of specified 'type' and dimensions. The 'x' component of
   'size' specifies the number of columns, and the 'y' component specifies
   the number of rows in the image.  Returns pointer to image handle.  Does
   not clear pixels in image. If a palette image is allocated then the
   palette is initialised to a grey scale.

   Returns NULL if any errors occur.

   The 'type' field must be one of the currently supported image types,
   which are:

\|      IM_BINARY  - Each pixel takes value TRUE or FALSE
\|      IM_BYTE    - Each pixel is a signed byte (8-bit)
\|      IM_UBYTE   - Each pixel is an unsigned byte (8-bit)
\|      IM_SHORT   - Signed integer (16-bit)
\|      IM_USHORT  - Unsigned integer (16-bit)
\|      IM_LONG    - Signed integer (32-bit)
\|      IM_ULONG   - Unsigned integer (32-bit)
\|      IM_FLOAT   - IEEE single precision float (32-bit)
\|      IM_DOUBLE  - IEEE double precision float (64-bit)
\|      IM_COMPLEX - IP library complex type (float)
\|      IM_DCOMPLEX - IP library complex type (double)
\|      IM_PALETTE - Unsigned byte (8-bit) which indexes into
\|                   a 256 entry colour palette
\|      IM_RGB     - RGB colour image (24-bit)
\|      IM_RGBA    - RGBA colour image (32-bit)
\|      IM_RGB16   - RGB colour image (48-bit)

   The only fields within the ip_image object that the caller programme is
   permitted to access (read, but not modify) are 'type', and 'size'.

   Access to individual image pixels is discouraged, but if necessary should
   only be done through the routines im_drawpoint(), im_value() or
   im_rowadr(). The macros IM_ROWADR() and IM_PIXEL() give very low level
   access to pixels.  See the header file image.h for description of the
   macros.

   NULL is returned if the image allocation fails with an error.

LIMITATIONS:

   The library header files define two more image types:

\|      IM_LONG64  - Signed integer (64-bit)
\|      IM_ULONG64 - Unsigned integer (64-bit)

   but these are currently not supported and an attempt to allocate an image
   with LONG64 or ULONG64 type will result in an ERR_INVALID_IMAGE_TYPE
   error.

ERRORS:

   ERR_OUT_OF_MEM
   ERR_INVALID_IMAGE_TYPE
   ERR_INVALID_IMAGE_SIZE

SEE ALSO:

   ip_free_image()   im_rowadr()
   im_clear()        im_set()          im_set_av()
   im_value()

*/

ip_image *ip_alloc_image(ip_image_type type, ip_coord2d size)
{
    ip_image *im;

    ip_log_routine(__func__);

    im = ip_alloc_image_common(type, size, NULL);

    ip_pop_routine();
    return im;
}



/*

NAME:

   ip_alloc_image_from_array() \- Allocate image copying data from array

PROTOTYPE:

   #include <ip/ip.h>

   ip_image * ip_alloc_image_from_array( type, size, array, row_size )
   ip_image_type type;
   ip_coord2d size;
   void *array;
   size_t row_size;

DESCRIPTION:

   Allocate an image using data from the provided C allocated array.  An
   image is allocated and the data in 'array' is copied into the image.
   'row_size' (in bytes) specifies the row stride of the C array or is zero.
   If 'row_size' is zero it is assumed that the array is packed with no row
   padding, and will be taken to be exactly the same as 'size.x' * sizeof
   pixel.  If row_size is non-zero and less than 'size.x' *
   ip_type_size('type') then an error is flagged and NULL is returned.

   Because the data are copied out of 'array' into the newly formed image,
   'array' remains in the caller's control and it is the responsibility of
   the caller to free 'array' when it is no longer needed.

   A more efficient method of copying an array of data into an image is
   provided by ip_alloc_image_using_imarray() but it requires an array
   allocated by ip_alloc_imarray().

   'array' is not modified by this routine.  The caller may free 'array' at
   any time after calling ip_alloc_image_from_array().

   If the image allocation fails with an error then NULL is returned and
   'array' remains intact.

   When finished with the image free it with ip_free_image(); 'array'
   remains intact (provided the caller has not freed it in the meantime).

ERRORS:

   ERR_OUT_OF_MEM
   ERR_INVALID_IMAGE_TYPE
   ERR_INVALID_IMAGE_SIZE
   ERR_BAD_PARAMETER

SEE ALSO:

   ip_alloc_image()             ip_free_image()

*/

ip_image *ip_alloc_image_from_array(ip_image_type type, ip_coord2d size,
				    void *array, size_t row_size)
{
    ip_image *im;
    char *aptr = (char *)array;

    ip_log_routine(__func__);

    if (! row_size)
	row_size = size.x * ip_type_size(type);

    if (row_size < size.x * ip_type_size(type)) {
	ip_log_error(ERR_BAD_PARAMETER);
	ip_pop_routine();
	return NULL;
    }

    if ((im = ip_alloc_image_common(type, size, NULL))) {
	for (int j=0; j<im->size.y; j++)
	    memcpy(IM_ROWADR(im, j, v), aptr+j*row_size, size.x*ip_type_size(type));
    }

    ip_pop_routine();
    return im;
}



/*

NAME:

   ip_alloc_image_using_imarray() \- Allocate image using pre-existing image array

PROTOTYPE:

   #include <ip/ip.h>

   ip_image * ip_alloc_image_using_imarray( type, size, array )
   ip_image_type type;
   ip_coord2d size;
   void *array;

DESCRIPTION:

   Allocate an image using an imarray previously allocated with
   ip_alloc_imarray().  After successfully calling
   ip_alloc_image_using_imarray() the imarray (argument 'array') is owned by
   the IP library.  Do not free it.

   There is no check that the array passed is in fact a valid imarray
   returned by ip_alloc_imarray().  Calling ip_alloc_image_using_imarray()
   with an array allocated with malloc() will likeliy lead to crashes.

   To free the image do one of either calling ip_free_image() in which case
   the imarray is also freed at the same time, or call
   ip_free_image_leaving_imarray() in which case the image handle is freed
   but the imarray is left intact.  The caller can then use imarray as it
   likes and should free it with ip_free().

   If an error occurs NULL is returned and the imarray is left intact and in
   the caller's control.

ERRORS:

   ERR_OUT_OF_MEM
   ERR_INVALID_IMAGE_TYPE
   ERR_INVALID_IMAGE_SIZE

SEE ALSO:

   ip_alloc_imarray()
   ip_alloc_image()             ip_free_image()

*/

ip_image *ip_alloc_image_using_imarray(ip_image_type type, ip_coord2d size,
				       void *array)
{
    ip_image *im;

    ip_log_routine(__func__);

    im = ip_alloc_image_common(type, size, array);

    ip_pop_routine();
    return im;
}


/*
 * ip_alloc_im_extra_rows()
 *
 * Allocate the extra rows in an image for working space or a dummy write
 * space.  This routine intended for use internally in the library only.
 */

int ip_im_alloc_extra_rows(ip_image *im, int numrows)
{
    int success = TRUE;

    ip_log_routine(__func__);

    if (im->num_extra_rows < numrows) {
	void *extra_rows;

	/* Need to allocate more rows */
	if ((extra_rows = ip_mallocx_with_row_alignment(im->row_size*(size_t)numrows))) {
	    if (im->num_extra_rows)
		ip_free(im->extra_rows);

	    im->extra_rows = extra_rows;
	    im->num_extra_rows = numrows;
	}else{
	    success = FALSE;
	}
    }

    ip_pop_routine();
    return success;
}


/*
 * ip_im_set_rowptr_padding()
 *
 * Set the row pointer padding at the end of the row pointer array of the
 * image to point somewhere useful according to the flag:
 *   IP_ROWPTRPAD_LASTROW   - set them all to point to last row of image.
 *   IP_ROWPTRPAD_EXTRAROW1 - set them all to point to first row in extra
 *                               rows.
 */

void ip_im_set_rowptr_padding(ip_image *im, int flag)
{
    int numpads = PADDED_ROW_SIZE(im->size.y) - im->size.y;

    ip_log_routine(__func__);

    if (flag == IP_ROWPTRPAD_LASTROW) {
	for (int j=0; j<numpads; j++) {
	    im->imrow.v[im->size.y+j] = im->imrow.v[im->size.y-1];
	}
    }else if (flag == IP_ROWPTRPAD_EXTRAROW1) {
	if (im->num_extra_rows < 1) {
	    ip_log_error(ERR_ALGORITHM_FAULT, "setting up rowptr padding without allocated extra rows.");
	}else{
	    for (int j=0; j<numpads; j++) {
		im->imrow.v[im->size.y+j] = im->extra_rows;
	    }
	}
    }else{
	ip_log_error(ERR_ALGORITHM_FAULT, "called with bad flags.");
    }

    ip_pop_routine();
}



/*

NAME:

   ip_free_image() \- Free a previously allocated image

PROTOTYPE:

   #include <ip/ip.h>

   void ip_free_image(ip_image *)

DESCRIPTION:

   Free memory and other resources associated with image handle.  The image
   handle is not valid for use after execution of ip_free_image().  Safe to
   call with NULL handle.

   If the image was allocated with ip_alloc_image_using_imarray() the
   imarray is also freed.

SEE ALSO:

   ip_alloc_image()

*/

void ip_free_image(ip_image *im)
{
    if (!im)
	return;

    ip_log_routine(__func__);

#if 0
    /* Release any FFTW plans allocated for COMPLEX images */
    for (int j=0; j<=1; j++) {
	if (im->type == IM_COMPLEX)
	    fftwf_destroy_plan(im->fftw_plans[j]);
	else
	    fftw_destroy_plan(im->fftw_plans[j]);
    }
#endif

    /* Release image data array(s) */
    if (im->status & IP_IMDATA_ONE_ARRAY) {
	/* Image data is in one great big array */
	if (im->imbuff)
	    ip_free(im->imbuff);
    }else{
	/* Image data in separate row allocations */
	for (int j=0; j<im->size.y; j++) {
	    if (im->imrow.v[j])
		ip_free(im->imrow.v[j]);
	}
    }
    /* Release row pointers */
    if (im->imrow.v)
	ip_free(im->imrow.v);
    /* Release extra working space rows */
    if (im->extra_rows)
	ip_free(im->extra_rows);
    /* Release palette if PALETTE image */
    if (im->type == IM_PALETTE && im->palette)
	ip_free(im->palette);
    /* Release image handle */
    ip_free(im);

    ip_pop_routine();
}



/*

NAME:

   ip_free_image_leaving_imarray() \- Free image but not the image data array

PROTOTYPE:

   #include <.h>

   void * ip_free_image_leaving_imarray( im )
   ip_image *im;

DESCRIPTION:

   Free the image and all its resources except leave the imarray intact.
   This is useful to extract the image array from an image and continue to
   use it as a C array.  Note that rows in the image array are padded at the
   end of each row and the caller can find out the amount of padding by
   calling ip_imarray_rowsize().

   Note that this routine can fail to extract the imarray from the image if
   the image data are not stored as one contiguous array.  In this case NULL
   is returned, no error is flagged, and the image is *not* freed.

   If the image array was allocated with ip_alloc_imarray() and then
   attached to an image with ip_alloc_image_using_imarray() then this
   routine is guaranteed to succeed and return the imarray.

   This routine cannot produce an error.  It is safe to call with a NULL
   image handle (and an imarray will fail to be extracted thus NULL will be
   returned).

SEE ALSO:

   ip_alloc_image()          ip_alloc_image_using_imarray()

*/

void *ip_free_image_leaving_imarray(ip_image *im)
{
    void *imarray;

    if (!im)
	return NULL;

    if (NOT (im->status & IP_IMDATA_ONE_ARRAY))
	return NULL;

    imarray = im->imbuff;
    im->imbuff = NULL;
    ip_free_image(im);

    return imarray;
}



/*

NAME:

   im_copy() \- Copy an image including all its image data

PROTOTYPE:

   #include <ip/ip.h>

   ip_image * im_copy( im )
   ip_image * im;

DESCRIPTION:

   Makes an identical copy of the image 'im'. The new image can be freed
   with ip_free_image() in the normal way.  Returns NULL if the copy fails.

ERRORS:

   ERR_OUT_OF_MEM

SEE ALSO:

   ip_alloc_image()    ip_free_image()    im_convert()    im_cut()

*/

ip_image *im_copy(ip_image *src)
{
    ip_image *dest;

    ip_log_routine(__func__);

    if (NOT image_valid(src))
	return NULL;

    if ((dest = ip_alloc_image(src->type,src->size)) != NULL) {

#ifdef HAVE_SIMD
	if (IP_USE_HWACCEL) {
	    /*
	     * We can bet the memcpy() used below because we know that the
	     * data are aligned, etc.
	     */
	    ip_simd_copy_image_data(dest, src, 0);

	    /* Also copy palette if necessary */
	    if (src->type == IM_PALETTE)
		im_palette_copy(dest,src);

	    ip_pop_routine();
	    return dest;
	}
#endif

	if ((src->status & IP_IMDATA_ONE_ARRAY) &&
	    (dest->status & IP_IMDATA_ONE_ARRAY)) {
	    /* Can copy image data as one array (we end up copying
	     * the row padding--if any--too)
	     */
	    memcpy(dest->imbuff, src->imbuff, (size_t)src->row_size*src->size.y);
	}else{
	    /* Must copy image data row by row */
	    for (int j=0; j<src->size.y; j++) {
		memcpy(IM_ROWADR(dest,j,v),IM_ROWADR(src,j,v),
		       src->size.x * ip_image_type_info[src->type].typesize);
	    }
	}
	/* Also copy palette if necessary */
	if (src->type == IM_PALETTE)
	    im_palette_copy(dest,src);
    }

    ip_pop_routine();

    return dest;
}


/*

NAME:

   im_switch()

PROTOTYPE:

   #include <ip/ip.h>

   void  im_switch( im1, im2 )
   ip_image *im1;
   ip_image *im2;

DESCRIPTION:

   Switch the image data arrays between 'im1' and 'im2' so that on exit
   'im1' contains the pixel data the image 'im2' had on entry, and that
   'im2' has the pixel data that image 'im1' had on entry.

   The two images must be the same size and the same type.

   This can be useful for implementing an in-place image operator by an
   out-of-place image operator and switching the arrays at the end.

ERRORS:

   ERR_NOT_SAME_SIZE
   ERR_NOT_SAME_TYPE

*/


int im_switch(ip_image *im1, ip_image *im2)
{
    void *imbuff, *rowadr, *palette, *extra_rows;
    int num_extra_rows, status;

    ip_log_routine(__func__);

    if (NOT images_compatible(im1, im2))
	return ip_error;

    ip_pop_routine();

    imbuff = im1->imbuff;
    rowadr = im1->imrow.v;
    palette = im1->palette;
    extra_rows = im1->extra_rows;
    num_extra_rows = im1->num_extra_rows;
    status = im1->status & IP_IMDATA_ONE_ARRAY;

    im1->imbuff = im2->imbuff;
    im1->imrow.v = im2->imrow.v;
    im1->palette = im2->palette;
    im1->extra_rows = im2->extra_rows;
    im1->num_extra_rows = im2->num_extra_rows;
    im1->status &= ~IP_IMDATA_ONE_ARRAY;
    im1->status |= im2->status & IP_IMDATA_ONE_ARRAY;

    im2->imbuff = imbuff;
    im2->imrow.v = rowadr;
    im2->palette = palette;
    im2->extra_rows = extra_rows;
    im2->num_extra_rows = num_extra_rows;
    im2->status &= ~IP_IMDATA_ONE_ARRAY;
    im2->status |= status;

    return OK;
}



/*

NAME:

   ip_init_anyval_dddd()

PROTOTYPE:

   #include <ip/ip.h>

   ip_anyval  ip_init_anyval_dddd( type, v1, v2, v3, v4 )
   int type;
   double v1;
   double v2;
   double v3;
   double v4;

DESCRIPTION:

   Initialise the union ip_anyval with the values 'v1', 'v2', 'v3', 'v4'
   according to the image type specified in 'type'.

   If the type is of integer, binary or palette image type then sets the
   ull[0] field to the rounded value of v1.

   If the type is of real or complex type then sets the d[0] field to the
   value of 'v1' and the d[1] field to the value of 'v2'.  Thus, for complex
   types pass the real value in 'v1' and the imaginary value in 'v2'.

   If the type is of one of the RGB types then sets ul[0] to the rounded
   value of 'v1', likewise ul[1] from 'v2', ul[2] from 'v3' and ul[3] from
   'v4'. Thus pass the red component in 'v1', the green component in 'v2',
   the blue component in 'v3', and for an RGBA type pass the alpha component
   in 'v4'.

   No value checking on the input arguments is performed.  On 32-bit arches
   (particularly) excessively large arguments for BYTE or SHORT sized types
   may not be well wrapped, that is, the resulting value stored in ul[0] may
   not be integer(v1) mod (type number bits) where integer(v1) means the
   conversion of v1 to an integer representation with as many bits as is
   required to preserve the integer value exactly.  As they say, "Garbage
   in, garbage out."

ERRORS:

   No errors can be generated by ip_init_anyval_dddd().

*/



ip_anyval ip_init_anyval_dddd(int type, double v1, double v2, double v3, double v4)
{
    ip_anyval av;

    if (type_integer(type) || type == IM_BINARY || type == IM_PALETTE) {
	av.ull[0] = (uint64_t)(int64_t)LROUND(v1);
	av.ull[1] = 0;
    }else if (type_real(type) || type_complex(type)) {
	av.d[0] = v1;
	av.d[1] = v2;
    }else if (type_rgbim(type)) {
	av.ul[0] = (uint32_t)IROUND(v1);
	av.ul[1] = (uint32_t)IROUND(v2);
	av.ul[2] = (uint32_t)IROUND(v3);
	av.ul[3] = (uint32_t)IROUND(v4);
    }else{
	av.ull[0] = av.ull[1] = 0;
    }

    return av;
}



/*

NAME:

   im_clear() \- Clear an image

PROTOTYPE:

   #include <ip/ip.h>

   void  im_clear( im )
   image *im;

DESCRIPTION:

   Clear all pixels in an image.  For integer images clears pixels to zero.
   For floating point and complex images sets pixels to floating point 0.0
   (+i0.0).  For RGB colour images set pixels to black.  For palette images
   sets pixels to palette number 0 (which may or may not be black)---it does
   not modify the palette.  For BINARY images clears all pixels to FALSE.

SEE ALSO:

   im_set()    im_set_av()      im_create()

*/

int im_clear(ip_image *im)
{
    int rv;

    ip_log_routine(__func__);
    rv = im_set(im, 0.0, NO_FLAG);
    ip_pop_routine();
    return rv;
}


/*

NAME:

   im_set() \- Set all pixels in an image to a value.

PROTOTYPE:

   #include <ip/ip.h>

   void  im_set( im, val, flag )
   ip_image *im;
   double val;

DESCRIPTION:

   Set all pixels in an image to one particular value.  For palette images
   only affects the pixels in the image; this routine does not modify the
   palette itself.

   'Flag' can have the following values:

\&      FLG_SREAL  - Set real part only of complex or dcomplex image.
\&      FLG_SIMAG  - Set imag part only of complex or dcomplex image.
\&      FLG_SRED   - Set red part of rgb or rgba image.
\&      FLG_SGREEN - Set green part of rgb or rgba image.
\&      FLG_SBLUE  - Set blue part of rgb or rgba image.
\&      FLG_SALPHA - Set alpha part of rgba image.

    OR the above flags with FLG_ZEROREST to zero other components of complex
    or colour images.  OR the above flags to set multiple components of
    complex or colour images to same value.  Set flag to NO_FLAG to set all
    components of a RGB or complex image.

    Also see im_set_av() which provides more arbitrary setting of complex
    and colour image types.

EXAMPLES:

\|   im_set(im, 10, NO_FLAG)

   For scalar image type sets all pixels to value of 10.  For
   multi-component types this command sets every component of every pixel to
   10.

\|   im_set(im, 10, FLG_REAL)

   For complex or dcomplex images sets the real part to 10 for each pixel,
   and leaves the imaginary component unchanged.  For scalar images it is
   the same as if NO_FLAG was passed, however use of FLG_IMAG on a
   non-complex image will generate a bad flag error.

\|   im_set(im, 10, FLG_REAL | FLG_IMAG)

   For complex or dcomplex images set both the real part and the imaginary
   part of every pixel to 10.  Generates a bad flag error on other image
   types.

\|   im_set(im, 10, FLG_REAL | FLG_ZEROREST)

   For complex and dcomplex images sets real part to 10 and clears imaginary
   part of every pixel.

ERRORS:

   ERR_BAD_PARAMETER
   ERR_BAD_FLAG

SEE ALSO:

   im_clear()    im_create()     im_set_av()

*/

int im_set(ip_image *im, double val, int flag)
{
    ip_anyval av;

    ip_log_routine(__func__);

    av = ip_init_anyval_dddd(im->type, val, val, val, val);

    if (im->type == IM_FLOAT && NOT ip_anyval_in_image("val", av, im))
	return ip_error;

    /* Get im_set_av() to do the hard work. */
    im_set_av(im, av, flag);

    ip_pop_routine();
    return ip_error;
}


/*
 * Helper functions for im_set_av().
 */

static void im_set_av_DOUBLE(ip_image *im, ip_anyval av, int flag)
{
    for (int j=0; j<im->size.y; j++) {
	double *ip = IM_ROWADR(im,j,v);
	for (int i=0; i<im->size.x; i++)
	    *ip++ = av.d[0];
    }
}

#define generate_set_av_complex_function(type, basetype) \
    static void im_set_av_ ## type(ip_image *im, ip_anyval av, int flag) \
    {									\
	if (flag == NO_FLAG) {						\
	    type ## _type cv = (type ## _type){(basetype)av.d[0], (basetype)av.d[1]}; \
	    for (int j=0; j<im->size.y; j++) {				\
		type ## _type *ip = IM_ROWADR(im,j,v);			\
		for (int i=0; i<im->size.x; i++)			\
		    *ip++ = cv;						\
	    }								\
	}else{								\
	    int offset;							\
	    basetype val;						\
									\
	    if (flag & FLG_SREAL) {					\
		offset = 0;						\
		val = (basetype)av.d[0];				\
	    }else{   /* FLG_SIMAG */					\
		offset = 1;						\
		val = (basetype)av.d[1];				\
	    }								\
	    for (int j=0; j<im->size.y; j++) {				\
		basetype *ip = IM_ROWADR(im,j,v);			\
		ip += offset;						\
		for (int i=0; i<im->size.x; i++) {			\
		    *ip = val;						\
		    ip += 2;						\
		}							\
	    }								\
	}								\
    }

generate_set_av_complex_function(COMPLEX, float)
generate_set_av_complex_function(DCOMPLEX, double)

#define generate_set_av_rgb_function(type, basetype)			\
    static void im_set_av_ ## type(ip_image *im, ip_anyval av, int flag) \
    {									\
	type ## _type rgbv = (type ## _type){av.ul[0], av.ul[1], av.ul[2]}; \
									\
	if (flag == NO_FLAG) {						\
	    for (int j=0; j<im->size.y; j++) {				\
		type ## _type *ip = im_rowadr(im,j);			\
		for (int i=0; i<im->size.x; i++) {			\
		    *ip++ = rgbv;					\
		}							\
	    }								\
	}else{								\
	    for (int j=0; j<im->size.y; j++) {				\
		type ## _type *ip = im_rowadr(im,j);			\
		for (int i=0; i<im->size.x; i++) {			\
		    type ## _type nval;					\
		    nval.r = (flag & FLG_SRED) ? rgbv.r : ip->r;	\
		    nval.g = (flag & FLG_SGREEN) ? rgbv.g : ip->g;	\
		    nval.b = (flag & FLG_SBLUE) ? rgbv.b : ip->b;	\
		    *ip++ = nval;					\
		}							\
	    }								\
	}								\
    }

generate_set_av_rgb_function(RGB, uint8_t)
generate_set_av_rgb_function(RGB16, uint16_t)

static void im_set_av_RGBA(ip_image *im, ip_anyval av, int flag)
{
    ip_rgba rgbav = (ip_rgba){av.ul[0], av.ul[1], av.ul[2], av.ul[3]};
    if (flag == NO_FLAG) {
	/* Set all components of rgb image */
	for (int j=0; j<im->size.y; j++) {
	    ip_rgba *ip = im_rowadr(im,j);
	    for (int i=0; i<im->size.x; i++) {
		*ip++ = rgbav;
	    }
	}
    }else{
	/* Set one or more components only */
	for (int j=0; j<im->size.y; j++) {
	    ip_rgba *ip = im_rowadr(im,j);
	    for (int i=0; i<im->size.x; i++) {
		ip_rgba nval;
		nval.r = (flag & FLG_SRED) ? rgbav.r : ip->r;
		nval.g = (flag & FLG_SGREEN) ? rgbav.g : ip->g;
		nval.b = (flag & FLG_SBLUE) ? rgbav.b : ip->b;
		nval.a = (flag & FLG_SALPHA) ? rgbav.a : ip->a;
		*ip++ = nval;
	    }
	}
    }
}

static improc_function_Iaf im_set_av_bytype[IM_TYPEMAX] = {
    [IM_DOUBLE] = im_set_av_DOUBLE,
    [IM_COMPLEX] = im_set_av_COMPLEX,
    [IM_DCOMPLEX] = im_set_av_DCOMPLEX,
    [IM_RGB] = im_set_av_RGB,
    [IM_RGBA] = im_set_av_RGBA,
    [IM_RGB16] = im_set_av_RGB16
};


/*

NAME:

   im_set_av() \- Set all pixels in an image to the specified constant value.

PROTOTYPE:

   #include <ip/ip.h>

   int  im_set_av( im, val, flag )
   ip_image *im;
   ip_anyval val;
   int flag;

DESCRIPTION:

   Set all pixels of the image to be the specified value.  This routine is
   more general than im_set(), in particular, every pixel in a COMPLEX image
   can be set to an arbitrary complex value, and every pixel in an RGB type
   image can be set to an arbitrary RGB value.

   For scalar integer images (including BINARY and PALETTE) set val.ull[0]
   to the value to use to initialise the image to.  For real images (FLOAT
   and DOUBLE) set val.d[0] to the value to use to initialise the image to.
   For complex images (COMPLEX and DCOMPLEX) set val.d[0] to the real
   component and val.d[1] to the imaginary component of the complex number
   to initialise the image to.  For an RGB image type set val.ul[0] to the
   red component, val.ul[1] to the green component and val.ul[2] to the blue
   component of the RGB value to set the image to.  For RGBA images also set
   val.ul[3] to the alpha component.

   If no flag passed sets all components of every pixel to the specified
   value in 'val'.  If one of FLG_SREAL or FLG_SIMAG is passed for a COMPLEX
   image then only set the specified component (real or imaginary) of every
   pixel leaving the other component unmodified.  Likewise FLG_SRED,
   FLG_SGREEN, FLG_SBLUE and FLG_SALPHA has a similar effect on RGB/RGBA
   images.  If the flag FLG_ZEROREST is ORed with any of the component set
   flags then the components that are not specified by a component set flag
   are zeroed.  For example, (FLG_SRED | FLG_SGREEN | FLG_ZEROREST) is
   passed as the flag when processing an RGB image then the red and green
   components are set to the specified values in 'val' and the blue
   component (and also alpha component if an RGBA image) is set to zero.

   It is an error to pass FLG_SREAL/FLG_SIMAG on anything but a complex
   image.  Likewise it is an error to pass any of FLG_SRED, etc., on
   anything but an RGB/RGBA colour image.

   When processing a PALETTE image only the image data are modified by
   im_set_av().  The palette is left untouched.

ERRORS:

   ERR_BAD_PARAMETER
   ERR_BAD_FLAG

SEE ALSO:

   im_set()		im_clear()
   ip_init_anyval_dddd()

*/

int im_set_av(ip_image *im, ip_anyval val, int flag)
{
    ip_log_routine(__func__);

    if (NOT image_valid(im))
	return ip_error;

    if (NOT ip_anyval_in_image("value", val, im))
	return ip_error;

    /* Check flags - depends on type of image */

    if (type_scalar(im->type) || im->type == IM_PALETTE)
	if (!ip_flag_ok(flag, FLG_SREAL, LASTFLAG))
	    return ip_error;

    if (type_rgbim(im->type) && im->type != IM_RGBA) {
	if (!ip_flag_bits_unused(flag, ~(FLG_SRED|FLG_SGREEN|FLG_SBLUE|FLG_ZEROREST)))
	    return ip_error;
	if (flag == (FLG_SRED | FLG_SGREEN | FLG_SBLUE))
	    flag = NO_FLAG;
    }

    if (im->type == IM_RGBA) {
	if (!ip_flag_bits_unused(flag, ~(FLG_SRED|FLG_SGREEN|FLG_SBLUE|FLG_SALPHA|FLG_ZEROREST)))
	    return ip_error;
	if (flag == (FLG_SRED | FLG_SGREEN | FLG_SBLUE | FLG_SALPHA))
	    flag = NO_FLAG;
    }

    if (type_complex(im->type)) {
	if (!ip_flag_bits_unused(flag, ~(FLG_SREAL|FLG_SIMAG|FLG_ZEROREST)))
	    return ip_error;
	if (flag == (FLG_SREAL | FLG_SIMAG))
	    flag = NO_FLAG;
    }

    if (flag == FLG_ZEROREST) {
	/* Cannot have FLG_ZEROREST by itself */
	ip_log_error(ERR_BAD_FLAG);
	ip_pop_routine();
	return ip_error;
    }

    if (flag & FLG_ZEROREST) {
	/* Implement FLG_ZEROREST behaviour */
	if (type_rgbim(im->type)) {
	    if (NOT (flag & FLG_SRED))
		val.ul[0] = 0;
	    if (NOT (flag & FLG_SGREEN))
		val.ul[1] = 0;
	    if (NOT (flag & FLG_SBLUE))
		val.ul[2] = 0;
	    if (NOT (flag & FLG_SALPHA))
		val.ul[3] = 0;
	}else if (type_complex(im->type)) {
	    if (NOT (flag & FLG_SREAL))
		val.d[0] = 0.0;
	    if (NOT (flag & FLG_SIMAG))
		val.d[1] = 0.0;
	}
	/* Indicate to code below to set all fields of a multi-field type */
	flag = NO_FLAG;
    }

    /* Binary image can only take 0 (false) and 255 (true) */
    if (im->type == IM_BINARY)
	val.ull[0] = (val.ull[0] == 0.0 ? 0.0 : 255.0);

    /*
     * Process BYTE orientated images with memset since that is likely to be
     * at least as efficient as any code we write.
     */

    if (im->type == IM_BYTE || im->type == IM_UBYTE
		|| im->type == IM_PALETTE || im->type == IM_BINARY) {

	for (int j=0; j<im->size.y; j++) {
	    memset(IM_ROWADR(im, j, v), (int)val.ull[0], im->size.x);
	}
	ip_pop_routine();
	return OK;
    }

    /*
     * Everything but byte images.  Try a SIMD vector write first since that
     * is likely to best saturate memory bandwidth.
     */

#ifdef HAVE_SIMD
    if (IP_USE_HWACCEL && ip_simd_set_bytype[im->type]) {
	ip_simd_set_bytype[im->type](im, val, flag);
	ip_pop_routine();
	return OK;
    }
#endif

    /*
     * No SIMD vector write options exist for the image type, so now try
     * writing out data at the natural machine integer size.
     */
    {
	unsigned long val_splatted;
	double val_to_splat = type_real(im->type) ? val.d[0] : (double)(int64_t)val.ull[0];

	if (ip_value_splat(im->type, val_to_splat, &val_splatted)) {
	    /* Succeeded in splatting value across an unsigned long so can write
	     * values efficiently using machine natural size (which we assume to
	     * be a long since 64 bit machines tend to make int 32 bit).
	     */
	    int rowlen = im->row_size/sizeof(unsigned long);

	    for (int j=0; j<im->size.y; j++) {
		unsigned long *ip;
		ip = IM_ROWADR(im,j,v);
		for (int i=0; i<rowlen; i++)
		    *ip++ = val_splatted;
	    }

	    ip_pop_routine();
	    return OK;
	}
    }

    /*
     * Since BYTE/UBYTE/BINARY/PALLETE already done, and (U)SHORT, (U)LONG
     * and FLOAT always splat, we are down to implementing DOUBLE, COMPLEX
     * types, and RGB image types from here on in.
     */

    if (im_set_av_bytype[im->type])
	im_set_av_bytype[im->type](im, val, flag);
    else
	ip_log_error(ERR_NOT_IMPLEMENTED);

    ip_pop_routine();
    return OK;
}




/*

NAME:

   im_value() \- Return the pixel value at a point in an image

PROTOTYPE:

   #include <ip/ip.h>

   ip_anyval  im_value( im, pt )
   ip_image *im;
   ip_coord *pt;

DESCRIPTION:

   Return image value at point 'pt' returned in an ip_anyval union.  Useful
   if you really need to get into image array!  But is a little slow since
   it does checking to make sure point is within image boundaries.

   If you desparately need fast access to image pixels the macro IM_PIXEL()
   may be used, however its use is discouraged.  See header file image.h for
   comments on use of IM_PIXEL().  Also see im_rowadr() for access to row in
   image.

   See ip_init_anyval_dddd() for a description of the ip_anyval union and
   conventions.

LIMITATIONS:

   Must have an error handler installed to detect any errors flagged
   (since returns .ul[0] = .ull[0] = .d[0] = 0 if any occurs).

ERRORS:

   ERR_BAD_PARAMETER
   ERR_BAD_TYPE

SEE ALSO:

   im_drawpoint()   im_rowadr()      ip_init_anyval_dddd()

*/

ip_anyval im_value(ip_image *im, ip_coord2d pt)
{
    ip_anyval av;

    ip_log_routine(__func__);

    if (NOT ip_point_in_image("point", pt, im))
	return (ip_anyval){.ull[0] = 0, .ull[1] = 0};

    switch (im->type) {
    case IM_BINARY:
	av.ull[0] = (IM_PIXEL(im, pt.x, pt.y, ub) == 0) ? 0 : 1;
	break;
    case IM_PALETTE:
    case IM_UBYTE:
	av.ull[0] = IM_PIXEL(im, pt.x, pt.y, ub);
	break;
    case IM_BYTE:
	av.ull[0] = (uint64_t)(int64_t)IM_PIXEL(im, pt.x, pt.y, b);
	break;
    case IM_USHORT:
	av.ull[0] = IM_PIXEL(im, pt.x, pt.y, us);
	break;
    case IM_SHORT:
	av.ull[0] = (uint64_t)(int64_t)IM_PIXEL(im, pt.x, pt.y, s);
	break;
    case IM_ULONG:
	av.ull[0] = IM_PIXEL(im, pt.x, pt.y, ul);
	break;
    case IM_LONG:
	av.ull[0] = (uint64_t)(int64_t)IM_PIXEL(im, pt.x, pt.y, l);
	break;
    case IM_FLOAT:
	av.d[0] =  (double)IM_PIXEL(im, pt.x, pt.y, f);
	break;
    case IM_DOUBLE:
	av.d[0] =  IM_PIXEL(im, pt.x, pt.y, d);
	break;
    case IM_COMPLEX:
	av.d[0] =  (double)IM_PIXEL(im, pt.x, pt.y, c).r;
	av.d[1] =  (double)IM_PIXEL(im, pt.x, pt.y, c).i;
	break;
    case IM_DCOMPLEX:
	av.d[0] =  IM_PIXEL(im, pt.x, pt.y, dc).r;
	av.d[1] =  IM_PIXEL(im, pt.x, pt.y, dc).i;
	break;
    case IM_RGB:
	av.ul[0] =  IM_PIXEL(im, pt.x, pt.y, r).r;
	av.ul[1] =  IM_PIXEL(im, pt.x, pt.y, r).g;
	av.ul[2] =  IM_PIXEL(im, pt.x, pt.y, r).b;
	break;
    case IM_RGBA:
	av.ul[0] =  IM_PIXEL(im, pt.x, pt.y, ra).r;
	av.ul[1] =  IM_PIXEL(im, pt.x, pt.y, ra).g;
	av.ul[2] =  IM_PIXEL(im, pt.x, pt.y, ra).b;
	av.ul[3] =  IM_PIXEL(im, pt.x, pt.y, ra).a;
	break;
    case IM_RGB16:
	av.ul[0] =  IM_PIXEL(im, pt.x, pt.y, r16).r;
	av.ul[1] =  IM_PIXEL(im, pt.x, pt.y, r16).g;
	av.ul[2] =  IM_PIXEL(im, pt.x, pt.y, r16).b;
	break;
#ifdef IP_INCLUDE_LONG64
    case IM_ULONG64:
	av.ull[0] = IM_PIXEL(im, pt.x, pt.y, ull);
	break;
    case IM_LONG64:
	av.ull[0] = (uint64_t)IM_PIXEL(im, pt.x, pt.y, ll);
	break;
#endif
    default:
	ip_log_error(ERR_BAD_TYPE);
	av.ull[0] = av.ull[1] = 0;
    }

    ip_pop_routine();
    return av;
}



/*

NAME:

   im_rowadr() \- Get address to row of image data in an image

PROTOTYPE:

   #include <ip/ip.h>

   void * im_rowadr( im, row )
   ip_image *im;
   int row;

DESCRIPTION:

   Returns the address to the specified row in the image.  It is guaranteed
   that the pointer returned points to an array in which all pixels across
   the row occur consequtively as increasing column (or x) position.  This
   provides a convenient and reasonable fast means to access the image data.
   Returns NULL and flags the error ERR_BAD_ROWADR if 'row' is not a valid
   row in the image.

   Note that im_rowadr() does not put its name into the subroutine name
   stack.  Therefore any errors will be reported as being directly incurred
   by the calling routine rather than im_rowadr().

   As an example of use, to implement adding a constant value to a UBYTE
   image, one could do:

\|   uint8_t val;
\|
\|   for (int j=0; j<im->size.y; j++) {
\|      uint8_t *rptr;
\|      rptr = im_rowadr(im, j);
\|      for (int i=0; i<im->size.x; i++) {
\|         *rptr++ += val;
\|      }
\|   }

   Of course, one is encouraged to use actual IP library entry points to
   image processing routines rather than resorting to direct image access as
   shown in the example above.

   Using im_rowadr() is the preferred method for implementing new routines
   in the IP library.

ERRORS:

   ERR_BAD_ROWADR

SEE ALSO:

   alloc_image()    im_value()

*/


void *im_rowadr(ip_image *im, int row)
{
    void *rowadr;

    if (row < 0 || row >= im->size.y) {
	ip_log_error(ERR_BAD_ROWADR, row, im->size.y);
	rowadr = NULL;
    }else{
	rowadr = im->imrow.v[row];
    }

    return rowadr;
}




/**** Private routines for the IP library ****/

/*
 * Don't use these routines unless you design new image processing operators
 * to operate on IP library images.
 */

/**** Image checks used by ip routines ****/

/*
 * image_valid(im)
 *
 * Checks that some fields of the image object are sensible.  Most
 * importantly check the type field as that is used in jump tables and if it
 * had a noxious value it could otherwise be used to execute arbitrary code.
 */

int image_valid(ip_image *im)
{
    int valid = 1;

    if (im) {
	if ((int)im->type < 0 || (int)im->type >= IM_TYPEMAX)
	    valid = 0;
	else if (im->size.x < 0 || im->size.y < 0)
	    valid = 0;
	else{
	    unsigned int row_size;

	    row_size = (((unsigned int)im->size.x * ip_type_size(im->type)) + (ROW_ALIGNMENT-1))
		& (~(ROW_ALIGNMENT-1));

	    if (im->row_size != row_size)
		valid = 0;
	    else if (im->type == IM_PALETTE && im->palette == NULL)
		valid = 0;
	    else if (im->imbuff == NULL || im->imrow.v == NULL)
		valid = 0;
	}
    }else{
	valid = 0;
    }

    /*
     * A further check would be to check that all row pointers are valid but
     * that is more time consuming and may not be an acceptable delay so we
     * don't do it.
     */

    if (! valid) {
	ip_log_error(ERR_INVALID_IMAGE);
	ip_pop_routine();
    }

    return valid;
}




/*
 * images_same_size(im1, im2)
 *
 * Returns TRUE if ( images are the same size ).
 *
 * Else flags error ERR_NOT_SAME_SIZE
 *      pops routine name
 *	returns FALSE
 */

int images_same_size(ip_image *im1, ip_image *im2)
{
    if ((im1->size.x != im2->size.x) || (im1->size.y != im2->size.y)) {
	ip_log_error(ERR_NOT_SAME_SIZE);
	ip_pop_routine();
	return FALSE;
    }
    return TRUE;
}



/*
 * images_same_type(im1, im2)
 *
 * Returns TRUE if ( images are the same type ).
 *
 * Else flags error ERR_NOT_SAME_TYPE
 *      pops routine name
 *	returns FALSE
 */

int images_same_type(ip_image *im1, ip_image *im2)
{
    if (im1->type != im2->type) {
	ip_log_error(ERR_NOT_SAME_TYPE);
	ip_pop_routine();
	return FALSE;
    }
    return TRUE;
}


/*
 * images_compatible(im1, im2)
 *
 * Returns TRUE if (      images same size
 *		      and images same type )
 *
 * Else flags one of the errors:
 *	     ERR_NOT_SAME_SIZE
 *	     ERR_NOT_SAME_TYPE
 *      pops routine name
 *	returns FALSE
 */

int images_compatible(ip_image *im1, ip_image *im2)
{
    return (images_same_size(im1,im2) && images_same_type(im1,im2));
}



/*
 * image_not_clrim(im)
 *
 * Returns TRUE if ( image is NOT a colour image ).
 *
 * Else flags error ERR_BAD_TYPE
 *      pops routine name
 *	returns FALSE
 *
 * Note that colour images are PALETTE, RGB, RGBA and RGB16 image types.
 */

int image_not_clrim(ip_image *im)
{
    if (image_type_clrim(im)) {
	ip_log_error(ERR_BAD_TYPE);
	ip_pop_routine();
	return FALSE;
    }
    return TRUE;
}


/*
 * image_bad_type(im, <image_type>, ... , IM_TYPEMAX)
 *
 * if ( image is the same type as one of the
 *		     image types listed in argument list ) then
 *      flag error ERR_BAD_TYPE
 *      pop routine name
 *	return TRUE
 *
 *  Else returns FALSE
 */


int image_bad_type(ip_image *im, ...)
{
    va_list ap;
    int bad;
    int arg;

    bad = FALSE;
    va_start(ap,im);
    do {
	arg = va_arg(ap, int);
	if (im->type == arg)
	    bad = TRUE;
    } while (arg >= 0 && arg < IM_TYPEMAX);
    if (bad) {
	ip_log_error(ERR_BAD_TYPE);
	ip_pop_routine();
    }
    va_end(ap);
    return bad;
}



/*
 * type_real(type)
 *
 * Returns TRUE if ( image data type is one of IM_FLOAT or IM_DOUBLE )
 *
 * Else returns FALSE
 */

int type_real(int type)
{
    return ((type == IM_FLOAT) || (type == IM_DOUBLE));
}



/*
 * type_integer(type)
 *
 * Returns TRUE if ( image data type is one of
 *			IM_UBYTE or IM_SHORT or IM_LONG )
 *
 * Else returns FALSE
 */

int type_integer(int type)
{
    return ((type == IM_BYTE) || (type == IM_UBYTE) || (type == IM_SHORT)
	    || (type == IM_USHORT) || (type == IM_LONG) || (type == IM_ULONG)
#ifdef IP_INCLUDE_LONG64
	    || (type == IM_LONG64) || (type == IM_ULONG64)
#endif
	    );
}


/*
 * type_scalar(type)
 *
 * Returns true if ( image data type is one of
 *                       IM_BINARY, IM_UBYTE, IM_USHORT, IM_SHORT, IM_LONG,
 *			 IM_FLOAT or IM_DOUBLE )
 *
 * Else returns FALSE
 *
 * Note:  IM_PALETTE is not considered a scalar image type.
 */

int type_scalar(int type)
{
    return type_integer(type) || type_real(type) || (type==IM_BINARY);
}


/*
 * type_complex(type)
 *
 * Returns true if ( image data type is one of
 *                        IM_COMPLEX or IM_DCOMPLEX)
 *
 * Else returns FALSE
 */

int type_complex(int type)
{
    return (type == IM_COMPLEX) || (type == IM_DCOMPLEX);
}


/*
 * type_rgbim(type)
 *
 * Returns TRUE if ( image data type is one of:
 *                     IM_RGB, IM_RGB16, IM_RGBA )
 *
 * Else returns FALSE
 */

int type_rgbim(int type)
{
    return (type == IM_RGB || type == IM_RGBA ||type == IM_RGB16);
}



/*
 * type_clrim(type)
 *
 * Returns TRUE if ( image data type is one of:
 *                     IM_PALETTE, IM_RGB, IM_RGB16, IM_RGBA )
 *
 * Else returns FALSE
 */

int type_clrim(int type)
{
    return (type == IM_PALETTE || type_rgbim(type));
}


/*
 * Returns TRUE for those image types that are initialised with ip_anyval ul
 * fields.
 */

int type_ul(int type)
{
    return type_rgbim(type);
}



/*
 * Returns TRUE for those image types that are initialised with ip_anyval
 * ull fields.
 */

int type_ull(int type)
{
    return (type == IM_PALETTE || type == IM_BINARY || type_integer(type));
}


/*
 * Returns TRUE for those image types that are initialised with ip_anyval d
 * fields.
 */

int type_d(int type)
{
    return type_real(type) || type_complex(type);
}


/*
 * Returns TRUE for those integer image types that are signed.
 */

int type_signed(int type)
{
    return type == IM_BYTE || type == IM_SHORT || type == IM_LONG
#ifdef IP_INCLUDE_LONG64
	|| type == IM_LONG64
#endif
	;
}


/*
 *  ip_point_in_image(char *parm, ip_coord pt, im )
 *
 *  Returns TRUE if ( pt describes valid pixel in image )
 *
 *  Else flags the error ERR_BAD_COORD
 *       pops routine name
 *	returns FALSE
 */

int ip_point_in_image(char *parm, ip_coord2d pt, ip_image *im)
{
    if (pt.x < 0 || pt.x >= im->size.x || pt.y < 0 || pt.y >= im->size.y) {
	ip_log_error(ERR_PARM_BAD_COORD, parm, im->size.x, im->size.y, pt.x, pt.y);
	ip_pop_routine();
	return FALSE;
    }
    return TRUE;
}



static int val_in_image(ip_image *im, double v)
{
    return (v >= ip_type_min(im->type)) && (v <= ip_type_max(im->type));
}


int in_image_val(ip_image *im, double v1, double v2)
{
    return (val_in_image(im,v1) && val_in_image(im,v2));
}


/*
 * ip_value_in_image(char *parm, double val, im)
 *
 * Returns TRUE if ( val is a valid pixel value for the image im )
 *
 * Else flags the error ERR_PARM_BAD_VALUE
 *       pops routine name
 *	returns FALSE
 *
 * For example, 256 is not a valid pixel value for a BYTE image but is ok
 * for a SHORT image.
 *
 * parm is an ascii description of the parameter value passed in.
 */

int ip_value_in_image(char *desc, double v, ip_image *im)
{
    if (NOT val_in_image(im,v)) {
	ip_log_error(ERR_PARM_BAD_VALUE, desc, ip_type_name(im->type), v);
	ip_pop_routine();
	return FALSE;
    }
    return TRUE;
}


/* Checks that the ip_anyval value is within allowed image pixel values */

int ip_anyval_in_image(char *desc, ip_anyval v, ip_image *im)
{
    int res = FALSE;
    double badval = 0.0;

    /* The value in v cannot be out-of-range for these image types. */
    if (im->type == IM_LONG64 || im->type == IM_ULONG64
	|| im->type == IM_DOUBLE || im->type == IM_DCOMPLEX)
	return TRUE;

    if (type_ull(im->type) || im->type == IM_FLOAT) {
	double val;

	if (im->type == IM_FLOAT)
	    val = v.d[0];
	else if (type_signed(im->type))
	    val = (double)(int64_t)v.ull[0];
	else
	    val = (double)v.ull[0];

	if (NOT val_in_image(im, val))
	    badval = val;
	else
	    res = TRUE;

    }else if (im->type == IM_COMPLEX) {

	if (NOT val_in_image(im, v.d[0])) {
	    badval = v.d[0];
	}else if (NOT val_in_image(im, v.d[1])) {
	    badval = v.d[1];
	}else{
	    res = TRUE;
	}

    }else if (type_rgbim(im->type)) {

	if (NOT val_in_image(im, (double)v.ul[0])) {
	    badval = v.ul[0];
	}else if (NOT val_in_image(im, (double)v.ul[1])) {
	    badval = v.ul[1];
	}else if (NOT val_in_image(im, (double)v.ul[2])) {
	    badval = v.ul[2];
	}else if (im->type == IM_RGBA && NOT val_in_image(im, (double)v.ul[3])) {
	    badval = v.ul[3];
	}else{
	    res = TRUE;
	}

    }

    if (NOT res) {
	ip_log_error(ERR_PARM_BAD_VALUE, desc, ip_type_name(im->type), badval);
	ip_pop_routine();
    }

    return res;
}

/*
 * ip_box_in_image(char *parm, ip_box b, im)
 *
 * Returns TRUE if ( b describes a box that lies entirely
 *			within the image boundaries)
 *
 * Else flags the error ERR_PARM_BAD_BOX
 *      pops routine name
 *      returns FALSE
 */

int ip_box_in_image(char *parm, ip_box b, ip_image *im)
{
    if (b.origin.x >= 0 && b.origin.x < im->size.x &&
	b.origin.y >= 0 && b.origin.y < im->size.y &&
	b.size.x >= 0 && b.size.y >= 0
	     && (b.origin.x + b.size.x <= im->size.x)
             && (b.origin.y + b.size.y <= im->size.y)) {
	return TRUE;
    }

    ip_log_error(ERR_PARM_BAD_BOX, parm, im->size.x, im->size.y,
			  b.origin.x, b.origin.y, b.size.x, b.size.y);
    ip_pop_routine();
    return FALSE;
}


/*
 * ip_flag_ok(int flag, int <ok_flag>, ... , LASTFLAG)
 *
 * Returns TRUE if ( flag is equal to some flag in the argument list
 *		      or if flag is set to NO_FLAG )
 *
 * Else flags the error ERR_BAD_FLAG
 *       pops routine name
 *	returns FALSE
 */

int ip_flag_ok(int flag, ...)
{
    va_list ap;
    int ok;

    ok = FALSE;
    va_start(ap,flag);
    if (flag == NO_FLAG)
	ok = TRUE;
    else{
	int arg;

	while ((arg = va_arg(ap,int)) != LASTFLAG)
	    if (flag == arg) ok = TRUE;
    }
    va_end(ap);
    if (NOT ok) {
	ip_log_error(ERR_BAD_FLAG2,flag,flag);
	ip_pop_routine();
    }
    return ok;
}



/*
 * ip_flag_bits_unused(int flag, int unused)
 *
 * Returns TRUE if ( flag has no bits set that are in unused )
 *
 * Else flags the error ERR_BAD_FLAG
 *       pops routine name
 *	returns FALSE
 */

int ip_flag_bits_unused(int flag, int unused)
{
    if (flag & unused) {
	ip_log_error(ERR_BAD_FLAG);
	ip_pop_routine();
	return FALSE;
    }
    return TRUE;
}



/*
 * ip_parm_powof2(char *str, int parm)
 *
 * Returns TRUE if ( parm value is a power of two )
 *
 * Else flags the error ERR_PARM_NOT_POWER_2
 *       pops routine name
 *	returns FALSE
 *
 * str should be set to a short descriptive name of the parameter.
 */

int ip_parm_powof2(char *parm, int v)
{
    int savev;

    savev = v;
    while (v && (NOT (v & 0x01)))
	v >>= 1;
    if ((v & 0x01) && (v != 1)) {
	ip_log_error(ERR_PARM_NOT_POWER_2, parm, savev);
	ip_pop_routine();
	return FALSE;
    }
    return TRUE;
}


/*
 * ip_parm_inrange(char *str, double parm, double min, dounle max)
 *
 * Returns TRUE if (parm >= min and parm <= max)
 *
 * Else flags the error ERR_OOR_PARAMETER
 *       pops routine name
 *	returns FALSE
 *
 * str should be set to a short descriptive name of the parameter.
 */

int ip_parm_inrange(char *parm, double val, double min, double max)
{
    if (val < min || val > max) {
	ip_log_error(ERR_PARM_OOR, parm, val, min, max);
	ip_pop_routine();
	return FALSE;
    }
    return TRUE;
}


/*
 * ip_parm_lesseq(char *str, double parm, double max)
 *
 * Returns TRUE if (parm value is less than or equal to the value max)
 *
 * Else flags the error ERR_TOOBIG_PARAMETER
 *      pops routine name
 *	returns FALSE
 *
 * str should be set to a short descriptive name of the parameter.
 */

int ip_parm_lesseq(char *parm, double val, double max)
{
    if (val > max) {
	ip_log_error(ERR_PARM_TOOBIG, parm, val, "or equal to ",  max);
	ip_pop_routine();
	return FALSE;
    }
    return TRUE;
}


/*
 * ip_parm_greatereq(char *str, double parm, double min)
 *
 * Returns TRUE if (parm value is greater than or equal the value min)
 *
 * Else flags the error ERR_TOOSMALL_PARAMETER
 *      pops routine name
 *	returns FALSE
 *
 * str should be set to a short descriptive name of the parameter.
 */

int ip_parm_greatereq(char *parm, double val, double min)
{
    if (val < min) {
	ip_log_error(ERR_PARM_TOOSMALL, parm, val, "or equal to ", min);
	ip_pop_routine();
	return FALSE;
    }
    return TRUE;
}


/*
 * ip_parm_less(char *str, double parm, double max)
 *
 * Returns TRUE if (parm value is less than the value max)
 *
 * Else flags the error ERR_TOOBIG_PARAMETER
 *      pops routine name
 *	returns FALSE
 *
 * str should be set to a short descriptive name of the parameter.
 */

int ip_parm_less(char *parm, double val, double max)
{
    if (val >= max) {
	ip_log_error(ERR_PARM_TOOBIG, parm, val, "", max);
	ip_pop_routine();
	return FALSE;
    }
    return TRUE;
}


/*
 * ip_parm_greater(char *str, double parm, double min)
 *
 * Returns TRUE if (parm value is greater than the value min)
 *
 * Else flags the error ERR_TOOSMALL_PARAMETER
 *      pops routine name
 *	returns FALSE
 *
 * str should be set to a short descriptive name of the parameter.
 */

int ip_parm_greater(char *parm, double val, double min)
{
    if (val <= min) {
	ip_log_error(ERR_PARM_TOOSMALL, parm, val, "", min);
	ip_pop_routine();
	return FALSE;
    }
    return TRUE;
}



/*
 * ip_parm_mult2(char *str, int parm)
 *
 * Returns TRUE if ( parm value is a multiple of two )
 *
 * Else flags the error ERR_PARM_NOT_MULT_2
 *      pops routine name
 *	returns FALSE
 *
 * str should be set to a short descriptive name of the parameter.
 *
 */

int ip_parm_mult2(char *parm, int val)
{
    if (val & 0x01) {
	ip_log_error(ERR_PARM_NOT_MULT_2, parm, val);
	ip_pop_routine();
	return FALSE;
    }
    return TRUE;
}



/*
 * ip_parm_odd(char *str, int parm)
 *
 * Returns TRUE if ( parm value is odd )
 *
 * Else flags the error ERR_PARM_NOT_ODD
 *      pops routine name
 *	returns FALSE
 *
 * str should be set to a short descriptive name of the vparameter.
 */

int ip_parm_odd(char *parm, int val)
{
    if (NOT (val & 0x01)) {
	ip_log_error(ERR_PARM_NOT_ODD, parm, val);
	ip_pop_routine();
	return FALSE;
    }
    return TRUE;
}



/*
 * ip_parm_coord_in_box( char *str, coord, box)
 *
 * Returns TRUE if (coord describes a point within box)
 *
 * Else flags the error ERR_PARM_NOT_IN_BOX
 *      pops routine name
 *      returns FALSE
 *
 * str should be set to a one-word descriptive name of the coord.
 */

int ip_parm_point_in_box(char *parm, ip_coord2d point, ip_box box)
{
    ip_coord2d botr = ip_box_botr(box);

    if (point.x < box.origin.x || point.y < box.origin.y
	|| point.x > botr.x || point.y > botr.y) {
	ip_log_error(ERR_PARM_NOT_IN_BOX, parm, point.x, point.y,
		     box.origin.x, box.origin.y, box.size.x, box.size.y);
	ip_pop_routine();
	return FALSE;
    }
    return TRUE;
}
