/*

   Module: ip/zoom.c
   Author: M. J. Cree

   Copyright (C) 1995-2000, 2008, 2014 by Michael J. Cree

   Image resizing/zooming.

ENTRY POINTS:

   im_expand()

   (others require conversion to new IP interface)

*/


#include <stdlib.h>
#include <ip/types.h>
#include <ip/image.h>
#include <ip/error.h>
#include <ip/flags.h>
#include <ip/proto.h>

/* #define DEBUG */
#if 0
#include <mjc/debug.h>
#else
#define D(x)
#endif

/* To indicate that internal routines to this module are calling up
   im_reducex() - this allows BINARY images to be treated as BYTE
   images and should be returned with values in between 0 and 255 to
   keep precision. */

#define FLG_INTERNAL 0x4000


static ip_image *imreduce(ip_image *src, int xskip, int yskip, int flag);
static ip_image *imexpand(ip_image * src, int xfactor, int yfactor, int flag );
static void zoom_bilinear(ip_image *dest, ip_image *src);


/*

NAME:

   im_sample() \- Sub-sample an image

PROTOTYPE:

   #include <ip/ip.h>

   ip_image * im_sample( im, skip )
   ip_image * im;
   int skip;

DESCRIPTION:

   Sub-sample the image 'im' by the factor 'skip'.  This routine
   calls im_reducex() as

\|   im_reducex(im, skip, skip, FLG_SAMPLE);

   Therefore see im_reducex() for more info.

ERRORS:

   ERR_OUT_OF_MEM
   ERR_OOR_PARAMETER
   ERR_BAD_PARAMETER

SEE ALSO:

   im_reduce()   im_reducex()   im_expand()   im_zoom()

*/


ip_image *im_sample(ip_image *im, int skip)
{
    ip_log_routine(__func__);
    return imreduce(im, skip, skip, FLG_SAMPLE);
}


/*

NAME:

   im_reduce() \- Create image of reduced size

PROTOTYPE:

   #include <ip/ip.h>

   ip_image * im_reduce( im, skip , flags )
   ip_image * im;
   int skip;
   int flags;

DESCRIPTION:

   Makes a new image which is reduced (in size) from the existing
   image 'im'.  The value of 'skip' specifies the sampling rate. For
   example, if 'skip' is two then the image produced is half the size
   (in each dimension) of 'im'.

   'Flag' may take the value:

\&   FLG_SAMPLE Each pixel 'skip' apart is sampled.
\&   FLG_AVERAGE Each pixel is averaged from neighbouring pixels.

\&-   Returns NULL if image allocation failed or any other error occurs.

   Implemented as a call to im_reducex() like so:

\|   im_reducex(im, skip, skip, flags);

ERRORS:

   ERR_OUT_OF_MEM
   ERR_OOR_PARAMETER
   ERR_BAD_FLAG

LIMITATIONS:

\-  FLG_SAMPLE is the only flag supported on colour image types.

SEE ALSO:

   im_sample()   im_expand()   im_reducex()   im_zoom()

*/


ip_image *im_reduce(ip_image *src, int skip, int flag)
{
    ip_log_routine(__func__);
    return imreduce(src, skip, skip, flag);
}


/*

NAME:

   im_reducex() \- Create reduced size image

PROTOTYPE:

   #include <.h>

   ip_image * im_reducex( src, xskip, yskip, flag )
   ip_image *src;
   int xskip;
   int yskip;
   int flag;

DESCRIPTION:

   This routine is like im_reduce() except you can specify a different
   'skip' for the 'x' and 'y' directions.  The resultant image size is
   the floor of the image size divided by the skip (for each
   dimension).  This routine takes the same flags

\&   FLG_SAMPLE   Sample image (equivalent to nearest-neighbour).
\&   FLG_AVERAGE  Average 'xskip' x 'yskip' block of pixels.

\&-   Returns NULL if image allocation failed or any other error occurs.

LIMITATIONS:

   Only FLG_SAMPLE is permissable on colour image types.

ERRORS:

   ERR_OUT_OF_MEM
   ERR_TOOSMALL_PARAMETER
   ERR_BAD_PARAMETER
   ERR_BAD_FLAG

SEE ALSO:

   im_reduce() im_sample()   im_expand()   im_zoom()

*/

ip_image *im_reducex(ip_image *src, int xskip, int yskip, int flag)
{
    ip_log_routine(__func__);
    return imreduce(src, xskip, yskip, flag);
}

/* Helper routine imreduce() follows */

/*
 *  t = type
 *  b = short type specifier for image access
 *  v = type of mean summation variable
 */
#define IMROW_AVERAGE(t,b,ts) {			\
    ts npixels = xskip*yskip;			\
    ts npixelsd2 = npixels/2;			\
    ts sum;					\
    int jskip, iskip;				\
    t *sp, *dp;					\
    jskip = j * yskip;				\
    dp = IM_ROWADR(dest, j, v);			\
    for (int i=0; i<size.x; i++) {		\
	sum = 0;				\
	iskip = i*xskip;			\
	for (int l=0; l<yskip; l++) {		\
	    sp = IM_ROWADR(src,jskip+l,b);	\
	    sp += iskip;			\
	    for (int k=0; k<xskip; k++)		\
		sum += *sp++;			\
	}					\
	*dp++ = (t)((sum+npixelsd2)/npixels);	\
    }						\
}


#define IMROW_AVERAGE_BINARY {				\
    int npixels = xskip*yskip;				\
    int sum;						\
    int jskip, iskip;					\
    uint8_t *sp, *dp;					\
    jskip = j * yskip;					\
    dp = IM_ROWADR(dest,j,ub);				\
    for (int i=0; i<size.x; i++) {			\
	sum = 0;					\
	iskip = i*xskip;				\
	for (int l=0; l<yskip; l++) {			\
	    sp = IM_ROWADR(src,jskip+l,ub);		\
	    sp += iskip;				\
	    for (int k=0; k<xskip; k++)			\
		sum += *sp++;				\
	}						\
	*dp++ = (uint8_t)((sum/npixels) < 128 ? 0 : 255);	\
    }								\
}

#define IMROW_AVERAGE_REAL(t,b) {		\
    double npixels = xskip*yskip;		\
    int jskip, iskip;				\
    t *sp, *dp;					\
    jskip = j * yskip;				\
    dp = IM_ROWADR(dest,j,v);			\
    for (int i=0; i<size.x; i++) {		\
	double sum;				\
	sum = 0;				\
	iskip = i*xskip;			\
	for (int l=0; l<yskip; l++) {		\
	    sp = IM_ROWADR(src,jskip+l,b);	\
	    sp += iskip;			\
	    for (int k=0; k<xskip; k++)		\
		sum += *sp++;			\
	}					\
	*dp++ = sum/npixels;			\
    }						\
}

#define IMROW_AVERAGE_COMPLEX(t,d) {		\
    double npixels = xskip*yskip;		\
    int jskip, iskip;				\
    t *sp, *dp;					\
    jskip = j * yskip;				\
    dp = IM_ROWADR(dest,j,v);			\
    for (int i=0; i<size.x; i++) {		\
	double sumr,sumi;			\
	sumr = sumi = 0;			\
	iskip = i*xskip;			\
	for (int l=0; l<yskip; l++) {		\
	    sp = IM_ROWADR(src,jskip+l,d);	\
	    sp += iskip;			\
	    for (int k=0; k<xskip; k++) {	\
		sumr += sp->r;			\
		sumi += sp->i;			\
		sp++;				\
	    }					\
	}					\
	dp->r = sumr/npixels;			\
	dp->i = sumi/npixels;			\
	dp++;					\
    }						\
}

#define IMROW_AVERAGE_RGB(t,d) {		\
    uint32_t npixels = xskip*yskip;		\
    uint32_t npixelsd2 = npixels/2;		\
    int jskip, iskip;				\
    t *sp, *dp;					\
    jskip = j * yskip;				\
    dp = IM_ROWADR(dest,j,v);			\
    for (int i=0; i<size.x; i++) {		\
	uint32_t sumr,sumg,sumb;		\
	sumr = sumg = sumb = 0;			\
	iskip = i*xskip;			\
	for (int l=0; l<yskip; l++) {		\
	    sp = IM_ROWADR(src,jskip+l,d);	\
	    sp += iskip;			\
	    for (int k=0; k<xskip; k++) {	\
		sumr += sp->r;			\
		sumg += sp->g;			\
		sumb += sp->b;			\
		sp++;				\
	    }					\
	}					\
	dp->r = (sumr+npixelsd2)/npixels;	\
	dp->g = (sumg+npixelsd2)/npixels;	\
	dp->b = (sumb+npixelsd2)/npixels;	\
	dp++;					\
    }						\
}

#define IMROW_SAMPLE(t) {			\
    t *spix, *dpix;\
    dpix = IM_ROWADR(dest,j,v);			\
    spix = IM_ROWADR(src,j*yskip+yoffset,v);	\
    spix += xoffset;				\
    for (int i=0; i<size.x; i++) {		\
	*dpix++ = *spix;			\
	spix += xskip;				\
    }						\
}


static ip_image *imreduce(ip_image *src, int xskip, int yskip, int flag)
{
    ip_image *dest;
    int internal;
    ip_coord2d size;

    internal = FALSE;		/* Internal routines to this module can set */
    if (flag & FLG_INTERNAL) {	/*   FLG_INTERNAL to say that FLG_AVERAGE */
	internal = TRUE;		/*   may be used on BINARY images - an */
	flag &= ~FLG_INTERNAL;	/*   invalid BINARY image is returned!  */
    }

    if (NOT ip_flag_ok(flag,FLG_SAMPLE,FLG_AVERAGE,LASTFLAG))
	return NULL;

    if (NOT ip_parm_inrange("x_skip", xskip, 1, src->size.x))
	return NULL;

    if (NOT ip_parm_inrange("y_skip", yskip, 1, src->size.y))
	return NULL;

    dest = NULL;

    if (xskip == 1 && yskip == 1) {
	dest = im_copy(src);
	goto exit_rx;
    }

    size.x = src->size.x / xskip;
    size.y = src->size.y / yskip;

    if ((dest = ip_alloc_image(src->type, size)) == NULL)
	goto exit_rx;

    if (dest->type == IM_PALETTE)
	im_palette_copy(dest,src);

    D(bug("imreduce: [%d,%d] skip by (%d,%d) to get [%d,%d].\n",
	  src->size.x, src->size.y, xskip, yskip, size.x, size.y));
    D(bug("          flags: AVERAGE=%c  SAMPLE=%c   INTERNAL=%c\n",
	  "FT"[flag==FLG_AVERAGE],"FT"[flag==FLG_SAMPLE],"FT"[internal]));

    if (flag == FLG_AVERAGE) {
	int imtype = src->type;

	if (imtype == IM_BINARY && internal) imtype = IM_BYTE;

	for (int j=0; j<size.y; j++) {

	    switch (imtype) {
	    case IM_BINARY:
		IMROW_AVERAGE_BINARY
		break;
	    case IM_BYTE:
		IMROW_AVERAGE(int8_t, b, int32_t)
		break;
	    case IM_UBYTE:
		IMROW_AVERAGE(uint8_t, ub, int32_t)
		break;
	    case IM_SHORT:
		IMROW_AVERAGE(int16_t, s, int32_t)
		break;
	    case IM_USHORT:
		IMROW_AVERAGE(uint16_t, us, int32_t)
		break;
	    case IM_LONG:
		IMROW_AVERAGE(int32_t, l, int64_t)
		break;
	    case IM_ULONG:
		IMROW_AVERAGE(uint32_t, ul, int64_t)
		break;
	    case IM_FLOAT:
		IMROW_AVERAGE_REAL(float, f)
		break;
	    case IM_DOUBLE:
		IMROW_AVERAGE_REAL(double, d)
		break;
	    case IM_COMPLEX:
		IMROW_AVERAGE_COMPLEX(ip_complex, c)
		break;
	    case IM_DCOMPLEX:
		IMROW_AVERAGE_COMPLEX(ip_dcomplex, dc)
		break;
	    case IM_RGB:
		IMROW_AVERAGE_RGB(ip_rgb, r);
		break;
	    case IM_RGB16:
		IMROW_AVERAGE_RGB(ip_lrgb, r16);
		break;
	    default:
		break;
	    }
	}
    }

    if (flag == FLG_SAMPLE) {
	int xoffset, yoffset;

	xoffset = (xskip - 1) / 2;
	yoffset = (yskip - 1) / 2;

	D(bug("          offset = (%d,%d)\n",xoffset,yoffset));

	for (int j=0; j<size.y; j++) {

	    switch (src->type) {
	    case IM_BINARY:
	    case IM_UBYTE:
	    case IM_PALETTE:
		IMROW_SAMPLE(uint8_t);
		break;
	    case IM_BYTE:
		IMROW_SAMPLE(int8_t);
		break;
	    case IM_SHORT:
		IMROW_SAMPLE(int16_t);
		break;
	    case IM_USHORT:
		IMROW_SAMPLE(uint16_t);
		break;
	    case IM_LONG:
		IMROW_SAMPLE(int32_t);
		break;
	    case IM_ULONG:
		IMROW_SAMPLE(uint32_t);
		break;
	    case IM_FLOAT:
		IMROW_SAMPLE(float);
		break;
	    case IM_DOUBLE:
		IMROW_SAMPLE(double);
		break;
	    case IM_COMPLEX:
		IMROW_SAMPLE(ip_complex);
		break;
	    case IM_DCOMPLEX:
		IMROW_SAMPLE(ip_dcomplex);
		break;
	    case IM_RGB:
		IMROW_SAMPLE(ip_rgb);
		break;
	    case IM_RGB16:
		IMROW_SAMPLE(ip_lrgb);
		break;
	    default:
		break;
	    }
	}
    }

#ifdef DEBUG
    {UBYTE *s,*d;
	s = im_rowadr(src,0);
	d = im_rowadr(dest,0);
	D(bug("dest: mem range %p -- %p\n",d, d+(size.x*size.y)));
	D(bug("src:  mem range %p -- %p\n",s, s+(src->size.x*src->size.y)));
    }
#endif


 exit_rx:
    ip_pop_routine();
    return dest;
}


#undef IMROW_AVERAGE
#undef IMROW_AVERAGE_BINARY
#undef IMROW_AVERAGE_REAL
#undef IMROW_AVERAGE_COMPLEX
#undef IMROW_SAMPLE


/*

NAME:

   im_expand() \- Create image of expanded size

PROTOTYPE:

   #include <ip/ip.h>

   ip_image *  im_expand( src, factor, flag )
   ip_image *src;
   int factor;
   int flag;

DESCRIPTION:

   Enlarge the image 'src' and return this new image.  One day this be
   quite a general expanding routine but at present is pretty limited.
   'Flag' can be:

\&   FLG_ZOOM Enlarge by nearest neighbour sampling.

\&   FLG_INTERP Use bilinear interpolation.

ERRORS:

   ERR_OUT_OF_MEM
   ERR_TOOSMALL_PARAMETER
   ERR_PARM_NOT_MULT_2
   ERR_BAD_FLAG

LIMITATIONS:

   Can only zoom images by a factor that is a multiple of two at
   present.

BUGS:

   Somewhere between doing im_reduce() followed by im_expand() there
   is a bug, since the image doesn''t expand up to exactly the right
   place in the image.  This is only really noticeable if you reduce
   the image by a 'skip' of more than two then enlarge it by the same
   amount.  The routines im_reducex() and im_zoom() do not have this
   bug.

SEE ALSO:

   im_reduce() im_sample()  im_reducex()   im_zoom()

*/


ip_image *im_expand(ip_image * src, int factor , int flag)
{
    ip_log_routine(__func__);
    return imexpand(src, factor, factor, flag);
}


ip_image *im_expandx(ip_image * src, int xfactor, int yfactor, int flag)
{
    ip_log_routine(__func__);
    return imexpand(src, xfactor, yfactor, flag);
}


#define IMROW_ZOOM(t)				\
    {						\
	t *dp = im_rowadr(dest,j);		\
	t *sp = im_rowadr(src,j/yfactor);	\
	int k=0;				\
	for (int i=0; i<size.x; i++) {		\
	    *dp++ = *sp;			\
	    k++;				\
	    if (k == xfactor) {			\
		sp++;				\
		k=0;				\
	    }					\
	}					\
    }

static ip_image *imexpand(ip_image * src, int xfactor, int yfactor, int flag)
{
    ip_image *dest;
    ip_coord2d size;

    if (NOT ip_flag_ok(flag,FLG_ZOOM,FLG_INTERP,LASTFLAG))
	return NULL;

    if (NOT ip_parm_greatereq("xfactor", xfactor, 1)) {
	return NULL;
    }

    if (NOT ip_parm_greatereq("yfactor", yfactor, 1)) {
	return NULL;
    }

    dest = NULL;

    if ((src->type == IM_BINARY || image_type_clrim(src))
						&& flag != FLG_ZOOM) {
	ip_log_error(ERR_BAD_FLAG);
	goto exit_e;
    }

    size.x = src->size.x*xfactor;
    size.y = src->size.y*yfactor;

    if ((dest = ip_alloc_image(src->type, size)) == NULL)
	goto exit_e;

    if (dest->type == IM_PALETTE)
	im_palette_copy(dest,src);

    if (flag == FLG_ZOOM) {
	for (int j=0; j<dest->size.y; j++) {
	    switch (dest->type) {
	    case IM_BINARY:
	    case IM_UBYTE:
	    case IM_PALETTE:
		IMROW_ZOOM(uint8_t);
		break;
	    case IM_BYTE:
		IMROW_ZOOM(int8_t);
		break;
	    case IM_SHORT:
		IMROW_ZOOM(int16_t);
		break;
	    case IM_USHORT:
		IMROW_ZOOM(uint16_t);
		break;
	    case IM_LONG:
		IMROW_ZOOM(int32_t);
		break;
	    case IM_ULONG:
		IMROW_ZOOM(uint32_t);
		break;
	    case IM_FLOAT:
		IMROW_ZOOM(float);
		break;
	    case IM_DOUBLE:
		IMROW_ZOOM(double);
		break;
	    case IM_COMPLEX:
		IMROW_ZOOM(ip_complex);
		break;
	    case IM_DCOMPLEX:
		IMROW_ZOOM(ip_dcomplex);
		break;
	    case IM_RGB:
		IMROW_ZOOM(ip_rgb);
		break;
	    case IM_RGB16:
		IMROW_ZOOM(ip_lrgb);
		break;
	    default:
		break;
	    }
	}
    }

    if (flag == FLG_INTERP) {
	zoom_bilinear(dest, src);
    }

 exit_e:
    ip_pop_routine();
    return dest;

}


/*

NAME:

   im_zoom() \- Zoom an image up or down

PROTOTYPE:

   #include <.h>

   ip_image * im_zoom( src, xfactor, yfactor, flag )
   ip_image *src;
   double xfactor;
   double yfactor;
   int flag;

DESCRIPTION:

   Creates an image that is a zoomed version of 'src'.  'Xfactor' and
   'yfactor' specify the zoom amount for the 'x' and 'y' directions,
   respectively.  Note that they may be fractional, so images may be
   zoomed by arbitrary amounts.  'Flag' must currently be:

\&   FLG_BILINEAR  Bilinear interpolation.

\&-   If for any reason an error occurs then NULL is returned.

ERRORS:

   ERR_BAD_FLAG
   ERR_TOOSMALL_PARAMETER
   ERR_BAD_PARAMETER
   ERR_BAD_TYPE

LIMITATIONS:

\-   Not implemented for COMPLEX nor colour images.
\-   Single precision arithmetic is used even if a DOUBLE image
       is being zoomed.

SEE ALSO:

   im_reduce() im_sample()  im_reducex()    im_expand()

*/

ip_image *im_zoom(ip_image *src, double xfactor, double yfactor, int flag)
{
    ip_image *dest, *tmp;
    int xredf, yredf;
    int dx, dy;

    ip_log_routine(__func__);

    if (NOT ip_flag_ok(flag,FLG_BILINEAR,LASTFLAG))
	return NULL;

    dest = NULL;

    if (NOT ip_parm_greater("x_factor", xfactor, 0.0)) {
	return NULL;
    }

    if (NOT ip_parm_greater("y_factor", yfactor, 0.0)) {
	return NULL;
    }

    if (NOT (src->type == IM_BINARY ||
	     image_type_integer(src) || image_type_real(src) || src->type == IM_RGB)) {
	ip_log_error(ERR_BAD_TYPE);
	goto exit_z;
    }

    dx = src->size.x * xfactor;
    dy = src->size.y * yfactor;

    if (dx == 0 || dy == 0) {
	ip_log_error(ERR_BAD_PARAMETER);
	goto exit_z;
    }

    if (dx == src->size.x && dy == src->size.y) {
	dest = im_copy(src);
	goto exit_z;
    }

    xredf = 1.0 / xfactor;
    if (xredf == 0) xredf = 1.0;
    yredf = 1.0 / yfactor;
    if (yredf == 0) yredf = 1.0;

    if (xredf == 1 && yredf == 1) {

	if ((dest = ip_alloc_image(src->type, (ip_coord2d){dx, dy})) == NULL)
	    goto exit_z;

	zoom_bilinear(dest, src);

    }else{

	if ((tmp = im_reducex(src, xredf, yredf,
			      FLG_AVERAGE|FLG_INTERNAL)) == NULL)
	    goto exit_z;

	if (tmp->size.x == dx && tmp->size.y == dy) {
	    dest = tmp;
	}else{
	    if ((dest = ip_alloc_image(src->type, (ip_coord2d){dx, dy})) == NULL) {
		ip_free_image(tmp);
		goto exit_z;
	    }
	    zoom_bilinear(dest, tmp);
	    ip_free_image(tmp);
	}
    }

    /* If BINARY image and doing interpolation, threshold image
       to get back to a true BINARY image */

    if (dest->type == IM_BINARY && flag == FLG_BILINEAR) {
	uint8_t *pixptr;

	for (int j=0; j<dest->size.y; j++) {
	    pixptr = IM_ROWADR(dest, j, ub);
	    for (int i=0; i<dest->size.x; i++) {
		*pixptr = *pixptr>=128 ? 255 : 0;
		pixptr++;
	    }
	}
    }

 exit_z:
    ip_pop_routine();
    return dest;
}



static void zoom_bilinear(ip_image *dest, ip_image *src)
{
    float xgap, ygap, xoffset, yoffset, xpos, ypos;
    double x, y;

    xgap = (float)src->size.x / dest->size.x;
    ygap = (float)src->size.y / dest->size.y;
    xoffset = xgap / 2.0f - 0.5f;
    yoffset = ygap / 2.0f - 0.5f;

/* Get a particular pixel or zero if outside image bounds */
#define GetSrcPixel(im, xx, yy, type,t)			\
    (  (xx) >= im->size.x || (yy) >= im->size.y )	\
        ? (type) 0					\
        : IM_PIXEL(im,xx,yy,t)

/* Get a particular pixel or zero if outside image bounds */
#define GetSrcPixel_RGB(im, xx, yy, type,t)		\
    (  (xx) >= im->size.x || (yy) >= im->size.y )	\
        ? (type) {0, 0, 0}				\
        : IM_PIXEL(im,xx,yy,t)


/* Find four nearest neighbours to pixel (i,j) and interpolate. */
#define Interpolation(type,t)                                \
        {                                                        \
            type tl_pixel, tr_pixel, bl_pixel, br_pixel;         \
                                                                 \
            tl_pixel = GetSrcPixel(src, (int)xpos, (int)ypos, type,t);    \
            tr_pixel = GetSrcPixel(src, ((int)xpos+1), (int)ypos, type,t);  \
            bl_pixel = GetSrcPixel(src, (int)xpos, ((int)ypos+1), type,t);  \
            br_pixel = GetSrcPixel(src, ((int)xpos+1),((int)ypos+1), type,t); \
            IM_PIXEL(dest,i,j,t) = (type)                        \
		       ( x * y * tl_pixel +              \
                               (1 - x) * y * tr_pixel +          \
                               x * (1 - y) * bl_pixel +          \
                               (1 - x) * (1 - y) * br_pixel );   \
	}

/* Find four nearest neighbours to pixel (i,j) and interpolate. */
#define Interpolation_RGB(type,t)                                \
        {                                                        \
            type tl_pixel, tr_pixel, bl_pixel, br_pixel;         \
	    type *dptr = IM_ROWADR(dest, j, v);			 \
	    dptr += i;						 \
                                                                 \
            tl_pixel = GetSrcPixel_RGB(src, (int)xpos, (int)ypos, type,t); \
            tr_pixel = GetSrcPixel_RGB(src, ((int)xpos+1), (int)ypos, type,t); \
            bl_pixel = GetSrcPixel_RGB(src, (int)xpos, ((int)ypos+1), type,t); \
            br_pixel = GetSrcPixel_RGB(src, ((int)xpos+1),((int)ypos+1), type,t); \
            dptr->r =							\
		( x * y * tl_pixel.r + (1 - x) * y * tr_pixel.r +	\
		  x * (1 - y) * bl_pixel.r + (1 - x) * (1 - y) * br_pixel.r ); \
            dptr->g =							\
		( x * y * tl_pixel.g + (1 - x) * y * tr_pixel.g +	\
		  x * (1 - y) * bl_pixel.g + (1 - x) * (1 - y) * br_pixel.g ); \
            dptr->b =							\
		( x * y * tl_pixel.b + (1 - x) * y * tr_pixel.b +	\
		  x * (1 - y) * bl_pixel.b + (1 - x) * (1 - y) * br_pixel.b ); \
	}


    for (int j=0; j<dest->size.y; j++) {
	for (int i=0; i<dest->size.x; i++) {

	    xpos = i * xgap + xoffset;
	    if (xpos < 0) xpos = 0;
	    if (xpos >= src->size.x) xpos = src->size.x-1;

	    ypos = j * ygap + yoffset;
	    if (ypos < 0) ypos = 0;
	    if (ypos >= src->size.y) ypos = src->size.y-1;

	    x = ((int)xpos + 1) - xpos;
	    y = ((int)ypos + 1) - ypos;

	    switch (src->type) {
	    case IM_BINARY:
	    case IM_UBYTE:
		Interpolation(uint8_t, ub);
		break;
	    case IM_BYTE:
		Interpolation(int8_t, b);
		break;
	    case IM_USHORT:
		Interpolation(uint16_t, us);
		break;
	    case IM_SHORT:
		Interpolation(int16_t, s);
		break;
	    case IM_LONG:
		Interpolation(int32_t, l);
		break;
	    case IM_FLOAT:
		Interpolation(float,f);
		break;
	    case IM_DOUBLE:
		Interpolation(double,d);
		break;
	    case IM_RGB:
		Interpolation_RGB(ip_rgb, r);
		break;
	    default:
		break;
	    }
	}
    }
}
