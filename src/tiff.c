/*
   Module: ip/tiff.c
   Author: M.J.Cree

   Copyright (C) 1996-2013, 2015, 2016 Michael Cree

   Routines for reading in and saving tiff images.
*/


#include <stdlib.h>
#include <stdio.h>
#include <errno.h>

#include <time.h>

#include <ip/ip.h>
#include <ip/stats.h>
#include <ip/tiff.h>

#include <tiff.h>

/* Define IP_OPHTHALTAG to include the extra OPHTHAL TAG support. */
#ifdef IP_OPHTHALTAG
#include <ip/xtiffio.h>
#else
#include <tiffio.h>
#endif

#include "internal.h"
#include "utils.h"

static ip_image*rd_tiff_image(ip_tiff_handle *th, int first_row, int nrows);

/* These must be in the same order as defined in tiff.h */
static uint32 tiff_tag[] = {
   0,				/* place holder */
   0,				/* TAG_MAXMIN */
   0,				/* TAG_MINIMUM */
   0,				/* TAG_MAXIMUM */
   TIFFTAG_SOFTWARE,		/* TAG_SOFTWARE */
   TIFFTAG_DOCUMENTNAME,	/* TAG_DOCNAME */
   TIFFTAG_IMAGEDESCRIPTION,	/* TAG_DESCRIPTION */
   TIFFTAG_HOSTCOMPUTER,	/* TAG_HOST */
   TIFFTAG_DATETIME,		/* TAG_DATE */
   TIFFTAG_XRESOLUTION,     	/* TAG_XRES */
   TIFFTAG_YRESOLUTION,		/* TAG_YRES */
#ifdef IP_OPHTHALTAG
   TIFFTAG_OPHTHALMICINFO,	/* TAG_OPHINFO */
#else
   0,
#endif
   COMPRESSION_NONE		/* TAG_NOCOMP */
};


/*

NAME:

   open_tiff_image() \- Open TIFF image file ready for reading

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/tiff.h>

   ip_tiff_handle *open_tiff_image( filename )
   char * filename

DESCRIPTION:

   Open TIFF image corresponding to 'filename'. Returns TIFF handle if
   successfully opened or alternatively NULL if any errors occur. One
   can check image type and size in the TIFF handle. This routine does
   not read in the actual image data, only the TIFF image header.

   Note that 'filename' must be a valid filename. A NULL pointer is not
   acceptable since can''t read TIFF files from stdin.

   The TIFF reading/writing code is TIFF version 6.0 baseline
   compliant.  It also supports the SAMPLEFORMAT tag for the reading
   and writing of SHORT, LONG, FLOAT and DOUBLE images.

   Example of reading in a TIFF image file:

\|   char *str;
\|   ip_tiff_handle *th;
\|
\|   th = open_tiff_image("fred");
\|   if (read_tiff_tag(th, TAG_SOFTWARE, &str)) {
\|      // Do whatever with software tag...
\|   }
\|   im = read_tiff_image(th);
\|   close_tiff_image(th);

LIMITATIONS:

\-   Reading COMPLEX images currently not supported.
\-   A fail on file open is reported as ERR_TIFF not ERR_FILE_OPEN

ERRORS:

   ERR_NO_STDIN - Can't read from stdin
   ERR_OUT_OF_MEM
   ERR_TIFF - libtiff generated error
   ERR_INVALID_TIFF_TYPE - Can't read that type of TIFF image

SEE ALSO:

   open_tiff_image()   read_tiff_image()   read_tiff_imagex()
   read_tiff_tag()     close_tiff_image()
   write_tiff_image()  write_tiff_imagex()
   ip_read_image()

*/

ip_tiff_handle *open_tiff_image(const char * fname)
{
   ip_tiff_handle *th;
   uint32 tmp;

   ip_log_routine("open_tiff_image");

   th = NULL;

   if (fname == NULL) {		/* Can't handle pipes */
      ip_log_error(ERR_NO_STDIN, "TIFF");
      goto exit_oti;
   }

   /* Allocate tiff handle. */

   if ((th = ip_mallocx(sizeof(ip_tiff_handle))) == NULL) {
      goto exit_oti;
   }
   th->f = NULL;
   th->fname = fname;

   /* Open disk file and read in first part in anticipation
      of tiff image */

   errno = 0;
#ifdef IP_OPHTHALTAG
   if ((th->f = XTIFFOpen(fname,"r")) == NULL) {
      ip_log_error(ERR_TIFF,fname);
      goto exit_oti;
   }
#else
   if ((th->f = TIFFOpen(fname,"r")) == NULL) {
      ip_log_error(ERR_TIFF,fname);
      goto exit_oti;
   }
#endif

   /* Interpret header info of tiff image. */

   TIFFGetField(th->f, TIFFTAG_PHOTOMETRIC, &th->pi );
   TIFFGetFieldDefaulted(th->f, TIFFTAG_BITSPERSAMPLE, &th->bps );
   if (TIFFGetField(th->f, TIFFTAG_SAMPLEFORMAT, &th->sf) != 1) {
      th->sf = 1;		/* libtiff.a returns wrong default value! */
   }
   TIFFGetFieldDefaulted(th->f, TIFFTAG_SAMPLESPERPIXEL, &th->spp );
   if (TIFFGetField(th->f, TIFFTAG_EXTRASAMPLES, &th->es_cnt, &th->es_values) != 1)
       th->es_cnt = 0;
   TIFFGetFieldDefaulted(th->f, TIFFTAG_COMPRESSION, &th->comp );
   TIFFGetField(th->f, TIFFTAG_IMAGEWIDTH, &tmp );
   th->size.x = tmp;
   TIFFGetField(th->f, TIFFTAG_IMAGELENGTH, &tmp );
   th->size.y = tmp;

   th->type = -1;		/* An illegal value */

   if (th->pi != PHOTOMETRIC_MINISWHITE
       && th->pi != PHOTOMETRIC_MINISBLACK
       && th->pi != PHOTOMETRIC_PALETTE
       && th->pi != PHOTOMETRIC_RGB
       && th->pi != PHOTOMETRIC_CIELAB) {
      ip_log_error(ERR_INVALID_TIFF_TYPE,fname);
      goto exit_oti;
   }

   /* Spoof a CIELAB load...  Not really good. */
   if (th->pi == PHOTOMETRIC_CIELAB) {
      fprintf(stderr,"WARNING: Tiff image %s is in CIELAB format.\n",th->fname);
      fprintf(stderr,"INFO: Loading as RGB and hoping for the best...!\n");
      th->pi = PHOTOMETRIC_RGB;
   }

   /*
    * photometric interpretation that we support reading:
    *
    * RGB -> spp must be 3 (RGB/RGB16) or 4 (RGBA)
    * MINISBLACK -> spp must be 1 (any scalar) or 2 (with alpha or COMPLEX/DCOMPLEX).
    * Otherwise -> spp must be 1.
    */

   if ((th->pi == PHOTOMETRIC_RGB && (th->spp < 3 || th->spp > 4))
       || (th->pi == PHOTOMETRIC_MINISBLACK && (th->spp < 1 || th->spp>2))
       || (th->pi != PHOTOMETRIC_RGB && th->pi != PHOTOMETRIC_MINISBLACK && th->spp != 1)) {
      ip_log_error(ERR_INVALID_TIFF_TYPE,fname);
      goto exit_oti;
   }

   /*
    * photometric interpration MINISBLACK
    * Only support spp == 2 on IEEE FP image types
    *      or grey-scale with unassociated alpha.
    */

   if ((th->pi == PHOTOMETRIC_MINISBLACK) && (th->spp != 1)) {
       if ((th->sf != SAMPLEFORMAT_IEEEFP) &&
	                    !(th->es_cnt == 1 && th->es_values[0] == EXTRASAMPLE_UNASSALPHA)) {
	   ip_log_error(ERR_INVALID_TIFF_TYPE,fname);
	   goto exit_oti;
       }
   }

   if (th->pi == PHOTOMETRIC_PALETTE) {
      if (th->bps != 8) {
	 ip_log_error(ERR_INVALID_TIFF_TYPE,fname);
	 goto exit_oti;
      }
      th->type = IM_PALETTE;
   }else if (th->pi == PHOTOMETRIC_RGB) {
      if (th->spp == 3) {
	 if ((th->bps != 8) && (th->bps != 16)) {
	    ip_log_error(ERR_INVALID_TIFF_TYPE,fname);
	    goto exit_oti;
	 }
	 th->type = th->bps == 8 ? IM_RGB : IM_RGB16;
      }else{
	 /* spp == 4 -> RGBA */
	 if (th->bps != 8) {
	    ip_log_error(ERR_INVALID_TIFF_TYPE,fname);
	    goto exit_oti;
	 }
	 th->type = IM_RGBA;
      }
   }else{
      switch (th->sf) {
       case SAMPLEFORMAT_UINT:
	 if (th->bps == 1) th->type = IM_BINARY;
	 if (th->bps == 4 || th->bps == 8) th->type = IM_UBYTE;
	 if (th->bps == 16) th->type = IM_USHORT;
	 if (th->bps == 32) th->type = IM_ULONG;
#ifdef IP_INCLUDE_LONG64
	 if (th->bps == 64) th->type = IM_ULONG64;
#endif
	 break;
       case SAMPLEFORMAT_INT:
	 if (th->bps == 8) th->type = IM_BYTE;
	 if (th->bps == 16) th->type = IM_SHORT;
	 if (th->bps == 32) th->type = IM_LONG;
#ifdef IP_INCLUDE_LONG64
	 if (th->bps == 64) th->type = IM_LONG64;
#endif
	 break;
       case SAMPLEFORMAT_IEEEFP:
	 if (th->bps == 32 && th->spp == 1) th->type = IM_FLOAT;
	 if (th->bps == 64 && th->spp == 1) th->type = IM_DOUBLE;
	 if (th->bps == 32 && th->spp == 2) th->type = IM_COMPLEX;
	 if (th->bps == 64 && th->spp == 2) th->type = IM_DCOMPLEX;
	 break;
       default:
	 ip_log_error(ERR_INVALID_TIFF_TYPE,fname);
	 goto exit_oti;
      }
   }

   if (th->type == -1) {
      ip_log_error(ERR_INVALID_TIFF_TYPE,fname);
      goto exit_oti;
   }

   ip_pop_routine();		/* Normal exit */
   return th;

 exit_oti:			/* Error exit */

  if (th) {
      if (th->f)
#ifdef IP_OPHTHALTAG
	 XTIFFClose(th->f);
#else
	 TIFFClose(th->f);
#endif
      ip_free(th);
   }
   ip_pop_routine();
   return NULL;
}




/*

NAME:

   write_tiff_image() \- Write an image as a TIFF file

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/tiff.h>

   int  write_tiff_image( filename, im )
   char *filename;
   image *im;

DESCRIPTION:

   Writes image 'im' as a TIFF file to 'filename'. Any error
   conditions returned.  Implemented as a macro in header file to
   call write_tiff_imagex().

   Note that 'filename' must be a valid pointer.  A NULL pointer is
   not acceptable since can''t write TIFF files to stdout.

   The TIFF reading/writing code is TIFF version 6.0 baseline
   compliant.  It also supports the SAMPLEFORMAT tag for the reading
   and writing of SHORT, LONG, FLOAT and DOUBLE images.

LIMITATIONS:

   Writing COMPLEX images currently not supported.

BUGS:

   A fail on file open is reported as ERR_TIFF not ERR_FILE_OPEN

ERRORS:

   ERR_NO_STDOUT - Can't write to stdout
   ERR_OUT_OF_MEM
   ERR_TIFF - libtiff generated error
   ERR_UNSUPPORTED_TIFF_WRITE - Can't write that type of image

SEE ALSO:

   open_tiff_image()   read_tiff_image()   read_tiff_imagex()
   read_tiff_tag()     close_tiff_image()
   write_tiff_imagex()
   ip_read_image()

*/



/*

NAME:

   write_tiff_imagex() \- Write image as a TIFF file

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/tiff.h>

   int  write_tiff_imagex( filename, im, flag, ... )
   char *filename;
   image *im;
   int flag;
   ...

DESCRIPTION:

   Writes image 'im' as a TIFF file to 'filename'. Any error
   conditions returned.  If 'flag' has FLG_TAGLIST ORed into it, then
   tags and their values follow on the argument list. They further
   describe the image.   Possible tags are:

\|       Tag       Value type     Description
\|   TAG_SOFTWARE    char *     Software name
\|   TAG_DATE         ---       Put in today's date
\|   TAG_DOCNAME     char *     Document name
\|   TAG_DESCRIPTION char *     Description of image
\|   TAG_HOST        char *     Host computer
\|   TAG_XRES        double     X-resolution (pixels per cm)
\|   TAG_YRES        double     Y-resolution (pixels per cm)
\|   TAG_MAXMIN       ---       Put in max/min sample value
\|   TAG_OPHINFO     int, char* Ophthalmic information
\|   TAG_NOCOMP       ---       Don't compress image.

   The taglist should be terminated with TAG_DONE.  Note that the
   TAG_OPHINFO takes two arguments, the first being the size of the
   buffer in bytes, and the second a pointer to the buffer.  The
   buffer should be a correctly formatted xtophinfo structure (see
   ophinfo_to_xtophinfo() and xtophinfo_to_ophinfo()).  TAG_OPHINFO
   is only implemented if IP_OPHTHALTAG is set during compilation.
   If it wasn't then using TAG_OPHINFO will generate the error
   ERR_NOT_IMPLEMENTED.

   An example invocation is:

\|   write_tiff_imagex( filename, im, FLG_TAGLIST,
\|                     TAG_SOFTWARE,    "programme_name",
\|                     TAG_DESCRIPTION, "A pretty picture!",
\|                     TAG_DATE,
\|                     TAG_DONE  );

   Note that 'filename' must be a valid pointer.  A NULL pointer is
   not acceptable since can''t write TIFF files to stdout.

   The TIFF reading/writing code is TIFF version 6.0 baseline
   compliant.  It also supports the SAMPLEFORMAT tag for the reading
   and writing of SHORT, LONG, FLOAT and DOUBLE images.

LIMITATIONS:

   Writing COMPLEX images currently not supported.

BUGS:

   A fail on file open is reported as ERR_TIFF, not ERR_FILE_OPEN

ERRORS:

   ERR_NO_STDOUT - Can't write to stdout
   ERR_OUT_OF_MEM
   ERR_BAD_TAG
   ERR_TIFF - libtiff generated error
   ERR_UNSUPPORTED_TIFF_WRITE - Can't write that type of image
   ERR_NOT_IMPLEMENTED - If this version of IP doesn't support
                         TAG_OPHINFO tag.

SEE ALSO:

   open_tiff_image()   read_tiff_image()   read_tiff_imagex()
   read_tiff_tag()     close_tiff_image()
   write_tiff_image()
   ip_read_image()

*/

int write_tiff_imagex(const char *fname, ip_image*im, int flag, ...)
{
   va_list ap;
   TIFF *f;
   ip_stats *st;
   int tag;
#ifdef IP_OPHTHALTAG
   int tag_size;
#endif
   char *tag_str;
   time_t today_t;
   struct tm *today;
#define DATESTR_LEN 22
   char datestr[DATESTR_LEN];
   double tag_dbl;
   int setresunit;
   int compression;
   unsigned int mask;
   uint16 red[256],green[256],blue[256];
   static uint16 bps[IM_TYPETOTAL] = {
      0,			/* Placeholder */
      1,			/* IM_BINARY */
      8, 8, 16, 16, 		/* BYTE to USHORT images*/
      32, 32,			/* (U)LONG images */
      32, 64,			/* float images */
      32, 64,			/* complex images */
      8, 8, 8, 16,		/* RGB type images */
      64, 64			/* (U)LONG64 images */
   };
   /* sample format */
   static uint16 sf[IM_TYPETOTAL] = {
      0,			/* Placeholder */
      SAMPLEFORMAT_UINT,	/* IM_BINARY */
      SAMPLEFORMAT_INT,		/* IM_BYTE */
      SAMPLEFORMAT_UINT,	/* IM_UBYTE */
      SAMPLEFORMAT_INT,		/* IM_SHORT */
      SAMPLEFORMAT_UINT,	/* IM_USHORT */
      SAMPLEFORMAT_INT,		/* IM_LONG */
      SAMPLEFORMAT_UINT,	/* IM_ULONG */
      SAMPLEFORMAT_IEEEFP,	/* IM_FLOAT */
      SAMPLEFORMAT_IEEEFP,	/* IM_DOUBLE */
      SAMPLEFORMAT_IEEEFP,	/* IM_COMPLEX */
      SAMPLEFORMAT_IEEEFP,	/* IM_DCOMPLEX */
      SAMPLEFORMAT_UINT,	/* IM_PALETTE */
      SAMPLEFORMAT_UINT,	/* IM_RGB */
      SAMPLEFORMAT_UINT,	/* IM_RGBA */
      SAMPLEFORMAT_UINT,	/* IM_RGB16 */
      SAMPLEFORMAT_INT,		/* IM_LONG64 */
      SAMPLEFORMAT_UINT		/* IM_ULONG64 */
   };
   /* photometric interpretation */
   static uint16 pi[IM_TYPETOTAL] = {
      0,			/* Placeholder */
      PHOTOMETRIC_MINISBLACK,	/* IM_BINARY */
      PHOTOMETRIC_MINISBLACK,	/* IM_BYTE */
      PHOTOMETRIC_MINISBLACK,	/* IM_UBYTE */
      PHOTOMETRIC_MINISBLACK,	/* IM_SHORT */
      PHOTOMETRIC_MINISBLACK,	/* IM_USHORT */
      PHOTOMETRIC_MINISBLACK,	/* IM_LONG */
      PHOTOMETRIC_MINISBLACK,	/* IM_ULONG */
      PHOTOMETRIC_MINISBLACK,	/* IM_FLOAT */
      PHOTOMETRIC_MINISBLACK,	/* IM_DOUBLE */
      PHOTOMETRIC_MINISBLACK,	/* IM_COMPLEX */
      PHOTOMETRIC_MINISBLACK,	/* IM_DCOMPLEX */
      PHOTOMETRIC_PALETTE,	/* IM_PALETTE */
      PHOTOMETRIC_RGB,		/* IM_RGB */
      PHOTOMETRIC_RGB,		/* IM_RGBA */
      PHOTOMETRIC_RGB,		/* IM_RGB16 */
      PHOTOMETRIC_MINISBLACK,	/* IM_LONG64 */
      PHOTOMETRIC_MINISBLACK	/* IM_ULONG64 */
   };
   /* samples per pixel */
   static uint16 spp[IM_TYPETOTAL] = {
      0,			/* placeholder */
      1, 			/* IM_BINARY */
      1, 1, 1, 1,		/* BYTE to USHORT */
      1, 1, 			/* (U)LONG */
      1, 1, 			/* float and double */
      2, 2, 			/* complex images */
      1, 3, 4, 3,		/* colour images */
      1, 1			/* (U)LONG64 */
   };

   va_start(ap,flag);
   ip_log_routine("write_tiff_image");
   f = NULL;

   if (fname == NULL) {		/* Can't handle non-seekable files. */
      ip_log_error(ERR_NO_STDOUT,"TIFF");
      goto exit_wti;
   }

   errno = 0;
#ifdef IP_OPHTHALTAG
   if ((f = XTIFFOpen(fname,"w")) == NULL) {
      ip_log_error(ERR_TIFF,fname);
      goto exit_wti;
   }
#else
   if ((f = TIFFOpen(fname,"w")) == NULL) {
      ip_log_error(ERR_TIFF,fname);
      goto exit_wti;
   }
#endif

   TIFFSetField(f, TIFFTAG_PHOTOMETRIC, pi[im->type]);
   TIFFSetField(f, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
   if (flag & FLG_BINARY || im->type == IM_BINARY) {
      TIFFSetField(f, TIFFTAG_BITSPERSAMPLE, (uint16) 1);
   }else{
      TIFFSetField(f, TIFFTAG_BITSPERSAMPLE, bps[im->type]);
      if ((im->type != IM_UBYTE) && (im->type != IM_USHORT) &&
	  (NOT image_type_clrim(im))) {
	 /* Some readers don't cope with this tag, so only use
	    when absolutely necessary. */
	 TIFFSetField(f, TIFFTAG_SAMPLEFORMAT, sf[im->type]);
      }
   }
   compression =  COMPRESSION_LZW;
   TIFFSetField(f, TIFFTAG_SAMPLESPERPIXEL, spp[im->type]);
   TIFFSetField(f, TIFFTAG_IMAGEWIDTH, (uint32)im->size.x);
   TIFFSetField(f, TIFFTAG_IMAGELENGTH, (uint32)im->size.y);

   if (im->type == IM_PALETTE) {
      for (int i=0; i<256; i++) {
	 red[i] = im->palette[i].r * 256 + im->palette[i].r;
	 green[i] = im->palette[i].g * 256 + im->palette[i].g;
	 blue[i] = im->palette[i].b * 256 + im->palette[i].b;
      }
      TIFFSetField(f, TIFFTAG_COLORMAP, red, green, blue);
   }

   TIFFSetField(f, TIFFTAG_ROWSPERSTRIP, TIFFDefaultStripSize(f, 0));

   /* Do tags in taglist. */

   setresunit = FALSE;
   if (flag & FLG_TAGLIST) {
      do {
	 tag = va_arg(ap, int);
	 switch (tag) {
	  case TAG_SKIP:
	    break;
	  case TAG_SKIPINT:
	    va_arg(ap, int);
	    break;
	  case TAG_SKIPFLOAT:
	    va_arg(ap, double);
	    break;
	  case TAG_SKIPPTR:
	    va_arg(ap, void *);
	    break;
	  case TAG_SOFTWARE:
	  case TAG_DOCNAME:
	  case TAG_DESCRIPTION:
	  case TAG_HOST:
	    tag_str = va_arg(ap, char *);
	    TIFFSetField(f, tiff_tag[tag], tag_str);
	    break;
	  case TAG_DATE:
	    today_t = time(NULL);
	    today = localtime(&today_t);
	    snprintf(datestr, DATESTR_LEN, "%4d:%02d:%02d %02d:%02d:%02d",
		     today->tm_year+1900,today->tm_mon+1,today->tm_mday,
		     today->tm_hour,today->tm_min,today->tm_sec);
	    TIFFSetField(f, tiff_tag[tag], datestr);
	    break;
	  case TAG_XRES:
	  case TAG_YRES:
	    tag_dbl = va_arg(ap, double);
	    TIFFSetField(f, tiff_tag[tag], tag_dbl);
	    setresunit = TRUE;
	    break;
	  case TAG_MAXMIN:
	    if (image_not_clrim(im)) {
	       st = ip_alloc_stats(NO_FLAG);
	       if (st) {
		   im_extrema(im, st, NULL);
		   if (im->type == IM_UBYTE || im->type == IM_BINARY) {
		       TIFFSetField(f, TIFFTAG_MAXSAMPLEVALUE, (uint16)st->max);
		       TIFFSetField(f, TIFFTAG_MINSAMPLEVALUE, (uint16)st->min);
		   }else{
		       TIFFSetField(f, TIFFTAG_SMAXSAMPLEVALUE, st->max);
		       TIFFSetField(f, TIFFTAG_SMINSAMPLEVALUE, st->min);
		   }
		   ip_free_stats(st);
	       }
	    }
	    break;
	  case TAG_OPHINFO:
#ifdef IP_OPHTHALTAG
	    tag_size = va_arg(ap, int);
	    tag_str = va_arg(ap, void *);
	    TIFFSetField(f, tiff_tag[tag], tag_size, tag_str);
#else
	    ip_log_error(ERR_NOT_IMPLEMENTED);
#endif
	    break;
	  case TAG_NOCOMP:
	    compression = COMPRESSION_NONE;
	    break;

	  case TAG_DONE:
	    break;
	  default:
	    ip_log_error(ERR_BAD_TAG);
	    break;
	 }
      } while (tag != TAG_DONE);
   }
   TIFFSetField(f, TIFFTAG_COMPRESSION, compression);
   if (setresunit)
      TIFFSetField(f, TIFFTAG_RESOLUTIONUNIT, RESUNIT_CENTIMETER);

   /* Now write actual image data... */

   if (flag & FLG_BINARY || im->type == IM_BINARY) {
      uint8_t *rowbuf;

      if ((rowbuf = ip_mallocx(TIFFScanlineSize(f))) == NULL) {
	 goto exit_wti;
      }
      for (int j=0; j<im->size.y; j++) {
	 uint8_t *iptr;

	 mask = 0x80;
	 for (int i=0; i<(im->size.x+7)/8; i++) {
	    rowbuf[i] = 0;
	 }
	 iptr = im_rowadr(im, j);
	 for (int i=0; i<im->size.x; i++) {
	    rowbuf[i/8] |= (*iptr++ != 0) * mask;
	    mask >>= 1;
	    if (mask == 0)
	       mask = 0x80;
	 }
	 if (TIFFWriteScanline(f, rowbuf, j, 0) != 1) {
	    ip_log_error(ERR_TIFF,fname);
	    ip_free(rowbuf);
	    goto exit_wti;
	 }
      }
      ip_free(rowbuf);

   }else{

      for (int j=0; j<im->size.y; j++) {
	 if (TIFFWriteScanline(f, im_rowadr(im,j), j, 0) != 1) {
	    ip_log_error(ERR_TIFF,fname);
	    goto exit_wti;
	 }
      }
   }

 exit_wti:

   if (f)
#ifdef IP_OPHTHALTAG
      XTIFFClose(f);
#else
      TIFFClose(f);
#endif

   ip_pop_routine();
   va_end(ap);
   return ip_error;
}




/*

NAME:

   read_tiff_tag() \- Read TIFF tags in a TIFF image file

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/tiff.h>

   int  read_tiff_tag( th, tag, value, ... )
   ip_tiff_handle *th;
   int tag;
   <something> *value;

DESCRIPTION:

   Read tags from a previously opened TIFF image.  Tags and their
   expected type for 'value' are:

\|       Tag       Return type     Description
\|   TAG_SOFTWARE    char **    Software name
\|   TAG_DATE        char **    Date field
\|   TAG_DOCNAME     char **    Document name
\|   TAG_DESCRIPTION char **    Description of image
\|   TAG_HOST        char **    Host computer
\|   TAG_XRES        double *   X-resolution (pixels per cm)
\|   TAG_YRES        double *   Y-resolution (pixels per cm)
\|   TAG_MINIMUM     double *   Minimum sample value
\|   TAG_MAXIMUM     double *   Maximum sample value
\|   TAG_OPHINFO     int, char* Ophthalmic information

   The tag value is returned in 'value'.  If the tag value is
   successfully read read_tiff_tag() returns TRUE otherwise it returns
   FALSE (and the 'value' is then not valid).  Note that the tag
   TAG_OPHINFO takes two value arguments, the first being the size
   in bytes and the second a pointer to the tag data.  TAG_OPHINFO
   is only implemented if the IP library is compiled with IP_OPHTHALTAG
   set.  If this is a version of IP that doesn't support TAG_OPHINFO
   then the error ERR_NOT_IMPLEMENTED will be generated if an attempt
   is made to use TAG_OPHINFO.

   Example of use:

\|   char *str;
\|   ip_tiff_handle *th;
\|
\|   th = open_tiff_image("fred");
\|   if (read_tiff_tag(th, TAG_SOFTWARE, &str)) {
\|      // Do whatever with software tag...
\|   }
\|   im = read_tiff_image(th);
\|   close_tiff_image(th);

SEE ALSO:

   open_tiff_image()   read_tiff_image()   read_tiff_imagex()
   close_tiff_image()
   write_tiff_image()  write_tiff_imagex()
   ip_read_image()

*/

int read_tiff_tag(ip_tiff_handle *th, int tag, ... )
{
   va_list ap;
   char ** str;
#ifdef IP_OPHTHALTAG
   int * intp;
#endif
   double * dbl;
   float res;
   uint16 resunit,ashort;
   int got;

   va_start(ap,tag);
   ip_log_routine("read_tiff_tag");

   switch (tag) {
    case TAG_SOFTWARE:
    case TAG_DOCNAME:
    case TAG_DESCRIPTION:
    case TAG_HOST:
    case TAG_DATE:
      str = va_arg(ap, char **);
      got = TIFFGetField(th->f, tiff_tag[tag], str);
      break;
    case TAG_XRES:
    case TAG_YRES:
      dbl = va_arg(ap, double *);
      TIFFGetFieldDefaulted(th->f, TIFFTAG_RESOLUTIONUNIT, &resunit);
      if ((got = TIFFGetField(th->f, tiff_tag[tag], &res)) == TRUE) {
	 if (resunit == RESUNIT_INCH) {
	    res *= 2.54;
	 }
	 *dbl = res;
      }
      break;
    case TAG_MAXIMUM:
      dbl = va_arg(ap, double *);
      if (TIFFGetField(th->f, TIFFTAG_MAXSAMPLEVALUE, &ashort)) {
	 got = TRUE;
	 *dbl = (double)ashort;
      }else{
	 got = TIFFGetField(th->f, TIFFTAG_SMAXSAMPLEVALUE, dbl);
      }
      break;
    case TAG_MINIMUM:
      dbl = va_arg(ap, double *);
      if (TIFFGetField(th->f, TIFFTAG_MINSAMPLEVALUE, &ashort)) {
	 got = TRUE;
	 *dbl = (double)ashort;
      }else{
	 got = TIFFGetField(th->f, TIFFTAG_SMINSAMPLEVALUE, dbl);
      }
      break;
    case TAG_OPHINFO:
#ifdef IP_OPHTHALTAG
      intp = va_arg(ap, int *);
      str = va_arg(ap, char **);
      got = TIFFGetField(th->f, tiff_tag[tag], intp, str);
#else
      got = 0;
      ip_log_error(ERR_NOT_IMPLEMENTED);
#endif
      break;
    default:
      got = FALSE;
      break;
   }

   va_end(ap);
   ip_pop_routine();
   return got;
}




/*

NAME:

   read_tiff_image() \- Read in image data from opened TIFF file

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/tiff.h>

   image * read_tiff_image( handle )
   ip_tiff_handle * handle;

DESCRIPTION:

   Reads in TIFF image corresponding to 'handle' from the
   disk. Returns the image data with image an opened image handle. One
   neads to call open_tiff_image() to get a valid TIFF handle before
   calling this routine.  Returns NULL if an error occurs.

   The TIFF reading/writing code is TIFF version 6.0 baseline
   compliant.  It also supports the SAMPLEFORMAT tag for the reading
   and writing of SHORT, LONG, FLOAT and DOUBLE images.

LIMITATIONS:

   Reading COMPLEX images is not supported.

ERRORS:

   ERR_BAD_TIFF_FILE - TIFF file is not fully TIFF compliant
   ERR_INVALID_TIFF_TYPE - Unsupported TIFF image file type
   ERR_TIFF - libtiff reported an error
   ERR_OUT_OF_MEM

SEE ALSO:

   open_tiff_image()   read_tiff_imagex()
   read_tiff_tag()     close_tiff_image()
   write_tiff_image()  write_tiff_imagex()
   ip_read_image()

*/

ip_image *read_tiff_image(ip_tiff_handle *th)
{
   ip_log_routine("read_tiff_image");
   return  rd_tiff_image(th, 0, th->size.y);
}




/*

NAME:

   read_tiff_imagex() \- Read in part/whole of a TIFF image

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/tiff.h>

   image * read_tiff_imagex( handle, first_row, nrows )
   ip_tiff_handle * handle;
   int first_row;
   int nrows;

DESCRIPTION:

   Reads in tiff image rows corresponding to 'handle' from the
   disk. Returns an image. One needs to call open_tiff_image() to get
   a valid 'handle' before calling this routine.  Returns NULL if an
   error occurs.

   The first row of an image is numbered zero. Hence to read only the
   first row of an image specify 'first_row'=0 and 'nrows'=1.

   The image returned has 'nrows' rows in it.

   The TIFF reading/writing code is TIFF version 6.0 baseline
   compliant.  It also supports the SAMPLEFORMAT tag for the reading
   and writing of SHORT, LONG, FLOAT and DOUBLE images.

LIMITATIONS:

   Reading COMPLEX images is not supported.

BUGS:

   If TIFF image data is compressed then only can start reading from
   row 0. If an attempt is made to start reading from a higher
   numbered row then an error is generated.

ERRORS:

   ERR_OOR_PARAMETER
   ERR_BAD_TIFF_FILE - TIFF file is not fully TIFF compliant
   ERR_INVALID_TIFF_TYPE - Unsupported TIFF image file type
   ERR_TIFF - libtiff reported an error
   ERR_OUT_OF_MEM

SEE ALSO:

   open_tiff_image()   read_tiff_image()
   read_tiff_tag()     close_tiff_image()
   write_tiff_image()  write_tiff_imagex()
   ip_read_image()

*/


ip_image * read_tiff_imagex(ip_tiff_handle *th, int first_row, int nrows)
{
   ip_log_routine("read_tiff_imagex");
   return  rd_tiff_image(th, first_row, nrows);
}



static ip_image * rd_tiff_image(ip_tiff_handle *th, int first_row, int nrows)
{
   ip_image*im;
   int bpsfunny;
   unsigned long sls,lineskip;
   int inverted;
   uint16 *red, *green, *blue;
   uint8_t *rowbuf = NULL;

   /* ip_log_routine() 'sposed to be done by calling routine.  */

   /* Check first_row and nrows are within range */

   if (NOT ip_parm_inrange("first_row", first_row, 0, th->size.y-1))
      return NULL;

   if (NOT ip_parm_inrange("number_rows", nrows, 1, th->size.y-first_row))
      return NULL;

   /* Allocate image */

   bpsfunny = FALSE;
   if (th->type == IM_BINARY || (th->type == IM_UBYTE && th->bps == 4)) {
      bpsfunny = TRUE;
   }

   if ((im = ip_alloc_image(th->type, (ip_coord2d){th->size.x, nrows})) == NULL)
      goto exit_rti;

   /* Get colour palette for palette images. */

   if (im->type == IM_PALETTE) {
      if (TIFFGetField(th->f, TIFFTAG_COLORMAP, &red, &green, &blue) == 1) {
	 for (int i=0; i<256; i++) {
	    im->palette[i].r = red[i] / 256;
	    im->palette[i].g = green[i] / 256;
	    im->palette[i].b = blue[i] / 256;
	 }
      }else{
	 ip_log_error(ERR_BAD_TIFF_FILE, th->fname);
	 goto exit_rti;
      }
   }

   sls = TIFFScanlineSize(th->f);
   lineskip = ip_type_size(im->type) * im->size.x;

   /* Invert images if min value is white. Then do another inversion
      if compression scheme is Fax CCITT compression schemes 3 or
      4. Really stupid, isn't it! */

   inverted = (th->pi == PHOTOMETRIC_MINISWHITE);
   if (th->comp == COMPRESSION_CCITTFAX3 || th->comp == COMPRESSION_CCITTFAX4)
      inverted = NOT inverted;
   if (inverted && im->type != IM_UBYTE && im->type != IM_BINARY) {
      ip_log_error(ERR_INVALID_TIFF_TYPE, th->fname);
      goto exit_rti;
   }

   /* Read in image data. */

   if (bpsfunny) {

      if ((rowbuf = ip_mallocx(sls)) == NULL) {
	 goto exit_rti;
      }

      for (int j=0; j<im->size.y; j++) {
	 uint8_t *iptr;
	 int mask;

	 if (TIFFReadScanline(th->f, rowbuf, j+first_row, 0) == -1) {
	    ip_log_error(ERR_TIFF,th->fname);
	    goto exit_rti;
	 }
	 iptr = im_rowadr(im, j);
	 if (th->bps == 1) {	/* BINARY image (bps == 1) */
	    mask = 0x80;
	    if (inverted) {
	       for (int i=0; i<im->size.x; i++) {
		  *iptr++ = (rowbuf[i/8] & mask) ? 0 : 255;
		  mask >>= 1;
		  if (mask == 0) mask = 0x80;
	       }
	    }else{
	       for (int i=0; i<im->size.x; i++) {
		  *iptr++ = (rowbuf[i/8] & mask) ? 255 : 0;
		  mask >>= 1;
		  if (mask == 0) mask = 0x80;
	       }
	    }

	 }else{			/* BYTE image (bps == 4) */
	    int tmp;

	    mask = 0xf0;
	    if (inverted) {
	       for (int i=0; i<im->size.x; i++) {
		  tmp = rowbuf[i/2] & mask;
		  *iptr++ = 255 -
			((mask == 0x0f) ? ((tmp<<4)+tmp) : (tmp+(tmp>>4)));
		  mask ^= 0xff;
	       }
	    }else{
	       for (int i=0; i<im->size.x; i++) {
		  tmp = rowbuf[i/2] & mask;
		  *iptr++ = (mask == 0x0f) ? ((tmp<<4)+tmp) : (tmp+(tmp>>4));
		  mask ^= 0xff;
	       }
	    }
	 }
      }

   }else{			/* BYTE image (bps==8) and all other images. */

      if (sls != lineskip && !(th->es_cnt == 1 && sls == 2*lineskip)) {
	 ip_log_error(ERR_ALGORITHM_FAULT,
		   "TIFFScanlineSize reports %ld when it should be %ld.",
		   sls,lineskip);
	 goto exit_rti;
      }

      if (th->es_cnt == 0) {
	  for (int j=0; j<im->size.y; j++) {
	      if (TIFFReadScanline(th->f, im_rowadr(im,j), j+first_row, 0) == -1) {
		  ip_log_error(ERR_TIFF,th->fname);
		  goto exit_rti;
	      }			/* Only implement inversion of BYTE images */
	      if (inverted && im->type == IM_UBYTE) {
		  uint8_t *iptr;

		  iptr = im_rowadr(im,j);
		  for (int i=0; i<im->size.x; i++) {
		      *iptr = 255 - *iptr;
		      iptr++;
		  }
	      }
	  }
      }else{
	  void *rowbuf;
	  int psize = ip_type_size(im->type);
	  /* Unassociated alpha presumably mixed up with pixels */
	  if (psize != 1 && psize != 2 && psize !=4) {
	      ip_log_error(ERR_INVALID_TIFF_TYPE, th->fname);
	      goto exit_rti;
	  }
	  if ((rowbuf = ip_mallocx(sls)) == NULL) {
	      goto exit_rti;
	  }

	  for (int j=0; j<im->size.y; j++) {
	      if (TIFFReadScanline(th->f, rowbuf, j+first_row, 0) == -1) {
		  ip_log_error(ERR_TIFF,th->fname);
		  goto exit_rti;
	      }
	      switch (psize) {
		  case 1: {
		      uint8_t *tptr = rowbuf;
		      uint8_t *imptr = IM_ROWADR(im, j, v);
		      for (int i=0; i<im->size.x; i++) {
			  *imptr++ = *tptr;
			  tptr += 2;
		      }
		  } break;
		  case 2: {
		      uint16_t *tptr = (uint16_t *)rowbuf;
		      uint16_t *imptr = IM_ROWADR(im, j, v);
		      for (int i=0; i<im->size.x; i++) {
			  *imptr++ = *tptr;
			  tptr += 2;
		      }
		  } break;
		  case 4: {
		      uint32_t *tptr = (uint32_t *)rowbuf;
		      uint32_t *imptr = IM_ROWADR(im, j, v);
		      for (int i=0; i<im->size.x; i++) {
			  *imptr++ = *tptr;
			  tptr += 2;
		      }
		  } break;
	      default:
		  break;
	      }
	  }
      }
   }

 exit_rti:

   if (ip_error != OK) {
      ip_free_image(im);
      im = NULL;
   }

   if (rowbuf) ip_free(rowbuf);

   ip_pop_routine();
   return im;
}



/*

NAME:

   close_tiff_image() \- Close previously opened TIFF image file

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/tiff.h>

   void close_tiff_image( handle )
   ip_tiff_handle *handle;

DESCRIPTION:

   Closes tiff file associated with 'handle'. 'Handle' is not valid
   anymore after calling close_tiff_image(). Safe to call with NULL
   'handle'.

SEE ALSO:

   open_tiff_image()

*/

void close_tiff_image(ip_tiff_handle *th)
{
   ip_log_routine("close_tiff_image");

   if (th) {
      if (th->f) {
#ifdef IP_OPHTHALTAG
	 XTIFFClose(th->f);
#else
         TIFFClose(th->f);
#endif
      }
      ip_free(th);
   }
   ip_pop_routine();
}
