/*
   Module: ip/convert.c
   Author: M. J. Cree

   Makes a duplicate of image but of a new type, i.e. cast images.

   Copyright (C) 1995-1998, 2000-2002, 2007-2008, 2014-2015, 2018 Michael J. Cree

ENTRY POINTS:

   im_convert()
   im_form_complex()
   im_form_rgb()

*/

#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <limits.h>
#include <float.h>

#include <ip/ip.h>

#include "internal.h"
#include "function_ops.h"
#include "utils.h"

#ifdef HAVE_SIMD
#include "simd/convert_simd.h"
#endif

/*

NAME:

   im_convert() \- Convert an image to an image of new datatype

PROTOTYPE:

   #include <ip/ip.h>

   ip_image * im_convert( src, type, flag )
   ip_image * src;
   ip_image_type type;
   int flag;

DESCRIPTION:

   Convert the image 'src' into a new type of image. The image 'src' is
   copied into a new image that has the new specified datatype 'type'.  A
   pointer to the new image is returned (or NULL if an error occurs).

   Flags are:

\&-   For use when downcasting to a less precise type:

\&      FLG_CLIP      Clip (saturate) all out of range values to
                      the limits of the new type.
\&      FLG_ROUND     Use conventional rounding when converting from a
		      real image type to an integer image type.

   For use with converting to/from COMPLEX images:

\&      FLG_REAL      Use real part of complex image
\&      FLG_IMAG      Use imaginary part of complex image
\&      FLG_MAG       Use magnitude part of complex image
\&      FLG_ANGLE     Use phase part of complex image

\&-   For converting RGB and PALETTE images to scalar image types.

\&      FLG_RED
\&      FLG_GREEN
\&      FLG_BLUE
\&      FLG_MAG       (Use Euclidean magnitude of RGB vector)
\&      FLG_MAGYUV    (Use Y part of YUV, i.e., intensity)
\&      FLG_GREY      (Synonym for FLG_MAGYUV)

\&-   For converting BINARY images to (U)BYTE, SHORT, LONG, FLOAT or DOUBLE
   images. (Note that unset pixels are always mapped to value 0).

\&      FLG_B1        Set pixels are mapped to the value 1.
\&      FLG_B255      Set pixels are mapped to the value 255.

\&- When converting integer images to BINARY, all non-zero pixels in the
   integer image are treated as set.  To threshold at any other value use
   im_threshold().

   By default when downcasting the larger integer type to a more restricted
   integer type standard C conversion rules apply (i.e. the value is
   truncated to the length of the destination type). If FLG_CLIP is
   specified the pixel value is clipped (saturated) to the range of the
   destination type before conversion.  When upcasting integer types to a
   larger integer type signed extension (if the source image is of a signed
   type) or unsigned extension (if the source image is of unsigned type) is
   used.

   When converting real images (FLOAT or DOUBLE) to integer images by
   default the fractional part is truncated.  If conventional rounding to
   nearest is preferred pass the flag FLG_ROUND.  The conversion (with or
   without rounding) is guaranteed to be at first to an intermediate signed
   integer of at least 32-bits for all resultant integer images types
   excepting that if the conversion if to ULONG and the intermediate integer
   is of exactly 32-bits, then a conversion to an unsigned 32-bit integer is
   used.  The resulted integer value (of at least 32-bits) is then truncated
   to the final integer type.  Conversion of any other real values (finite
   and outside the valid limits of a 32-bit integer, and non-finite values)
   is architecturally dependent.  Some arches convert to an intermediate
   integer of size larger than 32-bits (or even effectively of an infinite
   number of bits) and then truncate to the resultant type, and some clip
   (saturate) all finite values outside the intermediate integer size to the
   minimum or maximum value of the intermediate range.  Some may have
   differing intermediate integer sizes for generic code and SIMD code
   (e.g. A 64-bit arch might have 64-bit intermediate for generic code, but
   a 32-bit intermediate integer for SIMD code).

   When converting complex images (COMPLEX or DCOMPLEX) to a scalar image
   first the complex value is converted to a scalar taking the real part by
   default, or by request with the flags FLG_REAL, FLG_IMAGINARY, FLG_MAG
   and FLG_ANGLE, one of the real, imaginary, magnitude or phase parts can
   be extracted.  The conversion to the scalar value is at the same floating
   point precision as the complex image.  The scalar is then converted to
   the final image type (if necessary).  If the conversion is to an integer
   type then conversion of a real value to integer (as desribed in the
   paragraph above) is applied to the resultant scalar, excepting that the
   option to use conventional rounding (FLG_ROUND) is not implemented.

   When converting from a scalar image type to complex by default the real
   part of the resultant complex image is set to the source image and
   imaginary part is set to zero (that is, FLG_REAL is the default).  Use
   FLG_IMAG to put the source image into the complex imaginary part and zero
   the real part.

   When converting a UBYTE image to a PALETTE image, the PALETTE image is
   assigned a grey-scale palette.  When converting a PALETTE image to a
   UBYTE image, if no flag is specified then the colour palette is dropped
   and the "pen" values of each pixel are returned as the UBYTE image.  If
   one of the colour flags are specified then an indirection through the
   colour palette lookup table is done and the red (or green or blue or
   intensity) part of the colour image is returned as the UBYTE image.

   When converting RGB images with FLG_MAG it is permissible to convert to a
   FLOAT or DOUBLE image, thereby preserving the accuracy of the Euclidean
   magnitude.  When FLG_MAG is used to convert colour images to UBYTE, then
   the result is divided by three to ensure it fits in the 0-255 range.

   Specifying conversion flags with an inappropriate image type (such as
   FLG_IMAG with an RGB image) triggers an error. An exception is that
   FLG_CLIP is silently ignored without error if specified when up-casting
   or when it can have no effect.

LIMITATIONS:

\-   Not all possible conversions are implemented.
\-   BINARY images can only be converted to and from other scalar image types
     (i.e., (U)BYTE, (U)SHORT, (U)LONG, FLOAT and DOUBLE images).
\-   PALETTE images can only be converted to/from UBYTE images and
     to RGB images.
\-   When converting a colour image to a scalar type, RGB images can only be
     converted to UBYTE, and RGB16 images only to LONG (this needs fixing).

ERRORS:

   ERR_OUT_OF_MEM
   ERR_SAME_TYPE
   ERR_BAD_TYPE
   ERR_BAD_FLAG
   ERR_NOT_IMPLEMENTED

SEE ALSO:

   ip_alloc_image()        im_copy()    im_cut()
   im_ubyte_to_binary()    im_binary_to_ubyte()
   im_ubyte_to_palette()   im_palette_to_ubyte()
   im_form_rgb()           im_threshold()

*/



/*
 * Conversions to and from binary images.  We only suppport conversions with
 * scalar integer and real images.
 */


#define generate_convert_to_binary(st)				\
    static void convert_BINARY_ ## st(ip_image *dest, ip_image *src, int unused) \
    {								\
	for (int j=0; j<dest->size.y; j++) {			\
	    uint8_t *dd = IM_ROWADR(dest, j, v);		\
	    st ## _type *ss = IM_ROWADR(src, j, v);		\
	    for (int i=0; i<dest->size.x; i++) {		\
		st ## _type val = *ss++;			\
		*dd++ = val ? 255 : 0;				\
	    }							\
	}							\
    }

generate_convert_to_binary(UBYTE)
generate_convert_to_binary(USHORT)
generate_convert_to_binary(ULONG)
generate_convert_to_binary(FLOAT)
generate_convert_to_binary(DOUBLE)



#define generate_convert_from_binary(dt)				\
    static void convert_ ## dt ## _BINARY(ip_image *dest, ip_image *src, int trueval) \
    {									\
	for (int j=0; j<dest->size.y; j++) {				\
	    dt ## _type *dd = IM_ROWADR(dest, j, v);			\
	    uint8_t *ss = IM_ROWADR(src, j, v);				\
	    for (int i=0; i<dest->size.x; i++) {			\
		*dd++ = *ss++ ? trueval : 0;				\
	    }								\
	}								\
    }

generate_convert_from_binary(UBYTE)
generate_convert_from_binary(USHORT)
generate_convert_from_binary(ULONG)
generate_convert_from_binary(FLOAT)
generate_convert_from_binary(DOUBLE)


/*
 * General scalar (including complex) conversions; no clipping or checking
 * range when downcasting.
 *
 * For the case where the bit pattern remains exactly the same
 * (e.g. converting unsigned integer to signed integer of the same size and
 * vice versa) we just copy the image data as their is no conversion at the
 * pixel level to be performed.
 *
 * For real to integer conversion we provide two possibilities below.
 * Conversion via int64_t which on 32-bit platforms ensures better modulo
 * 32-bit behaviour of large integers and conversion via round().
 *
 * convert_basic_copy()
 *    -- integer to same size integer, just copy image data.
 *
 * convert_basic_DESTTYPE_SRCTYPE()
 *    -- integer to integer conversions
 *    -- integer to real conversions
 *    -- real to real conversions
 *
 * convert_via_int64_DESTTYPE_SRCTYPE()
 *    -- real to integer conversions
 *
 * convert_round_DESTTYPE_SRCTYPE()
 *    -- real to integer conversions with rounding
 *
 * convert_complex_DESTTYPE_SRCTYPE()
 *    -- complex to integer/real conversions
 *    -- integer/real to complex conversions
 *
 * convert_complex_complex_DESTTYPE_SRCTYPE()
 *    -- complex to complex conversions
 */


static void convert_basic_copy(ip_image *dest, ip_image *src, int unused)
{
    for (int j=0; j<dest->size.y; j++)
	memcpy(IM_ROWADR(dest, j, v), IM_ROWADR(src, j, v), dest->row_size);
}


#define generate_convert_basic(dt, st)					\
    static void convert_basic_ ## dt ## _ ## st(ip_image *dest, ip_image *src, int unused) \
    {									\
	for (int j=0; j<dest->size.y; j++) {				\
	    dt ## _type *dd = IM_ROWADR(dest, j, v);			\
	    st ## _type *ss = IM_ROWADR(src, j, v);			\
	    for (int i=0; i<dest->size.x; i++) {			\
		*dd++ = (dt ## _type)*ss++;				\
	    }								\
	}								\
    }

#define generate_convert_via_int64(dt, st)				\
    static void convert_via_int64_ ## dt ## _ ## st(ip_image *dest, ip_image *src, int unused) \
    {									\
	for (int j=0; j<dest->size.y; j++) {				\
	    dt ## _type *dd = IM_ROWADR(dest, j, v);			\
	    st ## _type *ss = IM_ROWADR(src, j, v);			\
	    for (int i=0; i<dest->size.x; i++) {			\
		*dd++ = (dt ## _type)((int64_t)*ss++);			\
	    }								\
	}								\
    }

#define generate_convert_round(dt, st, rnd)				\
    static void convert_round_ ## dt ## _ ## st(ip_image *dest, ip_image *src, int unused) \
    {									\
	for (int j=0; j<dest->size.y; j++) {				\
	    dt ## _type *dd = IM_ROWADR(dest, j, v);			\
	    st ## _type *ss = IM_ROWADR(src, j, v);			\
	    for (int i=0; i<dest->size.x; i++) {			\
		*dd++ = (dt ## _type)((int64_t)rnd(*ss++));		\
	    }								\
	}								\
    }

#define generate_convert_from_complex(dt, st, sbaset)			\
    static void convert_complex_ ## dt ## _ ## st(ip_image *dest, ip_image *src, int flag) \
    {									\
	for (int j=0; j<dest->size.y; j++) {				\
	    dt ## _type *dd = IM_ROWADR(dest, j, v);			\
	    st ## _type *ss = IM_ROWADR(src, j, v);			\
	    for (int i=0; i<dest->size.x; i++) {			\
		sbaset val;						\
		switch (flag) {						\
		default:						\
		case FLG_REAL:						\
		    val = (sbaset)ss->r;				\
		    break;						\
		case FLG_IMAG:						\
		    val = (sbaset)ss->i;				\
		    break;						\
		case FLG_MAG:						\
		    val = (sbaset)hypot(ss->r, ss->i);			\
		    break;						\
		case FLG_ANGLE:						\
		    val = (sbaset)atan2(ss->i, ss->r);			\
		    break;						\
		}							\
		*dd++ = (dt ## _type)val;				\
		ss++;							\
	    }								\
	}								\
    }

#define generate_convert_to_complex(dt, st, dbaset)			\
    static void convert_complex_ ## dt ## _ ## st(ip_image *dest, ip_image *src, int flag) \
    {									\
	for (int j=0; j<dest->size.y; j++) {				\
	    dt ## _type *dd = IM_ROWADR(dest, j, v);			\
	    st ## _type *ss = IM_ROWADR(src, j, v);			\
	    for (int i=0; i<dest->size.x; i++) {			\
		dbaset val = (dbaset)*ss++;				\
		if (flag == FLG_IMAG) {					\
		    dd->i = val;					\
		    dd->r = 0.0;					\
		}else{							\
		    dd->r = val;					\
		    dd->i = 0.0;					\
		}							\
		dd++;							\
	    }								\
	}								\
    }

#define generate_convert_complex_complex(dt, st, dbaset)		\
    static void convert_complex_ ## dt ## _ ## st(ip_image *dest, ip_image *src, int flag) \
    {									\
	for (int j=0; j<dest->size.y; j++) {				\
	    dt ## _type *dd = IM_ROWADR(dest, j, v);			\
	    st ## _type *ss = IM_ROWADR(src, j, v);			\
	    for (int i=0; i<dest->size.x; i++) {			\
		dd->r = (dbaset)ss->r;					\
		dd->i = (dbaset)ss->i;					\
		dd++; ss++;						\
	    }								\
	}								\
    }


/*
 * In the integer to integer conversions below we only need the following
 * routines.  (Note that we assume signed integers are represented by the
 * two's complement.)
 *
 * When downcasting from larger integer to smaller integer only one routine
 * is needed as the larger integer is truncated to the size of the smaller
 * whether it is signed or not or whether the resultant integer is signed or
 * not.
 *
 * When upcasting from a smaller integer to a larger integer two routines
 * are needed depending on whether the smaller integer is signed or not.  If
 * signed it must be sign extended, and if unsigned it must be zero
 * extended.  This remains true independently of whether the resultant
 * larger integer is signed or not.
 */

generate_convert_via_int64(BYTE, FLOAT)
generate_convert_via_int64(BYTE, DOUBLE)
generate_convert_from_complex(BYTE, COMPLEX, float)
generate_convert_from_complex(BYTE, DCOMPLEX, double)

generate_convert_round(BYTE, FLOAT, roundf)
generate_convert_round(BYTE, DOUBLE, round)

generate_convert_basic(UBYTE, USHORT)
generate_convert_basic(UBYTE, ULONG)
generate_convert_via_int64(UBYTE, FLOAT)
generate_convert_via_int64(UBYTE, DOUBLE)
generate_convert_from_complex(UBYTE, COMPLEX, float)
generate_convert_from_complex(UBYTE, DCOMPLEX, double)

generate_convert_round(UBYTE, FLOAT, roundf)
generate_convert_round(UBYTE, DOUBLE, round)

generate_convert_basic(SHORT, BYTE)
generate_convert_via_int64(SHORT, FLOAT)
generate_convert_via_int64(SHORT, DOUBLE)
generate_convert_from_complex(SHORT, COMPLEX, float)
generate_convert_from_complex(SHORT, DCOMPLEX, double)

generate_convert_round(SHORT, FLOAT, roundf)
generate_convert_round(SHORT, DOUBLE, round)

generate_convert_basic(USHORT, UBYTE)
generate_convert_basic(USHORT, ULONG)
generate_convert_via_int64(USHORT, FLOAT)
generate_convert_via_int64(USHORT, DOUBLE)
generate_convert_from_complex(USHORT, COMPLEX, float)
generate_convert_from_complex(USHORT, DCOMPLEX, double)

generate_convert_round(USHORT, FLOAT, roundf)
generate_convert_round(USHORT, DOUBLE, round)

generate_convert_basic(LONG, BYTE)
generate_convert_basic(LONG, SHORT)
generate_convert_via_int64(LONG, FLOAT)
generate_convert_via_int64(LONG, DOUBLE)
generate_convert_from_complex(LONG, COMPLEX, float)
generate_convert_from_complex(LONG, DCOMPLEX, double)

generate_convert_round(LONG, FLOAT, roundf)
generate_convert_round(LONG, DOUBLE, round)

generate_convert_basic(ULONG, UBYTE)
generate_convert_basic(ULONG, USHORT)
generate_convert_via_int64(ULONG, FLOAT)
generate_convert_via_int64(ULONG, DOUBLE)
generate_convert_from_complex(ULONG, COMPLEX, float)
generate_convert_from_complex(ULONG, DCOMPLEX, double)

generate_convert_round(ULONG, FLOAT, roundf)
generate_convert_round(ULONG, DOUBLE, round)

generate_convert_basic(FLOAT, BYTE)
generate_convert_basic(FLOAT, UBYTE)
generate_convert_basic(FLOAT, SHORT)
generate_convert_basic(FLOAT, USHORT)
generate_convert_basic(FLOAT, LONG)
generate_convert_basic(FLOAT, ULONG)
generate_convert_basic(FLOAT, DOUBLE)
generate_convert_from_complex(FLOAT, COMPLEX, float)
generate_convert_from_complex(FLOAT, DCOMPLEX, double)

generate_convert_basic(DOUBLE, BYTE)
generate_convert_basic(DOUBLE, UBYTE)
generate_convert_basic(DOUBLE, SHORT)
generate_convert_basic(DOUBLE, USHORT)
generate_convert_basic(DOUBLE, LONG)
generate_convert_basic(DOUBLE, ULONG)
generate_convert_basic(DOUBLE, FLOAT)
generate_convert_from_complex(DOUBLE, COMPLEX, float)
generate_convert_from_complex(DOUBLE, DCOMPLEX, double)

generate_convert_to_complex(COMPLEX, BYTE, float)
generate_convert_to_complex(COMPLEX, UBYTE, float)
generate_convert_to_complex(COMPLEX, SHORT, float)
generate_convert_to_complex(COMPLEX, USHORT, float)
generate_convert_to_complex(COMPLEX, LONG, float)
generate_convert_to_complex(COMPLEX, ULONG, float)
generate_convert_to_complex(COMPLEX, FLOAT, float)
generate_convert_to_complex(COMPLEX, DOUBLE, float)
generate_convert_complex_complex(COMPLEX, DCOMPLEX, float)

generate_convert_to_complex(DCOMPLEX, BYTE, double)
generate_convert_to_complex(DCOMPLEX, UBYTE, double)
generate_convert_to_complex(DCOMPLEX, SHORT, double)
generate_convert_to_complex(DCOMPLEX, USHORT, double)
generate_convert_to_complex(DCOMPLEX, LONG, double)
generate_convert_to_complex(DCOMPLEX, ULONG, double)
generate_convert_to_complex(DCOMPLEX, FLOAT, double)
generate_convert_to_complex(DCOMPLEX, DOUBLE, double)
generate_convert_complex_complex(DCOMPLEX, COMPLEX, double)




/*
 * Colour conversion routines.
 */

static void cnvrt_paltob(ip_image *dest, ip_image *src, int flag)
{
    for (int j=0; j<src->size.y; j++) {
	uint8_t *sptr = IM_ROWADR(src, j, v);
	uint8_t *dptr = IM_ROWADR(dest,j, v);

	switch (flag) {
	case FLG_RED:
	    for (int i=0; i<src->size.x; i++) {
		*dptr++ = src->palette[*sptr++].r;
	    }
	    break;
	case FLG_GREEN:
	    for (int i=0; i<src->size.x; i++) {
		*dptr++ = src->palette[*sptr++].g;
	    }
	    break;
	case FLG_BLUE:
	    for (int i=0; i<src->size.x; i++) {
		*dptr++ = src->palette[*sptr++].b;
	    }
	    break;
	case FLG_MAG:
	    for (int i=0; i<src->size.x; i++) {
		ip_rgb *tcol = src->palette + *sptr++;
		*dptr++ = (uint8_t)sqrtf(((int)tcol->r*tcol->r
					  + (int)tcol->g*tcol->g
					  + (int)tcol->b*tcol->b)/3.0F);
	    }
	    break;
	case FLG_MAGYUV:
	    for (int i=0; i<src->size.x; i++) {
		ip_rgb *tcol = src->palette + *sptr++;
		*dptr++ = (uint8_t)((19595*tcol->r + 38470*tcol->g + 7471*tcol->b)>>16);
	    }
	    break;
	}
    }
}


static void cnvrt_rgbtob(ip_image *dest, ip_image *src, int flag)
{
    for (int j=0; j<src->size.y; j++) {
	ip_rgb *sptr = IM_ROWADR(src, j, v);
	uint8_t *dptr = IM_ROWADR(dest, j, v);

	switch (flag) {
	case FLG_RED:
	    for (int i=0; i<src->size.x; i++) {
		*dptr++ = sptr->r;
		sptr++;
	    }
	    break;
	case FLG_GREEN:
	    for (int i=0; i<src->size.x; i++) {
		*dptr++ = sptr->g;
		sptr++;
	    }
	    break;
	case FLG_BLUE:
	    for (int i=0; i<src->size.x; i++) {
		*dptr++ = sptr->b;
		sptr++;
	    }
	    break;
	case FLG_MAG:
	    for (int i=0; i<src->size.x; i++) {
		*dptr++ = (uint8_t)sqrtf(((int)sptr->r*sptr->r
					  + (int)sptr->g*sptr->g
					  + (int)sptr->b*sptr->b)/3.0F);
		sptr++;
	    }
	    break;
	case FLG_MAGYUV:
	    for (int i=0; i<src->size.x; i++) {
		*dptr++ = (uint8_t)((19595*sptr->r + 38470*sptr->g + 7471*sptr->b)>>16);
		sptr++;
	    }
	    break;
	}
    }
}


static void cnvrt_rgbtof(ip_image *dest, ip_image *src, int flag)
{
    for (int j=0; j<src->size.y; j++) {
	ip_rgb *sptr = IM_ROWADR(src, j, v);
	float *dptr = IM_ROWADR(dest, j, v);

	for (int i=0; i<src->size.x; i++) {
	    *dptr++ = sqrtf((int)sptr->r*sptr->r
			    + (int)sptr->g*sptr->g
			    + (int)sptr->b*sptr->b);
	    sptr++;
	}
    }
}


static void cnvrt_rgbtod(ip_image *dest, ip_image *src, int flag)
{
    for (int j=0; j<src->size.y; j++) {
	ip_rgb *sptr = IM_ROWADR(src, j, v);
	double *dptr = IM_ROWADR(dest, j, v);

	for (int i=0; i<src->size.x; i++) {
	    *dptr++ = sqrt((int)sptr->r*sptr->r
			   + (int)sptr->g*sptr->g
			   + (int)sptr->b*sptr->b);
	    sptr++;
	}
    }
}


static void cnvrt_rgb16tos(ip_image *dest, ip_image *src, int flag)
{
    for (int j=0; j<src->size.y; j++) {
	ip_lrgb *sptr = IM_ROWADR(src, j, v);
	uint16_t *dptr = IM_ROWADR(dest, j, v);

	switch (flag) {
	case FLG_RED:
	    for (int i=0; i<src->size.x; i++) {
		*dptr++ = sptr->r;
		sptr++;
	    }
	    break;
	case FLG_GREEN:
	    for (int i=0; i<src->size.x; i++) {
		*dptr++ = sptr->g;
		sptr++;
	    }
	    break;
	case FLG_BLUE:
	    for (int i=0; i<src->size.x; i++) {
		*dptr++ = sptr->b;
		sptr++;
	    }
	    break;
	case FLG_MAG:
	    for (int i=0; i<src->size.x; i++) {
		*dptr++ = (uint16_t)sqrt(((int)sptr->r*sptr->r
					  + (int)sptr->g*sptr->g + (int)sptr->b*sptr->b)/3.0F);
		sptr++;
	    }
	    break;
	case FLG_MAGYUV:
	    for (int i=0; i<src->size.x; i++) {
		*dptr++ = (uint16_t)((19595*sptr->r + 38470*sptr->g + 7471*sptr->b)>>16);
		sptr++;
	    }
	    break;
	}
    }
}




static void cnvrt_paltorgb(ip_image *dest, ip_image *src, int unused)
{
    ip_rgb *pal = src->palette;

    for (int j=0; j<src->size.y; j++) {
	uint8_t *sptr = IM_ROWADR(src, j, v);
	ip_rgb *dptr = IM_ROWADR(dest, j, v);;

	for (int i=0; i<src->size.x; i++) {
	    *dptr++ = pal[*sptr++];
	}
   }
}




static void cnvrt_rgb16torgb(ip_image *dest, ip_image *src, int flag)
{
    for (int j=0; j<src->size.y; j++) {
	ip_lrgb *sptr = IM_ROWADR(src, j, v);
	ip_rgb *dptr = IM_ROWADR(dest, j, v);

	for (int i=0; i<src->size.x; i++) {
	    dptr->r = sptr->r/256;
	    dptr->g = sptr->g/256;
	    dptr->b = sptr->b/256;
	    sptr++; dptr++;
	}
    }
}



static void cnvrt_rgbtorgb16(ip_image *dest, ip_image *src, int flag)
{
    for (int j=0; j<src->size.y; j++) {
	ip_rgb *sptr = im_rowadr(src,j);
	ip_lrgb *dptr = im_rowadr(dest,j);

	for (int i=0; i<src->size.x; i++) {
	    dptr->r = sptr->r<<8 | sptr->r;
	    dptr->g = sptr->g<<8 | sptr->g;
	    dptr->b = sptr->b<<8 | sptr->b;
	    sptr++; dptr++;
	}
    }
}


/* If big endian need an offset to fix the shifts */

#ifdef HAVE_BIG_ENDIAN
#  define SOFFSET 8
#  define RGBA_ALPHA_255 0x000000ffUL
#else
#  define SOFFSET 0
#  define RGBA_ALPHA_255 0xff000000UL
#endif


#ifdef HAVE_64BIT
/* 64-bit routines */

#  define SET_RGBA_ALPHA_255 ((RGBA_ALPHA_255<<32) | (RGBA_ALPHA_255))

static inline unsigned long rgb2rgba_repack_a(unsigned long x)
{
    return (x & 0xffffffUL)<<SOFFSET
	| (x & 0xffffff000000UL)<<(8+SOFFSET);
}

static inline unsigned long rgb2rgba_repack_b(unsigned long x, unsigned long y)
{
    return (x & 0xffff000000000000UL)>>(48-SOFFSET)
	| (y & 0xffUL)<<(16+SOFFSET)
	| (y & 0xffffff00UL) << (24+SOFFSET);
}

static inline unsigned long rgb2rgba_repack_c(unsigned long y, unsigned long z)
{
    return (y & 0xffffff00000000UL)>>(32-SOFFSET)
	| (y & 0xff00000000000000UL)>>(24-SOFFSET)
	| (z & 0xffffUL) << (40+SOFFSET);
}

static inline unsigned long rgb2rgba_repack_d(unsigned long z)
{
    return (z & 0xffffff0000UL)>>(16-SOFFSET)
	| (z & 0xffffff0000000000UL)>>(8-SOFFSET);
}

#else
/* 32-bit routines */

#  define SET_RGBA_ALPHA_255 RGBA_ALPHA_255

static inline unsigned long rgb2rgba_repack_a(unsigned long x)
{
    return (x & 0xffffffUL)<<SOFFSET;
}

static inline unsigned long rgb2rgba_repack_b(unsigned long x, unsigned long y)
{
    return (x & 0xff000000UL)>>(24-SOFFSET) | (y & 0xffffUL)<<(8+SOFFSET);
}

static inline unsigned long rgb2rgba_repack_c(unsigned long y, unsigned long z)
{
    return (y & 0xffff0000UL)>>(16-SOFFSET) | (z & 0xffUL)<<(16+SOFFSET);
}

static inline unsigned long rgb2rgba_repack_d(unsigned long z)
{
    return (z & 0xffffff00UL)>>(8-SOFFSET);
}

#endif


#ifdef HAVE_BIG_ENDIAN
#  define REPACK_A(x)    rgb2rgba_repack_d(x)
#  define REPACK_B(x, y) rgb2rgba_repack_c(y, x)
#  define REPACK_C(x, y) rgb2rgba_repack_b(y, x)
#  define REPACK_D(z)    rgb2rgba_repack_a(z)
#else
#  define REPACK_A(x)    rgb2rgba_repack_a(x)
#  define REPACK_B(x, y) rgb2rgba_repack_b(x, y)
#  define REPACK_C(x, y) rgb2rgba_repack_c(x, y)
#  define REPACK_D(z)    rgb2rgba_repack_d(z)
#endif



static void cnvrt_rgbtorgba(ip_image *dest, ip_image *src, int unused)
{
#if 0
    for (int j=0; j<src->size.y; j++) {
	ip_rgb *sptr;
	ip_rgba *dptr;

	sptr = IM_ROWADR(src, j, v);
	dptr = IM_ROWADR(dest, j, v);
	for (int i=0; i<src->size.x; i++) {
	    dptr->r = sptr->r;
	    dptr->g = sptr->g;
	    dptr->b = sptr->b;
	    dptr->a = 255;
	    sptr++; dptr++;
	}
    }
#else
    int N = src->size.x / sizeof(unsigned long);
    int rem = src->size.x % sizeof(unsigned long);

    for (int j=0; j<src->size.y; j++) {
	unsigned long *sptr;
	unsigned long *dptr;

	sptr = IM_ROWADR(src, j, v);
	dptr = IM_ROWADR(dest, j, v);
	for (int i=0; i<N; i++) {
	    unsigned long s1, s2, s3;

	    /* Encourage compiler to load values early */
	    s1 = sptr[0];
	    s2 = sptr[1];
	    s3 = sptr[2];

	    /* Do the repacking of rgb into rgba */
	    dptr[0] = REPACK_A(s1)     | SET_RGBA_ALPHA_255;
	    dptr[1] = REPACK_B(s1, s2) | SET_RGBA_ALPHA_255;
	    dptr[2] = REPACK_C(s2, s3) | SET_RGBA_ALPHA_255;
	    dptr[3] = REPACK_D(s3)     | SET_RGBA_ALPHA_255;

	    /* and to the next ... */
	    sptr += 3;
	    dptr += 4;
	}
	ip_rgb *sptr_r  = (ip_rgb *)sptr;
	ip_rgba *dptr_ra = (ip_rgba *)dptr;

	for (int i=0; i<rem; i++) {
	    dptr_ra->r = sptr_r->r;
	    dptr_ra->g = sptr_r->g;
	    dptr_ra->b = sptr_r->b;
	    dptr_ra->a = 255;
	    dptr_ra++; sptr_r++;
	}
    }
#endif
}

#undef REPACK_A
#undef REPACK_B
#undef REPACK_C
#undef REPACK_D




#ifdef HAVE_64BIT

#  ifdef HAVE_BIG_ENDIAN
/* 64-bit, big endian */

static inline unsigned long rgba2rgb_repack_a(unsigned long u, unsigned long v)
{
    return (u & 0xffffff0000000000UL)
	| (u & 0xffffff00UL) << 8
	| (v & 0xffff000000000000UL) >> 48;
}

static inline unsigned long rgba2rgb_repack_b(unsigned long v, unsigned long x)
{
    return (v & 0xff0000000000UL) << 16
	| (v & 0xffffff00UL) << 24
	| (x & 0xffffff0000000000UL) >> 32
	| (x & 0xff000000UL) >> 24;
}

static inline unsigned long rgba2rgb_repack_c(unsigned long x, unsigned long y)
{
    return (x & 0xffff00UL) << 40
	| (y & 0xffffff0000000000UL) >> 16
	| (y & 0xffffff00UL) >> 8;
}

#  else
/* 64-bit, little endian */

static inline unsigned long rgba2rgb_repack_a(unsigned long u, unsigned long v)
{
    return (u & 0xffffffUL)
	| (u & 0xffffff00000000UL) >> 8
	| (v & 0xffffUL) << 48;
}

static inline unsigned long rgba2rgb_repack_b(unsigned long v, unsigned long x)
{
    return (v & 0xff0000UL) >> 16
	| (v & 0xffffff00000000UL) >> 24
	| (x & 0xffffffUL) << 32
	| (x & 0xff00000000UL) << 24;
}

static inline unsigned long rgba2rgb_repack_c(unsigned long x, unsigned long y)
{
    return (x & 0xffff0000000000UL) >> 40
	| (y & 0xffffffUL) << 16
	| (y & 0xffffff00000000UL) << 8;
}
# endif

#else

#  ifdef HAVE_BIG_ENDIAN
/* 32-bit, big endian versions */

static inline unsigned long rgba2rgb_repack_a(unsigned long u, unsigned long v)
{
    return (u & 0xffffff00UL) | (v & 0xff000000UL) >> 24;
}

static inline unsigned long rgba2rgb_repack_b(unsigned long v, unsigned long x)
{
    return (v & 0xffff00UL) << 8 | (x & 0xffff0000UL) >> 16;
}

static inline unsigned long rgba2rgb_repack_c(unsigned long x, unsigned long y)
{
    return (x & 0xff00UL) << 16 | (y & 0xffffff00UL) >> 8;
}

#  else
/* 32-bit, little endian versions */

static inline unsigned long rgba2rgb_repack_a(unsigned long u, unsigned long v)
{
    return (u & 0xffffffUL) | (v & 0xffUL) << 24;
}

static inline unsigned long rgba2rgb_repack_b(unsigned long v, unsigned long x)
{
    return (v & 0xffff00UL) >> 8 | (x & 0xffffUL) << 16;
}

static inline unsigned long rgba2rgb_repack_c(unsigned long x, unsigned long y)
{
    return (x & 0xff0000UL) >> 16 | (y & 0xffffffUL) << 8;
}

#  endif

#endif

#define REPACK_A(x, y) rgba2rgb_repack_a(x, y)
#define REPACK_B(x, y) rgba2rgb_repack_b(x, y)
#define REPACK_C(x, y) rgba2rgb_repack_c(x, y)


static void cnvrt_rgbatorgb(ip_image *dest, ip_image *src, int unused)
{
#if 0
    for (int j=0; j<src->size.y; j++) {
	ip_rgba *sptr;
	ip_rgb *dptr;

	sptr = IM_ROWADR(src, j, v);
	dptr = IM_ROWADR(dest, j, v);
	for (int i=0; i<src->size.x; i++) {
	    dptr->r = sptr->r;
	    dptr->g = sptr->g;
	    dptr->b = sptr->b;
	    sptr++; dptr++;
	}
    }
#else
    int N = src->size.x / sizeof(unsigned long);
    int rem = src->size.x % sizeof(unsigned long);

    for (int j=0; j<src->size.y; j++) {
	unsigned long *sptr;
	unsigned long *dptr;

	sptr = IM_ROWADR(src, j, v);
	dptr = IM_ROWADR(dest, j, v);
	for (int i=0; i<N; i++) {
	    unsigned long s1, s2, s3, s4;

	    /* Encourage compiler to load values early */
	    s1 = sptr[0];
	    s2 = sptr[1];
	    s3 = sptr[2];
	    s4 = sptr[3];

	    /* Do the repacking of rgb into rgba */
	    dptr[0] = REPACK_A(s1, s2);
	    dptr[1] = REPACK_B(s2, s3);
	    dptr[2] = REPACK_C(s3, s4);

	    /* and to the next ... */
	    sptr += 4;
	    dptr += 3;
	}
	ip_rgba *sptr_ra  = (ip_rgba *)sptr;
	ip_rgb *dptr_r = (ip_rgb *)dptr;

	for (int i=0; i<rem; i++) {
	    dptr_r->r = sptr_ra->r;
	    dptr_r->g = sptr_ra->g;
	    dptr_r->b = sptr_ra->b;
	    dptr_r++; sptr_ra++;
	}
    }
#endif
}



static improc_function_IIf convert_to_BINARY[IM_TYPEMAX] = {
    [IM_BYTE] = convert_BINARY_UBYTE,
    [IM_UBYTE] = convert_BINARY_UBYTE,
    [IM_SHORT] = convert_BINARY_USHORT,
    [IM_USHORT] = convert_BINARY_USHORT,
    [IM_LONG] = convert_BINARY_ULONG,
    [IM_ULONG] = convert_BINARY_ULONG,
    [IM_FLOAT] = convert_BINARY_FLOAT,
    [IM_DOUBLE] = convert_BINARY_DOUBLE,
};

static improc_function_IIf convert_to_BYTE[IM_TYPEMAX] = {
    [IM_BINARY] = convert_UBYTE_BINARY,
    [IM_UBYTE] = convert_basic_copy,
    [IM_SHORT] = convert_basic_UBYTE_USHORT,
    [IM_USHORT] = convert_basic_UBYTE_USHORT,
    [IM_LONG] = convert_basic_UBYTE_ULONG,
    [IM_ULONG] = convert_basic_UBYTE_ULONG,
    [IM_FLOAT] = convert_via_int64_BYTE_FLOAT,
    [IM_DOUBLE] = convert_via_int64_BYTE_DOUBLE,
    [IM_COMPLEX] = convert_complex_BYTE_COMPLEX,
    [IM_DCOMPLEX] = convert_complex_BYTE_DCOMPLEX,
};

static improc_function_IIf convert_to_UBYTE[IM_TYPEMAX] = {
    [IM_BINARY] = convert_UBYTE_BINARY,
    [IM_BYTE] = convert_basic_copy,
    [IM_SHORT] = convert_basic_UBYTE_USHORT,
    [IM_USHORT] = convert_basic_UBYTE_USHORT,
    [IM_LONG] = convert_basic_UBYTE_ULONG,
    [IM_ULONG] = convert_basic_UBYTE_ULONG,
    [IM_FLOAT] = convert_via_int64_UBYTE_FLOAT,
    [IM_DOUBLE] = convert_via_int64_UBYTE_DOUBLE,
    [IM_COMPLEX] = convert_complex_UBYTE_COMPLEX,
    [IM_DCOMPLEX] = convert_complex_UBYTE_DCOMPLEX,
    [IM_PALETTE] = cnvrt_paltob,
    [IM_RGB] = cnvrt_rgbtob
};

static improc_function_IIf convert_to_SHORT[IM_TYPEMAX] = {
    [IM_BINARY] = convert_USHORT_BINARY,
    [IM_BYTE] = convert_basic_SHORT_BYTE,
    [IM_UBYTE] = convert_basic_USHORT_UBYTE,
    [IM_USHORT] = convert_basic_copy,
    [IM_LONG] = convert_basic_USHORT_ULONG,
    [IM_ULONG] = convert_basic_USHORT_ULONG,
    [IM_FLOAT] = convert_via_int64_SHORT_FLOAT,
    [IM_DOUBLE] = convert_via_int64_SHORT_DOUBLE,
    [IM_COMPLEX] = convert_complex_SHORT_COMPLEX,
    [IM_DCOMPLEX] = convert_complex_SHORT_DCOMPLEX,
};

static improc_function_IIf convert_to_USHORT[IM_TYPEMAX] = {
    [IM_BINARY] = convert_USHORT_BINARY,
    [IM_BYTE] = convert_basic_SHORT_BYTE,
    [IM_UBYTE] = convert_basic_USHORT_UBYTE,
    [IM_SHORT] = convert_basic_copy,
    [IM_LONG] = convert_basic_USHORT_ULONG,
    [IM_ULONG] = convert_basic_USHORT_ULONG,
    [IM_FLOAT] = convert_via_int64_USHORT_FLOAT,
    [IM_DOUBLE] = convert_via_int64_USHORT_DOUBLE,
    [IM_COMPLEX] = convert_complex_USHORT_COMPLEX,
    [IM_DCOMPLEX] = convert_complex_USHORT_DCOMPLEX,
    [IM_RGB16] = cnvrt_rgb16tos
};

static improc_function_IIf convert_to_LONG[IM_TYPEMAX] = {
    [IM_BINARY] = convert_ULONG_BINARY,
    [IM_BYTE] = convert_basic_LONG_BYTE,
    [IM_UBYTE] = convert_basic_ULONG_UBYTE,
    [IM_SHORT] = convert_basic_LONG_SHORT,
    [IM_USHORT] = convert_basic_ULONG_USHORT,
    [IM_ULONG] = convert_basic_copy,
    [IM_FLOAT] = convert_via_int64_LONG_FLOAT,
    [IM_DOUBLE] = convert_via_int64_LONG_DOUBLE,
    [IM_COMPLEX] = convert_complex_LONG_COMPLEX,
    [IM_DCOMPLEX] = convert_complex_LONG_DCOMPLEX,
};

static improc_function_IIf convert_to_ULONG[IM_TYPEMAX] = {
    [IM_BINARY] = convert_ULONG_BINARY,
    [IM_BYTE] = convert_basic_LONG_BYTE,
    [IM_UBYTE] = convert_basic_ULONG_UBYTE,
    [IM_SHORT] = convert_basic_LONG_SHORT,
    [IM_USHORT] = convert_basic_ULONG_USHORT,
    [IM_LONG] = convert_basic_copy,
    [IM_FLOAT] = convert_via_int64_ULONG_FLOAT,
    [IM_DOUBLE] = convert_via_int64_ULONG_DOUBLE,
    [IM_COMPLEX] = convert_complex_ULONG_COMPLEX,
    [IM_DCOMPLEX] = convert_complex_ULONG_DCOMPLEX,
};

static improc_function_IIf convert_to_FLOAT[IM_TYPEMAX] = {
    [IM_BINARY] = convert_FLOAT_BINARY,
    [IM_BYTE] = convert_basic_FLOAT_BYTE,
    [IM_UBYTE] = convert_basic_FLOAT_UBYTE,
    [IM_SHORT] = convert_basic_FLOAT_SHORT,
    [IM_USHORT] = convert_basic_FLOAT_USHORT,
    [IM_LONG] = convert_basic_FLOAT_LONG,
    [IM_ULONG] = convert_basic_FLOAT_ULONG,
    [IM_DOUBLE] = convert_basic_FLOAT_DOUBLE,
    [IM_COMPLEX] = convert_complex_FLOAT_COMPLEX,
    [IM_DCOMPLEX] = convert_complex_FLOAT_DCOMPLEX,
    [IM_RGB] = cnvrt_rgbtof
};

static improc_function_IIf convert_to_DOUBLE[IM_TYPEMAX] = {
    [IM_BINARY] = convert_DOUBLE_BINARY,
    [IM_BYTE] = convert_basic_DOUBLE_BYTE,
    [IM_UBYTE] = convert_basic_DOUBLE_UBYTE,
    [IM_SHORT] = convert_basic_DOUBLE_SHORT,
    [IM_USHORT] = convert_basic_DOUBLE_USHORT,
    [IM_LONG] = convert_basic_DOUBLE_LONG,
    [IM_ULONG] = convert_basic_DOUBLE_ULONG,
    [IM_FLOAT] = convert_basic_DOUBLE_FLOAT,
    [IM_COMPLEX] = convert_complex_DOUBLE_COMPLEX,
    [IM_DCOMPLEX] = convert_complex_DOUBLE_DCOMPLEX,
    [IM_RGB] = cnvrt_rgbtod
};

static improc_function_IIf convert_to_COMPLEX[IM_TYPEMAX] = {
    [IM_BYTE] = convert_complex_COMPLEX_BYTE,
    [IM_UBYTE] = convert_complex_COMPLEX_UBYTE,
    [IM_SHORT] = convert_complex_COMPLEX_SHORT,
    [IM_USHORT] = convert_complex_COMPLEX_USHORT,
    [IM_LONG] = convert_complex_COMPLEX_LONG,
    [IM_ULONG] = convert_complex_COMPLEX_ULONG,
    [IM_FLOAT] = convert_complex_COMPLEX_FLOAT,
    [IM_DOUBLE] = convert_complex_COMPLEX_DOUBLE,
    [IM_DCOMPLEX] = convert_complex_COMPLEX_DCOMPLEX,
};

static improc_function_IIf convert_to_DCOMPLEX[IM_TYPEMAX] = {
    [IM_BYTE] = convert_complex_DCOMPLEX_BYTE,
    [IM_UBYTE] = convert_complex_DCOMPLEX_UBYTE,
    [IM_SHORT] = convert_complex_DCOMPLEX_SHORT,
    [IM_USHORT] = convert_complex_DCOMPLEX_USHORT,
    [IM_LONG] = convert_complex_DCOMPLEX_LONG,
    [IM_ULONG] = convert_complex_DCOMPLEX_ULONG,
    [IM_FLOAT] = convert_complex_DCOMPLEX_FLOAT,
    [IM_DOUBLE] = convert_complex_DCOMPLEX_DOUBLE,
    [IM_COMPLEX] = convert_complex_DCOMPLEX_COMPLEX,
};

static improc_function_IIf convert_to_RGB[IM_TYPEMAX] = {
    [IM_PALETTE] = cnvrt_paltorgb,
    [IM_RGB16] = cnvrt_rgb16torgb,
    [IM_RGBA] = cnvrt_rgbatorgb
};

static improc_function_IIf convert_to_RGB16[IM_TYPEMAX] = {
    [IM_RGB] = cnvrt_rgbtorgb16
};

static improc_function_IIf convert_to_RGBA[IM_TYPEMAX] = {
    [IM_RGB] = cnvrt_rgbtorgba
};


/*
 * The 2D array of standard conversion functions indexed first by the
 * destination type and then by the source type.
 */

static improc_function_IIf *convert_basic[IM_TYPEMAX] = {
    [IM_BINARY] = convert_to_BINARY,
    [IM_BYTE] = convert_to_BYTE,
    [IM_UBYTE] = convert_to_UBYTE,
    [IM_SHORT] = convert_to_SHORT,
    [IM_USHORT] = convert_to_USHORT,
    [IM_LONG] = convert_to_LONG,
    [IM_ULONG] = convert_to_ULONG,
    [IM_FLOAT] = convert_to_FLOAT,
    [IM_DOUBLE] = convert_to_DOUBLE,
    [IM_COMPLEX] = convert_to_COMPLEX,
    [IM_DCOMPLEX] = convert_to_DCOMPLEX,
    [IM_RGB] = convert_to_RGB,
    [IM_RGB16] = convert_to_RGB16,
    [IM_RGBA] = convert_to_RGBA,
};


/* The conversion with rounding table follows */

static improc_function_IIf round_to_BYTE[IM_TYPEMAX] = {
    [IM_FLOAT] = convert_round_BYTE_FLOAT,
    [IM_DOUBLE] = convert_round_BYTE_DOUBLE,
};

static improc_function_IIf round_to_UBYTE[IM_TYPEMAX] = {
    [IM_FLOAT] = convert_round_UBYTE_FLOAT,
    [IM_DOUBLE] = convert_round_UBYTE_DOUBLE,
};

static improc_function_IIf round_to_SHORT[IM_TYPEMAX] = {
    [IM_FLOAT] = convert_round_SHORT_FLOAT,
    [IM_DOUBLE] = convert_round_SHORT_DOUBLE,
};

static improc_function_IIf round_to_USHORT[IM_TYPEMAX] = {
    [IM_FLOAT] = convert_round_USHORT_FLOAT,
    [IM_DOUBLE] = convert_round_USHORT_DOUBLE,
};

static improc_function_IIf round_to_LONG[IM_TYPEMAX] = {
    [IM_FLOAT] = convert_round_LONG_FLOAT,
    [IM_DOUBLE] = convert_round_LONG_DOUBLE,
};

static improc_function_IIf round_to_ULONG[IM_TYPEMAX] = {
    [IM_FLOAT] = convert_round_ULONG_FLOAT,
    [IM_DOUBLE] = convert_round_ULONG_DOUBLE,
};


/*
 * The 2D array that contains the conversion from real image types with
 * rounding to integer image types.
 */

static improc_function_IIf *convert_round[IM_TYPEMAX] = {
    [IM_BYTE] = round_to_BYTE,
    [IM_UBYTE] = round_to_UBYTE,
    [IM_SHORT] = round_to_SHORT,
    [IM_USHORT] = round_to_USHORT,
    [IM_LONG] = round_to_LONG,
    [IM_ULONG] = round_to_ULONG,
};


/*
 * Conversions that involve clipping the result to the range of the
 * destination type (i.e. saturation) when downcasting.
 *
 * convert_clipped_DESTTYPE_SRCTYPE()
 *     -- clipped (signed) integer to lower precision integer conversions
 *     -- clipped real (DOUBLE) to lower precision real (FLOAT) conversions
 *
 * convert_clipped_upper_DESTTYPE_SRCTYPE()
 *     -- clipped integer conversion where only upper bound should be checked.
 *
 * convert_clipped_lower_DESTTYPE_SRCTYPE()
 *     -- clipped integer conversion where only lower bound should be checked.
 *
 * convert_via_int64_clipped_DESTTYPE_SRCTYPE()
 *     -- clipped real to integer conversions
 *
 * convert_round_clipped_DESTTYPE_SRCTYPE()
 *     -- clipped and rounded real to integer conversions
 *
 * convert_complex_clipped_DESTTYPE_SRCTYPE()
 *     -- clipped complex to real/integer conversions
 *
 * There are no clipping routines implemented for when COMPLEX is the
 * destination type.  Also note that by "lower precision" in the above
 * descriptions we mean that the destination type cannot faithfully
 * represent all values of the source type, not necessarily that it is fewer
 * bits.
 *
 * Where the conversion is upcasted (i.e. when no loss of precision can
 * occur) the routines without clipping implemented above are used.
 */

/*
 * Note that we implement the down-conversion with comparisons in nested
 * ternary operators.  This is important for the FLOAT to LONG conversion
 * because INT32_MAX is not exactly representable in float, indeed the
 * closest float value is INT32_MAX+1, so if the following code is used:
 *
 * if (val >= dt##_max) val = dt##_max
 *
 * INT32_MAX+1 gets assigned to val (being the nearest value to INT32_MAX
 * representable in single-precision floating point) which when converted to
 * int32_t becomes INT32_MIN due to 32-bit modulo-32 truncation!  By using
 * the ternary operator we assign directly the value of INT32_MAX to the
 * destination variable without a cast when the clipped value is required.
 */
#define generate_convert_clipped(dt, st)				\
    static void convert_clipped_ ## dt ## _ ## st(ip_image *dest, ip_image *src, int unused) \
    {									\
	for (int j=0; j<dest->size.y; j++) {				\
	    dt ## _type *dd = IM_ROWADR(dest, j, v);			\
	    st ## _type *ss = IM_ROWADR(src, j, v);			\
	    for (int i=0; i<dest->size.x; i++) {			\
		st ## _type val = *ss++;				\
		*dd++ = (val >= dt##_max) ? dt##_max : ((val <= dt##_min) ? dt##_min : (dt##_type)val);	\
	    }								\
	}								\
    }

#define generate_convert_clipped_upper(dt, st)				\
    static void convert_clipped_ ## dt ## _ ## st(ip_image *dest, ip_image *src, int unused) \
    {									\
	for (int j=0; j<dest->size.y; j++) {				\
	    dt ## _type *dd = IM_ROWADR(dest, j, v);			\
	    st ## _type *ss = IM_ROWADR(src, j, v);			\
	    for (int i=0; i<dest->size.x; i++) {			\
		st ## _type val = *ss++;				\
		if (val > dt ## _max)					\
		    val = dt ## _max;					\
		*dd++ = (dt ## _type)val;				\
	    }								\
	}								\
    }

#define generate_convert_clipped_lower(dt, st)				\
    static void convert_clipped_ ## dt ## _ ## st(ip_image *dest, ip_image *src, int unused) \
    {									\
	for (int j=0; j<dest->size.y; j++) {				\
	    dt ## _type *dd = IM_ROWADR(dest, j, v);			\
	    st ## _type *ss = IM_ROWADR(src, j, v);			\
	    for (int i=0; i<dest->size.x; i++) {			\
		st ## _type val = *ss++;				\
		if (val < dt ## _min)					\
		    val = dt ## _min;					\
		*dd++ = (dt ## _type)val;				\
	    }								\
	}								\
    }


#define generate_convert_clipped_round(dt, st, rnd)				\
    static void convert_clipped_round_ ## dt ## _ ## st(ip_image *dest, ip_image *src, int unused) \
    {									\
	for (int j=0; j<dest->size.y; j++) {				\
	    dt ## _type *dd = IM_ROWADR(dest, j, v);			\
	    st ## _type *ss = IM_ROWADR(src, j, v);			\
	    for (int i=0; i<dest->size.x; i++) {			\
		st ## _type val = rnd(*ss++);				\
		*dd++ = (val >= dt##_max) ? dt##_max : ((val <= dt##_min) ? dt##_min : (dt##_type)val);	\
	    }								\
	}								\
    }

#define generate_convert_clipped_from_complex(dt, st, sbaset)		\
    static void convert_complex_clipped_ ## dt ## _ ## st(ip_image *dest, ip_image *src, int flag) \
    {									\
	for (int j=0; j<dest->size.y; j++) {				\
	    dt ## _type *dd = IM_ROWADR(dest, j, v);			\
	    st ## _type *ss = IM_ROWADR(src, j, v);			\
	    for (int i=0; i<dest->size.x; i++) {			\
		sbaset val;						\
		switch (flag) {						\
		default:						\
		case FLG_REAL:						\
		    val = (sbaset)ss->r;				\
		    break;						\
		case FLG_IMAG:						\
		    val = (sbaset)ss->i;				\
		    break;						\
		case FLG_MAG:						\
		    val = (sbaset)hypot(ss->r, ss->i);			\
		    break;						\
		case FLG_ANGLE:						\
		    val = (sbaset)atan2(ss->i, ss->r);			\
		    break;						\
		}							\
		*dd++ = (val >= dt##_max) ? dt##_max : ((val <= dt##_min) ? dt##_min : (dt##_type)val);	\
		ss++;							\
	    }								\
	}								\
    }


generate_convert_clipped_upper(BYTE, UBYTE)
generate_convert_clipped(BYTE, SHORT)
generate_convert_clipped_upper(BYTE, USHORT)
generate_convert_clipped(BYTE, LONG)
generate_convert_clipped_upper(BYTE, ULONG)
generate_convert_clipped(BYTE, FLOAT)
generate_convert_clipped(BYTE, DOUBLE)
generate_convert_clipped_from_complex(BYTE, COMPLEX, float)
generate_convert_clipped_from_complex(BYTE, DCOMPLEX, double)

generate_convert_clipped_round(BYTE, FLOAT, roundf)
generate_convert_clipped_round(BYTE, DOUBLE, round)

generate_convert_clipped_lower(UBYTE, BYTE)
generate_convert_clipped(UBYTE, SHORT)
generate_convert_clipped_upper(UBYTE, USHORT)
generate_convert_clipped(UBYTE, LONG)
generate_convert_clipped_upper(UBYTE, ULONG)
generate_convert_clipped(UBYTE, FLOAT)
generate_convert_clipped(UBYTE, DOUBLE)
generate_convert_clipped_from_complex(UBYTE, COMPLEX, float)
generate_convert_clipped_from_complex(UBYTE, DCOMPLEX, double)

generate_convert_clipped_round(UBYTE, FLOAT, roundf)
generate_convert_clipped_round(UBYTE, DOUBLE, round)

generate_convert_clipped_upper(SHORT, USHORT)
generate_convert_clipped(SHORT, LONG)
generate_convert_clipped_upper(SHORT, ULONG)
generate_convert_clipped(SHORT, FLOAT)
generate_convert_clipped(SHORT, DOUBLE)
generate_convert_clipped_from_complex(SHORT, COMPLEX, float)
generate_convert_clipped_from_complex(SHORT, DCOMPLEX, double)

generate_convert_clipped_round(SHORT, FLOAT, roundf)
generate_convert_clipped_round(SHORT, DOUBLE, round)

generate_convert_clipped_lower(USHORT, BYTE)
generate_convert_clipped_lower(USHORT, SHORT)
generate_convert_clipped(USHORT, LONG)
generate_convert_clipped_upper(USHORT, ULONG)
generate_convert_clipped(USHORT, FLOAT)
generate_convert_clipped(USHORT, DOUBLE)
generate_convert_clipped_from_complex(USHORT, COMPLEX, float)
generate_convert_clipped_from_complex(USHORT, DCOMPLEX, double)

generate_convert_clipped_round(USHORT, FLOAT, roundf)
generate_convert_clipped_round(USHORT, DOUBLE, round)

generate_convert_clipped_upper(LONG, ULONG)
generate_convert_clipped(LONG, FLOAT)
generate_convert_clipped(LONG, DOUBLE)
generate_convert_clipped_from_complex(LONG, COMPLEX, float)
generate_convert_clipped_from_complex(LONG, DCOMPLEX, double)

generate_convert_clipped_round(LONG, FLOAT, roundf)
generate_convert_clipped_round(LONG, DOUBLE, round)

generate_convert_clipped_lower(ULONG, BYTE)
generate_convert_clipped_lower(ULONG, SHORT)
generate_convert_clipped_lower(ULONG, LONG)
generate_convert_clipped(ULONG, FLOAT)
generate_convert_clipped(ULONG, DOUBLE)
generate_convert_clipped_from_complex(ULONG, COMPLEX, float)
generate_convert_clipped_from_complex(ULONG, DCOMPLEX, double)

generate_convert_clipped_round(ULONG, FLOAT, roundf)
generate_convert_clipped_round(ULONG, DOUBLE, round)

generate_convert_clipped(FLOAT, DOUBLE)
generate_convert_clipped_from_complex(FLOAT, DCOMPLEX, double)

static improc_function_IIf convert_to_BYTE_clipped[IM_TYPEMAX] = {
    [IM_BINARY] = convert_UBYTE_BINARY,
    [IM_UBYTE] = convert_clipped_BYTE_UBYTE,
    [IM_SHORT] = convert_clipped_BYTE_SHORT,
    [IM_USHORT] = convert_clipped_BYTE_USHORT,
    [IM_LONG] = convert_clipped_BYTE_LONG,
    [IM_ULONG] = convert_clipped_BYTE_ULONG,
    [IM_FLOAT] = convert_clipped_BYTE_FLOAT,
    [IM_DOUBLE] = convert_clipped_BYTE_DOUBLE,
    [IM_COMPLEX] = convert_complex_clipped_BYTE_COMPLEX,
    [IM_DCOMPLEX] = convert_complex_clipped_BYTE_DCOMPLEX,
};

static improc_function_IIf convert_to_UBYTE_clipped[IM_TYPEMAX] = {
    [IM_BINARY] = convert_UBYTE_BINARY,
    [IM_BYTE] = convert_clipped_UBYTE_BYTE,
    [IM_SHORT] = convert_clipped_UBYTE_SHORT,
    [IM_USHORT] = convert_clipped_UBYTE_USHORT,
    [IM_LONG] = convert_clipped_UBYTE_LONG,
    [IM_ULONG] = convert_clipped_UBYTE_ULONG,
    [IM_FLOAT] = convert_clipped_UBYTE_FLOAT,
    [IM_DOUBLE] = convert_clipped_UBYTE_DOUBLE,
    [IM_COMPLEX] = convert_complex_clipped_UBYTE_COMPLEX,
    [IM_DCOMPLEX] = convert_complex_clipped_UBYTE_DCOMPLEX,
};

static improc_function_IIf convert_to_SHORT_clipped[IM_TYPEMAX] = {
    [IM_BINARY] = convert_USHORT_BINARY,
    [IM_BYTE] = convert_basic_SHORT_BYTE,
    [IM_UBYTE] = convert_basic_USHORT_UBYTE,
    [IM_USHORT] = convert_clipped_SHORT_USHORT,
    [IM_LONG] = convert_clipped_SHORT_LONG,
    [IM_ULONG] = convert_clipped_SHORT_ULONG,
    [IM_FLOAT] = convert_clipped_SHORT_FLOAT,
    [IM_DOUBLE] = convert_clipped_SHORT_DOUBLE,
    [IM_COMPLEX] = convert_complex_clipped_SHORT_COMPLEX,
    [IM_DCOMPLEX] = convert_complex_clipped_SHORT_DCOMPLEX,
};

static improc_function_IIf convert_to_USHORT_clipped[IM_TYPEMAX] = {
    [IM_BINARY] = convert_USHORT_BINARY,
    [IM_BYTE] = convert_clipped_USHORT_BYTE,
    [IM_UBYTE] = convert_basic_USHORT_UBYTE,
    [IM_SHORT] = convert_clipped_USHORT_SHORT,
    [IM_LONG] = convert_clipped_USHORT_LONG,
    [IM_ULONG] = convert_clipped_USHORT_ULONG,
    [IM_FLOAT] = convert_clipped_USHORT_FLOAT,
    [IM_DOUBLE] = convert_clipped_USHORT_DOUBLE,
    [IM_COMPLEX] = convert_complex_clipped_USHORT_COMPLEX,
    [IM_DCOMPLEX] = convert_complex_clipped_USHORT_DCOMPLEX,
};

static improc_function_IIf convert_to_LONG_clipped[IM_TYPEMAX] = {
    [IM_BINARY] = convert_ULONG_BINARY,
    [IM_BYTE] = convert_basic_LONG_BYTE,
    [IM_UBYTE] = convert_basic_ULONG_UBYTE,
    [IM_SHORT] = convert_basic_LONG_SHORT,
    [IM_USHORT] = convert_basic_ULONG_USHORT,
    [IM_ULONG] = convert_clipped_LONG_ULONG,
    [IM_FLOAT] = convert_clipped_LONG_FLOAT,
    [IM_DOUBLE] = convert_clipped_LONG_DOUBLE,
    [IM_COMPLEX] = convert_complex_clipped_LONG_COMPLEX,
    [IM_DCOMPLEX] = convert_complex_clipped_LONG_DCOMPLEX,
};

static improc_function_IIf convert_to_ULONG_clipped[IM_TYPEMAX] = {
    [IM_BINARY] = convert_ULONG_BINARY,
    [IM_BYTE] = convert_clipped_ULONG_BYTE,
    [IM_UBYTE] = convert_basic_ULONG_UBYTE,
    [IM_SHORT] = convert_clipped_ULONG_SHORT,
    [IM_USHORT] = convert_basic_ULONG_USHORT,
    [IM_LONG] = convert_clipped_ULONG_LONG,
    [IM_FLOAT] = convert_clipped_ULONG_FLOAT,
    [IM_DOUBLE] = convert_clipped_ULONG_DOUBLE,
    [IM_COMPLEX] = convert_complex_clipped_ULONG_COMPLEX,
    [IM_DCOMPLEX] = convert_complex_clipped_ULONG_DCOMPLEX,
};

static improc_function_IIf convert_to_FLOAT_clipped[IM_TYPEMAX] = {
    [IM_BINARY] = convert_FLOAT_BINARY,
    [IM_BYTE] = convert_basic_FLOAT_BYTE,
    [IM_UBYTE] = convert_basic_FLOAT_UBYTE,
    [IM_SHORT] = convert_basic_FLOAT_SHORT,
    [IM_USHORT] = convert_basic_FLOAT_USHORT,
    [IM_LONG] = convert_basic_FLOAT_LONG,
    [IM_ULONG] = convert_basic_FLOAT_ULONG,
    [IM_DOUBLE] = convert_clipped_FLOAT_DOUBLE,
    [IM_COMPLEX] = convert_complex_FLOAT_COMPLEX,
    [IM_DCOMPLEX] = convert_complex_clipped_FLOAT_DCOMPLEX,
};


static improc_function_IIf *convert_clipped[IM_TYPEMAX] = {
    [IM_BINARY] = convert_to_BINARY,
    [IM_BYTE] = convert_to_BYTE_clipped,
    [IM_UBYTE] = convert_to_UBYTE_clipped,
    [IM_SHORT] = convert_to_SHORT_clipped,
    [IM_USHORT] = convert_to_USHORT_clipped,
    [IM_LONG] = convert_to_LONG_clipped,
    [IM_ULONG] = convert_to_ULONG_clipped,
    [IM_FLOAT] = convert_to_FLOAT_clipped,
    [IM_DOUBLE] = convert_to_DOUBLE,
    [IM_COMPLEX] = convert_to_COMPLEX,
    [IM_DCOMPLEX] = convert_to_DCOMPLEX,
};



static improc_function_IIf clipped_round_to_BYTE[IM_TYPEMAX] = {
    [IM_FLOAT] = convert_clipped_round_BYTE_FLOAT,
    [IM_DOUBLE] = convert_clipped_round_BYTE_DOUBLE,
};

static improc_function_IIf clipped_round_to_UBYTE[IM_TYPEMAX] = {
    [IM_FLOAT] = convert_clipped_round_UBYTE_FLOAT,
    [IM_DOUBLE] = convert_clipped_round_UBYTE_DOUBLE,
};

static improc_function_IIf clipped_round_to_SHORT[IM_TYPEMAX] = {
    [IM_FLOAT] = convert_clipped_round_SHORT_FLOAT,
    [IM_DOUBLE] = convert_clipped_round_SHORT_DOUBLE,
};

static improc_function_IIf clipped_round_to_USHORT[IM_TYPEMAX] = {
    [IM_FLOAT] = convert_clipped_round_USHORT_FLOAT,
    [IM_DOUBLE] = convert_clipped_round_USHORT_DOUBLE,
};

static improc_function_IIf clipped_round_to_LONG[IM_TYPEMAX] = {
    [IM_FLOAT] = convert_clipped_round_LONG_FLOAT,
    [IM_DOUBLE] = convert_clipped_round_LONG_DOUBLE,
};

static improc_function_IIf clipped_round_to_ULONG[IM_TYPEMAX] = {
    [IM_FLOAT] = convert_clipped_round_ULONG_FLOAT,
    [IM_DOUBLE] = convert_clipped_round_ULONG_DOUBLE,
};

static improc_function_IIf *convert_clipped_round[IM_TYPEMAX] = {
    [IM_BYTE] = clipped_round_to_BYTE,
    [IM_UBYTE] = clipped_round_to_UBYTE,
    [IM_SHORT] = clipped_round_to_SHORT,
    [IM_USHORT] = clipped_round_to_USHORT,
    [IM_LONG] = clipped_round_to_LONG,
    [IM_ULONG] = clipped_round_to_ULONG,
};



/*
 * Do the lookup into the 2D conversion function arrays.
 *
 * We carefully check for NULLs in both dimensions as the rounding tables,
 * in particular, have many NULLs, and we don't want a crash here!
 */

static int do_conversion(improc_function_IIf *conversions[], ip_image *dest, ip_image *src, int flag)
{
    int done = 0;
    if (conversions[dest->type]) {
	if (conversions[dest->type][src->type]) {
	    conversions[dest->type][src->type](dest, src, flag);
	    done = 1;
	}
    }
    return done;
}


ip_image *im_convert(ip_image *src, ip_image_type type, int flag)
{
    ip_image *dest;
    int done;
    int flag_to_pass = 0;
    int bad_flag = 0;
    int clip = flag & FLG_CLIP;
    int roundr = flag & FLG_ROUND;
    int convert_part = flag & FLGPRT_CNVRT;

    ip_log_routine(__func__);

    if (NOT image_valid(src))
	return NULL;

    if (type == src->type) {
	ip_log_error(ERR_SAME_TYPE);
	ip_pop_routine();
	return NULL;
    }

    if (flag & FLGPRT_CNVRT_NOTUSED) {
	ip_log_error(ERR_BAD_FLAG);
	ip_pop_routine();
	return NULL;
    }

    /*** PALETTE image conversions special cases ***/
    if (type == IM_PALETTE && src->type == IM_UBYTE && flag == FLG_NONE) {
	dest = im_copy(src);
	if (dest)
	    im_ubyte_to_palette(dest, NULL);
	ip_pop_routine();
	return dest;
    }

    if (type == IM_UBYTE && src->type == IM_PALETTE && flag == FLG_NONE) {
	dest = im_copy(src);
	if (dest)
	    im_palette_to_ubyte(dest);
	ip_pop_routine();
	return dest;
    }

    /*
     * Checks on the flags
     */

    /* Conversion with BINARY images */
    if (src->type == IM_BINARY) {
	flag_to_pass = (convert_part == FLG_B1) ? 1 : 255;
    }else{
	if (convert_part == FLG_B1 || convert_part == FLG_B255)
	    bad_flag = 1;
    }

    /* Conversion with COMPLEX images */
    if (type == IM_COMPLEX || type == IM_DCOMPLEX) {
	if (convert_part == FLG_REAL  || convert_part == FLG_IMAG
		 || convert_part == 0)
	    flag_to_pass = convert_part;
	else
	    bad_flag = 1;
    }

    if (src->type == IM_COMPLEX || src->type == IM_DCOMPLEX) {
	if (convert_part == FLG_REAL || convert_part == FLG_IMAG ||
	    convert_part == FLG_MAG || convert_part == FLG_ANGLE || convert_part == 0)
	    flag_to_pass = convert_part;
	else
	    bad_flag = 1;
    }

    /* Integer/Real conversions with each other */
    if ((type_real(type) || type_integer(type)) &&
	(image_type_real(src) || image_type_integer(src))) {
	if (convert_part != 0)
	    bad_flag = 1;
    }

    if (roundr && !image_type_real(src))
	bad_flag = 1;

    /* Conversion from colour images */
    if (type_clrim(src->type)) {
	if (type_real(type) && !(convert_part == FLG_MAG || convert_part == 0))
	    bad_flag = 1;

	if (type_integer(type) && !(convert_part == FLG_RED || convert_part == FLG_GREEN
				    || convert_part == FLG_BLUE || convert_part == FLG_MAG
				    || convert_part == FLG_MAGYUV))
	    bad_flag = 1;

	if (!type_real(type) && !type_integer(type) && convert_part != NO_FLAG)
	    bad_flag = 1;

	if ((clip) || (roundr))
	    bad_flag = 1;

	flag_to_pass = convert_part;
    }

    /* If bad flags bail now */
    if (bad_flag) {
	ip_log_error(ERR_BAD_FLAG);
	ip_pop_routine();
	return NULL;
    }

    /* Allocate destination image */
    dest = ip_alloc_image(type, src->size);
    if (!dest) {
	ip_pop_routine();
	return NULL;
    }

    /* Now find the conversion; first try SIMD implementation */
#ifdef HAVE_SIMD
    done = 0;
    if (IP_USE_HWACCEL) {
	if (roundr) {
	    if (clip)
		done = do_conversion(ip_simd_convert_round_clipped, dest, src, flag_to_pass);
	    else
		done = do_conversion(ip_simd_convert_round, dest, src, flag_to_pass);
	}else{
	    if (clip)
		done = do_conversion(ip_simd_convert_clipped, dest, src, flag_to_pass);
	    else
		done = do_conversion(ip_simd_convert, dest, src, flag_to_pass);
	}
    }
    if (done) {
	ip_pop_routine();
	return dest;
    }
#endif

    /* Got here no SIMD so try pixel pumping code */

    if (clip) {
	/* Convert with clipping of values to destination type if downcasting */
	if (roundr)
	    done = do_conversion(convert_clipped_round, dest, src, flag_to_pass);
	else
	    done = do_conversion(convert_clipped, dest, src, flag_to_pass);
    }else{
	if (roundr)
	    done = do_conversion(convert_round, dest, src, flag_to_pass);
	else
	    done = do_conversion(convert_basic, dest, src, flag_to_pass);
    }

    if (!done) {
	ip_free_image(dest);
	ip_log_error(ERR_NOT_IMPLEMENTED);
	ip_pop_routine();
	return NULL;
    }

    ip_pop_routine();
    return dest;

}


/*

NAME:

   im_form_complex()

PROTOTYPE:

   #include <ip/ip.h>

   ip_image * im_form_complex( im, real, imag )
   ip_image *im;
   ip_image *real;
   ip_image *imag;

DESCRIPTION:

   Takes two FLOAT (or DOUBLE) images, namely 'real' and 'imag' and combines
   them together to make one new COMPLEX (respectively DCOMPLEX) image which
   is returned. Both images must be of the same size and type else an error
   is generated.

   If argument 'im' is non-NULL it must be an allocated image of same size
   as 'real' and 'imag' and of matching complex type, and if so, the result
   of forming the complex image from 'real' and 'imag' is written into 'im'
   and 'im' is returned as the result.  If 'im' is NULL then a new image is
   allocated to receive the result and is returned as the result.

   On error NULL is returned (whether argument 'im' was NULL or not).

ERRORS:

   ERR_BAD_TYPE
   ERR_NOT_SAME_SIZE

*/


ip_image *im_form_complex(ip_image *im, ip_image *real, ip_image *imag)
{
    int type;

    ip_log_routine(__func__);

    if (real->type != IM_FLOAT && real->type != IM_DOUBLE) {
	ip_log_error(ERR_BAD_TYPE);
	ip_pop_routine();
	return NULL;
    }

    if (NOT images_compatible(real, imag))
	return NULL;

    type = (real->type==IM_FLOAT) ? IM_COMPLEX : IM_DCOMPLEX;

    if (im) {
	if (im->type != type) {
	    ip_log_error(ERR_BAD_TYPE);
	    ip_pop_routine();
	    return NULL;
	}
	if (NOT images_same_size(im, real))
	    return NULL;
    }else{
	if ((im = ip_alloc_image(type, real->size)) == NULL) {
	    ip_pop_routine();
	    return NULL;
	}
    }

    if (im->type == IM_COMPLEX) {
	for (int j=0; j<real->size.y; j++) {
	    float *rptr, *iptr;
	    ip_complex *cptr;

	    rptr = IM_ROWADR(real, j, v);
	    iptr = IM_ROWADR(imag, j, v);
	    cptr = IM_ROWADR(im, j, v);

	    for (int i=0; i<real->size.x; i++) {
		cptr->r = *rptr++;
		cptr->i = *iptr++;
		cptr++;
	    }
	}
    }else{
	for (int j=0; j<real->size.y; j++) {
	    double *rptr, *iptr;
	    ip_dcomplex *cptr;

	    rptr = IM_ROWADR(real, j, v);
	    iptr = IM_ROWADR(imag, j, v);
	    cptr = IM_ROWADR(im, j, v);

	    for (int i=0; i<real->size.x; i++) {
		cptr->r = *rptr++;
		cptr->i = *iptr++;
		cptr++;
	    }
	}
    }

    ip_pop_routine();
    return im;
}



/*

NAME:

   im_form_rgb() \- Combine three images to make RGB image

PROTOTYPE:

   #include <ip/ip.h>

   ip_image * im_form_rgb( im, red, green, blue, alpha)
   ip_image *red;
   ip_image *green;
   ip_image *blue;
   ip_image *alpha;
   int flag;

DESCRIPTION:

   If 'red', 'green' and 'blue' are UBYTE images and 'alpha' is NULL then
   combine the red, green and blue parts to make an RGB image. If 'im' is
   NULL allocate the RGB image and return its handle .  If 'im' is non-null
   and a RGB image then use 'im' as the destination RGB image and return it
   as the handle.  All passed images must be the same size and compatible
   else an error is flagged.

   If 'red', 'green', 'blue' and 'alpha' are UBYTE images then combine the
   four images to make an RGBA image.  If 'im' is NULL allocate the RGBA
   image and return its handle .  If 'im' is non-null and is an RGBA image,
   then use 'im' as the destination RGBA image and return it as the handle.
   All passed images must be the same size and compatible else an error is
   flagged.

   If 'red', 'green' and 'blue' are USHORT images and 'alpha' is NULL then
   combine the red, green and blue parts to make an RGB16 image. If 'im' is
   NULL allocate the RGB16 image and return its handle .  If 'im' is
   non-null and is a RGB16 image then use 'im' as the destination RGB16
   image and return it as the handle.  All passed images must be the same
   size and compatible else an error is flagged.

   NULL is returned if an error occurs, else the resultant image handle is
   returned (which will be the same as input argument 'im' if 'im' is
   non-NULL).

ERRORS:

   ERR_BAD_TYPE
   ERR_NOT_SAME_SIZE

*/


ip_image *im_form_rgb(ip_image *im, ip_image *r, ip_image *g, ip_image *b, ip_image *a)
{
   int i,j;
   int type;

   ip_log_routine("im_form_rgb");

   if (r->type != IM_UBYTE && r->type != IM_USHORT) {
       ip_log_error(ERR_BAD_TYPE);
       ip_pop_routine();
       return NULL;
   }

   if (NOT images_compatible(r,g))
      return NULL;
   if (NOT images_compatible(r,b))
      return NULL;
   if (a) {
       if (NOT images_compatible(r,a))
	   return NULL;

       if (r->type != IM_UBYTE) {
	   ip_log_error(ERR_BAD_TYPE);
	   ip_pop_routine();
	   return NULL;
       }

       type = IM_RGBA;
   }else{
       type = (r->type == IM_UBYTE) ? IM_RGB : IM_RGB16;
   }

   if (im) {

       if (im->type != type) {
	   ip_log_error(ERR_BAD_TYPE);
	   ip_pop_routine();
	   return NULL;
       }
       if (NOT images_same_size(im, r))
	   return NULL;

   }else{

       if ((im = ip_alloc_image( type, r->size)) == NULL) {
	   ip_pop_routine();
	   return NULL;
       }

   }

   if (im->type == IM_RGB) {
       for (j=0; j<im->size.y; j++) {
	   uint8_t *rptr, *bptr, *gptr;
	   ip_rgb *imptr;

	   rptr = IM_ROWADR(r, j, v);
	   gptr = IM_ROWADR(g, j, v);
	   bptr = IM_ROWADR(b, j, v);
	   imptr = IM_ROWADR(im, j, v);

	   for (i=0; i<im->size.x; i++) {
	       imptr->r = *rptr++;
	       imptr->g = *gptr++;
	       imptr->b = *bptr++;
	       imptr++;
	   }
       }
   }else if (im->type == IM_RGBA) {
       for (j=0; j<im->size.y; j++) {
	   uint8_t *rptr, *bptr, *gptr, *aptr;
	   ip_rgba *imptr;

	   rptr = IM_ROWADR(r, j, v);
	   gptr = IM_ROWADR(g, j, v);
	   bptr = IM_ROWADR(b, j, v);
	   aptr = IM_ROWADR(a, j, v);
	   imptr = IM_ROWADR(im, j, v);

	   for (i=0; i<im->size.x; i++) {
	       imptr->r = *rptr++;
	       imptr->g = *gptr++;
	       imptr->b = *bptr++;
	       imptr->a = *aptr++;
	       imptr++;
	   }
       }
   }else{
       for (j=0; j<im->size.y; j++) {
	   uint16_t *rptr, *bptr, *gptr;
	   ip_lrgb *imptr;

	   rptr = IM_ROWADR(r, j, v);
	   gptr = IM_ROWADR(g, j, v);
	   bptr = IM_ROWADR(b, j, v);
	   imptr = IM_ROWADR(im, j, v);

	   for (i=0; i<im->size.x; i++) {
	       imptr->r = *rptr++;
	       imptr->g = *gptr++;
	       imptr->b = *bptr++;
	       imptr++;
	   }
       }
   }

   ip_pop_routine();
   return im;
}
