/*
 * Module: divide.c
 * Author: Michael J. Cree
 *
 * Copyright (C) 2016 by Michael J. Cree
 *
 * Integer division algorithms.
 */

#include <stdint.h>
#include <math.h>

#include <ip/divide.h>


/*
 * Let n and d be 32-bit unsigned integers. Calculate m, the integer
 * reciprocal of d (less 2^32), so that we can calculate n/d by multiplying
 * m by n with some shifts.  To get n/d accurately requires an m of 33-bit
 * width which we represent by subtracting 2^32 off as described by Granlund
 * and Montgomery 1994 ('Division by Invariant Integers Using
 * Multiplication', Proc. SIGPLAN '94 Conference on Programming Language
 * Design and Implementation).  ip_urcp32() below and ip_udiv32() in
 * divide.h (except for the case d==1 which we do not use in the IP library)
 * together implement the algorithm described in Figure 4.1 of ibid.
 *
 * Hence to perform repeated unsigned integer division with a common
 * denominator d call ip_urcp32_n32() once to get the reciprocal of d, then
 * call ip_udiv32_n32() on the many numerators to divide them all by d.
 * This can be more efficient than using a CPU integer divide instruction
 * even when the arch provides such an instruction.
 *
 * The effect of calling ip_urcp32_n32() with a zero argument is undefined.
 * Don't do it.
 *
 * On some architectures ip_urcp32_n33()/ip_udiv32_n33() provides a faster
 * implementation.
 */

struct ip_uint32_rcp_n32 ip_urcp32_n32(uint32_t d)
{
    struct ip_uint32_rcp_n32 rcp;
    double dd = (double)d;
    double l;

    l = ceil(log2(dd));
    rcp.m = (uint32_t)(floor(exp2(32.0) * (exp2(l) - dd) / dd) + 1.0);
    if (d > 1) {
	rcp.shift = (int)(l - 1.0);
	rcp.shift_1 = 1;
    }else{
	rcp.shift = 0;
	rcp.shift_1 = 0;
    }
    return rcp;
}


/*
 * Likewise for signed 32-bit integers.  ip_ircp32() together with
 * ip_idiv32() in divide.h implement the algorithm described in Figure 5.1
 * of ibid.
 */

struct ip_int32_rcp ip_ircp32(int32_t d)
{
    struct ip_int32_rcp rcp;
    double dd = fabs((double)d);
    double l;

    l = ceil(log2(dd));
    if (l < 1) l = 1;
    rcp.m = (1.0  + floor(exp2(31.0+l)/dd)) - exp2(32.0);
    rcp.dsign = d>>31;
    rcp.shift = (int32_t)l - 1;
    return rcp;
}



/*
 * Some architectures are able to perform the multiply-add with greater than
 * 32-bit precision which enables a more efficient way of calculating
 * unsigned 32-bit integer division.
 *
 * This implements the division scheme described by Robison 2005, "N-Bit
 * Unsigned Division Via N-Bit Multiply-Add," Proc. 17th IEEE
 * Symp. Comp. Arith. (ARITH'05) pp. 131-139.
 *
 * The effect of calling ip_urcp32_n33() with a zero argument is undefined.
 * Don't do it.
 */

struct ip_uint32_rcp_n33 ip_urcp32_n33(uint32_t d)
{
    struct ip_uint32_rcp_n33 r;
    double l;

    l = 31 - ip_clz(d);		/* floor(log2(d)) */

    r.shift = l;
    if ((d - (1<<r.shift)) == 0) {
	/* d is power of 2; turn mulitply-add into identity */
	r.m = 0xffffffff;
	r.b = 0xffffffff;
    }else{
	uint32_t t, s;

	t = (1ULL<<(r.shift+32)) / d;
	s = t*d + d;
	if (s <= (1<<r.shift)) {
	    r.m = t+1;
	    r.b = 0;
	}else{
	    r.m = t;
	    r.b = t;
	}
    }
    return r;
}


/*
 * 16-bit reciprocal and division.  Used by the SIMD code only.
 *
 * ip_urcp16_n16() to be used when simd_mulhi_USHORT is available.
 * ip_urcp16_n17() can be used if at higher precision.
 */


struct ip_uint16_rcp_n16 ip_urcp16_n16(uint16_t d)
{
    struct ip_uint16_rcp_n16 rcp;
    double dd = (double)d;
    double l;

    l = ceil(log2(dd));
    rcp.m = (uint16_t)(floor(exp2(16.0) * (exp2(l) - dd) / dd) + 1.0);
    if (d > 1) {
	rcp.shift = (int)(l - 1.0);
	rcp.shift_1 = 1;
    }else{
	rcp.shift = 0;
	rcp.shift_1 = 0;
    }
    return rcp;
}



struct ip_uint16_rcp_n17 ip_urcp16_n17(uint16_t d)
{
    struct ip_uint16_rcp_n17 r;
    double l;

    l = 31 - ip_clz(d);		/* floor(log2(d)) */

    r.shift = l;
    if ((d - (1<<r.shift)) == 0) {
	/* d is power of 2; turn mulitply-add into identity */
	r.m = 0xffff;
	r.b = 0xffff;
    }else{
	uint16_t t, s;

	t = (1UL<<(r.shift+16)) / d;
	s = t*d + d;
	if (s <= (1<<r.shift)) {
	    r.m = t+1;
	    r.b = 0;
	}else{
	    r.m = t;
	    r.b = t;
	}
    }
    return r;
}



struct ip_int16_rcp ip_ircp16(int16_t d)
{
    struct ip_int16_rcp rcp;
    double dd = fabs((double)d);
    double l;

    l = ceil(log2(dd));
    if (l < 1) l = 1;
    rcp.m = (1.0  + floor(exp2(15.0+l)/dd)) - exp2(16.0);
    rcp.dsign = d>>15;
    rcp.shift = (int)l - 1;
    return rcp;
}
