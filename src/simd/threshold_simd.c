/*
   Module: src/simd/threshold_simd.c
   Author: M.J.Cree

   SIMD implementation for thresholding images to generate a binary image.
   Entry points are in threshold.c

   Copyright (C) 2013, 2015 Michael J. Cree

*/

#include <math.h>

#include <ip/ip.h>

#include "../internal.h"
#include "../function_ops.h"

#include "simd.h"
#include "threshold_simd.h"



/*
 * Macro to threshold images of specified input type type with SIMD 'vector'
 * operators.  The rnd argument specifies an operator to apply to the low
 * and high values before using them (such as rounding for integer image
 * types).
 *
 * Destination image d is always a binary image which is the thresholded
 * result of source image s.
 * Source image s is of type type.
 *
 * simd_init_threshold_TYPE() is a macro from the arch specific simd header
 * that may modify vlow, vhigh and/or vres to put them in a form that
 * enables the most efficient processing of the threshold operator.
 *
 * The s2 form of the generate macro are to be used when there are two
 * source simd vectors for every output simd vector as happens when
 * thresholding (U)SHORT input.
 *
 * The s4 and s8 forms are likewise to be used when there are four (or
 * eight) source simd vectors needed for every single output vector.
 */

#define generate_image_threshold_function(type, rnd)			\
    static void								\
    ip_simd_threshold_ ## type(ip_image *d, ip_image *s,		\
			       double low, double high, uint8_t tv)	\
    {									\
	const int rowlen = d->row_size / sizeof(simd_##type);		\
	int ilow = rnd(low);						\
	int ihigh = rnd(high);						\
	simd_UBYTE vres = simd_splat_UBYTE(tv);				\
	simd_ ## type vlow = simd_splat_ ## type(ilow);			\
	simd_ ## type vhigh = simd_splat_ ## type(ihigh);		\
	simd_init_threshold_ ## type();					\
	for (int j=0; j<d->size.y; j++) {				\
	    simd_UBYTE *dptr = IM_ROWADR(d, j, v);			\
	    simd_ ## type *sptr = IM_ROWADR(s, j, v);			\
	    for (int i=0; i<rowlen; i++) {				\
		PREFETCH_READ_ONCE(sptr+PREFETCH_OFS);			\
		*dptr = simd_threshold_ ## type(*sptr, vlow, vhigh, vres); \
		dptr++; sptr+=sizeof(type ## _type);			\
	    }								\
	}								\
	SIMD_EXIT();							\
    }									\


#define generate_image_threshold_s2_function(type, rnd)			\
    static void								\
    ip_simd_threshold_ ## type(ip_image *d, ip_image *s,		\
			       double low, double high, uint8_t tv)	\
    {									\
	int rowlen = s->row_size / sizeof(simd_##type);			\
	const int extras = rowlen & 1;					\
	int ilow = rnd(low);						\
	int ihigh = rnd(high);						\
	simd_UBYTE vres = simd_splat_UBYTE(tv);				\
	rowlen >>= 1;							\
	simd_ ## type vlow = simd_splat_ ## type(ilow);			\
	simd_ ## type vhigh = simd_splat_ ## type(ihigh);		\
	simd_init_threshold_ ## type();					\
	for (int j=0; j<d->size.y; j++) {				\
	    simd_UBYTE *dptr = IM_ROWADR(d, j, v);			\
	    simd_##type *sptr = IM_ROWADR(s, j, v);			\
	    for (int i=0; i<rowlen; i++) {				\
		PREFETCH_READ_ONCE(sptr+PREFETCH_OFS);			\
		*dptr = simd_threshold_##type(sptr[0], sptr[1], vlow, vhigh, vres); \
		dptr++; sptr+=sizeof(type ## _type);			\
	    }								\
	    if (extras) {						\
		*dptr = simd_threshold_##type(sptr[0], sptr[0], vlow, vhigh, vres); \
	    }								\
	}								\
	SIMD_EXIT();							\
    }									\

#define generate_image_threshold_s4_function(type, rnd)			\
    static void								\
    ip_simd_threshold_ ## type(ip_image *d, ip_image *s,		\
			       double low, double high, uint8_t tv)	\
    {									\
	int rowlen = s->row_size / sizeof(simd_##type);			\
	const int extras = rowlen & 3;					\
	type ## _type xlow = rnd(low);					\
	type ## _type xhigh = rnd(high);				\
	simd_UBYTE vres = simd_splat_UBYTE(tv);				\
	rowlen >>= 2;							\
	simd_ ## type vlow = simd_splat_ ## type(xlow);			\
	simd_ ## type vhigh = simd_splat_ ## type(xhigh);		\
	simd_init_threshold_ ## type();					\
	for (int j=0; j<d->size.y; j++) {				\
	    simd_UBYTE *dptr = IM_ROWADR(d, j, v);			\
	    simd_ ## type *sptr = IM_ROWADR(s, j, v);			\
	    for (int i=0; i<rowlen; i++) {				\
		PREFETCH_READ_ONCE(sptr+PREFETCH_OFS);			\
		*dptr = simd_threshold_##type(sptr[0], sptr[1], sptr[2], sptr[3], \
					      vlow, vhigh, vres); \
		dptr++; sptr+=sizeof(type ## _type);			\
	    }								\
	    if (extras) {						\
		const simd_##type vzero = simd_splat_##type(0);		\
		*dptr= simd_threshold_##type(sptr[0], (extras>1) ? sptr[1] : vzero, \
					     (extras>2) ? sptr[2] : vzero, vzero, \
					     vlow, vhigh, vres);	\
	    }								\
	}								\
	SIMD_EXIT();							\
    }									\


#define generate_image_threshold_s8_function(type, rnd)			\
    static void								\
    ip_simd_threshold_ ## type(ip_image *d, ip_image *s,		\
			       double low, double high, uint8_t tv)	\
    {									\
	int rowlen = s->row_size / sizeof(simd_##type);			\
	const int extras = rowlen & 7;					\
	simd_UBYTE vres = simd_splat_UBYTE(tv);				\
	rowlen >>= 3;							\
	simd_ ## type vlow = simd_splat_ ## type(rnd(low));		\
	simd_ ## type vhigh = simd_splat_ ## type(rnd(high));		\
	simd_init_threshold_ ## type();					\
	for (int j=0; j<d->size.y; j++) {				\
	    simd_UBYTE *dptr = IM_ROWADR(d, j, v);			\
	    simd_ ## type *sptr = IM_ROWADR(s, j, v);			\
	    for (int i=0; i<rowlen; i++) {				\
		PREFETCH_READ_ONCE(sptr+PREFETCH_OFS);			\
		*dptr = simd_threshold_##type(sptr[0], sptr[1], sptr[2], sptr[3], \
					      sptr[4], sptr[5], sptr[6], sptr[7], \
					      vlow, vhigh, vres);	\
		dptr++; sptr+=sizeof(type ## _type);			\
	    }								\
	    if (extras) {						\
		const simd_##type vzero = simd_splat_##type(0);		\
		*dptr= simd_threshold_##type(sptr[0],\
					     (extras>1) ? sptr[1] : vzero, \
					     (extras>2) ? sptr[2] : vzero, \
					     (extras>3) ? sptr[3] : vzero, \
					     (extras>4) ? sptr[4] : vzero, \
					     (extras>5) ? sptr[5] : vzero, \
					     (extras>6) ? sptr[6] : vzero, \
					     vzero,			\
					     vlow, vhigh, vres);	\
	    }								\
	}								\
	SIMD_EXIT();							\
    }									\



#define ASIS(x) x


/*
 * Note that we assume if their is an SIMD operator for an unsigned integer
 * image it is always possible to create an operator for a signed integer
 * image, thus it exists and need not be tested for.
 */

#ifdef simd_threshold_UBYTE
generate_image_threshold_function(BYTE, (int8_t)IROUND)
generate_image_threshold_function(UBYTE, (uint8_t)IROUND)
#endif
#ifdef simd_threshold_USHORT
generate_image_threshold_s2_function(SHORT, (int16_t)IROUND)
generate_image_threshold_s2_function(USHORT, (uint16_t)IROUND)
#endif
#ifdef simd_threshold_ULONG
generate_image_threshold_s4_function(LONG, (int32_t)LROUND)
generate_image_threshold_s4_function(ULONG, (uint32_t)LROUND)
#endif
#ifdef simd_threshold_FLOAT
generate_image_threshold_s4_function(FLOAT, (float))
#endif
#ifdef simd_threshold_DOUBLE
generate_image_threshold_s8_function(DOUBLE, ASIS)
#endif


improc_function_IIccb ip_simd_threshold_bytype[IM_TYPEMAX] = {
#ifdef simd_threshold_UBYTE
    [IM_BYTE] = ip_simd_threshold_BYTE,
    [IM_UBYTE] = ip_simd_threshold_UBYTE,
#endif
#ifdef simd_threshold_USHORT
    [IM_SHORT] = ip_simd_threshold_SHORT,
    [IM_USHORT] = ip_simd_threshold_USHORT,
#endif
#ifdef simd_threshold_ULONG
    [IM_LONG] = ip_simd_threshold_LONG,
    [IM_ULONG] = ip_simd_threshold_ULONG,
#endif
#ifdef simd_threshold_FLOAT
    [IM_FLOAT] = ip_simd_threshold_FLOAT,
#endif
#ifdef simd_threshold_DOUBLE
    [IM_DOUBLE] = ip_simd_threshold_DOUBLE
#endif
};
