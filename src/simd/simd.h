/*
   Name: ip/simd/simd.h
   Author: M. J. Cree

   (C) 2012-2013, 2019 Michael J. Cree

   SIMD support for the IP library.

   This file detects the type of processor defined in config.h and includes
   the correct arch specific SIMD header file.

*/

#ifndef IP_SIMD_SIMD_H
#define IP_SIMD_SIMD_H

#include "../config.h"

#include <stdint.h>
#include <float.h>

/* Assume arch does not get benefit from prefetching */
#define PREFETCH_READ_WRITE(x)
#define PREFETCH_READ_ONCE(x)
#define PREFETCH_OFS 0

/* Include arch specfic header file */

#ifdef HAVE_INTEL_MMX
#include "simd_intel_mmx.h"
#endif

#ifdef HAVE_INTEL_SSE2
#include "simd_intel_sse.h"
#endif

#ifdef HAVE_INTEL_AVX2
#include "simd_intel_avx2.h"
#endif

#ifdef HAVE_ARM_NEON
#include "simd_arm_neon.h"
#endif

#ifdef HAVE_POWERPC_ALTIVEC
#include "simd_powerpc_altivec.h"
#endif

#ifdef HAVE_ALPHA_MVI
#include "simd_alpha_mvi.h"
#endif

#define SIMD_NUM_ELEMENTS(t) (sizeof(simd_ ## t) / sizeof(t ## _type))

#ifndef SIMD_EXIT
#define SIMD_EXIT()
#endif

#endif
