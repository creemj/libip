/*
   Name: ip/simd/intel_sse.h
   Author: M. J. Cree

   Copyright (C) 2015-2018 Michael J. Cree

   SIMD support for the IP library.

   This defines the Intel AVX2 support.

*/


/*
 * Intel AVX2 SIMD support
 *
 * Intel AVX/AVX2 SIMD is frustrating to program with because:
 * 1) AVX2 is 2x128-bit and not truly 256-bit thus the full-vector shift and
 *    shuffle/permute intrinsics are broken.
 * 2) All other potentially useful instructions (extract, insert, shifts)
 *    require an immediate operand thus are useless for general purpose.
 * In short, AVX2 is fundamentally flawed.
 *
 * Hence some convoluted implementations result below to get useful simd
 * functionality, so much so, that in some cases the SSE implementation
 * might actually be faster.
 */

#include <immintrin.h>

#include <ip/divide.h>


/* Basic SIMD types */
typedef __m256i simd_UBYTE;
typedef __m256i simd_BYTE;
typedef __m256i simd_USHORT;
typedef __m256i simd_SHORT;
typedef __m256i simd_ULONG;
typedef __m256i simd_LONG;
typedef __m256  simd_FLOAT;
typedef __m256d simd_DOUBLE;
typedef __m256  simd_COMPLEX;
typedef __m256d simd_DCOMPLEX;

/*
 * We only define ULONG64 type (and consequently LONG64 for completeness) to
 * enable manipulation of image row pointers, i.e., to do size_t type
 * calculations efficiently.
 */
typedef __m256i simd_ULONG64;
typedef __m256i simd_LONG64;

/*
 * simd_splat
 *
 * For splatting a scalar value across the simd vector.
 */
#define simd_splat_UBYTE(c)   _mm256_set1_epi8(c)
#define simd_splat_BYTE(c)    _mm256_set1_epi8(c)
#define simd_splat_USHORT(c)  _mm256_set1_epi16(c)
#define simd_splat_SHORT(c)   _mm256_set1_epi16(c)
#define simd_splat_ULONG(c)   _mm256_set1_epi32(c)
#define simd_splat_LONG(c)    _mm256_set1_epi32(c)
#define simd_splat_ULONG64(c) _mm256_set1_epi64x(c)
#define simd_splat_LONG64(c)  _mm256_set1_epi64x(c)
#define simd_splat_FLOAT(c)   _mm256_set1_ps(c)
#define simd_splat_DOUBLE(c)  _mm256_set1_pd(c)

#define simd_splat_COMPLEX(u,v)  _mm256_set_ps(v,u,v,u,v,u,v,u)
#define simd_splat_DCOMPLEX(u,v) _mm256_set_pd(v,u,v,u)

/*
 * Now that the splat routines are defined and, thus, indicate which image
 * types we support in SIMD, now get the rest of the typedefs that are
 * needed for the convert routines below.
 */

#include "simd_td.h"

/*
 * simd_splat_v0
 *
 * Splat the zeroth element across the whole vector.
 */
#define simd_splat_v0_BYTE(x) _mm256_broadcastb_epi8(_mm256_extracti128_si256(x, 0))
#define simd_splat_v0_UBYTE(x) _mm256_broadcastb_epi8(_mm256_extracti128_si256(x, 0))
#define simd_splat_v0_SHORT(x) _mm256_broadcastw_epi16(_mm256_extracti128_si256(x, 0))
#define simd_splat_v0_USHORT(x) _mm256_broadcastw_epi16(_mm256_extracti128_si256(x, 0))
#define simd_splat_v0_LONG(x) _mm256_broadcastd_epi32(_mm256_extracti128_si256(x, 0))
#define simd_splat_v0_ULONG(x) _mm256_broadcastd_epi32(_mm256_extracti128_si256(x, 0))
#define simd_splat_v0_LONG64(x) _mm256_broadcastq_epi64(_mm256_extracti128_si256(x, 0))
#define simd_splat_v0_ULONG64(x) _mm256_broadcastq_epi64(_mm256_extracti128_si256(x, 0))
#define simd_splat_v0_FLOAT(x) _mm256_broadcastss_ps(_mm256_extractf128_ps(x, 0))
#define simd_splat_v0_DOUBLE(x) _mm256_broadcastsd_pd(_mm256_extractf128_pd(x, 0))

/*
 * simd_mask
 *
 * Masks for multielement image types (complex and the rgb image types)
 */
#define simd_mask_COMPLEX(r, i) _mm256_set_epi32(i, r, i, r, i, r, i, r)
#define simd_mask_DCOMPLEX(r, i) _mm256_set_epi32(i, i, r, r, i, i, r, r)
#define simd_mask_RGB(r, g, b) _mm256_set_epi8(g, r, b, g, r, b, g, r, b, g, r, b, g, r, b, g, \
					       r, b, g, r, b, g, r, b, g, r, b, g, r, b, g, r)
#define simd_mask_RGBA(r, g, b, a) _mm256_set_epi8(a, b, g, r, a, b, g, r, a, b, g, r, a, b, g, r, \
						   a, b, g, r, a, b, g, r, a, b, g, r, a, b, g, r)
#define simd_mask_RGB16(r, g, b) _mm256_set_epi16(r, b, g, r, b, g, r, b, g, r, b, g, r, b, g, r)


/*
 * simd_extract
 *
 * Extract the specified element from a vector.
 *
 * AVX2 does not support this well as the standard extract intrinsic takes
 * an immediate argument only.  We therefore resort to a memory access via a
 * union to get to the element.  Hopefully the compiler can sometimes
 * optimise the memory access away.
 */
union ip_simd_access {
    __m256i v;
    uint8_t ub[32];
    uint16_t us[16];
    uint32_t ul[8];
};
static inline int _ip_simd_extract_UBYTE(simd_UBYTE u, int pos)
{
    union ip_simd_access x;
    x.v = u;
    return x.ub[pos];
}
#define simd_extract_BYTE(v, p) _ip_simd_extract_UBYTE(v, p)
#define simd_extract_UBYTE(v, p) _ip_simd_extract_UBYTE(v, p)

static inline int _ip_simd_extract_USHORT(simd_USHORT u, int pos)
{
    union ip_simd_access x;
    x.v = u;
    return x.us[pos];
}
#define simd_extract_SHORT(v, p) _ip_simd_extract_USHORT(v, p)
#define simd_extract_USHORT(v, p) _ip_simd_extract_USHORT(v, p)


/*
 * It is useful to have a macro to shuffle int32 elements about.
 */

static inline __m256i avx_shuffle_epi32(__m256i a, __m256i b, const int ctr)
{
    return (__m256i)_mm256_shuffle_ps((__m256)a, (__m256)b, ctr);
}


/*
 * It is useful to have a macro to swap 128-bit banks as there are no
 * 256-bit shifts, etc.
 * These two instructions are costly; 3 cycles latency.
 */

#define avx_swap_128bit_banks(x)  _mm256_permute4x64_epi64(x, 0x4e)
#define avx_swap_inner_64bit_banks(x)  _mm256_permute4x64_epi64(x, 0xd8)

/*
 * simd_perm
 *
 * Intel AVX2 does not provide a proper 256-bit byte permutation
 * instruction.  We construct one here.
 */

static inline simd_UBYTE _ip_simd_perm_UBYTE(simd_UBYTE x, simd_UBYTE idx)
{
    simd_UBYTE idx_own, idx_other, idx128;
    const simd_UBYTE signbit_low =
	_mm256_setr_epi8(0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80,
			 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80,
			 0,    0,    0,    0,    0,    0,    0,    0,
			 0,    0,    0,    0,    0,    0,    0,    0);
    const simd_UBYTE signbit_all =
	_mm256_setr_epi8(0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80,
			 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80,
			 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80,
			 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80);

    /* which 128-bit bank to sign bit */
    idx128 = _mm256_slli_epi64(idx, 3);
    /* make sign bit indicate whether own 128-bit bank or the other 128-bit bank */
    idx128 = _mm256_xor_si256(signbit_low, idx128);

    /* Ensure sign bit (128-bit bank indicator) cleared in idx */
    idx = _mm256_andnot_si256(signbit_all, idx);
    /* Ensure 128-bit bank indicator (sign bit) only in idx128 */
    idx128 = _mm256_and_si256(signbit_all, idx128);

    /* Contains indices for perm result where byte comes from own 128-bit bank */
    idx_own = _mm256_or_si256(_mm256_xor_si256(signbit_all, idx128), idx);
    /* Contains (swapped) indices for perm result where byte comes from other 128-bit bank */
    idx_other = avx_swap_128bit_banks(_mm256_or_si256(idx, idx128));

    /*
     * Permutate; note if sign bit set in index then result byte is
     * zero. This is important for final OR below to give correct result.
     */
    idx_own = _mm256_shuffle_epi8(x, idx_own);
    idx_other = _mm256_shuffle_epi8(x, idx_other);

    /* Swap back banks in idx_other */
    idx_other = avx_swap_128bit_banks(idx_other);

    /* Construct result */
    return _mm256_or_si256(idx_own, idx_other);
}
#define simd_perm_UBYTE(x, idx) _ip_simd_perm_UBYTE(x, idx)


/*
 * Helper function: select bits of a where corresponding bit of vs is true
 * else select bit of b.
 */
static inline __m256i v_select(__m256i vs, __m256i a, __m256i b)
{
    return _mm256_blendv_epi8(b, a, vs);
}

static inline __m256 v_selectf(__m256 vs, __m256 a, __m256 b)
{
    return _mm256_blendv_ps(b, a, vs);
}

static inline __m256d v_selectd(__m256d vs, __m256d a, __m256d b)
{
    return _mm256_blendv_pd(b, a, vs);
}

/*
 * simd_splat_vN
 *
 * Splat Nth element of vector to whole vector. Uses simd_perm above so is
 * not particularly efficient, but presumably is faster (have not tested
 * this) than using the extract primitives above.
 */
static inline simd_USHORT _ip_splat_vN_USHORT(simd_USHORT x, int pos)
{
    const simd_USHORT incr = simd_splat_USHORT(0x100);
    simd_UBYTE idx = _mm256_add_epi8(simd_splat_UBYTE(2*pos), incr);
    return simd_perm_UBYTE(x, idx);
}
static inline simd_ULONG _ip_splat_vN_ULONG(simd_ULONG x, int pos)
{
    const simd_USHORT incr = simd_splat_ULONG(0x03020100);
    simd_UBYTE idx = _mm256_add_epi8(simd_splat_UBYTE(4*pos), incr);
    return simd_perm_UBYTE(x, idx);
}
static inline simd_DOUBLE _ip_splat_vN_DOUBLE(simd_DOUBLE x, int pos)
{
    const simd_ULONG incr = _mm256_setr_epi32(0x03020100, 0x07060504,
					      0x03020100, 0x07060504,
					      0x03020100, 0x07060504,
					      0x03020100, 0x07060504);
    simd_UBYTE idx = _mm256_add_epi8(simd_splat_UBYTE(8*pos), incr);
    return (simd_DOUBLE)simd_perm_UBYTE((simd_UBYTE)x, idx);
}
#define simd_splat_vN_BYTE(x, n) simd_perm_UBYTE(x, simd_splat_BYTE(n))
#define simd_splat_vN_UBYTE(x, n) simd_perm_UBYTE(x, simd_splat_UBYTE(n))
#define simd_splat_vN_SHORT(x, n) _ip_splat_vN_USHORT(x, n)
#define simd_splat_vN_USHORT(x, n) _ip_splat_vN_USHORT(x, n)
#define simd_splat_vN_LONG(x, n) _ip_splat_vN_ULONG(x, n)
#define simd_splat_vN_ULONG(x, n) _ip_splat_vN_ULONG(x, n)
#define simd_splat_vN_FLOAT(x, n) ((simd_FLOAT)_ip_splat_vN_ULONG((simd_ULONG)x, n))
#define simd_splat_vN_DOUBLE(x, n) _ip_splat_vN_DOUBLE(x, n)

/*
 * simd_cvu_imm
 *
 * Construct vector unaligned from two neighbouring aligned vectors with the
 * offset known at compile time.
 *
 * Due to the brokeness of the AVX2 2x128-bit implementation we have to go
 * to ridiculous lengths to get a simd_cvu_imm implementation.  The
 * permute4x64 and permute2x128 instructions (i.e. anything that bridges the
 * two 128-bit SIMD units) are rather slow at 3 cycles on Haswell, so all
 * up, this is probably much slower than using SSE!!!!
 */
static inline simd_UBYTE _ip_simd_cvu_imm_UBYTE(simd_UBYTE l, simd_UBYTE r, const int offset)
{
    simd_UBYTE v1, v2, v3;
    const int start = (offset >> 4) & 1;
    const unsigned char idx = offset & 0x0f;
    /*
     * Since start is known at compile time we expect that the compiler will
     * optimise away the unused branch of this conditional.
     */
    if (start) {
	/* Second sixteen bytes, i.e., high 128 bits */
	v1 = _mm256_permute4x64_epi64(l, 0x0e);
	v2 = r;
	v3 = _mm256_permute4x64_epi64(r, 0x0e);
    }else{
	/* First sixteen bytes, i.e., low 128 bits */
	v1 = l;
	v2 = _mm256_permute4x64_epi64(l, 0x0e);
	v3 = r;
    }
    v1 = _mm256_alignr_epi8(v2, v1, idx);
    v2 = _mm256_alignr_epi8(v3, v2, idx);
    return _mm256_permute2x128_si256(v1, v2, 0x20);
}
#define simd_cvu_imm_UBYTE(l, r, o) _ip_simd_cvu_imm_UBYTE(l, r, o)
#define simd_cvu_imm_BYTE(l, r, o) _ip_simd_cvu_imm_UBYTE(l, r, o)
#define simd_cvu_imm_USHORT(l, r, o) _ip_simd_cvu_imm_UBYTE(l, r, 2*(o))
#define simd_cvu_imm_SHORT(l, r, o) _ip_simd_cvu_imm_UBYTE(l, r, 2*(o))
#define simd_cvu_imm_ULONG(l, r, o) _ip_simd_cvu_imm_UBYTE(l, r, 4*(o))
#define simd_cvu_imm_LONG(l, r, o) _ip_simd_cvu_imm_UBYTE(l, r, 4*(o))
#define simd_cvu_imm_FLOAT(l, r, o) \
    ((simd_FLOAT)_ip_simd_cvu_imm_UBYTE((simd_UBYTE)l, (simd_UBYTE)r, 4*(o)))
#define simd_cvu_imm_DOUBLE(l, r, o) \
    ((simd_DOUBLE)_ip_simd_cvu_imm_UBYTE((simd_UBYTE)l, (simd_UBYTE)r, 8*(o)))

/*
 * We do not implement simd_cvu on AVX2.  It is far too broken for such
 * sensibility.
 */

/*
 * simd_ldu
 *
 * Load unaligned vector from arbitrary memory address.
 *
 * Some code, on finding no simd_cvu_TYPE macros, will attempt to use
 * simd_ldu_TYPE macros instead.
 */
#define simd_ldu_UBYTE(m) _mm256_lddqu_si256((__m256i *)(m))
#define simd_ldu_BYTE(m) _mm256_lddqu_si256((__m256i *)(m))
#define simd_ldu_SHORT(m) _mm256_lddqu_si256((__m256i *)(m))
#define simd_ldu_USHORT(m) _mm256_lddqu_si256((__m256i *)(m))
#define simd_ldu_ULONG(m) _mm256_lddqu_si256((__m256i *)(m))
#define simd_ldu_LONG(m) _mm256_lddqu_si256((__m256i *)(m))
#define simd_ldu_FLOAT(m) ((simd_FLOAT)_mm256_lddqu_si256((__m256i *)(m)))
#define simd_ldu_DOUBLE(m) ((simd_DOUBLE)_mm256_lddqu_si256((__m256i *)(m)))

/*
 * Scattered memory loads.  Despite the brokenness of AVX2, the gather load
 * instructions are a rather nice idea.  We make use of them here.
 *
 * simd_column_load_ULONG(adr, offsets)
 *
 * Loads ULONGs down a column starting at address adr.  The first 4-byte
 * element is loaded from (adr+offset[0]), the second from (adr+offset[1]),
 * and so on, where adr is a byte pointer.  Since an AVX vector has eight
 * ULONGs we get eight ULONGs loaded from arbitrary addresses, but we call
 * this column_load because the intended use is that the offsets be the row
 * offsets for sequential rows in an image.
 */

static inline simd_ULONG ip_simd_column_load_ULONG(size_t col, int idx, simd_ULONG64 *rows)
{
    __m128i first, second;
    __m256i idx1, idx2;

    idx1 = rows[idx];
    idx2 = rows[idx+1];
    first = _mm256_i64gather_epi32((void *)col, idx1, 1);
    second = _mm256_i64gather_epi32((void *)col, idx2, 1);
    return _mm256_inserti128_si256(_mm256_castsi128_si256(first), second, 1);
}

static inline simd_ULONG_2 simd_column_load_2_ULONG(size_t col, void *adrs)
{
    simd_ULONG_2 res;

    res.ul[0] = ip_simd_column_load_ULONG(col, 0, adrs);
    res.ul[1] = ip_simd_column_load_ULONG(col, 2, adrs);
    return res;
}


static inline simd_ULONG_4 simd_column_load_4_ULONG(size_t col, void *adrs)
{
    simd_ULONG_4 res;

    res.ul[0] = ip_simd_column_load_ULONG(col, 0, adrs);
    res.ul[1] = ip_simd_column_load_ULONG(col, 2, adrs);
    res.ul[2] = ip_simd_column_load_ULONG(col, 4, adrs);
    res.ul[3] = ip_simd_column_load_ULONG(col, 6, adrs);
    return res;
}

#define simd_column_load_ULONG(col, adr) ip_simd_column_load_ULONG(col, 0, (simd_ULONG64 *)(adr))


/*
 * Helpers needed for column write
 */

static inline simd_ULONG_4
ip_simd_transpose_ul_4x4x2(simd_ULONG a, simd_ULONG b, simd_ULONG c, simd_ULONG d)
{
    simd_ULONG tmp1, tmp2, tmp3, tmp4;
    simd_ULONG_4 res;

    tmp1 = _mm256_unpacklo_epi32(a, b);
    tmp3 = _mm256_unpacklo_epi32(c, d);
    tmp2 = _mm256_unpackhi_epi32(a, b);
    tmp4 = _mm256_unpackhi_epi32(c, d);
    res.ul[0] = _mm256_unpacklo_epi64(tmp1, tmp3);
    res.ul[2] = _mm256_unpacklo_epi64(tmp2, tmp4);
    res.ul[1] = _mm256_unpackhi_epi64(tmp1, tmp3);
    res.ul[3] = _mm256_unpackhi_epi64(tmp2, tmp4);
    return res;
}

static inline void ip_simd_store2_m128i(__m128i *hi, __m128i *lo, __m256i x)
{
    *lo = _mm256_castsi256_si128(x);
    *hi = _mm256_extractf128_si256(x, 1);
}

static inline __m128i *get_imloc_address(uint32_t **adr, int x, int y)
{
    return (__m128i *)(adr[y] + x);
}


/*
 * Column write; writes SSE wide vectors
 */

static inline void ip_simd_column_write4_ULONG(int x, int y, void *adr,
                                               simd_ULONG a, simd_ULONG b, simd_ULONG c, simd_ULONG d)
{
    simd_ULONG_4 tmp;
    uint32_t **rowptr = (uint32_t **)adr;
    __m128i *high, *low;

    tmp = ip_simd_transpose_ul_4x4x2(a, b, c, d);

    low = get_imloc_address(rowptr, x, y);
    high = get_imloc_address(rowptr, x, y+4);
    ip_simd_store2_m128i(high, low, tmp.ul[0]);

    low = get_imloc_address(rowptr, x, y+1);
    high = get_imloc_address(rowptr, x, y+5);
    ip_simd_store2_m128i(high, low, tmp.ul[1]);

    low = get_imloc_address(rowptr, x, y+2);
    high = get_imloc_address(rowptr, x, y+6);
    ip_simd_store2_m128i(high, low, tmp.ul[2]);

    low = get_imloc_address(rowptr, x, y+3);
    high = get_imloc_address(rowptr, x, y+7);
    ip_simd_store2_m128i(high, low, tmp.ul[3]);
}
#define simd_column_write4_ULONG(a,b,c,d,e,f,g) ip_simd_column_write4_ULONG(a,b,c,d,e,f,g)


/*
 * Tranposes of matrix of simd vectors
 *
 * These first two are for transposing the column vector loaded with the
 * column loads above into row vectors.  This is used in the AVX2 (U)BYTE
 * and (U)SHORT image transposes (which would otherwise incur register spill
 * if implemented with the more conventional approach used for (U)LONG/FLOAT
 * and other bigger image types).
 */

static inline simd_UBYTE_4
_ip_simd_transpose_4_UBYTE(simd_ULONG_4 in)
{
    const simd_ULONG byte_resort = _mm256_set_epi8(15, 11, 7, 3, 14, 10, 6, 2, 13, 9, 5, 1, 12, 8, 4, 0,
						   15, 11, 7, 3, 14, 10, 6, 2, 13, 9, 5, 1, 12, 8, 4, 0);
    simd_ULONG u, v, x, y;
    simd_UBYTE_4 vec;
    simd_UBYTE a = in.ul[0];
    simd_UBYTE b = in.ul[1];
    simd_UBYTE c = in.ul[2];
    simd_UBYTE d = in.ul[3];
    /* Sort bytes to give correct ULONG elements in vectors */
    a = _mm256_shuffle_epi8(a, byte_resort);
    b = _mm256_shuffle_epi8(b, byte_resort);
    c = _mm256_shuffle_epi8(c, byte_resort);
    d = _mm256_shuffle_epi8(d, byte_resort);
    /* Sort 128-bit halves to put them in the correct high or low bank */
    u = _mm256_permute2x128_si256(a, c, 0x20);
    v = _mm256_permute2x128_si256(a, c, 0x31);
    x = _mm256_permute2x128_si256(b, d, 0x20);
    y = _mm256_permute2x128_si256(b, d, 0x31);
    /* Sort 64-bit halves to put them somewhere useful */
    a = avx_shuffle_epi32(u, x, 0x44);
    b = avx_shuffle_epi32(u, x, 0xee);
    c = avx_shuffle_epi32(v, y, 0x11);
    d = avx_shuffle_epi32(v, y, 0xbb);
    /* And a blend sorts into the four vectors */
    u = _mm256_blend_epi32(a, c, 0xaa);
    v = _mm256_blend_epi32(a, c, 0x55);
    x = _mm256_blend_epi32(b, d, 0xaa);
    y = _mm256_blend_epi32(b, d, 0x55);
    vec.ub[0] = u;
    vec.ub[1] = _mm256_shuffle_epi32(v, 0xb1);
    vec.ub[2] = x;
    vec.ub[3] = _mm256_shuffle_epi32(y, 0xb1);
    return vec;
}
#define simd_transpose_4_UBYTE _ip_simd_transpose_4_UBYTE



static inline simd_USHORT_2
_ip_simd_transpose_2_USHORT(simd_ULONG_2 in)
{
    const simd_ULONG short_resort = _mm256_set_epi8(15, 14, 11, 10, 7, 6, 3, 2, 13, 12, 9, 8, 5, 4, 1, 0,
						    15, 14, 11, 10, 7, 6, 3, 2, 13, 12, 9, 8, 5, 4, 1, 0);
    simd_USHORT_2 res;
    simd_USHORT a = in.ul[0];
    simd_USHORT b = in.ul[1];
    simd_USHORT c;

    a = _mm256_shuffle_epi8(a, short_resort);
    b = _mm256_shuffle_epi8(b, short_resort);
    c = _mm256_permute2f128_si256(a, b, 0x20);
    a = _mm256_permute2f128_si256(a, b, 0x31);
    res.us[0] = (__m256i)_mm256_shuffle_pd((__m256d)c, (__m256d)a, 0);
    res.us[1] = (__m256i)_mm256_shuffle_pd((__m256d)c, (__m256d)a, 0xf);;

    return res;
}
#define simd_transpose_2_USHORT(a) _ip_simd_transpose_2_USHORT(a)

/*
 * The following are transposes of square matrices of elements consisting of
 * eight SIMD vectors (in the case of float/long/ulong), four SIMD vectors
 * (in the case of double/complex) or two SIMD vectors (in the case of
 * dcomplex).
 */

/*
 * simd_transpose_FLOAT() - 8x8 transpose of FLOAT in eight AVX vectors.
 * Helper functions follow first --- note the helpers don't do true
 * transposes despite their name, but the final result is the desired
 * transpose.
 */

static inline simd_FLOAT_2 avx2_simd_transpose_x2_FLOAT(simd_FLOAT a, simd_FLOAT b)
{
    simd_FLOAT_2 res;

    res.f[0] = _mm256_unpacklo_ps(a, b);
    res.f[1] = _mm256_unpackhi_ps(a, b);
    return res;
}

static inline simd_FLOAT_2 avx2_simd_transpose_x4h_FLOAT(simd_FLOAT a, simd_FLOAT b)
{
    simd_FLOAT_2 res;

    res.f[0] = _mm256_shuffle_ps(a, b, 0x44);
    res.f[1] = _mm256_shuffle_ps(a, b, 0xee);
    return res;
}

static inline simd_FLOAT_2 avx2_simd_transpose_x8q_FLOAT(simd_FLOAT a, simd_FLOAT b)
{
    simd_FLOAT_2 res;

    res.f[0] = _mm256_permute2f128_ps(a, b, 0x20);
    res.f[1] = _mm256_permute2f128_ps(a, b, 0x31);
    return res;
}

static inline simd_FLOAT_8 _ip_simd_transpose_FLOAT(simd_FLOAT_8 x)
{
    simd_FLOAT_2 u, v, e1, e2, e3, e4;
    simd_FLOAT_8 res;

    u = avx2_simd_transpose_x2_FLOAT(x.f[0], x.f[1]);
    v = avx2_simd_transpose_x2_FLOAT(x.f[2], x.f[3]);
    e1 = avx2_simd_transpose_x4h_FLOAT(u.f[0], v.f[0]);
    e2 = avx2_simd_transpose_x4h_FLOAT(u.f[1], v.f[1]);
    u = avx2_simd_transpose_x2_FLOAT(x.f[4], x.f[5]);
    v = avx2_simd_transpose_x2_FLOAT(x.f[6], x.f[7]);
    e3 = avx2_simd_transpose_x4h_FLOAT(u.f[0], v.f[0]);
    e4 = avx2_simd_transpose_x4h_FLOAT(u.f[1], v.f[1]);
    u = avx2_simd_transpose_x8q_FLOAT(e1.f[0], e3.f[0]);
    v = avx2_simd_transpose_x8q_FLOAT(e1.f[1], e3.f[1]);
    res.f[0] = u.f[0]; res.f[4] = u.f[1];
    res.f[1] = v.f[0]; res.f[5] = v.f[1];
    u = avx2_simd_transpose_x8q_FLOAT(e2.f[0], e4.f[0]);
    v = avx2_simd_transpose_x8q_FLOAT(e2.f[1], e4.f[1]);
    res.f[2] = u.f[0]; res.f[6] = u.f[1];
    res.f[3] = v.f[0]; res.f[7] = v.f[1];
    return res;
}
#define simd_transpose_FLOAT(x) _ip_simd_transpose_FLOAT(x)

static inline simd_ULONG_8 _ip_simd_transpose_ULONG(simd_ULONG_8 x)
{
    simd_FLOAT_8 f8;

    f8.f[0] = (simd_FLOAT)x.ul[0];
    f8.f[1] = (simd_FLOAT)x.ul[1];
    f8.f[2] = (simd_FLOAT)x.ul[2];
    f8.f[3] = (simd_FLOAT)x.ul[3];
    f8.f[4] = (simd_FLOAT)x.ul[4];
    f8.f[5] = (simd_FLOAT)x.ul[5];
    f8.f[6] = (simd_FLOAT)x.ul[6];
    f8.f[7] = (simd_FLOAT)x.ul[7];
    f8 = simd_transpose_FLOAT(f8);
    x.ul[0] = (simd_ULONG)f8.f[0];
    x.ul[1] = (simd_ULONG)f8.f[1];
    x.ul[2] = (simd_ULONG)f8.f[2];
    x.ul[3] = (simd_ULONG)f8.f[3];
    x.ul[4] = (simd_ULONG)f8.f[4];
    x.ul[5] = (simd_ULONG)f8.f[5];
    x.ul[6] = (simd_ULONG)f8.f[6];
    x.ul[7] = (simd_ULONG)f8.f[7];
    return x;
}
#define simd_transpose_ULONG(a) _ip_simd_transpose_ULONG(a)

/*
 * simd_transpose_DOUBLE() - 4x4 transpose of DOUBLE in four AVX vectors.
 * Helper functions follow first.
 */

static inline simd_DOUBLE_2 avx2_simd_transpose_x2_DOUBLE(simd_DOUBLE a, simd_DOUBLE b)
{
    simd_DOUBLE_2 res;

    res.d[0] = _mm256_shuffle_pd(a, b, 0x00);
    res.d[1] = _mm256_shuffle_pd(a, b, 0x0f);
    return res;
}

static inline simd_DOUBLE_2 avx2_simd_transpose_x4h_DOUBLE(simd_DOUBLE a, simd_DOUBLE b)
{
    simd_DOUBLE_2 res;

    res.d[0] = _mm256_permute2f128_pd(a, b, 0x20);
    res.d[1] = _mm256_permute2f128_pd(a, b, 0x31);
    return res;
}

static inline simd_DOUBLE_4 _ip_simd_transpose_DOUBLE(simd_DOUBLE_4 x)
{
    simd_DOUBLE_2 u, v, e;

    u = avx2_simd_transpose_x2_DOUBLE(x.d[0], x.d[1]);
    v = avx2_simd_transpose_x2_DOUBLE(x.d[2], x.d[3]);
    e = avx2_simd_transpose_x4h_DOUBLE(u.d[0], v.d[0]);
    x.d[0] = e.d[0];
    x.d[2] = e.d[1];
    e = avx2_simd_transpose_x4h_DOUBLE(u.d[1], v.d[1]);
    x.d[1] = e.d[0];
    x.d[3] = e.d[1];
    return x;
}
#define simd_transpose_DOUBLE(a) _ip_simd_transpose_DOUBLE(a)

static inline simd_COMPLEX_4 _ip_simd_transpose_COMPLEX(simd_COMPLEX_4 x)
{
    simd_DOUBLE_4 d4;

    d4.d[0] = (simd_DOUBLE)x.c[0];
    d4.d[1] = (simd_DOUBLE)x.c[1];
    d4.d[2] = (simd_DOUBLE)x.c[2];
    d4.d[3] = (simd_DOUBLE)x.c[3];
    d4 = simd_transpose_DOUBLE(d4);
    x.c[0] = (simd_COMPLEX)d4.d[0];
    x.c[1] = (simd_COMPLEX)d4.d[1];
    x.c[2] = (simd_COMPLEX)d4.d[2];
    x.c[3] = (simd_COMPLEX)d4.d[3];
    return x;
}
#define simd_transpose_COMPLEX(a) _ip_simd_transpose_COMPLEX(a)

static inline simd_DCOMPLEX_2 _ip_simd_transpose_DCOMPLEX(simd_DCOMPLEX_2 z)
{
    simd_DCOMPLEX a;
    a = _mm256_permute2f128_pd(z.dc[0], z.dc[1], 0x20);
    z.dc[1] = _mm256_permute2f128_pd(z.dc[0], z.dc[1], 0x31);
    z.dc[0] = a;
    return z;
}
#define simd_transpose_DCOMPLEX(z) _ip_simd_transpose_DCOMPLEX(z)


/*
 * simd_interleave - interleave the elements of two vectors
 * simd_deinterleave - inverse of simd_interleave.
 */

static inline simd_UBYTE_2 _ip_simd_interleave_UBYTE(simd_UBYTE x, simd_UBYTE y)
{
    simd_UBYTE a, b;
    simd_UBYTE_2 res;

    a = _mm256_unpacklo_epi8(x, y);
    b = _mm256_unpackhi_epi8(x, y);
    res.ub[0] = _mm256_permute2x128_si256(a, b, 0x20);
    res.ub[1] = _mm256_permute2x128_si256(a, b, 0x31);
    return res;
}
#define simd_interleave_UBYTE(a, b) _ip_simd_interleave_UBYTE(a, b)

static inline simd_BYTE_2 _ip_simd_interleave_BYTE(simd_BYTE x, simd_BYTE y)
{
    simd_BYTE a, b;
    simd_BYTE_2 res;

    a = _mm256_unpacklo_epi8(x, y);
    b = _mm256_unpackhi_epi8(x, y);
    res.b[0] = _mm256_permute2x128_si256(a, b, 0x20);
    res.b[1] = _mm256_permute2x128_si256(a, b, 0x31);
    return res;
}
#define simd_interleave_BYTE(a, b) _ip_simd_interleave_BYTE(a, b)

static inline simd_USHORT_2 _ip_simd_interleave_USHORT(simd_USHORT x, simd_USHORT y)
{
    simd_USHORT a, b;
    simd_USHORT_2 res;

    a = _mm256_unpacklo_epi16(x, y);
    b = _mm256_unpackhi_epi16(x, y);
    res.us[0] = _mm256_permute2x128_si256(a, b, 0x20);
    res.us[1] = _mm256_permute2x128_si256(a, b, 0x31);
    return res;
}
#define simd_interleave_USHORT(a, b) _ip_simd_interleave_USHORT(a, b)

static inline simd_SHORT_2 _ip_simd_interleave_SHORT(simd_SHORT x, simd_SHORT y)
{
    simd_SHORT a, b;
    simd_SHORT_2 res;

    a = _mm256_unpacklo_epi16(x, y);
    b = _mm256_unpackhi_epi16(x, y);
    res.s[0] = _mm256_permute2x128_si256(a, b, 0x20);
    res.s[1] = _mm256_permute2x128_si256(a, b, 0x31);
    return res;
}
#define simd_interleave_SHORT(a, b) _ip_simd_interleave_SHORT(a, b)

static inline simd_ULONG_2 _ip_simd_interleave_ULONG(simd_ULONG x, simd_ULONG y)
{
    simd_ULONG a, b;
    simd_ULONG_2 res;

    a = _mm256_unpacklo_epi32(x, y);
    b = _mm256_unpackhi_epi32(x, y);
    res.ul[0] = _mm256_permute2x128_si256(a, b, 0x20);
    res.ul[1] = _mm256_permute2x128_si256(a, b, 0x31);
    return res;
}
#define simd_interleave_ULONG(a, b) _ip_simd_interleave_ULONG(a, b)

static inline simd_LONG_2 _ip_simd_interleave_LONG(simd_LONG x, simd_LONG y)
{
    simd_LONG a, b;
    simd_LONG_2 res;

    a = _mm256_unpacklo_epi32(x, y);
    b = _mm256_unpackhi_epi32(x, y);
    res.l[0] = _mm256_permute2x128_si256(a, b, 0x20);
    res.l[1] = _mm256_permute2x128_si256(a, b, 0x31);
    return res;
}
#define simd_interleave_LONG(a, b) _ip_simd_interleave_LONG(a, b)

static inline simd_FLOAT_2 _ip_simd_interleave_FLOAT(simd_FLOAT x, simd_FLOAT y)
{
    simd_FLOAT a, b;
    simd_FLOAT_2 res;

    a = _mm256_unpacklo_ps(x, y);
    b = _mm256_unpackhi_ps(x, y);
    res.f[0] = _mm256_permute2f128_ps(a, b, 0x20);
    res.f[1] = _mm256_permute2f128_ps(a, b, 0x31);
    return res;
}
#define simd_interleave_FLOAT(a, b) _ip_simd_interleave_FLOAT(a, b)

static inline simd_DOUBLE_2 _ip_simd_interleave_DOUBLE(simd_DOUBLE x, simd_DOUBLE y)
{
    simd_DOUBLE a, b;
    simd_DOUBLE_2 res;

    a = _mm256_unpacklo_pd(x, y);
    b = _mm256_unpackhi_pd(x, y);
    res.d[0] = _mm256_permute2f128_pd(a, b, 0x20);
    res.d[1] = _mm256_permute2f128_pd(a, b, 0x31);
    return res;
}
#define simd_interleave_DOUBLE(a, b) _ip_simd_interleave_DOUBLE(a, b)


/*
 * Comparison of whole SIMD vector
 */

static inline int _ip_simd_vector_zero_UBYTE(simd_UBYTE x)
{
    return _mm256_testz_si256(x, x);
}

static inline int _ip_simd_vector_equal_UBYTE(simd_UBYTE x, simd_UBYTE y)
{
    return _ip_simd_vector_zero_UBYTE(_mm256_sub_epi8(x, y));
}

#define simd_vector_zero_UBYTE(x) _ip_simd_vector_zero_UBYTE(x)
#define simd_vector_equal_UBYTE(x, y) _ip_simd_vector_equal_UBYTE(x, y)


/*
 * simd_create_byte_mask
 *
 * Create a byte mask for masking out padding on last vector in image
 * row. If numbytes is N then the first N bytes in the returned vector have
 * all bits sets; subsequent bytes are zero.
 *
 * Also see simd_zero_right_TYPE() macros below which are used for similar
 * purposes.
 */

static inline simd_UBYTE _ip_simd_create_byte_mask(int numbytes)
{
    const simd_UBYTE vinc = _mm256_setr_epi8(0, 1, 2, 3, 4, 5, 6, 7,
					     8, 9, 10, 11, 12, 13, 14, 15,
					     16, 17, 18, 19, 20, 21, 22, 23,
					     24, 25, 26, 27, 28, 29, 30, 31);
    simd_UBYTE vnumb = simd_splat_UBYTE(numbytes);
    return _mm256_cmpgt_epi8(vnumb, vinc);
}
#define simd_create_byte_mask(x) _ip_simd_create_byte_mask(x)


/*
 * Conversion routines for converting one image type to another.
 *
 * simd_cnvrt_DTYPE_STYPE
 *   - convert source type STYPE to type DTYPE
 *
 * simd_cnvrt_clip_DTYPE_STYPE
 *   - convert source type STYPE to type DTYPE with clipping (saturation).
 *
 */

/*
 * Integer to integer conversions first.
 */

/* BYTE to SHORT, signed extended */
static inline simd_SHORT_2 _ip_simd_cnvrt_SHORT_BYTE(simd_BYTE x)
{
    simd_SHORT_2 res;
    res.s[0] = _mm256_cvtepi8_epi16(_mm256_castsi256_si128(x));
    res.s[1] = _mm256_cvtepi8_epi16(_mm256_castsi256_si128(avx_swap_128bit_banks(x)));
    return res;
}

#define simd_cnvrt_SHORT_BYTE(x) _ip_simd_cnvrt_SHORT_BYTE(x)

/* BYTE to SHORT, zero extended */

static inline simd_USHORT_2 _ip_simd_cnvrt_USHORT_UBYTE(simd_UBYTE x)
{
    simd_USHORT_2 res;
    res.us[0] = _mm256_cvtepu8_epi16(_mm256_castsi256_si128(x));
    res.us[1] = _mm256_cvtepu8_epi16(_mm256_castsi256_si128(avx_swap_128bit_banks(x)));
    return res;
}

#define simd_cnvrt_USHORT_UBYTE(x) _ip_simd_cnvrt_USHORT_UBYTE(x)

/* USHORT to UBYTE by truncation */
static inline simd_UBYTE _ip_simd_cnvrt_UBYTE_USHORT(simd_USHORT a, simd_USHORT b)
{
    const simd_USHORT mask = _mm256_set1_epi16(0x00ff);
    a = _mm256_and_si256(a, mask);
    b = _mm256_and_si256(b, mask);
    return avx_swap_inner_64bit_banks(_mm256_packus_epi16(a, b));
}

#define simd_cnvrt_UBYTE_USHORT(a, b) _ip_simd_cnvrt_UBYTE_USHORT(a, b)

/* BYTE to LONG, signed extended. */
static inline simd_LONG_4 _ip_simd_cnvrt_LONG_BYTE(simd_BYTE x)
{
    simd_LONG_4 res;
    simd_BYTE highx;
    res.l[0] = _mm256_cvtepi8_epi32(_mm256_castsi256_si128(x));
    res.l[1] = _mm256_cvtepi8_epi32(_mm256_castsi256_si128(_mm256_srli_si256(x, 8)));
    highx = avx_swap_128bit_banks(x);
    res.l[2] = _mm256_cvtepi8_epi32(_mm256_castsi256_si128(highx));
    res.l[3] = _mm256_cvtepi8_epi32(_mm256_castsi256_si128(_mm256_srli_si256(highx, 8)));
    return res;
}

#define simd_cnvrt_LONG_BYTE(x) _ip_simd_cnvrt_LONG_BYTE(x)

/* BYTE to LONG, zero extended. */
static inline simd_ULONG_4 _ip_simd_cnvrt_ULONG_UBYTE(simd_UBYTE x)
{
    simd_ULONG_4 res;
    simd_UBYTE highx;
    res.ul[0] = _mm256_cvtepu8_epi32(_mm256_castsi256_si128(x));
    res.ul[1] = _mm256_cvtepu8_epi32(_mm256_castsi256_si128(_mm256_srli_si256(x, 8)));
    highx = avx_swap_128bit_banks(x);
    res.ul[2] = _mm256_cvtepu8_epi32(_mm256_castsi256_si128(highx));
    res.ul[3] = _mm256_cvtepu8_epi32(_mm256_castsi256_si128(_mm256_srli_si256(highx, 8)));
    return res;
}

#define simd_cnvrt_ULONG_UBYTE(x) _ip_simd_cnvrt_ULONG_UBYTE(x)

/* LONG to BYTE by truncation */
static inline simd_UBYTE _ip_simd_cnvrt_UBYTE_ULONG(simd_ULONG a, simd_ULONG b, simd_ULONG c, simd_ULONG d)
{
    const simd_ULONG mask = _mm256_set1_epi32(0x00ff);
    simd_ULONG tmp;
    a = _mm256_and_si256(a, mask);
    b = _mm256_and_si256(b, mask);
    c = _mm256_and_si256(c, mask);
    d = _mm256_and_si256(d, mask);
    tmp = _mm256_packus_epi16(_mm256_packus_epi32(a, b),_mm256_packus_epi32(c, d));
    return _mm256_shuffle_epi32(avx_swap_inner_64bit_banks(tmp), 0xd8);
}

#define simd_cnvrt_UBYTE_ULONG(a, b, c, d) _ip_simd_cnvrt_UBYTE_ULONG(a, b, c, d)

/* SHORT to LONG by sign extension */
static inline simd_LONG_2 _ip_simd_cnvrt_LONG_SHORT(simd_SHORT x)
{
    simd_LONG_2 res;
    res.l[0] = _mm256_cvtepi16_epi32(_mm256_castsi256_si128(x));
    res.l[1] = _mm256_cvtepi16_epi32(_mm256_castsi256_si128(avx_swap_128bit_banks(x)));
    return res;
}

#define simd_cnvrt_LONG_SHORT(x) _ip_simd_cnvrt_LONG_SHORT(x)

/* USHORT to ULONG by zero extension. */
static inline simd_ULONG_2 _ip_simd_cnvrt_ULONG_USHORT(simd_USHORT x)
{
    simd_ULONG_2 res;
    res.ul[0] = _mm256_cvtepu16_epi32(_mm256_castsi256_si128(x));
    res.ul[1] = _mm256_cvtepu16_epi32(_mm256_castsi256_si128(avx_swap_128bit_banks(x)));
    return res;
}

#define simd_cnvrt_ULONG_USHORT(x) _ip_simd_cnvrt_ULONG_USHORT(x)

/* ULONG to USHORT by truncation */
static inline simd_USHORT _ip_simd_cnvrt_USHORT_ULONG(simd_ULONG a, simd_ULONG b)
{
    const simd_ULONG mask = _mm256_set1_epi32(0xffff);
    a = _mm256_and_si256(a, mask);
    b = _mm256_and_si256(b, mask);
    return avx_swap_inner_64bit_banks(_mm256_packus_epi32(a, b));
}

#define simd_cnvrt_USHORT_ULONG(a, b) _ip_simd_cnvrt_USHORT_ULONG(a, b)

/* ULONG64 to ULONG by truncation */
static inline simd_ULONG _ip_simd_cnvrt_ULONG_ULONG64(simd_ULONG64 a, simd_ULONG64 b)
{
    simd_ULONG64 res;

    res = avx_shuffle_epi32(a, b, 0x88);
    return avx_swap_inner_64bit_banks(res);
}
#define simd_cnvrt_ULONG_ULONG64(a, b) _ip_simd_cnvrt_ULONG_ULONG64(a, b)


/* Convert BYTE to UBYTE with clipping */
#define simd_cnvrt_clip_UBYTE_BYTE(a) _mm256_max_epi8(a, _mm256_setzero_si256())

/* Convert BYTE to USHORT with clipping */
#define simd_cnvrt_clip_USHORT_BYTE(a) simd_cnvrt_USHORT_UBYTE(_mm256_max_epi8(a, _mm256_setzero_si256()))

/* Convert BYTE to ULONG with clipping */
#define simd_cnvrt_clip_ULONG_BYTE(a) simd_cnvrt_ULONG_UBYTE(_mm256_max_epi8(a, _mm256_setzero_si256()))

/* Convert UBYTE to BYTE with clipping */
#define simd_cnvrt_clip_BYTE_UBYTE(a) _mm256_min_epu8(a, _mm256_set1_epi8(127))

/* Convert SHORT to BYTE with clipping */
#define simd_cnvrt_clip_BYTE_SHORT(a,b)  avx_swap_inner_64bit_banks(_mm256_packs_epi16(a, b))

/* Convert SHORT to UBYTE with clipping */
#define simd_cnvrt_clip_UBYTE_SHORT(a,b) avx_swap_inner_64bit_banks(_mm256_packus_epi16(a, b))

/* Convert SHORT to USHORT with clipping */
#define simd_cnvrt_clip_USHORT_SHORT(a) _mm256_max_epi16(a, _mm256_setzero_si256())

/* Convert SHORT to ULONG with clipping */
#define simd_cnvrt_clip_ULONG_SHORT(a) simd_cnvrt_ULONG_USHORT(_mm256_max_epi16(a, _mm256_setzero_si256()))

/* Convert USHORT to BYTE with clipping */
static inline simd_BYTE _ip_simd_cnvrt_clip_BYTE_USHORT(simd_USHORT a, simd_USHORT b)
{
    const simd_USHORT vzero = _mm256_setzero_si256();
    const simd_USHORT vmax = _mm256_set1_epi16(127);
    simd_USHORT sela, selb;

    sela = _mm256_cmpgt_epi16(vzero, a);
    a = v_select(sela, vmax, a);
    selb = _mm256_cmpgt_epi16(vzero, b);
    b = v_select(selb, vmax, b);
    return  avx_swap_inner_64bit_banks(_mm256_packs_epi16(a, b));
}

#define simd_cnvrt_clip_BYTE_USHORT(a,b)  _ip_simd_cnvrt_clip_BYTE_USHORT(a,b)

/* Convert USHORT to UBYTE with clipping */
static inline simd_UBYTE _ip_simd_cnvrt_clip_UBYTE_USHORT(simd_USHORT a, simd_USHORT b)
{
    const simd_USHORT vmax = _mm256_set1_epi16(255);
    a = _mm256_min_epu16(a, vmax);
    b = _mm256_min_epu16(b, vmax);
    return  avx_swap_inner_64bit_banks(_mm256_packus_epi16(a, b));
}

#define simd_cnvrt_clip_UBYTE_USHORT(a,b)  _ip_simd_cnvrt_clip_UBYTE_USHORT(a,b)

/* Convert USHORT to SHORT with clipping */
#define simd_cnvrt_clip_SHORT_USHORT(a) _mm256_min_epu16(a, _mm256_set1_epi16(INT16_MAX))

/* Convert LONG to BYTE with clipping */
static inline simd_BYTE _ip_simd_cnvrt_clip_BYTE_LONG(simd_LONG a, simd_LONG b, simd_LONG c, simd_LONG d)
{
    simd_LONG tmp;
    tmp = _mm256_packs_epi16(_mm256_packs_epi32(a, b),_mm256_packs_epi32(c, d));
    return _mm256_shuffle_epi32(avx_swap_inner_64bit_banks(tmp), 0xd8);
}

#define simd_cnvrt_clip_BYTE_LONG(a,b,c,d) _ip_simd_cnvrt_clip_BYTE_LONG(a,b,c,d)

/* Convert LONG to UBYTE with clipping */
static inline simd_UBYTE _ip_simd_cnvrt_clip_UBYTE_LONG(simd_LONG a, simd_LONG b, simd_LONG c, simd_LONG d)
{
    simd_LONG tmp;
    tmp = _mm256_packus_epi16(_mm256_packs_epi32(a, b),_mm256_packs_epi32(c, d));
    return _mm256_shuffle_epi32(avx_swap_inner_64bit_banks(tmp), 0xd8);
}
#define simd_cnvrt_clip_UBYTE_LONG(a,b,c,d) _ip_simd_cnvrt_clip_UBYTE_LONG(a,b,c,d)

/* Convert LONG to SHORT with clipping */

#define simd_cnvrt_clip_SHORT_LONG(a,b) avx_swap_inner_64bit_banks(_mm256_packs_epi32(a, b))

/* Convert LONG to USHORT with clipping */

#define simd_cnvrt_clip_USHORT_LONG(a,b) avx_swap_inner_64bit_banks(_mm256_packus_epi32(a, b))

/* Convert LONG to ULONG with clipping */
#define simd_cnvrt_clip_ULONG_LONG(a) _mm256_max_epi32(a, _mm256_setzero_si256())

/* Convert ULONG to BYTE with clipping */
static inline simd_BYTE _ip_simd_cnvrt_clip_BYTE_ULONG(simd_ULONG a, simd_ULONG b, simd_ULONG c, simd_ULONG d)
{
    const simd_ULONG vzero = _mm256_setzero_si256();
    const simd_ULONG vmax = _mm256_set1_epi16(127);
    simd_ULONG sela, selb;

    sela = _mm256_cmpgt_epi16(vzero, a);
    a = v_select(sela, vmax, a);
    selb = _mm256_cmpgt_epi16(vzero, b);
    b = v_select(selb, vmax, b);
    sela = _mm256_cmpgt_epi16(vzero, c);
    c = v_select(sela, vmax, c);
    selb = _mm256_cmpgt_epi16(vzero, d);
    d = v_select(selb, vmax, d);
    return  simd_cnvrt_clip_BYTE_LONG(a,b,c,d);
}

#define simd_cnvrt_clip_BYTE_ULONG(a,b,c,d)  _ip_simd_cnvrt_clip_BYTE_ULONG(a,b,c,d)

/* Convert ULONG to UBYTE with clipping */
static inline simd_UBYTE _ip_simd_cnvrt_clip_UBYTE_ULONG(simd_ULONG a, simd_ULONG b, simd_ULONG c, simd_ULONG d)
{
    const simd_ULONG vmax = _mm256_set1_epi32(255);
    simd_UBYTE tmp;

    a = _mm256_min_epu32(a, vmax);
    b = _mm256_min_epu32(b, vmax);
    c = _mm256_min_epu32(c, vmax);
    d = _mm256_min_epu32(d, vmax);
    tmp = _mm256_packus_epi16(_mm256_packus_epi32(a, b),_mm256_packus_epi32(c, d));
    return _mm256_shuffle_epi32(avx_swap_inner_64bit_banks(tmp), 0xd8);
}

#define simd_cnvrt_clip_UBYTE_ULONG(a,b,c,d)  _ip_simd_cnvrt_clip_UBYTE_ULONG(a,b,c,d)

/* Convert ULONG to SHORT with clipping */
static inline simd_SHORT _ip_simd_cnvrt_clip_SHORT_ULONG(simd_ULONG a, simd_ULONG b)
{
    const simd_ULONG vzero = _mm256_setzero_si256();
    const simd_ULONG vmax = _mm256_set1_epi32(INT16_MAX);
    simd_ULONG sela, selb;

    sela = _mm256_cmpgt_epi32(vzero, a);
    a = v_select(sela, vmax, a);
    selb = _mm256_cmpgt_epi32(vzero, b);
    b = v_select(selb, vmax, b);
    return  avx_swap_inner_64bit_banks(_mm256_packs_epi32(a, b));
}

#define simd_cnvrt_clip_SHORT_ULONG(a,b)  _ip_simd_cnvrt_clip_SHORT_ULONG(a,b)

/* Convert ULONG to USHORT with clipping */
static inline simd_USHORT _ip_simd_cnvrt_clip_USHORT_ULONG(simd_ULONG a, simd_ULONG b)
{
    const simd_ULONG vmax = _mm256_set1_epi32(UINT16_MAX);
    a = _mm256_min_epu32(a, vmax);
    b = _mm256_min_epu32(b, vmax);
    return  avx_swap_inner_64bit_banks(_mm256_packus_epi32(a, b));
}

#define simd_cnvrt_clip_USHORT_ULONG(a,b)  _ip_simd_cnvrt_clip_USHORT_ULONG(a,b)

/* Convert ULONG to LONG with clipping */
#define simd_cnvrt_clip_LONG_ULONG(a) _mm256_min_epu32(a, _mm256_set1_epi32(INT32_MAX))

/*
 * Integer to float and vice versa conversions.
 */

/* LONG/FLOAT conversions */
#define simd_cnvrt_FLOAT_LONG(x) _mm256_cvtepi32_ps(x)
#define simd_cnvrt_LONG_FLOAT(x) _mm256_cvttps_epi32(x)


/*
 * Round float to long.  The _mm256_cvtps_epi32() intrinsic is no use to us
 * because it does not implement conventional rounding.  Hence the
 * following:
 */
static inline simd_LONG _ip_simd_cnvrt_rnd_LONG_FLOAT(simd_FLOAT x)
{
    const simd_FLOAT signbit = (simd_FLOAT)simd_splat_ULONG(0x80000000U);
    simd_FLOAT half = simd_splat_FLOAT(0x1.fffffep-2); /* ohalf = nextafterf(0.5, 0) */
    simd_FLOAT sign = _mm256_and_ps(x, signbit);
    x = _mm256_add_ps(x, _mm256_or_ps(half, sign));    /* x + copysignf(ohalf, x) */
    return _mm256_cvttps_epi32(x);
}
#define simd_cnvrt_rnd_LONG_FLOAT(x) _ip_simd_cnvrt_rnd_LONG_FLOAT(x)


/*
 * The conversion of FLOAT to LONG with clipping is subtle, because the
 * value of INT32_MAX is not exactly representable in single precision
 * floating point.  If it is assigned to the float it becomes INT32_MAX+1
 * (the nearest available float value) which then becomes INT32_MIN when
 * converted to int32_t!  We therefore use a comparison and vselect() to
 * assign INT32_MAX to the result vector when clipping at the max value.
 */

static inline simd_LONG _ip_simd_cnvrt_clip_LONG_FLOAT(simd_FLOAT x)
{
    __m256i toobig;
    simd_LONG res;
    /* INT32_MIN is exactly representable in float so this is safe */
    x = _mm256_max_ps(x, _mm256_set1_ps(INT32_MIN));
    /* But INT32_MAX is not... */
    toobig = (__m256i)_mm256_cmp_ps(x, _mm256_set1_ps(INT32_MAX), _CMP_GE_OQ);
    res = simd_cnvrt_LONG_FLOAT(x);
    return v_select(toobig, _mm256_set1_epi32(INT32_MAX), res);
}
#define simd_cnvrt_clip_LONG_FLOAT(x) _ip_simd_cnvrt_clip_LONG_FLOAT(x)

static inline simd_LONG _ip_simd_cnvrt_rnd_clip_LONG_FLOAT(simd_FLOAT x)
{
    __m256i toobig;
    simd_LONG res;
    /* INT32_MIN is exactly representable in float so this is safe */
    x = _mm256_max_ps(x, _mm256_set1_ps(INT32_MIN));
    /* But INT32_MAX is not... */
    toobig = (__m256i)_mm256_cmp_ps(x, _mm256_set1_ps(INT32_MAX), _CMP_GE_OQ);
    res = simd_cnvrt_rnd_LONG_FLOAT(x);
    return v_select(toobig, _mm256_set1_epi32(INT32_MAX), res);
}
#define simd_cnvrt_rnd_clip_LONG_FLOAT(x) _ip_simd_cnvrt_rnd_clip_LONG_FLOAT(x)

/* LONG/DOUBLE conversions */
static inline simd_DOUBLE_2 _ip_simd_cnvrt_DOUBLE_LONG(simd_LONG x)
{
    simd_DOUBLE_2 res;

    res.d[0] = _mm256_cvtepi32_pd(_mm256_castsi256_si128(x));
    res.d[1] = _mm256_cvtepi32_pd(_mm256_castsi256_si128(avx_swap_128bit_banks(x)));
    return res;
}
#define simd_cnvrt_DOUBLE_LONG(x) _ip_simd_cnvrt_DOUBLE_LONG(x)

static inline simd_LONG _ip_simd_cnvrt_LONG_DOUBLE(simd_DOUBLE x, simd_DOUBLE y)
{
    __m128i left = _mm256_cvttpd_epi32(x);
    __m128i right = _mm256_cvttpd_epi32(y);
    return _mm256_inserti128_si256(_mm256_castsi128_si256(left), right, 1);
}
#define simd_cnvrt_LONG_DOUBLE(x,y) _ip_simd_cnvrt_LONG_DOUBLE(x,y)


/* Helper function: convert double to a half-length long with rounding. */
static inline __m128i avx2_rnd_LONG_DOUBLE(simd_DOUBLE x)
{
    const simd_DOUBLE signbit = (simd_DOUBLE)simd_splat_ULONG64(0x8000000000000000UL);
    simd_DOUBLE half = simd_splat_DOUBLE(0x1.fffffffffffffp-2); /* ohalf = nextafter(0.5, 0) */
    simd_DOUBLE sign = _mm256_and_pd(x, signbit);
    x = _mm256_add_pd(x, _mm256_or_pd(half, sign));		/* x + copysign(ohalf, x) */
    return _mm256_cvttpd_epi32(x);
}

static inline simd_LONG _ip_simd_cnvrt_rnd_LONG_DOUBLE(simd_DOUBLE x, simd_DOUBLE y)
{
    __m128i left = avx2_rnd_LONG_DOUBLE(x);
    __m128i right = avx2_rnd_LONG_DOUBLE(y);
    return _mm256_inserti128_si256(_mm256_castsi128_si256(left), right, 1);
}
#define simd_cnvrt_rnd_LONG_DOUBLE(x,y) _ip_simd_cnvrt_rnd_LONG_DOUBLE(x,y)

static inline simd_LONG _ip_simd_cnvrt_clip_LONG_DOUBLE(simd_DOUBLE x, simd_DOUBLE y)
{
    x = _mm256_min_pd(x, _mm256_set1_pd(INT32_MAX));
    x = _mm256_max_pd(x, _mm256_set1_pd(INT32_MIN));
    y = _mm256_min_pd(y, _mm256_set1_pd(INT32_MAX));
    y = _mm256_max_pd(y, _mm256_set1_pd(INT32_MIN));
    return simd_cnvrt_LONG_DOUBLE(x, y);
}
#define simd_cnvrt_clip_LONG_DOUBLE(x,y) _ip_simd_cnvrt_clip_LONG_DOUBLE(x,y)

static inline simd_LONG _ip_simd_cnvrt_rnd_clip_LONG_DOUBLE(simd_DOUBLE x, simd_DOUBLE y)
{
    x = _mm256_min_pd(x, _mm256_set1_pd(INT32_MAX));
    x = _mm256_max_pd(x, _mm256_set1_pd(INT32_MIN));
    y = _mm256_min_pd(y, _mm256_set1_pd(INT32_MAX));
    y = _mm256_max_pd(y, _mm256_set1_pd(INT32_MIN));
    return simd_cnvrt_rnd_LONG_DOUBLE(x, y);
}
#define simd_cnvrt_rnd_clip_LONG_DOUBLE(x,y) _ip_simd_cnvrt_rnd_clip_LONG_DOUBLE(x,y)

/* FLOAT/DOUBLE Conversions */

static inline simd_DOUBLE_2 _ip_simd_cnvrt_DOUBLE_FLOAT(simd_FLOAT x)
{
    simd_DOUBLE_2 res;

    res.d[0] = _mm256_cvtps_pd(_mm256_castps256_ps128(x));
    res.d[1] = _mm256_cvtps_pd(_mm256_castps256_ps128((__m256)avx_swap_128bit_banks((__m256i)x)));
    return res;
}
#define simd_cnvrt_DOUBLE_FLOAT(x) _ip_simd_cnvrt_DOUBLE_FLOAT(x)

static inline simd_FLOAT _ip_simd_cnvrt_FLOAT_DOUBLE(simd_DOUBLE x, simd_DOUBLE y)
{
    __m128 left = _mm256_cvtpd_ps(x);
    __m128 right = _mm256_cvtpd_ps(y);
    return _mm256_insertf128_ps(_mm256_castps128_ps256(left), right, 1);
}
#define simd_cnvrt_FLOAT_DOUBLE(x,y) _ip_simd_cnvrt_FLOAT_DOUBLE(x,y)

static inline simd_FLOAT _ip_simd_cnvrt_clip_FLOAT_DOUBLE(simd_DOUBLE x, simd_DOUBLE y)
{
    x = _mm256_max_pd(x, _mm256_set1_pd(-FLT_MAX));
    x = _mm256_min_pd(x, _mm256_set1_pd(FLT_MAX));
    y = _mm256_max_pd(y, _mm256_set1_pd(-FLT_MAX));
    y = _mm256_min_pd(y, _mm256_set1_pd(FLT_MAX));
    return simd_cnvrt_FLOAT_DOUBLE(x,y);
}
#define simd_cnvrt_clip_FLOAT_DOUBLE(x,y) _ip_simd_cnvrt_clip_FLOAT_DOUBLE(x,y)

/*
 * Back to ULONG to/from FLOAT/DOUBLE conversions.  Intel does not provide
 * CPU instructions for these conversion so we have to invent them from the
 * conversions between LONG and FLOAT/DOUBLE.
 *
 * Note that we do the manual re-wrapping of the ULONG to/from LONG at
 * double precision to avoid loss of significance in the wrapping. (Float
 * has 23 bits of precision thus is not able to faithfully replicate the
 * wrapping of negative values of LONG to the higher values of ULONG or vice
 * versa.)
 */

static inline simd_FLOAT _ip_simd_cnvrt_FLOAT_ULONG(simd_ULONG x)
{
    const simd_DOUBLE zero = simd_splat_DOUBLE(0.0);
    const simd_DOUBLE uint32_wrap = simd_splat_DOUBLE((double)(UINT32_MAX+1L));
    simd_DOUBLE_2 dx = simd_cnvrt_DOUBLE_LONG(x);
    simd_DOUBLE sel = _mm256_cmp_pd(zero, dx.d[0], _CMP_GT_OQ);
    dx.d[0] = v_selectd(sel, _mm256_add_pd(dx.d[0], uint32_wrap), dx.d[0]);
    sel = _mm256_cmp_pd(zero, dx.d[1], _CMP_GT_OQ);
    dx.d[1] = v_selectd(sel, _mm256_add_pd(dx.d[1], uint32_wrap), dx.d[1]);
    return simd_cnvrt_FLOAT_DOUBLE(dx.d[0], dx.d[1]);
}
#define simd_cnvrt_FLOAT_ULONG(x) _ip_simd_cnvrt_FLOAT_ULONG(x)

static inline simd_ULONG _ip_simd_cnvrt_ULONG_FLOAT(simd_FLOAT x)
{
    const simd_DOUBLE int32_max = simd_splat_DOUBLE((double)INT32_MAX);
    const simd_DOUBLE uint32_wrap = simd_splat_DOUBLE((double)(UINT32_MAX+1L));
    simd_DOUBLE_2 dx = simd_cnvrt_DOUBLE_FLOAT(x);
    simd_DOUBLE sel = _mm256_cmp_pd(int32_max, dx.d[0], _CMP_LT_OQ);
    dx.d[0] = v_selectd(sel, _mm256_sub_pd(dx.d[0], uint32_wrap), dx.d[0]);
    sel = _mm256_cmp_pd(int32_max, dx.d[1], _CMP_LT_OQ);
    dx.d[1] = v_selectd(sel, _mm256_sub_pd(dx.d[1], uint32_wrap), dx.d[1]);
    return simd_cnvrt_LONG_DOUBLE(dx.d[0], dx.d[1]);
}
#define simd_cnvrt_ULONG_FLOAT(x) _ip_simd_cnvrt_ULONG_FLOAT(x)

static inline simd_ULONG _ip_simd_cnvrt_clip_ULONG_FLOAT(simd_FLOAT x)
{
    const simd_DOUBLE int32_max = simd_splat_DOUBLE((double)INT32_MAX);
    const simd_DOUBLE uint32_wrap = simd_splat_DOUBLE((double)(UINT32_MAX+1L));
    const simd_DOUBLE uint32_max = simd_splat_DOUBLE((double)UINT32_MAX);
    const simd_DOUBLE vzero = simd_splat_DOUBLE(0.0);
    simd_DOUBLE_2 dx = simd_cnvrt_DOUBLE_FLOAT(x);
    dx.d[0] = _mm256_min_pd(dx.d[0], uint32_max);
    dx.d[0] = _mm256_max_pd(dx.d[0], vzero);
    dx.d[1] = _mm256_min_pd(dx.d[1], uint32_max);
    dx.d[1] = _mm256_max_pd(dx.d[1], vzero);
    simd_DOUBLE sel = _mm256_cmp_pd(int32_max, dx.d[0], _CMP_LT_OQ);
    dx.d[0] = v_selectd(sel, _mm256_sub_pd(dx.d[0], uint32_wrap), dx.d[0]);
    sel = _mm256_cmp_pd(int32_max, dx.d[1], _CMP_LT_OQ);
    dx.d[1] = v_selectd(sel, _mm256_sub_pd(dx.d[1], uint32_wrap), dx.d[1]);
    return simd_cnvrt_LONG_DOUBLE(dx.d[0], dx.d[1]);
}
#define simd_cnvrt_clip_ULONG_FLOAT(x) _ip_simd_cnvrt_clip_ULONG_FLOAT(x)

static inline simd_ULONG _ip_simd_cnvrt_rnd_ULONG_FLOAT(simd_FLOAT x)
{
    const simd_DOUBLE int32_max = simd_splat_DOUBLE((double)INT32_MAX);
    const simd_DOUBLE uint32_wrap = simd_splat_DOUBLE((double)(UINT32_MAX+1L));
    simd_DOUBLE_2 dx = simd_cnvrt_DOUBLE_FLOAT(x);
    simd_DOUBLE sel = _mm256_cmp_pd(int32_max, dx.d[0], _CMP_LT_OQ);
    dx.d[0] = v_selectd(sel, _mm256_sub_pd(dx.d[0], uint32_wrap), dx.d[0]);
    sel = _mm256_cmp_pd(int32_max, dx.d[1], _CMP_LT_OQ);
    dx.d[1] = v_selectd(sel, _mm256_sub_pd(dx.d[1], uint32_wrap), dx.d[1]);
    return simd_cnvrt_rnd_LONG_DOUBLE(dx.d[0], dx.d[1]);
}
#define simd_cnvrt_rnd_ULONG_FLOAT(x) _ip_simd_cnvrt_rnd_ULONG_FLOAT(x)

static inline simd_ULONG _ip_simd_cnvrt_rnd_clip_ULONG_FLOAT(simd_FLOAT x)
{
    const simd_DOUBLE int32_max = simd_splat_DOUBLE((double)INT32_MAX);
    const simd_DOUBLE uint32_wrap = simd_splat_DOUBLE((double)(UINT32_MAX+1L));
    const simd_DOUBLE uint32_max = simd_splat_DOUBLE((double)UINT32_MAX);
    const simd_DOUBLE vzero = simd_splat_DOUBLE(0.0);
    simd_DOUBLE_2 dx = simd_cnvrt_DOUBLE_FLOAT(x);
    dx.d[0] = _mm256_min_pd(dx.d[0], uint32_max);
    dx.d[0] = _mm256_max_pd(dx.d[0], vzero);
    dx.d[1] = _mm256_min_pd(dx.d[1], uint32_max);
    dx.d[1] = _mm256_max_pd(dx.d[1], vzero);
    simd_DOUBLE sel = _mm256_cmp_pd(int32_max, dx.d[0], _CMP_LT_OQ);
    dx.d[0] = v_selectd(sel, _mm256_sub_pd(dx.d[0], uint32_wrap), dx.d[0]);
    sel = _mm256_cmp_pd(int32_max, dx.d[1], _CMP_LT_OQ);
    dx.d[1] = v_selectd(sel, _mm256_sub_pd(dx.d[1], uint32_wrap), dx.d[1]);
    return simd_cnvrt_rnd_LONG_DOUBLE(dx.d[0], dx.d[1]);
}
#define simd_cnvrt_rnd_clip_ULONG_FLOAT(x) _ip_simd_cnvrt_rnd_clip_ULONG_FLOAT(x)

static inline simd_DOUBLE_2 _ip_simd_cnvrt_DOUBLE_ULONG(simd_ULONG x)
{
    const simd_DOUBLE zero = simd_splat_DOUBLE(0.0);
    const simd_DOUBLE uint32_wrap = simd_splat_DOUBLE((double)(UINT32_MAX+1L));
    simd_DOUBLE_2 dx = simd_cnvrt_DOUBLE_LONG(x);
    simd_DOUBLE sel = _mm256_cmp_pd(zero, dx.d[0], _CMP_GT_OQ);
    dx.d[0] = v_selectd(sel, _mm256_add_pd(dx.d[0], uint32_wrap), dx.d[0]);
    sel = _mm256_cmp_pd(zero, dx.d[1], _CMP_GT_OQ);
    dx.d[1] = v_selectd(sel, _mm256_add_pd(dx.d[1], uint32_wrap), dx.d[1]);
    return dx;
}
#define simd_cnvrt_DOUBLE_ULONG(x) _ip_simd_cnvrt_DOUBLE_ULONG(x)

static inline simd_ULONG _ip_simd_cnvrt_ULONG_DOUBLE(simd_DOUBLE x, simd_DOUBLE y)
{
    const simd_DOUBLE int32_maxd = simd_splat_DOUBLE((double)INT32_MAX+1.0);
    const simd_ULONG int32_maxul = simd_splat_ULONG((unsigned long)INT32_MAX+1UL);
    simd_ULONG res, sel;
    simd_DOUBLE selx, sely;
    selx = _mm256_cmp_pd(int32_maxd, x, _CMP_LT_OQ);
    x = v_selectd(selx, _mm256_sub_pd(x, int32_maxd), x);
    sely = _mm256_cmp_pd(int32_maxd, y, _CMP_LT_OQ);
    y = v_selectd(sely, _mm256_sub_pd(y, int32_maxd), y);
    res = simd_cnvrt_LONG_DOUBLE(x, y);
    sel = avx_swap_inner_64bit_banks(_mm256_packs_epi32((__m256i)selx, (__m256i)sely));
    return v_select(sel, _mm256_add_epi32(res, int32_maxul), res);
}
#define simd_cnvrt_ULONG_DOUBLE(x, y) _ip_simd_cnvrt_ULONG_DOUBLE(x, y)

static inline simd_ULONG _ip_simd_cnvrt_clip_ULONG_DOUBLE(simd_DOUBLE x, simd_DOUBLE y)
{
    const simd_DOUBLE int32_maxd = simd_splat_DOUBLE((double)INT32_MAX+1.0);
    const simd_ULONG int32_maxul = simd_splat_ULONG((unsigned long)INT32_MAX+1UL);
    const simd_DOUBLE uint32_max = simd_splat_DOUBLE((double)UINT32_MAX);
    const simd_DOUBLE vzero = simd_splat_DOUBLE(0.0);
    simd_ULONG res, sel;
    simd_DOUBLE selx, sely;
    x = _mm256_min_pd(x, uint32_max);
    x = _mm256_max_pd(x, vzero);
    y = _mm256_min_pd(y, uint32_max);
    y = _mm256_max_pd(y, vzero);
    selx = _mm256_cmp_pd(int32_maxd, x, _CMP_LT_OQ);
    x = v_selectd(selx, _mm256_sub_pd(x, int32_maxd), x);
    sely = _mm256_cmp_pd(int32_maxd, y, _CMP_LT_OQ);
    y = v_selectd(sely, _mm256_sub_pd(y, int32_maxd), y);
    res = simd_cnvrt_LONG_DOUBLE(x, y);
    sel = avx_swap_inner_64bit_banks(_mm256_packs_epi32((__m256i)selx, (__m256i)sely));
    return v_select(sel, _mm256_add_epi32(res, int32_maxul), res);
}
#define simd_cnvrt_clip_ULONG_DOUBLE(x, y) _ip_simd_cnvrt_clip_ULONG_DOUBLE(x, y)

static inline simd_ULONG _ip_simd_cnvrt_rnd_ULONG_DOUBLE(simd_DOUBLE x, simd_DOUBLE y)
{
    const simd_DOUBLE int32_maxd = simd_splat_DOUBLE((double)INT32_MAX+1.0);
    const simd_ULONG int32_maxul = simd_splat_ULONG((unsigned long)INT32_MAX+1UL);
    simd_ULONG res, sel;
    simd_DOUBLE selx, sely;
    selx = _mm256_cmp_pd(int32_maxd, x, _CMP_LT_OQ);
    x = v_selectd(selx, _mm256_sub_pd(x, int32_maxd), x);
    sely = _mm256_cmp_pd(int32_maxd, y, _CMP_LT_OQ);
    y = v_selectd(sely, _mm256_sub_pd(y, int32_maxd), y);
    res = simd_cnvrt_rnd_LONG_DOUBLE(x, y);
    sel = avx_swap_inner_64bit_banks(_mm256_packs_epi32((__m256i)selx, (__m256i)sely));
    return v_select(sel, _mm256_add_epi32(res, int32_maxul), res);
}
#define simd_cnvrt_rnd_ULONG_DOUBLE(x, y) _ip_simd_cnvrt_rnd_ULONG_DOUBLE(x, y)

static inline simd_ULONG _ip_simd_cnvrt_rnd_clip_ULONG_DOUBLE(simd_DOUBLE x, simd_DOUBLE y)
{
    const simd_DOUBLE int32_maxd = simd_splat_DOUBLE((double)INT32_MAX+1.0);
    const simd_ULONG int32_maxul = simd_splat_ULONG((unsigned long)INT32_MAX+1UL);
    const simd_DOUBLE uint32_max = simd_splat_DOUBLE((double)UINT32_MAX);
    const simd_DOUBLE vzero = simd_splat_DOUBLE(0.0);
    simd_ULONG res, sel;
    simd_DOUBLE selx, sely;
    x = _mm256_min_pd(x, uint32_max);
    x = _mm256_max_pd(x, vzero);
    y = _mm256_min_pd(y, uint32_max);
    y = _mm256_max_pd(y, vzero);
    selx = _mm256_cmp_pd(int32_maxd, x, _CMP_LT_OQ);
    x = v_selectd(selx, _mm256_sub_pd(x, int32_maxd), x);
    sely = _mm256_cmp_pd(int32_maxd, y, _CMP_LT_OQ);
    y = v_selectd(sely, _mm256_sub_pd(y, int32_maxd), y);
    res = simd_cnvrt_rnd_LONG_DOUBLE(x, y);
    sel = avx_swap_inner_64bit_banks(_mm256_packs_epi32((__m256i)selx, (__m256i)sely));
    return v_select(sel, _mm256_add_epi32(res, int32_maxul), res);
}
#define simd_cnvrt_rnd_clip_ULONG_DOUBLE(x, y) _ip_simd_cnvrt_rnd_clip_ULONG_DOUBLE(x, y)


/*
 * Conversions to/from BINARY
 */

static inline simd_UBYTE _ip_simd_cnvrt_BINARY_UBYTE(simd_UBYTE x)
{
    const simd_UBYTE vfalse = simd_splat_UBYTE(0);
    const simd_UBYTE vtrue = simd_splat_UBYTE(255);
    return _mm256_andnot_si256(_mm256_cmpeq_epi8(x, vfalse), vtrue);
}
#define simd_cnvrt_BINARY_UBYTE(x) _ip_simd_cnvrt_BINARY_UBYTE(x)

static inline simd_UBYTE _ip_simd_cnvrt_BINARY_USHORT(simd_USHORT a, simd_USHORT b)
{
    const simd_USHORT vfalse = simd_splat_USHORT(0);
    const simd_USHORT vtrue = simd_splat_USHORT(0xffff);
    a = _mm256_andnot_si256(_mm256_cmpeq_epi16(a, vfalse), vtrue);
    b = _mm256_andnot_si256(_mm256_cmpeq_epi16(b, vfalse), vtrue);
    return avx_swap_inner_64bit_banks(_mm256_packs_epi16(a, b));
}
#define simd_cnvrt_BINARY_USHORT(x, y) _ip_simd_cnvrt_BINARY_USHORT(x, y)

static inline simd_UBYTE _ip_simd_cnvrt_BINARY_ULONG(simd_ULONG a, simd_ULONG b,
						     simd_ULONG c, simd_ULONG d)
{
    const simd_ULONG vfalse = simd_splat_ULONG(0);
    const simd_ULONG vtrue = simd_splat_ULONG(0xffffffff);
    simd_ULONG tmp;
    a = _mm256_andnot_si256(_mm256_cmpeq_epi32(a, vfalse), vtrue);
    b = _mm256_andnot_si256(_mm256_cmpeq_epi32(b, vfalse), vtrue);
    c = _mm256_andnot_si256(_mm256_cmpeq_epi32(c, vfalse), vtrue);
    d = _mm256_andnot_si256(_mm256_cmpeq_epi32(d, vfalse), vtrue);
    tmp = _mm256_packs_epi16(_mm256_packs_epi32(a, b),_mm256_packs_epi32(c, d));
    return _mm256_shuffle_epi32(avx_swap_inner_64bit_banks(tmp), 0xd8);
}
#define simd_cnvrt_BINARY_ULONG(x, y, u, v) _ip_simd_cnvrt_BINARY_ULONG(x, y, u, v)

static inline simd_UBYTE _ip_simd_cnvrt_BINARY_FLOAT(simd_FLOAT a, simd_FLOAT b,
						     simd_FLOAT c, simd_FLOAT d)
{
    const simd_FLOAT vfalse = simd_splat_FLOAT(0.0);
    const simd_ULONG vtrue = simd_splat_ULONG(0xffffffff);
    simd_ULONG x, y, u, v, tmp;
    x = _mm256_andnot_si256((__m256i)_mm256_cmp_ps(a, vfalse, _CMP_EQ_OQ), vtrue);
    y = _mm256_andnot_si256((__m256i)_mm256_cmp_ps(b, vfalse, _CMP_EQ_OQ), vtrue);
    u = _mm256_andnot_si256((__m256i)_mm256_cmp_ps(c, vfalse, _CMP_EQ_OQ), vtrue);
    v = _mm256_andnot_si256((__m256i)_mm256_cmp_ps(d, vfalse, _CMP_EQ_OQ), vtrue);
    tmp = _mm256_packs_epi16(_mm256_packs_epi32(x, y),_mm256_packs_epi32(u, v));
    return _mm256_shuffle_epi32(avx_swap_inner_64bit_banks(tmp), 0xd8);
}
#define simd_cnvrt_BINARY_FLOAT(x, y, u, v) _ip_simd_cnvrt_BINARY_FLOAT(x, y, u, v)

static inline simd_UBYTE _ip_simd_cnvrt_UBYTE_BINARY(simd_UBYTE x, int tval)
{
    const simd_UBYTE vfalse = simd_splat_UBYTE(0);
    simd_UBYTE vtrue = simd_splat_UBYTE(tval);
    return v_select(x, vtrue, vfalse);
}
#define simd_cnvrt_UBYTE_BINARY(x, tv) _ip_simd_cnvrt_UBYTE_BINARY(x, tv)

static inline simd_USHORT_2 _ip_simd_cnvrt_USHORT_BINARY(simd_UBYTE x, int tval)
{
    const simd_UBYTE vfalse = simd_splat_UBYTE(0);
    simd_UBYTE vtrue = simd_splat_UBYTE(tval);
    x = v_select(x, vtrue, vfalse);
    return simd_cnvrt_USHORT_UBYTE(x);
}
#define simd_cnvrt_USHORT_BINARY(x, tv) _ip_simd_cnvrt_USHORT_BINARY(x, tv)

static inline simd_ULONG_4 _ip_simd_cnvrt_ULONG_BINARY(simd_UBYTE x, int tval)
{
    const simd_UBYTE vfalse = simd_splat_UBYTE(0);
    simd_UBYTE vtrue = simd_splat_UBYTE(tval);
    x = v_select(x, vtrue, vfalse);
    return simd_cnvrt_ULONG_UBYTE(x);
}
#define simd_cnvrt_ULONG_BINARY(x, tv) _ip_simd_cnvrt_ULONG_BINARY(x, tv)

/*
 * Colour conversions
 */

/* Repack 3 RGB vectors into 4 RGBA vectors */
static inline simd_UBYTE_4 ip_simd_cnvrt_RGBA_RGB(simd_UBYTE a, simd_UBYTE b, simd_UBYTE c)
{
    simd_UBYTE_4 y;
    simd_UBYTE t1, t2, t3;
    const simd_ULONG shuffle_a1 = _mm256_set_epi32(0, 5, 4, 3, 0, 2, 1, 0);
    const simd_ULONG shuffle_a2 = _mm256_set_epi32(0, 0, 0, 0, 0, 0, 7, 6);
    const simd_ULONG shuffle_b1 = _mm256_set_epi32(0, 3, 2, 1, 0, 0, 0, 0);
    const simd_ULONG shuffle_b2 = _mm256_set_epi32(0, 0, 0, 7, 0, 6, 5, 4);
    const simd_ULONG shuffle_c1 = _mm256_set_epi32(0, 1, 0, 0, 0, 0, 0, 0);
    const simd_ULONG shuffle_c2 = _mm256_set_epi32(0, 7, 6, 5, 0, 4, 3, 2);
    const simd_UBYTE bytesel = _mm256_set_epi8(-1, 11, 10,  9, -1,  8,  7,  6,
					       -1,  5,  4,  3, -1,  2,  1,  0,
					       -1, 11, 10,  9, -1,  8,  7,  6,
					       -1,  5,  4,  3, -1,  2,  1,  0);
    const simd_UBYTE set_a_255 = _mm256_set_epi8(0xff, 0, 0, 0, 0xff, 0, 0, 0,
						 0xff, 0, 0, 0, 0xff, 0, 0, 0,
						 0xff, 0, 0, 0, 0xff, 0, 0, 0,
						 0xff, 0, 0, 0, 0xff, 0, 0, 0);
    t1 = _mm256_permutevar8x32_epi32(a, shuffle_a1);
    y.ub[0] = _mm256_or_si256(_mm256_shuffle_epi8(t1, bytesel), set_a_255);
    t1 = _mm256_permutevar8x32_epi32(a, shuffle_a2);
    t2 = _mm256_permutevar8x32_epi32(b, shuffle_b1);
    t3 = _mm256_blend_epi32(t1, t2, 0xfc);
    y.ub[1] = _mm256_or_si256(_mm256_shuffle_epi8(t3, bytesel), set_a_255);
    t1 = _mm256_permutevar8x32_epi32(b, shuffle_b2);
    t2 = _mm256_permutevar8x32_epi32(c, shuffle_c1);
    t3 = _mm256_blend_epi32(t1, t2, 0xe0);
    y.ub[2] = _mm256_or_si256(_mm256_shuffle_epi8(t3, bytesel), set_a_255);
    t1 = _mm256_permutevar8x32_epi32(c, shuffle_c2);
    y.ub[3] = _mm256_or_si256(_mm256_shuffle_epi8(t1, bytesel), set_a_255);

    return y;
}
#define simd_cnvrt_RGBA_RGB(a,b,c) ip_simd_cnvrt_RGBA_RGB(a,b,c)


/* Repack 4 RGBA vectors into 3 RGB vectors */
static inline simd_UBYTE_3 ip_simd_cnvrt_RGB_RGBA(simd_UBYTE a, simd_UBYTE b,
						  simd_UBYTE c, simd_UBYTE d)
{
    simd_UBYTE_3 y;
    const simd_UBYTE bytesel = _mm256_set_epi8(-1, -1, -1, -1, 14, 13, 12, 10,
					        9,  8,  6,  5,  4,  2,  1,  0,
					       -1, -1, -1, -1, 14, 13, 12, 10,
					        9,  8,  6,  5,  4,  2,  1,  0);
    const simd_ULONG shuffle_a = _mm256_set_epi32(7, 7, 6, 5, 4, 2, 1, 0);
    const simd_ULONG shuffle_b = _mm256_set_epi32(1, 0, 7, 7, 6, 5, 4, 2);
    const simd_ULONG shuffle_c = _mm256_set_epi32(4, 2, 1, 0, 7, 7, 6, 5);
    const simd_ULONG shuffle_d = _mm256_set_epi32(6, 5, 4, 2, 1, 0, 7, 7);


    a = _mm256_permutevar8x32_epi32(_mm256_shuffle_epi8(a, bytesel), shuffle_a);
    b = _mm256_permutevar8x32_epi32(_mm256_shuffle_epi8(b, bytesel), shuffle_b);
    c = _mm256_permutevar8x32_epi32(_mm256_shuffle_epi8(c, bytesel), shuffle_c);
    d = _mm256_permutevar8x32_epi32(_mm256_shuffle_epi8(d, bytesel), shuffle_d);

    y.ub[0] = _mm256_blend_epi32(a, b, 0xc0);
    y.ub[1] = _mm256_blend_epi32(b, c, 0xf0);
    y.ub[2] = _mm256_blend_epi32(c, d, 0xfc);

    return y;
}
#define simd_cnvrt_RGB_RGBA(a,b,c,d) ip_simd_cnvrt_RGB_RGBA(a,b,c,d)

static inline simd_UBYTE ip_simd_cnvrt_RGB_RGB16(simd_USHORT x, simd_USHORT y)
{
    return simd_cnvrt_UBYTE_USHORT(_mm256_srli_epi16(x, 8),
				   _mm256_srli_epi16(y, 8));
}
#define simd_cnvrt_RGB_RGB16(x,y) ip_simd_cnvrt_RGB_RGB16(x,y)

static inline simd_USHORT_2 ip_simd_cnvrt_RGB16_RGB(simd_UBYTE x)
{
    simd_USHORT_2 r16 = simd_cnvrt_USHORT_UBYTE(x);
    r16.us[0] = _mm256_or_si256(r16.us[0], _mm256_slli_epi16(r16.us[0], 8));
    r16.us[1] = _mm256_or_si256(r16.us[1], _mm256_slli_epi16(r16.us[1], 8));
    return r16;
}
#define simd_cnvrt_RGB16_RGB(x) ip_simd_cnvrt_RGB16_RGB(x)



/* Logical operators on the SIMD vectors */
#define simd_or_UBYTE(a,b) _mm256_or_si256(a,b)
#define simd_or_BYTE(a,b) _mm256_or_si256(a,b)
#define simd_or_USHORT(a,b) _mm256_or_si256(a,b)
#define simd_or_SHORT(a,b) _mm256_or_si256(a,b)
#define simd_or_ULONG(a,b) _mm256_or_si256(a,b)
#define simd_or_LONG(a,b) _mm256_or_si256(a,b)

#define simd_and_UBYTE(a,b) _mm256_and_si256(a,b)
#define simd_and_BYTE(a,b) _mm256_and_si256(a,b)
#define simd_and_USHORT(a,b) _mm256_and_si256(a,b)
#define simd_and_SHORT(a,b) _mm256_and_si256(a,b)
#define simd_and_ULONG(a,b) _mm256_and_si256(a,b)
#define simd_and_LONG(a,b) _mm256_and_si256(a,b)

#define simd_xor_UBYTE(a,b) _mm256_xor_si256(a,b)
#define simd_xor_BYTE(a,b) _mm256_xor_si256(a,b)
#define simd_xor_USHORT(a,b) _mm256_xor_si256(a,b)
#define simd_xor_SHORT(a,b) _mm256_xor_si256(a,b)
#define simd_xor_ULONG(a,b) _mm256_xor_si256(a,b)
#define simd_xor_LONG(a,b) _mm256_xor_si256(a,b)

#define simd_not_UBYTE(a) _mm256_andnot_si256(a, simd_splat_UBYTE(-1))
#define simd_not_BYTE(a) _mm256_andnot_si256(a, simd_splat_BYTE(-1))
#define simd_not_USHORT(a) _mm256_andnot_si256(a, simd_splat_USHORT(-1))
#define simd_not_SHORT(a) _mm256_andnot_si256(a, simd_splat_SHORT(-1))
#define simd_not_ULONG(a) _mm256_andnot_si256(a, simd_splat_ULONG(-1))
#define simd_not_LONG(a) _mm256_andnot_si256(a, simd_splat_LONG(-1))

/*
 * Shifts
 */

/* Per element shifts */
#define simd_shlc_SHORT(x,s) _mm256_sll_epi16(x, _mm256_castsi256_si128(s))
#define simd_shlc_USHORT(x,s) _mm256_sll_epi16(x, _mm256_castsi256_si128(s))
#define simd_shlc_LONG(x,s) _mm256_sll_epi32(x, _mm256_castsi256_si128(s))
#define simd_shlc_ULONG(x,s) _mm256_sll_epi32(x, _mm256_castsi256_si128(s))

#define simd_shl_imm_SHORT(x,c) _mm256_slli_epi16(x,c)
#define simd_shl_imm_USHORT(x,c) _mm256_slli_epi16(x,c)
#define simd_shl_imm_LONG(x,c) _mm256_slli_epi32(x,c)
#define simd_shl_imm_ULONG(x,c) _mm256_slli_epi32(x,c)

#define simd_shr_imm_SHORT(x,c) _mm256_srli_epi16(x,c)
#define simd_shr_imm_USHORT(x,c) _mm256_srli_epi16(x,c)
#define simd_shr_imm_LONG(x,c) _mm256_srli_epi32(x,c)
#define simd_shr_imm_ULONG(x,c) _mm256_srli_epi32(x,c)

#define simd_shra_imm_SHORT(x,c) _mm256_srai_epi16(x,c)
#define simd_shra_imm_USHORT(x,c) _mm256_srai_epi16(x,c)
#define simd_shra_imm_LONG(x,c) _mm256_srai_epi32(x,c)
#define simd_shra_imm_ULONG(x,c) _mm256_srai_epi32(x,c)

static inline simd_UBYTE _ip_simd_ror_RGB(simd_UBYTE a)
{
    const __m256i b = _mm256_set_epi8(14, 13, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2,
				      14, 13, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2);
    return _mm256_shuffle_epi8(a, b);
}
static inline simd_USHORT _ip_simd_ror_RGB16(simd_USHORT a)
{
    const __m256i b = _mm256_set_epi8(11, 10, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2,
				      11, 10, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2);
    return _mm256_shuffle_epi8(a, b);
}

#define simd_ror_RGB(x) _ip_simd_ror_RGB(x)
#define simd_ror_RGB16(x) _ip_simd_ror_RGB16(x)

/* Basic arithmetic operators */
/* Some still to do in multiplication and division */

/* addition */
#define simd_add_UBYTE(d,s)  _mm256_add_epi8(d,s)
#define simd_add_BYTE(d,s)   _mm256_add_epi8(d,s)
#define simd_add_USHORT(d,s) _mm256_add_epi16(d,s)
#define simd_add_SHORT(d,s)  _mm256_add_epi16(d,s)
#define simd_add_ULONG(d,s)  _mm256_add_epi32(d,s)
#define simd_add_LONG(d,s)   _mm256_add_epi32(d,s)
#define simd_add_FLOAT(d,s)  _mm256_add_ps(d,s)
#define simd_add_DOUBLE(d,s) _mm256_add_pd(d,s)
#define simd_add_COMPLEX(d,s) _mm256_add_ps(d,s)
#define simd_add_DCOMPLEX(d,s) _mm256_add_pd(d,s)

#define simd_add_ULONG64(d,s)  _mm256_add_epi64(d,s)
#define simd_add_LONG64(d,s)  _mm256_add_epi64(d,s)

/* addition with saturation */
#define simd_add_clipped_UBYTE(d,s) _mm256_adds_epu8(d,s)
#define simd_add_clipped_BYTE(d,s) _mm256_adds_epi8(d,s)
#define simd_add_clipped_USHORT(d,s) _mm256_adds_epu16(d,s)
#define simd_add_clipped_SHORT(d,s) _mm256_adds_epi16(d,s)

/* subtraction */
#define simd_sub_UBYTE(d,s)  _mm256_sub_epi8(d,s)
#define simd_sub_BYTE(d,s)   _mm256_sub_epi8(d,s)
#define simd_sub_USHORT(d,s) _mm256_sub_epi16(d,s)
#define simd_sub_SHORT(d,s)  _mm256_sub_epi16(d,s)
#define simd_sub_ULONG(d,s)  _mm256_sub_epi32(d,s)
#define simd_sub_LONG(d,s)   _mm256_sub_epi32(d,s)
#define simd_sub_FLOAT(d,s)  _mm256_sub_ps(d,s)
#define simd_sub_DOUBLE(d,s) _mm256_sub_pd(d,s)
#define simd_sub_COMPLEX(d,s)  _mm256_sub_ps(d,s)
#define simd_sub_DCOMPLEX(d,s) _mm256_sub_pd(d,s)

#define simd_sub_ULONG64(d,s)  _mm256_sub_epi64(d,s)
#define simd_sub_LONG64(d,s)  _mm256_sub_epi64(d,s)

/* subtraction with saturation */
#define simd_sub_clipped_UBYTE(d,s)  _mm256_subs_epu8(d,s)
#define simd_sub_clipped_BYTE(d,s)   _mm256_subs_epi8(d,s)
#define simd_sub_clipped_USHORT(d,s) _mm256_subs_epu16(d,s)
#define simd_sub_clipped_SHORT(d,s)  _mm256_subs_epi16(d,s)

/* multication */
static inline simd_UBYTE _ip_simd_mul_UBYTE(simd_UBYTE d, simd_UBYTE s)
{
    simd_USHORT_2 td, ts;
    td = simd_cnvrt_USHORT_UBYTE(d);
    ts = simd_cnvrt_USHORT_UBYTE(s);
    td.us[0] = _mm256_mullo_epi16(td.us[0], ts.us[0]);
    td.us[1] = _mm256_mullo_epi16(td.us[1], ts.us[1]);
    return simd_cnvrt_UBYTE_USHORT(td.us[0], td.us[1]);
}
#define simd_mul_BYTE(d,s) _ip_simd_mul_UBYTE(d,s)
#define simd_mul_UBYTE(d,s) _ip_simd_mul_UBYTE(d,s)
#define simd_mul_SHORT(d,s)  _mm256_mullo_epi16(d,s)
#define simd_mul_USHORT(d,s) _mm256_mullo_epi16(d,s)
#define simd_mul_LONG(d,s) _mm256_mullo_epi32(d,s)
#define simd_mul_ULONG(d,s) _mm256_mullo_epi32(d,s)
#define simd_mul_FLOAT(d,s)  _mm256_mul_ps(d,s)
#define simd_mul_DOUBLE(d,s) _mm256_mul_pd(d,s)

static inline __m256 __ip_multiply_COMPLEX(__m256 d, __m256 s)
{
    __m256 t1, t2, t3;		    /* N.B. d = [d2i d2r d1i d1r], s likewise */

    t1 = _mm256_moveldup_ps(d);	    /* [d2r d2r d1r d1r] */
    t2 = _mm256_movehdup_ps(d);	    /* [d2i d2i d1i d1i] */
    t3 = _mm256_shuffle_ps(s, s, 0xb1); /* [s2r s2i s1r s1i] */
    t1 = _mm256_mul_ps(t1, s);	    /* [d2r*s2i d2r*s2r d1r*s1i d1r*s1r] */
    t2 = _mm256_mul_ps(t2, t3);	    /* [d2i*s2r d2i*s2i d1i*s1r d1i*s1i] */
    return _mm256_addsub_ps(t1, t2); /* add/sub to get complex multiplication */
}
#define simd_mul_COMPLEX(d,s) __ip_multiply_COMPLEX(d,s)

static inline __m256d __ip_multiply_DCOMPLEX(__m256d d, __m256d s)
{
    __m256d t1, t2, t3;		    /* N.B. d = [d2i d2r d1i d1r], s likewise */

    t1 = _mm256_movedup_pd(d);	    /* [d2r d2r d1r d1r] */
    t2 = _mm256_shuffle_pd(d, d, 0xf); /* [d2i d2i d1i d1i] */
    t3 = _mm256_shuffle_pd(s, s, 0x5); /* [s2r s2i s1r s1i] */
    t1 = _mm256_mul_pd(t1, s);	    /* [dr*si dr*sr] */
    t2 = _mm256_mul_pd(t2, t3);	    /* [di*sr di*si] */
    return _mm256_addsub_pd(t1, t2); /* add/sub to get complex multiplication */
}
#define simd_mul_DCOMPLEX(d,s) __ip_multiply_DCOMPLEX(d,s)


/* multiplication with saturation */
static inline simd_UBYTE _ip_simd_mul_clipped_UBYTE(simd_UBYTE d, simd_UBYTE s)
{
    simd_USHORT_2 td, ts;
    const simd_USHORT ubyte_max = simd_splat_USHORT(0xff);
    td = simd_cnvrt_USHORT_UBYTE(d);
    ts = simd_cnvrt_USHORT_UBYTE(s);
    td.us[0] = _mm256_mullo_epi16(td.us[0], ts.us[0]);
    td.us[1] = _mm256_mullo_epi16(td.us[1], ts.us[1]);
    td.us[0] = _mm256_min_epu16(td.us[0], ubyte_max);
    td.us[1] = _mm256_min_epu16(td.us[1], ubyte_max);
    return simd_cnvrt_UBYTE_USHORT(td.us[0], td.us[1]);
}
#define simd_mul_clipped_UBYTE(d,s) _ip_simd_mul_clipped_UBYTE(d,s)

static inline simd_BYTE _ip_simd_mul_clipped_BYTE(simd_BYTE d, simd_BYTE s)
{
    simd_SHORT_2 td, ts;
    const simd_SHORT byte_max = simd_splat_SHORT(127);
    const simd_SHORT byte_min = simd_splat_SHORT(-128);
    td = simd_cnvrt_SHORT_BYTE(d);
    ts = simd_cnvrt_SHORT_BYTE(s);
    td.s[0] = _mm256_mullo_epi16(td.s[0], ts.s[0]);
    td.s[1] = _mm256_mullo_epi16(td.s[1], ts.s[1]);
    td.s[0] = _mm256_min_epi16(td.s[0], byte_max);
    td.s[1] = _mm256_min_epi16(td.s[1], byte_max);
    td.s[0] = _mm256_max_epi16(td.s[0], byte_min);
    td.s[1] = _mm256_max_epi16(td.s[1], byte_min);
    return simd_cnvrt_UBYTE_USHORT(td.s[0], td.s[1]);
}
#define simd_mul_clipped_BYTE(d,s) _ip_simd_mul_clipped_BYTE(d,s)

static inline simd_USHORT _ip_simd_mul_clipped_USHORT(simd_USHORT d, simd_USHORT s)
{
    simd_SHORT hi = _mm256_mulhi_epu16(d, s);
    simd_SHORT lo = _mm256_mullo_epi16(d, s);
    simd_SHORT good = _mm256_cmpeq_epi16(hi, _mm256_setzero_si256());
    return v_select(good, lo, simd_splat_USHORT(UINT16_MAX));
}
#define simd_mul_clipped_USHORT(d,s) _ip_simd_mul_clipped_USHORT(d,s)

static inline simd_SHORT _ip_simd_mul_clipped_SHORT(simd_SHORT d, simd_SHORT s)
{
    simd_SHORT under;
    simd_SHORT hi = _mm256_mulhi_epi16(d, s);
    simd_SHORT lo = _mm256_mullo_epi16(d, s);
    simd_SHORT hi_neg = _mm256_cmpgt_epi16(_mm256_setzero_si256(), hi);
    simd_SHORT hi_zero = _mm256_cmpeq_epi16(_mm256_setzero_si256(), hi);
    simd_SHORT hi_minus1 = _mm256_cmpeq_epi16(simd_splat_SHORT(-1), hi);
    simd_SHORT over = _mm256_cmpgt_epi16(hi, _mm256_setzero_si256());
    simd_SHORT lo_neg = _mm256_cmpgt_epi16(_mm256_setzero_si256(), lo);
    over = _mm256_or_si256(over, _mm256_and_si256(hi_zero, lo_neg));
    under = _mm256_or_si256(_mm256_andnot_si256(hi_minus1, hi_neg), _mm256_andnot_si256(lo_neg, hi_minus1));
    lo = v_select(under, simd_splat_SHORT(INT16_MIN), lo);
    lo = v_select(over, simd_splat_SHORT(INT16_MAX), lo);
    return lo;
 }
#define simd_mul_clipped_SHORT(d,s) _ip_simd_mul_clipped_SHORT(d,s)

/*
 * simd_mulhi
 *
 * Return upper N-bits of N-bit by N-bit multiplication.
 */

#define simd_mulhi_USHORT(a,b) _mm256_mulhi_epu16(a,b)
#define simd_mulhi_SHORT(a,b) _mm256_mulhi_epi16(a,b)

static inline simd_ULONG _ip_simd_mulhi_ULONG(simd_ULONG a, simd_ULONG b)
{
    simd_ULONG x1, x2;

    x1 = _mm256_mul_epu32(a, b);
    x2 = _mm256_mul_epu32(_mm256_srli_si256(a, 4), _mm256_srli_si256(b, 4));
    return _mm256_blend_epi32(_mm256_srli_si256(x1, 4), x2, 0xaa);
}

static inline simd_LONG _ip_simd_mulhi_LONG(simd_LONG a, simd_LONG b)
{
    simd_LONG x1, x2;

    x1 = _mm256_mul_epi32(a, b);
    x2 = _mm256_mul_epi32(_mm256_srli_si256(a, 4), _mm256_srli_si256(b, 4));
    return _mm256_blend_epi32(_mm256_srli_si256(x1, 4), x2, 0xaa);
}

#define simd_mulhi_ULONG(a,b) _ip_simd_mulhi_ULONG(a,b)
#define simd_mulhi_LONG(a,b) _ip_simd_mulhi_LONG(a,b)

/*
 * simd_fmaddhi
 *
 * Integer fused multiply-add return hi-bits.
 */

static inline simd_ULONG _ip_simd_fmaddhi_ULONG(simd_ULONG a, simd_ULONG b, simd_ULONG c)
{
    simd_ULONG64 x1, x2;
    const simd_ULONG mask  = _mm256_set_epi32(0, -1, 0, -1, 0, -1, 0, -1);

    x1 = _mm256_mul_epu32(a, b);
    x2 = _mm256_mul_epu32(_mm256_srli_si256(a, 4), _mm256_srli_si256(b, 4));
    x1 = simd_add_ULONG64(x1, simd_and_ULONG(c, mask));
    x2 = simd_add_ULONG64(x2, simd_and_ULONG(_mm256_srli_si256(c, 4), mask));
    return _mm256_blend_epi32(_mm256_srli_si256(x1, 4), x2, 0xaa);
}
#define simd_fmaddhi_ULONG(a, b, c) _ip_simd_fmaddhi_ULONG(a, b, c)


/*
 * simd_imulfN
 *
 * Fixed-point integer multiplication where N=32 for 32/32 fixed-point and
 * N=16 for 16/16 fixed-point.
 *
 * Calculates round(a*c) for integer multiplicand a and fixed-point
 * multiplier c = cw + cf, with cw = floor(c) and cf = 2^N(c - cw). Intended
 * for use when c >= 0.5.  If c < 0.5 consider using the idivfN functions
 * below as they better preserve precision of the fraction.
 */

static inline simd_USHORT _ip_simd_imulf16_USHORT(simd_USHORT a, simd_USHORT cw, simd_USHORT cf)
{
    const simd_ULONG half = _mm256_set1_epi32(1<<15);
    simd_USHORT sh, sl;
    simd_ULONG x1, x2;

    /* Multiply a by the fraction cf and round to nearest integer */
    sh = _mm256_mulhi_epu16(a, cf);
    sl = _mm256_mullo_epi16(a, cf);
    x1 = _mm256_unpacklo_epi16(sl, sh);
    x2 = _mm256_unpackhi_epi16(sl, sh);
    x1 = simd_add_ULONG(x1, half);
    x2 = simd_add_ULONG(x2, half);
    cf =  _mm256_packus_epi32(_mm256_srli_epi32(x1, 16), _mm256_srli_epi32(x2, 16));
    /* Multiply a by the whole number part cw */
    cw = _mm256_mullo_epi16(a, cw);
    /* Add two parts together for final result */
    return simd_add_USHORT(cf, cw);
}
#define simd_imulf16_USHORT(a, b, c) _ip_simd_imulf16_USHORT(a, b, c)

static inline simd_SHORT _ip_simd_imulf16_SHORT(simd_SHORT a, simd_USHORT cw, simd_USHORT cf)
{
    const simd_SHORT zero = _mm256_set1_epi16(0);
    simd_LONG rounder = _mm256_set1_epi16(1<<15);
    simd_LONG x1, x2, rh, rl, negc, sign;
    simd_SHORT res;

    /*
     * See comment below in _ip_simd_imulf32_LONG() as to why sign is
     * calculated in this manner.
     */
    sign = simd_xor_SHORT(_mm256_cmpgt_epi16(zero, cw), _mm256_cmpgt_epi16(zero, a));
    /* rounder = (result < 0) ? (half-1) : half */
    rounder = simd_add_SHORT(rounder, sign);

    /* Low 16-bits of 32-bit unsigned a * cw */
    cw = _mm256_mullo_epi16(a, cw);

    /* signed correction to multiply:  negc = (a < 0) ? -cf : 0 */
    negc = simd_and_SHORT(simd_sub_SHORT(zero, cf), _mm256_cmpgt_epi16(zero, a));

    /* Compute 32-bit result of unsigned a * cf */
    rh = _mm256_mulhi_epu16(a, cf);
    rl = _mm256_mullo_epi16(a, cf);
    x1 = _mm256_unpacklo_epi16(rl, rh);
    x2 = _mm256_unpackhi_epi16(rl, rh);

    /* Unpack rounder into some format as x1/x2 */
    rl = _mm256_unpacklo_epi16(rounder, negc);
    rh = _mm256_unpackhi_epi16(rounder, negc);

    /* Add in rounder and correction to turn unsigned mul to signed */
    x1 = simd_add_LONG(x1, rl);
    x2 = simd_add_LONG(x2, rh);

    /* Pick off the top 16-bits of a*cf and re-pack */
    cf = _mm256_packus_epi32(_mm256_srli_epi32(x1, 16), _mm256_srli_epi32(x2, 16));

    /* And back into main result */
    res = simd_add_SHORT(cf, cw);

    return res;
}
#define simd_imulf16_SHORT(a, b, c) _ip_simd_imulf16_SHORT(a, b, c)


static inline simd_ULONG _ip_simd_imulf32_ULONG(simd_ULONG a, simd_ULONG cw, simd_ULONG cf)
{
    const simd_ULONG64 half = _mm256_set1_epi64x(1L<<31);
    simd_ULONG64 x1, x2;

    /* Multiply a by the fraction cf and round to nearest integer */
    x1 = _mm256_mul_epu32(a, cf);
    x2 = _mm256_mul_epu32(_mm256_srli_si256(a, 4), _mm256_srli_si256(cf, 4));
    x1 = simd_add_ULONG64(x1, half);
    x2 = simd_add_ULONG64(x2, half);
    cf = _mm256_blend_epi32(_mm256_srli_si256(x1, 4), x2, 0xaa);
    /* Multiply a by the whole number part cw */
    cw = _mm256_mullo_epi32(a, cw);
    /* Add two parts together for final result */
   return simd_add_ULONG(cf, cw);
}
#define simd_imulf32_ULONG(a, b, c) _ip_simd_imulf32_ULONG(a, b, c)

static inline simd_LONG _ip_simd_imulf32_LONG(simd_LONG a, simd_ULONG cw, simd_ULONG cf)
{
    const simd_LONG64 zero = _mm256_set1_epi64x(0);
    simd_LONG64 rounder = _mm256_set1_epi32(1<<31);
    simd_LONG64 x1, x2, rh, rl, negc, sign;
    simd_LONG res;

    /*
     * The rounder depends on the sign of the result.  It is tempting to use
     * cw calculated with the mullo below but cw can be zero which looses
     * the sign, hence this way immediately below to get the result sign,
     * and it's better because it gets the sign result quicker than the
     * mullo below so calculating the rounder etc., can happen earlier
     * giving an overall speedup.
     */
    sign = simd_xor_LONG(_mm256_cmpgt_epi32(zero, cw), _mm256_cmpgt_epi32(zero, a));
    /* rounder = (result < 0) ? (half-1) : half */
    rounder = simd_add_LONG(rounder, sign);

    /* Low 32-bits of 64-bit unsigned a * cw */
    cw = _mm256_mullo_epi32(a, cw);

    /* signed correction to multiply:  negc = (a < 0) ? -cf : 0 */
    negc = simd_and_LONG(simd_sub_LONG(zero, cf), _mm256_cmpgt_epi32(zero, a));

    /* Compute 64-bit result of unsigned a * cf */
    x1 = _mm256_mul_epu32(a, cf);
    x2 = _mm256_mul_epu32(_mm256_srli_si256(a,4), _mm256_srli_si256(cf, 4));

    /* Repack rounder/negc as (negc<<32 | rounder) */
    rl = _mm256_blend_epi32(_mm256_slli_si256(negc, 4), rounder, 0x55);
    rh = _mm256_blend_epi32(negc, _mm256_srli_si256(rounder, 4), 0x55);

    /* Add in rounder and correction to turn unsigned mul to signed */
    x1 = simd_add_LONG64(x1, rl);
    x2 = simd_add_LONG64(x2, rh);

    /* Unpack x1/x2 */
    cf = _mm256_blend_epi32(_mm256_srli_si256(x1, 4), x2, 0xaa);

    /* And back into main result */
    res = simd_add_LONG(cf, cw);

    return res;
}
#define simd_imulf32_LONG(a, b, c) _ip_simd_imulf32_LONG(a, b, c)


/* division */
#define simd_div_FLOAT(d,s)  _mm256_div_ps(d,s)
#define simd_div_DOUBLE(d,s) _mm256_div_pd(d,s)


/*
 * Integer division by a constant (i.e. reused) divisor
 *
 * We provide simd_rcpc functions to calculate a vector integer reciprocal
 * of a scalar divisor, and simd_idiv functions to calculate the integer
 * division using the reciprocal from the simd_rcpc functions.
 *
 * The IP library only expects these at (U)SHORT and (U)LONG precision.
 */

static inline struct simd_rcp_USHORT simd_rcpc_USHORT(unsigned int d)
{
    struct ip_uint16_rcp_n16 r = ip_urcp16_n16(d);
    struct simd_rcp_USHORT rcp;
    rcp.m = simd_splat_USHORT(r.m);
    rcp.b = simd_splat_USHORT(r.shift_1);
    rcp.s = _mm256_set1_epi64x((int64_t)r.shift);
    return rcp;
}

static inline struct simd_rcp_SHORT simd_rcpc_SHORT(int d)
{
    struct ip_int16_rcp r = ip_ircp16(d);
    struct simd_rcp_SHORT rcp;
    rcp.m = simd_splat_SHORT(r.m);
    rcp.b = simd_splat_SHORT(r.dsign);
    rcp.s = _mm256_set1_epi64x((int64_t)r.shift);
    return rcp;
}

static inline struct simd_rcp_ULONG simd_rcpc_ULONG(unsigned int d)
{
    struct ip_uint32_rcp_n33 r = ip_urcp32_n33(d);
    struct simd_rcp_ULONG rcp;
    rcp.m = simd_splat_ULONG(r.m);
    rcp.b = simd_splat_ULONG(r.b);
    rcp.s = _mm256_set1_epi64x((int64_t)r.shift);
    return rcp;
}

static inline struct simd_rcp_LONG simd_rcpc_LONG(int d)
{
    struct ip_int32_rcp r = ip_ircp32(d);
    struct simd_rcp_LONG rcp;
    rcp.m = simd_splat_LONG(r.m);
    rcp.b = simd_splat_LONG(r.dsign);
    rcp.s = _mm256_set1_epi64x((int64_t)r.shift);
    return rcp;
}

static inline simd_USHORT _ip_simd_idiv_USHORT(simd_USHORT n,
					       struct simd_rcp_USHORT r)
{
    /*
     * Calculate the integer division by multiplication by reciprocal.
     * Note that this implementation exploits an optimisation that gives
     * incorrect results for a divisor of 1.
     */
    simd_USHORT x1, t;
    t = simd_mulhi_USHORT(n, r.m);
    x1 = _mm256_srli_epi16(simd_sub_USHORT(n, t), 1);
    return _mm256_srl_epi16(simd_add_USHORT(t, x1), _mm256_castsi256_si128(r.s));
}
#define simd_idiv_USHORT(n, r) _ip_simd_idiv_USHORT(n, r)

static inline simd_SHORT _ip_simd_idiv_SHORT(simd_SHORT n,
					   struct simd_rcp_SHORT r)
{
    /* Calculate the integer division by multiplication by reciprocal */
    simd_SHORT q0;
    q0 = simd_add_SHORT(n, simd_mulhi_SHORT(n, r.m));
    q0 = _mm256_sra_epi16(q0, _mm256_castsi256_si128(r.s));
    q0 = simd_sub_SHORT(q0,  _mm256_srai_epi16(n, 15));
    return simd_sub_SHORT(simd_xor_SHORT(q0, r.b), r.b);
}
#define simd_idiv_SHORT(n, r) _ip_simd_idiv_SHORT(n, r)


static inline simd_ULONG _ip_simd_idiv_ULONG(simd_ULONG n,
					     struct simd_rcp_ULONG r)
{

    /* Calculate the integer division by multiplication by reciprocal */
    return _mm256_srl_epi32(simd_fmaddhi_ULONG(n, r.m, r.b), _mm256_castsi256_si128(r.s));
}
#define simd_idiv_ULONG(n, r) _ip_simd_idiv_ULONG(n, r)

static inline simd_LONG _ip_simd_idiv_LONG(simd_LONG n,
					   struct simd_rcp_LONG r)
{
    /* Calculate the integer division by multiplication by reciprocal */
    simd_LONG q0;
    q0 = simd_add_LONG(n, simd_mulhi_LONG(n, r.m));
    q0 = _mm256_sra_epi32(q0, _mm256_castsi256_si128(r.s));
    q0 = simd_sub_LONG(q0,  _mm256_srai_epi32(n, 31));
    return simd_sub_LONG(simd_xor_LONG(q0, r.b), r.b);
}
#define simd_idiv_LONG(n, r) _ip_simd_idiv_LONG(n, r)


/*
 * idivfN
 *
 * Integer division with conventional rounding.  These are implemented as a
 * multiply with the reciprocal.  These functions, equivalently, can be
 * thought of as a multiple by c for c < 0.5 such that cf = 2^(N+l)*c where
 * l is the shift required to shift c so that c's first 1 bit is in the MSB
 * of cf (not counting the sign bit), thus we always have an N-bit (or N-1
 * bit if signed) fraction in cf.
 */

static inline simd_USHORT
_ip_simd_idivf16_USHORT(simd_USHORT a, simd_USHORT cf, simd_USHORT shift)
{
    const simd_USHORT zero = simd_splat_USHORT(0);
    const simd_ULONG half = _mm256_set1_epi32(1<<15);
    simd_USHORT mh, ml, sh, sl;
    simd_ULONG x1, x2;

    mh = _mm256_mulhi_epu16(a, cf);
    ml = _mm256_mullo_epi16(a, cf);
    x1 = _mm256_unpacklo_epi16(ml, mh);
    x2 = _mm256_unpackhi_epi16(ml, mh);
    sl = _mm256_unpacklo_epi16(shift, zero);
    sh = _mm256_unpackhi_epi16(shift, zero);
    x1 = _mm256_srlv_epi32(simd_add_ULONG(x1, _mm256_sllv_epi32(half, sl)), sl);
    x2 = _mm256_srlv_epi32(simd_add_ULONG(x2, _mm256_sllv_epi32(half, sh)), sh);
    return _mm256_packus_epi32(_mm256_srli_epi32(x1, 16), _mm256_srli_epi32(x2, 16));
}
#define simd_idivf16_USHORT(a, b, s) _ip_simd_idivf16_USHORT(a, b, s)

static inline simd_ULONG
_ip_simd_idivf32_ULONG(simd_ULONG a, simd_ULONG cf, simd_ULONG shift)
{
    simd_ULONG64 x1, x2, sl, sh;
    const simd_ULONG half = _mm256_set1_epi64x(1L<<31);
    const simd_ULONG mask = _mm256_set_epi32(0, -1, 0, -1, 0, -1, 0, -1);

    x1 = _mm256_mul_epu32(a, cf);
    x2 = _mm256_mul_epu32(_mm256_srli_si256(a, 4), _mm256_srli_si256(cf, 4));
    sl = simd_and_ULONG(shift, mask);
    sh = _mm256_srli_epi64(shift, 32);
    x1 = simd_add_ULONG64(x1, _mm256_sllv_epi64(half, sl));
    x2 = simd_add_ULONG64(x2, _mm256_sllv_epi64(half, sh));
    x1 = _mm256_srlv_epi64(x1, sl);
    x2 = _mm256_srlv_epi64(x2, sh);
    return _mm256_blend_epi32(_mm256_srli_si256(x1, 4), x2, 0xaa);
}
#define simd_idivf32_ULONG(a, b, s) _ip_simd_idivf32_ULONG(a, b, s)



static inline simd_SHORT _ip_simd_idivf16_SHORT(simd_SHORT a,
						simd_SHORT cf, simd_USHORT shift)
{
    const simd_SHORT zero = _mm256_set1_epi16(0);
    simd_LONG rounder = _mm256_set1_epi32(1<<15);
    simd_LONG x1, x2, rh, rl, sh, sl, signl, signh;
    simd_SHORT res;

    /* Compute 32-bit result of signed a * cf */
    rh = _mm256_mulhi_epi16(a, cf);
    rl = _mm256_mullo_epi16(a, cf);
    x1 = _mm256_unpacklo_epi16(rl, rh);
    x2 = _mm256_unpackhi_epi16(rl, rh);

    /* Unpack shift to 32-bit domain */
    sl = _mm256_unpacklo_epi16(shift, zero);
    sh = _mm256_unpackhi_epi16(shift, zero);

    /* Get sign of a * cf */
    signl = _mm256_cmpgt_epi32(zero, x1);
    signh = _mm256_cmpgt_epi32(zero, x2);

    /* Let half = (2^15)<<shift, then rounder = (cf < 0) ? (half-1) : half */
    rl = simd_add_LONG(_mm256_sllv_epi32(rounder, sl), signl);
    rh = simd_add_LONG(_mm256_sllv_epi32(rounder, sh), signh);
    x1 = simd_add_LONG(x1, rl);
    x2 = simd_add_LONG(x2, rh);

    /* Shift back to get result */
    x1 = _mm256_srav_epi32(x1, sl);
    x2 = _mm256_srav_epi32(x2, sh);

    /* Pick off the top 16-bits of a*cf and re-pack */
    res = _mm256_packus_epi32(_mm256_srli_epi32(x1, 16), _mm256_srli_epi32(x2, 16));

    return res;
}
#define simd_idivf16_SHORT(a, b, s) _ip_simd_idivf16_SHORT(a, b, s)


static inline simd_LONG _ip_simd_idivf32_LONG(simd_LONG a,
                                              simd_ULONG cf, simd_ULONG shift)
{
    const simd_LONG64 zero = _mm256_set1_epi64x(0);
    const simd_LONG mask = _mm256_set_epi32(0, -1, 0, -1, 0, -1, 0, -1);
    simd_LONG64 rounder = _mm256_set1_epi64x(1LL<<31);
    simd_LONG64 x1, x2, rh, rl, sh, sl;
    simd_LONG res;

    /* Compute 64-bit result a * cf */
    x1 = _mm256_mul_epi32(a, cf);
    x2 = _mm256_mul_epi32(_mm256_srli_si256(a,4), _mm256_srli_si256(cf, 4));

    sl = simd_and_ULONG(shift, mask);
    sh = _mm256_srli_epi64(shift, 32);

    /* rounder = (cw < 0) ? (half-1) : half */
    rl = simd_add_LONG64(_mm256_sllv_epi64(rounder, sl), _mm256_cmpgt_epi64(zero, x1));
    rh = simd_add_LONG64(_mm256_sllv_epi64(rounder, sh), _mm256_cmpgt_epi64(zero, x2));
    x1 = simd_add_LONG64(x1, rl);
    x2 = simd_add_LONG64(x2, rh);

    /* Unpack x1/x2 */
    res = _mm256_blend_epi32(_mm256_srli_si256(x1, 4), x2, 0xaa);
    res =_mm256_srav_epi32(res, shift);

    return res;
}
#define simd_idivf32_LONG(a, b, s) _ip_simd_idivf32_LONG(a, b, s)


/*
 * simd_mean - take the mean of two vectors with rounding
 */

static inline simd_BYTE _ip_simd_mean_BYTE(simd_BYTE a, simd_BYTE b)
{
    simd_SHORT_2 sa, sb;
    simd_SHORT sel;
    const simd_SHORT zero = _mm256_setzero_si256();
    const simd_SHORT one = simd_splat_SHORT(1);
    sa = simd_cnvrt_SHORT_BYTE(a);
    sb = simd_cnvrt_SHORT_BYTE(b);
    sa.s[0] = _mm256_add_epi16(sa.s[0], sb.s[0]);
    sa.s[1] = _mm256_add_epi16(sa.s[1], sb.s[1]);
    sel = _mm256_cmpgt_epi16(zero, sa.s[0]);
    sa.s[0] = v_select(sel, sa.s[0], simd_add_SHORT(sa.s[0], one));
    sel = _mm256_cmpgt_epi16(zero, sa.s[1]);
    sa.s[1] = v_select(sel, sa.s[1], simd_add_SHORT(sa.s[1], one));
    sa.s[0] = _mm256_srai_epi16(sa.s[0], 1);
    sa.s[1] = _mm256_srai_epi16(sa.s[1], 1);
    return simd_cnvrt_UBYTE_USHORT(sa.s[0], sa.s[1]);
}
#define simd_mean_BYTE(a,b) _ip_simd_mean_BYTE(a, b)
#define simd_mean_UBYTE(a,b) _mm256_avg_epu8(a, b)

static inline simd_SHORT _ip_simd_mean_SHORT(simd_SHORT a, simd_SHORT b)
{
    simd_LONG_2 sa, sb;
    simd_LONG sel;
    const simd_LONG zero = _mm256_setzero_si256();
    const simd_LONG one = simd_splat_LONG(1);
    sa = simd_cnvrt_LONG_SHORT(a);
    sb = simd_cnvrt_LONG_SHORT(b);
    sa.l[0] = _mm256_add_epi32(sa.l[0], sb.l[0]);
    sa.l[1] = _mm256_add_epi32(sa.l[1], sb.l[1]);
    sel = _mm256_cmpgt_epi32(zero, sa.l[0]);
    sa.l[0] = v_select(sel, sa.l[0], simd_add_LONG(sa.l[0], one));
    sel = _mm256_cmpgt_epi32(zero, sa.l[1]);
    sa.l[1] = v_select(sel, sa.l[1], simd_add_LONG(sa.l[1], one));
    sa.l[0] = _mm256_srai_epi32(sa.l[0], 1);
    sa.l[1] = _mm256_srai_epi32(sa.l[1], 1);
    return simd_cnvrt_USHORT_ULONG(sa.l[0], sa.l[1]);
}
#define simd_mean_SHORT(a,b) _ip_simd_mean_SHORT(a, b)
#define simd_mean_USHORT(a,b) _mm256_avg_epu16(a, b)

static inline simd_LONG _ip_simd_mean_LONG(simd_LONG a, simd_LONG b)
{
    simd_LONG sel;
    const simd_LONG zero = _mm256_setzero_si256();
    const simd_LONG one = simd_splat_LONG(1);
    a = _mm256_add_epi32(a, b);
    sel = _mm256_cmpgt_epi32(zero, a);
    a = v_select(sel, a, simd_add_LONG(a, one));
    return _mm256_srai_epi32(a, 1);
}
#define simd_mean_LONG(a,b) _ip_simd_mean_LONG(a,b)

static inline simd_ULONG _ip_simd_mean_ULONG(simd_ULONG a, simd_ULONG b)
{
    a = _mm256_add_epi32(a, b);
    a = _mm256_add_epi32(a, simd_splat_ULONG(1));
    return _mm256_srli_epi32(a, 1);
}
#define simd_mean_ULONG(a,b) _ip_simd_mean_ULONG(a,b)

#define simd_mean_FLOAT(a,b) _mm256_mul_ps(_mm256_add_ps(a,b), simd_splat_FLOAT(0.5F))
#define simd_mean_DOUBLE(a,b) _mm256_mul_pd(_mm256_add_pd(a,b), simd_splat_DOUBLE(0.5))


/* Elementwise Min/Max */
#define simd_min_BYTE(d, s) _mm256_min_epi8(d, s)
#define simd_min_UBYTE(d, s) _mm256_min_epu8(d, s)
#define simd_min_SHORT(d, s) _mm256_min_epi16(d, s)
#define simd_min_USHORT(d, s) _mm256_min_epu16(d, s)
#define simd_min_LONG(d, s) _mm256_min_epi32(d, s)
#define simd_min_ULONG(d, s) _mm256_min_epu32(d, s)
#define simd_min_FLOAT(d,s) _mm256_min_ps(d, s)
#define simd_min_DOUBLE(d,s) _mm256_min_pd(d, s)

#define simd_max_BYTE(d, s) _mm256_max_epi8(d, s)
#define simd_max_UBYTE(d, s) _mm256_max_epu8(d, s)
#define simd_max_SHORT(d, s) _mm256_max_epi16(d, s)
#define simd_max_USHORT(d, s) _mm256_max_epu16(d, s)
#define simd_max_LONG(d, s) _mm256_max_epi32(d, s)
#define simd_max_ULONG(d, s) _mm256_max_epu32(d, s)
#define simd_max_FLOAT(d,s) _mm256_max_ps(d, s)
#define simd_max_DOUBLE(d,s) _mm256_max_pd(d, s)


/*
 * simd_cmpgt
 * simd_cmpeq
 *
 * Comparison operators.
 */

static inline simd_UBYTE _ip_simd_cmpgt_UBYTE(simd_UBYTE a, simd_UBYTE b)
{
    const simd_UBYTE bias = simd_splat_UBYTE(0x80);
    return _mm256_cmpgt_epi8(simd_sub_UBYTE(a, bias), simd_sub_UBYTE(b, bias));
}

static inline simd_USHORT _ip_simd_cmpgt_USHORT(simd_USHORT a, simd_USHORT b)
{
    const simd_USHORT bias = simd_splat_USHORT(0x8000);
    return _mm256_cmpgt_epi16(simd_sub_USHORT(a, bias), simd_sub_USHORT(b, bias));
}

static inline simd_ULONG _ip_simd_cmpgt_ULONG(simd_ULONG a, simd_ULONG b)
{
    const simd_ULONG bias = simd_splat_ULONG(0x80000000);
    return _mm256_cmpgt_epi32(simd_sub_ULONG(a, bias), simd_sub_ULONG(b, bias));
}

#define simd_cmpgt_UBYTE(a, b) _ip_simd_cmpgt_UBYTE(a, b)
#define simd_cmpgt_BYTE(a, b) _mm256_cmpgt_epi8(a, b)
#define simd_cmpgt_USHORT(a, b) _ip_simd_cmpgt_USHORT(a, b)
#define simd_cmpgt_SHORT(a, b) _mm256_cmpgt_epi16(a, b)
#define simd_cmpgt_ULONG(a, b) _ip_simd_cmpgt_ULONG(a, b)
#define simd_cmpgt_LONG(a, b) _mm256_cmpgt_epi32(a, b)
#define simd_cmpgt_FLOAT(a, b) _mm256_cmp_ps(a, b, _CMP_GT_OQ)
#define simd_cmpgt_DOUBLE(a, b) _mm256_cmp_pd(a, b, _CMP_GT_OQ)

#define simd_cmplt_UBYTE(a, b) simd_cmpgt_UBYTE(b, a)
#define simd_cmplt_BYTE(a, b) simd_cmpgt_BYTE(b, a)
#define simd_cmplt_USHORT(a, b) simd_cmpgt_USHORT(b, a)
#define simd_cmplt_SHORT(a, b) simd_cmpgt_SHORT(b, a)
#define simd_cmplt_ULONG(a, b) simd_cmpgt_ULONG(b, a)
#define simd_cmplt_LONG(a, b) simd_cmpgt_LONG(b, a)
#define simd_cmplt_FLOAT(a, b) _mm256_cmp_ps(a, b, _CMP_LT_OQ)
#define simd_cmplt_DOUBLE(a, b) _mm256_cmp_pd(a, b, _CMP_LT_OQ)

#define simd_cmpeq_UBYTE(a, b) _mm256_cmpeq_epi8(a, b)
#define simd_cmpeq_BYTE(a, b) _mm256_cmpeq_epi8(a, b)
#define simd_cmpeq_USHORT(a, b) _mm256_cmpeq_epi16(a, b)
#define simd_cmpeq_SHORT(a, b) _mm256_cmpeq_epi16(a, b)
#define simd_cmpeq_ULONG(a, b) _mm256_cmpeq_epi32(a, b)
#define simd_cmpeq_LONG(a, b) _mm256_cmpeq_epi32(a, b)
#define simd_cmpeq_FLOAT(a, b) _mm256_cmp_ps(a, b, _CMP_EQ_OQ)
#define simd_cmpeq_DOUBLE(a, b) _mm256_cmp_pd(a, b, _CMP_EQ_OQ)


/*
 * simd_select
 *
 * Bitwise Selection operator
 */

#define simd_select_UBYTE(s, a, b) v_select(s, a, b)
#define simd_select_BYTE(s, a, b) v_select(s, a, b)
#define simd_select_USHORT(s, a, b) v_select(s, a, b)
#define simd_select_SHORT(s, a, b) v_select(s, a, b)
#define simd_select_ULONG(s, a, b) v_select(s, a, b)
#define simd_select_LONG(s, a, b) v_select(s, a, b)
#define simd_select_FLOAT(s, a, b) v_selectf(s, a, b)
#define simd_select_DOUBLE(s, a, b) v_selectd(s, a, b)



/*
 * Thresholding operators
 *
 * They consist of a simd_threshold_init_<type> macro that can be used to
 * set up necessary variables before entering the processing loop, and a
 * simd_threshold_<type> macro that does the actual pixel processing within
 * the loop and uses any guff set up by simd_threshold_init_<type>.
 */


/*
 * Helper functions: simd element _false_ if low <= s <= high.
 * i.e., these routines return an inverted result.
 */

static inline __m256i v_cmp2_s8(__m256i vs, __m256i vlow, __m256i vhigh)
{
    __m256i t1, t2;
    t1 = _mm256_cmpgt_epi8(vlow, vs);
    t2 = _mm256_cmpgt_epi8(vs, vhigh);
    return _mm256_or_si256(t2, t1);
}
static inline __m256i v_cmp2_s16(__m256i vs, __m256i vlow, __m256i vhigh)
{
    __m256i t1, t2;
    t1 = _mm256_cmpgt_epi16(vlow, vs);
    t2 = _mm256_cmpgt_epi16(vs, vhigh);
    return _mm256_or_si256(t2, t1);
}
static inline __m256i v_cmp2_s32(__m256i vs, __m256i vlow, __m256i vhigh)
{
    __m256i t1, t2;
    t1 = _mm256_cmpgt_epi32(vlow, vs);
    t2 = _mm256_cmpgt_epi32(vs, vhigh);
    return _mm256_or_si256(t2, t1);
}
static inline __m256i v_cmp2_f(__m256 vs, __m256 vlow, __m256 vhigh)
{
    __m256i t1, t2;
    t1 = _mm256_castps_si256(_mm256_cmp_ps(vlow, vs, 30));  /* 30 = cmpgt non signalling */
    t2 = _mm256_castps_si256(_mm256_cmp_ps(vs, vhigh, 30)); /* 30 = cmpgt non signalling */
    return _mm256_or_si256(t2, t1);
}
static inline __m256i v_cmp2_d(__m256d vs, __m256d vlow, __m256d vhigh)
{
    __m256i t1, t2;
    t1 = _mm256_castpd_si256(_mm256_cmp_pd(vlow, vs, 30));
    t2 = _mm256_castpd_si256(_mm256_cmp_pd(vs, vhigh, 30));
    return _mm256_or_si256(t2, t1);
}


/*
 * The addition of 0x80 to all unsigned bytes in the SIMD vector enables us
 * to do unsigned comparisons with the signed SIMD comparison CPU
 * instruction (which appears to be all that is available).
 */
#define simd_init_threshold_UBYTE()			\
    const simd_UBYTE sofs = simd_splat_UBYTE(0x80);	\
    vlow = simd_add_UBYTE(vlow, sofs);			\
    vhigh = simd_add_UBYTE(vhigh, sofs);

#define simd_threshold_UBYTE(s1, vlow, vhigh, vres) \
    ({simd_UBYTE t1;				   \
	t1 = simd_add_UBYTE(s1, sofs);		   \
	t1 = v_cmp2_s8(t1, vlow, vhigh);	   \
	_mm256_xor_si256(t1, vres);})

/* Have signed compare so no initialisation guff */
#define simd_init_threshold_BYTE()
static inline simd_UBYTE
_ip_simd_threshold_BYTE(simd_BYTE s1, const simd_BYTE vlow, const simd_BYTE vhigh,
			const simd_UBYTE vres)
{
    simd_BYTE t1 = v_cmp2_s8(s1, vlow, vhigh);
    return _mm256_xor_si256(t1, vres);
}
#define simd_threshold_BYTE _ip_simd_threshold_BYTE

/*
 * To fix the cockup that is _mm256_packs_epi16()
 */
#define FIX_PACK16 0xd8
static inline simd_UBYTE fixed_packs_epi16(simd_SHORT a, simd_SHORT b)
{
    return _mm256_permute4x64_epi64(_mm256_packs_epi16(a, b), FIX_PACK16);
}
static inline simd_SHORT fixed_packs_epi32(simd_LONG a, simd_LONG b)
{
    return _mm256_permute4x64_epi64(_mm256_packs_epi32(a, b), FIX_PACK16);
}


/* ushort needs an init function to transform short into ushort */
#define simd_init_threshold_USHORT()                    \
    const simd_USHORT sofs = simd_splat_USHORT(0x8000);	\
    vlow = simd_add_USHORT(vlow, sofs);			\
    vhigh = simd_add_USHORT(vhigh, sofs);

#define simd_threshold_USHORT(s1, s2, vlow, vhigh, vres)	\
    ({simd_USHORT t1, t2;					\
	t1 = simd_add_USHORT(s1, sofs);				\
	t2 = simd_add_USHORT(s2, sofs);				\
	t1 = v_cmp2_s16(t1, vlow, vhigh);			\
	t2 = v_cmp2_s16(t2, vlow, vhigh);			\
	t1 = fixed_packs_epi16(t1, t2);				\
	_mm256_xor_si256(t1, vres);})

/* short simpler since short simd operators exist */
#define simd_init_threshold_SHORT()

static inline simd_UBYTE
_ip_simd_threshold_SHORT(simd_SHORT s1, simd_SHORT s2,
			 const simd_SHORT vlow, const simd_SHORT vhigh,
			 const simd_SHORT vres)
{
    __m256i t1 = v_cmp2_s16(s1, vlow, vhigh);
    __m256i t2 = v_cmp2_s16(s2, vlow, vhigh);
    t1 = fixed_packs_epi16(t1, t2);
    return _mm256_xor_si256(t1, vres);
}
#define simd_threshold_SHORT _ip_simd_threshold_SHORT

/* Must be a more efficient way of implementing these no doubt */
#define simd_init_threshold_ULONG()			\
    const simd_ULONG sofs = simd_splat_ULONG(0x8000);	\
    vlow = simd_add_ULONG(vlow, sofs);			\
    vhigh = simd_add_ULONG(vhigh, sofs);

#define simd_threshold_ULONG(s1, s2, s3, s4, vlow, vhigh, vres) \
    ({__m256i t1, t2, t3;					\
	t1 = simd_add_ULONG(s1, sofs),				\
        t2 = simd_add_ULONG(s2, sofs),				\
	t1 = v_cmp2_s32(t1, vlow, vhigh),			\
	t2 = v_cmp2_s32(t2, vlow, vhigh),			\
	t1 = fixed_packs_epi32(t1, t2),				\
	t2 = simd_add_ULONG(s3, sofs),				\
	t3 = simd_add_ULONG(s4, sofs),				\
	t2 = v_cmp2_s32(t2, vlow, vhigh),			\
	t3 = v_cmp2_s32(t3, vlow, vhigh),			\
	t2 = fixed_packs_epi32(t2, t3),				\
	t1 = fixed_packs_epi16(t1, t2),				\
	 _mm256_xor_si256(t1, vres);})


static inline simd_UBYTE
_ip_simd_threshold_LONG(simd_LONG s1, simd_LONG s2, simd_LONG s3, simd_LONG s4,
			const simd_LONG vlow, const simd_LONG vhigh,
			const simd_UBYTE vres)
{
    __m256i t1, t2, t3;
    t1 = v_cmp2_s32(s1, vlow, vhigh);
    t2 = v_cmp2_s32(s2, vlow, vhigh);
    t1 = fixed_packs_epi32(t1, t2);
    t2 = v_cmp2_s32(s3, vlow, vhigh);
    t3 = v_cmp2_s32(s4, vlow, vhigh);
    t2 = fixed_packs_epi32(t2, t3);
    t1 = fixed_packs_epi16(t1, t2);
    return _mm256_xor_si256(t1, vres);
}
#define simd_init_threshold_LONG()
#define simd_threshold_LONG _ip_simd_threshold_LONG


static inline simd_UBYTE
_ip_simd_threshold_FLOAT(simd_FLOAT s1, simd_FLOAT s2, simd_FLOAT s3, simd_FLOAT s4,
			 const simd_FLOAT vlow, const simd_FLOAT vhigh,
			 const simd_UBYTE vres)
{
    __m256i t1, t2, t3;
    t1 = v_cmp2_f(s1, vlow, vhigh);
    t2 = v_cmp2_f(s2, vlow, vhigh);
    t1 = fixed_packs_epi32(t1, t2);
    t2 = v_cmp2_f(s3, vlow, vhigh);
    t3 = v_cmp2_f(s4, vlow, vhigh);
    t2 = fixed_packs_epi32(t2, t3);
    t1 = fixed_packs_epi16(t1, t2);
    return _mm256_xor_si256(t1, vres);
}
#define simd_init_threshold_FLOAT()
#define simd_threshold_FLOAT _ip_simd_threshold_FLOAT

static inline simd_UBYTE
_ip_simd_threshold_DOUBLE(simd_DOUBLE s1, simd_DOUBLE s2, simd_DOUBLE s3, simd_DOUBLE s4,
			  simd_DOUBLE s5, simd_DOUBLE s6, simd_DOUBLE s7, simd_DOUBLE s8,
			  const simd_DOUBLE vlow, const simd_DOUBLE vhigh,
			  const simd_UBYTE vres)
{
    __m256i t1, t2, t3, t4;
    t1 = v_cmp2_d(s1, vlow, vhigh);
    t2 = v_cmp2_d(s2, vlow, vhigh);
    t1 = fixed_packs_epi32(t1, t2);
    t2 = v_cmp2_d(s3, vlow, vhigh);
    t3 = v_cmp2_d(s4, vlow, vhigh);
    t2 = fixed_packs_epi32(t2, t3);
    t1 = fixed_packs_epi32(t1, t2);
    t2 = v_cmp2_d(s5, vlow, vhigh);
    t3 = v_cmp2_d(s6, vlow, vhigh);
    t2 = fixed_packs_epi32(t2, t3);
    t3 = v_cmp2_d(s7, vlow, vhigh);
    t4 = v_cmp2_d(s8, vlow, vhigh);
    t3 = fixed_packs_epi32(t3, t4);
    t2 = fixed_packs_epi32(t2, t3);
    t1 = fixed_packs_epi16(t1, t2);
    return _mm256_xor_si256(t1, vres);
}
#define simd_init_threshold_DOUBLE()
#define simd_threshold_DOUBLE _ip_simd_threshold_DOUBLE

/*
 * simd_replicate_right
 * simd_replicate_left
 *
 * Replicate the Nth element in the vector to all elements to the right (or
 * left) of the Nth element.  Useful for managing image edges.  Right means
 * replicate in the direction of increasing memory address, i.e. along an
 * image row in increasing pixel order.
 */

static inline simd_UBYTE _ip_simd_replicate_right_UBYTE(simd_UBYTE x, int pos)
{
    const simd_UBYTE vinc = _mm256_setr_epi8(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,
					     16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31);
    simd_UBYTE vpos = simd_splat_UBYTE(pos);
    return simd_perm_UBYTE(x, simd_min_UBYTE(vinc, vpos));
}
static inline simd_UBYTE _ip_simd_replicate_left_UBYTE(simd_UBYTE x, int pos)
{
    const simd_UBYTE vinc = _mm256_setr_epi8(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,
					     16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31);
    simd_UBYTE vpos = simd_splat_UBYTE(pos);
    return simd_perm_UBYTE(x, simd_max_UBYTE(vinc, vpos));
}

static inline simd_USHORT _ip_simd_replicate_right_USHORT(simd_USHORT x, int pos)
{
    const simd_USHORT vinc = _mm256_setr_epi16(0x0100, 0x0302, 0x0504, 0x0706,
					       0x0908, 0x0b0a, 0x0d0c, 0x0f0e,
					       0x1110, 0x1312, 0x1514, 0x1716,
					       0x1918, 0x1b1a, 0x1d1c, 0x1f1e);
    simd_USHORT vpos = simd_splat_USHORT((pos<<1) + (pos<<9) + 0X0100);
    return simd_perm_UBYTE(x, simd_min_USHORT(vinc, vpos));
}
static inline simd_USHORT _ip_simd_replicate_left_USHORT(simd_USHORT x, int pos)
{
    const simd_USHORT vinc = _mm256_setr_epi16(0x0100, 0x0302, 0x0504, 0x0706,
					       0x0908, 0x0b0a, 0x0d0c, 0x0f0e,
					       0x1110, 0x1312, 0x1514, 0x1716,
					       0x1918, 0x1b1a, 0x1d1c, 0x1f1e);
    simd_USHORT vpos = simd_splat_USHORT((pos<<1) + (pos<<9) + 0X0100);
    return simd_perm_UBYTE(x, simd_max_USHORT(vinc, vpos));
}

static inline simd_ULONG _ip_simd_replicate_right_ULONG(simd_ULONG x, int pos)
{
    const simd_ULONG vinc = _mm256_setr_epi32(0, 1, 2, 3, 4, 5, 6, 7);
    simd_ULONG vpos = simd_splat_ULONG(pos);
    return _mm256_permutevar8x32_epi32(x, simd_min_ULONG(vinc, vpos));
}
static inline simd_ULONG _ip_simd_replicate_left_ULONG(simd_ULONG x, int pos)
{
    const simd_ULONG vinc = _mm256_setr_epi32(0, 1, 2, 3, 4, 5, 6, 7);
    simd_ULONG vpos = simd_splat_ULONG(pos);
    return _mm256_permutevar8x32_epi32(x, simd_max_ULONG(vinc, vpos));
}

static inline simd_DOUBLE _ip_simd_replicate_right_DOUBLE(simd_DOUBLE x, int pos)
{
    const simd_ULONG vinc = _mm256_setr_epi32(0, 0, 2, 2, 4, 4, 6, 6);
    const simd_ULONG hoffset = _mm256_setr_epi32(0, 1, 0, 1, 0, 1, 0, 1);
    simd_ULONG vpos = simd_splat_ULONG(pos<<1);
    vpos = simd_add_ULONG(simd_min_ULONG(vinc, vpos), hoffset);
    return (simd_DOUBLE)_mm256_permutevar8x32_epi32((__m256i)x, vpos);
}
static inline simd_DOUBLE _ip_simd_replicate_left_DOUBLE(simd_DOUBLE x, int pos)
{
    const simd_ULONG vinc = _mm256_setr_epi32(0, 0, 2, 2, 4, 4, 6, 6);
    const simd_ULONG hoffset = _mm256_setr_epi32(0, 1, 0, 1, 0, 1, 0, 1);
    simd_ULONG vpos = simd_splat_ULONG(pos<<1);
    vpos = simd_add_ULONG(simd_max_ULONG(vinc, vpos), hoffset);
    return (simd_DOUBLE)_mm256_permutevar8x32_epi32((__m256i)x, vpos);
}

#define simd_replicate_right_UBYTE(x, p) _ip_simd_replicate_right_UBYTE(x, p)
#define simd_replicate_right_BYTE(x, p) _ip_simd_replicate_right_UBYTE(x, p)
#define simd_replicate_left_UBYTE(x, p) _ip_simd_replicate_left_UBYTE(x, p)
#define simd_replicate_left_BYTE(x, p) _ip_simd_replicate_left_UBYTE(x, p)

#define simd_replicate_right_USHORT(x, p) _ip_simd_replicate_right_USHORT(x, p)
#define simd_replicate_right_SHORT(x, p) _ip_simd_replicate_right_USHORT(x, p)
#define simd_replicate_left_USHORT(x, p) _ip_simd_replicate_left_USHORT(x, p)
#define simd_replicate_left_SHORT(x, p) _ip_simd_replicate_left_USHORT(x, p)

#define simd_replicate_right_ULONG(x, p) _ip_simd_replicate_right_ULONG(x, p)
#define simd_replicate_right_LONG(x, p) _ip_simd_replicate_right_ULONG(x, p)
#define simd_replicate_left_ULONG(x, p) _ip_simd_replicate_left_ULONG(x, p)
#define simd_replicate_left_LONG(x, p) _ip_simd_replicate_left_ULONG(x, p)

#define simd_replicate_right_FLOAT(x, p) ((simd_FLOAT)_ip_simd_replicate_right_ULONG((simd_ULONG)x, p))
#define simd_replicate_left_FLOAT(x, p) ((simd_FLOAT)_ip_simd_replicate_left_ULONG((simd_ULONG)x, p))

#define simd_replicate_right_DOUBLE(x, p) (_ip_simd_replicate_right_DOUBLE(x, p))
#define simd_replicate_left_DOUBLE(x, p) (_ip_simd_replicate_left_DOUBLE(x, p))


/*
 * simd_zero_right
 *
 * zero_right means to zero all elements to the right (i.e. in increasing
 * memory address order) of the specified position pos.  It is like
 * replicate_right but is used to for effecting a zero boundary on the right
 * side of images in filtering operations.
 */

static inline simd_UBYTE _ip_simd_zero_right_UBYTE(simd_UBYTE x, int pos)
{
    const simd_UBYTE vinc = _mm256_setr_epi8(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,
					     16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31);
    simd_UBYTE vpos = simd_splat_UBYTE(pos);
    return _mm256_andnot_si256(_mm256_cmpgt_epi8(vinc, vpos), x);
}

static inline simd_USHORT _ip_simd_zero_right_USHORT(simd_USHORT x, int pos)
{
    const simd_USHORT vinc = _mm256_setr_epi16(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15);
    simd_USHORT vpos = simd_splat_USHORT(pos);
    return _mm256_andnot_si256(_mm256_cmpgt_epi16(vinc, vpos), x);
}

static inline simd_ULONG _ip_simd_zero_right_ULONG(simd_ULONG x, int pos)
{
    const simd_ULONG vinc = _mm256_setr_epi32(0, 1, 2, 3, 4, 5, 6, 7);
    simd_ULONG vpos = simd_splat_ULONG(pos);
    return _mm256_andnot_si256(_mm256_cmpgt_epi32(vinc, vpos), x);
}

static inline simd_FLOAT _ip_simd_zero_right_FLOAT(simd_FLOAT x, int pos)
{
    const simd_ULONG vinc = _mm256_setr_epi32(0, 1, 2, 3, 4, 5, 6, 7);
    simd_ULONG vpos = simd_splat_ULONG(pos);
    return _mm256_andnot_ps((__m256)_mm256_cmpgt_epi32(vinc, vpos), x);
}

static inline simd_DOUBLE _ip_simd_zero_right_DOUBLE(simd_DOUBLE x, int pos)
{
    const simd_ULONG64 vinc = _mm256_setr_epi64x(0, 1, 2, 3);
    simd_ULONG64 vpos = simd_splat_ULONG64(pos);
    return _mm256_andnot_pd((__m256d)_mm256_cmpgt_epi64(vinc, vpos), x);
}

#define simd_zero_right_UBYTE(x, p) _ip_simd_zero_right_UBYTE(x, p)
#define simd_zero_right_BYTE(x, p) _ip_simd_zero_right_UBYTE(x, p)
#define simd_zero_right_USHORT(x, p) _ip_simd_zero_right_USHORT(x, p)
#define simd_zero_right_SHORT(x, p) _ip_simd_zero_right_USHORT(x, p)
#define simd_zero_right_ULONG(x, p) _ip_simd_zero_right_ULONG(x, p)
#define simd_zero_right_LONG(x, p) _ip_simd_zero_right_ULONG(x, p)
#define simd_zero_right_FLOAT(x, p) _ip_simd_zero_right_FLOAT(x, p)
#define simd_zero_right_DOUBLE(x, p) _ip_simd_zero_right_DOUBLE(x, p)

static inline simd_UBYTE _ip_simd_zero_left_UBYTE(simd_UBYTE x, int pos)
{
    const simd_UBYTE vinc = _mm256_setr_epi8(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,
					     16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31);
    simd_UBYTE vpos = simd_splat_UBYTE(pos);
    return _mm256_andnot_si256(_mm256_cmpgt_epi8(vpos, vinc), x);
}

static inline simd_USHORT _ip_simd_zero_left_USHORT(simd_USHORT x, int pos)
{
    const simd_USHORT vinc = _mm256_setr_epi16(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15);
    simd_USHORT vpos = simd_splat_USHORT(pos);
    return _mm256_andnot_si256(_mm256_cmpgt_epi16(vpos, vinc), x);
}

static inline simd_ULONG _ip_simd_zero_left_ULONG(simd_ULONG x, int pos)
{
    const simd_ULONG vinc = _mm256_setr_epi32(0, 1, 2, 3, 4, 5, 6, 7);
    simd_ULONG vpos = simd_splat_ULONG(pos);
    return _mm256_andnot_si256(_mm256_cmpgt_epi32(vpos, vinc), x);
}

static inline simd_FLOAT _ip_simd_zero_left_FLOAT(simd_FLOAT x, int pos)
{
    const simd_ULONG vinc = _mm256_setr_epi32(0, 1, 2, 3, 4, 5, 6, 7);
    simd_ULONG vpos = simd_splat_ULONG(pos);
    return _mm256_andnot_ps((__m256)_mm256_cmpgt_epi32(vpos, vinc), x);
}

static inline simd_DOUBLE _ip_simd_zero_left_DOUBLE(simd_DOUBLE x, int pos)
{
    const simd_ULONG64 vinc = _mm256_setr_epi64x(0, 1, 2, 3);
    simd_ULONG64 vpos = simd_splat_ULONG64(pos);
    return _mm256_andnot_pd((__m256d)_mm256_cmpgt_epi64(vpos, vinc), x);
}

#define simd_zero_left_UBYTE(x, p) _ip_simd_zero_left_UBYTE(x, p)
#define simd_zero_left_BYTE(x, p) _ip_simd_zero_left_UBYTE(x, p)
#define simd_zero_left_USHORT(x, p) _ip_simd_zero_left_USHORT(x, p)
#define simd_zero_left_SHORT(x, p) _ip_simd_zero_left_USHORT(x, p)
#define simd_zero_left_ULONG(x, p) _ip_simd_zero_left_ULONG(x, p)
#define simd_zero_left_LONG(x, p) _ip_simd_zero_left_ULONG(x, p)
#define simd_zero_left_FLOAT(x, p) _ip_simd_zero_left_FLOAT(x, p)
#define simd_zero_left_DOUBLE(x, p) _ip_simd_zero_left_DOUBLE(x, p)
