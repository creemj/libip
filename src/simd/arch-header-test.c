/* To get strdup and getopt */
#define _XOPEN_SOURCE 500

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdarg.h>
#include <assert.h>
#include <math.h>

#include <unistd.h>

#include "../internal.h"
#include <ip/complex.h>
#include <ip/random.h>

#include "simd.h"

static int vdebug = 0;
static char *function_to_test = NULL;
static int dump_as_hex = 0;
static int expect_failure = 0;
static int use_colour = 0;

/* Colour terminal sequences */

#define COLOUR_NORMAL "\033[0m"
#define COLOUR_OK ""
#define COLOUR_WARN "\033[0;33m"
#define COLOUR_FAILED "\033[1;31m"


/* For use in types_used below */

#define BINARY_bit 0x0001
#define UBYTE_bit 0x0002
#define BYTE_bit 0x0004
#define USHORT_bit 0x0008
#define SHORT_bit 0x0010
#define ULONG_bit 0x0020
#define LONG_bit 0x0040
#define FLOAT_bit 0x0080
#define DOUBLE_bit 0x0100
#define COMPLEX_bit 0x0200
#define DCOMPLEX_bit 0x0400
#define ULONG64_bit 0x0800
#define LONG64_bit 0x1000

static int types_used;

char *type_strs[18] = {
    "BINARY",
    "UBYTE", "BYTE", "USHORT", "SHORT", "ULONG", "LONG",
    "FLOAT", "DOUBLE", "COMPLEX", "DCOMPLEX",
    "PALETTE", "RGB", "RGBA", "RGB16"
#ifdef simd_splat_ULONG64
    , "ULONG64", "LONG64"
#else
    , "", ""
#endif
};

typedef union {
    simd_UBYTE ub;
    simd_BYTE b;
    simd_USHORT us;
    simd_SHORT s;
#ifdef simd_splat_ULONG
    simd_ULONG ul;
    simd_LONG l;
#endif
#ifdef simd_splat_FLOAT
    simd_FLOAT f;
#endif
#ifdef simd_splat_DOUBLE
    simd_DOUBLE d;
#endif
#ifdef simd_splat_ULONG64
    simd_ULONG64 ull;
    simd_LONG64 ll;
#endif
} vany;


/* Typedef vrep_UBYTE, etc. */

#define generate_union_vrep(tt)			\
    typedef union {				\
	simd_ ## tt v;				\
	tt ## _type e[SIMD_NUM_ELEMENTS(tt)];	\
    } vrep_ ## tt;

generate_union_vrep(BYTE)
generate_union_vrep(UBYTE)
generate_union_vrep(SHORT)
generate_union_vrep(USHORT)
#ifdef simd_splat_ULONG
generate_union_vrep(LONG)
generate_union_vrep(ULONG)
#endif
#ifdef simd_splat_FLOAT
generate_union_vrep(FLOAT)
#endif
#ifdef simd_splat_DOUBLE
generate_union_vrep(DOUBLE)
#endif
#ifdef simd_splat_ULONG64
generate_union_vrep(LONG64)
generate_union_vrep(ULONG64)
#endif
#ifdef simd_splat_COMPLEX
generate_union_vrep(COMPLEX)
#endif
#ifdef simd_splat_DCOMPLEX
generate_union_vrep(DCOMPLEX)
#endif

#define generate_dump(tt, d, f, hexf)			\
    static void dump_ ## tt(char *msg, simd_ ## tt x)	\
    {							\
	const char *fmtstr;				\
	vrep_ ## tt v;					\
	v.v = x;					\
	if (dump_as_hex)				\
	    fmtstr = " %0" #d hexf;			\
	else						\
	    fmtstr = " %" #d f;				\
	printf("%5s:",msg);				\
	for (int j=0; j<SIMD_NUM_ELEMENTS(tt); j++)	\
	    printf(fmtstr, v.e[j]);			\
	putchar('\n');					\
    }

generate_dump(UBYTE, 3, "u", "x")
generate_dump(BYTE, 4, "d", "x")
generate_dump(USHORT, 5, "u", "x")
generate_dump(SHORT, 6, "d", "x")

#ifdef simd_splat_ULONG
generate_dump(ULONG, 8, "u", "x")
generate_dump(LONG, 8, "d", "x")
#endif

#ifdef simd_splat_FLOAT
generate_dump(FLOAT, 10, "g", "a")
#endif

#ifdef simd_splat_DOUBLE
generate_dump(DOUBLE, 10, "g", "a")
#endif

#ifdef simd_splat_ULONG64
generate_dump(ULONG64, 16, PRIu64, PRIx64)
generate_dump(LONG64, 16, PRId64, PRIx64)
#endif

/*
 * Generate vector equality operators for each type.
 *
 * This code extracts each element from the two vectors and checks that each
 * element of the two vectors is identical.  In this way we do not rely upon
 * on any simd code from the header file under test to do the checks.
 *
 * For floating point comparisons we check the bit patterns match as so not
 * only finite numbers are checked be the same, but also require any NaNs or
 * infinities to be exactly the same.
 */

#define generate_vectors_identical(tt)				\
    static int vectors_identical_ ## tt(simd_ ## tt a, simd_ ## tt b)	\
    {								\
	vrep_ ## tt va, vb;					\
	int ok = 1;						\
	va.v = a;						\
	vb.v = b;						\
	for (int j=0; j<SIMD_NUM_ELEMENTS(tt); j++) {		\
	    if (va.e[j] != vb.e[j]) {				\
		ok = 0;						\
		break;						\
	    }							\
	}							\
	if (!(vdebug&0x02) && ((vdebug&0x01) && !ok))		\
	    printf("%s:\n", #tt);				\
	if ((vdebug&0x02) || ((vdebug&0x01) && !ok)) {		\
	    dump_ ## tt("simd", a);				\
	    dump_ ## tt("base", b);				\
	}							\
	return ok;						\
    }

#define generate_vectors_identical_real(tt, xt)				\
    static int vectors_identical_ ## tt(simd_ ## tt a, simd_ ## tt b)	\
    {								\
	vrep_ ## xt va, vb;					\
	int ok = 1;						\
	va.v = (simd_##xt)a;					\
	vb.v = (simd_##xt)b;					\
	for (int j=0; j<SIMD_NUM_ELEMENTS(tt); j++) {		\
	    if (va.e[j] != vb.e[j]) {				\
		ok = 0;						\
		break;						\
	    }							\
	}							\
	if (!(vdebug&0x02) && ((vdebug&0x01) && !ok))		\
	    printf("%s:\n", #tt);				\
	if ((vdebug&0x02) || ((vdebug&0x01) && !ok)) {		\
	    dump_ ## tt("simd", a);				\
	    dump_ ## tt("base", b);				\
	}							\
	return ok;						\
    }

generate_vectors_identical(BYTE)
generate_vectors_identical(UBYTE)
generate_vectors_identical(SHORT)
generate_vectors_identical(USHORT)
#ifdef simd_splat_ULONG
generate_vectors_identical(LONG)
generate_vectors_identical(ULONG)
#endif
#ifdef simd_splat_FLOAT
generate_vectors_identical_real(FLOAT, ULONG)
#endif
#ifdef simd_splat_DOUBLE
/* Assumes that simd_ULONG64 is defined if simd_DOUBLE is defined */
generate_vectors_identical_real(DOUBLE, ULONG64)
#endif
#ifdef simd_splat_ULONG64
generate_vectors_identical(LONG64)
generate_vectors_identical(ULONG64)
#endif

#ifdef simd_splat_COMPLEX
static int vectors_identical_COMPLEX(simd_COMPLEX a, simd_COMPLEX b)
{
    return vectors_identical_FLOAT(a, b);
}
#endif

#ifdef simd_splat_DCOMPLEX
static int vectors_identical_DCOMPLEX(simd_DCOMPLEX a, simd_DCOMPLEX b)
{
    return vectors_identical_DOUBLE(a, b);
}
#endif


#define generate_vectors_identical_2(type, tt)			\
    static int vectors_identical_2_ ## type(simd_ ## type ## _2 a, simd_ ## type ## _2 b) \
    {									\
	return vectors_identical_ ## type(a.tt[0], b.tt[0])		\
	    & vectors_identical_## type(a.tt[1], b.tt[1]);		\
    }

generate_vectors_identical_2(BYTE, b)
generate_vectors_identical_2(UBYTE, ub)

generate_vectors_identical_2(SHORT, s)
generate_vectors_identical_2(USHORT, us)

#ifdef simd_splat_ULONG
generate_vectors_identical_2(LONG, l)
generate_vectors_identical_2(ULONG, ul)
#endif

#ifdef simd_splat_FLOAT
generate_vectors_identical_2(FLOAT, f)
#endif

#ifdef simd_splat_DOUBLE
generate_vectors_identical_2(DOUBLE, d)
#endif

#ifdef simd_splat_COMPLEX
static int vectors_identical_2_COMPLEX(simd_COMPLEX_2 a, simd_COMPLEX_2 b)
{
    simd_FLOAT_2 fa, fb;
    fa.f[0] = a.c[0];  fa.f[1] = a.c[1];
    fb.f[0] = b.c[0];  fb.f[1] = b.c[1];
    return vectors_identical_2_FLOAT(fa, fb);
}
#endif

#ifdef simd_splat_DCOMPLEX
static int vectors_identical_2_DCOMPLEX(simd_DCOMPLEX_2 a, simd_DCOMPLEX_2 b)
{
    simd_DOUBLE_2 fa, fb;
    fa.d[0] = a.dc[0];  fa.d[1] = a.dc[1];
    fb.d[0] = b.dc[0];  fb.d[1] = b.dc[1];
    return vectors_identical_2_DOUBLE(fa, fb);
}
#endif

#define generate_vectors_identical_4(type, tt)			\
    static int vectors_identical_4_ ## type(simd_ ## type ## _4 a, simd_ ## type ## _4 b) \
    {									\
	return vectors_identical_ ## type(a.tt[0], b.tt[0])		\
	    & vectors_identical_## type(a.tt[1], b.tt[1])		\
	    & vectors_identical_## type(a.tt[2], b.tt[2])		\
	    & vectors_identical_## type(a.tt[3], b.tt[3]);		\
    }

generate_vectors_identical_4(UBYTE, ub)
#ifdef simd_splat_ULONG
generate_vectors_identical_4(LONG, l)
generate_vectors_identical_4(ULONG, ul)
#endif
#ifdef simd_splat_FLOAT
generate_vectors_identical_4(FLOAT, f)
#endif
#ifdef simd_splat_DOUBLE
generate_vectors_identical_4(DOUBLE, d)
#endif

#define generate_vectors_identical_8(type, tt)			\
    static int vectors_identical_8_ ## type(simd_ ## type ## _8 a, simd_ ## type ## _8 b) \
    {									\
	return vectors_identical_ ## type(a.tt[0], b.tt[0])		\
	    & vectors_identical_## type(a.tt[1], b.tt[1])		\
	    & vectors_identical_## type(a.tt[2], b.tt[2])		\
	    & vectors_identical_## type(a.tt[3], b.tt[3])		\
	    & vectors_identical_## type(a.tt[4], b.tt[4])		\
	    & vectors_identical_## type(a.tt[5], b.tt[5])		\
	    & vectors_identical_## type(a.tt[6], b.tt[6])		\
	    & vectors_identical_## type(a.tt[7], b.tt[7]);		\
    }

generate_vectors_identical_8(USHORT, us)
#ifdef simd_splat_ULONG
generate_vectors_identical_8(ULONG, ul)
#endif
#ifdef simd_splat_FLOAT
generate_vectors_identical_8(FLOAT, f)
#endif



/*
 * Set all elements of a vector to the same value.
 *
 * Sets each element individually with scalar code.
 */

#define generate_vector_constant_int(tt)			\
    static simd_ ## tt vector_constant_ ## tt(int64_t val)	\
    {								\
	vrep_ ## tt a;						\
	for (int j=0; j<SIMD_NUM_ELEMENTS(tt); j++)		\
	    a.e[j] = (tt ## _type)val;				\
	return a.v;						\
    }

#define generate_vector_constant_real(tt)			\
    static simd_ ## tt vector_constant_ ## tt(double val)	\
    {								\
	vrep_ ## tt a;						\
	for (int j=0; j<SIMD_NUM_ELEMENTS(tt); j++)		\
	    a.e[j] = (tt ## _type)val;				\
	return a.v;						\
    }

generate_vector_constant_int(BYTE)
generate_vector_constant_int(UBYTE)
generate_vector_constant_int(SHORT)
generate_vector_constant_int(USHORT)
#ifdef simd_splat_ULONG
generate_vector_constant_int(LONG)
generate_vector_constant_int(ULONG)
#endif
#ifdef simd_splat_FLOAT
generate_vector_constant_real(FLOAT)
#endif
#ifdef simd_splat_DOUBLE
generate_vector_constant_real(DOUBLE)
#endif


#define generate_vector_set_element_int(tt)					\
    static simd_ ## tt vector_set_element_ ## tt(simd_ ## tt a, int pos, int64_t val) \
    {									\
	vrep_ ## tt va;							\
	va.v = a;							\
	va.e[pos] = (tt ## _type)val;					\
	return va.v;							\
    }

#define generate_vector_set_element_real(tt)					\
    static simd_ ## tt vector_set_element_ ## tt(simd_ ## tt a, int pos, double val) \
    {									\
	vrep_ ## tt va;							\
	va.v = a;							\
	va.e[pos] = (tt ## _type)val;					\
	return va.v;							\
    }

generate_vector_set_element_int(BYTE)
generate_vector_set_element_int(UBYTE)
generate_vector_set_element_int(SHORT)
generate_vector_set_element_int(USHORT)
#ifdef simd_splat_ULONG
generate_vector_set_element_int(LONG)
generate_vector_set_element_int(ULONG)
#endif
#ifdef simd_splat_FLOAT
generate_vector_set_element_real(FLOAT)
#endif
#ifdef simd_splat_DOUBLE
generate_vector_set_element_real(DOUBLE)
#endif


#define generate_vector_set_incr(tt)					\
    static simd_ ## tt vector_set_incr_ ## tt(int start, int *next)	\
    {									\
	vrep_ ## tt va;							\
	for (int j=0; j<SIMD_NUM_ELEMENTS(tt); j++)			\
	    va.e[j] = start+j;						\
	if (next)							\
	    *next = start + SIMD_NUM_ELEMENTS(tt);			\
	return va.v;							\
    }

#define generate_vector_set_incr_real(tt)				\
    static simd_ ## tt vector_set_incr_ ## tt(double start, double *next) \
    {									\
	vrep_ ## tt va;							\
	for (int j=0; j<SIMD_NUM_ELEMENTS(tt); j++)			\
	    va.e[j] = start+j;						\
	if (next)							\
	    *next = start + SIMD_NUM_ELEMENTS(tt);			\
	return va.v;							\
    }

generate_vector_set_incr(UBYTE)
generate_vector_set_incr(BYTE)
generate_vector_set_incr(USHORT)
generate_vector_set_incr(SHORT)
#ifdef simd_splat_ULONG
generate_vector_set_incr(LONG)
generate_vector_set_incr(ULONG)
#endif
#ifdef simd_splat_FLOAT
generate_vector_set_incr_real(FLOAT)
#endif
#ifdef simd_splat_DOUBLE
generate_vector_set_incr_real(DOUBLE)
#endif

#define generate_vector_set_incr_with_step(tt)				\
    static simd_ ## tt vector_set_incr_with_step_ ## tt(int start, int step, int reset) \
    {									\
	vrep_ ## tt va;							\
	int id_1 = start;						\
	int id_2 = 0;							\
	for (int j=0; j<SIMD_NUM_ELEMENTS(tt); j++) {			\
	    va.e[j] = id_1 + id_2;					\
	    if ((j % reset) == (reset-1)) {				\
		id_2++;							\
		id_1 = start;						\
	    }else{							\
		id_1 += step;						\
	    }								\
	}								\
	return va.v;							\
    }

generate_vector_set_incr_with_step(UBYTE)
generate_vector_set_incr_with_step(BYTE)
generate_vector_set_incr_with_step(USHORT)
generate_vector_set_incr_with_step(SHORT)
#ifdef simd_splat_ULONG
generate_vector_set_incr_with_step(LONG)
generate_vector_set_incr_with_step(ULONG)
#endif


#define generate_vector_set_mask_complex(tt, basett)			\
    static simd_##basett vector_set_mask_##tt(double real, double imag)	\
    {									\
	vrep_##basett va;						\
	for (int j=0; j<SIMD_NUM_ELEMENTS(basett); j++) {		\
	    va.e[j] = (basett##_type)real;				\
	    va.e[j+1] = (basett##_type)imag;				\
	}								\
	return va.v;							\
    }

#define generate_vector_set_mask_rgb(tt, basett)			\
    static simd_##basett vector_set_mask_##tt(int r, int g, int b)	\
    {									\
	vrep_##basett va;						\
	for (int j=0; j<SIMD_NUM_ELEMENTS(basett); j++) {		\
	    int field = j % 3;						\
	    switch (field) {						\
	    case 0:							\
		va.e[j] = (basett##_type)r;				\
		break;							\
	    case 1:							\
		va.e[j] = (basett##_type)g;				\
		break;							\
	    case 2:							\
		va.e[j] = (basett##_type)b;				\
		break;							\
	    }								\
	}								\
	return va.v;							\
    }

#ifdef simd_splat_COMPLEX
generate_vector_set_mask_complex(COMPLEX, FLOAT)
#endif
#ifdef simd_splat_DCOMPLEX
generate_vector_set_mask_complex(DCOMPLEX, DOUBLE)
#endif

generate_vector_set_mask_rgb(RGB, UBYTE)
generate_vector_set_mask_rgb(RGB16, USHORT)

static simd_UBYTE vector_set_mask_RGBA(int r, int g, int b, int a)
{
    vrep_UBYTE va;
    for (int j=0; j<SIMD_NUM_ELEMENTS(UBYTE); j++) {
	int field = j & 0x03;
	switch (field) {
	case 0:
	    va.e[j] = (uint8_t)r;
	    break;
	case 1:
	    va.e[j] = (uint8_t)g;
	    break;
	case 2:
	    va.e[j] = (uint8_t)b;
	    break;
	case 3:
	    va.e[j] = (uint8_t)a;
	    break;
	}
    }
    return va.v;
}

/*
 * Generate uniformly distributed random numbers for integer types
 */

#define generate_vector_random(tt)					\
    static simd_ ## tt vector_random_ ## tt(void)			\
    {									\
	vrep_UBYTE a;							\
	for (int j=0; j<SIMD_NUM_ELEMENTS(UBYTE); j++)			\
	    a.e[j] = 255.9 * ((double)rand() / (double)RAND_MAX);	\
	return (simd_ ## tt)a.v;					\
    }

generate_vector_random(BYTE)
generate_vector_random(UBYTE)
generate_vector_random(SHORT)
generate_vector_random(USHORT)
#ifdef simd_splat_ULONG
generate_vector_random(LONG)
generate_vector_random(ULONG)
#endif

/*
 * Generate weighted distributed random numbers for integer types with a
 * preference for smaller values than very large values.
 */

static simd_USHORT vector_wrand_USHORT(void)
{
    vrep_USHORT a;
    for (int j=0; j<SIMD_NUM_ELEMENTS(USHORT); j++) {
	a.e[j] = ip_rand_wushort();
    }
    return (simd_USHORT)a.v;
}

static simd_SHORT vector_wrand_SHORT(void)
{
    vrep_SHORT a;
    for (int j=0; j<SIMD_NUM_ELEMENTS(SHORT); j++) {
	a.e[j] = ip_rand_wshort();
    }
    return (simd_SHORT)a.v;
}

static simd_ULONG vector_wrand_ULONG(void)
{
    vrep_ULONG a;
    for (int j=0; j<SIMD_NUM_ELEMENTS(ULONG); j++) {
	a.e[j] = ip_rand_wulong();
    }
    return (simd_ULONG)a.v;
}

static simd_LONG vector_wrand_LONG(void)
{
    vrep_LONG a;
    for (int j=0; j<SIMD_NUM_ELEMENTS(LONG); j++) {
	a.e[j] = ip_rand_wlong();
    }
    return (simd_LONG)a.v;
}

/* Numbers produced should not exceed max */
static simd_SHORT vector_wrandlim_SHORT(int max)
{
    vrep_SHORT a;
    int lz = ip_clz(max) - 17;
    if (lz < 0) lz  = 0;
    for (int j=0; j<SIMD_NUM_ELEMENTS(SHORT); j++) {
	int num = (int)((unsigned int)rand() / (unsigned int)(RAND_MAX>>16));
	unsigned int shift = rand() / (RAND_MAX/(8-lz)) + lz;
	a.e[j] = num >> shift;
    }
    return (simd_SHORT)a.v;
}

/* Numbers produced should not exceed max */
static simd_LONG vector_wrandlim_LONG(int max)
{
    vrep_LONG a;
    int lz = ip_clz(max) - 1;
    for (int j=0; j<SIMD_NUM_ELEMENTS(LONG); j++) {
	int num = (int)((unsigned int)rand() << 1);
	unsigned int shift = rand() / (RAND_MAX/(20-lz)) + lz;
	a.e[j] = num >> shift;
    }
    return (simd_LONG)a.v;
}


/*
 * Generate normally distributed numbers for floating point with mean zero
 * and unit standard devation.
 */

static double rand_normal(void)
{
    static int iset = 0;
    static double gset;

    if  (iset == 0) {
	double fac, r2, v1, v2;
        do {
            v1 = 2.0 * (double)rand()/(double)RAND_MAX - 1.0;
            v2 = 2.0 * (double)rand()/(double)RAND_MAX - 1.0;
            r2 = v1*v1 + v2*v2;
        } while (r2 >= 1.0);
        fac = sqrt(-2.0 * log(r2) / r2);
        gset = v1*fac;
        iset = 1;
        return v2*fac;
    } else {
        iset = 0;
        return gset;
    }
}

#define generate_vector_random_real(tt, bt)			\
    static simd_##tt vector_random_##tt(double sd)		\
    {								\
	vrep_##tt res;						\
	for (int j=0; j<SIMD_NUM_ELEMENTS(tt); j++) {		\
	    res.e[j] = (bt)(sd * rand_normal());		\
	}							\
	return (simd_##tt)res.v;				\
    }

#ifdef simd_splat_FLOAT
generate_vector_random_real(FLOAT, float)
#endif
#ifdef simd_splat_DOUBLE
generate_vector_random_real(DOUBLE, double)
#endif


/*
 * Generate floating point random number within the limits of INT32_MIN and
 * INT32_MAX inclusive (after truncation of the fraction).  The distribution
 * is weighted towards lower values but has a long tail so that big values
 * can be generated (the probability density function is an exponential
 * decay).  This is achieved by raising two to the power of the uniformly
 * distributed numbers (thus, every floating-point binade in the int32 range
 * has equal likelihood to be represented).
 *
 * These routines are not quite perfect. For FLOAT they overflow because
 * RAND_MAX is INT32_MAX which is not representable as a single precision
 * float.  For DOUBLE they remain in range but don't quite get to INT32_MIN
 * and INT32_MAX.  Oh well...
 */


#define generate_vector_random_i32_real(tt, bt, ff)			\
    static simd_##tt vector_random_i32_##tt(void)			\
    {									\
	vrep_##tt res;							\
	for (int j=0; j<SIMD_NUM_ELEMENTS(tt); j++) {			\
	    bt mag;							\
	    bt x = (bt)(rand() - (int)(((int64_t)RAND_MAX+1)/2));	\
	    x *= 2.0/((int64_t)RAND_MAX+1);				\
	    mag = 33.0*fabs##ff(x) - 2.0;				\
	    res.e[j] = copysign##ff(exp2##ff(mag), x);			\
	}								\
	return (simd_##tt)res.v;					\
    }

#ifdef simd_splat_FLOAT
generate_vector_random_i32_real(FLOAT, float, f)
#endif
#ifdef simd_splat_DOUBLE
generate_vector_random_i32_real(DOUBLE, double, )
#endif


#define generate_vector_random_u32_real(tt, bt)			\
    static simd_##tt vector_random_u32_##tt(void)		\
    {								\
	vrep_##tt res;						\
	for (int j=0; j<SIMD_NUM_ELEMENTS(tt); j++) {		\
	    bt x = (bt)rand();					\
	    x *= 34.0 / (bt)((int64_t)RAND_MAX+1) - 2.0;	\
	    res.e[j] = exp2(x);					\
	}							\
	return (simd_##tt)res.v;				\
    }


#ifdef simd_splat_FLOAT
generate_vector_random_u32_real(FLOAT, float)
#endif
#ifdef simd_splat_DOUBLE
generate_vector_random_u32_real(DOUBLE, double)
#endif

/*
 * These routines intialise vectors with a number of boundary important
 * values for testing the conversion routines.  They initialise the first
 * 64-bits of the vector only as that is the shortest vector we support.
 */

static simd_SHORT vector_important_values_SHORT(int flag)
{
    vrep_SHORT vres;
    int16_t ivals[] = {0, 1, 127, 128, -128, -129, INT16_MAX, INT16_MIN,
		       -1, 255, 256, 1235};

    assert(flag >= 1 && flag <= 3);

    vres.v = vector_random_SHORT();
    for (int j=0; j<4; j++) {
	vres.e[j] = ivals[4*(flag-1)+j];
    }

    return vres.v;
}


static simd_USHORT vector_important_values_USHORT(int flag)
{
    vrep_USHORT vres;
    uint16_t ivals[] = {0, 1, 127, 128, 255, 256, INT16_MAX, INT16_MAX+1,
		      UINT16_MAX, 73, 50267, 1235};

    assert(flag >= 1 && flag <= 3);

    vres.v = vector_random_USHORT();
    for (int j=0; j<4; j++) {
	vres.e[j] = ivals[4*(flag-1)+j];
    }

    return vres.v;
}



/*
 * Note that ensure_both_signs_USHORT() and ensure_some_bytes_USHORT() can
 * be safely used on the same vector (i.e. they affect different SHORTs in
 * the vector), however this is not the case for ensure_both_signs_ULONG()
 * and ensure_some_bytes_ULONG(). The latter two should not be used on the
 * same vector as they affect the same parts of the vector so that we can
 * support 64-bit vectors.
 */

#define generate_ensure_both_signs(tt, signbit)			\
    static void ensure_both_signs_ ## tt(simd_ ## tt *a)	\
    {								\
	vrep_ ## tt *aa = (vrep_ ## tt *)a;			\
	aa->e[0] &= ~(signbit);					\
	aa->e[1] |= (signbit);					\
	return;							\
    }								\

generate_ensure_both_signs(UBYTE, 0x80)
generate_ensure_both_signs(USHORT, 0x8000)
#ifdef simd_splat_ULONG
generate_ensure_both_signs(ULONG, 0x80000000)
#endif
#ifdef simd_splat_ULONG64
generate_ensure_both_signs(ULONG64, 0x8000000000000000ULL)
#endif

#define generate_ensure_some_bytes(tt, start)			\
    static void ensure_some_bytes_ ## tt(simd_ ## tt *a)	\
    {								\
	vrep_ ## tt *aa = (vrep_ ## tt *)a;			\
	aa->e[start] &= 0x007f;					\
	aa->e[start] |= 0x0080;					\
	aa->e[start+1] &= 0x007f;				\
	return;							\
    }								\

generate_ensure_some_bytes(USHORT, 2)
#ifdef simd_splat_ULONG
generate_ensure_some_bytes(ULONG, 0)
#endif

#define FMT_BYTE "b"
#define FMT_UBYTE "B"
#define FMT_SHORT "s"
#define FMT_USHORT "S"
#define FMT_LONG "l"
#define FMT_ULONG "L"
#define FMT_LONG64 "x"
#define FMT_ULONG64 "X"
#define FMT_FLOAT "f"
#define FMT_DOUBLE "d"
#define FMT_COMPLEX "c"
#define FMT_DCOMPLEX "C"


static void print_type(char t)
{
    switch(t) {
    case 'b':
	printf("BYTE");
	break;
    case 'B':
	printf("UBYTE");
	break;
    case 's':
	printf("SHORT");
	break;
    case 'S':
	printf("USHORT");
	break;
    case 'l':
	printf("LONG");
	break;
    case 'L':
	printf("ULONG");
	break;
    case 'x':
	printf("LONG64");
	break;
    case 'X':
	printf("ULONG64");
	break;
    case 'f':
	printf("FLOAT");
	break;
    case 'd':
	printf("DOUBLE");
	break;
    case 'c':
	printf("COMPLEX");
	break;
    case 'C':
	printf("DCOMPLEX");
	break;
    case 'i':
	printf("int");
	break;
    case 'r':
	printf("double");
	break;
    default:
	break;
    }
}


static void print_initial(char *dt, char *simdf, char *fmt, ...)
{
    va_list ap;
    if (! (vdebug & 0x02))
	return;
    va_start(ap, fmt);
    if (dt) {
	printf("%s:%s(", dt, simdf);
	for (int j=0; j<strlen(fmt); j++) {
	    if (j>0) putchar(',');
	    print_type(fmt[j]);
	}
	printf(")\n");
    }
    for (int j=0; j<strlen(fmt); j++) {
	switch (fmt[j]) {
	case 'b': {
	    simd_BYTE v = va_arg(ap, simd_BYTE);
	    dump_BYTE("arg", v);
	}   break;
	case 'B': {
	    simd_UBYTE v = va_arg(ap, simd_UBYTE);
	    dump_UBYTE("arg", v);
	}   break;
	case 's': {
	    simd_SHORT v = va_arg(ap, simd_SHORT);
	    dump_SHORT("arg", v);
	}   break;
	case 'S': {
	    simd_USHORT v = va_arg(ap, simd_USHORT);
	    dump_USHORT("arg", v);
	}   break;
#ifdef simd_splat_ULONG
	case 'l': {
	    simd_LONG v = va_arg(ap, simd_LONG);
	    dump_LONG("arg", v);
	}   break;
	case 'L': {
	    simd_ULONG v = va_arg(ap, simd_ULONG);
	    dump_ULONG("arg", v);
	}   break;
#endif
#ifdef simd_splat_ULONG64
	case 'x': {
	    simd_LONG64 v = va_arg(ap, simd_LONG64);
	    dump_LONG64("arg", v);
	}   break;
	case 'X': {
	    simd_ULONG64 v = va_arg(ap, simd_ULONG64);
	    dump_ULONG64("arg", v);
	}   break;
#endif
#ifdef simd_splat_FLOAT
	case 'f':
#ifdef simd_splat_COMPLEX
	case 'c':
#endif
	{
	    simd_FLOAT v = va_arg(ap, simd_FLOAT);
	    dump_FLOAT("arg", v);
	}   break;
#endif
#ifdef simd_splat_DOUBLE
	case 'd':
#ifdef simd_splat_DCOMPLEX
	case 'C':
#endif
	{
	    simd_DOUBLE v = va_arg(ap, simd_DOUBLE);
	    dump_DOUBLE("arg", v);
	}   break;
#endif
	case 'i': {
	    int v = va_arg(ap, int);
	    printf("  arg:  %d\n", v);
	}   break;
	case 'r': {
	    double v = va_arg(ap, double);
	    printf("  arg: %g\n", v);
	}   break;
	default:
	    break;
	}
    }
    va_end(ap);
}


/*
 * Simple mechanism to register footnotes on tests.
 *
 * In a test use the REGISTER_NOTE() macro to register a footnote to the
 * test.  The footnotes are printed at the end of all test reports. This
 * is useful if wish to report some behaviour which does not constitute
 * failure but would be useful to inform the user.
 */

struct notes_node {
    struct notes_node *next;
    char *note;
};

static struct notes_node *notes_list = NULL;

static int push_note(char *note)
{
    struct notes_node *nnode;
    struct notes_node *prev;
    int num;

    if ((nnode = malloc(sizeof(struct notes_node))) == NULL) {
	fprintf(stderr, "Out of memory\n");
	exit(1);
    }

    nnode->next = NULL;
    if ((nnode->note = strdup(note)) == NULL) {
	fprintf(stderr, "Out of memory\n");
	exit(1);
    }

    if (notes_list) {
	for (prev = notes_list, num=1; prev->next; prev = prev->next, num++)
	    /* do nothing */ ;
	prev->next = nnode;
	num++;
    } else {
	notes_list = nnode;
	num = 1;
    }

    return num;
}


static void dump_notes(void)
{
    struct notes_node *note;

    if (notes_list) {
	int num;

	printf("Notes:\n");
	for (note = notes_list, num=1; note; note = note->next, num++) {
	    printf("  %d: %s\n", num, note->note);
	}
    }
}


static void run_test(const char *name, int line, int type, int *ok, int result)
{
    int idx;

    types_used |= type;
    for (idx = 0; type; idx++)
	type >>= 1;

    if (!result) {
	if (expect_failure) {
	    /* Register a warning only if no prior fails occurred. */
	    if (*ok)
		*ok = 2;
	}else{
	    printf("%-29s (%s) at line %d FAILS.\n", name, type_strs[idx-1], line);
	    *ok = 0;
	}
    }
}


static void print_outcome(int result)
{
    static char *msg[] = {"FAILED", "OK", "WARN"};
    static char *colour[] = {COLOUR_FAILED, COLOUR_OK, COLOUR_WARN};

    fputs("... ", stdout);
    if (use_colour)
	fputs(colour[result], stdout);
    fputs(msg[result], stdout);
    if (use_colour)
	fputs(COLOUR_NORMAL, stdout);
}


static void print_result(const char *name, int result, char *note)
{
    int len = 0;

    assert(result >=0 && result <= 2);
    printf("%-33s ", name);
    if (! types_used) {
	len = printf("Not implemented!");
	result = 2;
    } else {
	for (int j=0; j<18; j++)
	    if (types_used & (1<<j))
		len += printf("%s ", type_strs[j]);
    }
    for (int j=len; j<50; j++)
	putchar(' ');
    print_outcome(result);
    if (note) {
	int num = push_note(note);
	printf(" [Note %d]", num);
    }
    printf("\n");
}

#define SKIP_IF_NOT_REQUESTED()					\
    if (function_to_test && strcmp(__func__, function_to_test))	\
	return;

#define START_TEST()				\
    int ok = 1;					\
    const char *name = __func__;		\
    char *note = NULL;				\
    types_used = 0;				\
    SKIP_IF_NOT_REQUESTED();			\
    if (vdebug & 0x02) {printf("%s:\n", __func__);}

#define RUN_TEST(t, x)  run_test(name, __LINE__, t ## _bit, &ok, (x))

#define REGISTER_NOTE(msg) note = msg

#define END_TEST() \
    print_result(name, ok, note)

#define TEST_IDENTICAL(t,x,y)  RUN_TEST(t, vectors_identical_ ## t((x),(y)))
#define TEST_IDENTICAL_2(t,x,y)  RUN_TEST(t, vectors_identical_2_ ## t((x),(y)))
#define TEST_IDENTICAL_4(t,x,y)  RUN_TEST(t, vectors_identical_4_ ## t((x),(y)))

#define TEST_IDENTICAL_I1(tt,x,simdf,basef)				\
    do {								\
	print_initial(#tt, #simdf, "i", x);				\
	RUN_TEST(tt, vectors_identical_## tt(simdf(x), basef(x)));	\
    } while(0)

#define TEST_IDENTICAL_I3(tt,x,y,z,simdf,basef)				\
    do {								\
	print_initial(#tt, #simdf, "iii", x, y, z);			\
	RUN_TEST(tt, vectors_identical_## tt(simdf(x,y,z), basef(x,y,z))); \
    } while(0)

#define TEST_IDENTICAL_I4(tt,x,y,z,a,simdf,basef)			\
    do {								\
	print_initial(#tt, #simdf, "iiii", x, y, z, a);			\
	RUN_TEST(tt, vectors_identical_## tt(simdf(x,y,z,a), basef(x,y,z,a))); \
    } while(0)

#define TEST_IDENTICAL_R1(tt,x,simdf,basef)				\
    do {								\
	print_initial(#tt, #simdf, "r", (x));				\
	RUN_TEST(tt, vectors_identical_## tt(simdf(x), basef(x)));	\
    } while(0)

#define TEST_IDENTICAL_S1(tt,x,simdf,basef)				\
    do {								\
	print_initial(#tt, #simdf, FMT_##tt, x);			\
	RUN_TEST(tt, vectors_identical_## tt(simdf(x), basef(x)));	\
    } while(0)

#define TEST_IDENTICAL_S1_R(tt,x,simdf,basef)				\
    do {								\
	print_initial(#tt, #simdf, FMT_##tt, x);			\
	RUN_TEST(tt, vectors_identical_## tt(simdf(x), basef));		\
    } while(0)

#define TEST_IDENTICAL_S1I1(tt,x,c,simdf,basef)				\
    do {								\
	print_initial(#tt, #simdf, FMT_##tt "i", x, c);			\
	RUN_TEST(tt, vectors_identical_## tt(simdf(x,c), basef(x,c)));	\
    } while(0)

#define TEST_IDENTICAL_S1I1_R(tt,x,c,simdf,basef)			\
    do {								\
	print_initial(#tt, #simdf, FMT_##tt "i", x, c);			\
	RUN_TEST(tt, vectors_identical_## tt(simdf(x,c), basef));	\
    } while(0)

#define TEST_IDENTICAL_S2(tt,x,y,simdf,basef)				\
    do {								\
	print_initial(#tt, #simdf, FMT_##tt FMT_##tt, x, y);		\
	RUN_TEST(tt, vectors_identical_## tt(simdf(x,y), basef(x,y)));	\
    } while(0)

#define TEST_IDENTICAL_S2I1(tt,x,y,c,simdf,basef)			\
    do {								\
	print_initial(#tt, #simdf, FMT_##tt FMT_##tt "i", x, y, c);	\
	RUN_TEST(tt, vectors_identical_## tt(simdf(x,y,c), basef(x,y,c))); \
    } while(0)

#define TEST_IDENTICAL_S3(tt,x,y,z,simdf,basef)				\
    do {								\
	print_initial(#tt, #simdf, FMT_##tt FMT_##tt FMT_##tt, x, y, z); \
	RUN_TEST(tt, vectors_identical_## tt(simdf(x,y,z), basef(x,y,z))); \
    } while(0)

#define TEST_IDENTICAL_S4(tt,x,y,z,w,simdf,basef)			\
    do {								\
	print_initial(#tt, #simdf, FMT_##tt FMT_##tt FMT_##tt FMT_##tt, x, y, z, w); \
	RUN_TEST(tt, vectors_identical_## tt(simdf(x,y,z,w), basef(x,y,z,w))); \
    } while(0)

#define TEST_IDENTICAL_D1_S1(dt,st,x,simdf,basef)			\
    do {								\
	print_initial(#dt, #simdf, FMT_##st, x);			\
	RUN_TEST(dt, vectors_identical_ ## dt(simdf(x), basef(x)));	\
    } while(0)

#define TEST_IDENTICAL_D1_S2(dt,st,x,y,simdf,basef) 			\
    do {								\
	print_initial(#dt, #simdf, FMT_##st FMT_##st, x, y);		\
	RUN_TEST(dt, vectors_identical_ ## dt(simdf(x,y), basef(x,y)));	\
    } while(0)

#define TEST_IDENTICAL_D1_S4(dt,st,v,w,x,y,simdf,basef)			\
    do {								\
	print_initial(#dt, #simdf, FMT_##st FMT_##st FMT_##st FMT_##st, v, w, x, y); \
	RUN_TEST(dt, vectors_identical_ ## dt(simdf(v,w,x,y), basef(v,w,x,y))); \
    } while(0)

#define TEST_IDENTICAL_D2_S1(dt,st,x,simdf,basef)			\
    do {								\
	print_initial(#dt, #simdf, FMT_##st, x);			\
	RUN_TEST(dt, vectors_identical_2_ ## dt(simdf(x), basef(x)));	\
    } while(0)

#define TEST_IDENTICAL_D4_S1(dt,st,x,simdf,basef)			\
    do {								\
	print_initial(#dt, #simdf, FMT_##st, x);			\
	RUN_TEST(dt, vectors_identical_4_ ## dt(simdf(x), basef(x)));	\
    } while(0)

#define TEST_IDENTICAL_D2_S2(t,x,el,simdf,basef)			\
    do {								\
	print_initial(#t, #simdf, FMT_##t FMT_##t, x.el[0], x.el[1]);	\
	RUN_TEST(t, vectors_identical_2_##t(simdf(x), basef(x)));	\
    } while(0)

#define TEST_IDENTICAL_D2_S2x(t,x,y,simdf,basef)			\
    do {								\
	print_initial(#t, #simdf, FMT_##t FMT_##t, x, y);		\
	RUN_TEST(t, vectors_identical_2_##t(simdf(x,y), basef(x,y)));	\
    } while(0)

#define TEST_IDENTICAL_D4_S4(t,x,el,simdf,basef)			\
    do {								\
	print_initial(#t, #simdf, FMT_##t FMT_##t FMT_##t FMT_##t,	\
		      x.el[0], x.el[1], x.el[2], x.el[3]);	\
	RUN_TEST(t, vectors_identical_4_##t(simdf(x), basef(x)));	\
    } while(0)

#define TEST_IDENTICAL_D8_S8(t,x,el,simdf,basef)			\
    do {								\
	print_initial(#t, #simdf, FMT_##t FMT_##t FMT_##t FMT_##t FMT_##t FMT_##t FMT_##t FMT_##t, \
		      x.el[0], x.el[1], x.el[2], x.el[3], x.el[4], x.el[5], x.el[6], x.el[7]); \
	RUN_TEST(t, vectors_identical_8_##t(simdf(x), basef(x)));	\
    } while(0)




static void test_simd(void)
{
    printf("Testing the follow SIMD types: BYTE, UBYTE, SHORT, USHORT");
#ifdef simd_splat_ULONG
    printf(", LONG, ULONG");
#endif
#ifdef simd_splat_FLOAT
    printf(", FLOAT");
#endif
#ifdef simd_splat_DOUBLE
    printf(", DOUBLE");
#endif
    printf("\n");
    printf("Config reports your system as ");
#ifdef HAVE_LITTLE_ENDIAN
    printf("LITTLE ENDIAN\n");
#else
#  ifdef HAVE_BIG_ENDIAN
    printf("BIG ENDIAN\n");
#  else
    printf("... actually ENDIANESS not detected!!!\n"
	   "FIX AND RECOMPILE ME BEFORE I CAN PROCEED.\n");
    exit(1);
#  endif
#endif
}


static void test_simd_splat(void)
{
    START_TEST();

    TEST_IDENTICAL_I1(UBYTE, 0, simd_splat_UBYTE, vector_constant_UBYTE);
    TEST_IDENTICAL_I1(UBYTE, 1, simd_splat_UBYTE, vector_constant_UBYTE);
    TEST_IDENTICAL_I1(UBYTE, 255, simd_splat_UBYTE, vector_constant_UBYTE);
    TEST_IDENTICAL_I1(UBYTE, 162, simd_splat_UBYTE, vector_constant_UBYTE);

    TEST_IDENTICAL_I1(BYTE, 0, simd_splat_BYTE, vector_constant_BYTE);
    TEST_IDENTICAL_I1(BYTE, 1, simd_splat_BYTE, vector_constant_BYTE);
    TEST_IDENTICAL_I1(BYTE, INT8_MIN, simd_splat_BYTE, vector_constant_BYTE);
    TEST_IDENTICAL_I1(BYTE, -33, simd_splat_BYTE, vector_constant_BYTE);

    TEST_IDENTICAL_I1(USHORT, 0, simd_splat_USHORT, vector_constant_USHORT);
    TEST_IDENTICAL_I1(USHORT, 1, simd_splat_USHORT, vector_constant_USHORT);
    TEST_IDENTICAL_I1(USHORT, UINT16_MAX, simd_splat_USHORT, vector_constant_USHORT);
    TEST_IDENTICAL_I1(USHORT, 1621, simd_splat_USHORT, vector_constant_USHORT);

    TEST_IDENTICAL_I1(SHORT, 0, simd_splat_SHORT, vector_constant_SHORT);
    TEST_IDENTICAL_I1(SHORT, 1, simd_splat_SHORT, vector_constant_SHORT);
    TEST_IDENTICAL_I1(SHORT, -1, simd_splat_SHORT, vector_constant_SHORT);
    TEST_IDENTICAL_I1(SHORT, 2654, simd_splat_SHORT, vector_constant_SHORT);

#ifdef simd_splat_ULONG
    TEST_IDENTICAL_I1(ULONG, 0, simd_splat_ULONG, vector_constant_ULONG);
    TEST_IDENTICAL_I1(ULONG, 6453421, simd_splat_ULONG, vector_constant_ULONG);

    TEST_IDENTICAL_I1(LONG, 0, simd_splat_LONG, vector_constant_LONG);
    TEST_IDENTICAL_I1(LONG, -46352413, simd_splat_LONG, vector_constant_LONG);
#endif

#ifdef simd_splat_FLOAT
    TEST_IDENTICAL_R1(FLOAT, 0.0, simd_splat_FLOAT, vector_constant_FLOAT);
    TEST_IDENTICAL_R1(FLOAT, 654321.0, simd_splat_FLOAT, vector_constant_FLOAT);
#endif

#ifdef simd_splat_DOUBLE
    TEST_IDENTICAL_R1(DOUBLE, 0.0, simd_splat_DOUBLE, vector_constant_DOUBLE);
    TEST_IDENTICAL_R1(DOUBLE, 6543.21, simd_splat_DOUBLE, vector_constant_DOUBLE);
#endif

    END_TEST();
}


static void test_simd_splat_v0(void)
{
    vany a;

    START_TEST();

    a.ub = vector_set_element_UBYTE(vector_random_UBYTE(), 0, 0);
    TEST_IDENTICAL_S1_R(UBYTE, a.ub, simd_splat_v0_UBYTE, vector_constant_UBYTE(0));

    a.ub = vector_set_element_UBYTE(vector_random_UBYTE(), 0, 165);
    TEST_IDENTICAL_S1_R(UBYTE, a.ub, simd_splat_v0_UBYTE, vector_constant_UBYTE(165));

    a.b = vector_set_element_BYTE((simd_BYTE)vector_random_UBYTE(), 0, -100);
    TEST_IDENTICAL_S1_R(BYTE, a.b, simd_splat_v0_BYTE, vector_constant_BYTE(-100));

    a.us = vector_set_element_USHORT((simd_USHORT)vector_random_UBYTE(), 0, 1946);
    TEST_IDENTICAL_S1_R(USHORT, a.us, simd_splat_v0_USHORT, vector_constant_USHORT(1946));

    a.s = vector_set_element_SHORT((simd_SHORT)vector_random_UBYTE(), 0, -19241);
    TEST_IDENTICAL_S1_R(SHORT, a.s, simd_splat_v0_SHORT, vector_constant_SHORT(-19241));

#ifdef simd_splat_v0_ULONG
    a.ul = vector_set_element_ULONG((simd_ULONG)vector_random_UBYTE(), 0, 1946222);
    TEST_IDENTICAL_S1_R(ULONG, a.ul, simd_splat_v0_ULONG, vector_constant_ULONG(1946222));

    a.l = vector_set_element_LONG((simd_LONG)vector_random_UBYTE(), 0, -19241222);
    TEST_IDENTICAL_S1_R(LONG, a.l, simd_splat_v0_LONG, vector_constant_LONG(-19241222));
#endif

#ifdef simd_splat_v0_FLOAT
    a.f = vector_set_element_FLOAT((simd_FLOAT)vector_random_FLOAT(1e5), 0, 194.6222);
    TEST_IDENTICAL_S1_R(FLOAT, a.f, simd_splat_v0_FLOAT, vector_constant_FLOAT(194.6222));
#endif

#ifdef simd_splat_v0_DOUBLE
    a.d = vector_set_element_DOUBLE((simd_DOUBLE)vector_random_DOUBLE(2e5), 0, -194.6223);
    TEST_IDENTICAL_S1_R(DOUBLE, a.d, simd_splat_v0_DOUBLE, vector_constant_DOUBLE(-194.6223));
#endif

    END_TEST();
}


static void test_simd_splat_vN(void)
{
    vany a;

    START_TEST();

    a.ub = vector_set_element_UBYTE(vector_random_UBYTE(), 1, 0);
    TEST_IDENTICAL_S1I1_R(UBYTE, a.ub, 1, simd_splat_vN_UBYTE, vector_constant_UBYTE(0));

    a.ub = vector_set_element_UBYTE(vector_random_UBYTE(), 5, 165);
    TEST_IDENTICAL_S1I1_R(UBYTE, a.ub, 5, simd_splat_vN_UBYTE, vector_constant_UBYTE(165));

    if (SIMD_NUM_ELEMENTS(UBYTE) > 16) {
	a.ub = vector_set_element_UBYTE(vector_random_UBYTE(), 25, 201);
	TEST_IDENTICAL_S1I1_R(UBYTE, a.ub, 25, simd_splat_vN_UBYTE, vector_constant_UBYTE(201));
    }

    a.b = vector_set_element_BYTE(vector_random_BYTE(), 7, -3);
    TEST_IDENTICAL_S1I1_R(BYTE, a.b, 7, simd_splat_vN_BYTE, vector_constant_BYTE(-3));

    a.us = vector_set_element_USHORT(vector_random_USHORT(), 2, 1765);
    TEST_IDENTICAL_S1I1_R(USHORT, a.us, 2, simd_splat_vN_USHORT, vector_constant_USHORT(1765));

    a.us = vector_set_element_USHORT(vector_random_USHORT(), 3, 165);
    TEST_IDENTICAL_S1I1_R(USHORT, a.us, 3, simd_splat_vN_USHORT, vector_constant_USHORT(165));

    if (SIMD_NUM_ELEMENTS(USHORT) > 8) {
	a.us = vector_set_element_USHORT(vector_random_USHORT(), 12, 32910);
	TEST_IDENTICAL_S1I1_R(USHORT, a.us, 12, simd_splat_vN_USHORT, vector_constant_USHORT(32910));
    }

    a.s = vector_set_element_SHORT(vector_random_SHORT(), 2, -32000);
    TEST_IDENTICAL_S1I1_R(SHORT, a.s, 2, simd_splat_vN_SHORT, vector_constant_SHORT(-32000));

#ifdef simd_splat_vN_ULONG
    a.ul = vector_set_element_ULONG(vector_random_ULONG(), 1, 123765);
    TEST_IDENTICAL_S1I1_R(ULONG, a.ul, 1, simd_splat_vN_ULONG, vector_constant_ULONG(123765));

    if (SIMD_NUM_ELEMENTS(ULONG) > 2) {
	a.ul = vector_set_element_ULONG(vector_random_ULONG(), 3, 10132910);
	TEST_IDENTICAL_S1I1_R(ULONG, a.ul, 3, simd_splat_vN_ULONG, vector_constant_ULONG(10132910));
    }

    if (SIMD_NUM_ELEMENTS(ULONG) > 4) {
	a.ul = vector_set_element_ULONG(vector_random_ULONG(), 7, 110132910);
	TEST_IDENTICAL_S1I1_R(ULONG, a.ul, 7, simd_splat_vN_ULONG, vector_constant_ULONG(110132910));
    }

    a.l = vector_set_element_LONG(vector_random_LONG(), 1, -320000);
    TEST_IDENTICAL_S1I1_R(LONG, a.l, 1, simd_splat_vN_LONG, vector_constant_LONG(-320000));
#endif

#ifdef simd_splat_vN_FLOAT
    a.f = vector_set_element_FLOAT(vector_random_FLOAT(3e5), 2, -123.765);
    TEST_IDENTICAL_S1I1_R(FLOAT, a.f, 2, simd_splat_vN_FLOAT, vector_constant_FLOAT(-123.765));

    if (SIMD_NUM_ELEMENTS(FLOAT) > 4) {
	a.f = vector_set_element_FLOAT(vector_random_FLOAT(3e5), 6, 110132910.1);
	TEST_IDENTICAL_S1I1_R(FLOAT, a.f, 6, simd_splat_vN_FLOAT, vector_constant_FLOAT(110132910.1));
    }
#endif

#ifdef simd_splat_vN_DOUBLE
    a.d = vector_set_element_DOUBLE(vector_random_DOUBLE(4e5), 1, -123.7652);
    TEST_IDENTICAL_S1I1_R(DOUBLE, a.d, 1, simd_splat_vN_DOUBLE, vector_constant_DOUBLE(-123.7652));

    a.d = vector_set_element_DOUBLE(vector_random_DOUBLE(4e5), 0, 123.7652);
    TEST_IDENTICAL_S1I1_R(DOUBLE, a.d, 0, simd_splat_vN_DOUBLE, vector_constant_DOUBLE(123.7652));
#endif

    END_TEST();
}


static void test_simd_mask(void)
{
    START_TEST();

    TEST_IDENTICAL_I3(UBYTE, 23, 42, 201, simd_mask_RGB, vector_set_mask_RGB);
    TEST_IDENTICAL_I3(UBYTE, 211, 0, 123, simd_mask_RGB, vector_set_mask_RGB);
    TEST_IDENTICAL_I3(USHORT, 23, 42, 201, simd_mask_RGB16, vector_set_mask_RGB16);
    TEST_IDENTICAL_I3(USHORT, 211, 0, 123, simd_mask_RGB16, vector_set_mask_RGB16);

    TEST_IDENTICAL_I4(UBYTE, 23, 42, 201, 152, simd_mask_RGBA, vector_set_mask_RGBA);

    END_TEST();
}

#if 0
/*
 * The vector_ror_* test is broken because it is too strict a test.
 */
static simd_UBYTE vector_ror_RGB(simd_UBYTE x)
{
    vrep_UBYTE vx, vr;

    vx.v = x;
    for (int j=0; j<SIMD_NUM_ELEMENTS(UBYTE)-1; j++)
	vr.e[j] = vx.e[j+1];
    vr.e[SIMD_NUM_ELEMENTS(UBYTE)-1] = vx.e[1];

    return vr.v;
}

static void test_simd_ror(void)
{
    simd_UBYTE ub;

    START_TEST();

    ub = vector_set_incr_UBYTE(1, NULL);

    TEST_IDENTICAL_S1(UBYTE, ub, simd_ror_RGB, vector_ror_RGB);

    ub = vector_set_mask_RGB(101,202,33);
    TEST_IDENTICAL_S1(UBYTE, ub, simd_ror_RGB, vector_ror_RGB);

    END_TEST();
}
#endif


/*
 * Testing the simd_cvu like macros.
 *
 * Example shown for simd_cvu_imm_UBYTE(left, right, 3):
 *
 * On little endian for 64-bit, 8-byte, SIMD registers, memory that is
 * initialised with incrementing by one byte values is organised as:
 *
 * Address:        0x0000          0x0008
 * Our reg ID:     left            right
 * Mem as loaded: |0 1 2 3 4 5 6 7|8 9 a b c e d f| ...
 * but want this:       |3 4 5 6 7 8 9 a|
 *
 * so is (left<<pos) | (right>>(simd_num_elements - pos))
 * where pos = 3 and simd_num_elements = 8 in illustration above.
 *
 * But for big endian:
 *
 * Address:                 0x0000          0x0008
 * Our reg ID:     left            right
 * Mem as loaded: |7 6 5 4 3 2 1 0|f e d c b a 9 8| ...
 * but want this:           |a 9 8 7 6 5 4 3|
 *
 * so is (left>>pos) | (right<<(simd_num_elements - pos))
 *
 * (We are also assuming '>>' is *logical* shift, i.e., shifts in zeros.)
 *
 * We implement the above using memory stores and manipulating memory until
 * the unaligned data are shifted into an aligned position and then read
 * back the now aligned vector.  In this manner we do not need to use any
 * SIMD operation (such as logical shifts) other than loads and stores. Our
 * memory store for this operation is marked as volatile to disable compiler
 * optimisation that (I'm speculating here) might defeat our test.  It is
 * also correct for both endian conventions.
 */

/* Sufficient memory to play with. */
#define generate_vreptwo(tt)				\
    union vreptwo_ ## tt {				\
	simd_ ## tt v[2];				\
	tt ##  _type e[2*SIMD_NUM_ELEMENTS(tt)];	\
    };							\
    volatile union vreptwo_ ## tt cvu_play_mem_ ## tt;

/* Note that this only works for non-negative pos */
#define generate_vector_cvu(tt)						\
    static simd_ ## tt vector_cvu_ ## tt(simd_ ## tt left, simd_ ## tt right, int pos) \
    {									\
	cvu_play_mem_ ## tt.v[0] = left;				\
	cvu_play_mem_ ## tt.v[1] = right;				\
	for (int j=0; j<SIMD_NUM_ELEMENTS(tt); j++) {			\
	    cvu_play_mem_ ## tt.e[j] = cvu_play_mem_ ## tt.e[j+pos];	\
	}								\
	return cvu_play_mem_ ## tt.v[0];				\
    }

generate_vreptwo(UBYTE)
generate_vreptwo(BYTE)
generate_vreptwo(USHORT)
generate_vreptwo(SHORT)

generate_vector_cvu(UBYTE)
generate_vector_cvu(BYTE)
generate_vector_cvu(USHORT)
generate_vector_cvu(SHORT)

#if defined(simd_cvu_imm_ULONG) || defined(simd_cvu_ULONG)
generate_vreptwo(ULONG)
generate_vreptwo(LONG)
generate_vector_cvu(ULONG)
generate_vector_cvu(LONG)
#endif

#if defined(simd_cvu_imm_FLOAT) || defined(simd_cvu_FLOAT)
generate_vreptwo(FLOAT)
generate_vector_cvu(FLOAT)
#endif

#if defined(simd_cvu_imm_DOUBLE) || defined(simd_cvu_DOUBLE)
generate_vreptwo(DOUBLE)
generate_vector_cvu(DOUBLE)
#endif


static void test_simd_cvu_imm(void)
{
    int last;
    double lastf;
    vany l, r;

    START_TEST();

    l.ub = vector_set_incr_UBYTE(0, &last);
    r.ub = vector_set_incr_UBYTE(last, NULL );
    TEST_IDENTICAL_S2I1(UBYTE, l.ub, r.ub, 0, simd_cvu_imm_UBYTE, vector_cvu_UBYTE);
    TEST_IDENTICAL_S2I1(UBYTE, l.ub, r.ub, 6, simd_cvu_imm_UBYTE, vector_cvu_UBYTE);
    if (SIMD_NUM_ELEMENTS(UBYTE) > 8)
	TEST_IDENTICAL_S2I1(UBYTE, l.ub, r.ub, 13, simd_cvu_imm_UBYTE, vector_cvu_UBYTE);
    if (SIMD_NUM_ELEMENTS(UBYTE) > 16)
	TEST_IDENTICAL_S2I1(UBYTE, l.ub, r.ub, 23, simd_cvu_imm_UBYTE, vector_cvu_UBYTE);

    l.b = vector_set_incr_BYTE(-5, &last);
    r.b = vector_set_incr_BYTE(last, NULL );
    TEST_IDENTICAL_S2I1(BYTE, l.b, r.b, 0, simd_cvu_imm_BYTE, vector_cvu_BYTE);
    TEST_IDENTICAL_S2I1(BYTE, l.b, r.b, 2, simd_cvu_imm_BYTE, vector_cvu_BYTE);

    l.us = vector_set_incr_USHORT(10, &last);
    r.us = vector_set_incr_USHORT(last, NULL );
    TEST_IDENTICAL_S2I1(USHORT, l.us, r.us, 0, simd_cvu_imm_USHORT, vector_cvu_USHORT);
    TEST_IDENTICAL_S2I1(USHORT, l.us, r.us, 1, simd_cvu_imm_USHORT, vector_cvu_USHORT);
    if (SIMD_NUM_ELEMENTS(USHORT) > 4)
	TEST_IDENTICAL_S2I1(USHORT, l.us, r.us, 7, simd_cvu_imm_USHORT, vector_cvu_USHORT);
    if (SIMD_NUM_ELEMENTS(USHORT) > 8)
	TEST_IDENTICAL_S2I1(USHORT, l.us, r.us, 11, simd_cvu_imm_USHORT, vector_cvu_USHORT);

    l.s = vector_set_incr_SHORT(-1521, &last);
    r.s = vector_set_incr_SHORT(last, NULL );
    TEST_IDENTICAL_S2I1(SHORT, l.s, r.s, 0, simd_cvu_imm_SHORT, vector_cvu_SHORT);
    TEST_IDENTICAL_S2I1(SHORT, l.s, r.s, 3, simd_cvu_imm_SHORT, vector_cvu_SHORT);

#ifdef simd_cvu_imm_ULONG
    l.ul = vector_set_incr_ULONG(1056347, &last);
    r.ul = vector_set_incr_ULONG(last, NULL );
    TEST_IDENTICAL_S2I1(ULONG, l.ul, r.ul, 0, simd_cvu_imm_ULONG, vector_cvu_ULONG);
    TEST_IDENTICAL_S2I1(ULONG, l.ul, r.ul, 1, simd_cvu_imm_ULONG, vector_cvu_ULONG);

    l.l = vector_set_incr_LONG(1056347, &last);
    r.l = vector_set_incr_LONG(last, NULL );
    TEST_IDENTICAL_S2I1(LONG, l.l, r.l, 0, simd_cvu_imm_LONG, vector_cvu_LONG);
    TEST_IDENTICAL_S2I1(LONG, l.l, r.l, 1, simd_cvu_imm_LONG, vector_cvu_LONG);
#endif

#ifdef simd_cvu_imm_FLOAT
    l.f = vector_set_incr_FLOAT(1.51, &lastf);
    r.f = vector_set_incr_FLOAT(lastf, NULL );
    TEST_IDENTICAL_S2I1(FLOAT, l.f, r.f, 0, simd_cvu_imm_FLOAT, vector_cvu_FLOAT);
    TEST_IDENTICAL_S2I1(FLOAT, l.f, r.f, 1, simd_cvu_imm_FLOAT, vector_cvu_FLOAT);

    if (SIMD_NUM_ELEMENTS(FLOAT) > 4) {
	TEST_IDENTICAL_S2I1(FLOAT, l.f, r.f, 5, simd_cvu_imm_FLOAT, vector_cvu_FLOAT);
    }
#endif

#ifdef simd_cvu_imm_DOUBLE
    l.d = vector_set_incr_DOUBLE(-0.51, &lastf);
    r.d = vector_set_incr_DOUBLE(lastf, NULL );
    TEST_IDENTICAL_S2I1(DOUBLE, l.d, r.d, 0, simd_cvu_imm_DOUBLE, vector_cvu_DOUBLE);
    TEST_IDENTICAL_S2I1(DOUBLE, l.d, r.d, 1, simd_cvu_imm_DOUBLE, vector_cvu_DOUBLE);

    if (SIMD_NUM_ELEMENTS(DOUBLE) > 2) {
	TEST_IDENTICAL_S2I1(DOUBLE, l.d, r.d, 2, simd_cvu_imm_DOUBLE, vector_cvu_DOUBLE);
    }
#endif

    END_TEST();
}



static void test_simd_cvu(void)
{
    int last;
    vany l, r;

    START_TEST();

#ifdef simd_cvu_UBYTE
    l.ub = vector_set_incr_UBYTE(0, &last);
    r.ub = vector_set_incr_UBYTE(last, NULL );
    TEST_IDENTICAL(UBYTE, simd_cvu_UBYTE(l.ub, r.ub, 0), vector_cvu_UBYTE(l.ub, r.ub, 0));
    TEST_IDENTICAL(UBYTE, simd_cvu_UBYTE(l.ub, r.ub, last-3), vector_cvu_UBYTE(l.ub, r.ub, last-3));

    l.b = vector_set_incr_BYTE(-5, &last);
    r.b = vector_set_incr_BYTE(last, NULL );
    TEST_IDENTICAL(BYTE, simd_cvu_UBYTE(l.b, r.b, 0), vector_cvu_UBYTE(l.b, r.b, 0));
    TEST_IDENTICAL(BYTE, simd_cvu_UBYTE(l.b, r.b, 2), vector_cvu_UBYTE(l.b, r.b, last));
#endif

#ifdef simd_cvu_USHORT
    l.us = vector_set_incr_USHORT(10, &last);
    r.us = vector_set_incr_USHORT(last, NULL );
    TEST_IDENTICAL(USHORT, simd_cvu_USHORT(l.us, r.us, 0), vector_cvu_USHORT(l.us, r.us, 0));
    TEST_IDENTICAL(USHORT, simd_cvu_USHORT(l.us, r.us, 1), vector_cvu_USHORT(l.us, r.us, last-5));

    l.s = vector_set_incr_SHORT(-1521, &last);
    r.s = vector_set_incr_SHORT(last, NULL );
    TEST_IDENTICAL(SHORT, simd_cvu_SHORT(l.s, r.s, 0), vector_cvu_SHORT(l.s, r.s, 0));
    TEST_IDENTICAL(SHORT, simd_cvu_SHORT(l.s, r.s, 3), vector_cvu_SHORT(l.s, r.s, 3));
#endif

#ifdef simd_cvu_ULONG
    l.ul = vector_set_incr_ULONG(1056347, &last);
    r.ul = vector_set_incr_ULONG(last, NULL );
    TEST_IDENTICAL(ULONG, simd_cvu_ULONG(l.ul, r.ul, 0), vector_cvu_ULONG(l.ul, r.ul, 0));
    TEST_IDENTICAL(ULONG, simd_cvu_ULONG(l.ul, r.ul, 1), vector_cvu_ULONG(l.ul, r.ul, last-1056346));

    l.l = vector_set_incr_LONG(1056347, &last);
    r.l = vector_set_incr_LONG(last, NULL );
    TEST_IDENTICAL(LONG, simd_cvu_LONG(l.l, r.l, 0), vector_cvu_LONG(l.l, r.l, 0));
    TEST_IDENTICAL(LONG, simd_cvu_LONG(l.l, r.l, 1), vector_cvu_LONG(l.l, r.l, 1));
#endif

    END_TEST();
}


static void test_simd_ldu(void)
{
    vany mem[2];
    int last;

    START_TEST();

#ifdef simd_ldu_UBYTE
    mem[0].ub = vector_set_incr_UBYTE(0, &last);
    mem[1].ub = vector_set_incr_UBYTE(last, NULL);
    TEST_IDENTICAL(UBYTE, simd_ldu_UBYTE(((char *)mem) + (last-5)),
		   vector_cvu_UBYTE(mem[0].ub, mem[1].ub, last-5));
#endif

#ifdef simd_ldu_USHORT
    mem[0].us = vector_set_incr_USHORT(0, &last);
    mem[1].us = vector_set_incr_USHORT(last, NULL);
    TEST_IDENTICAL(USHORT, simd_ldu_USHORT(((char *)mem) + ((last-3)*sizeof(USHORT_type))),
		   vector_cvu_USHORT(mem[0].us, mem[1].us, last-3));
#endif

    END_TEST();
}


#define generate_mul(tt)						\
    static simd_ ## tt vector_mul_ ## tt(simd_##tt x, simd_##tt y)	\
    {									\
	vrep_##tt vr, vx, vy;						\
	vx.v = x;							\
	vy.v = y;							\
	for (int j=0; j<SIMD_NUM_ELEMENTS(tt); j++)			\
	    vr.e[j] = vx.e[j] * vy.e[j];				\
	return vr.v;							\
    }

generate_mul(UBYTE)
generate_mul(USHORT)
generate_mul(ULONG)
#ifdef simd_splat_FLOAT
generate_mul(FLOAT)
#endif
#ifdef simd_splat_DOUBLE
generate_mul(DOUBLE)
#endif

#ifdef simd_splat_COMPLEX
static simd_COMPLEX vector_mul_COMPLEX(simd_COMPLEX x, simd_COMPLEX y)
{
    vrep_FLOAT vr, vx, vy;
    vx.v = (simd_FLOAT)x;
    vy.v = (simd_FLOAT)y;
    for (int j=0; j<SIMD_NUM_ELEMENTS(FLOAT); j+=2) {
	ip_complex cx, cy, cz;

	cx = (ip_complex){vx.e[j], vx.e[j+1]};
	cy = (ip_complex){vy.e[j], vy.e[j+1]};
	cz = ip_complex_mul(cx, cy);
	vr.e[j] = cz.r;
	vr.e[j+1] = cz.i;
    }
    return vr.v;
}
#endif

#ifdef simd_splat_DCOMPLEX
static simd_DCOMPLEX vector_mul_DCOMPLEX(simd_DCOMPLEX x, simd_DCOMPLEX y)
{
    vrep_DOUBLE vr, vx, vy;
    vx.v = (simd_DOUBLE)x;
    vy.v = (simd_DOUBLE)y;
    for (int j=0; j<SIMD_NUM_ELEMENTS(DOUBLE); j+=2) {
	ip_dcomplex cx, cy, cz;

	cx = (ip_dcomplex){vx.e[j], vx.e[j+1]};
	cy = (ip_dcomplex){vy.e[j], vy.e[j+1]};
	cz = ip_dcomplex_mul(cx, cy);
	vr.e[j] = cz.r;
	vr.e[j+1] = cz.i;
    }
    return vr.v;
}
#endif

#define generate_mul_clipped_int(tt)					\
    static simd_ ## tt vector_mul_clipped_## tt(simd_##tt x, simd_##tt y)	\
    {									\
	vrep_##tt vr, vx, vy;						\
	vx.v = x;							\
	vy.v = y;							\
	for (int j=0; j<SIMD_NUM_ELEMENTS(tt); j++) {			\
	    int64_t res;						\
	    res = (int64_t)vx.e[j] * (int64_t)vy.e[j];			\
	    if (res > tt##_max) res = tt##_max;				\
	    if (res < tt##_min) res = tt##_min;				\
	    vr.e[j] = (tt##_type)res;					\
	}								\
	return vr.v;							\
    }

generate_mul_clipped_int(BYTE)
generate_mul_clipped_int(UBYTE)
generate_mul_clipped_int(SHORT)
generate_mul_clipped_int(USHORT)
#ifdef simd_splat_ULONG
generate_mul_clipped_int(LONG)
generate_mul_clipped_int(ULONG)
#endif

static void test_simd_mul(void)
{
    vany mem[2];

    START_TEST();

    mem[0].ub = vector_random_UBYTE();
    mem[1].ub = vector_random_UBYTE();
#ifdef simd_mul_UBYTE
    mem[0].ub = vector_set_element_UBYTE(mem[0].ub, 0, 1);
    mem[0].ub = vector_set_element_UBYTE(mem[0].ub, 1, 2);
    mem[0].ub = vector_set_element_UBYTE(mem[0].ub, 2, 3);
    TEST_IDENTICAL_S2(UBYTE, mem[0].ub, mem[1].ub, simd_mul_UBYTE, vector_mul_UBYTE);
#endif

#ifdef simd_mul_USHORT
    mem[0].us = vector_wrand_USHORT();
    mem[1].us = vector_wrand_USHORT();
    TEST_IDENTICAL_S2(USHORT, mem[0].us, mem[1].us, simd_mul_USHORT, vector_mul_USHORT);
#endif

#ifdef simd_mul_ULONG
    mem[0].ul = vector_wrand_ULONG();
    mem[1].ul = vector_wrand_ULONG();
    TEST_IDENTICAL_S2(ULONG, mem[0].ul, mem[1].ul, simd_mul_ULONG, vector_mul_ULONG);
#endif

#ifdef simd_mul_FLOAT
    mem[0].f = vector_random_FLOAT(1.5);
    mem[1].f = vector_random_FLOAT(1e4);

    TEST_IDENTICAL_S2(FLOAT, mem[0].f, mem[1].f, simd_mul_FLOAT, vector_mul_FLOAT);

    mem[0].f = vector_set_element_FLOAT(mem[0].f, 0, -1.0);
    mem[0].f = vector_set_element_FLOAT(mem[0].f, 1, 0.0);
    TEST_IDENTICAL_S2(FLOAT, mem[0].f, mem[1].f, simd_mul_FLOAT, vector_mul_FLOAT);

#ifdef simd_mul_COMPLEX
    TEST_IDENTICAL_S2(COMPLEX, mem[0].f, mem[1].f, simd_mul_COMPLEX, vector_mul_COMPLEX);
#endif
#endif

#ifdef simd_mul_DOUBLE
    mem[0].d = vector_random_DOUBLE(3.1);
    mem[1].d = vector_random_DOUBLE(1e65);

    TEST_IDENTICAL_S2(DOUBLE, mem[0].d, mem[1].d, simd_mul_DOUBLE, vector_mul_DOUBLE);

    mem[0].d = vector_set_element_DOUBLE(mem[0].d, 0, -1.0);
    mem[0].d = vector_set_element_DOUBLE(mem[0].d, 1, 0.0);
    TEST_IDENTICAL_S2(DOUBLE, mem[0].d, mem[1].d, simd_mul_DOUBLE, vector_mul_DOUBLE);

#ifdef simd_mul_DCOMPLEX
    mem[0].d = vector_random_DOUBLE(1e3);
    mem[1].d = vector_random_DOUBLE(1e4);
    TEST_IDENTICAL_S2(DCOMPLEX, mem[0].d, mem[1].d, simd_mul_DCOMPLEX, vector_mul_DCOMPLEX);
#endif
#endif

    END_TEST();
}


 static void test_simd_mul_clipped(void)
{
    vany mem[2];

    START_TEST();

    mem[0].ub = vector_random_UBYTE();
    mem[1].ub = vector_random_UBYTE();

#ifdef simd_mul_clipped_BYTE
    mem[0].ub = vector_set_element_UBYTE(mem[0].ub, 0, 1);
    mem[0].ub = vector_set_element_UBYTE(mem[0].ub, 1, 2);
    mem[0].ub = vector_set_element_UBYTE(mem[0].ub, 2, -1);
    TEST_IDENTICAL_S2(BYTE, mem[0].b, mem[1].b, simd_mul_clipped_BYTE, vector_mul_clipped_BYTE);
#endif

#ifdef simd_mul_clipped_UBYTE
    TEST_IDENTICAL_S2(UBYTE, mem[0].ub, mem[1].ub, simd_mul_clipped_UBYTE, vector_mul_clipped_UBYTE);
#endif

#ifdef simd_mul_clipped_SHORT
    mem[0].s = vector_wrand_SHORT();
    mem[1].s = vector_wrand_SHORT();
    ensure_some_bytes_USHORT(&mem[0].us);
    ensure_some_bytes_USHORT(&mem[1].us);
    TEST_IDENTICAL_S2(SHORT, mem[0].s, mem[1].s, simd_mul_clipped_SHORT, vector_mul_clipped_SHORT);
    mem[0].s = vector_wrand_SHORT();
    mem[1].s = vector_wrand_SHORT();
    mem[0].us = vector_set_element_USHORT(mem[0].us, 0, 0);
    mem[1].us = vector_set_element_USHORT(mem[1].us, 0, INT16_MAX);
    mem[0].us = vector_set_element_USHORT(mem[0].us, 1, 2);
    mem[1].us = vector_set_element_USHORT(mem[1].us, 1, INT16_MAX);
    mem[0].us = vector_set_element_USHORT(mem[0].us, 2, 1);
    mem[1].us = vector_set_element_USHORT(mem[1].us, 2, INT16_MAX);
    mem[0].us = vector_set_element_USHORT(mem[0].us, 3, -1);
    mem[1].us = vector_set_element_USHORT(mem[1].us, 3, INT16_MAX);
    TEST_IDENTICAL_S2(SHORT, mem[0].s, mem[1].s, simd_mul_clipped_SHORT, vector_mul_clipped_SHORT);
    mem[0].s = vector_wrand_SHORT();
    mem[1].s = vector_wrand_SHORT();
    mem[0].us = vector_set_element_USHORT(mem[0].us, 0, INT16_MIN);
    mem[1].us = vector_set_element_USHORT(mem[1].us, 0, INT16_MAX);
    mem[0].us = vector_set_element_USHORT(mem[0].us, 1, -2);
    mem[1].us = vector_set_element_USHORT(mem[1].us, 1, INT16_MAX);
    mem[0].us = vector_set_element_USHORT(mem[0].us, 2, INT16_MAX);
    mem[1].us = vector_set_element_USHORT(mem[1].us, 2, INT16_MAX);
    mem[0].us = vector_set_element_USHORT(mem[0].us, 3, INT16_MIN);
    mem[1].us = vector_set_element_USHORT(mem[1].us, 3, INT16_MIN);
    TEST_IDENTICAL_S2(SHORT, mem[0].s, mem[1].s, simd_mul_clipped_SHORT, vector_mul_clipped_SHORT);
#endif

#ifdef simd_mul_clipped_USHORT
    mem[0].us = vector_wrand_USHORT();
    mem[1].us = vector_wrand_USHORT();
    mem[0].us = vector_set_element_USHORT(mem[0].us, 0, 0);
    mem[1].us = vector_set_element_USHORT(mem[1].us, 0, INT16_MAX);
    mem[0].us = vector_set_element_USHORT(mem[0].us, 1, 2);
    mem[1].us = vector_set_element_USHORT(mem[1].us, 1, INT16_MAX);
    TEST_IDENTICAL_S2(USHORT, mem[0].us, mem[1].us, simd_mul_clipped_USHORT, vector_mul_clipped_USHORT);
    mem[0].us = vector_wrand_USHORT();
    mem[1].us = vector_wrand_USHORT();
    ensure_some_bytes_USHORT(&mem[0].us);
    ensure_some_bytes_USHORT(&mem[1].us);
    TEST_IDENTICAL_S2(USHORT, mem[0].us, mem[1].us, simd_mul_clipped_USHORT, vector_mul_clipped_USHORT);
    mem[0].us = vector_wrand_USHORT();
    mem[1].us = vector_wrand_USHORT();
    mem[0].us = vector_set_element_USHORT(mem[0].us, 0, 2046);
    mem[1].us = vector_set_element_USHORT(mem[1].us, 0, 21);
    mem[0].us = vector_set_element_USHORT(mem[0].us, 1, 2);
    mem[1].us = vector_set_element_USHORT(mem[1].us, 1, 32768);
    TEST_IDENTICAL_S2(USHORT, mem[0].us, mem[1].us, simd_mul_clipped_USHORT, vector_mul_clipped_USHORT);
#endif

    END_TEST();
}



/*
 * simd_mulhi
 *
 * For N-bit arithmetic calculate the 2N-bit result of mulitplying two N-bit
 * numbers and return the high N-bits of the result.
 */

#define generate_mulhi(tt, ttbig, shift)			       \
    static simd_##tt vector_mulhi_##tt(simd_##tt x, simd_##tt y)       \
    {                                                                  \
	vrep_##tt vr, vx, vy;					       \
	vx.v = x; vy.v = y;					       \
	for (int j=0; j<SIMD_NUM_ELEMENTS(tt); j++) {		       \
	    ttbig res;						       \
	    res = (ttbig)vx.e[j] * (ttbig)vy.e[j];		       \
	    vr.e[j] = (tt##_type)(res >> shift);		       \
	}							       \
	return vr.v;						       \
    }

generate_mulhi(SHORT, int32_t, 16)
generate_mulhi(USHORT, uint32_t, 16)
generate_mulhi(LONG, int64_t, 32)
generate_mulhi(ULONG, uint64_t, 32)


static void test_simd_mulhi(void)
{
    vany mem[2];

    START_TEST();

    mem[0].ub = vector_random_UBYTE();

#ifdef simd_mulhi_USHORT
    mem[1].us = vector_wrand_USHORT();
    TEST_IDENTICAL_S2(USHORT, mem[0].us, mem[1].us, simd_mulhi_USHORT, vector_mulhi_USHORT);
#endif

#ifdef simd_mulhi_SHORT
    mem[1].s = vector_wrand_SHORT();
    TEST_IDENTICAL_S2(SHORT, mem[0].s, mem[1].s, simd_mulhi_SHORT, vector_mulhi_SHORT);
#endif

#ifdef simd_mulhi_ULONG
    mem[1].ul = vector_wrand_ULONG();
    TEST_IDENTICAL_S2(ULONG, mem[0].ul, mem[1].ul, simd_mulhi_ULONG, vector_mulhi_ULONG);
#endif

#ifdef simd_mulhi_LONG
    mem[1].l = vector_wrand_LONG();
    TEST_IDENTICAL_S2(LONG, mem[0].l, mem[1].l, simd_mulhi_LONG, vector_mulhi_LONG);
#endif

    END_TEST();
}


/*
 * simd_fmaddhi
 *
 * Integer fused multiply-add: For N-bit inputs calculate the 2N-bit result
 * of multiplying two N-bit numbers and adding in the third N-bit number
 * while maintaining the 2N-bit precision.  Return the high N-bits of the
 * result.
 */

#define generate_fmaddhi(tt, ttbig, shift)			       \
    static simd_##tt vector_fmaddhi_##tt(simd_##tt x, simd_##tt y, simd_##tt c) \
    {                                                                  \
	vrep_##tt vr, vx, vy, vc;				       \
	vx.v = x; vy.v = y; vc.v = c;				       \
	for (int j=0; j<SIMD_NUM_ELEMENTS(tt); j++) {		       \
	    ttbig res;						       \
	    res = (ttbig)vx.e[j] * (ttbig)vy.e[j] + (ttbig)vc.e[j];    \
	    vr.e[j] = (tt##_type)(res >> shift);		       \
	}							       \
	return vr.v;						       \
    }

generate_fmaddhi(SHORT, int32_t, 16)
generate_fmaddhi(USHORT, uint32_t, 16)
generate_fmaddhi(LONG, int64_t, 32)
generate_fmaddhi(ULONG, uint64_t, 32)


static void test_simd_fmaddhi(void)
{
    vany mem[3];

    START_TEST();

    mem[0].ub = vector_random_UBYTE();
    mem[2].ub = vector_random_UBYTE();

#ifdef simd_fmaddhi_USHORT
    mem[1].us = vector_wrand_USHORT();
    TEST_IDENTICAL_S3(USHORT, mem[0].us, mem[1].us, mem[2].us, simd_fmaddhi_USHORT, vector_fmaddhi_USHORT);
#endif

#ifdef simd_fmaddhi_SHORT
    mem[1].s = vector_wrand_SHORT();
    TEST_IDENTICAL_S3(SHORT, mem[0].s, mem[1].s, mem[2].s, simd_fmaddhi_SHORT, vector_fmaddhi_SHORT);
#endif

#ifdef simd_fmaddhi_ULONG
    mem[1].ul = vector_wrand_ULONG();
    mem[0].ul = vector_set_element_ULONG(mem[0].ul, 0, 0x73215);
    mem[1].ul = vector_set_element_ULONG(mem[1].ul, 0, 0x508273);
    mem[2].ul = vector_set_element_ULONG(mem[2].ul, 0, 0xffff0000);

    TEST_IDENTICAL_S3(ULONG, mem[0].ul, mem[1].ul, mem[2].ul, simd_fmaddhi_ULONG, vector_fmaddhi_ULONG);
#endif

#ifdef simd_fmaddhi_LONG
    mem[1].l = vector_wrand_LONG();
    TEST_IDENTICAL_S3(LONG, mem[0].l, mem[1].l, mem[2].l, simd_fmaddhi_LONG, vector_fmaddhi_LONG);
#endif

    END_TEST();
}


static simd_USHORT vector_imulf16_USHORT(simd_USHORT d, simd_USHORT cw, simd_USHORT cf)
{
    vrep_USHORT vd, vcw, vcf, res;
    vd.v = d;
    vcw.v = cw;
    vcf.v = cf;

    for (int j=0; j<SIMD_NUM_ELEMENTS(USHORT); j++) {
	uint32_t c = (((uint32_t)vcw.e[j])<<16) + (uint32_t)vcf.e[j];
	uint32_t umula = (uint32_t)vd.e[j] * c + (1<<15);
	res.e[j] = (uint16_t)(umula >> 16);
    }
    return res.v;
}


static simd_SHORT vector_imulf16_SHORT(simd_SHORT d, simd_SHORT cw, simd_USHORT cf)
{
    vrep_SHORT vd, vcw, res;
    vrep_USHORT vcf;
    vd.v = d;
    vcw.v = cw;
    vcf.v = cf;

    for (int j=0; j<SIMD_NUM_ELEMENTS(SHORT); j++) {
	int32_t ci = (vcw.e[j]<<16) + vcf.e[j];
	double c = (double)ci / pow(2, 16);
	res.e[j] = (int32_t)round((double)vd.e[j] * c);
    }
    return res.v;
}


static simd_ULONG vector_imulf32_ULONG(simd_ULONG d, simd_ULONG cw, simd_ULONG cf)
{
    vrep_ULONG vd, vcw, vcf, res;
    vd.v = d;
    vcw.v = cw;
    vcf.v = cf;

    for (int j=0; j<SIMD_NUM_ELEMENTS(ULONG); j++) {
	uint64_t c = (((uint64_t)vcw.e[j])<<32) + (uint64_t)vcf.e[j];
	uint64_t umula = (uint64_t)vd.e[j] * c + (1ULL<<31);
	res.e[j] = (uint32_t)(umula >> 32);
    }
    return res.v;
}


static simd_LONG vector_imulf32_LONG(simd_LONG d, simd_LONG cw, simd_ULONG cf)
{
    vrep_LONG vd, res, vcw;
    vrep_ULONG vcf;
    vd.v = d;
    vcw.v = cw;
    vcf.v = cf;

    for (int j=0; j<SIMD_NUM_ELEMENTS(LONG); j++) {
	uint64_t c = (((uint64_t)vcw.e[j])<<32) + (uint64_t)vcf.e[j];
	int64_t smul = (int64_t)((uint64_t)vd.e[j] * c);
	int64_t rounder = (smul >=0) ? (1ULL<<31) : ((1ULL<<31)-1);
	res.e[j] = (int32_t)(uint32_t)((smul + rounder) >> 32);
    }
    return res.v;
}




static void test_simd_imulfN(void)
{
    static const uint16_t cw16[] = {0,      0,      1,      1,
				    1,      1,      2,      2,
				    2,      2,      3,      9,
				    11,     32,     543,    8765
    };
    static const uint16_t cf16[] = {0x8000, 0xfe43, 0x0000, 0x0321,
				    0x8000, 0x9a52, 0x00f1, 0x7fff,
				    0x8000, 0xefa9, 0x6a41, 0x4af3,
				    0x1000, 0xd7a2, 0x0000, 0xcf5a
    };
    static const uint16_t cf32l[] = {0x0000, 0xa4e3, 0x0000, 0x3210,
				     0x0000, 0x1234, 0x9876, 0xff7f,
				     0x0000, 0x4321, 0xa73b, 0x4af3,
				     0x1001, 0xf346, 0x0000, 0xf71a
    };
    vany a, cw, cf;

    START_TEST();

    /*
     * SHORT and USHORT are required to be calculated to the precision of an
     * int (i.e. 32bits) so can inialise multicand (a) with any valid value.
     */

#ifdef simd_imulf16_USHORT
    for (int j=0; j<16; j++) {
	a.us = vector_wrand_USHORT();
	cw.us = vector_constant_USHORT(cw16[j]);
	cf.us = vector_constant_USHORT(cf16[j]);
	TEST_IDENTICAL_S3(USHORT, a.us, cw.us, cf.us, simd_imulf16_USHORT, vector_imulf16_USHORT);
    }

    cw.us = vector_wrand_USHORT();
    cf.us = vector_random_USHORT();
    TEST_IDENTICAL_S3(USHORT, a.us, cw.us, cf.us, simd_imulf16_USHORT, vector_imulf16_USHORT);
#endif

#ifdef simd_imulf16_SHORT
    for (int j=0; j<16; j++) {
	a.s = vector_wrand_SHORT();
	cw.s = vector_constant_SHORT(cw16[j]);
	cf.s = vector_constant_SHORT(cf16[j]);
	TEST_IDENTICAL_S3(SHORT, a.s, cw.s, cf.us, simd_imulf16_SHORT, vector_imulf16_SHORT);
	cw.s = vector_constant_SHORT(cw16[j]>0 ? -cw16[j] : -1);
	TEST_IDENTICAL_S3(SHORT, a.s, cw.s, cf.us, simd_imulf16_SHORT, vector_imulf16_SHORT);
    }

    cw.s = vector_wrand_SHORT();
    cf.s = vector_random_SHORT();
    TEST_IDENTICAL_S3(SHORT, a.s, cw.s, cf.us, simd_imulf16_SHORT, vector_imulf16_SHORT);
#endif

    /*
     * LONG and ULONG are also only required to be calculated with the
     * precision of 32-bits which means that they could overflow.  In the
     * case of ULONG that should not be a problem because there is only one
     * rounding direction, but for LONG that can be a problem because the
     * rounding direction arguably depends on the wrapped result (not solely
     * the sign of the arguments) therefore we do not guarantee correct
     * rounding if LONG overflows (and wraps), thus we initialise LONG with
     * a multiplicand that cannot overflow given its multiplier in the tests
     * below.
     */

#ifdef simd_imulf32_ULONG
    for (int j=0; j<16; j++) {
	a.ul = vector_wrand_ULONG();
	cw.ul = vector_constant_ULONG(cw16[j]);
	cf.ul = vector_constant_ULONG((int)cf16[j]<<16 | cf32l[j]);
	TEST_IDENTICAL_S3(ULONG, a.ul, cw.ul, cf.ul, simd_imulf32_ULONG, vector_imulf32_ULONG);
    }

    cw.ul = vector_wrand_ULONG();
    cf.ul = vector_random_ULONG();
    TEST_IDENTICAL_S3(ULONG, a.ul, cw.ul, cf.ul, simd_imulf32_ULONG, vector_imulf32_ULONG);
#endif

#ifdef simd_imulf32_LONG
    for (int j=0; j<16; j++) {
	/* Initialise a with values that cannot overflow result */
	a.l = vector_wrandlim_LONG( INT32_MAX / (cw16[j] ? cw16[j] : 1));
	cw.l = vector_constant_LONG(cw16[j]);
	cf.l = vector_constant_LONG((int)cf16[j]<<16 | cf32l[j]);
	TEST_IDENTICAL_S3(LONG, a.l, cw.l, cf.ul, simd_imulf32_LONG, vector_imulf32_LONG);
	cw.l = vector_constant_LONG(cw16[j]>0 ? -cw16[j] : -1);
	TEST_IDENTICAL_S3(LONG, a.l, cw.l, cf.ul, simd_imulf32_LONG, vector_imulf32_LONG);
    }

    cw.l = vector_wrand_LONG();
    cf.l = vector_random_LONG();
    TEST_IDENTICAL_S3(LONG, a.l, cw.l, cf.ul, simd_imulf32_LONG, vector_imulf32_LONG);
#endif

    END_TEST();
}


static simd_USHORT vector_idivf16_USHORT(simd_USHORT d,
					 simd_USHORT cf, simd_USHORT shift)
{
    vrep_USHORT vd, vcf, vshift, res;
    vd.v = d;
    vcf.v = cf;
    vshift.v = shift;

    for (int j=0; j<SIMD_NUM_ELEMENTS(USHORT); j++) {
	uint32_t c = (uint32_t)vcf.e[j];
	uint32_t umula = (uint32_t)vd.e[j] * c + ((1<<15)<<vshift.e[j]);
	res.e[j] = (uint16_t)((umula >> 16) >> vshift.e[j]);
    }
    return res.v;
}


static simd_SHORT vector_idivf16_SHORT(simd_SHORT d,
				       simd_SHORT cf, simd_USHORT shift)
{
    vrep_USHORT vshift;
    vrep_SHORT vd, vcf, res;
    vd.v = d;
    vcf.v = cf;
    vshift.v = shift;

    for (int j=0; j<SIMD_NUM_ELEMENTS(USHORT); j++) {
	int32_t ci = vcf.e[j];
	double c = (double)ci / pow(2, 16+vshift.e[j]);
	res.e[j] = (int32_t)round((double)vd.e[j] * c);
    }
    return res.v;
}


static simd_ULONG vector_idivf32_ULONG(simd_ULONG d,
				       simd_ULONG cf, simd_ULONG shift)
{
    vrep_ULONG vd, vcf, vshift, res;
    vd.v = d;
    vcf.v = cf;
    vshift.v = shift;

    for (int j=0; j<SIMD_NUM_ELEMENTS(ULONG); j++) {
	uint64_t c = (uint64_t)vcf.e[j];
	uint64_t umula = (uint64_t)vd.e[j] * c + ((1ULL<<31)<<vshift.e[j]);
	res.e[j] = (uint32_t)((umula >> 32) >> vshift.e[j]);
    }
    return res.v;
}

static simd_LONG vector_idivf32_LONG(simd_LONG d,
				     simd_LONG cf, simd_ULONG shift)
{
    vrep_ULONG vshift;
    vrep_LONG vd, vcf, res;
    vd.v = d;
    vcf.v = cf;
    vshift.v = shift;

    for (int j=0; j<SIMD_NUM_ELEMENTS(LONG); j++) {
	int64_t c = vcf.e[j];
	int64_t smul = vd.e[j] * c;
	int64_t rounder = (smul >=0) ? (1LL<<31) : ((1LL<<31)-1);
	res.e[j] = (int32_t)(((smul+(rounder<<vshift.e[j])) >> 32) >> vshift.e[j]);
    }
    return res.v;
}



static void test_simd_idivfN(void)
{
    vany mem[4];

    START_TEST();

    mem[0].ub = vector_random_UBYTE(); /* dividend */
    mem[2].ub = vector_random_UBYTE(); /* divisor represents number in [0.5, 1.0) */

#ifdef simd_idivf32_ULONG
    mem[3].ul = vector_set_incr_ULONG(1, NULL); /* Shift; divides divisor by 2^shift */
    TEST_IDENTICAL_S3(ULONG, mem[0].ul, mem[2].ul, mem[3].ul, simd_idivf32_ULONG, vector_idivf32_ULONG);
#endif

#ifdef simd_idivf16_USHORT
    mem[3].us = vector_set_incr_USHORT(0, NULL); /* Shift; divides divisor by 2^shift */
    mem[3].us = vector_set_element_USHORT(mem[3].us, 0, 1);
    TEST_IDENTICAL_S3(USHORT, mem[0].us, mem[2].us, mem[3].us, simd_idivf16_USHORT, vector_idivf16_USHORT);
#endif

#ifdef simd_idivf16_SHORT
    mem[0].s = vector_set_element_SHORT(mem[0].s, 0, -105);
    mem[2].s = vector_set_element_SHORT(mem[2].s, 0, 19661);
    mem[3].s = vector_set_element_SHORT(mem[3].s, 0, 0);
    TEST_IDENTICAL_S3(SHORT, mem[0].s, mem[2].s, mem[3].us, simd_idivf16_SHORT, vector_idivf16_SHORT);
#endif

#ifdef simd_idivf32_LONG
    mem[0].l = vector_set_element_LONG(mem[0].l, 1, 0x456789ab);
    mem[3].ul = vector_set_incr_ULONG(1, NULL); /* Shift; divides divisor by 2^shift */
    TEST_IDENTICAL_S3(LONG, mem[0].l, mem[2].l, mem[3].ul, simd_idivf32_LONG, vector_idivf32_LONG);
#endif

    END_TEST();
}


/*
 * simd_idiv
 *
 * Vector integer division; both dividend and divisor vectors.
 * Currently only test the simd_rcpc_*() functionality for creating
 * reciprocal vectors, thus can only divide by a common divisor.
 */

static simd_USHORT vector_idiv_USHORT(simd_USHORT n, simd_USHORT d)
{
    vrep_USHORT vn, vd, res;
    vn.v = n;
    vd.v = d;

    for (int j=0; j<SIMD_NUM_ELEMENTS(USHORT); j++) {
       res.e[j] = vn.e[j] / vd.e[j];
    }
    return res.v;
}

static simd_SHORT vector_idiv_SHORT(simd_SHORT n, simd_SHORT d)
{
    vrep_SHORT vn, vd, res;
    vn.v = n;
    vd.v = d;

    for (int j=0; j<SIMD_NUM_ELEMENTS(SHORT); j++) {
       res.e[j] = vn.e[j] / vd.e[j];
    }
    return res.v;
}

static simd_ULONG vector_idiv_ULONG(simd_ULONG n, simd_ULONG d)
{
    vrep_ULONG vn, vd, res;
    vn.v = n;
    vd.v = d;

    for (int j=0; j<SIMD_NUM_ELEMENTS(ULONG); j++) {
       res.e[j] = vn.e[j] / vd.e[j];
    }
    return res.v;
}

static simd_LONG vector_idiv_LONG(simd_LONG n, simd_LONG d)
{
    vrep_LONG vn, vd, res;
    vn.v = n;
    vd.v = d;

    for (int j=0; j<SIMD_NUM_ELEMENTS(LONG); j++) {
       res.e[j] = vn.e[j] / vd.e[j];
    }
    return res.v;
}


#define TEST_IDENTICAL_IDIV_RCPC(tt,x,c)				\
    {									\
	print_initial(#tt, "simd_idiv_" #tt, FMT_##tt "i", x, c);	\
	RUN_TEST(tt, vectors_identical_## tt(simd_idiv_##tt(x, simd_rcpc_##tt(c)),\
					     vector_idiv_##tt(x, simd_splat_##tt(c))));	\
    }


static void test_simd_idiv(void)
{
#define NUM_SIGNED_TESTS 10
#define NUM_UNSIGNED_TESTS 8
    int signed_test_values[NUM_SIGNED_TESTS] =
	{-999, -32, -7, -3, 1, 2, 7, 8, 31, 1648};
    unsigned int unsigned_test_values[NUM_UNSIGNED_TESTS] =
	{1, 2, 7, 8, 31, 128, 1648, 26453};
    vany u;
    int bad_ushort_1 = 0;
    int bad_ulong_1 = 0;

    START_TEST();

#ifdef simd_idiv_USHORT
    for (int j=0; j<NUM_UNSIGNED_TESTS; j++) {
	/*
	 * We permit divisor of 1 to fail as it allows an optimisation in
	 * the unsigned division implementation, BUT warn on the failure so
	 * that one does not lose sight of it.
	 */
	if (unsigned_test_values[j] == 1)
	    expect_failure = 1;
	u.us = vector_wrand_USHORT();
	TEST_IDENTICAL_IDIV_RCPC(USHORT, u.us, unsigned_test_values[j]);
	if (unsigned_test_values[j] == 1) {
	    expect_failure = 0;
	    if (ok == 2) {
		bad_ushort_1 = 1;
	    }
	}
    }
#endif

#ifdef simd_idiv_SHORT
    for (int j=0; j<NUM_SIGNED_TESTS; j++) {
	u.s = vector_wrand_SHORT();
	TEST_IDENTICAL_IDIV_RCPC(SHORT, u.s, signed_test_values[j]);
    };
#endif

#ifdef simd_idiv_ULONG
    int old_ok = ok;
    ok = 1;
    for (int j=0; j<NUM_UNSIGNED_TESTS; j++) {
	/* See comment regarding divisor of 1 above */
	if (unsigned_test_values[j] == 1)
	    expect_failure = 1;
	u.ul = vector_wrand_ULONG();
	TEST_IDENTICAL_IDIV_RCPC(ULONG, u.ul, unsigned_test_values[j]);
	if (unsigned_test_values[j] == 1) {
	    expect_failure = 0;
	    if (ok == 2) {
		bad_ulong_1 = 1;
	    }
	}
    }
    if (ok && old_ok != 1)
	ok = old_ok;
#endif

#ifdef simd_idiv_LONG
    for (int j=0; j<NUM_SIGNED_TESTS; j++) {
	u.l = vector_wrand_LONG();
	TEST_IDENTICAL_IDIV_RCPC(LONG, u.l, signed_test_values[j]);
    }
#endif

    if (bad_ushort_1 || bad_ulong_1) {
	if (bad_ushort_1 && bad_ulong_1)
	    REGISTER_NOTE("idiv USHORT/ULONG both do not support divisor of 1.");
	else if (bad_ushort_1)
	    REGISTER_NOTE("idiv USHORT does not support divisor of 1.");
	else
	    REGISTER_NOTE("idiv ULONG does not support divisor of 1.");
    }

    END_TEST();
}

/*
 * simd_replicate_right()
 *
 * Replicates the x-th element of the vector to all elements to the right of
 * the x-th element, where by right we mean in increasing memory order, thus
 * "right" along an image row.
 *
 * Little endian:
 *
 * input:   |0 1 2 3 4 5 6 7|
 * output:  |0 1 2 3 4 5 5 5|
 *
 * Big endian:
 *
 * input:   |7 6 5 4 3 2 1 0|
 * output:  |5 5 5 4 3 2 1 0|
 */

#define generate_replicate_right(tt)					\
    static simd_ ## tt vector_replicate_right_ ## tt(simd_ ## tt x, int pos) \
    {									\
	vrep_ ## tt v;							\
	assert(pos >= 0 && pos < SIMD_NUM_ELEMENTS(tt));		\
	v.v = x;							\
	for (int j=pos+1; j<SIMD_NUM_ELEMENTS(tt); j++)			\
	    v.e[j] = v.e[pos];						\
	return v.v;							\
    }

#define generate_replicate_left(tt)					\
    static simd_ ## tt vector_replicate_left_ ## tt(simd_ ## tt x, int pos) \
    {									\
	vrep_ ## tt v;							\
	assert(pos >= 0 && pos < SIMD_NUM_ELEMENTS(tt));		\
	v.v = x;							\
	for (int j=0; j<pos; j++)					\
	    v.e[j] = v.e[pos];						\
	return v.v;							\
    }

generate_replicate_right(UBYTE)
generate_replicate_right(BYTE)
generate_replicate_right(USHORT)
generate_replicate_right(SHORT)
#ifdef simd_replicate_right_ULONG
generate_replicate_right(ULONG)
generate_replicate_right(LONG)
#endif
#ifdef simd_replicate_right_FLOAT
generate_replicate_right(FLOAT)
#endif
#ifdef simd_replicate_right_DOUBLE
generate_replicate_right(DOUBLE)
#endif

generate_replicate_left(UBYTE)
generate_replicate_left(BYTE)
generate_replicate_left(USHORT)
generate_replicate_left(SHORT)
#ifdef simd_replicate_right_ULONG
generate_replicate_left(ULONG)
generate_replicate_left(LONG)
#endif
#ifdef simd_replicate_right_FLOAT
generate_replicate_left(FLOAT)
#endif
#ifdef simd_replicate_right_DOUBLE
generate_replicate_left(DOUBLE)
#endif


static void test_simd_replicate(void)
{
    vany v;
    int rand_pos = rand() & 0x07; /* Provide a position arg only known at run time */

    START_TEST();

    v.ub = vector_random_UBYTE();
    TEST_IDENTICAL_S1I1(UBYTE, v.ub, 1, simd_replicate_right_UBYTE, vector_replicate_right_UBYTE);
    TEST_IDENTICAL_S1I1(UBYTE, v.ub, 3, simd_replicate_left_UBYTE, vector_replicate_left_UBYTE);
    TEST_IDENTICAL_S1I1(UBYTE, v.ub, rand_pos, simd_replicate_left_UBYTE, vector_replicate_left_UBYTE);
    TEST_IDENTICAL_S1I1(BYTE, v.b, rand_pos, simd_replicate_right_BYTE, vector_replicate_right_BYTE);
    TEST_IDENTICAL_S1I1(BYTE, v.b, 5, simd_replicate_left_BYTE, vector_replicate_left_BYTE);

    if (SIMD_NUM_ELEMENTS(UBYTE) > 16) {
	TEST_IDENTICAL_S1I1(UBYTE, v.ub, 27, simd_replicate_right_UBYTE, vector_replicate_right_UBYTE);
	TEST_IDENTICAL_S1I1(UBYTE, v.ub, 21, simd_replicate_left_UBYTE, vector_replicate_left_UBYTE);
    }

    rand_pos &= 0x02;
    v.us = (simd_USHORT)vector_random_UBYTE();
    TEST_IDENTICAL_S1I1(USHORT, v.us, 3, simd_replicate_right_USHORT, vector_replicate_right_USHORT);
    TEST_IDENTICAL_S1I1(USHORT, v.us, 2, simd_replicate_left_USHORT, vector_replicate_left_USHORT);
    TEST_IDENTICAL_S1I1(USHORT, v.us, rand_pos, simd_replicate_left_USHORT, vector_replicate_left_USHORT);
    TEST_IDENTICAL_S1I1(SHORT, v.s, 1, simd_replicate_right_SHORT, vector_replicate_right_SHORT);
    TEST_IDENTICAL_S1I1(SHORT, v.s, 0, simd_replicate_left_SHORT, vector_replicate_left_SHORT);

    if (SIMD_NUM_ELEMENTS(USHORT) > 8) {
	TEST_IDENTICAL_S1I1(USHORT, v.us, 11, simd_replicate_right_USHORT, vector_replicate_right_USHORT);
	TEST_IDENTICAL_S1I1(USHORT, v.us, 12, simd_replicate_left_USHORT, vector_replicate_left_USHORT);
    }

#ifdef simd_replicate_right_ULONG
    v.ul = (simd_ULONG)vector_random_UBYTE();
    TEST_IDENTICAL_S1I1(ULONG, v.ul, 0, simd_replicate_right_ULONG, vector_replicate_right_ULONG);
    TEST_IDENTICAL_S1I1(ULONG, v.ul, 1, simd_replicate_right_ULONG, vector_replicate_right_ULONG);
    TEST_IDENTICAL_S1I1(ULONG, v.ul, 1, simd_replicate_left_ULONG, vector_replicate_left_ULONG);
    if (SIMD_NUM_ELEMENTS(ULONG) > 2)
	TEST_IDENTICAL_S1I1(ULONG, v.ul, 2, simd_replicate_left_ULONG, vector_replicate_left_ULONG);
    rand_pos = rand() & (SIMD_NUM_ELEMENTS(ULONG)-1);
    TEST_IDENTICAL_S1I1(LONG, v.l, rand_pos, simd_replicate_left_LONG, vector_replicate_left_LONG);

    if (SIMD_NUM_ELEMENTS(ULONG) > 4)
	TEST_IDENTICAL_S1I1(ULONG, v.ul, 5, simd_replicate_left_ULONG, vector_replicate_left_ULONG);
#endif

#ifdef simd_replicate_right_FLOAT
    v.f = vector_random_FLOAT(2.0);
    TEST_IDENTICAL_S1I1(FLOAT, v.f, 0, simd_replicate_right_FLOAT, vector_replicate_right_FLOAT);
    TEST_IDENTICAL_S1I1(FLOAT, v.f, 2, simd_replicate_right_FLOAT, vector_replicate_right_FLOAT);
    TEST_IDENTICAL_S1I1(FLOAT, v.f, 2, simd_replicate_left_FLOAT, vector_replicate_left_FLOAT);
    TEST_IDENTICAL_S1I1(FLOAT, v.f, 3, simd_replicate_left_FLOAT, vector_replicate_left_FLOAT);

    if (SIMD_NUM_ELEMENTS(FLOAT) > 4)
	TEST_IDENTICAL_S1I1(FLOAT, v.f, 6, simd_replicate_left_FLOAT, vector_replicate_left_FLOAT);
#endif

#ifdef simd_replicate_right_DOUBLE
    v.d = vector_random_DOUBLE(2.3);
    TEST_IDENTICAL_S1I1(DOUBLE, v.d, 0, simd_replicate_right_DOUBLE, vector_replicate_right_DOUBLE);
    TEST_IDENTICAL_S1I1(DOUBLE, v.d, 1, simd_replicate_right_DOUBLE, vector_replicate_right_DOUBLE);
    TEST_IDENTICAL_S1I1(DOUBLE, v.d, 0, simd_replicate_left_DOUBLE, vector_replicate_left_DOUBLE);
    TEST_IDENTICAL_S1I1(DOUBLE, v.d, 1, simd_replicate_left_DOUBLE, vector_replicate_left_DOUBLE);

    if (SIMD_NUM_ELEMENTS(DOUBLE) > 2) {
	TEST_IDENTICAL_S1I1(DOUBLE, v.d, 2, simd_replicate_right_DOUBLE, vector_replicate_right_DOUBLE);
	TEST_IDENTICAL_S1I1(DOUBLE, v.d, 2, simd_replicate_left_DOUBLE, vector_replicate_left_DOUBLE);
    }
#endif

    END_TEST();
}

/*
 * simd_zero_right_UBYTE()
 */

#define generate_zero_right(tt)						\
    static simd_ ## tt vector_zero_right_ ## tt(simd_ ## tt x, int pos) \
    {									\
	vrep_ ## tt v;							\
	assert(pos >= 0 && pos < SIMD_NUM_ELEMENTS(tt));		\
	v.v = x;							\
	for (int j=pos+1; j<SIMD_NUM_ELEMENTS(tt); j++)			\
	    v.e[j] = 0;							\
	return v.v;							\
    }

#define generate_zero_left(tt)						\
    static simd_ ## tt vector_zero_left_ ## tt(simd_ ## tt x, int pos)	\
    {									\
	vrep_ ## tt v;							\
	assert(pos >= 0 && pos < SIMD_NUM_ELEMENTS(tt));		\
	v.v = x;							\
	for (int j=0; j<pos; j++)					\
	    v.e[j] = 0;							\
	return v.v;							\
    }

generate_zero_right(BYTE)
generate_zero_right(UBYTE)
generate_zero_right(SHORT)
generate_zero_right(USHORT)
#ifdef simd_zero_right_ULONG
generate_zero_right(LONG)
generate_zero_right(ULONG)
#endif
#ifdef simd_zero_right_FLOAT
generate_zero_right(FLOAT)
#endif
#ifdef simd_zero_right_DOUBLE
generate_zero_right(DOUBLE)
#endif

generate_zero_left(BYTE)
generate_zero_left(UBYTE)
generate_zero_left(SHORT)
generate_zero_left(USHORT)
#ifdef simd_zero_left_ULONG
generate_zero_left(LONG)
generate_zero_left(ULONG)
#endif
#ifdef simd_zero_left_FLOAT
generate_zero_left(FLOAT)
#endif
#ifdef simd_zero_left_DOUBLE
generate_zero_left(DOUBLE)
#endif

static void test_simd_zero(void)
{
    vany v;
    int rand_pos = rand() & 0x07; /* Provide a position arg only known at run time */
    v.ub = vector_random_UBYTE();
    int have_right = 0;
    int have_left = 0;
    int missing_routines = 0;

    START_TEST();

#ifdef simd_zero_right_UBYTE
    have_right = 1;
    TEST_IDENTICAL_S1I1(UBYTE, v.ub, 3, simd_zero_right_UBYTE, vector_zero_right_UBYTE);
    TEST_IDENTICAL_S1I1(UBYTE, v.ub, rand_pos, simd_zero_right_UBYTE, vector_zero_right_UBYTE);
    TEST_IDENTICAL_S1I1(BYTE, v.b, rand_pos, simd_zero_right_BYTE, vector_zero_right_BYTE);

    if (SIMD_NUM_ELEMENTS(UBYTE) > 16) {
	TEST_IDENTICAL_S1I1(UBYTE, v.ub, 27, simd_zero_right_UBYTE, vector_zero_right_UBYTE);
    }
#endif

#ifdef simd_zero_left_UBYTE
    have_left = 1;
    TEST_IDENTICAL_S1I1(UBYTE, v.ub, 2, simd_zero_left_UBYTE, vector_zero_left_UBYTE);
    TEST_IDENTICAL_S1I1(UBYTE, v.ub, rand_pos, simd_zero_left_UBYTE, vector_zero_left_UBYTE);
    TEST_IDENTICAL_S1I1(BYTE, v.b, rand_pos, simd_zero_left_BYTE, vector_zero_left_BYTE);

    if (SIMD_NUM_ELEMENTS(UBYTE) > 16) {
	TEST_IDENTICAL_S1I1(UBYTE, v.ub, 22, simd_zero_left_UBYTE, vector_zero_left_UBYTE);
    }
#endif

    if (have_right ^ have_left)
	missing_routines = 1;

    have_right = have_left = 0;

#ifdef simd_zero_right_USHORT
    have_right = 1;
    TEST_IDENTICAL_S1I1(USHORT, v.us, 1, simd_zero_right_USHORT, vector_zero_right_USHORT);
    TEST_IDENTICAL_S1I1(USHORT, v.us, rand_pos>>1, simd_zero_right_USHORT, vector_zero_right_USHORT);
    TEST_IDENTICAL_S1I1(SHORT, v.s, 2, simd_zero_right_SHORT, vector_zero_right_SHORT);

    if (SIMD_NUM_ELEMENTS(USHORT) > 8) {
	TEST_IDENTICAL_S1I1(USHORT, v.us, 13, simd_zero_right_USHORT, vector_zero_right_USHORT);
    }
#endif

#ifdef simd_zero_left_USHORT
    have_left = 1;
    TEST_IDENTICAL_S1I1(USHORT, v.us, 1, simd_zero_left_USHORT, vector_zero_left_USHORT);
    TEST_IDENTICAL_S1I1(USHORT, v.us, rand_pos>>1, simd_zero_left_USHORT, vector_zero_left_USHORT);
    TEST_IDENTICAL_S1I1(SHORT, v.s, 3, simd_zero_left_SHORT, vector_zero_left_SHORT);

    if (SIMD_NUM_ELEMENTS(USHORT) > 8) {
	TEST_IDENTICAL_S1I1(USHORT, v.us, 12, simd_zero_left_USHORT, vector_zero_left_USHORT);
    }
#endif

    if (have_right ^ have_left)
	missing_routines = 1;

    have_right = have_left = 0;

#ifdef simd_zero_right_ULONG
    have_right = 1;
    TEST_IDENTICAL_S1I1(ULONG, v.ul, 1, simd_zero_right_ULONG, vector_zero_right_ULONG);
    TEST_IDENTICAL_S1I1(ULONG, v.ul, 0, simd_zero_right_ULONG, vector_zero_right_ULONG);
    TEST_IDENTICAL_S1I1(LONG, v.l, 1, simd_zero_right_LONG, vector_zero_right_LONG);
    if (SIMD_NUM_ELEMENTS(ULONG) > 2) {
	TEST_IDENTICAL_S1I1(ULONG, v.ul, 2, simd_zero_right_ULONG, vector_zero_right_ULONG);
    }
    if (SIMD_NUM_ELEMENTS(ULONG) > 4) {
	TEST_IDENTICAL_S1I1(ULONG, v.ul, 6, simd_zero_right_ULONG, vector_zero_right_ULONG);
    }
#endif

#ifdef simd_zero_left_ULONG
    have_left = 1;
    TEST_IDENTICAL_S1I1(ULONG, v.ul, 1, simd_zero_left_ULONG, vector_zero_left_ULONG);
    TEST_IDENTICAL_S1I1(ULONG, v.ul, 0, simd_zero_left_ULONG, vector_zero_left_ULONG);
    TEST_IDENTICAL_S1I1(LONG, v.l, 1, simd_zero_left_LONG, vector_zero_left_LONG);
    if (SIMD_NUM_ELEMENTS(ULONG) > 2) {
	TEST_IDENTICAL_S1I1(ULONG, v.ul, 2, simd_zero_left_ULONG, vector_zero_left_ULONG);
    }
    if (SIMD_NUM_ELEMENTS(ULONG) > 4) {
	TEST_IDENTICAL_S1I1(ULONG, v.ul, 6, simd_zero_left_ULONG, vector_zero_left_ULONG);
    }
#endif

    if (have_right ^ have_left)
	missing_routines = 1;

    have_right = have_left = 0;

#ifdef simd_zero_right_FLOAT
    have_right = 1;
    v.f = vector_random_FLOAT(1e5);
    TEST_IDENTICAL_S1I1(FLOAT, v.f, 1, simd_zero_right_FLOAT, vector_zero_right_FLOAT);
    TEST_IDENTICAL_S1I1(FLOAT, v.f, 2, simd_zero_right_FLOAT, vector_zero_right_FLOAT);

    if (SIMD_NUM_ELEMENTS(FLOAT) > 4) {
	TEST_IDENTICAL_S1I1(FLOAT, v.f, 6, simd_zero_right_FLOAT, vector_zero_right_FLOAT);
    }
#endif

#ifdef simd_zero_left_FLOAT
    have_left = 1;
    v.f = vector_random_FLOAT(1e5);
    TEST_IDENTICAL_S1I1(FLOAT, v.f, 1, simd_zero_left_FLOAT, vector_zero_left_FLOAT);
    TEST_IDENTICAL_S1I1(FLOAT, v.f, 2, simd_zero_left_FLOAT, vector_zero_left_FLOAT);

    if (SIMD_NUM_ELEMENTS(FLOAT) > 4) {
	TEST_IDENTICAL_S1I1(FLOAT, v.f, 6, simd_zero_left_FLOAT, vector_zero_left_FLOAT);
    }
#endif

    if (have_right ^ have_left)
	missing_routines = 1;

    have_right = have_left = 0;

#ifdef simd_zero_right_DOUBLE
    have_right = 1;
    v.d = vector_random_DOUBLE(1e6);
    TEST_IDENTICAL_S1I1(DOUBLE, v.d, 0, simd_zero_right_DOUBLE, vector_zero_right_DOUBLE);
    TEST_IDENTICAL_S1I1(DOUBLE, v.d, 1, simd_zero_right_DOUBLE, vector_zero_right_DOUBLE);

    if (SIMD_NUM_ELEMENTS(DOUBLE) > 2) {
	TEST_IDENTICAL_S1I1(DOUBLE, v.d, 2, simd_zero_right_DOUBLE, vector_zero_right_DOUBLE);
    }
#endif

#ifdef simd_zero_left_DOUBLE
    have_left = 1;
    v.d = vector_random_DOUBLE(1e6);
    TEST_IDENTICAL_S1I1(DOUBLE, v.d, 0, simd_zero_left_DOUBLE, vector_zero_left_DOUBLE);
    TEST_IDENTICAL_S1I1(DOUBLE, v.d, 1, simd_zero_left_DOUBLE, vector_zero_left_DOUBLE);

    if (SIMD_NUM_ELEMENTS(DOUBLE) > 2) {
	TEST_IDENTICAL_S1I1(DOUBLE, v.d, 2, simd_zero_left_DOUBLE, vector_zero_left_DOUBLE);
    }
#endif

    if (have_right ^ have_left)
	missing_routines = 1;

    if (missing_routines) {
	if (ok)
	    ok = 2;		/* Register warning if not previous failure. */
	REGISTER_NOTE("Some matching right or left routines are missing.\n");
    }

    END_TEST();
}


/*
 * Test comparison routines.
 *
 * simd_cmplt, simd_cmpgt and simd_cmpeq.
 */

#define generate_vector_compare(tt, name, op)                          \
    static simd_##tt vector_##name##_##tt(simd_##tt a, simd_##tt b)    \
    {                                                                  \
       vrep_##tt va, vb, vres;                                         \
       va.v = a;  vb.v = b;                                            \
       for (int j=0; j<SIMD_NUM_ELEMENTS(tt); j++) {                   \
           vres.e[j] = (va.e[j] op vb.e[j]) ? -1 : 0;                  \
       }                                                               \
       return vres.v;                                                  \
     }


typedef union {
    float f;
    int32_t x;
} FLOAT_access;

typedef union  {
    double f;
    int64_t x;
} DOUBLE_access;


#define generate_vector_compare_real(tt, name, op)                     \
    static simd_##tt vector_##name##_##tt(simd_##tt a, simd_##tt b)    \
    {                                                                  \
       vrep_##tt va, vb, vres;                                         \
       va.v = a;  vb.v = b;                                            \
       for (int j=0; j<SIMD_NUM_ELEMENTS(tt); j++) {                   \
           tt##_access res;                                            \
           res.x = (va.e[j] op vb.e[j]) ? -1 : 0;                      \
           vres.e[j] = res.f;                                          \
       }                                                               \
       return vres.v;                                                  \
    }

generate_vector_compare(BYTE, cmplt, <)
generate_vector_compare(UBYTE, cmplt, <)
generate_vector_compare(SHORT, cmplt, <)
generate_vector_compare(USHORT, cmplt, <)
#ifdef simd_cmplt_ULONG
generate_vector_compare(LONG, cmplt, <)
generate_vector_compare(ULONG, cmplt, <)
#endif
#ifdef simd_cmplt_FLOAT
generate_vector_compare_real(FLOAT, cmplt, <)
#endif
#ifdef simd_cmplt_DOUBLE
generate_vector_compare_real(DOUBLE, cmplt, <)
#endif

generate_vector_compare(BYTE, cmpgt, >)
generate_vector_compare(UBYTE, cmpgt, >)
generate_vector_compare(SHORT, cmpgt, >)
generate_vector_compare(USHORT, cmpgt, >)
#ifdef simd_cmpgt_ULONG
generate_vector_compare(LONG, cmpgt, >)
generate_vector_compare(ULONG, cmpgt, >)
#endif
#ifdef simd_cmpgt_FLOAT
generate_vector_compare_real(FLOAT, cmpgt, >)
#endif
#ifdef simd_cmpgt_DOUBLE
generate_vector_compare_real(DOUBLE, cmpgt, >)
#endif

generate_vector_compare(BYTE, cmpeq, ==)
generate_vector_compare(UBYTE, cmpeq, ==)
generate_vector_compare(SHORT, cmpeq, ==)
generate_vector_compare(USHORT, cmpeq, ==)
#ifdef simd_cmpeq_ULONG
generate_vector_compare(LONG, cmpeq, ==)
generate_vector_compare(ULONG, cmpeq, ==)
#endif
#ifdef simd_cmpeq_FLOAT
generate_vector_compare_real(FLOAT, cmpeq, ==)
#endif
#ifdef simd_cmpeq_DOUBLE
generate_vector_compare_real(DOUBLE, cmpeq, ==)
#endif


static void test_simd_cmplt(void)
{
    vany u, v;

    START_TEST();

    u.ub = vector_random_UBYTE();
    v.ub = vector_random_UBYTE();

    u.ub = vector_set_element_UBYTE(u.ub, 1, 0x5a);
    v.ub = vector_set_element_UBYTE(v.ub, 1, 0x5a);

#ifdef simd_cmplt_UBYTE
    TEST_IDENTICAL_S2(UBYTE, u.ub, v.ub, simd_cmplt_UBYTE, vector_cmplt_UBYTE);
    TEST_IDENTICAL_S2(BYTE, u.b, v.b, simd_cmplt_BYTE, vector_cmplt_BYTE);
#endif

#ifdef simd_cmplt_USHORT
    u.us = vector_random_USHORT();
    v.us = vector_random_USHORT();

    u.us = vector_set_element_USHORT(u.us, 1, 0x5a29);
    v.us = vector_set_element_USHORT(v.us, 1, 0x5a29);
    TEST_IDENTICAL_S2(USHORT, u.us, v.us, simd_cmplt_USHORT, vector_cmplt_USHORT);
    TEST_IDENTICAL_S2(SHORT, u.s, v.s, simd_cmplt_SHORT, vector_cmplt_SHORT);
#endif

#ifdef simd_cmplt_ULONG
    u.ul = vector_random_ULONG();
    v.ul = vector_random_ULONG();

    u.ul = vector_set_element_ULONG(u.ul, 1, 0x5a29123b);
    v.ul = vector_set_element_ULONG(v.ul, 1, 0x5a29123b);
    TEST_IDENTICAL_S2(ULONG, u.ul, v.ul, simd_cmplt_ULONG, vector_cmplt_ULONG);
    TEST_IDENTICAL_S2(LONG, u.l, v.l, simd_cmplt_LONG, vector_cmplt_LONG);
#endif

#ifdef simd_cmplt_FLOAT
    u.f = vector_random_FLOAT(10);
    v.f = vector_random_FLOAT(10);

    u.f = vector_set_element_FLOAT(u.f, 2, -0.15982312354);
    v.f = vector_set_element_FLOAT(v.f, 2, -0.15982312354);
    TEST_IDENTICAL_S2(FLOAT, u.f, v.f, simd_cmplt_FLOAT, vector_cmplt_FLOAT);
#endif

#ifdef simd_cmplt_DOUBLE
    u.d = vector_random_DOUBLE(10);
    v.d = vector_random_DOUBLE(10);
    u.d = vector_set_element_DOUBLE(u.d, 0, 0.8172);
    v.d = vector_set_element_DOUBLE(v.d, 0, 0.81722);
    u.d = vector_set_element_DOUBLE(u.d, 1, 0.1872);
    v.d = vector_set_element_DOUBLE(v.d, 1, 0.18719);
    TEST_IDENTICAL_S2(DOUBLE, u.d, v.d, simd_cmplt_DOUBLE, vector_cmplt_DOUBLE);

    u.d = vector_random_DOUBLE(10);
    v.d = vector_random_DOUBLE(10);
    u.d = vector_set_element_DOUBLE(u.d, 1, 0.187456);
    v.d = vector_set_element_DOUBLE(v.d, 1, 0.187456);
    TEST_IDENTICAL_S2(DOUBLE, u.d, v.d, simd_cmplt_DOUBLE, vector_cmplt_DOUBLE);
#endif

    END_TEST();
}

static void test_simd_cmpgt(void)
{
    vany u, v;

    START_TEST();

    u.ub = vector_random_UBYTE();
    v.ub = vector_random_UBYTE();

    u.ub = vector_set_element_UBYTE(u.ub, 2, 0x39);
    v.ub = vector_set_element_UBYTE(v.ub, 2, 0x39);

#ifdef simd_cmpgt_UBYTE
    TEST_IDENTICAL_S2(UBYTE, u.ub, v.ub, simd_cmpgt_UBYTE, vector_cmpgt_UBYTE);
    TEST_IDENTICAL_S2(BYTE, u.b, v.b, simd_cmpgt_BYTE, vector_cmpgt_BYTE);
#endif

#ifdef simd_cmpgt_USHORT
    u.us = vector_random_USHORT();
    v.us = vector_random_USHORT();

    u.us = vector_set_element_USHORT(u.us, 1, 0x5a29);
    v.us = vector_set_element_USHORT(v.us, 1, 0x5a29);
    TEST_IDENTICAL_S2(USHORT, u.us, v.us, simd_cmpgt_USHORT, vector_cmpgt_USHORT);
    TEST_IDENTICAL_S2(SHORT, u.s, v.s, simd_cmpgt_SHORT, vector_cmpgt_SHORT);
#endif

#ifdef simd_cmpgt_ULONG
    u.ul = vector_random_ULONG();
    v.ul = vector_random_ULONG();

    u.ul = vector_set_element_ULONG(u.ul, 1, 0x5a29123b);
    v.ul = vector_set_element_ULONG(v.ul, 1, 0x5a29123b);
    TEST_IDENTICAL_S2(ULONG, u.ul, v.ul, simd_cmpgt_ULONG, vector_cmpgt_ULONG);
    TEST_IDENTICAL_S2(LONG, u.l, v.l, simd_cmpgt_LONG, vector_cmpgt_LONG);
#endif

#ifdef simd_cmpgt_FLOAT
    u.f = vector_random_FLOAT(10);
    v.f = vector_random_FLOAT(10);

    u.f = vector_set_element_FLOAT(u.f, 2, -0.15982312354);
    v.f = vector_set_element_FLOAT(v.f, 2, -0.15982312354);
    TEST_IDENTICAL_S2(FLOAT, u.f, v.f, simd_cmpgt_FLOAT, vector_cmpgt_FLOAT);
#endif

#ifdef simd_cmpgt_DOUBLE
    u.d = vector_random_DOUBLE(10);
    v.d = vector_random_DOUBLE(10);
    u.d = vector_set_element_DOUBLE(u.d, 0, 0.8172);
    v.d = vector_set_element_DOUBLE(v.d, 0, 0.81722);
    u.d = vector_set_element_DOUBLE(u.d, 1, 0.1872);
    v.d = vector_set_element_DOUBLE(v.d, 1, 0.18719);
    TEST_IDENTICAL_S2(DOUBLE, u.d, v.d, simd_cmpgt_DOUBLE, vector_cmpgt_DOUBLE);

    u.d = vector_random_DOUBLE(10);
    v.d = vector_random_DOUBLE(10);
    u.d = vector_set_element_DOUBLE(u.d, 1, 0.187456);
    v.d = vector_set_element_DOUBLE(v.d, 1, 0.187456);
    TEST_IDENTICAL_S2(DOUBLE, u.d, v.d, simd_cmpgt_DOUBLE, vector_cmpgt_DOUBLE);
#endif

    END_TEST();
}

static void test_simd_cmpeq(void)
{
    vany u, v;

    START_TEST();

    u.ub = vector_random_UBYTE();
    v.ub = vector_random_UBYTE();

    u.ub = vector_set_element_UBYTE(u.ub, 1, 0xab);
    v.ub = vector_set_element_UBYTE(v.ub, 1, 0xab);

#ifdef simd_cmpeq_UBYTE
    TEST_IDENTICAL_S2(UBYTE, u.ub, v.ub, simd_cmpeq_UBYTE, vector_cmpeq_UBYTE);
    TEST_IDENTICAL_S2(BYTE, u.b, v.b, simd_cmpeq_BYTE, vector_cmpeq_BYTE);
#endif

#ifdef simd_cmpeq_USHORT
    u.us = vector_random_USHORT();
    v.us = vector_random_USHORT();

    u.us = vector_set_element_USHORT(u.us, 1, 0x5a29);
    v.us = vector_set_element_USHORT(v.us, 1, 0x5a29);
    TEST_IDENTICAL_S2(USHORT, u.us, v.us, simd_cmpeq_USHORT, vector_cmpeq_USHORT);
    TEST_IDENTICAL_S2(SHORT, u.s, v.s, simd_cmpeq_SHORT, vector_cmpeq_SHORT);
#endif

#ifdef simd_cmpeq_ULONG
    u.ul = vector_random_ULONG();
    v.ul = vector_random_ULONG();

    u.ul = vector_set_element_ULONG(u.ul, 1, 0x5a29123b);
    v.ul = vector_set_element_ULONG(v.ul, 1, 0x5a29123b);
    TEST_IDENTICAL_S2(ULONG, u.ul, v.ul, simd_cmpeq_ULONG, vector_cmpeq_ULONG);
    TEST_IDENTICAL_S2(LONG, u.l, v.l, simd_cmpeq_LONG, vector_cmpeq_LONG);
#endif

#ifdef simd_cmpeq_FLOAT
    u.f = vector_random_FLOAT(10);
    v.f = vector_random_FLOAT(10);

    u.f = vector_set_element_FLOAT(u.f, 2, -0.15982312354);
    v.f = vector_set_element_FLOAT(v.f, 2, -0.15982312354);
    TEST_IDENTICAL_S2(FLOAT, u.f, v.f, simd_cmpeq_FLOAT, vector_cmpeq_FLOAT);
#endif

#ifdef simd_cmpeq_DOUBLE
    u.d = vector_random_DOUBLE(10);
    v.d = vector_random_DOUBLE(10);
    u.d = vector_set_element_DOUBLE(u.d, 0, 0.8172);
    v.d = vector_set_element_DOUBLE(v.d, 0, 0.81722);
    u.d = vector_set_element_DOUBLE(u.d, 1, 0.1872);
    v.d = vector_set_element_DOUBLE(v.d, 1, 0.18719);
    TEST_IDENTICAL_S2(DOUBLE, u.d, v.d, simd_cmpeq_DOUBLE, vector_cmpeq_DOUBLE);

    u.d = vector_random_DOUBLE(10);
    v.d = vector_random_DOUBLE(10);
    u.d = vector_set_element_DOUBLE(u.d, 1, 0.187456);
    v.d = vector_set_element_DOUBLE(v.d, 1, 0.187456);
    TEST_IDENTICAL_S2(DOUBLE, u.d, v.d, simd_cmpeq_DOUBLE, vector_cmpeq_DOUBLE);
#endif

    END_TEST();
}


/*
 * simd_horiz_min and simd_horiz_max
 *
 * Return all elements of the vector set to the minimum or maximum element
 * present in the vector, i.e., a horizontal min or max operation.
 */

#define generate_vector_horiz_minmax(tt, name, op, init)       \
    static simd_##tt vector_horiz_##name##_##tt(simd_##tt x)   \
    {                                                          \
       vrep_##tt vx, vres;                                     \
       vx.v = x;                                               \
       double minmax = init;                                   \
       for (int j=0; j<SIMD_NUM_ELEMENTS(tt); j++) {           \
           if ((double)vx.e[j] op minmax)                      \
               minmax = (double)vx.e[j];                       \
       }                                                       \
       for (int j=0; j<SIMD_NUM_ELEMENTS(tt); j++)             \
           vres.e[j] = (tt##_type)minmax;                      \
       return vres.v;                                          \
    }

#ifdef simd_horiz_max_UBYTE
generate_vector_horiz_minmax(BYTE, max, >, -HUGE_VAL)
generate_vector_horiz_minmax(UBYTE, max, >, -HUGE_VAL)
#endif
#ifdef simd_horiz_max_USHORT
generate_vector_horiz_minmax(SHORT, max, >, -HUGE_VAL)
generate_vector_horiz_minmax(USHORT, max, >, -HUGE_VAL)
#endif
#ifdef simd_horiz_max_ULONG
generate_vector_horiz_minmax(LONG, max, >, -HUGE_VAL)
generate_vector_horiz_minmax(ULONG, max, >, -HUGE_VAL)
#endif
#ifdef simd_horiz_max_FLOAT
generate_vector_horiz_minmax(FLOAT, max, >, -HUGE_VAL)
#endif
#ifdef simd_horiz_max_DOUBLE
generate_vector_horiz_minmax(DOUBLE, max, >, -HUGE_VAL)
#endif

#ifdef simd_horiz_min_UBYTE
generate_vector_horiz_minmax(BYTE, min, <, HUGE_VAL)
generate_vector_horiz_minmax(UBYTE, min, <, HUGE_VAL)
#endif
#ifdef simd_horiz_max_USHORT
generate_vector_horiz_minmax(SHORT, min, <, HUGE_VAL)
generate_vector_horiz_minmax(USHORT, min, <, HUGE_VAL)
#endif
#ifdef simd_horiz_min_ULONG
generate_vector_horiz_minmax(LONG, min, <, HUGE_VAL)
generate_vector_horiz_minmax(ULONG, min, <, HUGE_VAL)
#endif
#ifdef simd_horiz_max_FLOAT
generate_vector_horiz_minmax(FLOAT, min, <, HUGE_VAL)
#endif
#ifdef simd_horiz_max_DOUBLE
generate_vector_horiz_minmax(DOUBLE, min, <, HUGE_VAL)
#endif


static void test_simd_horiz_minmax(void)
{
    START_TEST();

    vany vx;
    int have_min = 0, have_max = 0;
    int missing_routines = 0;

#ifdef simd_horiz_min_UBYTE
    have_min = 1;
    vx.ub = vector_random_UBYTE();
    TEST_IDENTICAL_S1(UBYTE, vx.ub, simd_horiz_min_UBYTE, vector_horiz_min_UBYTE);
#endif

#ifdef simd_horiz_max_UBYTE
    have_max = 1;
    vx.ub = vector_random_UBYTE();
    TEST_IDENTICAL_S1(UBYTE, vx.ub, simd_horiz_max_UBYTE, vector_horiz_max_UBYTE);
#endif

    if (have_min ^ have_max)
	missing_routines = 1;
    have_min = have_max = 0;

#ifdef simd_horiz_min_BYTE
    have_min = 1;
    vx.b = vector_random_BYTE();
    TEST_IDENTICAL_S1(BYTE, vx.b, simd_horiz_min_BYTE, vector_horiz_min_BYTE);
#endif

#ifdef simd_horiz_max_BYTE
    have_max = 1;
    vx.b = vector_random_BYTE();
    TEST_IDENTICAL_S1(BYTE, vx.b, simd_horiz_max_BYTE, vector_horiz_max_BYTE);
#endif

    if (have_min ^ have_max)
	missing_routines = 1;
    have_min = have_max = 0;

#ifdef simd_horiz_min_USHORT
    have_min = 1;
    vx.us = vector_random_USHORT();
    TEST_IDENTICAL_S1(USHORT, vx.us, simd_horiz_min_USHORT, vector_horiz_min_USHORT);
#endif

#ifdef simd_horiz_max_USHORT
    have_max = 1;
    vx.us = vector_random_USHORT();
    TEST_IDENTICAL_S1(USHORT, vx.us, simd_horiz_max_USHORT, vector_horiz_max_USHORT);
#endif

    if (have_min ^ have_max)
	missing_routines = 1;
    have_min = have_max = 0;

#ifdef simd_horiz_min_SHORT
    have_min = 1;
    vx.s = vector_random_SHORT();
    TEST_IDENTICAL_S1(SHORT, vx.s, simd_horiz_min_SHORT, vector_horiz_min_SHORT);
#endif

#ifdef simd_horiz_max_SHORT
    have_max = 1;
    vx.s = vector_random_SHORT();
    TEST_IDENTICAL_S1(SHORT, vx.s, simd_horiz_max_SHORT, vector_horiz_max_SHORT);
#endif

    if (have_min ^ have_max)
	missing_routines = 1;
    have_min = have_max = 0;

#ifdef simd_horiz_min_ULONG
    have_min = 1;
    vx.ul = vector_random_ULONG();
    TEST_IDENTICAL_S1(ULONG, vx.ul, simd_horiz_min_ULONG, vector_horiz_min_ULONG);
#endif

#ifdef simd_horiz_max_ULONG
    have_max = 1;
    vx.ul = vector_random_ULONG();
    TEST_IDENTICAL_S1(ULONG, vx.ul, simd_horiz_max_ULONG, vector_horiz_max_ULONG);
#endif

    if (have_min ^ have_max)
	missing_routines = 1;
    have_min = have_max = 0;

#ifdef simd_horiz_min_LONG
    have_min = 1;
    vx.l = vector_random_LONG();
    TEST_IDENTICAL_S1(LONG, vx.l, simd_horiz_min_LONG, vector_horiz_min_LONG);
#endif

#ifdef simd_horiz_max_LONG
    have_max = 1;
    vx.l = vector_random_LONG();
    TEST_IDENTICAL_S1(LONG, vx.l, simd_horiz_max_LONG, vector_horiz_max_LONG);
#endif

    if (have_min ^ have_max)
	missing_routines = 1;
    have_min = have_max = 0;

#ifdef simd_horiz_min_FLOAT
    have_min = 1;
    vx.f = vector_random_FLOAT(10);
    TEST_IDENTICAL_S1(FLOAT, vx.f, simd_horiz_min_FLOAT, vector_horiz_min_FLOAT);
#endif

#ifdef simd_horiz_max_FLOAT
    have_max = 1;
    vx.f = vector_random_FLOAT(10);
    TEST_IDENTICAL_S1(FLOAT, vx.f, simd_horiz_max_FLOAT, vector_horiz_max_FLOAT);
#endif

    if (have_min ^ have_max)
	missing_routines = 1;
    have_min = have_max = 0;

#ifdef simd_horiz_min_DOUBLE
    have_min = 1;
    vx.d = vector_random_DOUBLE(10);
    TEST_IDENTICAL_S1(DOUBLE, vx.d, simd_horiz_min_DOUBLE, vector_horiz_min_DOUBLE);
#endif

#ifdef simd_horiz_max_DOUBLE
    have_max = 1;
    vx.d = vector_random_DOUBLE(10);
    TEST_IDENTICAL_S1(DOUBLE, vx.d, simd_horiz_max_DOUBLE, vector_horiz_max_DOUBLE);
#endif

    if (have_min ^ have_max)
	missing_routines = 1;
    have_min = have_max = 0;

    if (missing_routines) {
	if (ok == 1)
	    ok = 2;
	REGISTER_NOTE("Some matching min or max routines are missing.");
    }

    END_TEST();
}




/*
 * Test conversion routines.
 *
 * First generate reference functions for the various tests
 */

/* Generate functions that convert pixel type to type same size */

#define generate_vector_cnvrt(dtype, stype, name, convert)		\
    static simd_ ## dtype vector_##name##_## dtype ## _ ## stype(simd_ ## stype x) \
    {									\
	vrep_ ## stype vx;						\
	vrep_ ## dtype res;						\
	vx.v = x;							\
	for (int j=0; j<SIMD_NUM_ELEMENTS(dtype); j++) {		\
	    res.e[j] = (dtype ## _type)convert(vx.e[j]);		\
	}								\
	return res.v;							\
    }

/* Generate functions that convert pixel type to type double the size */

#define generate_vector_cnvrt_up(dtype, tt, stype, name, convert)	\
    static simd_ ## dtype ## _2 vector_##name##_ ## dtype ## _ ## stype(simd_ ## stype x) \
    {									\
	vrep_ ## stype vx;						\
	vrep_ ## dtype vr;						\
	simd_ ## dtype ## _2 res;					\
	vx.v = x;							\
	for (int j=0; j<SIMD_NUM_ELEMENTS(dtype); j++) {		\
	    vr.e[j] = (dtype ## _type)convert(vx.e[j]);			\
	}								\
	res.tt[0] = vr.v;						\
	for (int j=0; j<SIMD_NUM_ELEMENTS(dtype); j++) {		\
	    vr.e[j] = (dtype ## _type)convert(vx.e[j+SIMD_NUM_ELEMENTS(dtype)]); \
	}								\
	res.tt[1] = vr.v;						\
	return res;							\
    }

/* Generate functions that convert pixel type to type quadruple the size */

#define generate_vector_cnvrt_up_2(dtype, tt, stype, name, convert)		\
    static simd_ ## dtype ## _4 vector_##name##_ ## dtype ## _ ## stype(simd_ ## stype x) \
    {									\
	vrep_ ## stype vx;						\
	vrep_ ## dtype vr;						\
	simd_ ## dtype ## _4 res;					\
	vx.v = x;							\
	for (int j=0; j<SIMD_NUM_ELEMENTS(dtype); j++) {		\
	    vr.e[j] = (dtype ## _type)convert(vx.e[j]);			\
	}								\
	res.tt[0] = vr.v;						\
	for (int j=0; j<SIMD_NUM_ELEMENTS(dtype); j++) {		\
	    vr.e[j] = (dtype ## _type)convert(vx.e[j+SIMD_NUM_ELEMENTS(dtype)]); \
	}								\
	res.tt[1] = vr.v;						\
	for (int j=0; j<SIMD_NUM_ELEMENTS(dtype); j++) {		\
	    vr.e[j] = (dtype ## _type)convert(vx.e[j+2*SIMD_NUM_ELEMENTS(dtype)]); \
	}								\
	res.tt[2] = vr.v;						\
	for (int j=0; j<SIMD_NUM_ELEMENTS(dtype); j++) {		\
	    vr.e[j] = (dtype ## _type)convert(vx.e[j+3*SIMD_NUM_ELEMENTS(dtype)]); \
	}								\
	res.tt[3] = vr.v;						\
	return res;							\
    }

/* Generate functions that convert pixel type to type halve the size */

#define generate_vector_cnvrt_down(dtype, stype, name, convert)		\
    static simd_ ## dtype vector_##name##_ ## dtype ## _ ## stype(simd_ ## stype x, simd_ ## stype y) \
    {									\
	vrep_ ## stype vx, vy;						\
	vrep_ ## dtype res;						\
	vx.v = x; vy.v = y;						\
	for (int j=0; j<SIMD_NUM_ELEMENTS(stype); j++) {		\
	    res.e[j] = (dtype ## _type)convert(vx.e[j]);		\
	    res.e[j+SIMD_NUM_ELEMENTS(stype)] = (dtype ## _type)convert(vy.e[j]); \
	}								\
	return res.v;							\
    }

/* Generate functions that convert pixel type to type quarter the size */

#define generate_vector_cnvrt_down_2(dtype, stype, name, convert)	\
    static simd_ ## dtype vector_##name##_ ## dtype ## _ ## stype(simd_ ## stype w, simd_ ## stype x, simd_ ## stype y, simd_ ## stype z) \
    {									\
	vrep_ ## stype vw, vx, vy, vz;					\
	vrep_ ## dtype res;						\
	vw.v = w; vx.v = x; vy.v = y; vz.v = z;				\
	for (int j=0; j<SIMD_NUM_ELEMENTS(stype); j++) {		\
	    res.e[j] = (dtype ## _type)convert(vw.e[j]);		\
	    res.e[j+SIMD_NUM_ELEMENTS(stype)] = (dtype ## _type)convert(vx.e[j]); \
	    res.e[j+2*SIMD_NUM_ELEMENTS(stype)] = (dtype ## _type)convert(vy.e[j]); \
	    res.e[j+3*SIMD_NUM_ELEMENTS(stype)] = (dtype ## _type)convert(vz.e[j]); \
	}								\
	return res.v;							\
    }

/*
 * Argument cnvrt in the macros can be:
 *   convert()          - simple conversion
 *   roundf()/round()   - round to integer
 *   clip_DTYPE()       - one of the type specific clip routines
 *   round_clip_DTYPE() - round and clip
 *   roundf_clip_DTYPE() - round and clip
 */

#define convert(x) (x)

#if defined(simd_splat_LONG) && defined(simd_splat_FLOAT)
generate_vector_cnvrt(FLOAT, LONG, cnvrt, convert)
generate_vector_cnvrt(LONG, FLOAT, cnvrt, convert)
generate_vector_cnvrt(FLOAT, ULONG, cnvrt, convert)
generate_vector_cnvrt(ULONG, FLOAT, cnvrt, convert)
#endif

generate_vector_cnvrt_up(SHORT, s, BYTE, cnvrt, convert)
generate_vector_cnvrt_up(USHORT, us, UBYTE, cnvrt, convert)
#ifdef simd_splat_ULONG
generate_vector_cnvrt_up(LONG, l, SHORT, cnvrt, convert)
generate_vector_cnvrt_up(ULONG, ul, USHORT, cnvrt, convert)
#endif

#ifdef simd_splat_DOUBLE
#  ifdef simd_splat_LONG
generate_vector_cnvrt_up(DOUBLE, d, LONG, cnvrt, convert)
generate_vector_cnvrt_up(DOUBLE, d, ULONG, cnvrt, convert)
#  endif
#  ifdef simd_splat_FLOAT
generate_vector_cnvrt_up(DOUBLE, d, FLOAT, cnvrt, convert)
#  endif
#endif

#ifdef simd_splat_ULONG
generate_vector_cnvrt_up_2(LONG, l, BYTE, cnvrt, convert)
generate_vector_cnvrt_up_2(ULONG, ul, UBYTE, cnvrt, convert)
#endif

generate_vector_cnvrt_down(UBYTE, USHORT, cnvrt, convert)
generate_vector_cnvrt_down(BYTE, SHORT, cnvrt, convert)
#ifdef simd_splat_ULONG
generate_vector_cnvrt_down(USHORT, ULONG, cnvrt, convert)
generate_vector_cnvrt_down(SHORT, LONG, cnvrt, convert)

#ifdef simd_splat_ULONG64
generate_vector_cnvrt_down(ULONG, ULONG64, cnvrt, convert)
#endif
#endif

#ifdef simd_splat_DOUBLE
#  ifdef simd_splat_LONG
generate_vector_cnvrt_down(LONG, DOUBLE, cnvrt, convert)
generate_vector_cnvrt_down(ULONG, DOUBLE, cnvrt, convert)
#  endif
#  ifdef simd_splat_FLOAT
generate_vector_cnvrt_down(FLOAT, DOUBLE, cnvrt, convert)
#  endif
#endif

#ifdef simd_splat_ULONG
generate_vector_cnvrt_down_2(UBYTE, ULONG, cnvrt, convert)
generate_vector_cnvrt_down_2(BYTE, LONG, cnvrt, convert)
#endif


/*
 * Conversion of floating point to integer by conventional rounding
 */

#ifdef simd_splat_FLOAT
generate_vector_cnvrt(LONG, FLOAT, cnvrt_rnd, (int64_t)roundf)
generate_vector_cnvrt(ULONG, FLOAT, cnvrt_rnd, (int64_t)roundf)
#endif
#ifdef simd_splat_DOUBLE
generate_vector_cnvrt_down(LONG, DOUBLE, cnvrt_rnd, (int64_t)round)
generate_vector_cnvrt_down(ULONG, DOUBLE, cnvrt_rnd, (int64_t)round)
#endif


/*
 * We need a number of convert clip variations:
 */

#define generate_clip_upper(dtype, stype)				\
    static inline dtype##_type clip_upper_##dtype##_##stype(stype##_type x) \
    {									\
	if (x > dtype##_max) x = dtype##_max;				\
	return (dtype##_type)x;						\
    }

#define generate_clip_positive(stype)					\
    static inline stype##_type clip_positive_##stype(stype##_type x)	\
    {									\
	if (x < 0) x = 0;						\
	return x;							\
    }

#define generate_clip(dtype, stype)					\
    static inline dtype##_type clip_##dtype##_##stype(stype##_type x)	\
    {									\
	if (x > dtype##_max) x = dtype##_max;				\
	if (x < dtype##_min) x = dtype##_min;				\
	return (dtype##_type)x;						\
    }

/* Clipping where we only need to clip the negative values to zero */

generate_clip_positive(BYTE)
generate_clip_positive(SHORT)
generate_clip_positive(LONG)

generate_vector_cnvrt(UBYTE, BYTE, cnvrt_clip, clip_positive_BYTE)
generate_vector_cnvrt_up(USHORT, us, BYTE, cnvrt_clip, clip_positive_BYTE)
generate_vector_cnvrt_up_2(ULONG, ul, BYTE, cnvrt_clip, clip_positive_BYTE)

generate_vector_cnvrt(USHORT, SHORT, cnvrt_clip, clip_positive_SHORT)
generate_vector_cnvrt_up(ULONG, ul, SHORT, cnvrt_clip, clip_positive_SHORT)

generate_vector_cnvrt(ULONG, LONG, cnvrt_clip, clip_positive_LONG)

/* Clipping where we only need to clip the biggest values */

generate_clip_upper(BYTE, UBYTE)
generate_vector_cnvrt(BYTE, UBYTE, cnvrt_clip, clip_upper_BYTE_UBYTE)

generate_clip_upper(UBYTE, USHORT)
generate_clip_upper(BYTE, USHORT)
generate_clip_upper(SHORT, USHORT)
generate_vector_cnvrt_down(UBYTE, USHORT, cnvrt_clip, clip_upper_UBYTE_USHORT)
generate_vector_cnvrt_down(BYTE, USHORT, cnvrt_clip, clip_upper_BYTE_USHORT)
generate_vector_cnvrt(SHORT, USHORT, cnvrt_clip, clip_upper_SHORT_USHORT)

#ifdef simd_splat_LONG
generate_clip_upper(UBYTE, ULONG)
generate_clip_upper(BYTE, ULONG)
generate_clip_upper(USHORT, ULONG)
generate_clip_upper(SHORT, ULONG)
generate_clip_upper(LONG, ULONG)
generate_vector_cnvrt_down_2(UBYTE, ULONG, cnvrt_clip, clip_upper_UBYTE_ULONG)
generate_vector_cnvrt_down_2(BYTE, ULONG, cnvrt_clip, clip_upper_BYTE_ULONG)
generate_vector_cnvrt_down(SHORT, ULONG, cnvrt_clip, clip_upper_SHORT_ULONG)
generate_vector_cnvrt_down(USHORT, ULONG, cnvrt_clip, clip_upper_USHORT_ULONG)
generate_vector_cnvrt(LONG, ULONG, cnvrt_clip, clip_upper_LONG_ULONG)
#endif

/* Clipping at both ends of the range */
generate_clip(UBYTE, SHORT)
generate_clip(BYTE, SHORT)
generate_vector_cnvrt_down(UBYTE, SHORT, cnvrt_clip, clip_UBYTE_SHORT)
generate_vector_cnvrt_down(BYTE, SHORT, cnvrt_clip, clip_BYTE_SHORT)

#ifdef simd_splat_LONG
generate_clip(UBYTE, LONG)
generate_clip(BYTE, LONG)
generate_clip(USHORT, LONG)
generate_clip(SHORT, LONG)
generate_vector_cnvrt_down_2(UBYTE, LONG, cnvrt_clip, clip_UBYTE_LONG)
generate_vector_cnvrt_down_2(BYTE, LONG, cnvrt_clip, clip_BYTE_LONG)
generate_vector_cnvrt_down(USHORT, LONG, cnvrt_clip, clip_USHORT_LONG)
generate_vector_cnvrt_down(SHORT, LONG, cnvrt_clip, clip_SHORT_LONG)

#  ifdef simd_splat_FLOAT
/*
 * We implement these specially to ensure correct saturation at INT32_MAX
 * (LONG_MAX) which is not exactly representable in float.
 */
static simd_LONG vector_cnvrt_clip_LONG_FLOAT(simd_FLOAT x)
{
    vrep_FLOAT vx;
    vrep_LONG res;
    vx.v = x;
    for (int j=0; j<SIMD_NUM_ELEMENTS(FLOAT); j++) {
	int exceeded;
	float val = vx.e[j];
	exceeded = (val >= LONG_max);
	if (val <= LONG_min) val = LONG_min;
	res.e[j] = exceeded ? LONG_max : (LONG_type)val;
    }
    return res.v;
}
static simd_ULONG vector_cnvrt_clip_ULONG_FLOAT(simd_FLOAT x)
{
    vrep_FLOAT vx;
    vrep_ULONG res;
    vx.v = x;
    for (int j=0; j<SIMD_NUM_ELEMENTS(FLOAT); j++) {
	int exceeded;
	float val = vx.e[j];
	exceeded = (val >= ULONG_max);
	if (val <= 0.0) val = 0.0;
	res.e[j] = exceeded ? ULONG_max : (ULONG_type)val;
    }
    return res.v;
}
#  endif

#  ifdef simd_splat_DOUBLE
generate_clip(LONG, DOUBLE)
generate_clip(ULONG, DOUBLE)
generate_vector_cnvrt_down(LONG, DOUBLE, cnvrt_clip, clip_LONG_DOUBLE)
generate_vector_cnvrt_down(ULONG, DOUBLE, cnvrt_clip, clip_ULONG_DOUBLE)
#  endif
#endif

#if defined(simd_splat_FLOAT) && defined(simd_splat_DOUBLE)
generate_clip(FLOAT, DOUBLE)
generate_vector_cnvrt_down(FLOAT, DOUBLE, cnvrt_clip, clip_FLOAT_DOUBLE)
#endif


/*
 * Reference functions for rounding with clipping.
 * Only needed for floating point to (U)LONG conversions.
 */

#define generate_rnd_clip(dtype, stype, cnvrt)				\
    static inline dtype##_type rnd_clip_##dtype##_##stype(stype##_type x) \
    {									\
	if (x > dtype##_max) x = dtype##_max;				\
	if (x < dtype##_min) x = dtype##_min;				\
	return (dtype##_type)cnvrt(x);					\
    }


#ifdef simd_splat_LONG
#  ifdef simd_splat_FLOAT
/*
 * Implement these specially because the max values are not exactly
 * representable in single precision floating point.
 */
static simd_LONG vector_cnvrt_rnd_clip_LONG_FLOAT(simd_FLOAT x)
{
    vrep_FLOAT vx;
    vrep_LONG res;
    vx.v = x;
    for (int j=0; j<SIMD_NUM_ELEMENTS(FLOAT); j++) {
	int exceeded;
	float val = vx.e[j];
	exceeded = (val >= LONG_max);
	if (val <= LONG_min) val = LONG_min;
	res.e[j] = exceeded ? LONG_max : (LONG_type)roundf(val);
    }
    return res.v;
}
static simd_ULONG vector_cnvrt_rnd_clip_ULONG_FLOAT(simd_FLOAT x)
{
    vrep_FLOAT vx;
    vrep_ULONG res;
    vx.v = x;
    for (int j=0; j<SIMD_NUM_ELEMENTS(FLOAT); j++) {
	int exceeded;
	float val = vx.e[j];
	exceeded = (val >= ULONG_max);
	if (val <= 0.0) val = 0.0;
	res.e[j] = exceeded ? ULONG_max : (ULONG_type)roundf(val);
    }
    return res.v;
}
#  endif
#  ifdef simd_splat_DOUBLE
generate_rnd_clip(LONG, DOUBLE, round)
generate_rnd_clip(ULONG, DOUBLE, round)
generate_vector_cnvrt_down(LONG, DOUBLE, cnvrt_rnd_clip, rnd_clip_LONG_DOUBLE)
generate_vector_cnvrt_down(ULONG, DOUBLE, cnvrt_rnd_clip, rnd_clip_ULONG_DOUBLE)
#  endif
#endif

static void test_simd_cnvrt_X_byte(void)
{
    vany v;

    START_TEST();

    v.ub = vector_random_UBYTE();
    ensure_both_signs_UBYTE(&v.ub);

    /* Zero-extend ubyte */
#ifdef simd_cnvrt_USHORT_UBYTE
    TEST_IDENTICAL_D2_S1(USHORT, UBYTE, v.ub, simd_cnvrt_USHORT_UBYTE, vector_cnvrt_USHORT_UBYTE);
#endif

#ifdef simd_cnvrt_ULONG_UBYTE
    TEST_IDENTICAL_D4_S1(ULONG, UBYTE, v.ub, simd_cnvrt_ULONG_UBYTE, vector_cnvrt_ULONG_UBYTE);
#endif

    /* Sign-extend byte */
#ifdef simd_cnvrt_SHORT_BYTE
    TEST_IDENTICAL_D2_S1(SHORT, BYTE, v.b, simd_cnvrt_SHORT_BYTE, vector_cnvrt_SHORT_BYTE);
#endif

#ifdef simd_cnvrt_LONG_BYTE
    TEST_IDENTICAL_D4_S1(LONG, BYTE, v.b, simd_cnvrt_LONG_BYTE, vector_cnvrt_LONG_BYTE);
#endif

    END_TEST();
}


static void test_simd_cnvrt_X_short(void)
{
    vany u, v, w;

    START_TEST();

    u.us = vector_important_values_USHORT(1);
    v.us = vector_important_values_USHORT(2);
    w.us = vector_important_values_USHORT(3);

    /* Zero extend ushort */
#ifdef simd_cnvrt_ULONG_USHORT
    TEST_IDENTICAL_D2_S1(ULONG, USHORT, u.us, simd_cnvrt_ULONG_USHORT, vector_cnvrt_ULONG_USHORT);
    TEST_IDENTICAL_D2_S1(ULONG, USHORT, v.us, simd_cnvrt_ULONG_USHORT, vector_cnvrt_ULONG_USHORT);
    TEST_IDENTICAL_D2_S1(ULONG, USHORT, w.us, simd_cnvrt_ULONG_USHORT, vector_cnvrt_ULONG_USHORT);
#endif

    /* Truncate (u)short */
#ifdef simd_cnvrt_UBYTE_USHORT
    TEST_IDENTICAL_D1_S2(UBYTE, USHORT, u.us, v.us, simd_cnvrt_UBYTE_USHORT, vector_cnvrt_UBYTE_USHORT);
    TEST_IDENTICAL_D1_S2(UBYTE, USHORT, v.us, w.us, simd_cnvrt_UBYTE_USHORT, vector_cnvrt_UBYTE_USHORT);
#endif

    u.s = vector_important_values_SHORT(1);
    v.s = vector_important_values_SHORT(2);
    w.s = vector_important_values_SHORT(3);

    /* Sign-extend short */
#ifdef simd_cnvrt_LONG_SHORT
    TEST_IDENTICAL_D2_S1(LONG, SHORT, u.s, simd_cnvrt_LONG_SHORT, vector_cnvrt_LONG_SHORT);
    TEST_IDENTICAL_D2_S1(LONG, SHORT, v.s, simd_cnvrt_LONG_SHORT, vector_cnvrt_LONG_SHORT);
    TEST_IDENTICAL_D2_S1(LONG, SHORT, w.s, simd_cnvrt_LONG_SHORT, vector_cnvrt_LONG_SHORT);
#endif

    END_TEST();
}


static void test_simd_cnvrt_X_long(void)
{
    vany v, w, x, y;

    START_TEST();

    /* Truncate (u)long */
#ifdef simd_cnvrt_USHORT_ULONG
    v.ub = vector_random_UBYTE();
    ensure_both_signs_ULONG(&v.ul);
    w.ub = vector_random_UBYTE();
    ensure_both_signs_ULONG(&w.ul);

    TEST_IDENTICAL_D1_S2(USHORT, ULONG, v.ul, w.ul, simd_cnvrt_USHORT_ULONG, vector_cnvrt_USHORT_ULONG);
#endif

#ifdef simd_cnvrt_UBYTE_ULONG
    v.ub = vector_random_UBYTE();
    ensure_both_signs_ULONG(&v.ul);
    w.ub = vector_random_UBYTE();
    ensure_both_signs_ULONG(&w.ul);
    x.ub = vector_random_UBYTE();
    ensure_both_signs_ULONG(&x.ul);
    y.ub = vector_random_UBYTE();
    ensure_both_signs_ULONG(&y.ul);

    TEST_IDENTICAL_D1_S4(UBYTE, ULONG, v.ul, w.ul, x.ul, y.ul, simd_cnvrt_UBYTE_ULONG, vector_cnvrt_UBYTE_ULONG);
#endif

#ifdef simd_cnvrt_FLOAT_LONG
    v.ub = vector_random_UBYTE();
    ensure_both_signs_ULONG(&v.ul);
    TEST_IDENTICAL_D1_S1(FLOAT, LONG, v.l, simd_cnvrt_FLOAT_LONG, vector_cnvrt_FLOAT_LONG);
#endif

#ifdef simd_cnvrt_FLOAT_ULONG
    v.ub = vector_random_UBYTE();
    TEST_IDENTICAL_D1_S1(FLOAT, ULONG, v.ul, simd_cnvrt_FLOAT_ULONG, vector_cnvrt_FLOAT_ULONG);
#endif

#ifdef simd_cnvrt_DOUBLE_LONG
    v.ub = vector_random_UBYTE();
    ensure_both_signs_ULONG(&v.ul);
    TEST_IDENTICAL_D2_S1(DOUBLE, LONG, v.l, simd_cnvrt_DOUBLE_LONG, vector_cnvrt_DOUBLE_LONG);
#endif

#ifdef simd_cnvrt_DOUBLE_ULONG
    v.ub = vector_random_UBYTE();
    TEST_IDENTICAL_D2_S1(DOUBLE, ULONG, v.ul, simd_cnvrt_DOUBLE_ULONG, vector_cnvrt_DOUBLE_ULONG);
#endif

    END_TEST();
}


#ifdef simd_splat_ULONG64
static void test_simd_cnvrt_X_long64(void)
{
    vany v, w;

    START_TEST();

    /* Truncate (u)long */
#ifdef simd_cnvrt_ULONG_ULONG64
    v.ub = vector_random_UBYTE();
    ensure_both_signs_ULONG64(&v.ull);
    w.ub = vector_random_UBYTE();
    ensure_both_signs_ULONG64(&w.ull);

    TEST_IDENTICAL_D1_S2(ULONG, ULONG64, v.ull, w.ull, simd_cnvrt_ULONG_ULONG64, vector_cnvrt_ULONG_ULONG64);
#endif

    END_TEST();
}
#endif


#ifdef simd_splat_FLOAT
static void test_simd_cnvrt_X_float(void)
{
    vany u, v, w;

    START_TEST();

    /* Convert float to int by truncation of fraction */
#ifdef simd_cnvrt_LONG_FLOAT
    u.f = vector_random_i32_FLOAT();
    u.f = vector_set_element_FLOAT(u.f, 0, nextafterf(0.5, 0));
    u.f = vector_set_element_FLOAT(u.f, 1, 0.5);
    v.f = vector_random_i32_FLOAT();
    v.f = vector_set_element_FLOAT(v.f, 0, nextafterf(0.5, 1));
    v.f = vector_set_element_FLOAT(v.f, 1, -37563422.1);
    w.f = vector_random_i32_FLOAT();
    w.f = vector_set_element_FLOAT(w.f, 0, -nextafterf(0.5, 1));
    /* INT32_MAX-128 is exactly reprsentable as float whereas INT32_MAX isn't */
    w.f = vector_set_element_FLOAT(w.f, 1, (float)(INT32_MAX - 128));
    TEST_IDENTICAL_D1_S1(LONG, FLOAT, u.f, simd_cnvrt_LONG_FLOAT, vector_cnvrt_LONG_FLOAT);
    TEST_IDENTICAL_D1_S1(LONG, FLOAT, v.f, simd_cnvrt_LONG_FLOAT, vector_cnvrt_LONG_FLOAT);
    TEST_IDENTICAL_D1_S1(LONG, FLOAT, w.f, simd_cnvrt_LONG_FLOAT, vector_cnvrt_LONG_FLOAT);
#endif

#ifdef simd_cnvrt_ULONG_FLOAT
    u.f = vector_random_u32_FLOAT();
    u.f = vector_set_element_FLOAT(u.f, 1, 0.5);
    v.f = vector_random_u32_FLOAT();
    v.f = vector_set_element_FLOAT(v.f, 1, 784653.2134);
    w.f = vector_random_u32_FLOAT();
    w.f = vector_set_element_FLOAT(w.f, 1, nextafterf(0.5, 1.0));
    TEST_IDENTICAL_D1_S1(ULONG, FLOAT, u.f, simd_cnvrt_ULONG_FLOAT, vector_cnvrt_ULONG_FLOAT);
    TEST_IDENTICAL_D1_S1(ULONG, FLOAT, v.f, simd_cnvrt_ULONG_FLOAT, vector_cnvrt_ULONG_FLOAT);
    TEST_IDENTICAL_D1_S1(ULONG, FLOAT, w.f, simd_cnvrt_ULONG_FLOAT, vector_cnvrt_ULONG_FLOAT);
#endif

    /* Convert float to double */
#ifdef simd_cnvrt_DOUBLE_FLOAT
    u.f = vector_random_FLOAT(2e4);
    u.f = vector_set_element_FLOAT(u.f, 1, 0.5);
    v.f = vector_random_FLOAT(1e-5);
    v.f = vector_set_element_FLOAT(v.f, 1, -37563422.1);
    TEST_IDENTICAL_D2_S1(DOUBLE, FLOAT, u.f, simd_cnvrt_DOUBLE_FLOAT, vector_cnvrt_DOUBLE_FLOAT);
    TEST_IDENTICAL_D2_S1(DOUBLE, FLOAT, v.f, simd_cnvrt_DOUBLE_FLOAT, vector_cnvrt_DOUBLE_FLOAT);
#endif

    END_TEST();
}
#endif


#ifdef simd_splat_FLOAT
static void test_simd_cnvrt_rnd_X_float(void)
{
    vany u, v, w;

    START_TEST();

    /* Convert float to int by truncation of fraction */
#ifdef simd_cnvrt_rnd_LONG_FLOAT
    u.f = vector_random_i32_FLOAT();
    u.f = vector_set_element_FLOAT(u.f, 0, nextafterf(0.5, 0));
    u.f = vector_set_element_FLOAT(u.f, 1, 0.5);
    v.f = vector_random_i32_FLOAT();
    v.f = vector_set_element_FLOAT(v.f, 0, nextafterf(0.5, 1));
    v.f = vector_set_element_FLOAT(v.f, 1, -0.5);
    w.f = vector_random_i32_FLOAT();
    w.f = vector_set_element_FLOAT(w.f, 0, nextafterf(1, 0));
    w.f = vector_set_element_FLOAT(w.f, 1, 1.0);

    TEST_IDENTICAL_D1_S1(LONG, FLOAT, u.f, simd_cnvrt_rnd_LONG_FLOAT, vector_cnvrt_rnd_LONG_FLOAT);
    TEST_IDENTICAL_D1_S1(LONG, FLOAT, v.f, simd_cnvrt_rnd_LONG_FLOAT, vector_cnvrt_rnd_LONG_FLOAT);
    TEST_IDENTICAL_D1_S1(LONG, FLOAT, w.f, simd_cnvrt_rnd_LONG_FLOAT, vector_cnvrt_rnd_LONG_FLOAT);
#endif

#ifdef simd_cnvrt_rnd_ULONG_FLOAT
    u.f = vector_random_u32_FLOAT();
    u.f = vector_set_element_FLOAT(u.f, 0, nextafterf(0.5, 0.0));
    u.f = vector_set_element_FLOAT(u.f, 1, 0.5);
    v.f = vector_random_u32_FLOAT();
    v.f = vector_set_element_FLOAT(v.f, 1, 784653.2134);
    w.f = vector_random_u32_FLOAT();
    w.f = vector_set_element_FLOAT(w.f, 0, nextafterf(1.0, 0));
    w.f = vector_set_element_FLOAT(w.f, 1, 1.0);
    TEST_IDENTICAL_D1_S1(ULONG, FLOAT, u.f, simd_cnvrt_rnd_ULONG_FLOAT, vector_cnvrt_rnd_ULONG_FLOAT);
    TEST_IDENTICAL_D1_S1(ULONG, FLOAT, v.f, simd_cnvrt_rnd_ULONG_FLOAT, vector_cnvrt_rnd_ULONG_FLOAT);
    TEST_IDENTICAL_D1_S1(ULONG, FLOAT, w.f, simd_cnvrt_rnd_ULONG_FLOAT, vector_cnvrt_rnd_ULONG_FLOAT);
#endif

    END_TEST();
}
#endif


#ifdef simd_splat_DOUBLE
static void test_simd_cnvrt_X_double(void)
{
    vany u, v, w;

    START_TEST();

    /* Convert float to int by truncation of fraction */
#ifdef simd_cnvrt_LONG_DOUBLE
    u.d = vector_random_i32_DOUBLE();
    u.d = vector_set_element_DOUBLE(u.d, 0, nextafter(0.5, 0));
    u.d = vector_set_element_DOUBLE(u.d, 1, 0.5);
    v.d = vector_random_i32_DOUBLE();
    v.d = vector_set_element_DOUBLE(v.d, 0, 37563422.09);
    v.d = vector_set_element_DOUBLE(v.d, 1, -37563422.1);
    w.d = vector_random_i32_DOUBLE();
    w.d = vector_set_element_DOUBLE(w.d, 1, nextafter(0.5, 1));
    TEST_IDENTICAL_D1_S2(LONG, DOUBLE, u.d, v.d, simd_cnvrt_LONG_DOUBLE, vector_cnvrt_LONG_DOUBLE);
    TEST_IDENTICAL_D1_S2(LONG, DOUBLE, v.d, w.d, simd_cnvrt_LONG_DOUBLE, vector_cnvrt_LONG_DOUBLE);
#endif

#ifdef simd_cnvrt_ULONG_DOUBLE
    u.d = vector_set_element_DOUBLE(u.d, 0, nextafter(0.5, 0));
    u.d = vector_set_element_DOUBLE(u.d, 1, 0.50000);
    v.d = vector_set_element_DOUBLE(v.d, 0, 37563422.1);
    v.d = vector_set_element_DOUBLE(v.d, 1, nextafter(0.5, 1));
    TEST_IDENTICAL_D1_S2(ULONG, DOUBLE, u.d, v.d, simd_cnvrt_ULONG_DOUBLE, vector_cnvrt_ULONG_DOUBLE);
    u.d = vector_set_element_DOUBLE(u.d, 0, 4.164524185746e9);
    u.d = vector_set_element_DOUBLE(u.d, 1, 2147483648.1);
    v.d = vector_set_element_DOUBLE(v.d, 0, 2147483647.9);
    v.d = vector_set_element_DOUBLE(v.d, 1, 2147483648.0);
    TEST_IDENTICAL_D1_S2(ULONG, DOUBLE, u.d, v.d, simd_cnvrt_ULONG_DOUBLE, vector_cnvrt_ULONG_DOUBLE);

    u.d = vector_set_element_DOUBLE(u.d, 0, (double)UINT32_MAX);
    u.d = vector_set_element_DOUBLE(u.d, 1, (double)UINT32_MAX-1);
    v.d = vector_set_element_DOUBLE(v.d, 0, (double)1.09);
    v.d = vector_set_element_DOUBLE(v.d, 1, -0.999);
    TEST_IDENTICAL_D1_S2(ULONG, DOUBLE, u.d, v.d, simd_cnvrt_ULONG_DOUBLE, vector_cnvrt_ULONG_DOUBLE);
#endif

    /* Convert double to float */
#ifdef simd_cnvrt_FLOAT_DOUBLE
    u.d = vector_random_DOUBLE(1.1);
    u.d = vector_set_element_DOUBLE(u.d, 1, 0.5);
    v.d = vector_random_DOUBLE(8e21);
    v.d = vector_set_element_DOUBLE(v.d, 1, -37563422.1);
    TEST_IDENTICAL_D1_S2(FLOAT, DOUBLE, u.d, v.d, simd_cnvrt_FLOAT_DOUBLE, vector_cnvrt_FLOAT_DOUBLE);
#endif

    END_TEST();
}
#endif


#ifdef simd_splat_DOUBLE
static void test_simd_cnvrt_rnd_X_double(void)
{
    vany u, v, w;

    START_TEST();

    /* Convert float to int by truncation of fraction */
#ifdef simd_cnvrt_rnd_LONG_DOUBLE
    u.d = vector_random_i32_DOUBLE();
    u.d = vector_set_element_DOUBLE(u.d, 0, nextafter(0.5, 0));
    u.d = vector_set_element_DOUBLE(u.d, 1, 0.5);
    v.d = vector_random_i32_DOUBLE();
    v.d = vector_set_element_DOUBLE(v.d, 0, 37563422.09);
    v.d = vector_set_element_DOUBLE(v.d, 1, -37563422.1);
    w.d = vector_random_i32_DOUBLE();
    w.d = vector_set_element_DOUBLE(w.d, 1, nextafter(0.5, 1));
    TEST_IDENTICAL_D1_S2(LONG, DOUBLE, u.d, v.d, simd_cnvrt_rnd_LONG_DOUBLE, vector_cnvrt_rnd_LONG_DOUBLE);
    TEST_IDENTICAL_D1_S2(LONG, DOUBLE, v.d, w.d, simd_cnvrt_rnd_LONG_DOUBLE, vector_cnvrt_rnd_LONG_DOUBLE);
#endif

#ifdef simd_cnvrt_rnd_ULONG_DOUBLE
    const double int32maxp1 = (double)INT32_MAX+1.0;
    u.d = vector_set_element_DOUBLE(u.d, 0, nextafter(0.5, 0));
    u.d = vector_set_element_DOUBLE(u.d, 1, 0.50000);
    v.d = vector_set_element_DOUBLE(v.d, 0, 37563422.1);
    v.d = vector_set_element_DOUBLE(v.d, 1, nextafter(0.5, 1));
    TEST_IDENTICAL_D1_S2(ULONG, DOUBLE, u.d, v.d, simd_cnvrt_rnd_ULONG_DOUBLE, vector_cnvrt_rnd_ULONG_DOUBLE);
    u.d = vector_set_element_DOUBLE(u.d, 0, 4.164524185746e9);
    u.d = vector_set_element_DOUBLE(u.d, 1, nextafter(int32maxp1, 0));
    v.d = vector_set_element_DOUBLE(v.d, 0, int32maxp1);
    v.d = vector_set_element_DOUBLE(v.d, 1, nextafter(int32maxp1, 30000000000));
    TEST_IDENTICAL_D1_S2(ULONG, DOUBLE, u.d, v.d, simd_cnvrt_rnd_ULONG_DOUBLE, vector_cnvrt_rnd_ULONG_DOUBLE);

    u.d = vector_set_element_DOUBLE(u.d, 0, (double)UINT32_MAX);
    u.d = vector_set_element_DOUBLE(u.d, 1, (double)UINT32_MAX-1);
    v.d = vector_set_element_DOUBLE(v.d, 0, (double)INT32_MAX+0.2);
    v.d = vector_set_element_DOUBLE(v.d, 1, 0.999);
    TEST_IDENTICAL_D1_S2(ULONG, DOUBLE, u.d, v.d, simd_cnvrt_rnd_ULONG_DOUBLE, vector_cnvrt_rnd_ULONG_DOUBLE);
#endif

    END_TEST();
}
#endif




static void test_simd_cnvrt_clip_X_byte(void)
{
    vany v;

    START_TEST();

    v.ub = vector_random_UBYTE();
    ensure_both_signs_UBYTE(&v.ub);

#ifdef simd_cnvrt_clip_UBYTE_BYTE
    TEST_IDENTICAL_D1_S1(UBYTE, BYTE, v.b, simd_cnvrt_clip_UBYTE_BYTE, vector_cnvrt_clip_UBYTE_BYTE);
#endif

#ifdef simd_cnvrt_clip_BYTE_UBYTE
    TEST_IDENTICAL_D1_S1(BYTE, UBYTE, v.ub, simd_cnvrt_clip_BYTE_UBYTE, vector_cnvrt_clip_BYTE_UBYTE);
#endif

#ifdef simd_cnvrt_clip_USHORT_BYTE
    TEST_IDENTICAL_D2_S1(USHORT, BYTE, v.b, simd_cnvrt_clip_USHORT_BYTE, vector_cnvrt_clip_USHORT_BYTE);
#endif

#ifdef simd_cnvrt_clip_ULONG_BYTE
    TEST_IDENTICAL_D4_S1(ULONG, BYTE, v.b, simd_cnvrt_clip_ULONG_BYTE, vector_cnvrt_clip_ULONG_BYTE);
#endif

    END_TEST();
}


static void test_simd_cnvrt_clip_X_short(void)
{
    vany v, w;

    START_TEST();

    v.ub = vector_random_UBYTE();
    ensure_both_signs_USHORT(&v.us);
    ensure_some_bytes_USHORT(&v.us);
    w.ub = vector_random_UBYTE();
    ensure_both_signs_USHORT(&w.us);
    ensure_some_bytes_USHORT(&w.us);

#ifdef simd_cnvrt_clip_UBYTE_USHORT
    TEST_IDENTICAL_D1_S2(UBYTE, USHORT, v.us, w.us, simd_cnvrt_clip_UBYTE_USHORT, vector_cnvrt_clip_UBYTE_USHORT);
#endif

#ifdef simd_cnvrt_clip_BYTE_USHORT
    TEST_IDENTICAL_D1_S2(BYTE, USHORT, v.us, w.us, simd_cnvrt_clip_BYTE_USHORT, vector_cnvrt_clip_BYTE_USHORT);
#endif

#ifdef simd_cnvrt_clip_UBYTE_SHORT
    TEST_IDENTICAL_D1_S2(UBYTE, SHORT, v.s, w.s, simd_cnvrt_clip_UBYTE_SHORT, vector_cnvrt_clip_UBYTE_SHORT);
#endif

#ifdef simd_cnvrt_clip_BYTE_SHORT
    TEST_IDENTICAL_D1_S2(BYTE, SHORT, v.s, w.s, simd_cnvrt_clip_BYTE_SHORT, vector_cnvrt_clip_BYTE_SHORT);
#endif

#ifdef simd_cnvrt_clip_USHORT_SHORT
    TEST_IDENTICAL_D1_S1(USHORT, SHORT, v.s, simd_cnvrt_clip_USHORT_SHORT, vector_cnvrt_clip_USHORT_SHORT);
#endif

#ifdef simd_cnvrt_clip_SHORT_USHORT
    TEST_IDENTICAL_D1_S1(SHORT, USHORT, v.us, simd_cnvrt_clip_SHORT_USHORT, vector_cnvrt_clip_SHORT_USHORT);
#endif

#ifdef simd_cnvrt_clip_ULONG_SHORT
    TEST_IDENTICAL_D2_S1(ULONG, SHORT, v.s, simd_cnvrt_clip_ULONG_SHORT, vector_cnvrt_clip_ULONG_SHORT);
#endif

    END_TEST();
}


static void test_simd_cnvrt_clip_X_long(void)
{
    vany v, w, x, y;

    START_TEST();

    v.ub = vector_random_UBYTE();
    ensure_both_signs_ULONG(&v.ul);
    w.ub = vector_random_UBYTE();
    ensure_some_bytes_ULONG(&w.ul);
    x.ub = vector_random_UBYTE();
    ensure_both_signs_ULONG(&x.ul);
    y.ub = vector_random_UBYTE();
    ensure_some_bytes_ULONG(&y.ul);

#ifdef simd_cnvrt_clip_USHORT_ULONG
    TEST_IDENTICAL_D1_S2(USHORT, ULONG, v.ul, w.ul, simd_cnvrt_clip_USHORT_ULONG, vector_cnvrt_clip_USHORT_ULONG);
#endif

#ifdef simd_cnvrt_clip_SHORT_ULONG
    TEST_IDENTICAL_D1_S2(SHORT, ULONG, v.ul, w.ul, simd_cnvrt_clip_SHORT_ULONG, vector_cnvrt_clip_SHORT_ULONG);
#endif

#ifdef simd_cnvrt_clip_USHORT_LONG
    TEST_IDENTICAL_D1_S2(USHORT, LONG, v.l, w.l, simd_cnvrt_clip_USHORT_LONG, vector_cnvrt_clip_USHORT_LONG);
#endif

#ifdef simd_cnvrt_clip_SHORT_LONG
    TEST_IDENTICAL_D1_S2(SHORT, LONG, v.l, w.l, simd_cnvrt_clip_SHORT_LONG, vector_cnvrt_clip_SHORT_LONG);
#endif

    ensure_some_bytes_ULONG(&x.ul);

#ifdef simd_cnvrt_clip_UBYTE_ULONG
    TEST_IDENTICAL_D1_S4(UBYTE, ULONG, v.ul, w.ul, x.ul, y.ul, simd_cnvrt_clip_UBYTE_ULONG, vector_cnvrt_clip_UBYTE_ULONG);
#endif

#ifdef simd_cnvrt_clip_BYTE_ULONG
    TEST_IDENTICAL_D1_S4(BYTE, ULONG, v.ul, w.ul, x.ul, y.ul, simd_cnvrt_clip_BYTE_ULONG, vector_cnvrt_clip_BYTE_ULONG);
#endif

#ifdef simd_cnvrt_clip_UBYTE_LONG
    TEST_IDENTICAL_D1_S4(UBYTE, LONG, v.l, w.l, x.l, y.l, simd_cnvrt_clip_UBYTE_LONG, vector_cnvrt_clip_UBYTE_LONG);
#endif

#ifdef simd_cnvrt_clip_BYTE_LONG
    TEST_IDENTICAL_D1_S4(BYTE, LONG, v.l, w.l, x.l, y.l, simd_cnvrt_clip_BYTE_LONG, vector_cnvrt_clip_BYTE_LONG);
#endif

#ifdef simd_cnvrt_clip_LONG_ULONG
    TEST_IDENTICAL_D1_S1(LONG, ULONG, v.ul, simd_cnvrt_clip_LONG_ULONG, vector_cnvrt_clip_LONG_ULONG);
#endif

#ifdef simd_cnvrt_clip_ULONG_LONG
    TEST_IDENTICAL_D1_S1(ULONG, LONG, v.l, simd_cnvrt_clip_ULONG_LONG, vector_cnvrt_clip_ULONG_LONG);
#endif

    END_TEST();
}


#ifdef simd_splat_FLOAT
static void test_simd_cnvrt_clip_X_float(void)
{
    vany u, v, w;

    START_TEST();

    /* Convert float to int by truncation of fraction with clipping */
#ifdef simd_cnvrt_clip_LONG_FLOAT
    u.f = vector_random_i32_FLOAT();
    u.f = vector_set_element_FLOAT(u.f, 1, 1.46352e21);
    if (SIMD_NUM_ELEMENTS(FLOAT) > 2)
	u.f = vector_set_element_FLOAT(u.f, 2, INT32_MAX);
    v.f = vector_random_i32_FLOAT();
    v.f = vector_set_element_FLOAT(v.f, 1, -3.198137e17);
    w.f = vector_random_u32_FLOAT();
    w.f = vector_set_element_FLOAT(w.f, 1, 0.500001);
    TEST_IDENTICAL_D1_S1(LONG, FLOAT, u.f, simd_cnvrt_clip_LONG_FLOAT, vector_cnvrt_clip_LONG_FLOAT);
    TEST_IDENTICAL_D1_S1(LONG, FLOAT, v.f, simd_cnvrt_clip_LONG_FLOAT, vector_cnvrt_clip_LONG_FLOAT);
    TEST_IDENTICAL_D1_S1(LONG, FLOAT, w.f, simd_cnvrt_clip_LONG_FLOAT, vector_cnvrt_clip_LONG_FLOAT);
#endif

#ifdef simd_cnvrt_clip_ULONG_FLOAT
    u.f = vector_random_u32_FLOAT();
    u.f = vector_set_element_FLOAT(u.f, 1, 1.46352e21);
    if (SIMD_NUM_ELEMENTS(FLOAT) > 2)
	u.f = vector_set_element_FLOAT(u.f, 2, UINT32_MAX);
    v.f = vector_random_u32_FLOAT();
    v.f = vector_set_element_FLOAT(v.f, 1, -3.198137e17);
    w.f = vector_random_i32_FLOAT();
    w.f = vector_set_element_FLOAT(w.f, 1, 0.500001);
    TEST_IDENTICAL_D1_S1(ULONG, FLOAT, u.f, simd_cnvrt_clip_ULONG_FLOAT, vector_cnvrt_clip_ULONG_FLOAT);
    TEST_IDENTICAL_D1_S1(ULONG, FLOAT, v.f, simd_cnvrt_clip_ULONG_FLOAT, vector_cnvrt_clip_ULONG_FLOAT);
    TEST_IDENTICAL_D1_S1(ULONG, FLOAT, w.f, simd_cnvrt_clip_ULONG_FLOAT, vector_cnvrt_clip_ULONG_FLOAT);
#endif

    END_TEST();
}
#endif


#ifdef simd_splat_DOUBLE
static void test_simd_cnvrt_clip_X_double(void)
{
    vany u, v, w;

    START_TEST();

    /* Convert double to int by truncation of fraction */
#ifdef simd_cnvrt_clip_LONG_DOUBLE
    u.d = vector_random_i32_DOUBLE();
    u.d = vector_set_element_DOUBLE(u.d, 1, 6.34625412e11);
    v.d = vector_random_i32_DOUBLE();
    v.d = vector_set_element_DOUBLE(v.d, 1, -3.111625111e12);
    w.d = vector_random_u32_DOUBLE();
    w.d = vector_set_element_DOUBLE(w.d, 1, 0.500001);
    TEST_IDENTICAL_D1_S2(LONG, DOUBLE, u.d, v.d, simd_cnvrt_clip_LONG_DOUBLE, vector_cnvrt_clip_LONG_DOUBLE);
    TEST_IDENTICAL_D1_S2(LONG, DOUBLE, v.d, w.d, simd_cnvrt_clip_LONG_DOUBLE, vector_cnvrt_clip_LONG_DOUBLE);
#endif

#ifdef simd_cnvrt_clip_ULONG_DOUBLE
    TEST_IDENTICAL_D1_S2(ULONG, DOUBLE, u.d, v.d, simd_cnvrt_clip_ULONG_DOUBLE, vector_cnvrt_clip_ULONG_DOUBLE);
    TEST_IDENTICAL_D1_S2(ULONG, DOUBLE, v.d, w.d, simd_cnvrt_clip_ULONG_DOUBLE, vector_cnvrt_clip_ULONG_DOUBLE);
#endif


    /* Convert double to float */
#ifdef simd_cnvrt_clip_FLOAT_DOUBLE
    u.d = vector_random_DOUBLE(1e-5);
    u.d = vector_set_element_DOUBLE(u.d, 1, 1.11111e41);
    v.d = vector_random_DOUBLE(1e5);
    v.d = vector_set_element_DOUBLE(v.d, 1, -3.75634221e40);
    TEST_IDENTICAL_D1_S2(FLOAT, DOUBLE, u.d, v.d, simd_cnvrt_clip_FLOAT_DOUBLE, vector_cnvrt_clip_FLOAT_DOUBLE);
#endif

    END_TEST();
}
#endif

#ifdef simd_splat_FLOAT
static void test_simd_cnvrt_rnd_clip_X_float(void)
{
    vany u, v, w;

    START_TEST();

    /* Convert float to int by truncation of fraction with clipping */
#ifdef simd_cnvrt_rnd_clip_LONG_FLOAT
    u.f = vector_random_i32_FLOAT();
    u.f = vector_set_element_FLOAT(u.f, 0, INT32_MAX);
    u.f = vector_set_element_FLOAT(u.f, 1, 1.46352e21);
    v.f = vector_random_i32_FLOAT();
    v.f = vector_set_element_FLOAT(v.f, 1, -3.198137e17);
    w.f = vector_random_u32_FLOAT();
    w.f = vector_set_element_FLOAT(w.f, 1, 0.500001);
    TEST_IDENTICAL_D1_S1(LONG, FLOAT, u.f, simd_cnvrt_rnd_clip_LONG_FLOAT, vector_cnvrt_rnd_clip_LONG_FLOAT);
    TEST_IDENTICAL_D1_S1(LONG, FLOAT, v.f, simd_cnvrt_rnd_clip_LONG_FLOAT, vector_cnvrt_rnd_clip_LONG_FLOAT);
    TEST_IDENTICAL_D1_S1(LONG, FLOAT, w.f, simd_cnvrt_rnd_clip_LONG_FLOAT, vector_cnvrt_rnd_clip_LONG_FLOAT);
#endif

#ifdef simd_cnvrt_rnd_clip_ULONG_FLOAT
    u.f = vector_random_u32_FLOAT();
    u.f = vector_set_element_FLOAT(u.f, 1, 1.46352e21);
    if (SIMD_NUM_ELEMENTS(FLOAT) > 2)
	u.f = vector_set_element_FLOAT(u.f, 2, UINT32_MAX);
    v.f = vector_random_u32_FLOAT();
    v.f = vector_set_element_FLOAT(v.f, 1, -3.198137e17);
    w.f = vector_random_i32_FLOAT();
    w.f = vector_set_element_FLOAT(w.f, 1, 0.500001);
    TEST_IDENTICAL_D1_S1(ULONG, FLOAT, u.f, simd_cnvrt_rnd_clip_ULONG_FLOAT, vector_cnvrt_rnd_clip_ULONG_FLOAT);
    TEST_IDENTICAL_D1_S1(ULONG, FLOAT, v.f, simd_cnvrt_rnd_clip_ULONG_FLOAT, vector_cnvrt_rnd_clip_ULONG_FLOAT);
    TEST_IDENTICAL_D1_S1(ULONG, FLOAT, w.f, simd_cnvrt_rnd_clip_ULONG_FLOAT, vector_cnvrt_rnd_clip_ULONG_FLOAT);
#endif

    END_TEST();
}
#endif


#ifdef simd_splat_DOUBLE
static void test_simd_cnvrt_rnd_clip_X_double(void)
{
    vany u, v, w;

    START_TEST();

    /* Convert double to int by truncation of fraction */
#ifdef simd_cnvrt_rnd_clip_LONG_DOUBLE
    u.d = vector_random_i32_DOUBLE();
    u.d = vector_set_element_DOUBLE(u.d, 1, 6.34625412e11);
    v.d = vector_random_i32_DOUBLE();
    v.d = vector_set_element_DOUBLE(v.d, 1, -3.111625111e12);
    w.d = vector_random_u32_DOUBLE();
    w.d = vector_set_element_DOUBLE(w.d, 1, 0.500001);
    TEST_IDENTICAL_D1_S2(LONG, DOUBLE, u.d, v.d, simd_cnvrt_rnd_clip_LONG_DOUBLE, vector_cnvrt_rnd_clip_LONG_DOUBLE);
    TEST_IDENTICAL_D1_S2(LONG, DOUBLE, v.d, w.d, simd_cnvrt_rnd_clip_LONG_DOUBLE, vector_cnvrt_rnd_clip_LONG_DOUBLE);
#endif

#ifdef simd_cnvrt_rnd_clip_ULONG_DOUBLE
    TEST_IDENTICAL_D1_S2(ULONG, DOUBLE, u.d, v.d, simd_cnvrt_rnd_clip_ULONG_DOUBLE, vector_cnvrt_rnd_clip_ULONG_DOUBLE);
    TEST_IDENTICAL_D1_S2(ULONG, DOUBLE, v.d, w.d, simd_cnvrt_rnd_clip_ULONG_DOUBLE, vector_cnvrt_rnd_clip_ULONG_DOUBLE);
#endif

    END_TEST();
}
#endif


/*
 * Support for transpose testing follows
 */


#define SWAP_UBYTE(a,b) ({uint8_t t; t=b; b=a; a=t;})
#define SWAP_USHORT(a,b) ({uint16_t t; t=b; b=a; a=t;})
#define SWAP_ULONG(a,b) ({uint32_t t; t=b; b=a; a=t;})
#define SWAP_FLOAT(a,b) ({float t; t=b; b=a; a=t;})
#define SWAP_DOUBLE(a,b) ({double t; t=b; b=a; a=t;})
#define SWAP_DCOMPLEX(a,b) ({ip_dcomplex t; t=b; b=a; a=t;})

#define generate_vector_transpose_2x2(type, tt)				\
    static inline simd_##type##_2 vector_transpose_##type(simd_##type##_2 x) \
    {									\
	vrep_##type va, vb;						\
	simd_##type##_2 res;						\
	va.v = x.tt[0];							\
	vb.v = x.tt[1];							\
	SWAP_##type(va.e[1], vb.e[0]);					\
	res.tt[0] = va.v;						\
	res.tt[1] = vb.v;						\
	return res;							\
    }

#define generate_vector_transpose_4x4(type, tt)				\
    static inline simd_##type##_4 vector_transpose_##type(simd_##type##_4 x) \
    {									\
	vrep_##type va, vb, vc, vd;					\
	simd_##type##_4 res;						\
	va.v = x.tt[0];							\
	vb.v = x.tt[1];							\
	vc.v = x.tt[2];							\
	vd.v = x.tt[3];							\
	SWAP_##type(va.e[1], vb.e[0]);					\
	SWAP_##type(va.e[2], vc.e[0]);					\
	SWAP_##type(va.e[3], vd.e[0]);					\
	SWAP_##type(vb.e[2], vc.e[1]);					\
	SWAP_##type(vb.e[3], vd.e[1]);					\
	SWAP_##type(vc.e[3], vd.e[2]);					\
	res.tt[0] = va.v;						\
	res.tt[1] = vb.v;						\
	res.tt[2] = vc.v;						\
	res.tt[3] = vd.v;						\
	return res;							\
    }

#define generate_vector_transpose_8x8(type, tt)				\
    static inline simd_##type##_8 vector_transpose_##type(simd_##type##_8 x) \
    {									\
	vrep_##type vx[8];						\
	simd_##type##_8 res;						\
	for (int i=0; i<8; i++)						\
	    vx[i].v = x.tt[i];						\
	for (int i=0; i<8; i++) {					\
	    for (int j=i+1; j<8; j++) {					\
		SWAP_##type(vx[i].e[j], vx[j].e[i]);			\
	    }								\
	}								\
	for (int i=0; i<8; i++)						\
	    res.tt[i] = vx[i].v;					\
	return res;							\
    }


#if SIMD_VECTOR_SIZE == 16

/* Transpose tests for SIMD implementations of 16-byte vectors */

generate_vector_transpose_8x8(USHORT, us)
#ifdef simd_splat_ULONG
generate_vector_transpose_4x4(ULONG, ul)
#endif
#ifdef simd_splat_FLOAT
generate_vector_transpose_4x4(FLOAT, f)
#endif
#ifdef simd_splat_DOUBLE
generate_vector_transpose_2x2(DOUBLE, d)
#endif

static void test_simd_transpose(void)
{
    int last;
    double dlast;

    START_TEST();

#ifdef simd_transpose_USHORT
    simd_USHORT_8 vus;

    vus.us[0] = vector_set_incr_USHORT(0, &last);
    vus.us[1] = vector_set_incr_USHORT(last, &last);
    vus.us[2] = vector_set_incr_USHORT(last, &last);
    vus.us[3] = vector_set_incr_USHORT(last, &last);
    vus.us[4] = vector_set_incr_USHORT(last, &last);
    vus.us[5] = vector_set_incr_USHORT(last, &last);
    vus.us[6] = vector_set_incr_USHORT(last, &last);
    vus.us[7] = vector_set_incr_USHORT(last, &last);

    TEST_IDENTICAL_D8_S8(USHORT, vus, us, simd_transpose_USHORT, vector_transpose_USHORT);
#endif

#ifdef simd_transpose_ULONG
    simd_ULONG_4 vul;

    vul.ul[0] = vector_set_incr_ULONG(0, &last);
    vul.ul[1] = vector_set_incr_ULONG(last, &last);
    vul.ul[2] = vector_set_incr_ULONG(last, &last);
    vul.ul[3] = vector_set_incr_ULONG(last, &last);

    TEST_IDENTICAL_D4_S4(ULONG, vul, ul, simd_transpose_ULONG, vector_transpose_ULONG);
#endif

#ifdef simd_transpose_FLOAT
    simd_FLOAT_4 vf;

    vf.f[0] = vector_set_incr_FLOAT(0, &dlast);
    vf.f[1] = vector_set_incr_FLOAT(dlast, &dlast);
    vf.f[2] = vector_set_incr_FLOAT(dlast, &dlast);
    vf.f[3] = vector_set_incr_FLOAT(dlast, &dlast);

    TEST_IDENTICAL_D4_S4(FLOAT, vf, f, simd_transpose_FLOAT, vector_transpose_FLOAT);
#endif

#ifdef simd_transpose_DOUBLE
    simd_DOUBLE_2 vd;
    vd.d[0] = vector_set_incr_DOUBLE(0, &dlast);
    vd.d[1] = vector_set_incr_DOUBLE(dlast, &dlast);

    TEST_IDENTICAL_D2_S2(DOUBLE, vd, d, simd_transpose_DOUBLE, vector_transpose_DOUBLE);
#endif

    END_TEST();
}

#elif SIMD_VECTOR_SIZE == 32

/* Transpose tests for SIMD implementations of 32-byte vectors */

generate_vector_transpose_8x8(ULONG, ul)
#ifdef simd_splat_FLOAT
generate_vector_transpose_8x8(FLOAT, f)
#endif
#ifdef simd_splat_DOUBLE
generate_vector_transpose_4x4(DOUBLE, d)
generate_vector_transpose_2x2(DCOMPLEX, dc)
#endif

static void test_simd_transpose(void)
{
    double dlast;

    START_TEST();

#ifdef simd_transpose_ULONG
    simd_ULONG_8 vul;
    int last;

    vul.ul[0] = vector_set_incr_ULONG(0, &last);
    vul.ul[1] = vector_set_incr_ULONG(last, &last);
    vul.ul[2] = vector_set_incr_ULONG(last, &last);
    vul.ul[3] = vector_set_incr_ULONG(last, &last);
    vul.ul[4] = vector_set_incr_ULONG(last, &last);
    vul.ul[5] = vector_set_incr_ULONG(last, &last);
    vul.ul[6] = vector_set_incr_ULONG(last, &last);
    vul.ul[7] = vector_set_incr_ULONG(last, &last);

    TEST_IDENTICAL_D8_S8(ULONG, vul, ul, simd_transpose_ULONG, vector_transpose_ULONG);
#endif

#ifdef simd_transpose_FLOAT
    simd_FLOAT_8 vf;

    vf.f[0] = vector_set_incr_FLOAT(0, &dlast);
    vf.f[1] = vector_set_incr_FLOAT(dlast, &dlast);
    vf.f[2] = vector_set_incr_FLOAT(dlast, &dlast);
    vf.f[3] = vector_set_incr_FLOAT(dlast, &dlast);
    vf.f[4] = vector_set_incr_FLOAT(dlast, &dlast);
    vf.f[5] = vector_set_incr_FLOAT(dlast, &dlast);
    vf.f[6] = vector_set_incr_FLOAT(dlast, &dlast);
    vf.f[7] = vector_set_incr_FLOAT(dlast, &dlast);

    TEST_IDENTICAL_D8_S8(FLOAT, vf, f, simd_transpose_FLOAT, vector_transpose_FLOAT);
#endif

#ifdef simd_transpose_DOUBLE
    simd_DOUBLE_4 vd;

    vd.d[0] = vector_set_incr_DOUBLE(0, &dlast);
    vd.d[1] = vector_set_incr_DOUBLE(dlast, &dlast);
    vd.d[2] = vector_set_incr_DOUBLE(dlast, &dlast);
    vd.d[3] = vector_set_incr_DOUBLE(dlast, &dlast);

    TEST_IDENTICAL_D4_S4(DOUBLE, vd, d, simd_transpose_DOUBLE, vector_transpose_DOUBLE);
#endif

#ifdef simd_transpose_DCOMPLEX
    simd_DCOMPLEX_2 vdc;
    vdc.dc[0] = vector_set_incr_DOUBLE(0, &dlast);
    vdc.dc[1] = vector_set_incr_DOUBLE(dlast, &dlast);

    TEST_IDENTICAL_D2_S2(DCOMPLEX, vdc, dc, simd_transpose_DCOMPLEX, vector_transpose_DCOMPLEX);
#endif

    END_TEST();
}

#else

/* SIMD vector size not 16 nor 32 bytes */

static void test_simd_transpose(void)
{
    START_TEST();
    END_TEST();
}

#endif


#ifdef simd_transpose_4_UBYTE
static inline simd_UBYTE_4 vector_transpose_4_UBYTE(simd_ULONG_4 x)
{
    vrep_UBYTE va, vb, vc, vd;
    vrep_UBYTE vs, vt, vu, vv;
    simd_UBYTE_4 res;

    va.v = (simd_UBYTE)x.ul[0]; vb.v = (simd_UBYTE)x.ul[1];
    vc.v = (simd_UBYTE)x.ul[2]; vd.v = (simd_UBYTE)x.ul[3];

    for (int i=0; i<8; i++) {
	vs.e[i] = va.e[4*i];
	vt.e[i] = va.e[4*i+1];
	vu.e[i] = va.e[4*i+2];
	vv.e[i] = va.e[4*i+3];
	vs.e[8+i] = vb.e[4*i];
	vt.e[8+i] = vb.e[4*i+1];
	vu.e[8+i] = vb.e[4*i+2];
	vv.e[8+i] = vb.e[4*i+3];
	vs.e[16+i] = vc.e[4*i];
	vt.e[16+i] = vc.e[4*i+1];
	vu.e[16+i] = vc.e[4*i+2];
	vv.e[16+i] = vc.e[4*i+3];
	vs.e[24+i] = vd.e[4*i];
	vt.e[24+i] = vd.e[4*i+1];
	vu.e[24+i] = vd.e[4*i+2];
	vv.e[24+i] = vd.e[4*i+3];
    }
    res.ub[0] = (simd_UBYTE)vs.v;
    res.ub[1] = (simd_UBYTE)vt.v;
    res.ub[2] = (simd_UBYTE)vu.v;
    res.ub[3] = (simd_UBYTE)vv.v;
    return res;
}
#endif

#ifdef simd_transpose_2_USHORT
static inline simd_USHORT_2 vector_transpose_2_USHORT(simd_ULONG_2 x)
{
    vrep_USHORT va, vb;
    vrep_USHORT vs, vt;
    simd_USHORT_2 res;

    va.v = (simd_USHORT)x.ul[0];
    vb.v = (simd_USHORT)x.ul[1];

    for (int i=0; i<8; i++) {
	vs.e[i] = va.e[2*i];
	vt.e[i] = va.e[2*i+1];
	vs.e[8+i] = vb.e[2*i];
	vt.e[8+i] = vb.e[2*i+1];
    }
    res.us[0] = (simd_USHORT)vs.v;
    res.us[1] = (simd_USHORT)vt.v;
    return res;
}
#endif


#if defined(simd_transpose_4_UBYTE) || defined(simd_transpose_2_USHORT)
static void test_simd_transpose_4(void)
{
    simd_ULONG_4 ul4;
    simd_ULONG_2 ul2;

    START_TEST();

    ul4.ul[0] = vector_set_incr_with_step_UBYTE(1, 0x40, 4);
    ul4.ul[1] = vector_set_incr_with_step_UBYTE(9, 0x40, 4);
    ul4.ul[2] = vector_set_incr_with_step_UBYTE(17, 0x40, 4);
    ul4.ul[3] = vector_set_incr_with_step_UBYTE(25, 0x40, 4);

#ifdef simd_transpose_4_UBYTE
    TEST_IDENTICAL_D4_S4(UBYTE, ul4, ul, simd_transpose_4_UBYTE, vector_transpose_4_UBYTE);
#endif

#ifdef simd_transpose_2_USHORT
    ul2.ul[0] = vector_set_incr_with_step_USHORT(1, 0x100, 2);
    ul2.ul[1] = vector_set_incr_with_step_USHORT(9, 0x100, 2);
    TEST_IDENTICAL_D2_S2(USHORT, ul2, ul, simd_transpose_2_USHORT, vector_transpose_2_USHORT);
#endif

    END_TEST();
}
#endif


static void test_simd_vector_equal(void)
{
#ifdef simd_vector_equal_UBYTE
    vrep_UBYTE a, b;
#endif

    START_TEST();

#ifdef simd_vector_equal_UBYTE
    a.v = vector_set_incr_UBYTE(0, NULL);
    b = a;

    RUN_TEST(UBYTE, simd_vector_equal_UBYTE(a.v, b.v));

    for (int i=0; i<SIMD_NUM_ELEMENTS(UBYTE); i++) {
	a.e[i] = 0xff;
	RUN_TEST(UBYTE, !simd_vector_equal_UBYTE(a.v, b.v));
	a.e[i] = b.e[i];
    }
#endif

    END_TEST();
}


static void test_simd_create_byte_mask(void)
{
    START_TEST();

#ifdef simd_create_byte_mask
    for (int x=0; x<SIMD_NUM_ELEMENTS(UBYTE); x++) {
	vrep_UBYTE a;

	a.v = vector_constant_UBYTE(0);
	for (int j=0; j<x; j++)
	    a.e[j] = 0xff;

	print_initial("UBYTE", "simd_create_byte_mask", "i", x);
	RUN_TEST(UBYTE,
		 vectors_identical_UBYTE(simd_create_byte_mask(x), a.v));
    }
#endif

    END_TEST();
}


#define generate_interleave(type, tt)					\
    static simd_##type##_2 vector_interleave_##type(simd_##type x, simd_##type y) \
    {									\
	vrep_##type vr1, vr2, vx, vy;					\
	simd_##type##_2 res;						\
	const int secondhalf = SIMD_NUM_ELEMENTS(type)/2;		\
	vx.v = x;							\
	vy.v = y;							\
	for (int j=0; j<SIMD_NUM_ELEMENTS(type); j+=2) {		\
	    vr1.e[j] = vx.e[j/2];					\
	    vr1.e[j+1] = vy.e[j/2];					\
	    vr2.e[j] = vx.e[secondhalf+j/2];				\
	    vr2.e[j+1] = vy.e[secondhalf+j/2];				\
	}								\
	res.tt[0] = vr1.v;						\
	res.tt[1] = vr2.v;						\
	return res;							\
    }

#ifdef simd_interleave_UBYTE
generate_interleave(BYTE, b)
generate_interleave(UBYTE, ub)
generate_interleave(SHORT, s)
generate_interleave(USHORT, us)
#endif
#ifdef simd_interleave_ULONG
generate_interleave(LONG, l)
generate_interleave(ULONG, ul)
#endif
#ifdef simd_interleave_FLOAT
generate_interleave(FLOAT, f)
#endif
#ifdef simd_interleave_DOUBLE
generate_interleave(DOUBLE, d)
#endif


static void test_simd_interleave(void)
{
    vany x, y;
    int next;

    START_TEST();

    x.ub = vector_set_incr_UBYTE(1, &next);
    y.ub = vector_set_incr_UBYTE(next, NULL);

#ifdef simd_interleave_UBYTE
    TEST_IDENTICAL_D2_S2x(UBYTE, x.ub, y.ub, simd_interleave_UBYTE, vector_interleave_UBYTE);
    TEST_IDENTICAL_D2_S2x(BYTE, x.b, y.b, simd_interleave_BYTE, vector_interleave_BYTE);
#endif

#ifdef simd_interleave_USHORT
    TEST_IDENTICAL_D2_S2x(USHORT, x.us, y.us, simd_interleave_USHORT, vector_interleave_USHORT);
    TEST_IDENTICAL_D2_S2x(SHORT, x.s, y.s, simd_interleave_SHORT, vector_interleave_SHORT);
#endif

#ifdef simd_interleave_ULONG
    TEST_IDENTICAL_D2_S2x(ULONG, x.ul, y.ul, simd_interleave_ULONG, vector_interleave_ULONG);
    TEST_IDENTICAL_D2_S2x(LONG, x.l, y.l, simd_interleave_LONG, vector_interleave_LONG);
#endif

#ifdef simd_interleave_FLOAT
    x.f = vector_random_FLOAT(1.0);
    y.f = vector_random_FLOAT(7.32e4);
    TEST_IDENTICAL_D2_S2x(FLOAT, x.f, y.f, simd_interleave_FLOAT, vector_interleave_FLOAT);
#endif

#ifdef simd_interleave_DOUBLE
    x.d = vector_random_DOUBLE(1.23e9);
    y.d = vector_random_DOUBLE(1.2);
    TEST_IDENTICAL_D2_S2x(DOUBLE, x.d, y.d, simd_interleave_DOUBLE, vector_interleave_DOUBLE);
#endif

    END_TEST();
}



static void convert_not_implemented(void)
{
    SKIP_IF_NOT_REQUESTED();

    printf("The following SIMD_cnvrt routines not implemented:\n");

    /* basic convert from BYTE */
#ifndef simd_cnvrt_SHORT_BYTE
    printf("   simd_cnvrt_SHORT_BYTE\n");
#endif
#if defined(simd_splat_ULONG) && !defined(simd_cnvrt_LONG_BYTE)
    printf("   simd_cnvrt_LONG_BYTE\n");
#endif

    /* basic convert from UBYTE */
#ifndef simd_cnvrt_USHORT_UBYTE
    printf("   simd_cnvrt_USHORT_UBYTE\n");
#endif
#if defined(simd_splat_ULONG) && !defined(simd_cnvrt_ULONG_UBYTE)
    printf("   simd_cnvrt_ULONG_UBYTE\n");
#endif

    /* basic convert from SHORT */
#if defined(simd_splat_ULONG) && !defined(simd_cnvrt_LONG_SHORT)
    printf("   simd_cnvrt_LONG_SHORT\n");
#endif

    /* basic convert from USHORT */
#ifndef simd_cnvrt_UBYTE_USHORT
    printf("   simd_cnvrt_UBYTE_USHORT\n");
#endif
#ifdef simd_splat_ULONG
#  ifndef simd_cnvrt_ULONG_USHORT
    printf("   simd_cnvrt_ULONG_USHORT\n");
#  endif

    /* basic convert from LONG */

    /* basic convert from ULONG */
#  ifndef simd_cnvrt_UBYTE_ULONG
    printf("   simd_cnvrt_UBYTE_ULONG\n");
#  endif
#  ifndef simd_cnvrt_USHORT_ULONG
    printf("   simd_cnvrt_USHORT_ULONG\n");
#  endif
#endif

    /* convert with FLOAT */
#ifdef simd_splat_FLOAT
#  ifdef simd_splat_LONG
#    ifndef simd_cnvrt_FLOAT_LONG
    printf("   simd_cnvrt_FLOAT_LONG\n");
#    endif
#    ifndef simd_cnvrt_LONG_FLOAT
    printf("   simd_cnvrt_LONG_FLOAT\n");
#    endif
#  endif
#  ifdef simd_splat_ULONG
#    ifndef simd_cnvrt_FLOAT_ULONG
    printf("   simd_cnvrt_FLOAT_ULONG\n");
#    endif
#    ifndef simd_cnvrt_ULONG_FLOAT
    printf("   simd_cnvrt_ULONG_FLOAT\n");
#    endif
#  endif
#endif

    /* convert with DOUBLE */
#ifdef simd_splat_DOUBLE
#  ifdef simd_splat_LONG
#    ifndef simd_cnvrt_DOUBLE_LONG
    printf("   simd_cnvrt_DOUBLE_LONG\n");
#    endif
#    ifndef simd_cnvrt_LONG_DOUBLE
    printf("   simd_cnvrt_LONG_DOUBLE\n");
#    endif
#  endif
#  ifdef simd_splat_ULONG
#    ifndef simd_cnvrt_DOUBLE_ULONG
    printf("   simd_cnvrt_DOUBLE_ULONG\n");
#    endif
#    ifndef simd_cnvrt_ULONG_DOUBLE
    printf("   simd_cnvrt_ULONG_DOUBLE\n");
#    endif
#  endif
#  ifdef simd_splat_FLOAT
#    ifndef simd_cnvrt_DOUBLE_FLOAT
    printf("   simd_cnvrt_DOUBLE_FLOAT\n");
#    endif
#    ifndef simd_cnvrt_FLOAT_DOUBLE
    printf("   simd_cnvrt_FLOAT_DOUBLE\n");
#    endif
#  endif
#endif

    /* convert with rounding from FLOAT/DOUBLE */

#ifdef simd_splat_FLOAT
#  ifdef simd_splat_LONG
#    ifndef simd_cnvrt_rnd_LONG_FLOAT
    printf("   simd_cnvrt_rnd_LONG_FLOAT\n");
#    endif
#  endif
#  ifdef simd_splat_ULONG
#    ifndef simd_cnvrt_rnd_ULONG_FLOAT
    printf("   simd_cnvrt_rnd_ULONG_FLOAT\n");
#    endif
#  endif
#endif

#ifdef simd_splat_DOUBLE
#  ifdef simd_splat_LONG
#    ifndef simd_cnvrt_rnd_LONG_DOUBLE
    printf("   simd_cnvrt_rnd_LONG_DOUBLE\n");
#    endif
#  endif
#  ifdef simd_splat_ULONG
#    ifndef simd_cnvrt_rnd_ULONG_DOUBLE
    printf("   simd_cnvrt_rnd_ULONG_DOUBLE\n");
#    endif
#  endif
#endif

    /* convert with clipping from BYTE */
#ifndef simd_cnvrt_clip_UBYTE_BYTE
    printf("   simd_cnvrt_clip_UBYTE_BYTE\n");
#endif
#ifndef simd_cnvrt_clip_USHORT_BYTE
    printf("   simd_cnvrt_clip_USHORT_BYTE\n");
#endif
#if defined(simd_splat_ULONG) && !defined(simd_cnvrt_clip_ULONG_BYTE)
    printf("   simd_cnvrt_clip_ULONG_BYTE\n");
#endif

    /* convert with clipping from UBYTE */
#ifndef simd_cnvrt_clip_BYTE_UBYTE
    printf("   simd_cnvrt_clip_BYTE_UBYTE\n");
#endif

    /* convert with clipping from SHORT */
#ifndef simd_cnvrt_clip_BYTE_SHORT
    printf("   simd_cnvrt_clip_BYTE_SHORT\n");
#endif
#ifndef simd_cnvrt_clip_UBYTE_SHORT
    printf("   simd_cnvrt_clip_UBYTE_SHORT\n");
#endif
#ifndef simd_cnvrt_clip_USHORT_SHORT
    printf("   simd_cnvrt_clip_USHORT_SHORT\n");
#endif
#if defined(simd_splat_ULONG) && !defined(simd_cnvrt_clip_ULONG_SHORT)
    printf("   simd_cnvrt_clip_ULONG_SHORT\n");
#endif

    /* convert with clipping from USHORT */
#ifndef simd_cnvrt_clip_BYTE_USHORT
    printf("   simd_cnvrt_clip_BYTE_USHORT\n");
#endif
#ifndef simd_cnvrt_clip_UBYTE_USHORT
    printf("   simd_cnvrt_clip_UBYTE_USHORT\n");
#endif
#ifndef simd_cnvrt_clip_SHORT_USHORT
    printf("   simd_cnvrt_clip_SHORT_USHORT\n");
#endif

#ifdef simd_splat_ULONG
    /* convert with clipping from LONG */
#ifndef simd_cnvrt_clip_BYTE_LONG
    printf("   simd_cnvrt_clip_BYTE_LONG\n");
#endif
#ifndef simd_cnvrt_clip_UBYTE_LONG
    printf("   simd_cnvrt_clip_UBYTE_LONG\n");
#endif
#ifndef simd_cnvrt_clip_SHORT_LONG
    printf("   simd_cnvrt_clip_SHORT_LONG\n");
#endif
#ifndef simd_cnvrt_clip_USHORT_LONG
    printf("   simd_cnvrt_clip_USHORT_LONG\n");
#endif
#ifndef simd_cnvrt_clip_ULONG_LONG
    printf("   simd_cnvrt_clip_ULONG_LONG\n");
#endif

    /* convert with clipping from LONG */
#ifndef simd_cnvrt_clip_BYTE_ULONG
    printf("   simd_cnvrt_clip_BYTE_ULONG\n");
#endif
#ifndef simd_cnvrt_clip_UBYTE_ULONG
    printf("   simd_cnvrt_clip_UBYTE_ULONG\n");
#endif
#ifndef simd_cnvrt_clip_SHORT_ULONG
    printf("   simd_cnvrt_clip_SHORT_ULONG\n");
#endif
#ifndef simd_cnvrt_clip_USHORT_ULONG
    printf("   simd_cnvrt_clip_USHORT_ULONG\n");
#endif
#ifndef simd_cnvrt_clip_LONG_ULONG
    printf("   simd_cnvrt_clip_LONG_ULONG\n");
#endif
#endif

    /* convert with clipping from FLOAT */
#ifdef simd_splat_FLOAT
#  ifdef simd_splat_LONG
#    ifndef simd_cnvrt_clip_LONG_FLOAT
    printf("   simd_cnvrt_clip_LONG_FLOAT\n");
#    endif
#  endif
#  ifdef simd_splat_ULONG
#    ifndef simd_cnvrt_clip_ULONG_FLOAT
    printf("   simd_cnvrt_clip_ULONG_FLOAT\n");
#    endif
#  endif
#endif

    /* convert with DOUBLE */
#ifdef simd_splat_DOUBLE
#  ifdef simd_splat_LONG
#    ifndef simd_cnvrt_clip_LONG_DOUBLE
    printf("   simd_cnvrt_clip_LONG_DOUBLE\n");
#    endif
#  endif
#  ifdef simd_splat_ULONG
#    ifndef simd_cnvrt_clip_ULONG_DOUBLE
    printf("   simd_cnvrt_clip_ULONG_DOUBLE\n");
#    endif
#  endif
#  ifdef simd_splat_FLOAT
#    ifndef simd_cnvrt_clip_FLOAT_DOUBLE
    printf("   simd_cnvrt_clip_FLOAT_DOUBLE\n");
#    endif
#  endif
#endif


    /* convert with rounding and clipping from FLOAT/DOUBLE */

#ifdef simd_splat_FLOAT
#  ifdef simd_splat_LONG
#    ifndef simd_cnvrt_rnd_clip_LONG_FLOAT
    printf("   simd_cnvrt_rnd_clip_LONG_FLOAT\n");
#    endif
#  endif
#  ifdef simd_splat_ULONG
#    ifndef simd_cnvrt_rnd_clip_ULONG_FLOAT
    printf("   simd_cnvrt_rnd_clip_ULONG_FLOAT\n");
#    endif
#  endif
#endif

#ifdef simd_splat_DOUBLE
#  ifdef simd_splat_LONG
#    ifndef simd_cnvrt_rnd_clip_LONG_DOUBLE
    printf("   simd_cnvrt_rnd_clip_LONG_DOUBLE\n");
#    endif
#  endif
#  ifdef simd_splat_ULONG
#    ifndef simd_cnvrt_rnd_clip_ULONG_DOUBLE
    printf("   simd_cnvrt_rnd_clip_ULONG_DOUBLE\n");
#    endif
#  endif
#endif

}



int main(int argc, char *argv[])
{
    int opt;

    if (isatty(fileno(stdout)))
	use_colour = 1;

    while ((opt = getopt(argc, argv, "xc:f:v:")) != -1) {
	switch(opt) {
	case 'x':
	    dump_as_hex = 1;
	    break;
	case 'v':
	    vdebug = strtoul(optarg, NULL, 0);
	    break;
	case 'f':
	    function_to_test = strdup(optarg);
	    break;
	case 'c':
	    use_colour = strtol(optarg, NULL, 0);
	    break;
	default:
	    fprintf(stderr, "Usage: %s [-x] [-c <colour_switch>]"
		    " [-v <verbosity>] [-f <test_name>]\n", argv[0]);
	    exit(1);
	}
    }

    test_simd();
    test_simd_splat();
    test_simd_splat_v0();
    test_simd_splat_vN();
    test_simd_mask();
    test_simd_cvu_imm();
    test_simd_cvu();
    test_simd_ldu();
    test_simd_interleave();
    test_simd_vector_equal();
    test_simd_create_byte_mask();
    test_simd_mul();
    test_simd_mul_clipped();
    test_simd_mulhi();
    test_simd_fmaddhi();
    test_simd_imulfN();
    test_simd_idivfN();
    test_simd_idiv();
    test_simd_replicate();
    test_simd_zero();
    test_simd_cmplt();
    test_simd_cmpgt();
    test_simd_cmpeq();
    test_simd_horiz_minmax();
    test_simd_cnvrt_X_byte();
    test_simd_cnvrt_X_short();
    test_simd_cnvrt_X_long();
#ifdef simd_splat_ULONG64
    test_simd_cnvrt_X_long64();
#endif
#ifdef simd_splat_FLOAT
    test_simd_cnvrt_X_float();
#endif
#ifdef simd_splat_DOUBLE
    test_simd_cnvrt_X_double();
#endif
    test_simd_cnvrt_clip_X_byte();
    test_simd_cnvrt_clip_X_short();
    test_simd_cnvrt_clip_X_long();
#ifdef simd_splat_FLOAT
    test_simd_cnvrt_clip_X_float();
#endif
#ifdef simd_splat_DOUBLE
    test_simd_cnvrt_clip_X_double();
#endif
#ifdef simd_splat_FLOAT
    test_simd_cnvrt_rnd_X_float();
#endif
#ifdef simd_splat_DOUBLE
    test_simd_cnvrt_rnd_X_double();
#endif
#ifdef simd_splat_FLOAT
    test_simd_cnvrt_rnd_clip_X_float();
#endif
#ifdef simd_splat_DOUBLE
    test_simd_cnvrt_rnd_clip_X_double();
#endif
    test_simd_transpose();
#ifdef simd_transpose_4_UBYTE
    test_simd_transpose_4();
#endif
    convert_not_implemented();
    dump_notes();

    return 0;
}
