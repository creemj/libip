/*
   Name: src/simd/simd_powerpc_altivec.h
   Author: M. J. Cree

   Copyright (C) 2013, 2015-2019 Michael J. Cree

   SIMD support for the IP library.

   PowerPC altivec support.

*/

/*
 * PowerPC Altivec SIMD support
 */

#include <altivec.h>
#include <ip/divide.h>

/* Basic vector types */
typedef vector unsigned char simd_UBYTE;
typedef vector signed char simd_BYTE;
typedef vector unsigned short simd_USHORT;
typedef vector signed short simd_SHORT;
typedef vector unsigned int simd_ULONG;
typedef vector signed int simd_LONG;
typedef vector float simd_FLOAT;
typedef vector float simd_COMPLEX;

/* Splatting a value across the vector */
#define simd_splat_UBYTE(c) vec_splat(((simd_UBYTE){c}), 0)
#define simd_splat_BYTE(c)  vec_splat(((simd_BYTE){c}), 0)
#define simd_splat_USHORT(c) vec_splat(((simd_USHORT){c}), 0)
#define simd_splat_SHORT(c) vec_splat(((simd_SHORT){c}), 0)
#define simd_splat_ULONG(c) vec_splat(((simd_ULONG){c}), 0)
#define simd_splat_LONG(c)  vec_splat(((simd_LONG){c}), 0)
#define simd_splat_FLOAT(c) vec_splat(((simd_FLOAT){c}), 0)
#define simd_splat_COMPLEX(u, v) ((simd_FLOAT){u, v, u, v})

/*
 * Now that the splat routines are defined and, thus, indicate which image
 * types we support in SIMD, now get the rest of the typedefs that are
 * needed for the convert routines below.
 */

#include "simd_td.h"

#define simd_splat_v0_UBYTE(c) vec_splat(c, 0)
#define simd_splat_v0_BYTE(c)  vec_splat(c, 0)
#define simd_splat_v0_USHORT(c) vec_splat(c, 0)
#define simd_splat_v0_SHORT(c) vec_splat(c, 0)
#define simd_splat_v0_ULONG(c) vec_splat(c, 0)
#define simd_splat_v0_LONG(c)  vec_splat(c, 0)
#define simd_splat_v0_FLOAT(c) vec_splat(c, 0)

static inline simd_USHORT _ip_splat_vN_USHORT(simd_USHORT x, int pos)
{
    const simd_USHORT incr = simd_splat_USHORT(0x0001);
    simd_UBYTE idx = vec_add(simd_splat_UBYTE(2*pos), (simd_UBYTE)incr);
    return vec_perm(x, x, idx);
}

static inline simd_ULONG _ip_splat_vN_ULONG(simd_ULONG x, int pos)
{
    const simd_ULONG incr = simd_splat_ULONG(0x00010203);
    simd_UBYTE idx = vec_add(simd_splat_UBYTE(4*pos), (simd_UBYTE)incr);
    return vec_perm(x, x, idx);
}

#define simd_splat_vN_UBYTE(c,n) vec_perm(c, c, simd_splat_UBYTE(n))
#define simd_splat_vN_BYTE(c,n)  vec_perm(c, c, (simd_UBYTE)simd_splat_BYTE(n))
#define simd_splat_vN_USHORT(c,n) _ip_splat_vN_USHORT(c,n)
#define simd_splat_vN_SHORT(c,n) ((simd_SHORT)_ip_splat_vN_USHORT((simd_USHORT)(c),n))
#define simd_splat_vN_ULONG(c,n) _ip_splat_vN_ULONG(c,n)
#define simd_splat_vN_LONG(c,n) ((simd_LONG)_ip_splat_vN_ULONG((simd_ULONG)(c),n))
#define simd_splat_vN_FLOAT(c,n) ((simd_FLOAT)_ip_splat_vN_ULONG((simd_ULONG)(c),n))


/* Masks for setting values in multielement image types */
#define simd_mask_COMPLEX(u, v) ((simd_ULONG){u, v, u, v})
#define simd_mask_RGB(r, g, b) ((simd_UBYTE){r, g, b, r, g, b, r, g, b, r, g, b, r, g, b, r})
#define simd_mask_RGBA(r, g, b, a) ((simd_UBYTE){r, g, b, a, r, g, b, a, r, g, b, a, r, g, b, a})
#define simd_mask_RGB16(r, g, b) ((simd_USHORT){r, g, b, r, g, b, r, g})

static inline simd_UBYTE _ip_simd_cvu_imm_UBYTE(simd_UBYTE l, simd_UBYTE r, const int offset)
{
    vector unsigned char pidx = vec_lvsl(offset, (unsigned char *)NULL);
    return vec_perm(l, r, pidx);
}
static inline simd_BYTE _ip_simd_cvu_imm_BYTE(simd_BYTE l, simd_BYTE r, const int offset)
{
    vector unsigned char pidx = vec_lvsl(offset, (unsigned char *)NULL);
    return vec_perm(l, r, pidx);
}
#define simd_cvu_imm_UBYTE(l, r, o) _ip_simd_cvu_imm_UBYTE(l, r, o)
#define simd_cvu_imm_BYTE(l, r, o) _ip_simd_cvu_imm_BYTE(l, r, o)

static inline simd_USHORT _ip_simd_cvu_imm_USHORT(simd_USHORT l, simd_USHORT r, const int offset)
{
    vector unsigned char pidx = vec_lvsl(2*offset, (unsigned char *)NULL);
    return vec_perm(l, r, pidx);
}
static inline simd_SHORT _ip_simd_cvu_imm_SHORT(simd_SHORT l, simd_SHORT r, const int offset)
{
    vector unsigned char pidx = vec_lvsl(2*offset, (unsigned char *)NULL);
    return vec_perm(l, r, pidx);
}
#define simd_cvu_imm_USHORT(l, r, o) _ip_simd_cvu_imm_USHORT(l, r, o)
#define simd_cvu_imm_SHORT(l, r, o) _ip_simd_cvu_imm_SHORT(l, r, o)

static inline simd_ULONG _ip_simd_cvu_imm_ULONG(simd_ULONG l, simd_ULONG r, const int offset)
{
    vector unsigned char pidx = vec_lvsl(4*offset, (unsigned char *)NULL);
    return vec_perm(l, r, pidx);
}
static inline simd_LONG _ip_simd_cvu_imm_LONG(simd_LONG l, simd_LONG r, const int offset)
{
    vector unsigned char pidx = vec_lvsl(4*offset, (unsigned char *)NULL);
    return vec_perm(l, r, pidx);
}
#define simd_cvu_imm_ULONG(l, r, o) _ip_simd_cvu_imm_ULONG(l, r, o)
#define simd_cvu_imm_LONG(l, r, o) _ip_simd_cvu_imm_LONG(l, r, o)
#define simd_cvu_imm_FLOAT(l, r, o) \
    ((simd_FLOAT)_ip_simd_cvu_imm_ULONG((simd_ULONG)l, (simd_ULONG)r, o))

static inline simd_UBYTE _ip_simd_ldu_UBYTE(void *adr)
{
    simd_UBYTE *left;
    size_t aligned = (size_t)adr;
    aligned &= ~(0x0f);
    left = (simd_UBYTE *)aligned;
    return vec_perm(left[0], left[1], vec_lvsl(0, (unsigned char *)adr));
}
#define simd_ldu_UBYTE(a) _ip_simd_ldu_UBYTE(a)
#define simd_ldu_BYTE(a)   ((simd_BYTE)_ip_simd_ldu_UBYTE(a))
#define simd_ldu_USHORT(a) ((simd_USHORT)_ip_simd_ldu_UBYTE(a))
#define simd_ldu_SHORT(a)  ((simd_SHORT)_ip_simd_ldu_UBYTE(a))
#define simd_ldu_ULONG(a) ((simd_ULONG)_ip_simd_ldu_UBYTE(a))
#define simd_ldu_LONG(a)  ((simd_LONG)_ip_simd_ldu_UBYTE(a))
#define simd_ldu_FLOAT(a)  ((simd_FLOAT)_ip_simd_ldu_UBYTE(a))

static inline simd_UBYTE _ip_simd_ror_RGB(simd_UBYTE a)
{
    const simd_UBYTE perm = (simd_UBYTE){1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 12, 13};
    return vec_perm(a, a, perm);
}
static inline simd_USHORT _ip_simd_ror_RGB16(simd_USHORT a)
{
    const simd_UBYTE perm = (simd_UBYTE){4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 10, 11, 12, 13};
    simd_UBYTE aa = (simd_UBYTE)a;

    return (simd_USHORT)vec_perm(aa, aa, perm);
}

#define simd_ror_RGB(x) _ip_simd_ror_RGB(x)
#define simd_ror_RGB16(x) _ip_simd_ror_RGB16(x)

/*
 * Comparison of whole SIMD vector
 */

static inline int _ip_simd_vector_equal_UBYTE(simd_UBYTE x, simd_UBYTE y)
{
    return vec_all_eq(x, y);
}

static inline int _ip_simd_vector_zero_UBYTE(simd_UBYTE x)
{
    return vec_all_eq(x, simd_splat_UBYTE(0));
}

#define simd_vector_zero_UBYTE(x) _ip_simd_vector_zero_UBYTE(x)
#define simd_vector_equal_UBYTE(x, y) _ip_simd_vector_equal_UBYTE(x, y)


/*
 * simd_create_byte_mask
 *
 * Create a byte mask for masking out padding on last vector in image
 * row. If numbytes is N then the first N bytes in the returned vector have
 * all bits sets; subsequent bytes are zero.
 *
 * Also see simd_zero_right_TYPE() macros below which are used for similar
 * purposes.
 */

static inline simd_UBYTE _ip_simd_create_byte_mask(int numbytes)
{
    simd_UBYTE vinc = vec_lvsl(0, (unsigned char *)NULL);
    simd_UBYTE vnbyte = simd_splat_UBYTE(numbytes);
    simd_UBYTE mask = (simd_UBYTE)vec_cmpgt(vnbyte, vinc);

    return mask;
}
#define simd_create_byte_mask(x) _ip_simd_create_byte_mask(x)


/*
 * Transposition macros
 */

static inline simd_USHORT_8 _ip_simd_transpose_USHORT(simd_USHORT_8 x)
{
    simd_USHORT v1, v2, v3, v4, v5, v6, v7, v8;
    simd_USHORT t1, t2, t3, t4, t5, t6, t7, t8;
    simd_USHORT_8 res;

    v1 = vec_mergeh(x.us[0], x.us[4]);
    v2 = vec_mergel(x.us[0], x.us[4]);
    v3 = vec_mergeh(x.us[1], x.us[5]);
    v4 = vec_mergel(x.us[1], x.us[5]);
    v5 = vec_mergeh(x.us[2], x.us[6]);
    v6 = vec_mergel(x.us[2], x.us[6]);
    v7 = vec_mergeh(x.us[3], x.us[7]);
    v8 = vec_mergel(x.us[3], x.us[7]);
    t1 = vec_mergeh(v1, v5);
    t2 = vec_mergel(v1, v5);
    t3 = vec_mergeh(v2, v6);
    t4 = vec_mergel(v2, v6);
    t5 = vec_mergeh(v3, v7);
    t6 = vec_mergel(v3, v7);
    t7 = vec_mergeh(v4, v8);
    t8 = vec_mergel(v4, v8);
    res.us[0] = vec_mergeh(t1, t5);
    res.us[1] = vec_mergel(t1, t5);
    res.us[2] = vec_mergeh(t2, t6);
    res.us[3] = vec_mergel(t2, t6);
    res.us[4] = vec_mergeh(t3, t7);
    res.us[5] = vec_mergel(t3, t7);
    res.us[6] = vec_mergeh(t4, t8);
    res.us[7] = vec_mergel(t4, t8);
    return res;
}
#define simd_transpose_USHORT(x) _ip_simd_transpose_USHORT(x)

static inline simd_ULONG_4 _ip_simd_transpose_ULONG(simd_ULONG_4 x)
{
    simd_ULONG_4 res;
    simd_ULONG v1, v2, v3, v4;

    v1 = vec_mergeh(x.ul[0], x.ul[2]);
    v2 = vec_mergel(x.ul[0], x.ul[2]);
    v3 = vec_mergeh(x.ul[1], x.ul[3]);
    v4 = vec_mergel(x.ul[1], x.ul[3]);
    res.ul[0] = vec_mergeh(v1, v3);
    res.ul[1] = vec_mergel(v1, v3);
    res.ul[2] = vec_mergeh(v2, v4);
    res.ul[3] = vec_mergel(v2, v4);
    return res;
}
#define simd_transpose_ULONG(x) _ip_simd_transpose_ULONG(x)

static inline simd_FLOAT_4 _ip_simd_transpose_FLOAT(simd_FLOAT_4 x)
{
    simd_FLOAT_4 res;
    simd_FLOAT v1, v2, v3, v4;

    v1 = vec_mergeh(x.f[0], x.f[2]);
    v2 = vec_mergel(x.f[0], x.f[2]);
    v3 = vec_mergeh(x.f[1], x.f[3]);
    v4 = vec_mergel(x.f[1], x.f[3]);
    res.f[0] = vec_mergeh(v1, v3);
    res.f[1] = vec_mergel(v1, v3);
    res.f[2] = vec_mergeh(v2, v4);
    res.f[3] = vec_mergel(v2, v4);
    return res;
}
#define simd_transpose_FLOAT(x) _ip_simd_transpose_FLOAT(x)


/*
 * simd_interleave - interleave the elements of two vectors
 */

static inline simd_UBYTE_2 _ip_simd_interleave_UBYTE(simd_UBYTE a, simd_UBYTE b)
{
    simd_UBYTE_2 res;

    res.ub[0] = vec_mergeh(a, b);
    res.ub[1] = vec_mergel(a, b);
    return res;
}
#define simd_interleave_UBYTE(a, b) _ip_simd_interleave_UBYTE(a, b)

static inline simd_BYTE_2 _ip_simd_interleave_BYTE(simd_BYTE a, simd_BYTE b)
{
    simd_BYTE_2 res;

    res.b[0] = vec_mergeh(a, b);
    res.b[1] = vec_mergel(a, b);
    return res;
}
#define simd_interleave_BYTE(a, b) _ip_simd_interleave_BYTE(a, b)

static inline simd_USHORT_2 _ip_simd_interleave_USHORT(simd_USHORT a, simd_USHORT b)
{
    simd_USHORT_2 res;

    res.us[0] = vec_mergeh(a, b);
    res.us[1] = vec_mergel(a, b);
    return res;
}
#define simd_interleave_USHORT(a, b) _ip_simd_interleave_USHORT(a, b)

static inline simd_SHORT_2 _ip_simd_interleave_SHORT(simd_SHORT a, simd_SHORT b)
{
    simd_SHORT_2 res;

    res.s[0] = vec_mergeh(a, b);
    res.s[1] = vec_mergel(a, b);
    return res;
}
#define simd_interleave_SHORT(a, b) _ip_simd_interleave_SHORT(a, b)

static inline simd_ULONG_2 _ip_simd_interleave_ULONG(simd_ULONG a, simd_ULONG b)
{
    simd_ULONG_2 res;

    res.ul[0] = vec_mergeh(a, b);
    res.ul[1] = vec_mergel(a, b);
    return res;
}
#define simd_interleave_ULONG(a, b) _ip_simd_interleave_ULONG(a, b)

static inline simd_LONG_2 _ip_simd_interleave_LONG(simd_LONG a, simd_LONG b)
{
    simd_LONG_2 res;

    res.l[0] = vec_mergeh(a, b);
    res.l[1] = vec_mergel(a, b);
    return res;
}
#define simd_interleave_LONG(a, b) _ip_simd_interleave_LONG(a, b)

static inline simd_FLOAT_2 _ip_simd_interleave_FLOAT(simd_FLOAT a, simd_FLOAT b)
{
    simd_FLOAT_2 res;

    res.f[0] = vec_mergeh(a, b);
    res.f[1] = vec_mergel(a, b);
    return res;
}
#define simd_interleave_FLOAT(a, b) _ip_simd_interleave_FLOAT(a, b)


/*
 * Conversion routines for converting one image type to another.
 *
 * simd_cnvrt_DTYPE_STYPE
 *   - convert source type STYPE to type DTYPE
 *
 * simd_cnvrt_clip_DTYPE_STYPE
 *   - convert source type STYPE to type DTYPE with clipping (saturation).
 */

/* Zero extend smaller integer to larger integer type */
static inline simd_USHORT_2 _ip_simd_cnvrt_USHORT_UBYTE(simd_UBYTE a)
{
    simd_USHORT_2 res;
    const simd_UBYTE vzero = simd_splat_UBYTE(0);
    res.us[0] = (simd_USHORT)vec_mergeh(vzero, a);
    res.us[1] = (simd_USHORT)vec_mergel(vzero, a);
    return res;
}
#define simd_cnvrt_USHORT_UBYTE(a) _ip_simd_cnvrt_USHORT_UBYTE(a)

static inline simd_ULONG_4 _ip_simd_cnvrt_ULONG_UBYTE(simd_UBYTE a)
{
    simd_ULONG_4 res;
    const simd_UBYTE vzero = simd_splat_UBYTE(0);
    simd_SHORT h, l;
    h = (simd_SHORT)vec_mergeh(vzero, a);
    l = (simd_SHORT)vec_mergel(vzero, a);
    res.ul[0] = (simd_ULONG)vec_unpackh(h);
    res.ul[1] = (simd_ULONG)vec_unpackl(h);
    res.ul[2] = (simd_ULONG)vec_unpackh(l);
    res.ul[3] = (simd_ULONG)vec_unpackl(l);
    return res;
}
#define simd_cnvrt_ULONG_UBYTE(a) _ip_simd_cnvrt_ULONG_UBYTE(a)

static inline simd_ULONG_2 _ip_simd_cnvrt_ULONG_USHORT(simd_USHORT a)
{
    simd_ULONG_2 res;
    const simd_USHORT vzero = simd_splat_USHORT(0);
    res.ul[0] = (simd_ULONG)vec_mergeh(vzero, a);
    res.ul[1] = (simd_ULONG)vec_mergel(vzero, a);
    return res;
}
#define simd_cnvrt_ULONG_USHORT(a) _ip_simd_cnvrt_ULONG_USHORT(a)

/* Sign extend smaller integer to larger integer type */
static inline simd_SHORT_2 _ip_simd_cnvrt_SHORT_BYTE(simd_BYTE a)
{
    simd_SHORT_2 res;
    res.s[0] = vec_unpackh(a);
    res.s[1] = vec_unpackl(a);
    return res;
}
#define simd_cnvrt_SHORT_BYTE(a) _ip_simd_cnvrt_SHORT_BYTE(a)

static inline simd_LONG_4 _ip_simd_cnvrt_LONG_BYTE(simd_BYTE a)
{
    simd_LONG_4 res;
    simd_SHORT h, l;
    h = vec_unpackh(a);
    l = vec_unpackl(a);
    res.l[0] = vec_unpackh(h);
    res.l[1] = vec_unpackl(h);
    res.l[2] = vec_unpackh(l);
    res.l[3] = vec_unpackl(l);
    return res;
}
#define simd_cnvrt_LONG_BYTE(a) _ip_simd_cnvrt_LONG_BYTE(a)

static inline simd_LONG_2 _ip_simd_cnvrt_LONG_SHORT(simd_SHORT a)
{
    simd_LONG_2 res;
    res.l[0] = vec_unpackh(a);
    res.l[1] = vec_unpackl(a);
    return res;
}
#define simd_cnvrt_LONG_SHORT(a) _ip_simd_cnvrt_LONG_SHORT(a)

/* Truncate larger integer type to smaller integer */

#define simd_cnvrt_UBYTE_USHORT(a,b) vec_pack(a,b)
#define simd_cnvrt_UBYTE_ULONG(a,b,c,d) vec_pack(vec_pack(a,b), vec_pack(c,d))
#define simd_cnvrt_USHORT_ULONG(a,b) vec_pack(a,b)

/* Conversions of BYTE to UBYTE/USHORT/ULONG with clipping */

#define simd_cnvrt_clip_UBYTE_BYTE(a) \
    ((simd_UBYTE)vec_max(a, vec_splat_s8(0)))
#define simd_cnvrt_clip_USHORT_BYTE(a) \
    simd_cnvrt_USHORT_UBYTE((simd_UBYTE)vec_max(a, vec_splat_s8(0)))
#define simd_cnvrt_clip_ULONG_BYTE(a) \
    simd_cnvrt_ULONG_UBYTE((simd_UBYTE)vec_max(a, vec_splat_s8(0)))

/* Conversion of UBYTE to BYTE with clipping */

#define simd_cnvrt_clip_BYTE_UBYTE(a) ((simd_BYTE)vec_min(a, simd_splat_UBYTE(127)))

/* Conversions of SHORT to BYTE/UBYTE/USHORT/ULONG with clipping */

#define simd_cnvrt_clip_BYTE_SHORT(a,b) vec_packs(a,b)
#define simd_cnvrt_clip_UBYTE_SHORT(a,b) vec_packsu(a,b)
#define simd_cnvrt_clip_USHORT_SHORT(a) \
    ((simd_USHORT)vec_max(a,vec_splat_s16(0)))
#define simd_cnvrt_clip_ULONG_SHORT(a) \
    simd_cnvrt_ULONG_USHORT((simd_USHORT)vec_max(a, vec_splat_s16(0)))

/* Conversion of USHORT to BYTE/UBYTE/SHORT with clipping */

static inline simd_BYTE _ip_simd_cnvrt_clip_BYTE_USHORT(simd_USHORT a, simd_USHORT b)
{
    const simd_SHORT vzero = vec_splat_s16(0);
    const simd_SHORT vmax = simd_splat_SHORT(127);
    simd_SHORT ta = (simd_SHORT)a;
    simd_SHORT tb = (simd_SHORT)b;
    vector bool short sela, selb;

    sela = vec_cmpgt(vzero, ta);
    ta = vec_sel(ta, vmax, sela);
    selb = vec_cmpgt(vzero, tb);
    tb = vec_sel(tb, vmax, selb);
    return  simd_cnvrt_clip_BYTE_SHORT(ta, tb);
}
#define simd_cnvrt_clip_BYTE_USHORT(a,b)  _ip_simd_cnvrt_clip_BYTE_USHORT(a,b)

#define simd_cnvrt_clip_UBYTE_USHORT(a,b) vec_packs(a,b)
#define simd_cnvrt_clip_SHORT_USHORT(a) \
    ((simd_SHORT)vec_min(a, simd_splat_USHORT(INT16_MAX)))

/* Convert LONG to BYTE/UBYTE/SHORT/USHORT/ULONG with clipping */

#define simd_cnvrt_clip_BYTE_LONG(a,b,c,d) vec_packs(vec_packs(a,b), vec_packs(c,d))
#define simd_cnvrt_clip_UBYTE_LONG(a,b,c,d) vec_packsu(vec_packs(a,b), vec_packs(c,d))
#define simd_cnvrt_clip_SHORT_LONG(a,b) vec_packs(a, b)
#define simd_cnvrt_clip_USHORT_LONG(a,b) vec_packsu(a, b)
#define simd_cnvrt_clip_ULONG_LONG(a) ((simd_ULONG)vec_max(a, vec_splat_s32(0)))

/* Convert ULONG to BYTE/UBYTE/SHORT/USHORT/LONG with clipping */

static inline simd_BYTE _ip_simd_cnvrt_clip_BYTE_ULONG(simd_ULONG a, simd_ULONG b,
						       simd_ULONG c, simd_ULONG d)
{
    const simd_LONG vzero = vec_splat_s32(0);
    const simd_LONG vmax = simd_splat_LONG(127);
    simd_LONG ta = (simd_LONG)a;
    simd_LONG tb = (simd_LONG)b;
    simd_LONG tc = (simd_LONG)c;
    simd_LONG td = (simd_LONG)d;
    vector bool int sela, selb;

    sela = vec_cmpgt(vzero, ta);
    ta = vec_sel(ta, vmax, sela);
    selb = vec_cmpgt(vzero, tb);
    tb = vec_sel(tb, vmax, selb);
    sela = vec_cmpgt(vzero, tc);
    tc = vec_sel(tc, vmax, sela);
    selb = vec_cmpgt(vzero, td);
    td = vec_sel(td, vmax, selb);
    return  simd_cnvrt_clip_BYTE_LONG(ta, tb, tc, td);
}
#define simd_cnvrt_clip_BYTE_ULONG(a,b,c,d)  _ip_simd_cnvrt_clip_BYTE_ULONG(a,b,c,d)

static inline simd_SHORT _ip_simd_cnvrt_clip_SHORT_ULONG(simd_ULONG a, simd_ULONG b)
{
    const simd_LONG vzero = vec_splat_s32(0);
    const simd_LONG vmax = simd_splat_LONG(INT16_MAX);
    simd_LONG ta = (simd_LONG)a;
    simd_LONG tb = (simd_LONG)b;
    vector bool int sela, selb;

    sela = vec_cmpgt(vzero, ta);
    ta = vec_sel(ta, vmax, sela);
    selb = vec_cmpgt(vzero, tb);
    tb = vec_sel(tb, vmax, selb);
    return  simd_cnvrt_clip_SHORT_LONG(ta, tb);
}
#define simd_cnvrt_clip_SHORT_ULONG(a,b)  _ip_simd_cnvrt_clip_SHORT_ULONG(a,b)

#define simd_cnvrt_clip_UBYTE_ULONG(a,b,c,d) vec_packs(vec_packs(a,b),vec_packs(c,d))
#define simd_cnvrt_clip_USHORT_ULONG(a,b) vec_packs(a,b)
#define simd_cnvrt_clip_LONG_ULONG(a) ((simd_LONG)vec_min(a, simd_splat_ULONG(INT32_MAX)))

/* Conversion between LONG/ULONG and FLOAT */

#define simd_cnvrt_FLOAT_LONG(a) vec_ctf(a,0)
#define simd_cnvrt_FLOAT_ULONG(a) vec_ctf(a,0)

#define simd_cnvrt_LONG_FLOAT(a) vec_cts(a,0)
#define simd_cnvrt_ULONG_FLOAT(a) vec_ctu(a,0)

static inline simd_LONG _ip_simd_cnvrt_rnd_LONG_FLOAT(simd_FLOAT x)
{
    const simd_FLOAT signbit = (simd_FLOAT)simd_splat_ULONG(0x80000000U);
    simd_FLOAT half = simd_splat_FLOAT(0x1.fffffep-2); /* ohalf = nextafterf(0.5, 0) */
    simd_FLOAT sign = vec_and(x, signbit);
    x = vec_add(x, vec_or(half, sign));    /* x + copysignf(ohalf, x) */
    return vec_cts(x, 0);
}
#define simd_cnvrt_rnd_LONG_FLOAT(x) _ip_simd_cnvrt_rnd_LONG_FLOAT(x)

static inline simd_ULONG _ip_simd_cnvrt_rnd_ULONG_FLOAT(simd_FLOAT x)
{
    const simd_FLOAT signbit = (simd_FLOAT)simd_splat_ULONG(0x80000000U);
    simd_FLOAT half = simd_splat_FLOAT(0x1.fffffep-2); /* ohalf = nextafterf(0.5, 0) */
    simd_FLOAT sign = vec_and(x, signbit);
    x = vec_add(x, vec_or(half, sign));    /* x + copysignf(ohalf, x) */
    return vec_ctu(x, 0);
}
#define simd_cnvrt_rnd_ULONG_FLOAT(x) _ip_simd_cnvrt_rnd_ULONG_FLOAT(x)

/* Conversion between LONG/ULONG and FLOAT with clipping */
#define simd_cnvrt_clip_LONG_FLOAT(a) vec_cts(a,0)
#define simd_cnvrt_clip_ULONG_FLOAT(a) vec_ctu(a,0)
#define simd_cnvrt_rnd_clip_LONG_FLOAT(a) _ip_simd_cnvrt_rnd_LONG_FLOAT(a)
#define simd_cnvrt_rnd_clip_ULONG_FLOAT(a) _ip_simd_cnvrt_rnd_ULONG_FLOAT(a)

/* Conversions to/from BINARY; first some helper functions */

static inline simd_UBYTE vec_cmpne_UBYTE(simd_UBYTE x, simd_UBYTE y)
{
    simd_UBYTE tmp = (simd_UBYTE)vec_cmpeq(x, y);
    return vec_nor(tmp, tmp);
}

static inline simd_USHORT vec_cmpne_USHORT(simd_USHORT x, simd_USHORT y)
{
    simd_USHORT tmp = (simd_USHORT)vec_cmpeq(x, y);
    return vec_nor(tmp, tmp);
}

static inline simd_ULONG vec_cmpne_ULONG(simd_ULONG x, simd_ULONG y)
{
    simd_ULONG tmp = (simd_ULONG)vec_cmpeq(x, y);
    return vec_nor(tmp, tmp);
}

static inline simd_UBYTE _ip_simd_cnvrt_BINARY_UBYTE(simd_UBYTE x)
{
    const simd_UBYTE vfalse = simd_splat_UBYTE(0);
    return vec_cmpne_UBYTE(x, vfalse);
}
#define simd_cnvrt_BINARY_UBYTE(x) _ip_simd_cnvrt_BINARY_UBYTE(x)

static inline simd_UBYTE _ip_simd_cnvrt_BINARY_USHORT(simd_USHORT a, simd_USHORT b)
{
    const simd_USHORT vfalse = simd_splat_USHORT(0);
    a = vec_cmpne_USHORT(a, vfalse);
    b = vec_cmpne_USHORT(b, vfalse);
    return simd_cnvrt_UBYTE_USHORT(a, b);
}
#define simd_cnvrt_BINARY_USHORT(x, y) _ip_simd_cnvrt_BINARY_USHORT(x, y)

static inline simd_UBYTE _ip_simd_cnvrt_BINARY_ULONG(simd_ULONG a, simd_ULONG b,
						     simd_ULONG c, simd_ULONG d)
{
    const simd_ULONG vfalse = simd_splat_ULONG(0);
    a = vec_cmpne_ULONG(a, vfalse);
    b = vec_cmpne_ULONG(b, vfalse);
    c = vec_cmpne_ULONG(c, vfalse);
    d = vec_cmpne_ULONG(d, vfalse);
    return simd_cnvrt_UBYTE_ULONG(a, b, c, d);
}
#define simd_cnvrt_BINARY_ULONG(x, y, u, v) _ip_simd_cnvrt_BINARY_ULONG(x, y, u, v)



static inline simd_UBYTE _ip_simd_cnvrt_UBYTE_BINARY(simd_UBYTE x, int tval)
{
    const simd_UBYTE vfalse = simd_splat_UBYTE(0);
    simd_UBYTE vtrue = simd_splat_UBYTE(tval);
    return vec_sel(vfalse, vtrue, x);
}
#define simd_cnvrt_UBYTE_BINARY(x, tv) _ip_simd_cnvrt_UBYTE_BINARY(x, tv)

static inline simd_USHORT_2 _ip_simd_cnvrt_USHORT_BINARY(simd_UBYTE x, int tval)
{
    const simd_UBYTE vfalse = simd_splat_UBYTE(0);
    simd_UBYTE vtrue = simd_splat_UBYTE(tval);
    x = vec_sel(vfalse, vtrue, x);
    return simd_cnvrt_USHORT_UBYTE(x);
}
#define simd_cnvrt_USHORT_BINARY(x, tv) _ip_simd_cnvrt_USHORT_BINARY(x, tv)

static inline simd_ULONG_4 _ip_simd_cnvrt_ULONG_BINARY(simd_UBYTE x, int tval)
{
    const simd_UBYTE vfalse = simd_splat_UBYTE(0);
    simd_UBYTE vtrue = simd_splat_UBYTE(tval);
    x = vec_sel(vfalse, vtrue, x);
    return simd_cnvrt_ULONG_UBYTE(x);
}
#define simd_cnvrt_ULONG_BINARY(x, tv) _ip_simd_cnvrt_ULONG_BINARY(x, tv)


/* Colour conversions */
/* Repack 3 RGB vectors into 4 RGBA vectors */
static inline simd_UBYTE_4 ip_simd_cnvrt_RGBA_RGB(simd_UBYTE a, simd_UBYTE b, simd_UBYTE c)
{
    simd_UBYTE_4 y;
    const simd_UBYTE sel1 = (simd_UBYTE){ 0,  1,  2,  0,  3,  4,  5,  0,
					  6,  7,  8,  0,  9, 10, 11,  0};
    const simd_UBYTE sel2 = (simd_UBYTE){12, 13, 14,  0, 15, 16, 17,  0,
					 18, 19, 20,  0, 21, 22, 23,  0};
    const simd_UBYTE sel3 = (simd_UBYTE){ 8,  9, 10,  0, 11, 12, 13,  0,
					 14, 15, 16,  0, 17, 18, 19,  0};
    const simd_UBYTE sel4 = (simd_UBYTE){ 4,  5,  6,  0,  7,  8,  9,  0,
					 10, 11, 12,  0, 13, 14, 15,  0};
    const simd_UBYTE set_alpha_255 = (simd_UBYTE){0, 0, 0, 0xff, 0, 0, 0, 0xff,
						  0, 0, 0, 0xff, 0, 0, 0, 0xff};
    y.ub[0] = vec_or(vec_perm(a, a, sel1), set_alpha_255);
    y.ub[1] = vec_or(vec_perm(a, b, sel2), set_alpha_255);
    y.ub[2] = vec_or(vec_perm(b, c, sel3), set_alpha_255);
    y.ub[3] = vec_or(vec_perm(c, c, sel4), set_alpha_255);

    return y;
}
#define simd_cnvrt_RGBA_RGB(a,b,c) ip_simd_cnvrt_RGBA_RGB(a,b,c)

/* Repack 4 RGBA vectors into 3 RGB vectors */
static inline simd_UBYTE_3 ip_simd_cnvrt_RGB_RGBA(simd_UBYTE a, simd_UBYTE b,
						  simd_UBYTE c, simd_UBYTE d)
{
    simd_UBYTE_3 y;
    const simd_UBYTE sel1 = (simd_UBYTE){ 0,  1,  2,  4,  5,  6,  8,  9,
					 10, 12, 13, 14, 16, 17, 18, 20};
    const simd_UBYTE sel2 = (simd_UBYTE){ 5,  6,  8,  9, 10, 12, 13, 14,
					 16, 17, 18, 20, 21, 22, 24, 25};
    const simd_UBYTE sel3 = (simd_UBYTE){10, 12, 13, 14, 16, 17, 18, 20,
					 21, 22, 24, 25, 26, 28, 29, 30};

    y.ub[0] = vec_perm(a, b, sel1);
    y.ub[1] = vec_perm(b, c, sel2);
    y.ub[2] = vec_perm(c, d, sel3);

    return y;
}
#define simd_cnvrt_RGB_RGBA(a,b,c,d) ip_simd_cnvrt_RGB_RGBA(a,b,c,d)

static inline simd_UBYTE ip_simd_cnvrt_RGB_RGB16(simd_USHORT x, simd_USHORT y)
{
    const simd_USHORT eight = simd_splat_USHORT(8);
    return simd_cnvrt_UBYTE_USHORT(vec_sr(x, eight),
				   vec_sr(y, eight));
}
#define simd_cnvrt_RGB_RGB16(x,y) ip_simd_cnvrt_RGB_RGB16(x,y)

static inline simd_USHORT_2 ip_simd_cnvrt_RGB16_RGB(simd_UBYTE x)
{
    const simd_USHORT eight = simd_splat_USHORT(8);
    simd_USHORT_2 r16 = simd_cnvrt_USHORT_UBYTE(x);
    r16.us[0] = vec_or(r16.us[0], vec_sl(r16.us[0], eight));
    r16.us[1] = vec_or(r16.us[1], vec_sl(r16.us[1], eight));
    return r16;
}
#define simd_cnvrt_RGB16_RGB(x) ip_simd_cnvrt_RGB16_RGB(x)


/* Logical operators on SIMD vectors */

#define simd_or_UBYTE(a, b) vec_or(a, b)
#define simd_or_BYTE(a, b) vec_or(a, b)
#define simd_or_USHORT(a, b) vec_or(a, b)
#define simd_or_SHORT(a, b) vec_or(a, b)
#define simd_or_ULONG(a, b) vec_or(a, b)
#define simd_or_LONG(a, b) vec_or(a, b)

#define simd_xor_UBYTE(a, b) vec_xor(a, b)
#define simd_xor_BYTE(a, b) vec_xor(a, b)
#define simd_xor_USHORT(a, b) vec_xor(a, b)
#define simd_xor_SHORT(a, b) vec_xor(a, b)
#define simd_xor_ULONG(a, b) vec_xor(a, b)
#define simd_xor_LONG(a, b) vec_xor(a, b)

#define simd_and_UBYTE(a, b) vec_and(a, b)
#define simd_and_BYTE(a, b) vec_and(a, b)
#define simd_and_USHORT(a, b) vec_and(a, b)
#define simd_and_SHORT(a, b) vec_and(a, b)
#define simd_and_ULONG(a, b) vec_and(a, b)
#define simd_and_LONG(a, b) vec_and(a, b)

#define simd_not_UBYTE(a) vec_nor(a, a)
#define simd_not_BYTE(a) vec_nor(a, a)
#define simd_not_USHORT(a) vec_nor(a ,a)
#define simd_not_SHORT(a) vec_nor(a, a)
#define simd_not_ULONG(a) vec_nor(a, a)
#define simd_not_LONG(a) vec_nor(a, a)

/*
 * Shifts
 */

/* Shift left logical; shift per element */
#define simd_shl_BYTE(a, b) vec_sl(a, b)
#define simd_shl_UBYTE(a, b) vec_sl(a, b)
#define simd_shl_SHORT(a, b) vec_sl(a, b)
#define simd_shl_USHORT(a, b) vec_sl(a, b)
#define simd_shl_LONG(a, b) vec_sl(a, b)
#define simd_shl_ULONG(a, b) vec_sl(a, b)

/* Shift right logical; shift per element */
#define simd_shr_BYTE(a, b) vec_sr(a, b)
#define simd_shr_UBYTE(a, b) vec_sr(a, b)
#define simd_shr_SHORT(a, b) vec_sr(a, b)
#define simd_shr_USHORT(a, b) vec_sr(a, b)
#define simd_shr_LONG(a, b) vec_sr(a, b)
#define simd_shr_ULONG(a, b) vec_sr(a, b)

/* Shift right arithmetical; shift per element */
#define simd_shra_BYTE(a, b) vec_sra(a, b)
#define simd_shra_UBYTE(a, b) vec_sra(a, b)
#define simd_shra_SHORT(a, b) vec_sra(a, b)
#define simd_shra_USHORT(a, b) vec_sra(a, b)
#define simd_shra_LONG(a, b) vec_sra(a, b)
#define simd_shra_ULONG(a, b) vec_sra(a, b)

/* Shift left logical; constant shift applied to all elements */
#define simd_shl_imm_BYTE(a, x) vec_sl(a, simd_splat_BYTE(x))
#define simd_shl_imm_UBYTE(a, x) vec_sl(a, simd_splat_UBYTE(x))
#define simd_shl_imm_SHORT(a, x) vec_sl(a, simd_splat_SHORT(x))
#define simd_shl_imm_USHORT(a, x) vec_sl(a, simd_splat_USHORT(x))
#define simd_shl_imm_LONG(a, x) vec_sl(a, simd_splat_LONG(x))
#define simd_shl_imm_ULONG(a, x) vec_sl(a, simd_splat_ULONG(x))

/* Shift right logical; constant shift applied to all elements */
#define simd_shr_imm_BYTE(a, x) vec_sr(a, simd_splat_BYTE(x))
#define simd_shr_imm_UBYTE(a, x) vec_sr(a, simd_splat_UBYTE(x))
#define simd_shr_imm_SHORT(a, x) vec_sr(a, simd_splat_SHORT(x))
#define simd_shr_imm_USHORT(a, x) vec_sr(a, simd_splat_USHORT(x))
#define simd_shr_imm_LONG(a, x) vec_sr(a, simd_splat_LONG(x))
#define simd_shr_imm_ULONG(a, x) vec_sr(a, simd_splat_ULONG(x))

/* Basic arithmetic operators */
#define simd_add_UBYTE(d,s) vec_add(d,s)
#define simd_add_BYTE(d,s) vec_add(d,s)
#define simd_add_USHORT(d,s) vec_add(d,s)
#define simd_add_SHORT(d,s) vec_add(d,s)
#define simd_add_ULONG(d,s) vec_add(d,s)
#define simd_add_LONG(d,s) vec_add(d,s)
#define simd_add_FLOAT(d,s) vec_add(d,s)

#define simd_add_clipped_UBYTE(d,s) vec_adds(d,s)
#define simd_add_clipped_BYTE(d,s) vec_adds(d,s)
#define simd_add_clipped_USHORT(d,s) vec_adds(d,s)
#define simd_add_clipped_SHORT(d,s) vec_adds(d,s)

#define simd_sub_UBYTE(d,s) vec_sub(d,s)
#define simd_sub_BYTE(d,s) vec_sub(d,s)
#define simd_sub_USHORT(d,s) vec_sub(d,s)
#define simd_sub_SHORT(d,s) vec_sub(d,s)
#define simd_sub_ULONG(d,s) vec_sub(d,s)
#define simd_sub_LONG(d,s) vec_sub(d,s)
#define simd_sub_FLOAT(d,s) vec_sub(d,s)

#define simd_sub_clipped_UBYTE(d,s) vec_subs(d,s)
#define simd_sub_clipped_BYTE(d,s) vec_subs(d,s)
#define simd_sub_clipped_USHORT(d,s) vec_subs(d,s)
#define simd_sub_clipped_SHORT(d,s) vec_subs(d,s)

static inline simd_UBYTE _ip_simd_mul_UBYTE(simd_UBYTE d, simd_UBYTE s)
{
    const vector unsigned char v_reconstruct = (vector unsigned char)
	{0x01, 0x11, 0x03, 0x13, 0x05, 0x15, 0x07, 0x17,
	 0x09, 0x19, 0x0b, 0x1b, 0x0d, 0x1d, 0x0f, 0x1f};
    vector unsigned short t1, t2;

    t1 = vec_mule(d, s);
    t2 = vec_mulo(d, s);
    return (simd_UBYTE)vec_perm(t1, t2, v_reconstruct);
}
#define simd_mul_UBYTE(d,s) _ip_simd_mul_UBYTE(d,s)
#define simd_mul_BYTE(d,s) ((simd_BYTE)_ip_simd_mul_UBYTE((simd_UBYTE)d, (simd_UBYTE)s))

static inline simd_USHORT _ip_simd_mul_USHORT(simd_USHORT d, simd_USHORT s)
{
    const vector simd_USHORT zero = simd_splat_USHORT(0);
    return vec_mladd(d, s, zero);
}
#define simd_mul_USHORT(d,s) _ip_simd_mul_USHORT(d,s)
#define simd_mul_SHORT(d,s) ((simd_SHORT)_ip_simd_mul_USHORT((simd_USHORT)d, (simd_USHORT)s))


/*
 * Altivec has no 32-bit integer multiplication!!!!  Bloody idiots.
 *
 * We have to construct them out of 16-bit multiplies. Fortunately, the
 * vec_msum() intrinsic gives us two multiplies and an addition in one
 * instruction, which is about the only redeeming feature of this crap
 * situation.
 */
static inline simd_ULONG _ip_simd_mul_ULONG(simd_ULONG d, simd_ULONG s)
{
    const simd_ULONG sixteen = simd_splat_ULONG(16);
    const simd_ULONG zero = simd_splat_ULONG(0);
    simd_ULONG dl_sl; /* low 16 bits of d times low 16 sbits of s */
    simd_ULONG cross; /* cross terms = dl*sh + dh*sl */

    dl_sl = (simd_ULONG)vec_mulo((simd_USHORT)d, (simd_USHORT)s);
    s = vec_rl(s, sixteen);     /* swaps high and low parts of s */
    cross = vec_msum((simd_USHORT)d, (simd_USHORT)s, zero);

    return vec_add(dl_sl, vec_sl(cross, sixteen));
}
#define simd_mul_ULONG(d,s) _ip_simd_mul_ULONG(d,s)
#define simd_mul_LONG(d,s) ((simd_LONG)_ip_simd_mul_ULONG((simd_ULONG)d, (simd_ULONG)s))

/*
 * Clipping multiplication
 */

static inline simd_BYTE _ip_simd_mul_clipped_BYTE(simd_BYTE d, simd_BYTE s)
{
    const vector signed short byte_max = simd_splat_SHORT(127);
    const vector signed short byte_min = simd_splat_SHORT(-128);
    const vector unsigned char v_reconstruct = (vector unsigned char)
	{0x01, 0x11, 0x03, 0x13, 0x05, 0x15, 0x07, 0x17,
	 0x09, 0x19, 0x0b, 0x1b, 0x0d, 0x1d, 0x0f, 0x1f};
    vector signed short t1, t2;

    t1 = vec_mule(d, s);
    t2 = vec_mulo(d, s);
    t1 = vec_min(t1, byte_max);
    t2 = vec_min(t2, byte_max);
    t1 = vec_max(t1, byte_min);
    t2 = vec_max(t2, byte_min);
    return (simd_BYTE)vec_perm(t1, t2, v_reconstruct);
}
#define simd_mul_clipped_BYTE(d,s) _ip_simd_mul_clipped_BYTE(d,s)

static inline simd_UBYTE _ip_simd_mul_clipped_UBYTE(simd_UBYTE d, simd_UBYTE s)
{
    const vector unsigned short ubyte_max = simd_splat_USHORT(255);
    const vector unsigned char v_reconstruct = (vector unsigned char)
	{0x01, 0x11, 0x03, 0x13, 0x05, 0x15, 0x07, 0x17,
	 0x09, 0x19, 0x0b, 0x1b, 0x0d, 0x1d, 0x0f, 0x1f};
    vector unsigned short t1, t2;

    t1 = vec_mule(d, s);
    t2 = vec_mulo(d, s);
    t1 = vec_min(t1, ubyte_max);
    t2 = vec_min(t2, ubyte_max);
    return (simd_UBYTE)vec_perm(t1, t2, v_reconstruct);
}
#define simd_mul_clipped_UBYTE(d,s) _ip_simd_mul_clipped_UBYTE(d,s)

static inline simd_SHORT _ip_simd_mul_clipped_SHORT(simd_SHORT d, simd_SHORT s)
{
    const vector signed int short_max = simd_splat_LONG(INT16_MAX);
    const vector signed int short_min = simd_splat_LONG(INT16_MIN);
    const vector unsigned char v_reconstruct = (vector unsigned char)
	{0x02, 0x03, 0x12, 0x13, 0x06, 0x07, 0x16, 0x17,
	 0x0a, 0x0b, 0x1a, 0x1b, 0x0e, 0x0f, 0x1e, 0x1f};
    vector signed int t1, t2;

    t1 = vec_mule(d, s);
    t2 = vec_mulo(d, s);
    t1 = vec_min(t1, short_max);
    t2 = vec_min(t2, short_max);
    t1 = vec_max(t1, short_min);
    t2 = vec_max(t2, short_min);
    return (simd_SHORT)vec_perm(t1, t2, v_reconstruct);
}
#define simd_mul_clipped_SHORT(d,s) _ip_simd_mul_clipped_SHORT(d,s)

static inline simd_USHORT _ip_simd_mul_clipped_USHORT(simd_USHORT d, simd_USHORT s)
{
    const vector unsigned int ushort_max = simd_splat_ULONG(UINT16_MAX);
    const vector unsigned char v_reconstruct = (vector unsigned char)
	{0x02, 0x03, 0x12, 0x13, 0x06, 0x07, 0x16, 0x17,
	 0x0a, 0x0b, 0x1a, 0x1b, 0x0e, 0x0f, 0x1e, 0x1f};
    vector unsigned int t1, t2;

    t1 = vec_mule(d, s);
    t2 = vec_mulo(d, s);
    t1 = vec_min(t1, ushort_max);
    t2 = vec_min(t2, ushort_max);
    return (simd_USHORT)vec_perm(t1, t2, v_reconstruct);
}
#define simd_mul_clipped_USHORT(d,s) _ip_simd_mul_clipped_USHORT(d,s)

/*
 * Floating point and complex multiplication
 */
#define simd_mul_FLOAT(d,s) vec_madd(d,s,simd_splat_FLOAT(-0.0F))

static inline simd_COMPLEX _ip_simd_mul_COMPLEX(simd_COMPLEX a, simd_COMPLEX b)
{
    const simd_COMPLEX zero = simd_splat_COMPLEX(-0.0F, -0.0F);
    const simd_COMPLEX negi = (simd_COMPLEX){0.0F, -0.0F, 0.0F, -0.0F};
    const vector unsigned char swappairs = (vector unsigned char)
        {0x04, 0x05, 0x06, 0x07, 0x00, 0x01, 0x02, 0x03,
         0x0c, 0x0d, 0x0e, 0x0f, 0x08, 0x09, 0x0a, 0x0b};
    const vector unsigned char perm1 = (vector unsigned char)
        {0x00, 0x01, 0x02, 0x03, 0x10, 0x11, 0x12, 0x13,
         0x08, 0x09, 0x0a, 0x0b, 0x18, 0x19, 0x1a, 0x1b};
    const vector unsigned char perm2 = (vector unsigned char)
        {0x04, 0x05, 0x06, 0x07, 0x14, 0x15, 0x16, 0x17,
         0x0c, 0x0d, 0x0e, 0x0f, 0x1c, 0x1d, 0x1e, 0x1f};
    simd_COMPLEX same, cross;

    same = vec_xor(vec_madd(a, b, zero), negi);
    cross = vec_madd(a, vec_perm(b, b, swappairs), zero);

    return vec_add(vec_perm(same, cross, perm1), vec_perm(same, cross, perm2));
}
#define simd_mul_COMPLEX(a, b) _ip_simd_mul_COMPLEX(a, b)


/*
 * simd_mulhi
 *
 * Return upper N-bits of N-bit by N-bit multiplication.
 */

static inline simd_SHORT _ip_simd_mulhi_SHORT(simd_SHORT d, simd_SHORT s)
{
    const simd_SHORT zero = simd_splat_SHORT(0);
    const simd_USHORT one = simd_splat_USHORT(1);

    return vec_sra(vec_madds(d, s, zero), one);
}
#define simd_mulhi_SHORT(a, b) _ip_simd_mulhi_SHORT(a, b)

static inline simd_USHORT _ip_simd_mulhi_USHORT(simd_USHORT d, simd_USHORT s)
{
    const simd_ULONG sixteen = simd_splat_ULONG(16);
    simd_ULONG odds, evens;

    evens = vec_sr(vec_mule(d, s), sixteen);
    odds = vec_sr(vec_mulo(d, s), sixteen);
    return vec_pack(vec_mergeh(evens, odds), vec_mergel(evens, odds));
}
#define simd_mulhi_USHORT(a, b) _ip_simd_mulhi_USHORT(a, b)


/*
 * simd_imulfN
 *
 * Fixed-point integer multiplication where N=32 for 32/32 fixed-point and
 * N=16 for 16/16 fixed-point.
 *
 * Calculates round(a*c) for integer multiplicand a and fixed-point
 * multiplier c = cw + cf, with cw = floor(c) and cf = 2^N(c - cw). Intended
 * for use when c >= 0.5.  If c < 0.5 consider using the idivfN functions
 * below as they better preserve precision of the fraction.
 */

static inline simd_USHORT _ip_simd_imulf16_USHORT(simd_USHORT a, simd_USHORT cw, simd_USHORT cf)
{
    const simd_ULONG half = simd_splat_ULONG(1<<15);
    const simd_UBYTE sel = (simd_UBYTE){0x00, 0x01, 0x10, 0x11, 0x04, 0x05, 0x14, 0x15,
					0x08, 0x09, 0x18, 0x19, 0x0c, 0x0d, 0x1c, 0x1d};
    simd_USHORT axcf;
    simd_ULONG x1, x2;

    /* Multiply a by the fraction cf and round to nearest integer */
    x1 = vec_add(vec_mule(a, cf), half);
    x2 = vec_add(vec_mulo(a, cf), half);
    axcf = (simd_USHORT)vec_perm(x1, x2, sel);
    /* Multiply a by cw and add in the overflow from the fraction */
    return vec_mladd(a, cw, axcf);
}
#define simd_imulf16_USHORT(a,b,s) _ip_simd_imulf16_USHORT(a,b,s)



static inline simd_SHORT _ip_simd_imulf16_SHORT(simd_SHORT a, simd_SHORT cw, simd_USHORT cf)
{
    const simd_SHORT zero = vec_splat_s16(0);
    const simd_UBYTE sel = (simd_UBYTE){0x00, 0x01, 0x04, 0x05, 0x08, 0x09, 0x0c, 0x0d,
					0x10, 0x11, 0x14, 0x15, 0x18, 0x19, 0x1c, 0x1d};
    simd_USHORT rounder = simd_splat_USHORT(1<<15);
    simd_USHORT signa, negc;
    simd_SHORT axcf;
    simd_ULONG x1, x2, y1, y2;

    /* Sign of a */
    signa = (simd_USHORT)vec_cmpgt(zero, a);

    /* rounder = (result < 0) ? (half-1) : half */
    rounder = vec_add(rounder, vec_xor((simd_USHORT)vec_cmpgt(zero, cw), signa));

    /* Signed correction to unsigned multiply: negc = (a<0) ? -cf : 0 */
    negc = vec_and(vec_sub((simd_USHORT)zero, cf), signa);

    /* Compute 32-bit result of unsigned a *cf; and correct to signed with rounder */

    x1 = vec_mule((simd_USHORT)a, cf);
    x2 = vec_mulo((simd_USHORT)a, cf);
    y1 = vec_mergeh(x1, x2);
    y2 = vec_mergel(x1, x2);

    x1 = vec_add(y1, (simd_ULONG)vec_mergeh(negc, rounder));
    x2 = vec_add(y2, (simd_ULONG)vec_mergel(negc, rounder));

    /* Pick off high 16-bits of a*cf + rounder */
    axcf = (simd_SHORT)vec_perm(x1, x2, sel);

    /* Return low 16-bits of a*cw + axcf */
    return vec_mladd(a, cw, axcf);
}
#define simd_imulf16_SHORT(a,b,s) _ip_simd_imulf16_SHORT(a,b,s)


/*
 * Division; only correct for finite math; does not implement
 * handling infinities and NaNs.
 */

static inline simd_FLOAT _ip_simd_div_FLOAT(simd_FLOAT a, simd_FLOAT b)
{
    const simd_FLOAT zero = simd_splat_FLOAT(-0.0F);
    const simd_FLOAT one = simd_splat_FLOAT(1);
    simd_FLOAT r, y2, q;

    r = vec_re(b);
    r = vec_madd(r, vec_nmsub(r, b, one), r);
    y2 = vec_madd(r, vec_nmsub(r, b, one), r);
    q = vec_madd(a, y2, zero);
    r = vec_nmsub(b, q, a);
    return vec_madd(r, y2, q);
}
#define simd_div_FLOAT(a,b) _ip_simd_div_FLOAT(a, b)

/*
 * simd_rcpc
 * simd_idiv
 *
 * Integer division by a constant (i.e. reused) divisor
 *
 * We provide simd_rcpc functions to calculate a vector integer reciprocal
 * of a scalar divisor, and simd_idiv functions to calculate the integer
 * division using the reciprocal from the simd_rcpc functions.
 *
 * The IP library only expects these at (U)SHORT and (U)LONG precision.
 */

static inline struct simd_rcp_USHORT simd_rcpc_USHORT(unsigned int d)
{
    struct ip_uint16_rcp_n16 r = ip_urcp16_n16(d);
    struct simd_rcp_USHORT rcp;
    rcp.m = simd_splat_USHORT(r.m);
    rcp.b = simd_splat_USHORT(r.shift_1);
    rcp.s = simd_splat_USHORT(r.shift);
    return rcp;
}

static inline simd_USHORT _ip_simd_idiv_USHORT(simd_USHORT n,
					       struct simd_rcp_USHORT r)
{
    simd_USHORT x1, t;
    t = simd_mulhi_USHORT(n, r.m);
    x1 = vec_sr(simd_sub_USHORT(n, t), r.b);
    return vec_sr(simd_add_USHORT(t, x1), r.s);
}
#define simd_idiv_USHORT(n, r) _ip_simd_idiv_USHORT(n, r)

static inline struct simd_rcp_SHORT simd_rcpc_SHORT(int d)
{
    struct ip_int16_rcp r = ip_ircp16(d);
    struct simd_rcp_SHORT rcp;
    rcp.m = simd_splat_SHORT(r.m);
    rcp.b = simd_splat_SHORT(r.dsign);
    rcp.s = simd_splat_SHORT(r.shift);
    return rcp;
}

static inline simd_SHORT _ip_simd_idiv_SHORT(simd_SHORT n,
					   struct simd_rcp_SHORT r)
{
    const simd_USHORT fifteen = simd_splat_USHORT(15);
    simd_SHORT q0;
    q0 = simd_add_SHORT(n, simd_mulhi_SHORT(n, r.m));
    q0 = vec_sra(q0, (simd_USHORT)r.s);
    q0 = simd_sub_SHORT(q0,  vec_sra(n, fifteen));
    return simd_sub_SHORT(simd_xor_SHORT(q0, r.b), r.b);
}
#define simd_idiv_SHORT(n, r) _ip_simd_idiv_SHORT(n, r)


/*
 * idivfN
 *
 * Integer division with conventional rounding.  These are implemented as a
 * multiply with the reciprocal.  These functions, equivalently, can be
 * thought of as a multiple by c for c < 0.5 such that cf = 2^(N+l)*c where
 * l is the shift required to shift c so that c's first 1 bit is in the MSB
 * of cf (not counting the sign bit), thus we always have an N-bit (or N-1
 * bit if signed) fraction in cf.
 */

static inline simd_USHORT _ip_simd_idivf16_USHORT(simd_USHORT a, simd_USHORT cf, simd_USHORT shift)
{
    const simd_ULONG half = simd_splat_ULONG(1<<15);
    const simd_UBYTE sel = (simd_UBYTE){0x00, 0x01, 0x04, 0x05, 0x08, 0x09, 0x0c, 0x0d,
					0x10, 0x11, 0x14, 0x15, 0x18, 0x19, 0x1c, 0x1d};
    simd_USHORT axcf;
    simd_ULONG x1, x2, y1, y2;

    /* Multiply a by the fraction cf and round to nearest integer */
    y1 = vec_mule(a, cf);
    y2 = vec_mulo(a, cf);
    x1 = vec_add(vec_mergeh(y1, y2), vec_sl(half, (simd_ULONG)vec_unpackh((simd_SHORT)shift)));
    x2 = vec_add(vec_mergel(y1, y2), vec_sl(half, (simd_ULONG)vec_unpackl((simd_SHORT)shift)));
    axcf = (simd_USHORT)vec_perm(x1, x2, sel);
    return vec_sr(axcf, shift);
}
#define simd_idivf16_USHORT(a,b,s) _ip_simd_idivf16_USHORT(a,b,s)


static inline simd_SHORT _ip_simd_idivf16_SHORT(simd_SHORT a, simd_SHORT cf, simd_USHORT shift)
{
    const simd_LONG zero = vec_splat_s32(0);
    const simd_UBYTE sel = (simd_UBYTE){0x00, 0x01, 0x04, 0x05, 0x08, 0x09, 0x0c, 0x0d,
					0x10, 0x11, 0x14, 0x15, 0x18, 0x19, 0x1c, 0x1d};
    simd_LONG rounder = simd_splat_LONG(1<<15);
    simd_LONG sign1, sign2;
    simd_LONG x1, x2, y1, y2;
    simd_ULONG s1, s2;

    /* Compute 32-bit result of signed a *cf */
    x1 = vec_mule(a, cf);
    x2 = vec_mulo(a, cf);
    y1 = vec_mergeh(x1, x2);
    y2 = vec_mergel(x1, x2);

    /* Unpack shift */
    s1 = (simd_ULONG)vec_unpackh((simd_SHORT)shift);
    s2 = (simd_ULONG)vec_unpackl((simd_SHORT)shift);

    /* Get sign of result */
    sign1 = (simd_LONG)vec_cmpgt(zero, y1);
    sign2 = (simd_LONG)vec_cmpgt(zero, y2);

    /* Let half = (2^15)<<shift, then rounder = (res < 0) ? (half-1) : half */
    x1 = vec_add(vec_sl(rounder, s1), sign1);
    x2 = vec_add(vec_sl(rounder, s2), sign2);

    /* Add in rounder to result */
    x1 = vec_add(y1, x1);
    x2 = vec_add(y2, x2);

    /* Unpack and  shift back */
    return vec_sra((simd_SHORT)vec_perm(x1, x2, sel), shift);

}
#define simd_idivf16_SHORT(a,b,s) _ip_simd_idivf16_SHORT(a,b,s)




/* Elementwise min/max */
#define simd_min_UBYTE(d,s) vec_min(d,s)
#define simd_min_BYTE(d,s) vec_min(d,s)
#define simd_min_USHORT(d,s) vec_min(d,s)
#define simd_min_SHORT(d,s) vec_min(d,s)
#define simd_min_ULONG(d,s) vec_min(d,s)
#define simd_min_LONG(d,s) vec_min(d,s)
#define simd_min_FLOAT(d,s) vec_min(d,s)

#define simd_max_UBYTE(d,s) vec_max(d,s)
#define simd_max_BYTE(d,s) vec_max(d,s)
#define simd_max_USHORT(d,s) vec_max(d,s)
#define simd_max_SHORT(d,s) vec_max(d,s)
#define simd_max_ULONG(d,s) vec_max(d,s)
#define simd_max_LONG(d,s) vec_max(d,s)
#define simd_max_FLOAT(d,s) vec_max(d,s)


/* Comparison Operators */

#define simd_cmpgt_BYTE(a, b) ((simd_BYTE)vec_cmpgt(a, b))
#define simd_cmpgt_UBYTE(a, b) ((simd_UBYTE)vec_cmpgt(a, b))
#define simd_cmpgt_SHORT(a, b) ((simd_SHORT)vec_cmpgt(a, b))
#define simd_cmpgt_USHORT(a, b) ((simd_USHORT)vec_cmpgt(a, b))
#define simd_cmpgt_LONG(a, b) ((simd_LONG)vec_cmpgt(a, b))
#define simd_cmpgt_ULONG(a, b) ((simd_ULONG)vec_cmpgt(a, b))
#define simd_cmpgt_FLOAT(a, b) ((simd_FLOAT)vec_cmpgt(a, b))

#define simd_cmplt_BYTE(a, b) ((simd_BYTE)vec_cmplt(a, b))
#define simd_cmplt_UBYTE(a, b) ((simd_UBYTE)vec_cmplt(a, b))
#define simd_cmplt_SHORT(a, b) ((simd_SHORT)vec_cmplt(a, b))
#define simd_cmplt_USHORT(a, b) ((simd_USHORT)vec_cmplt(a, b))
#define simd_cmplt_LONG(a, b) ((simd_LONG)vec_cmplt(a, b))
#define simd_cmplt_ULONG(a, b) ((simd_ULONG)vec_cmplt(a, b))
#define simd_cmplt_FLOAT(a, b) ((simd_FLOAT)vec_cmplt(a, b))

#define simd_cmpeq_BYTE(a, b) ((simd_BYTE)vec_cmpeq(a, b))
#define simd_cmpeq_UBYTE(a, b) ((simd_UBYTE)vec_cmpeq(a, b))
#define simd_cmpeq_SHORT(a, b) ((simd_SHORT)vec_cmpeq(a, b))
#define simd_cmpeq_USHORT(a, b) ((simd_USHORT)vec_cmpeq(a, b))
#define simd_cmpeq_LONG(a, b) ((simd_LONG)vec_cmpeq(a, b))
#define simd_cmpeq_ULONG(a, b) ((simd_ULONG)vec_cmpeq(a, b))
#define simd_cmpeq_FLOAT(a, b) ((simd_FLOAT)vec_cmpeq(a, b))

/* Bitwise selection operator */

#define simd_select_BYTE(s, a, b) vec_sel(b, a, (simd_UBYTE)(s))
#define simd_select_UBYTE(s, a, b) vec_sel(b, a, s)
#define simd_select_SHORT(s, a, b) vec_sel(b, a, (simd_USHORT)(s))
#define simd_select_USHORT(s, a, b) vec_sel(b, a, s)
#define simd_select_LONG(s, a, b) vec_sel(b, a, (simd_ULONG)(s))
#define simd_select_ULONG(s, a, b) vec_sel(b, a, s)
#define simd_select_FLOAT(s, a, b) vec_sel(b, a, (simd_ULONG)(s))


/*
 * Thresholding operators
 */

static inline simd_UBYTE
_ip_simd_threshold_UBYTE(simd_UBYTE s1, const simd_UBYTE vlow, const simd_UBYTE vhigh,
			 const simd_UBYTE vtrue)
{
    vector bool char t1 = vec_cmplt(s1, vlow);
    vector bool char t2 = vec_cmpgt(s1, vhigh);
    t1 = vec_or(t1, t2);
    return vec_xor((simd_UBYTE)t1, vtrue);
}

static inline simd_UBYTE
_ip_simd_threshold_BYTE(simd_BYTE s1, const simd_BYTE vlow, const simd_BYTE vhigh,
			 const simd_UBYTE vtrue)
{
    vector bool char t1 = vec_cmplt(s1, vlow);
    vector bool char t2 = vec_cmpgt(s1, vhigh);
    t1 = vec_or(t1, t2);
    return vec_xor((simd_UBYTE)t1, vtrue);
}

static inline simd_UBYTE
_ip_simd_threshold_USHORT(simd_USHORT s1, simd_USHORT s2,
			  const simd_USHORT vlow, const simd_USHORT vhigh,
			  const simd_UBYTE vtrue)
{
    vector bool short t1 = vec_cmplt(s1, vlow);
    vector bool short t2 = vec_cmpgt(s1, vhigh);
    vector bool short t3 = vec_cmplt(s2, vlow);
    vector bool short t4 = vec_cmpgt(s2, vhigh);
    t1 = vec_or(t1, t2);
    t3 = vec_or(t3, t4);
    return vec_xor((simd_UBYTE)vec_pack(t1, t3), vtrue);
}


static inline simd_UBYTE
_ip_simd_threshold_SHORT(simd_SHORT s1, simd_SHORT s2,
			 const simd_SHORT vlow, const simd_SHORT vhigh,
			 const simd_UBYTE vtrue)
{
    vector bool short t1 = vec_cmplt(s1, vlow);
    vector bool short t2 = vec_cmpgt(s1, vhigh);
    vector bool short t3 = vec_cmplt(s2, vlow);
    vector bool short t4 = vec_cmpgt(s2, vhigh);
    t1 = vec_or(t1, t2);
    t3 = vec_or(t3, t4);
    return vec_xor((simd_UBYTE)vec_pack(t1, t3), vtrue);
}

static inline simd_UBYTE
_ip_simd_threshold_ULONG(simd_ULONG s1, simd_ULONG s2, simd_ULONG s3, simd_ULONG s4,
			 const simd_ULONG vlow, const simd_ULONG vhigh,
			 const simd_UBYTE vtrue)
{
    vector bool int t1 = vec_cmplt(s1, vlow);
    vector bool int t2 = vec_cmpgt(s1, vhigh);
    vector bool int t3 = vec_cmplt(s2, vlow);
    vector bool int t4 = vec_cmpgt(s2, vhigh);
    vector bool int t5 = vec_cmplt(s3, vlow);
    vector bool int t6 = vec_cmpgt(s3, vhigh);
    simd_USHORT a, b;
    t1 = vec_or(t1, t2);
    t3 = vec_or(t3, t4);
    t2 = vec_cmplt(s4, vlow);
    t4 = vec_cmpgt(s4, vhigh);
    t5 = vec_or(t5, t6);
    t2 = vec_or(t2, t4);
    a = (simd_USHORT)vec_pack(t1, t3);
    b = (simd_USHORT)vec_pack(t5, t2);
    return vec_xor(vec_pack(a, b), vtrue);
}

static inline simd_UBYTE
_ip_simd_threshold_LONG(simd_LONG s1, simd_LONG s2, simd_LONG s3, simd_LONG s4,
			const simd_LONG vlow, const simd_LONG vhigh,
			const simd_UBYTE vtrue)
{
    vector bool int t1 = vec_cmplt(s1, vlow);
    vector bool int t2 = vec_cmpgt(s1, vhigh);
    vector bool int t3 = vec_cmplt(s2, vlow);
    vector bool int t4 = vec_cmpgt(s2, vhigh);
    vector bool int t5 = vec_cmplt(s3, vlow);
    vector bool int t6 = vec_cmpgt(s3, vhigh);
    simd_USHORT a, b;
    t1 = vec_or(t1, t2);
    t3 = vec_or(t3, t4);
    t2 = vec_cmplt(s4, vlow);
    t4 = vec_cmpgt(s4, vhigh);
    t5 = vec_or(t5, t6);
    t2 = vec_or(t2, t4);
    a = (simd_USHORT)vec_pack(t1, t3);
    b = (simd_USHORT)vec_pack(t5, t2);
    return vec_xor(vec_pack(a, b), vtrue);
}


static inline simd_UBYTE
_ip_simd_threshold_FLOAT(simd_FLOAT s1, simd_FLOAT s2, simd_FLOAT s3, simd_FLOAT s4,
			 const simd_FLOAT vlow, const simd_FLOAT vhigh,
			 const simd_UBYTE vfalse)
{
    vector bool int t1 = vec_cmpge(s1, vlow);
    vector bool int t2 = vec_cmpge(vhigh, s1);
    vector bool int t3 = vec_cmpge(s2, vlow);
    vector bool int t4 = vec_cmpge(vhigh, s2);
    vector bool int t5 = vec_cmpge(s3, vlow);
    vector bool int t6 = vec_cmpge(vhigh, s3);
    simd_USHORT a, b;
    t1 = vec_and(t1, t2);
    t3 = vec_and(t3, t4);
    t2 = vec_cmpge(s4, vlow);
    t4 = vec_cmpge(vhigh, s4);
    t5 = vec_and(t5, t6);
    t2 = vec_and(t2, t4);
    a = (simd_USHORT)vec_pack(t1, t3);
    b = (simd_USHORT)vec_pack(t5, t2);
    return vec_xor(vec_pack(a, b), vfalse);
}


#define simd_init_threshold_UBYTE()
#define simd_init_threshold_BYTE()
#define simd_init_threshold_USHORT()
#define simd_init_threshold_SHORT()
#define simd_init_threshold_ULONG()
#define simd_init_threshold_LONG()
#define simd_init_threshold_FLOAT() vres = vec_nor(vres, vec_splat_u8(0));

#define simd_threshold_UBYTE _ip_simd_threshold_UBYTE
#define simd_threshold_BYTE _ip_simd_threshold_BYTE
#define simd_threshold_USHORT _ip_simd_threshold_USHORT
#define simd_threshold_SHORT _ip_simd_threshold_SHORT
#define simd_threshold_ULONG _ip_simd_threshold_ULONG
#define simd_threshold_LONG _ip_simd_threshold_LONG
#define simd_threshold_FLOAT _ip_simd_threshold_FLOAT

/*
 * For replicating one value of the vector to all other elements one side of
 * the value. Useful for managing image edges.  Right means replicate in the
 * direction of increasing memory address, i.e. along an image row in
 * increasing pixel order.
 */

static inline simd_UBYTE _ip_simd_replicate_right_UBYTE(simd_UBYTE x, int pos)
{
    const simd_UBYTE vinc = vec_lvsl(0, (unsigned char *)NULL);
    simd_UBYTE vpos = simd_splat_UBYTE(pos);
    return vec_perm(x, x, vec_min(vinc, vpos));
}
static inline simd_USHORT _ip_simd_replicate_right_USHORT(simd_USHORT x, int pos)
{
    const simd_USHORT vinc = (simd_USHORT)vec_lvsl(0, (unsigned char *)NULL);
    pos <<= 1;
    simd_USHORT vpos = simd_splat_USHORT((pos<<8)|(pos+1));
    return vec_perm(x, x, (simd_UBYTE)vec_min(vinc, vpos));
}
static inline simd_SHORT _ip_simd_replicate_right_SHORT(simd_SHORT x, int pos)
{
    const simd_SHORT vinc = (simd_SHORT)vec_lvsl(0, (unsigned char *)NULL);
    pos <<= 1;
    simd_SHORT vpos = simd_splat_SHORT((pos<<8)|(pos+1));
    return vec_perm(x, x, (simd_UBYTE)vec_min(vinc, vpos));
}
static inline simd_ULONG _ip_simd_replicate_right_ULONG(simd_ULONG x, int pos)
{
    const simd_ULONG vinc = (simd_ULONG)vec_lvsl(0, (unsigned char *)NULL);
    simd_ULONG vpos = simd_splat_ULONG(0x04040404*pos + 0x00010203);
    return vec_perm(x, x, (simd_UBYTE)vec_min(vinc, vpos));
}


static inline simd_UBYTE _ip_simd_replicate_left_UBYTE(simd_UBYTE x, int pos)
{
    const simd_UBYTE vinc = vec_lvsl(0, (unsigned char *)NULL);
    simd_UBYTE vpos = simd_splat_UBYTE(pos);
    return vec_perm(x, x, vec_max(vinc, vpos));
}
static inline simd_USHORT _ip_simd_replicate_left_USHORT(simd_USHORT x, int pos)
{
    const simd_USHORT vinc = (simd_USHORT)vec_lvsl(0, (unsigned char *)NULL);
    pos <<= 1;
    simd_USHORT vpos = simd_splat_USHORT((pos<<8)|(pos+1));
    return vec_perm(x, x, (simd_UBYTE)vec_max(vinc, vpos));
}
static inline simd_SHORT _ip_simd_replicate_left_SHORT(simd_SHORT x, int pos)
{
    const simd_SHORT vinc = (simd_SHORT)vec_lvsl(0, (unsigned char *)NULL);
    pos <<= 1;
    simd_SHORT vpos = simd_splat_SHORT((pos<<8)|(pos+1));
    return vec_perm(x, x, (simd_UBYTE)vec_max(vinc, vpos));
}
static inline simd_ULONG _ip_simd_replicate_left_ULONG(simd_ULONG x, int pos)
{
    const simd_ULONG vinc = (simd_ULONG)vec_lvsl(0, (unsigned char *)NULL);
    simd_ULONG vpos = simd_splat_ULONG(0x04040404*pos + 0x00010203);
    return vec_perm(x, x, (simd_UBYTE)vec_max(vinc, vpos));
}


#define simd_replicate_right_UBYTE(x, p) _ip_simd_replicate_right_UBYTE(x, p)
#define simd_replicate_right_BYTE(x, p) \
    ((simd_BYTE)_ip_simd_replicate_right_UBYTE((simd_UBYTE)x, p))
#define simd_replicate_left_UBYTE(x, p) _ip_simd_replicate_left_UBYTE(x, p)
#define simd_replicate_left_BYTE(x, p) \
    ((simd_BYTE)_ip_simd_replicate_left_UBYTE((simd_UBYTE)x, p))

#define simd_replicate_right_USHORT(x, p) _ip_simd_replicate_right_USHORT(x, p)
#define simd_replicate_right_SHORT(x, p) _ip_simd_replicate_right_SHORT(x, p)
#define simd_replicate_left_USHORT(x, p) _ip_simd_replicate_left_USHORT(x, p)
#define simd_replicate_left_SHORT(x, p) _ip_simd_replicate_left_SHORT(x, p)

#define simd_replicate_right_ULONG(x, p) _ip_simd_replicate_right_ULONG(x, p)
#define simd_replicate_right_LONG(x, p) \
    ((simd_LONG)_ip_simd_replicate_right_ULONG((simd_ULONG)x, p))
#define simd_replicate_left_ULONG(x, p) _ip_simd_replicate_left_ULONG(x, p)
#define simd_replicate_left_LONG(x, p) \
    ((simd_LONG)_ip_simd_replicate_left_ULONG((simd_ULONG)x, p))

#define simd_replicate_right_FLOAT(x, p) \
    ((simd_FLOAT)_ip_simd_replicate_right_ULONG((simd_ULONG)x, p))
#define simd_replicate_left_FLOAT(x, p) \
    ((simd_FLOAT)_ip_simd_replicate_left_ULONG((simd_ULONG)x, p))


/*
 * simd_zero_right
 *
 * zero_right means to zero all elements to the right (i.e. in increasing
 * memory address order) of the specified position pos.  It is like
 * replicate_right but is used to for effecting a zero boundary on the right
 * side of images in filtering operations.
 */

static inline simd_UBYTE _ip_simd_zero_right_UBYTE(simd_UBYTE x, int pos)
{
    const simd_UBYTE vinc = vec_lvsl(0, (unsigned char *)NULL);
    simd_UBYTE vpos = simd_splat_UBYTE(pos);
    return vec_andc(x, vec_cmpgt(vinc, vpos));
}

static inline simd_USHORT _ip_simd_zero_right_USHORT(simd_USHORT x, int pos)
{
    const simd_USHORT vinc = (simd_USHORT)vec_lvsl(0, (unsigned char *)NULL);
    simd_USHORT vpos = (simd_USHORT)simd_splat_UBYTE((pos<<1)+1);
    return vec_andc(x, vec_cmpgt(vinc, vpos));
}

static inline simd_ULONG _ip_simd_zero_right_ULONG(simd_ULONG x, int pos)
{
    const simd_ULONG vinc = (simd_ULONG)vec_lvsl(0, (unsigned char *)NULL);
    simd_ULONG vpos = (simd_ULONG)simd_splat_UBYTE((pos<<2)+3);
    return vec_andc(x, vec_cmpgt(vinc, vpos));
}

static inline simd_FLOAT _ip_simd_zero_right_FLOAT(simd_FLOAT x, int pos)
{
    const simd_ULONG vinc = (simd_ULONG)vec_lvsl(0, (unsigned char *)NULL);
    simd_ULONG vpos = (simd_ULONG)simd_splat_UBYTE((pos<<2)+3);
    return vec_andc(x, (simd_FLOAT)vec_cmpgt(vinc, vpos));
}

#define simd_zero_right_UBYTE(x, p) _ip_simd_zero_right_UBYTE(x, p)
#define simd_zero_right_BYTE(x, p) ((simd_BYTE)_ip_simd_zero_right_UBYTE((simd_UBYTE)x, p))
#define simd_zero_right_USHORT(x, p) _ip_simd_zero_right_USHORT(x, p)
#define simd_zero_right_SHORT(x, p) ((simd_SHORT)_ip_simd_zero_right_USHORT((simd_USHORT)x, p))
#define simd_zero_right_ULONG(x, p) _ip_simd_zero_right_ULONG(x, p)
#define simd_zero_right_LONG(x, p) ((simd_LONG)_ip_simd_zero_right_ULONG((simd_ULONG)x, p))
#define simd_zero_right_FLOAT(x, p) _ip_simd_zero_right_FLOAT(x, p)


static inline simd_UBYTE _ip_simd_zero_left_UBYTE(simd_UBYTE x, int pos)
{
    const simd_UBYTE vinc = vec_lvsl(0, (unsigned char *)NULL);
    simd_UBYTE vpos = simd_splat_UBYTE(pos);
    return vec_andc(x, vec_cmpgt(vpos, vinc));
}

static inline simd_USHORT _ip_simd_zero_left_USHORT(simd_USHORT x, int pos)
{
    const simd_USHORT vinc = (simd_USHORT)vec_lvsl(0, (unsigned char *)NULL);
    simd_USHORT vpos = (simd_USHORT)simd_splat_UBYTE(pos<<1);
    return vec_andc(x, vec_cmpgt(vpos, vinc));
}

static inline simd_ULONG _ip_simd_zero_left_ULONG(simd_ULONG x, int pos)
{
    const simd_ULONG vinc = (simd_ULONG)vec_lvsl(0, (unsigned char *)NULL);
    simd_ULONG vpos = (simd_ULONG)simd_splat_UBYTE(pos<<2);
    return vec_andc(x, vec_cmpgt(vpos, vinc));
}

static inline simd_FLOAT _ip_simd_zero_left_FLOAT(simd_FLOAT x, int pos)
{
    const simd_ULONG vinc = (simd_ULONG)vec_lvsl(0, (unsigned char *)NULL);
    simd_ULONG vpos = (simd_ULONG)simd_splat_UBYTE(pos<<2);
    return vec_andc(x, (simd_FLOAT)vec_cmpgt(vpos, vinc));
}

#define simd_zero_left_UBYTE(x, p) _ip_simd_zero_left_UBYTE(x, p)
#define simd_zero_left_BYTE(x, p) ((simd_BYTE)_ip_simd_zero_left_UBYTE((simd_UBYTE)x, p))
#define simd_zero_left_USHORT(x, p) _ip_simd_zero_left_USHORT(x, p)
#define simd_zero_left_SHORT(x, p) ((simd_SHORT)_ip_simd_zero_left_USHORT((simd_USHORT)x, p))
#define simd_zero_left_ULONG(x, p) _ip_simd_zero_left_ULONG(x, p)
#define simd_zero_left_LONG(x, p) ((simd_LONG)_ip_simd_zero_left_ULONG((simd_ULONG)x, p))
#define simd_zero_left_FLOAT(x, p) _ip_simd_zero_left_FLOAT(x, p)
