/*
  Header: simd/arith_simd.h
  Author: M. J. Cree

  The arrays holding the entry points to SIMD accelerated basic image
  arithmetic.

  Private to the IP library.

  (C) Michael J. Cree 2013

*/

extern improc_function_IIf ip_simd_add_same[IM_TYPEMAX];
extern improc_function_IIf ip_simd_add_clipped_same[IM_TYPEMAX];
extern improc_function_IIf ip_simd_add_mixed[IM_TYPEMAX];
extern improc_function_IIf ip_simd_add_clipped_mixed[IM_TYPEMAX];

extern improc_function_IIf ip_simd_sub_same[IM_TYPEMAX];
extern improc_function_IIf ip_simd_sub_clipped_same[IM_TYPEMAX];
extern improc_function_IIf ip_simd_sub_mixed[IM_TYPEMAX];
extern improc_function_IIf ip_simd_sub_clipped_mixed[IM_TYPEMAX];

extern improc_function_IIf ip_simd_mul_same[IM_TYPEMAX];
extern improc_function_IIf ip_simd_mul_clipped_same[IM_TYPEMAX];
extern improc_function_IIf ip_simd_mul_mixed[IM_TYPEMAX];
extern improc_function_IIf ip_simd_mul_clipped_mixed[IM_TYPEMAX];

extern improc_function_IIf ip_simd_div_same[IM_TYPEMAX];
extern improc_function_IIf ip_simd_div_mixed[IM_TYPEMAX];
extern improc_function_IIf ip_simd_div_protected_same[IM_TYPEMAX];
extern improc_function_IIf ip_simd_div_protected_mixed[IM_TYPEMAX];
