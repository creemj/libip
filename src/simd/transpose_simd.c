/*
   Module: src/simd/transpose_simd.c
   Author: M.J.Cree

   SIMD implementation for tranposing images.

   Copyright (C) 2016, 2019 Michael J. Cree

*/

#include <ip/ip.h>

#include "../internal.h"
#include "../function_ops.h"

#include "simd.h"
#include "transpose_simd.h"


#if defined(simd_transpose_4_UBYTE) || defined(simd_transpose_2_USHORT)

static simd_UBYTE *add_byte_incr_to_simd_ptr(simd_UBYTE *ptr, int incr)
{
    char *cptr;

    cptr = (char *)ptr;
    cptr += incr;
    return (simd_UBYTE *)cptr;
}

#endif

/*
 * We start by implementing the transpose of a tile. The tile is chosen to
 * be big enough to transform a reasonable amount of data along the rows to
 * exploit cache locality in the source and destination image without being
 * too big to trigger excessive cache misses in particular the destination
 * image as we write down columns.
 *
 * For most tile operators defined below the basic block processed in the
 * tile is of the size of the SIMD vector in bytes in both the x and y
 * direction, hence the block is square and consists of
 * sizeof(simd_TYPE)/ip_type_size(TYPE) pixels in both the x and in the y
 * directions.  Tiles must be a multiple of the basic block size.
 *
 * On Intel AVX2 we implement UBYTE and USHORT transposes with a completely
 * different technique and the basic block size differs as described below.
 */


#ifdef simd_transpose_4_UBYTE

/*
 * Only available on Intel AVX2.  This SIMD operator exploits the gather
 * loads of AVX2 to do efficient column loads of UBYTE data.
 *
 * The block size used in this tile is 4x32 pixels.  The tile size must
 * therefore be a multiple of 4 in the x-direction and of 32 in the
 * y-direction.
 */

static inline void transpose_tile_UBYTE(ip_image *dest, ip_image *src,
					int xofs, int yofs, int xsize, int ysize)
{
    for (int j=0; j<ysize; j+=32) {
	int y = yofs + j;
	for (int i=0; i<xsize; i+=4) {
	    simd_ULONG_4 col;
	    simd_UBYTE_4 res;
	    simd_UBYTE *dptr;
	    int x = xofs + i;

	    col = simd_column_load_4_ULONG(x, &src->imrow.v[y]);
	    res = simd_transpose_4_UBYTE(col);
	    dptr = add_byte_incr_to_simd_ptr(IM_ROWADR(dest, x, v), yofs);
	    *dptr = res.ub[0];
	    dptr = add_byte_incr_to_simd_ptr(IM_ROWADR(dest, x+1, v), yofs);
	    *dptr = res.ub[1];
	    dptr = add_byte_incr_to_simd_ptr(IM_ROWADR(dest, x+2, v), yofs);
	    *dptr = res.ub[2];
	    dptr = add_byte_incr_to_simd_ptr(IM_ROWADR(dest, x+3, v), yofs);
	    *dptr = res.ub[3];
	}
    }
}

#define HAVE_SIMD_TRANSPOSE_UBYTE
#endif

#ifdef simd_transpose_2_USHORT

/*
 * Only available on Intel AVX2.  This SIMD operator exploits the gather
 * loads of AVX2 to do efficient column loads of USHORT data.
 *
 * The block size used in this tile is 2x16 pixels.  The tile size must
 * therefore be a multiple of 2 in the x-direction and of 16 in the
 * y-direction.
 */

static inline void transpose_tile_USHORT(ip_image *dest, ip_image *src,
				  int xofs, int yofs, int xsize, int ysize)
{
    const int pixsize = sizeof(uint16_t);

    for (int j=0; j<ysize; j+=16) {
	int y = yofs + j;
	for (int i=0; i<xsize; i+=2) {
	    simd_ULONG_2 col;
	    simd_USHORT_2 res;
	    simd_USHORT *dptr;
	    int x = xofs + i;

	    col = simd_column_load_2_ULONG(x * pixsize,
					   &src->imrow.v[y]);
	    res = simd_transpose_2_USHORT(col);
	    dptr = add_byte_incr_to_simd_ptr(IM_ROWADR(dest, x, v), y*pixsize);
	    *dptr = res.us[0];
	    dptr = add_byte_incr_to_simd_ptr(IM_ROWADR(dest, x+1, v), y*pixsize);
	    *dptr = res.us[1];
	}
    }
}
#endif


/*
 * Generate a transpose_tile_TYPE() function for the case of a SIMD vector
 * containing eight elements of the pixel type.  The basic block size is
 * therefore 8x8 pixels.  Eight vectors are loaded and transposed.  This
 * should work nicely without register spill on architectures with only 16
 * SIMD registers.
 */

#define generate_process_tile_8x8(type, tt)				\
    static inline void transpose_tile_##type(ip_image *dest, ip_image *src, \
				      int xofs, int yofs, int xsize, int ysize) \
    {									\
	/* For each 8x8 pixel block in tile of xsize by ysize do. */	\
	for (int j=0; j<ysize; j+=8) {					\
	    int x = xofs;						\
	    int y = yofs + j;						\
	    simd_##type *spix1 = IM_ROWADR(src, y, v);			\
	    spix1 += xofs/8;						\
	    simd_##type *spix2 = IM_ROWADR(src, y+1, v);		\
	    spix2 += xofs/8;						\
	    simd_##type *spix3 = IM_ROWADR(src, y+2, v);		\
	    spix3 += xofs/8;						\
	    simd_##type *spix4 = IM_ROWADR(src, y+3, v);		\
	    spix4 += xofs/8;						\
	    simd_##type *spix5 = IM_ROWADR(src, y+4, v);		\
	    spix5 += xofs/8;						\
	    simd_##type *spix6 = IM_ROWADR(src, y+5, v);		\
	    spix6 += xofs/8;						\
	    simd_##type *spix7 = IM_ROWADR(src, y+6, v);		\
	    spix7 += xofs/8;						\
	    simd_##type *spix8 = IM_ROWADR(src, y+7, v);		\
	    spix8 += xofs/8;						\
	    for (int i=0; i<xsize; i+=8) {				\
		simd_##type##_8 block8x8;				\
		simd_##type *dpix;					\
		/* Read the 8x8 pixel block from the source image. */	\
		block8x8.tt[0] = *spix1++;				\
		block8x8.tt[1] = *spix2++;				\
		block8x8.tt[2] = *spix3++;				\
		block8x8.tt[3] = *spix4++;				\
		block8x8.tt[4] = *spix5++;				\
		block8x8.tt[5] = *spix6++;				\
		block8x8.tt[6] = *spix7++;				\
		block8x8.tt[7] = *spix8++;				\
		/* Tranpose the 8x8 pixel block. */			\
		block8x8 = simd_transpose_##type(block8x8);		\
		/* Write to transposed position in the destination image. */ \
		dpix = IM_ROWADR(dest, x++, v);				\
		dpix[y/8] = block8x8.tt[0];				\
		dpix = IM_ROWADR(dest, x++, v);				\
		dpix[y/8] = block8x8.tt[1];				\
		dpix = IM_ROWADR(dest, x++, v);				\
		dpix[y/8] = block8x8.tt[2];				\
		dpix = IM_ROWADR(dest, x++, v);				\
		dpix[y/8] = block8x8.tt[3];				\
		dpix = IM_ROWADR(dest, x++, v);				\
		dpix[y/8] = block8x8.tt[4];				\
		dpix = IM_ROWADR(dest, x++, v);				\
		dpix[y/8] = block8x8.tt[5];				\
		dpix = IM_ROWADR(dest, x++, v);				\
		dpix[y/8] = block8x8.tt[6];				\
		dpix = IM_ROWADR(dest, x++, v);				\
		dpix[y/8] = block8x8.tt[7];				\
	    }								\
	}								\
    }


/*
 * Generate a transpose_tile_TYPE() function for the case of a SIMD vector
 * containing four elements of the pixel type.  The basic block size is
 * therefore 4x4 pixels.  Four vectors are loaded and transposed.
 */

#define generate_process_tile_4x4(type, tt)				\
    static inline void transpose_tile_##type(ip_image *dest, ip_image *src, \
				      int xofs, int yofs, int xsize, int ysize) \
    {									\
	/* For each 4x4 pixel block in tile of xsize by ysize do. */	\
	for (int j=0; j<ysize; j+=4) {					\
	    int x = xofs;						\
	    int y = yofs + j;						\
	    simd_##type *spix1 = IM_ROWADR(src, y, v);			\
	    spix1 += xofs/4;						\
	    simd_##type *spix2 = IM_ROWADR(src, y+1, v);		\
	    spix2 += xofs/4;						\
	    simd_##type *spix3 = IM_ROWADR(src, y+2, v);		\
	    spix3 += xofs/4;						\
	    simd_##type *spix4 = IM_ROWADR(src, y+3, v);		\
	    spix4 += xofs/4;						\
	    for (int i=0; i<xsize; i+=4) {				\
		simd_##type##_4 block4x4;				\
		simd_##type *dpix;					\
		/* Read the 4x4 pixel block from the source image. */	\
		block4x4.tt[0] = *spix1++;				\
		block4x4.tt[1] = *spix2++;				\
		block4x4.tt[2] = *spix3++;				\
		block4x4.tt[3] = *spix4++;				\
		/* Tranpose the 4x4 pixel block. */			\
		block4x4 = simd_transpose_##type(block4x4);		\
		/* Write to transposed position in the destination image. */ \
		dpix = IM_ROWADR(dest, x++, v);				\
		dpix[y/4] = block4x4.tt[0];				\
		dpix = IM_ROWADR(dest, x++, v);				\
		dpix[y/4] = block4x4.tt[1];				\
		dpix = IM_ROWADR(dest, x++, v);				\
		dpix[y/4] = block4x4.tt[2];				\
		dpix = IM_ROWADR(dest, x++, v);				\
		dpix[y/4] = block4x4.tt[3];				\
	    }								\
	}								\
    }


/*
 * Generate a transpose_tile_TYPE() function for the case of a SIMD vector
 * containing two elements of the pixel type.  The basic block size is
 * therefore 2x2 pixels.  Two vectors are loaded and transposed.
 */

#define generate_process_tile_2x2(type, tt)				\
    static inline void transpose_tile_##type(ip_image *dest, ip_image *src, \
				      int xofs, int yofs, int xsize, int ysize) \
    {									\
	/* For each 2x2 pixel block in tile of xsize by ysize do. */	\
	for (int j=0; j<ysize; j+=2) {					\
	    int x = xofs;						\
	    int y = yofs + j;						\
	    simd_##type *spix1 = IM_ROWADR(src, y, v);			\
	    spix1 += xofs/2;						\
	    simd_##type *spix2 = IM_ROWADR(src, y+1, v);		\
	    spix2 += xofs/2;						\
	    for (int i=0; i<xsize; i+=2) {				\
		simd_##type##_2 block2x2;				\
		simd_##type *dpix;					\
		/* Read the 2x2 pixel block from the source image. */	\
		block2x2.tt[0] = *spix1++;				\
		block2x2.tt[1] = *spix2++;				\
		/* Tranpose the 2x2 pixel block. */			\
		block2x2 = simd_transpose_##type(block2x2);		\
		/* Write to transposed position in the destination image. */ \
		dpix = IM_ROWADR(dest, x++, v);				\
		dpix[y/2] = block2x2.tt[0];				\
		dpix = IM_ROWADR(dest, x++, v);				\
		dpix[y/2] = block2x2.tt[1];				\
	    }								\
	}								\
    }



/*
 * Generate the ip_simd_transpose_TYPE() functions.
 *
 * This function processes the image to transpose the image by tiles and
 * calls the function tile_transpose_TYPE() to transpose of the tile.
 *
 * These are the functions called from the generic code in transpose.c.
 */

#define generate_simd_im_transpose_tiled(type, tile_size)	       \
    static void ip_simd_transpose_##type(ip_image *dest, ip_image *src) \
    {									\
	int xlen = contained_pixels_in_padded_row(src, sizeof(simd_##type)); \
	int ylen = contained_pixels_in_padded_row(dest, sizeof(simd_##type)); \
	int xblks = (xlen + tile_size - 1) / tile_size;			\
	int yblks = (ylen + tile_size - 1) / tile_size;			\
	/* For each tile do */						\
	for (int m=0; m<yblks; m++) {					\
	    int yofs = m*tile_size;					\
	    int ysize = (ylen - yofs > tile_size) ? tile_size : (ylen - yofs); \
	    for (int k=0; k<xblks; k++) {				\
		int xofs = k*tile_size;					\
		int xsize = (xlen - xofs > tile_size) ? tile_size : (xlen - xofs); \
		/* Transpose tile */					\
		transpose_tile_##type(dest, src, xofs, yofs, xsize, ysize); \
	    }								\
	}								\
	SIMD_EXIT();							\
    }


/*
 * Now generate all the SIMD code.
 *
 * The tiling functions used for each type are dependent on the size of the
 * SIMD vector, hence we test for SIMD vector size.
 *
 * It is essential that the tile_size passed into
 * generate_simd_im_transpose_tiled() is an integer multiple of the basic
 * block used in the transpose_tile_TYPE() helper function.
 */


#if SIMD_VECTOR_SIZE == 8

/*
 * For a SIMD vector size of 8 bytes we use the 4x4 block for USHORT since
 * there are four USHORTS in the vector.  We currently do not implement
 * UBYTE because that would require 8x8 blocks, thus would result in
 * register spill on the only architecture supported here which has only
 * eight SIMD registers.
 */

#  ifdef simd_transpose_USHORT
generate_process_tile_4x4(USHORT, us)
generate_simd_im_transpose_tiled(USHORT, 32)
#    define HAVE_SIMD_TRANSPOSE_USHORT
#  endif

#  ifdef simd_transpose_ULONG
generate_process_tile_2x2(ULONG, ul)
generate_simd_im_transpose_tiled(ULONG, 16)
#    define HAVE_SIMD_TRANSPOSE_ULONG
#  endif

#elif SIMD_VECTOR_SIZE == 16

/*
 * For a SIMD vector size of 16 bytes we use the 8x8 block for USHORT since
 * there are eight USHORTS in the vector.  We currently do not implement
 * UBYTE because that would require 16x16 blocks, thus would result in
 * register spill on architectures with only 16 SIMD registers.
 */

#  ifdef simd_transpose_USHORT
generate_process_tile_8x8(USHORT, us)
generate_simd_im_transpose_tiled(USHORT, 32)
#    define HAVE_SIMD_TRANSPOSE_USHORT
#  endif

#  ifdef simd_transpose_ULONG
generate_process_tile_4x4(ULONG, ul)
generate_simd_im_transpose_tiled(ULONG, 16)
#    define HAVE_SIMD_TRANSPOSE_ULONG
#  endif

#  ifdef simd_transpose_DOUBLE
generate_process_tile_2x2(DOUBLE, d)
generate_simd_im_transpose_tiled(DOUBLE, 8)
#  define HAVE_SIMD_TRANSPOSE_DOUBLE
#  endif

#elif SIMD_VECTOR_SIZE == 32

/*
 * For SIMD vector of 32 bytes, ULONG/FLOAT are processed with 8x8 sized
 * blocks.  The implementations for UBYTE and USHORT depend on column gather
 * load instructions only available on Intel AVX2.  The implementations for
 * ULONG and larger types are more generic.
 */

#  ifdef simd_transpose_4_UBYTE
/*
 * transpose_tile_UBYTE() defined above for this case.
 * Tile size must be a multiple of 32.
 */
generate_simd_im_transpose_tiled(UBYTE, 32)
#    define HAVE_SIMD_TRANSPOSE_UBYTE
#  endif

#  ifdef simd_transpose_2_USHORT
/*
 * transpose_tile_USHORT() defined above for this case.
 * Tile size must be a multiple of 16.
 */
generate_simd_im_transpose_tiled(USHORT, 32)
#    define HAVE_SIMD_TRANSPOSE_USHORT
#  endif

#  ifdef simd_transpose_ULONG
generate_process_tile_8x8(ULONG, ul)
generate_simd_im_transpose_tiled(ULONG, 16)
#    define HAVE_SIMD_TRANSPOSE_ULONG
#  endif

#  ifdef simd_transpose_DOUBLE
generate_process_tile_4x4(DOUBLE, d)
generate_simd_im_transpose_tiled(DOUBLE, 8)
#    define HAVE_SIMD_TRANSPOSE_DOUBLE
#  endif

#  ifdef simd_transpose_DCOMPLEX
generate_process_tile_2x2(DCOMPLEX, dc)
generate_simd_im_transpose_tiled(DCOMPLEX, 8)
#    define HAVE_SIMD_TRANSPOSE_DCOMPLEX
#  endif

#endif


improc_function_II ip_simd_transpose[IM_TYPEMAX] = {
#ifdef HAVE_SIMD_TRANSPOSE_UBYTE
    [IM_PALETTE] = ip_simd_transpose_UBYTE,
    [IM_BINARY] = ip_simd_transpose_UBYTE,
    [IM_BYTE] = ip_simd_transpose_UBYTE,
    [IM_UBYTE] = ip_simd_transpose_UBYTE,
#endif
#ifdef HAVE_SIMD_TRANSPOSE_USHORT
    [IM_SHORT] = ip_simd_transpose_USHORT,
    [IM_USHORT] = ip_simd_transpose_USHORT,
#endif
#ifdef HAVE_SIMD_TRANSPOSE_ULONG
    [IM_LONG] = ip_simd_transpose_ULONG,
    [IM_ULONG] = ip_simd_transpose_ULONG,
    [IM_RGBA] = ip_simd_transpose_ULONG,
    [IM_FLOAT] = ip_simd_transpose_ULONG,
#endif
#ifdef HAVE_SIMD_TRANSPOSE_DOUBLE
    [IM_DOUBLE] = ip_simd_transpose_DOUBLE,
    [IM_COMPLEX] = ip_simd_transpose_DOUBLE,
#endif
#ifdef HAVE_SIMD_TRANSPOSE_DCOMPLEX
    [IM_DCOMPLEX] = ip_simd_transpose_DCOMPLEX,
#endif
};
