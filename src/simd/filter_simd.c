#include <ip/ip.h>

#include "../internal.h"
#include "../filterint.h"

#include "simd.h"
#include "simd-macros.h"
#include "filter_simd.h"

/*
 * Some protection against an incomplete SIMD arch heder file.  If this test
 * passes we assume we can at least support all of (U)BYTE and (U)SHORT.
 */
#if defined(simd_replicate_right_UBYTE) && defined(simd_cvu_imm_UBYTE)

/* Test to see whether we compile in LONG/ULONG support */
#if defined(simd_sub_LONG) && defined(simd_cvu_imm_LONG) && defined(simd_replicate_right_LONG)
#define SUPPORT_LONG
#endif

/* Test to see whether we compile in FLOAT support */
#if defined(simd_sub_FLOAT) && defined(simd_cvu_imm_FLOAT) && defined(simd_replicate_right_FLOAT)
#define SUPPORT_FLOAT
#endif

/* Test to see whether we compile in DOUBLE support */
#if defined(simd_sub_DOUBLE) && defined(simd_cvu_imm_DOUBLE) && defined(simd_replicate_right_DOUBLE)
#define SUPPORT_DOUBLE
#endif

/* Tests to see whether we compile in mean filters */
#if defined(simd_idiv_LONG)
#  if defined(simd_mean_SHORT)
#  define SUPPORT_MEAN_SHORT
#  endif
#  if defined(SUPPORT_LONG) && defined(simd_select_LONG) && defined(simd_mean_LONG)
#  define SUPPORT_MEAN_LONG
#  endif
#endif

#if defined(SUPPORT_FLOAT) && defined(simd_mean_FLOAT)
#define SUPPORT_MEAN_FLOAT
#endif

#if defined(SUPPORT_DOUBLE) && defined(simd_mean_DOUBLE)
#define SUPPORT_MEAN_DOUBLE
#endif


/*
 * To implememt filters that require some terms multiplied by either one or
 * two (e.g. the switch between Prewitt and Sobel) with the same function we
 * have to be able to multiply one calculation by two or not depending on
 * the filter.
 *
 * For integer types this is most efficient to effect by a shift left by 1
 * or 0.  For floating point we use multiplication.
 *
 * This is further complicated that we want to do this for all integer types
 * including UBYTE but Intel does not implement a suitable shift or
 * multiplication SIMD operator for UBYTE elements.
 *
 * Hence we separate out the multiplication into the following inline
 * functions and hide the trickery here.  There are two functions, one to
 * initialise the variable holding the multiplicand (for real data) or the
 * shift (for integer data), and the other to effect the multiplication by
 * one (Prewitt) or by two (Sobel).
 *
 * The code for the UBYTE on Intel architecture is then coded separately and
 * it calculates the multiplication by two by an addition, and then selects
 * between the two computation paths to get a mutliplication by one or by
 * two.
 */

#define generate_special_multiplier_init_int(tt, val)		\
    static simd_##tt special_multiplier_init_##tt(ip_kernel *k)	\
    {								\
	if ((k->reqtype & 0xff) == IP_KERNEL_SOBEL)		\
	    return val;						\
	else							\
	    return simd_splat_##tt(0);				\
    }

#define generate_special_multiplier_init_real(tt)			\
    static inline simd_##tt special_multiplier_init_##tt(ip_kernel *k)	\
    {									\
	if ((k->reqtype & 0xff) == IP_KERNEL_SOBEL)			\
	    return simd_splat_##tt(2.0);				\
	else								\
	    return simd_splat_##tt(1.0);				\
    }

#ifdef __x86_64__

/*
 * Implement the integer multiply on 64-bit Intel specially (mainly because
 * Intel does not have (u)byte shifts).
 */

generate_special_multiplier_init_int(UBYTE, simd_splat_UBYTE(0xff))
#ifdef HAVE_INTEL_AVX2
generate_special_multiplier_init_int(USHORT, _mm256_castsi128_si256(_mm_cvtsi32_si128(1)))
generate_special_multiplier_init_int(ULONG, _mm256_castsi128_si256(_mm_cvtsi32_si128(1)))
#else
generate_special_multiplier_init_int(USHORT, _mm_cvtsi32_si128(1))
generate_special_multiplier_init_int(ULONG, _mm_cvtsi32_si128(1))
#endif

static simd_UBYTE special_multiply_UBYTE(simd_UBYTE x, simd_UBYTE sel)
{
    simd_UBYTE doubled = simd_add_UBYTE(x,x);
    return v_select(sel, doubled, x);
}

#define special_multiply_USHORT(x,y) simd_shlc_USHORT(x,y)
#define special_multiply_ULONG(x,y) simd_shlc_ULONG(x,y)

#elif defined(__i386)

/* 32-bit Intel; same trickery as above for 64-bit */

generate_special_multiplier_init_int(UBYTE, simd_splat_UBYTE(0xff))
generate_special_multiplier_init_int(USHORT, _mm_cvtsi32_si64(1))
generate_special_multiplier_init_int(ULONG, _mm_cvtsi32_si64(1))

static simd_UBYTE special_multiply_UBYTE(simd_UBYTE x, simd_UBYTE sel)
{
    simd_UBYTE doubled = simd_add_UBYTE(x,x);
    return v_select(sel, doubled, x);
}

#define special_multiply_USHORT(x,y) simd_shlc_USHORT(x,y)
#define special_multiply_ULONG(x,y) simd_shlc_ULONG(x,y)

#elif defined(__alpha__)

/*
 * No arbitrary shifts on Alpha either so get up to special tricks here too.
 */

generate_special_multiplier_init_int(UBYTE, simd_splat_UBYTE(0xffU))
generate_special_multiplier_init_int(USHORT, simd_splat_USHORT(0xffffU))
generate_special_multiplier_init_int(ULONG, simd_splat_ULONG(0xffffffffU))

static simd_UBYTE special_multiply_UBYTE(simd_UBYTE x, simd_UBYTE sel)
{
    simd_UBYTE doubled = simd_add_UBYTE(x,x);
    return v_select(sel, doubled, x);
}

static simd_USHORT special_multiply_USHORT(simd_USHORT x, simd_USHORT sel)
{
    simd_USHORT doubled = simd_add_USHORT(x,x);
    return v_select(sel, doubled, x);
}

static simd_UBYTE special_multiply_ULONG(simd_ULONG x, simd_ULONG sel)
{
    simd_UBYTE doubled = simd_add_ULONG(x,x);
    return v_select(sel, doubled, x);
}

#else

/*
 * Implement the integer multiply on all arches except Intel and Alpha with
 * shifts.
 */

#define generate_special_multiply_int(tt)				\
    static inlint simd_##tt special_multiply_##tt(simd_##tt x, simd_##tt c)\
    {									\
	return simd_shl_##tt(x, c);					\
    }

generate_special_multiplier_init_int(UBYTE, simd_splat_UBYTE(1))
generate_special_multiplier_init_int(USHORT, simd_splat_USHORT(1))
generate_special_multiplier_init_int(ULONG, simd_splat_ULONG(1))

#define special_multiply_UBYTE(x,y) simd_shl_UBYTE(x,y)
#define special_multiply_USHORT(x,y) simd_shl_USHORT(x,y)
#define special_multiply_ULONG(x,y) simd_shl_ULONG(x,y)

/* End of architecture specific compilation units */
#endif


#ifdef SUPPORT_FLOAT
generate_special_multiplier_init_real(FLOAT)
#define special_multiply_FLOAT(x,y) simd_mul_FLOAT(x,y)
#endif

#ifdef SUPPORT_DOUBLE
generate_special_multiplier_init_real(DOUBLE)
#define special_multiply_DOUBLE(x,y) simd_mul_DOUBLE(x,y)
#endif


/*
 * 2-pt horizontal forward and backward differencing filters
 */

#define generate_filter_horiz_difference(tt)				\
    static void simd_filter_horiz_difference_##tt(ip_image *im,		\
						  ip_kernel *kern, ip_tile *tile) \
    {									\
	if (kern->type & KERNEL_BACKWARDS)				\
	    FILTER_HORIZ_2PT_BACKWARD_INPLACE(im, tt, simd_sub);	\
	else								\
	    FILTER_HORIZ_2PT_FORWARD_INPLACE(im, tt, simd_sub);		\
	SIMD_EXIT();							\
    }


generate_filter_horiz_difference(UBYTE)
generate_filter_horiz_difference(USHORT)
#ifdef SUPPORT_LONG
generate_filter_horiz_difference(ULONG)
#endif
#ifdef SUPPORT_FLOAT
generate_filter_horiz_difference(FLOAT)
#endif
#ifdef SUPPORT_DOUBLE
generate_filter_horiz_difference(DOUBLE)
#endif

improc_function_Ikt ip_simd_filter_horiz_difference_bytype[IM_TYPEMAX] = {
    [IM_BYTE] = simd_filter_horiz_difference_UBYTE,
    [IM_UBYTE] = simd_filter_horiz_difference_UBYTE,
    [IM_SHORT] = simd_filter_horiz_difference_USHORT,
    [IM_USHORT] = simd_filter_horiz_difference_USHORT,
#ifdef SUPPORT_LONG
    [IM_LONG] = simd_filter_horiz_difference_ULONG,
    [IM_ULONG] = simd_filter_horiz_difference_ULONG,
#endif
#ifdef SUPPORT_FLOAT
    [IM_FLOAT] = simd_filter_horiz_difference_FLOAT,
#endif
#ifdef SUPPORT_DOUBLE
    [IM_DOUBLE] = simd_filter_horiz_difference_DOUBLE,
#endif
};


/*
 * 2-pt vertical forward and backward differencing filters
 */

#define generate_filter_vert_difference(tt)				\
    static void simd_filter_vert_difference_##tt(ip_image *im, ip_kernel *kern, ip_tile *tile) \
    {                                                                   \
	int rowlen = im->row_size / sizeof(simd_##tt);			\
     									\
	if (kern->type & KERNEL_BACKWARDS) {				\
	    for (int j=im->size.y-1; j>0; j--) {			\
		simd_##tt *iptr = IM_ROWADR(im, j, v);			\
		simd_##tt *idown = IM_ROWADR(im, j-1, v);		\
									\
		for (int i=0; i<rowlen; i++)				\
		    iptr[i] = simd_sub_##tt(iptr[i], idown[i]);		\
	    }								\
	    simd_##tt *iptr = IM_ROWADR(im, 0, v);			\
	    for (int i=0; i<rowlen; i++)				\
		iptr[i] = simd_splat_##tt(0);				\
	}else{								\
	    for (int j=0; j<im->size.y-1; j++) {			\
		simd_##tt *iptr = IM_ROWADR(im, j, v);			\
		simd_##tt *iup = IM_ROWADR(im, j+1, v);			\
	     								\
		for (int i=0; i<rowlen; i++)				\
		    iptr[i] = simd_sub_##tt(iup[i], iptr[i]);		\
	    }								\
	    simd_##tt *iptr = IM_ROWADR(im, im->size.y-1, v);		\
	    for (int i=0; i<rowlen; i++)				\
		iptr[i] = simd_splat_##tt(0);				\
	}								\
	SIMD_EXIT();							\
    }


generate_filter_vert_difference(UBYTE)
generate_filter_vert_difference(USHORT)
#ifdef SUPPORT_LONG
generate_filter_vert_difference(ULONG)
#endif
#ifdef SUPPORT_FLOAT
generate_filter_vert_difference(FLOAT)
#endif
#ifdef SUPPORT_DOUBLE
generate_filter_vert_difference(DOUBLE)
#endif

improc_function_Ikt ip_simd_filter_vert_difference_bytype[IM_TYPEMAX] = {
    [IM_BYTE] = simd_filter_vert_difference_UBYTE,
    [IM_UBYTE] = simd_filter_vert_difference_UBYTE,
    [IM_SHORT] = simd_filter_vert_difference_USHORT,
    [IM_USHORT] = simd_filter_vert_difference_USHORT,
#ifdef SUPPORT_LONG
    [IM_LONG] = simd_filter_vert_difference_ULONG,
    [IM_ULONG] = simd_filter_vert_difference_ULONG,
#endif
#ifdef SUPPORT_FLOAT
    [IM_FLOAT] = simd_filter_vert_difference_FLOAT,
#endif
#ifdef SUPPORT_DOUBLE
    [IM_DOUBLE] = simd_filter_vert_difference_DOUBLE,
#endif
};



/*
 * 3-pt horizontal centre difference filter
 *
 * simd_cdiff: implements a simd operator that substracts the third arg from
 * the first.  Used in the implementation of the centre difference filter.
 */

#define simd_cdiff_UBYTE(a,b,c) simd_sub_UBYTE(c, a)
#define simd_cdiff_USHORT(a,b,c) simd_sub_USHORT(c, a)
#define simd_cdiff_ULONG(a,b,c) simd_sub_ULONG(c, a)
#define simd_cdiff_FLOAT(a,b,c) simd_sub_FLOAT(c, a)
#define simd_cdiff_DOUBLE(a,b,c) simd_sub_DOUBLE(c, a)


#define generate_filter_horiz_centre_difference(tt)				\
    static void simd_filter_horiz_centre_difference_##tt(ip_image *im, ip_kernel *kern, ip_tile *tile) \
    {									\
	FILTER_HORIZ_3PT_CENTRE_INPLACE(im, tt, simd_cdiff);		\
	SIMD_EXIT();							\
    }


generate_filter_horiz_centre_difference(UBYTE)
generate_filter_horiz_centre_difference(USHORT)
#ifdef SUPPORT_LONG
generate_filter_horiz_centre_difference(ULONG)
#endif
#ifdef SUPPORT_FLOAT
generate_filter_horiz_centre_difference(FLOAT)
#endif
#ifdef SUPPORT_DOUBLE
generate_filter_horiz_centre_difference(DOUBLE)
#endif


improc_function_Ikt ip_simd_filter_horiz_centre_difference_bytype[IM_TYPEMAX] = {
    [IM_BYTE] = simd_filter_horiz_centre_difference_UBYTE,
    [IM_UBYTE] = simd_filter_horiz_centre_difference_UBYTE,
    [IM_SHORT] = simd_filter_horiz_centre_difference_USHORT,
    [IM_USHORT] = simd_filter_horiz_centre_difference_USHORT,
#ifdef SUPPORT_LONG
    [IM_LONG] = simd_filter_horiz_centre_difference_ULONG,
    [IM_ULONG] = simd_filter_horiz_centre_difference_ULONG,
#endif
#ifdef SUPPORT_FLOAT
    [IM_FLOAT] = simd_filter_horiz_centre_difference_FLOAT,
#endif
#ifdef SUPPORT_DOUBLE
    [IM_DOUBLE] = simd_filter_horiz_centre_difference_DOUBLE,
#endif
};



#define generate_filter_vert_centre_difference(tt)			\
    static void simd_filter_vert_centre_difference_##tt(ip_image *im, ip_kernel *kern, ip_tile *tile) \
    {									\
	int rowlen = im->row_size / sizeof(simd_##tt);			\
	simd_##tt *this = IM_ROWADR(im, 0, v);				\
	simd_##tt *prev = IM_ROWADR(tile->tbuf, 0, v);			\
	for (int j=tile->origin.y; j<im->size.y; j++) {			\
	    simd_##tt *next;						\
	    if (j<im->size.y-1)						\
		next = IM_ROWADR(im, j+1, v);				\
	    else							\
		next = this;						\
	    for (int i=0; i<rowlen; i++) {				\
		simd_##tt tval = this[i];				\
		simd_##tt nval = simd_sub_##tt(next[i], prev[i]);	\
		prev[i] = tval;						\
		this[i] = nval;						\
	    }								\
	    this = next;						\
	}								\
	SIMD_EXIT();							\
    }

generate_filter_vert_centre_difference(UBYTE)
generate_filter_vert_centre_difference(USHORT)
#ifdef SUPPORT_LONG
generate_filter_vert_centre_difference(ULONG)
#endif
#ifdef SUPPORT_FLOAT
generate_filter_vert_centre_difference(FLOAT)
#endif
#ifdef SUPPORT_DOUBLE
generate_filter_vert_centre_difference(DOUBLE)
#endif


improc_function_Ikt ip_simd_filter_vert_centre_difference_bytype[IM_TYPEMAX] = {
    [IM_BYTE] = simd_filter_vert_centre_difference_UBYTE,
    [IM_UBYTE] = simd_filter_vert_centre_difference_UBYTE,
    [IM_SHORT] = simd_filter_vert_centre_difference_USHORT,
    [IM_USHORT] = simd_filter_vert_centre_difference_USHORT,
#ifdef SUPPORT_LONG
    [IM_LONG] = simd_filter_vert_centre_difference_ULONG,
    [IM_ULONG] = simd_filter_vert_centre_difference_ULONG,
#endif
#ifdef SUPPORT_FLOAT
    [IM_FLOAT] = simd_filter_vert_centre_difference_FLOAT,
#endif
#ifdef SUPPORT_DOUBLE
    [IM_DOUBLE] = simd_filter_vert_centre_difference_DOUBLE,
#endif
};


/*
 * The Laplace difference filter being an approximation to the second
 * derivative.
 *
 * simd_laplace_diff: a SIMD operator that implements the laplace filter,
 * namely (x[-1] - 2x[0] + x[1]).  Since multiplication can be slower on
 * some arches than addition we add x[0] to itself to get the multiplication
 * by two.  Futhermore, arranging as (x[-1]+x[1]) - (x[0]+x[0])) allows the
 * Laplace diff to be calculated in two steps on a superscalar CPU.
 */

#define simd_laplace_diff_UBYTE(a,b,c) \
    simd_sub_UBYTE(simd_add_UBYTE(a,c), simd_add_UBYTE(b, b))
#define simd_laplace_diff_USHORT(a,b,c) \
    simd_sub_USHORT(simd_add_USHORT(a,c), simd_add_USHORT(b, b))
#define simd_laplace_diff_ULONG(a,b,c) \
    simd_sub_ULONG(simd_add_ULONG(a,c), simd_add_ULONG(b, b))
#define simd_laplace_diff_FLOAT(a,b,c) \
    simd_sub_FLOAT(simd_add_FLOAT(a,c), simd_add_FLOAT(b, b))
#define simd_laplace_diff_DOUBLE(a,b,c) \
    simd_sub_DOUBLE(simd_add_DOUBLE(a,c), simd_add_DOUBLE(b, b))


#define generate_filter_horiz_laplace_difference(tt)				\
    static void simd_filter_horiz_laplace_difference_##tt(ip_image *im, ip_kernel *kern, ip_tile *tile) \
    {									\
	FILTER_HORIZ_3PT_CENTRE_INPLACE(im, tt, simd_laplace_diff);	\
	SIMD_EXIT();							\
    }

generate_filter_horiz_laplace_difference(UBYTE)
generate_filter_horiz_laplace_difference(USHORT)
#ifdef SUPPORT_LONG
generate_filter_horiz_laplace_difference(ULONG)
#endif
#ifdef SUPPORT_FLOAT
generate_filter_horiz_laplace_difference(FLOAT)
#endif
#ifdef SUPPORT_DOUBLE
generate_filter_horiz_laplace_difference(DOUBLE)
#endif

improc_function_Ikt ip_simd_filter_horiz_laplace_difference_bytype[IM_TYPEMAX] = {
    [IM_BYTE] = simd_filter_horiz_laplace_difference_UBYTE,
    [IM_UBYTE] = simd_filter_horiz_laplace_difference_UBYTE,
    [IM_SHORT] = simd_filter_horiz_laplace_difference_USHORT,
    [IM_USHORT] = simd_filter_horiz_laplace_difference_USHORT,
#ifdef SUPPORT_LONG
    [IM_LONG] = simd_filter_horiz_laplace_difference_ULONG,
    [IM_ULONG] = simd_filter_horiz_laplace_difference_ULONG,
#endif
#ifdef SUPPORT_FLOAT
    [IM_FLOAT] = simd_filter_horiz_laplace_difference_FLOAT,
#endif
#ifdef SUPPORT_DOUBLE
    [IM_DOUBLE] = simd_filter_horiz_laplace_difference_DOUBLE,
#endif
};


#define generate_filter_vert_laplace_difference(tt)			\
    static void simd_filter_vert_laplace_difference_##tt(ip_image *im, ip_kernel *kern, ip_tile *tile) \
    {									\
	int rowlen = im->row_size / sizeof(simd_##tt);			\
	simd_##tt *this = IM_ROWADR(im, 0, v);				\
	simd_##tt *prev = IM_ROWADR(tile->tbuf, 0, v);			\
	for (int j=tile->origin.y; j<im->size.y; j++) {			\
	    simd_##tt *next;						\
	    if (j<im->size.y-1)						\
		next = IM_ROWADR(im, j+1, v);				\
	    else							\
		next = this;						\
	    for (int i=0; i<rowlen; i++) {				\
		simd_##tt nval;						\
		simd_##tt tval = this[i];				\
		nval = simd_add_##tt(next[i], prev[i]);			\
		nval = simd_sub_##tt(nval, this[i]);			\
		nval = simd_sub_##tt(nval, this[i]);			\
		prev[i] = tval;						\
		this[i] = nval;						\
	    }								\
	    this = next;						\
	}								\
	SIMD_EXIT();							\
    }

generate_filter_vert_laplace_difference(UBYTE)
generate_filter_vert_laplace_difference(USHORT)
#ifdef SUPPORT_LONG
generate_filter_vert_laplace_difference(ULONG)
#endif
#ifdef SUPPORT_FLOAT
generate_filter_vert_laplace_difference(FLOAT)
#endif
#ifdef SUPPORT_DOUBLE
generate_filter_vert_laplace_difference(DOUBLE)
#endif

improc_function_Ikt ip_simd_filter_vert_laplace_difference_bytype[IM_TYPEMAX] = {
    [IM_BYTE] = simd_filter_vert_laplace_difference_UBYTE,
    [IM_UBYTE] = simd_filter_vert_laplace_difference_UBYTE,
    [IM_SHORT] = simd_filter_vert_laplace_difference_USHORT,
    [IM_USHORT] = simd_filter_vert_laplace_difference_USHORT,
#ifdef SUPPORT_LONG
    [IM_LONG] = simd_filter_vert_laplace_difference_ULONG,
    [IM_ULONG] = simd_filter_vert_laplace_difference_ULONG,
#endif
#ifdef SUPPORT_FLOAT
    [IM_FLOAT] = simd_filter_vert_laplace_difference_FLOAT,
#endif
#ifdef SUPPORT_DOUBLE
    [IM_DOUBLE] = simd_filter_vert_laplace_difference_DOUBLE,
#endif
};


/*
 * The well known edge detection filters, i.e. the Sobel and Prewitt
 * filters.
 *
 * Uses the special_multiply_TYPE() macros defined above to efficiently
 * calculate the multiplation by 1 or by 2 so that either Sobel or Prewitt
 * can be calculated with the same routine.
 */

#define generate_filter_vert_edge3x3(tt)				\
    static void simd_filter_vert_edge3x3_##tt(ip_image *im, ip_kernel *kern, ip_tile *tile) \
    {									\
	int rowlen = im->row_size / sizeof(simd_##tt);			\
	simd_##tt *up = IM_ROWADR(tile->tbuf, 0, v);			\
	simd_##tt *centre = IM_ROWADR(im, 0, v);			\
	simd_##tt multiplier;						\
	multiplier = special_multiplier_init_##tt(kern);		\
	for (int j=0; j<im->size.y; j++) {				\
	    simd_##tt *down;						\
	    simd_##tt pup, tup, nup, pdown, tdown, ndown;		\
	    int pos;							\
	    if (j<im->size.y-1)						\
		down = IM_ROWADR(im, j+1, v);				\
	    else							\
		down = centre;						\
	    tup = up[0];						\
	    tdown = down[0];						\
	    pup = simd_splat_v0_##tt(tup);				\
	    pdown = simd_splat_v0_##tt(tdown);				\
	    for (int i=0; i<rowlen-1; i++) {				\
		simd_##tt val, upval, downval, accum;			\
		nup = up[i+1];						\
		ndown = down[i+1];					\
		upval = simd_cvu_imm_##tt(pup, tup, SIMD_NUM_ELEMENTS(tt)-1); \
		downval = simd_cvu_imm_##tt(pdown, tdown, SIMD_NUM_ELEMENTS(tt)-1); \
		accum = simd_sub_##tt(downval, upval);			\
		val = simd_sub_##tt(tdown, tup);			\
		val = special_multiply_##tt(val, multiplier);		\
		accum = simd_add_##tt(accum, val);			\
		upval = simd_cvu_imm_##tt(tup, nup, 1);			\
		downval = simd_cvu_imm_##tt(tdown, ndown, 1);		\
		val = simd_sub_##tt(downval, upval);			\
		accum = simd_add_##tt(accum, val);			\
		up[i] = centre[i];					\
		centre[i] = accum;					\
		pup = tup;						\
		tup = nup;						\
		pdown = tdown;						\
		tdown = ndown;						\
	    }								\
	    pos = im->size.x - SIMD_NUM_ELEMENTS(tt)*(rowlen-1) - 1;	\
	    if (pos >= 0) {						\
		simd_##tt val, upval, downval, accum;			\
		tup = simd_replicate_right_##tt(tup, pos);		\
		nup = simd_replicate_left_##tt(tup, pos);		\
		tdown = simd_replicate_right_##tt(tdown, pos);		\
		ndown = simd_replicate_left_##tt(tdown, pos);		\
		upval = simd_cvu_imm_##tt(pup, tup, SIMD_NUM_ELEMENTS(tt)-1); \
		downval = simd_cvu_imm_##tt(pdown, tdown, SIMD_NUM_ELEMENTS(tt)-1); \
		accum = simd_sub_##tt(downval, upval);			\
		val = simd_sub_##tt(tdown, tup);			\
	        val = special_multiply_##tt(val, multiplier);		\
		accum = simd_add_##tt(accum, val);			\
		upval = simd_cvu_imm_##tt(tup, nup, 1);			\
		downval = simd_cvu_imm_##tt(tdown, ndown, 1);		\
		val = simd_sub_##tt(downval, upval);			\
		accum = simd_add_##tt(accum, val);			\
		up[rowlen-1] = centre[rowlen-1];			\
	        centre[rowlen-1] = accum;				\
	    }								\
	    centre = down;						\
	}								\
	SIMD_EXIT();							\
    }


generate_filter_vert_edge3x3(UBYTE)
generate_filter_vert_edge3x3(USHORT)
#ifdef SUPPORT_LONG
generate_filter_vert_edge3x3(ULONG)
#endif
#ifdef SUPPORT_FLOAT
generate_filter_vert_edge3x3(FLOAT)
#endif
#ifdef SUPPORT_DOUBLE
generate_filter_vert_edge3x3(DOUBLE)
#endif

improc_function_Ikt ip_simd_filter_vert_edge3x3_bytype[IM_TYPEMAX] = {
    [IM_BYTE] = simd_filter_vert_edge3x3_UBYTE,
    [IM_UBYTE] = simd_filter_vert_edge3x3_UBYTE,
    [IM_SHORT] = simd_filter_vert_edge3x3_USHORT,
    [IM_USHORT] = simd_filter_vert_edge3x3_USHORT,
#ifdef SUPPORT_LONG
    [IM_LONG] = simd_filter_vert_edge3x3_ULONG,
    [IM_ULONG] = simd_filter_vert_edge3x3_ULONG,
#endif
#ifdef SUPPORT_FLOAT
    [IM_FLOAT] = simd_filter_vert_edge3x3_FLOAT,
#endif
#ifdef SUPPORT_DOUBLE
    [IM_DOUBLE] = simd_filter_vert_edge3x3_DOUBLE,
#endif
};





#define generate_filter_horiz_edge3x3(tt)				\
    static void simd_filter_horiz_edge3x3_##tt(ip_image *im, ip_kernel *kern, ip_tile *tile) \
    {									\
	int rowlen = im->row_size / sizeof(simd_##tt);			\
	simd_##tt *up = IM_ROWADR(tile->tbuf, 0, v);			\
	simd_##tt *centre = IM_ROWADR(im, 0, v);			\
	simd_##tt multiplier;						\
	multiplier = special_multiplier_init_##tt(kern);		\
	for (int j=0; j<im->size.y; j++) {				\
	    int pos;							\
	    simd_##tt *down;						\
	    simd_##tt pup, tup, nup, pdown, tdown, ndown;		\
	    simd_##tt pcentre, tcentre, ncentre;			\
	    if (j < im->size.y-1)					\
		down = IM_ROWADR(im, j+1, v);				\
	    else							\
		down = IM_ROWADR(im, j, v);				\
	    tup = up[0];						\
	    tcentre = centre[0];					\
	    tdown = down[0];						\
	    pup = simd_splat_v0_##tt(tup);				\
	    pcentre = simd_splat_v0_##tt(tcentre);			\
	    pdown = simd_splat_v0_##tt(tdown);				\
	    for (int i=0; i<rowlen-1; i++) {				\
		simd_##tt val, lval, rval, accum;			\
		nup = up[i+1];						\
		ncentre = centre[i+1];					\
		ndown = down[i+1];					\
		lval = simd_cvu_imm_##tt(pup, tup, SIMD_NUM_ELEMENTS(tt)-1); \
	        rval = simd_cvu_imm_##tt(tup, nup, 1);			\
		accum = simd_sub_##tt(rval, lval);			\
		lval = simd_cvu_imm_##tt(pcentre, tcentre, SIMD_NUM_ELEMENTS(tt)-1); \
		rval = simd_cvu_imm_##tt(tcentre, ncentre, 1);		\
		val = simd_sub_##tt(rval, lval);			\
		val = special_multiply_##tt(val, multiplier);		\
		accum = simd_add_##tt(accum, val);			\
		lval = simd_cvu_imm_##tt(pdown, tdown, SIMD_NUM_ELEMENTS(tt)-1); \
		rval = simd_cvu_imm_##tt(tdown, ndown, 1);		\
		val = simd_sub_##tt(rval, lval);			\
		accum = simd_add_##tt(accum, val);			\
		up[i] = centre[i];					\
		centre[i] = accum;					\
		pup = tup;						\
		tup = nup;						\
		pcentre = tcentre;					\
		tcentre = ncentre;					\
		pdown = tdown;						\
		tdown = ndown;						\
	    }								\
	    pos = im->size.x - SIMD_NUM_ELEMENTS(tt)*(rowlen-1) - 1;	\
	    if (pos >= 0) {						\
		simd_##tt val, lval, rval, accum;			\
		tup = simd_replicate_right_##tt(tup, pos);		\
		nup = simd_replicate_left_##tt(tup, pos);		\
		tdown = simd_replicate_right_##tt(tdown, pos);		\
		ndown = simd_replicate_left_##tt(tdown, pos);		\
		tcentre = simd_replicate_right_##tt(tcentre, pos);	\
		ncentre = simd_replicate_left_##tt(tcentre, pos);	\
		lval = simd_cvu_imm_##tt(pup, tup, SIMD_NUM_ELEMENTS(tt)-1); \
	        rval = simd_cvu_imm_##tt(tup, nup, 1);			\
		accum = simd_sub_##tt(rval, lval);			\
		lval = simd_cvu_imm_##tt(pcentre, tcentre, SIMD_NUM_ELEMENTS(tt)-1); \
		rval = simd_cvu_imm_##tt(tcentre, ncentre, 1);		\
		val = simd_sub_##tt(rval, lval);			\
		val = special_multiply_##tt(val, multiplier);		\
		accum = simd_add_##tt(accum, val);			\
		lval = simd_cvu_imm_##tt(pdown, tdown, SIMD_NUM_ELEMENTS(tt)-1); \
		rval = simd_cvu_imm_##tt(tdown, ndown, 1);		\
		val = simd_sub_##tt(rval, lval);			\
		accum = simd_add_##tt(accum, val);			\
		up[rowlen-1] = centre[rowlen-1];			\
		centre[rowlen-1] = accum;				\
	    }								\
	    centre = down;						\
	}								\
	SIMD_EXIT();							\
    }


generate_filter_horiz_edge3x3(UBYTE)
generate_filter_horiz_edge3x3(USHORT)
#ifdef SUPPORT_LONG
generate_filter_horiz_edge3x3(ULONG)
#endif
#ifdef SUPPORT_FLOAT
generate_filter_horiz_edge3x3(FLOAT)
#endif
#ifdef SUPPORT_DOUBLE
generate_filter_horiz_edge3x3(DOUBLE)
#endif

improc_function_Ikt ip_simd_filter_horiz_edge3x3_bytype[IM_TYPEMAX] = {
    [IM_BYTE] = simd_filter_horiz_edge3x3_UBYTE,
    [IM_UBYTE] = simd_filter_horiz_edge3x3_UBYTE,
    [IM_SHORT] = simd_filter_horiz_edge3x3_USHORT,
    [IM_USHORT] = simd_filter_horiz_edge3x3_USHORT,
#ifdef SUPPORT_LONG
    [IM_LONG] = simd_filter_horiz_edge3x3_ULONG,
    [IM_ULONG] = simd_filter_horiz_edge3x3_ULONG,
#endif
#ifdef SUPPORT_FLOAT
    [IM_FLOAT] = simd_filter_horiz_edge3x3_FLOAT,
#endif
#ifdef SUPPORT_DOUBLE
    [IM_DOUBLE] = simd_filter_horiz_edge3x3_DOUBLE,
#endif
};


/*
 * Mean filters.
 *
 * Only partially implemented.
 */


/*
 * Helpers; the two element long mean filter on 32-bit data is not
 * calculated at a higher precision.  Define a meanp simd operator for this
 * purpose.
 */

#ifdef SUPPORT_MEAN_LONG
static inline simd_ULONG simd_meanp_ULONG(simd_ULONG a, simd_ULONG b)
{
    const simd_ULONG one = simd_splat_ULONG(1);
    return simd_shr_imm_ULONG(simd_add_ULONG(simd_add_ULONG(a, b), one), 1);
}

static inline simd_LONG simd_meanp_LONG(simd_LONG a, simd_LONG b)
{
    const simd_LONG zero = simd_splat_LONG(0);
    const simd_LONG one = simd_splat_LONG(1);
    simd_LONG ures = simd_add_LONG(a, b);
    ures = simd_select_LONG(simd_cmplt_LONG(ures, zero), ures, simd_add_LONG(ures, one));
    return simd_shra_imm_LONG(ures, 1);
}
#endif


/*
 * Inplace 1xM vertical mean filter.  We process down columns but to make
 * better use of cache we process four simd vectors wide.
 */

#define INIT_KSUMS_WITH_N_TIMES_VAL(tt, num, iptr) \
    simd_##tt multiplier = simd_splat_##tt(num);   \
    ksum1 = simd_mul_##tt(multiplier, iptr[i]);	   \
    ksum2 = simd_mul_##tt(multiplier, iptr[i+1]);  \
    ksum3 = simd_mul_##tt(multiplier, iptr[i+2]);  \
    ksum4 = simd_mul_##tt(multiplier, iptr[i+3]);
#define ADD_TO_KSUMS(tt, iptr)			\
    ksum1 = simd_add_##tt(ksum1, iptr[i]);	\
    ksum2 = simd_add_##tt(ksum2, iptr[i+1]);	\
    ksum3 = simd_add_##tt(ksum3, iptr[i+2]);	\
    ksum4 = simd_add_##tt(ksum4, iptr[i+3]);
#define SUB_FROM_KSUMS(tt, tptr, idx)		\
    ksum1 = simd_sub_##tt(ksum1, tptr##1[idx]);	\
    ksum2 = simd_sub_##tt(ksum2, tptr##2[idx]);	\
    ksum3 = simd_sub_##tt(ksum3, tptr##3[idx]);	\
    ksum4 = simd_sub_##tt(ksum4, tptr##4[idx]);
#define SET_PIXELS_FROM_KSUMS(tt, iptr, divider, rounder)	\
    iptr[i] = divider(ksum1, rounder);		\
    iptr[i+1] = divider(ksum2, rounder);	\
    iptr[i+2] = divider(ksum3, rounder);	\
    iptr[i+3] = divider(ksum4, rounder);
#define SET_TILE_PIXELS(tptr, iptr)		\
    tptr##1[j] = iptr[i];			\
    tptr##2[j] = iptr[i+1];			\
    tptr##3[j] = iptr[i+2];			\
    tptr##4[j] = iptr[i+3];

#define generate_filter_vert_mean_inplace_n(tt, divider_init, divider)	\
    static void simd_filter_vert_mean_inplace_n_##tt(ip_image *im, ip_kernel *kern, ip_tile *tile) \
    {									\
	int len = kern->size.y;						\
	int ridx = len / 2;						\
	int lidx = (len-1) / 2;						\
	ip_image *tbuf = tile->tbuf;					\
	int rowlen = im->row_size / sizeof(simd_##tt);			\
	divider_init(tt, len);						\
	/* Process along the rows four simd vectors at a time */	\
	for (int i=0; i<(rowlen & ~3); i+=4) {				\
	    simd_##tt ksum1, ksum2, ksum3, ksum4;			\
	    simd_##tt *impix, *dpix;					\
	    simd_##tt *tpix1, *tpix2, *tpix3, *tpix4;			\
	    tpix1 = IM_ROWADR(tbuf, 0, v);				\
	    tpix2 = IM_ROWADR(tbuf, 1, v);				\
	    tpix3 = IM_ROWADR(tbuf, 2, v);				\
	    tpix4 = IM_ROWADR(tbuf, 3, v);				\
	    /* Initialise kernel at 0th row */				\
	    dpix = IM_ROWADR(im, 0, v);					\
	    INIT_KSUMS_WITH_N_TIMES_VAL(tt, lidx, dpix);		\
	    for (int j=0; j<ridx; j++) {				\
		dpix = IM_ROWADR(im, j, v);				\
		ADD_TO_KSUMS(tt, dpix);					\
	    }								\
	    /* Process top edge while kernel is not completely in column. */ \
	    for (int j=0; j<lidx; j++) {				\
		dpix = IM_ROWADR(im, j+ridx, v);			\
		impix = IM_ROWADR(im, j, v);				\
		ADD_TO_KSUMS(tt, dpix);					\
		SET_TILE_PIXELS(tpix, impix);				\
		SET_PIXELS_FROM_KSUMS(tt, impix, divider, vround);	\
		SUB_FROM_KSUMS(tt, tpix, 0);				\
	    }								\
	    /* Process down columns while kernel completely in image. */ \
	    for (int j=lidx; j<im->size.y-ridx; j++) {			\
		dpix = IM_ROWADR(im, j+ridx, v);			\
		impix = IM_ROWADR(im, j, v);				\
		ADD_TO_KSUMS(tt, dpix);					\
		SET_TILE_PIXELS(tpix, impix);				\
		SET_PIXELS_FROM_KSUMS(tt, impix, divider, vround);	\
		SUB_FROM_KSUMS(tt, tpix, j-lidx);			\
	    }								\
	    /* Process bottom edge where kernel is off bottom of column. */ \
	    dpix = IM_ROWADR(im, im->size.y-1, v);			\
	    for (int j=im->size.y-ridx; j<im->size.y; j++) {		\
		impix = IM_ROWADR(im, j, v);				\
		ADD_TO_KSUMS(tt, dpix);					\
		SET_PIXELS_FROM_KSUMS(tt, impix, divider, vround);	\
		SUB_FROM_KSUMS(tt, tpix, j-lidx);			\
	    }								\
	}								\
	/* Finish off last few columns */				\
	for (int i=(rowlen & ~3); i<rowlen; i++) {			\
	    simd_##tt ksum;						\
	    simd_##tt *impix, *dpix, *tpix;				\
	    tpix = IM_ROWADR(tbuf, 0, v);				\
	    /* Initialise kernel at 0th row */				\
	    dpix = IM_ROWADR(im, 0, v);					\
	    ksum = simd_mul_##tt(simd_splat_##tt(lidx), dpix[i]);	\
	    for (int j=0; j<ridx; j++) {				\
		dpix = IM_ROWADR(im, j, v);				\
		ksum = simd_add_##tt(ksum, dpix[i]);			\
	    }								\
	    /* Process top edge while kernel is not completely in column. */ \
	    for (int j=0; j<lidx; j++) {				\
		dpix = IM_ROWADR(im, j+ridx, v);			\
		impix = IM_ROWADR(im, j, v);				\
		ksum = simd_add_##tt(ksum, dpix[i]);			\
		tpix[j] = impix[i];					\
		impix[i] = divider(ksum, vround);			\
		ksum = simd_sub_##tt(ksum, tpix[0]);			\
	    }								\
	    /* Process along column while kernel completely in tile column. */ \
	    for (int j=lidx; j<tile->size.y-ridx; j++) {		\
		dpix = IM_ROWADR(im, j+ridx, v);			\
		impix = IM_ROWADR(im, j, v);				\
		ksum = simd_add_##tt(ksum, dpix[i]);			\
		tpix[j] = impix[i];					\
		impix[i] = divider(ksum, vround);			\
		ksum = simd_sub_##tt(ksum, tpix[j-lidx]);		\
	    }								\
	    /* Process bottom edge where kernel is off bottom of column. */ \
	    dpix = IM_ROWADR(im, im->size.y-1, v);			\
	    for (int j=im->size.y-ridx; j<im->size.y; j++) {		\
		impix = IM_ROWADR(im, j, v);				\
		ksum = simd_add_##tt(ksum, dpix[i]);			\
		impix[i] = divider(ksum, vround);			\
		ksum = simd_sub_##tt(ksum, tpix[j-lidx]);		\
	    }								\
	}								\
    }


#define ULONG_DIV_INIT(tt, denom)			\
    struct simd_rcp_ULONG vrcp = simd_rcpc_ULONG(denom);\
    simd_ULONG vround = simd_splat_ULONG(len/2);
#define ULONG_DIV(numer, round)				\
    simd_idiv_ULONG(simd_add_ULONG(numer, round), vrcp)

#define LONG_DIV_INIT(tt, denom)			\
    simd_LONG rounder;					\
    const simd_LONG vzero = simd_splat_LONG(0);		\
    struct simd_rcp_LONG vrcp = simd_rcpc_LONG(denom);	\
    simd_LONG vround = simd_splat_LONG(len/2);
#define LONG_DIV(numer, round)	({					\
    rounder = simd_select_LONG(simd_cmplt_LONG(vzero, numer), vround, simd_sub_LONG(vzero, vround)); \
    simd_idiv_LONG(simd_add_LONG(numer, rounder), vrcp);})

#define FLOAT_DIV_INIT(tt, denom)			\
    simd_FLOAT vrcp = simd_splat_FLOAT(1.0/denom);
#define FLOAT_DIV(numer, round)				\
    simd_mul_FLOAT(numer, vrcp)

#define DOUBLE_DIV_INIT(tt, denom)			\
    simd_DOUBLE vrcp = simd_splat_DOUBLE(1.0/denom);
#define DOUBLE_DIV(numer, round)			\
    simd_mul_DOUBLE(numer, vrcp)

#ifdef SUPPORT_MEAN_LONG
generate_filter_vert_mean_inplace_n(ULONG, ULONG_DIV_INIT, ULONG_DIV)
generate_filter_vert_mean_inplace_n(LONG, LONG_DIV_INIT, LONG_DIV)
#endif
#ifdef SUPPORT_MEAN_FLOAT
generate_filter_vert_mean_inplace_n(FLOAT, FLOAT_DIV_INIT, FLOAT_DIV)
#endif
#ifdef SUPPORT_MEAN_DOUBLE
generate_filter_vert_mean_inplace_n(DOUBLE, DOUBLE_DIV_INIT, DOUBLE_DIV)
#endif

#undef INIT_KSUMS_WITH_N_TIMES_VAL
#undef ADD_TO_KSUMS
#undef SUB_FROM_KSUMS
#undef SET_PIXELS_FROM_KSUMS
#undef SET_TILE_PIXELS

/*
 * Simd arch header tends not to define this as it is exactly the same as
 * unsigned variant.
 */
#ifndef simd_cnvrt_SHORT_LONG
#define simd_cnvrt_SHORT_LONG(a,b) \
    ((simd_SHORT)simd_cnvrt_USHORT_ULONG((simd_ULONG)(a), (simd_ULONG)(b)))
#endif

#define INIT_KSUMS_WITH_N_TIMES_VAL(inttt, tt, xx, num, iptr) \
    simd_##inttt multiplier = simd_splat_##inttt(num);	      \
    cnv = simd_cnvrt_##inttt##_##tt(iptr[i]);		      \
    ksum1 = simd_mul_##inttt(multiplier, cnv.xx[0]);	      \
    ksum2 = simd_mul_##inttt(multiplier, cnv.xx[1]);	      \
    cnv = simd_cnvrt_##inttt##_##tt(iptr[i+1]);		      \
    ksum3 = simd_mul_##inttt(multiplier, cnv.xx[0]);	      \
    ksum4 = simd_mul_##inttt(multiplier, cnv.xx[1]);

#define ADD_TO_KSUMS(inttt, tt, xx, iptr)		      \
    cnv = simd_cnvrt_##inttt##_##tt(iptr[i]);		      \
    ksum1 = simd_add_##inttt(ksum1, cnv.xx[0]);		      \
    ksum2 = simd_add_##inttt(ksum2, cnv.xx[1]);		      \
    cnv = simd_cnvrt_##inttt##_##tt(iptr[i+1]);		      \
    ksum3 = simd_add_##inttt(ksum3, cnv.xx[0]);		      \
    ksum4 = simd_add_##inttt(ksum4, cnv.xx[1]);

#define SUB_FROM_KSUMS(inttt, tptr, idx)		\
    ksum1 = simd_sub_##inttt(ksum1, tptr##1[idx]);	\
    ksum2 = simd_sub_##inttt(ksum2, tptr##2[idx]);	\
    ksum3 = simd_sub_##inttt(ksum3, tptr##3[idx]);	\
    ksum4 = simd_sub_##inttt(ksum4, tptr##4[idx]);

#define SET_PIXELS_FROM_KSUMS(inttt, tt, iptr, divider, rounder)	   \
    iptr[i] = simd_cnvrt_##tt##_##inttt(divider(ksum1, rounder), divider(ksum2, rounder)); \
    iptr[i+1] = simd_cnvrt_##tt##_##inttt(divider(ksum3, rounder), divider(ksum4, rounder));

#define SET_TILE_PIXELS(inttt, tt, xx, tptr, iptr)    \
    cnv = simd_cnvrt_##inttt##_##tt(iptr[i]);	      \
    tptr##1[j] = cnv.xx[0];			      \
    tptr##2[j] = cnv.xx[1];			      \
    cnv = simd_cnvrt_##inttt##_##tt(iptr[i+1]);	      \
    tptr##3[j] = cnv.xx[0];			      \
    tptr##4[j] = cnv.xx[1];



#define generate_filter_vert_mean_inplace_nby2(tt, inttt, xx, divider_init, divider) \
    static void simd_filter_vert_mean_inplace_n_##tt(ip_image *im, ip_kernel *kern, ip_tile *tile)\
    {									\
	int len = kern->size.y;						\
	int ridx = len / 2;						\
	int lidx = (len-1) / 2;						\
	ip_image *tbuf = tile->tbuf;					\
	int rowlen = im->row_size / sizeof(simd_##tt);			\
	divider_init(inttt, len);					\
	/* Process along the rows four simd vectors at a time */	\
	for (int i=0; i<(rowlen & ~2); i+=2) {				\
	    simd_##inttt ksum1, ksum2, ksum3, ksum4;			\
	    simd_##tt *impix, *dpix;					\
	    simd_##inttt *tpix1, *tpix2, *tpix3, *tpix4;		\
	    simd_##inttt##_2 cnv;					\
	    tpix1 = IM_ROWADR(tbuf, 0, v);				\
	    tpix2 = IM_ROWADR(tbuf, 1, v);				\
	    tpix3 = IM_ROWADR(tbuf, 2, v);				\
	    tpix4 = IM_ROWADR(tbuf, 3, v);				\
	    /* Initialise kernel at 0th row */				\
	    dpix = IM_ROWADR(im, 0, v);					\
	    INIT_KSUMS_WITH_N_TIMES_VAL(inttt, tt, xx, lidx, dpix);	\
	    for (int j=0; j<ridx; j++) {				\
		dpix = IM_ROWADR(im, j, v);				\
		ADD_TO_KSUMS(inttt, tt, xx, dpix);			\
	    }								\
	    /* Process top edge while kernel is not completely in column. */ \
	    for (int j=0; j<lidx; j++) {				\
		dpix = IM_ROWADR(im, j+ridx, v);			\
		impix = IM_ROWADR(im, j, v);				\
		ADD_TO_KSUMS(inttt, tt, xx, dpix);			\
		SET_TILE_PIXELS(inttt, tt, xx, tpix, impix);	\
		SET_PIXELS_FROM_KSUMS(inttt, tt, impix, divider, vround);	\
		SUB_FROM_KSUMS(inttt, tpix, 0);				\
	    }								\
	    /* Process down columns while kernel completely in image. */ \
	    for (int j=lidx; j<im->size.y-ridx; j++) {			\
		dpix = IM_ROWADR(im, j+ridx, v);			\
		impix = IM_ROWADR(im, j, v);				\
		ADD_TO_KSUMS(inttt, tt, xx, dpix);			\
		SET_TILE_PIXELS(inttt, tt, xx, tpix, impix);	\
		SET_PIXELS_FROM_KSUMS(inttt, tt, impix, divider, vround);	\
		SUB_FROM_KSUMS(inttt, tpix, j-lidx);			\
	    }								\
	    /* Process boLONGom edge where kernel is off bottom of column. */ \
	    dpix = IM_ROWADR(im, im->size.y-1, v);			\
	    for (int j=im->size.y-ridx; j<im->size.y; j++) {		\
		impix = IM_ROWADR(im, j, v);				\
		ADD_TO_KSUMS(inttt, tt, xx, dpix);			\
		SET_PIXELS_FROM_KSUMS(inttt, tt, impix, divider, vround);	\
		SUB_FROM_KSUMS(inttt, tpix, j-lidx);			\
	    }								\
	}								\
	/* Finish off last few columns */				\
	for (int i=(rowlen & ~3); i<rowlen; i++) {			\
	    simd_##inttt ksum1, ksum2;					\
	    simd_##tt *impix, *dpix;					\
	    simd_##inttt *tpix1, *tpix2;				\
	    simd_##inttt##_2 cnv;					\
	    tpix1 = IM_ROWADR(tbuf, 0, v);				\
	    tpix2 = IM_ROWADR(tbuf, 1, v);				\
	    /* Initialise kernel at 0th row */				\
	    dpix = IM_ROWADR(im, 0, v);					\
	    cnv = simd_cnvrt_##inttt##_##tt(dpix[i]);			\
	    ksum1 = simd_mul_##inttt(simd_splat_##inttt(lidx), cnv.xx[0]); \
	    ksum2 = simd_mul_##inttt(simd_splat_##inttt(lidx), cnv.xx[1]); \
	    for (int j=0; j<ridx; j++) {				\
		dpix = IM_ROWADR(im, j, v);				\
		cnv = simd_cnvrt_##inttt##_##tt(dpix[i]);		\
		ksum1 = simd_add_##inttt(ksum1, cnv.xx[0]);		\
		ksum2 = simd_add_##inttt(ksum2, cnv.xx[1]);		\
	    }								\
	    /* Process top edge while kernel is not completely in column. */ \
	    for (int j=0; j<lidx; j++) {				\
		dpix = IM_ROWADR(im, j+ridx, v);			\
		impix = IM_ROWADR(im, j, v);				\
		cnv = simd_cnvrt_##inttt##_##tt(dpix[i]);		\
		ksum1 = simd_add_##inttt(ksum1, cnv.xx[0]);		\
		ksum2 = simd_add_##inttt(ksum2, cnv.xx[1]);		\
		cnv = simd_cnvrt_##inttt##_##tt(impix[i]);		\
		tpix1[j] = cnv.xx[0];					\
		tpix2[j] = cnv.xx[1];					\
		impix[i] = simd_cnvrt_##tt##_##inttt(divider(ksum1, vround), divider(ksum2, vround));	\
		ksum1 = simd_sub_##inttt(ksum1, tpix1[0]);		\
		ksum2 = simd_sub_##inttt(ksum2, tpix2[0]);		\
	    }								\
	    /* Process along column while kernel completely in tile column. */ \
	    for (int j=lidx; j<tile->size.y-ridx; j++) {		\
		dpix = IM_ROWADR(im, j+ridx, v);			\
		impix = IM_ROWADR(im, j, v);				\
		cnv = simd_cnvrt_##inttt##_##tt(dpix[i]);		\
		ksum1 = simd_add_##inttt(ksum1, cnv.xx[0]);		\
		ksum2 = simd_add_##inttt(ksum2, cnv.xx[1]);		\
		cnv = simd_cnvrt_##inttt##_##tt(impix[i]);		\
		tpix1[j] = cnv.xx[0];					\
		tpix2[j] = cnv.xx[1];					\
		impix[i] = simd_cnvrt_##tt##_##inttt(divider(ksum1, vround), divider(ksum2, vround));	\
		ksum1 = simd_sub_##inttt(ksum1, tpix1[j-lidx]);		\
		ksum2 = simd_sub_##inttt(ksum2, tpix2[j-lidx]);		\
	    }								\
	    /* Process bottom edge where kernel is off bottom of column. */ \
	    dpix = IM_ROWADR(im, im->size.y-1, v);			\
	    for (int j=im->size.y-ridx; j<im->size.y; j++) {		\
		impix = IM_ROWADR(im, j, v);				\
		cnv = simd_cnvrt_##inttt##_##tt(dpix[i]);		\
		ksum1 = simd_add_##inttt(ksum1, cnv.xx[0]);		\
		ksum2 = simd_add_##inttt(ksum2, cnv.xx[1]);		\
		impix[i] = simd_cnvrt_##tt##_##inttt(divider(ksum1, vround), divider(ksum2, vround));	\
		ksum1 = simd_sub_##inttt(ksum1, tpix1[j-lidx]);		\
		ksum2 = simd_sub_##inttt(ksum2, tpix2[j-lidx]);		\
	    }								\
	}								\
    }

#ifdef SUPPORT_MEAN_SHORT
generate_filter_vert_mean_inplace_nby2(USHORT, ULONG, ul, ULONG_DIV_INIT, ULONG_DIV)
generate_filter_vert_mean_inplace_nby2(SHORT, LONG, l, LONG_DIV_INIT, LONG_DIV)
#endif

/*
 * The vertical mean filter in-place filter.  Specially handle length 2
 * kernels for greater speed otherwise call the
 * simd_filter_vert_mean_inplace_n_TYPE() routines defined above.
 */
#define generate_filter_vert_mean_inplace(tt, op2)				\
    static void simd_filter_vert_mean_inplace_##tt(ip_image *im, ip_kernel *kern, ip_tile *tile) \
    {                                                                   \
	int rowlen = im->row_size / sizeof(simd_##tt);			\
     									\
	if (kern->size.y == 2) {					\
	    for (int j=0; j<im->size.y-1; j++) {			\
		simd_##tt *iptr = IM_ROWADR(im, j, v);			\
		simd_##tt *iup = IM_ROWADR(im, j+1, v);			\
	     								\
		for (int i=0; i<rowlen; i++)				\
		    iptr[i] = simd_##op2##_##tt(iup[i], iptr[i]);	\
	    }								\
	    simd_##tt *iptr = IM_ROWADR(im, im->size.y-1, v);		\
	    for (int i=0; i<rowlen; i++)				\
		iptr[i] = simd_##op2##_##tt(iptr[i], iptr[i]);		\
	}else{								\
	    simd_filter_vert_mean_inplace_n_##tt(im, kern, tile);	\
	}								\
	SIMD_EXIT();							\
    }


#ifdef SUPPORT_MEAN_SHORT
generate_filter_vert_mean_inplace(SHORT, mean);
generate_filter_vert_mean_inplace(USHORT, mean);
#else
#define simd_filter_vert_mean_inplace_SHORT NULL
#define simd_filter_vert_mean_inplace_USHORT NULL
#endif

#ifdef SUPPORT_MEAN_LONG
generate_filter_vert_mean_inplace(ULONG, meanp);
generate_filter_vert_mean_inplace(LONG, meanp);
#else
#define simd_filter_vert_mean_inplace_LONG NULL
#define simd_filter_vert_mean_inplace_ULONG NULL
#endif

#ifdef SUPPORT_MEAN_FLOAT
generate_filter_vert_mean_inplace(FLOAT, mean);
#else
#define simd_filter_vert_mean_inplace_FLOAT NULL
#endif

#ifdef SUPPORT_MEAN_DOUBLE
generate_filter_vert_mean_inplace(DOUBLE, mean);
#else
#define simd_filter_vert_mean_inplace_DOUBLE NULL
#endif

improc_function_Ikt ip_simd_filter_vert_mean_inplace_bytype[IM_TYPEMAX] = {
    [IM_SHORT] = simd_filter_vert_mean_inplace_SHORT,
    [IM_USHORT] = simd_filter_vert_mean_inplace_USHORT,
    [IM_LONG] = simd_filter_vert_mean_inplace_LONG,
    [IM_ULONG] = simd_filter_vert_mean_inplace_ULONG,
    [IM_FLOAT] = simd_filter_vert_mean_inplace_FLOAT,
    [IM_DOUBLE] = simd_filter_vert_mean_inplace_DOUBLE,
};



improc_function_Ikt ip_simd_filter_horiz_mean_inplace_bytype[IM_TYPEMAX];
improc_function_Ikt ip_simd_filter_horiz_mean_bytype[IM_TYPEMAX];
improc_function_Ikt ip_simd_filter_vert_mean_bytype[IM_TYPEMAX];

#else
/*
 * Dont't have sufficient SIMD support in arch header file so just compile
 * in stubs.
 */
improc_function_Ikt ip_simd_filter_horiz_difference_bytype[IM_TYPEMAX] = {};
improc_function_Ikt ip_simd_filter_horiz_centre_difference_bytype[IM_TYPEMAX] = {};
improc_function_Ikt ip_simd_filter_horiz_laplace_difference_bytype[IM_TYPEMAX] = {};
improc_function_Ikt ip_simd_filter_horiz_edge3x3_bytype[IM_TYPEMAX] = {};

improc_function_Ikt ip_simd_filter_vert_difference_bytype[IM_TYPEMAX] = {};
improc_function_Ikt ip_simd_filter_vert_centre_difference_bytype[IM_TYPEMAX] = {};
improc_function_Ikt ip_simd_filter_vert_laplace_difference_bytype[IM_TYPEMAX] = {};
improc_function_Ikt ip_simd_filter_vert_edge3x3_bytype[IM_TYPEMAX] = {};

improc_function_Ikt ip_simd_filter_horiz_mean_inplace_bytype[IM_TYPEMAX];
improc_function_Ikt ip_simd_filter_vert_mean_inplace_bytype[IM_TYPEMAX];
improc_function_Ikt ip_simd_filter_horiz_mean_bytype[IM_TYPEMAX];
improc_function_Ikt ip_simd_filter_vert_mean_bytype[IM_TYPEMAX];

#endif
