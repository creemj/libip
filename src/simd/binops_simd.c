/*
   Module: src/simd/binops_simd.c
   Author: M.J.Cree

   Binary pixelwise operators between two images (SIMD implementations).
   Entry points are in logical.c and maxmin.c.

   Arithmetic binary operators are in arith_simd.c.

   Copyright (C) 2016 Michael J. Cree

*/

#include <ip/ip.h>

#include "../internal.h"
#include "../function_ops.h"

#include "simd.h"
#include "binops_simd.h"


/*
 * Macro to process images with SIMD 'vector' operators.
 */

#define SIMD_PROCESS_IMAGE(d, s, type, op)			\
    {								\
	int length = d->row_size / sizeof(simd_ ## type);	\
	for (int j=0; j<d->size.y; j++) {			\
	    simd_ ## type *dptr = IM_ROWADR(d, j, v);		\
	    simd_ ## type *sptr = IM_ROWADR(s, j, v);		\
	    for (int i=0; i<length; i++) {			\
		PREFETCH_READ_WRITE(dptr+PREFETCH_OFS);		\
		PREFETCH_READ_ONCE(sptr+PREFETCH_OFS);		\
		*dptr = simd_ ## op ## _ ## type(*dptr, *sptr);	\
		dptr++; sptr++;					\
	    }							\
	}							\
    }

#define generate_image_same_function(type, operator)			\
    static void								\
    im_simd_ ## operator ## _ ## type(ip_image *d, ip_image *s)	\
    {									\
	SIMD_PROCESS_IMAGE(d, s, type, operator);			\
	SIMD_EXIT();							\
    }

/*
 * Logical operators on a pair of images.
 *
 * Since these operate on bits independently of pixel type we implement one
 * version only.
 */

generate_image_same_function(ULONG, or);
generate_image_same_function(ULONG, and);
generate_image_same_function(ULONG, xor);

void ip_simd_or(ip_image *d, ip_image *s)
{
    im_simd_or_ULONG(d, s);
}

void ip_simd_and(ip_image *d, ip_image *s)
{
    im_simd_and_ULONG(d, s);
}

void ip_simd_xor(ip_image *d, ip_image *s)
{
    im_simd_xor_ULONG(d, s);
}


/*
 * Max/min operators on a pair of images.
 */

#ifdef simd_max_UBYTE
generate_image_same_function(UBYTE, max)
#endif
#ifdef simd_max_BYTE
generate_image_same_function(BYTE, max)
#endif
#ifdef simd_max_USHORT
generate_image_same_function(USHORT, max)
#endif
#ifdef simd_max_SHORT
generate_image_same_function(SHORT, max)
#endif
#ifdef simd_max_ULONG
generate_image_same_function(ULONG, max)
#endif
#ifdef simd_max_LONG
generate_image_same_function(LONG, max)
#endif
#ifdef simd_max_FLOAT
generate_image_same_function(FLOAT, max)
#endif
#ifdef simd_max_DOUBLE
generate_image_same_function(DOUBLE, max)
#endif

#ifdef simd_min_UBYTE
generate_image_same_function(UBYTE, min)
#endif
#ifdef simd_min_BYTE
generate_image_same_function(BYTE, min)
#endif
#ifdef simd_min_USHORT
generate_image_same_function(USHORT, min)
#endif
#ifdef simd_min_SHORT
generate_image_same_function(SHORT, min)
#endif
#ifdef simd_min_ULONG
generate_image_same_function(ULONG, min)
#endif
#ifdef simd_min_LONG
generate_image_same_function(LONG, min)
#endif
#ifdef simd_min_FLOAT
generate_image_same_function(FLOAT, min)
#endif
#ifdef simd_min_DOUBLE
generate_image_same_function(DOUBLE, min)
#endif


improc_function_II ip_simd_max_bytype[IM_TYPEMAX] = {
    [IM_BINARY] = ip_simd_or,
#ifdef simd_max_UBYTE
    [IM_UBYTE] = im_simd_max_UBYTE,
#endif
#ifdef simd_max_BYTE
    [IM_BYTE] = im_simd_max_BYTE,
#endif
#ifdef simd_max_USHORT
    [IM_USHORT] = im_simd_max_USHORT,
#endif
#ifdef simd_max_SHORT
    [IM_SHORT] = im_simd_max_SHORT,
#endif
#ifdef simd_max_ULONG
    [IM_ULONG] = im_simd_max_ULONG,
#endif
#ifdef simd_max_LONG
    [IM_LONG] = im_simd_max_LONG,
#endif
#ifdef simd_max_FLOAT
    [IM_FLOAT] = im_simd_max_FLOAT,
#endif
#ifdef simd_max_DOUBLE
    [IM_DOUBLE] = im_simd_max_DOUBLE,
#endif
};

improc_function_II ip_simd_min_bytype[IM_TYPEMAX] = {
    [IM_BINARY] = ip_simd_and,
#ifdef simd_min_UBYTE
    [IM_UBYTE] = im_simd_min_UBYTE,
#endif
#ifdef simd_min_BYTE
    [IM_BYTE] = im_simd_min_BYTE,
#endif
#ifdef simd_min_USHORT
    [IM_USHORT] = im_simd_min_USHORT,
#endif
#ifdef simd_min_SHORT
    [IM_SHORT] = im_simd_min_SHORT,
#endif
#ifdef simd_min_ULONG
    [IM_ULONG] = im_simd_min_ULONG,
#endif
#ifdef simd_min_LONG
    [IM_LONG] = im_simd_min_LONG,
#endif
#ifdef simd_min_FLOAT
    [IM_FLOAT] = im_simd_min_FLOAT,
#endif
#ifdef simd_min_DOUBLE
    [IM_DOUBLE] = im_simd_min_DOUBLE,
#endif
};

