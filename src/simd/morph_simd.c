/*
   Module: ip/simd/morph1d_simd.c
   Author: M. J. Cree

   SIMD implementation of aspects of 1d line morphology.

   Copyright (C) 2015, 2017 Michael J. Cree
*/


#include <stdio.h>

#include <ip/ip.h>

#include "../internal.h"
#include "../function_ops.h"
#include "../morphint.h"

#include "simd.h"
#include "morph_simd.h"

/*
 * This module contains all SIMD implementations of basic morphology that
 * are required by morph1d.c and morphchord.c.
 */


/*
 * Some protection against compiling if don't have sufficient SIMD support
 */

#if !defined(simd_min_UBYTE) || !defined(simd_replicate_right_UBYTE) || !defined(simd_replicate_left_UBYTE) || !defined(simd_cvu_imm_UBYTE) || !defined(simd_ldu_UBYTE)

improc_morphfun_se2 ip_simd_m1d_horiz_se2_erode_bytype[IM_TYPEMAX];
improc_morphfun_se2 ip_simd_m1d_horiz_se2_dilate_bytype[IM_TYPEMAX];
improc_morphfun_horiz ip_simd_m1d_horiz_erode_bytype[IM_TYPEMAX];
improc_morphfun_horiz ip_simd_m1d_horiz_dilate_bytype[IM_TYPEMAX];
improc_morphfun_se2 ip_simd_m1d_vert_se2_erode_bytype[IM_TYPEMAX];
improc_morphfun_se2 ip_simd_m1d_vert_se2_dilate_bytype[IM_TYPEMAX];
improc_morphfun_vert ip_simd_m1d_vert_erode_bytype[IM_TYPEMAX];
improc_morphfun_vert ip_simd_m1d_vert_dilate_bytype[IM_TYPEMAX];
ct_function_piii ip_simd_update_row_erode_bytype[IM_TYPEMAX];
ct_function_piii ip_simd_update_row_dilate_bytype[IM_TYPEMAX];
ct_function_ITi ip_simd_process_imrow_erode_bytype[IM_TYPEMAX];
ct_function_ITi ip_simd_process_imrow_dilate_bytype[IM_TYPEMAX];

#else

/*
 * We presume that if we have support for UBYTE we at least have full
 * support for all of UBYTE, BYTE, USHORT and SHORT.
 */


/* While we use scalar operators to finish off row. */
#define scalar_min MIN
#define scalar_max MAX

/*
 * SIMD support for basic morphology with a structuring element of a
 * horizontal line of length two pixels.
 */

#define generate_m1d_horiz_se2_function(type, opname, op)		\
    static void simd_m1d_horiz_se2_ ## opname ## _ ## type(ip_image *im, int se_origin) \
    {									\
	int rowlen = im->row_size / sizeof(simd_ ## type);		\
	for (int j=0; j<im->size.y; j++) {				\
	    simd_ ## type *iptr = IM_ROWADR(im, j, v);			\
									\
	    if (se_origin == 0) {					\
		simd_ ## type this, next, offby1;			\
		int pos;						\
		this = iptr[0];						\
		for (int i=0; i<rowlen-1; i++) {			\
		    next = iptr[i+1];					\
		    offby1 = simd_cvu_imm_ ## type(this, next, 1);	\
		    iptr[i] = op(this, offby1);				\
		    this = next;					\
		}							\
		pos = im->size.x - SIMD_NUM_ELEMENTS(type)*(rowlen-1) - 1; \
		if (pos >= 0) {						\
		    this = simd_replicate_right_ ## type(this, pos);	\
		    next = simd_replicate_left_ ## type(this, pos);	\
		    offby1 = simd_cvu_imm_ ## type(this, next, 1);	\
		    iptr[rowlen-1] = op(this, offby1);			\
		}							\
	    }else{							\
		simd_ ## type this, prev;				\
		prev = simd_splat_v0_ ## type(iptr[0]);			\
		for (int i=0; i<rowlen; i++) {				\
		    simd_ ## type offby1;				\
		    this = iptr[i];					\
		    offby1 = simd_cvu_imm_ ## type(prev, this, SIMD_NUM_ELEMENTS(type)-1); \
		    iptr[i] = op(this, offby1);				\
		    prev = this;					\
		}							\
	    }								\
	}								\
	SIMD_EXIT();							\
    }

generate_m1d_horiz_se2_function(BYTE, erode, simd_min_BYTE)
generate_m1d_horiz_se2_function(BYTE, dilate, simd_max_BYTE)
generate_m1d_horiz_se2_function(UBYTE, erode, simd_min_UBYTE)
generate_m1d_horiz_se2_function(UBYTE, dilate, simd_max_UBYTE)
generate_m1d_horiz_se2_function(USHORT, erode, simd_min_USHORT)
generate_m1d_horiz_se2_function(USHORT, dilate, simd_max_USHORT)
generate_m1d_horiz_se2_function(SHORT, erode, simd_min_SHORT)
generate_m1d_horiz_se2_function(SHORT, dilate, simd_max_SHORT)

#ifdef simd_min_ULONG
generate_m1d_horiz_se2_function(ULONG, erode, simd_min_ULONG)
generate_m1d_horiz_se2_function(ULONG, dilate, simd_max_ULONG)
#endif
#ifdef simd_min_LONG
generate_m1d_horiz_se2_function(LONG, erode, simd_min_LONG)
generate_m1d_horiz_se2_function(LONG, dilate, simd_max_LONG)
#endif
#ifdef simd_min_FLOAT
generate_m1d_horiz_se2_function(FLOAT, erode, simd_min_FLOAT)
generate_m1d_horiz_se2_function(FLOAT, dilate, simd_max_FLOAT)
#endif
#ifdef simd_min_DOUBLE
generate_m1d_horiz_se2_function(DOUBLE, erode, simd_min_DOUBLE)
generate_m1d_horiz_se2_function(DOUBLE, dilate, simd_max_DOUBLE)
#endif


improc_morphfun_se2 ip_simd_m1d_horiz_se2_erode_bytype[IM_TYPEMAX] = {
    [IM_BINARY] = simd_m1d_horiz_se2_erode_UBYTE,
    [IM_UBYTE] = simd_m1d_horiz_se2_erode_UBYTE,
    [IM_BYTE] = simd_m1d_horiz_se2_erode_BYTE,
    [IM_USHORT] = simd_m1d_horiz_se2_erode_USHORT,
    [IM_SHORT] = simd_m1d_horiz_se2_erode_SHORT,
#ifdef simd_min_ULONG
    [IM_ULONG] = simd_m1d_horiz_se2_erode_ULONG,
#endif
#ifdef simd_min_LONG
    [IM_LONG] = simd_m1d_horiz_se2_erode_LONG,
#endif
#ifdef simd_min_FLOAT
    [IM_FLOAT] = simd_m1d_horiz_se2_erode_FLOAT,
#endif
#ifdef simd_min_DOUBLE
    [IM_DOUBLE] = simd_m1d_horiz_se2_erode_DOUBLE
#endif
};

improc_morphfun_se2 ip_simd_m1d_horiz_se2_dilate_bytype[IM_TYPEMAX] = {
    [IM_BINARY] = simd_m1d_horiz_se2_dilate_UBYTE,
    [IM_UBYTE] = simd_m1d_horiz_se2_dilate_UBYTE,
    [IM_BYTE] = simd_m1d_horiz_se2_dilate_BYTE,
    [IM_USHORT] = simd_m1d_horiz_se2_dilate_USHORT,
    [IM_SHORT] = simd_m1d_horiz_se2_dilate_SHORT,
#ifdef simd_min_ULONG
    [IM_ULONG] = simd_m1d_horiz_se2_dilate_ULONG,
#endif
#ifdef simd_min_LONG
    [IM_LONG] = simd_m1d_horiz_se2_dilate_LONG,
#endif
#ifdef simd_min_FLOAT
    [IM_FLOAT] = simd_m1d_horiz_se2_dilate_FLOAT,
#endif
#ifdef simd_min_DOUBLE
    [IM_DOUBLE] = simd_m1d_horiz_se2_dilate_DOUBLE
#endif
};



/*
 * SIMD support for basic morphology with a structuring element of a
 * horizontal line of arbitrary length.
 *
 * It would be nice to implement the O(1) method of Soile et al., 1996,
 * however that requires a min/max SIMD operator that operates consecutively
 * along elements in the vector and that is not available on any extant
 * hardware.  One could implement a transpose, use the standard SIMD min/max
 * vector operators, and finish off with a transpose, but that would require
 * far too many SIMD CPU registers for UBYTE or USHORT thus risks CPU
 * register stack overflow.
 *
 * We instead implement the less efficient O(log(k)) recursive method (for a
 * structuring element of length k) since that lends itself much more
 * naturally to an SIMD implementation. It is convenient to use the line
 * buffer (rbuf) to simplify the implementation for arbitrary structuring
 * element origin. The first iteration writes into rbuf and extends past
 * image row ends.  Each subsequent iteration works on rbuf and the last
 * iteration writes back to the image.  The origin is applied on the last
 * iteration only.
 */

#define generate_m1d_horiz_function(tt, opname, op) \
    static void simd_m1d_horiz_ ## opname ## _ ## tt(ip_image *im, int se_len, \
						     int se_origin, ip_image *rbuf) \
    {									\
	for (int j=0; j<im->size.y; j++) {				\
	    simd_ ## tt this, next;					\
	    simd_ ## tt *imrow;						\
	    simd_ ## tt *buf;						\
	    int disp;							\
	    int rowlen;							\
	    int i;							\
	    imrow = IM_ROWADR(im, j, v);				\
	    buf  = IM_ROWADR(rbuf, 0, v);				\
	    /* Calculate region left of image */			\
	    int leftskip = PADDED_ROW_SIZE(se_len*sizeof(tt ## _type))/sizeof(simd_ ## tt); \
	    next = imrow[0];						\
	    this = simd_splat_v0_ ## tt(next);				\
	    for (int i=0; i<leftskip-1; i++)				\
		buf[i] = this;						\
	    buf[leftskip-1] = op(this, simd_cvu_imm_ ## tt(this, next, 1)); \
	    buf += leftskip;						\
	    /* First iteration from image to rbuf with displacement of 1 pixel */ \
	    rowlen = (im->row_size / sizeof(simd_ ## tt));		\
	    for (i=0; i<rowlen-1; i++) {				\
		this = next;						\
		next = imrow[i+1];					\
		buf[i] = op(this, simd_cvu_imm_ ## tt(this, next, 1));	\
	    }								\
	    /* Calculate region right of image */			\
	    disp = (SIMD_NUM_ELEMENTS(tt) - (rowlen*SIMD_NUM_ELEMENTS(tt)-im->size.x)-1) \
		& (SIMD_NUM_ELEMENTS(tt)-1);				\
	    this = simd_replicate_right_ ## tt(next, disp);		\
	    next = simd_splat_vN_ ## tt(next, disp);			\
	    buf[rowlen-1] = op(this, simd_cvu_imm_ ## tt(this, next, 1)); \
	    for (i=rowlen; i<rowlen+leftskip; i++)			\
		buf[i] = next;						\
	    /* Second, etc., iteration works on rbuf */			\
	    buf -= leftskip;						\
	    rowlen = im->row_size / sizeof(simd_ ## tt);		\
	    for (disp=2;  2*disp < se_len; disp*=2) {			\
		for (i=0; i<rowlen+leftskip; i++) {			\
		    unsigned char *buf_offset = (unsigned char *)(&buf[i]); \
		    buf_offset += disp*sizeof(tt ## _type);		\
		    buf[i] = op(buf[i], simd_ldu_ ## tt(buf_offset));	\
		}							\
	    }								\
	    /* last iteration; copy back to image row and apply shift due to origin */ \
	    if (2*disp > se_len) {					\
		disp = se_len - disp;					\
	    }								\
	    imrow = IM_ROWADR(im, j, v);				\
	    rowlen = im->row_size / sizeof(simd_ ## tt);		\
	    for (int i=0; i<rowlen; i++) {				\
		tt ## _type *left = (tt ## _type *)(&buf[leftskip+i]);	\
		left -= se_origin;					\
		tt ## _type *right = left + disp;			\
		imrow[i] = op(simd_ldu_ ## tt(left),  simd_ldu_ ## tt(right)); \
	    }								\
	}								\
	SIMD_EXIT();							\
    }


generate_m1d_horiz_function(BYTE, erode, simd_min_BYTE)
generate_m1d_horiz_function(BYTE, dilate, simd_max_BYTE)
generate_m1d_horiz_function(UBYTE, erode, simd_min_UBYTE)
generate_m1d_horiz_function(UBYTE, dilate, simd_max_UBYTE)
generate_m1d_horiz_function(SHORT, erode, simd_min_SHORT)
generate_m1d_horiz_function(SHORT, dilate, simd_max_SHORT)
generate_m1d_horiz_function(USHORT, erode, simd_min_USHORT)
generate_m1d_horiz_function(USHORT, dilate, simd_max_USHORT)
#ifdef simd_min_ULONG
generate_m1d_horiz_function(ULONG, erode, simd_min_ULONG)
generate_m1d_horiz_function(ULONG, dilate, simd_max_ULONG)
#endif
#ifdef simd_min_LONG
generate_m1d_horiz_function(LONG, erode, simd_min_LONG)
generate_m1d_horiz_function(LONG, dilate, simd_max_LONG)
#endif
#ifdef simd_min_FLOAT
generate_m1d_horiz_function(FLOAT, erode, simd_min_FLOAT)
generate_m1d_horiz_function(FLOAT, dilate, simd_max_FLOAT)
#endif
#ifdef simd_min_DOUBLE
generate_m1d_horiz_function(DOUBLE, erode, simd_min_DOUBLE)
generate_m1d_horiz_function(DOUBLE, dilate, simd_max_DOUBLE)
#endif



improc_morphfun_horiz ip_simd_m1d_horiz_erode_bytype[IM_TYPEMAX] = {
    [IM_BINARY] = simd_m1d_horiz_erode_UBYTE,
    [IM_UBYTE] = simd_m1d_horiz_erode_UBYTE,
    [IM_BYTE] = simd_m1d_horiz_erode_BYTE,
    [IM_USHORT] = simd_m1d_horiz_erode_USHORT,
    [IM_SHORT] = simd_m1d_horiz_erode_SHORT,
#ifdef simd_min_ULONG
    [IM_ULONG] = simd_m1d_horiz_erode_ULONG,
#endif
#ifdef simd_min_LONG
    [IM_LONG] = simd_m1d_horiz_erode_LONG,
#endif
#ifdef simd_min_FLOAT
    [IM_FLOAT] = simd_m1d_horiz_erode_FLOAT,
#endif
#ifdef simd_min_DOUBLE
    [IM_DOUBLE] = simd_m1d_horiz_erode_DOUBLE
#endif
};

improc_morphfun_horiz ip_simd_m1d_horiz_dilate_bytype[IM_TYPEMAX] = {
    [IM_BINARY] = simd_m1d_horiz_dilate_UBYTE,
    [IM_UBYTE] = simd_m1d_horiz_dilate_UBYTE,
    [IM_BYTE] = simd_m1d_horiz_dilate_BYTE,
    [IM_USHORT] = simd_m1d_horiz_dilate_USHORT,
    [IM_SHORT] = simd_m1d_horiz_dilate_SHORT,
#ifdef simd_min_ULONG
    [IM_ULONG] = simd_m1d_horiz_dilate_ULONG,
#endif
#ifdef simd_min_LONG
    [IM_LONG] = simd_m1d_horiz_dilate_LONG,
#endif
#ifdef simd_min_FLOAT
    [IM_FLOAT] = simd_m1d_horiz_dilate_FLOAT,
#endif
#ifdef simd_min_DOUBLE
    [IM_DOUBLE] = simd_m1d_horiz_dilate_DOUBLE
#endif
};



/* SIMD implementation of length 2 vertical line structuring element. */

#define generate_m1d_vert_se2_function(type, opname, op)		\
    static void simd_m1d_vert_se2_ ## opname ## _ ## type(ip_image *im, int se_origin) \
    {									\
	int rowlen = im->row_size / sizeof(simd_ ## type);		\
									\
	if (se_origin == 0) {						\
	    for (int j=0; j<im->size.y-1; j++) {			\
		simd_ ## type *iptr = IM_ROWADR(im, j, v);		\
		simd_ ## type *iup = IM_ROWADR(im, j+1, v);		\
									\
		for (int i=0; i<rowlen; i++)				\
		    iptr[i] = op(iptr[i], iup[i]);			\
	    }								\
	}else{								\
	    for (int j=im->size.y-1; j>0; j--) {			\
		simd_ ## type *iptr = IM_ROWADR(im, j, v);		\
		simd_ ## type *idown = IM_ROWADR(im, j-1, v);		\
									\
		for (int i=0; i<rowlen; i++)				\
		    iptr[i] = op(iptr[i], idown[i]);			\
	    }								\
	}								\
	SIMD_EXIT();							\
    }

generate_m1d_vert_se2_function(BYTE, erode, simd_min_BYTE)
generate_m1d_vert_se2_function(BYTE, dilate, simd_max_BYTE)
generate_m1d_vert_se2_function(UBYTE, erode, simd_min_UBYTE)
generate_m1d_vert_se2_function(UBYTE, dilate, simd_max_UBYTE)
generate_m1d_vert_se2_function(USHORT, erode, simd_min_USHORT)
generate_m1d_vert_se2_function(USHORT, dilate, simd_max_USHORT)
generate_m1d_vert_se2_function(SHORT, erode, simd_min_SHORT)
generate_m1d_vert_se2_function(SHORT, dilate, simd_max_SHORT)
#ifdef simd_min_ULONG
generate_m1d_vert_se2_function(ULONG, erode, simd_min_ULONG)
generate_m1d_vert_se2_function(ULONG, dilate, simd_max_ULONG)
#endif
#ifdef simd_min_LONG
generate_m1d_vert_se2_function(LONG, erode, simd_min_LONG)
generate_m1d_vert_se2_function(LONG, dilate, simd_max_LONG)
#endif
#ifdef simd_min_FLOAT
generate_m1d_vert_se2_function(FLOAT, erode, simd_min_FLOAT)
generate_m1d_vert_se2_function(FLOAT, dilate, simd_max_FLOAT)
#endif
#ifdef simd_min_DOUBLE
generate_m1d_vert_se2_function(DOUBLE, erode, simd_min_DOUBLE)
generate_m1d_vert_se2_function(DOUBLE, dilate, simd_max_DOUBLE)
#endif


improc_morphfun_se2 ip_simd_m1d_vert_se2_erode_bytype[IM_TYPEMAX] = {
    [IM_BINARY] = simd_m1d_vert_se2_erode_UBYTE,
    [IM_UBYTE] = simd_m1d_vert_se2_erode_UBYTE,
    [IM_BYTE] = simd_m1d_vert_se2_erode_BYTE,
    [IM_USHORT] = simd_m1d_vert_se2_erode_USHORT,
    [IM_SHORT] = simd_m1d_vert_se2_erode_SHORT,
#ifdef simd_min_ULONG
    [IM_ULONG] = simd_m1d_vert_se2_erode_ULONG,
#endif
#ifdef simd_min_LONG
    [IM_LONG] = simd_m1d_vert_se2_erode_LONG,
#endif
#ifdef simd_min_FLOAT
    [IM_FLOAT] = simd_m1d_vert_se2_erode_FLOAT,
#endif
#ifdef simd_min_DOUBLE
    [IM_DOUBLE] = simd_m1d_vert_se2_erode_DOUBLE
#endif
};

improc_morphfun_se2 ip_simd_m1d_vert_se2_dilate_bytype[IM_TYPEMAX] = {
    [IM_BINARY] = simd_m1d_vert_se2_dilate_UBYTE,
    [IM_UBYTE] = simd_m1d_vert_se2_dilate_UBYTE,
    [IM_BYTE] = simd_m1d_vert_se2_dilate_BYTE,
    [IM_USHORT] = simd_m1d_vert_se2_dilate_USHORT,
    [IM_SHORT] = simd_m1d_vert_se2_dilate_SHORT,
#ifdef simd_min_ULONG
    [IM_ULONG] = simd_m1d_vert_se2_dilate_ULONG,
#endif
#ifdef simd_min_LONG
    [IM_LONG] = simd_m1d_vert_se2_dilate_LONG,
#endif
#ifdef simd_min_FLOAT
    [IM_FLOAT] = simd_m1d_vert_se2_dilate_FLOAT,
#endif
#ifdef simd_min_DOUBLE
    [IM_DOUBLE] = simd_m1d_vert_se2_dilate_DOUBLE
#endif
};



/*
 * Vertical line structuring element of greater than two pixels length.
 *
 * We use two column buffers of width CACHE_LINE_SIZE and process image
 * in blocks of CACHE_LINE_SIZE width and im-size.y rows.
 */

#define NUMBER_OF_ELEMENTS(x, d) (((x) + (d) - 1) / (d))

#define generate_m1d_vert_function(type, opname, op)			\
    static void simd_m1d_vert_ ## opname ## _ ## type(ip_image *im, int se_len, int se_origin, \
						 ip_image *gbuf, ip_image *hbuf)	\
    {									\
	int hofs, gofs, numsteps;					\
									\
	hofs = se_len - se_origin;					\
	gofs = (se_len-1) - se_origin;					\
									\
	/* Process the image in the x direction in steps of CACHE_LINE_SIZE */ \
	numsteps = NUMBER_OF_ELEMENTS(im->row_size, CACHE_LINE_SIZE);	\
	for (int k=0; k<numsteps; k++) {				\
	    int xofs, xsteps;						\
	    int cnt = se_len;						\
	    const int els_per_cline = CACHE_LINE_SIZE / sizeof(simd_ ## type); \
									\
	    xofs = k * els_per_cline;					\
	    if (k == numsteps-1)					\
		xsteps = NUMBER_OF_ELEMENTS(im->row_size-k*CACHE_LINE_SIZE, sizeof(simd_ ## type)); \
	    else							\
		xsteps = els_per_cline;					\
									\
	    /* Set up right ordered array of minima */			\
	    for (int j=0; j<im->size.y; j++) {				\
		simd_ ## type *iptr = IM_ROWADR(im, j, v);		\
		simd_ ## type *gptr = IM_ROWADR(gbuf, j, v);		\
									\
		iptr += xofs;						\
		if (cnt == se_len) {					\
		    cnt = 0;						\
		    for (int i=0; i<xsteps; i++)			\
			gptr[i] = iptr[i];				\
		}else{							\
		    simd_ ## type *gup = IM_ROWADR(gbuf, j-1, v);	\
		    for (int i=0; i<xsteps; i++)			\
			gptr[i] = op(gup[i], iptr[i]);			\
		}							\
		cnt++;							\
	    }								\
									\
	    /* Pad past end of line with last value */			\
	    for (int j=im->size.y; j<im->size.y+se_len; j++) {		\
		simd_ ## type *gptr = IM_ROWADR(gbuf, j, v);		\
									\
		if (cnt == se_len) {					\
		    simd_ ## type *iptr = IM_ROWADR(im, im->size.y-1, v); \
		    iptr += xofs;					\
		    cnt = 0;						\
		    for (int i=0; i<xsteps; i++)			\
			gptr[i] = iptr[i];				\
		}else{							\
		    simd_ ## type *gup = IM_ROWADR(gbuf, j-1, v);	\
		    for (int i=0; i<xsteps; i++)			\
			gptr[i] = gup[i];				\
		}							\
		cnt++;							\
	    }								\
									\
	    /* Set up left ordered array of minima */			\
	    {								\
		simd_ ## type *iptr = IM_ROWADR(im, im->size.y-1, v);	\
		simd_ ## type *hptr = IM_ROWADR(hbuf, im->size.y-1+se_len, v); \
		iptr += xofs;						\
		for (int i=0; i<xsteps; i++)				\
		    hptr[i] = iptr[i];					\
	    }								\
									\
	    cnt = (im->size.y-2) % se_len;				\
	    for (int j=im->size.y-2; j>=0; j--) {			\
		simd_ ## type *iptr = IM_ROWADR(im, j, v);		\
		simd_ ## type *hptr = IM_ROWADR(hbuf, j+se_len, v);	\
									\
		iptr += xofs;						\
		if (cnt == 0) {						\
		    for (int i=0; i<xsteps; i++)			\
			hptr[i] = iptr[i];				\
		    cnt = se_len;					\
		}else{							\
		    simd_ ## type *hdown = IM_ROWADR(hbuf, j+se_len+1, v); \
		    for (int i=0; i<xsteps; i++)			\
			hptr[i] = op(hdown[i], iptr[i]);		\
		}							\
		cnt--;							\
	    }								\
									\
	    /* Pad before start of line with first value */		\
	    for (int j=0; j<se_len; j++) {				\
		simd_ ## type *iptr = IM_ROWADR(im, 0, v);		\
		simd_ ## type *hptr = IM_ROWADR(hbuf, j, v);		\
									\
		iptr += xofs;						\
		for (int i=0; i<xsteps; i++)				\
		    hptr[i] = iptr[i];					\
	    }								\
									\
	    /* Perform the erosion by taking minimum of two values in g and h. */ \
	    for (int j=0; j<im->size.y; j++) {				\
		simd_ ## type *iptr = IM_ROWADR(im, j, v);		\
		simd_ ## type *gptr = IM_ROWADR(gbuf, j+gofs, v);	\
		simd_ ## type *hptr = IM_ROWADR(hbuf, j+hofs, v);	\
									\
		iptr += xofs;						\
		for (int i=0; i<xsteps; i++)				\
		    iptr[i] = op(gptr[i], hptr[i]);			\
	    }								\
	}								\
	SIMD_EXIT();							\
    }



generate_m1d_vert_function(UBYTE, erode, simd_min_UBYTE)
generate_m1d_vert_function(UBYTE, dilate, simd_max_UBYTE)
generate_m1d_vert_function(BYTE, erode, simd_min_BYTE)
generate_m1d_vert_function(BYTE, dilate, simd_max_BYTE)
generate_m1d_vert_function(USHORT, erode, simd_min_USHORT)
generate_m1d_vert_function(USHORT, dilate, simd_max_USHORT)
generate_m1d_vert_function(SHORT, erode, simd_min_SHORT)
generate_m1d_vert_function(SHORT, dilate, simd_max_SHORT)
#ifdef simd_min_ULONG
generate_m1d_vert_function(ULONG, erode, simd_min_ULONG)
generate_m1d_vert_function(ULONG, dilate, simd_max_ULONG)
#endif
#ifdef simd_min_LONG
generate_m1d_vert_function(LONG, erode, simd_min_LONG)
generate_m1d_vert_function(LONG, dilate, simd_max_LONG)
#endif
#ifdef simd_min_FLOAT
generate_m1d_vert_function(FLOAT, erode, simd_min_FLOAT)
generate_m1d_vert_function(FLOAT, dilate, simd_max_FLOAT)
#endif
#ifdef simd_min_DOUBLE
generate_m1d_vert_function(DOUBLE, erode, simd_min_DOUBLE)
generate_m1d_vert_function(DOUBLE, dilate, simd_max_DOUBLE)
#endif



improc_morphfun_vert ip_simd_m1d_vert_erode_bytype[IM_TYPEMAX] = {
    [IM_BINARY] = simd_m1d_vert_erode_UBYTE,
    [IM_UBYTE] = simd_m1d_vert_erode_UBYTE,
    [IM_BYTE] = simd_m1d_vert_erode_BYTE,
    [IM_USHORT] = simd_m1d_vert_erode_USHORT,
    [IM_SHORT] = simd_m1d_vert_erode_SHORT,
#ifdef simd_min_ULONG
    [IM_ULONG] = simd_m1d_vert_erode_ULONG,
#endif
#ifdef simd_min_LONG
    [IM_LONG] = simd_m1d_vert_erode_LONG,
#endif
#ifdef simd_min_FLOAT
    [IM_FLOAT] = simd_m1d_vert_erode_FLOAT,
#endif
#ifdef simd_min_DOUBLE
    [IM_DOUBLE] = simd_m1d_vert_erode_DOUBLE
#endif
};

improc_morphfun_vert ip_simd_m1d_vert_dilate_bytype[IM_TYPEMAX] = {
    [IM_BINARY] = simd_m1d_vert_dilate_UBYTE,
    [IM_UBYTE] = simd_m1d_vert_dilate_UBYTE,
    [IM_BYTE] = simd_m1d_vert_dilate_BYTE,
    [IM_USHORT] = simd_m1d_vert_dilate_USHORT,
    [IM_SHORT] = simd_m1d_vert_dilate_SHORT,
#ifdef simd_min_ULONG
    [IM_ULONG] = simd_m1d_vert_dilate_ULONG,
#endif
#ifdef simd_min_LONG
    [IM_LONG] = simd_m1d_vert_dilate_LONG,
#endif
#ifdef simd_min_FLOAT
    [IM_FLOAT] = simd_m1d_vert_dilate_FLOAT,
#endif
#ifdef simd_min_DOUBLE
    [IM_DOUBLE] = simd_m1d_vert_dilate_DOUBLE
#endif
};



/*
 * Processing of the rows of the chord table implemented in morphchord.c.
 *
 * We prefer the simd_cvu_TYPE macros for constructing an unaligned vector
 * from two adjacent aligned vectors, particularly for the shorter chords,
 * however if that is not implemented we then try the simd_ldu_TYPE macros
 * for direct loading of a vector from an unaligned memory address.
 */

#if defined(simd_cvu_UBYTE) || defined(simd_ldu_UBYTE)
#ifdef simd_cvu_UBYTE

/* TBD */

#else


#define generate_update_row_function(type, opname, op)	\
static void simd_update_row_ ## opname ## _ ## type(void *ctptr, int trow, int clen, int disp)\
{                                                                       \
    ip_chordtbl *ct = (ip_chordtbl *)ctptr;                             \
    int vnum = (ct->length - disp)/ sizeof(simd_ ## type);              \
    int dofs = disp / sizeof(simd_ ## type);                            \
    int drem = disp - dofs*sizeof(simd_ ## type);                       \
    int x;								\
    simd_ ## type *up = chordtbl_cl_rowadr(ct, trow, clen-1);           \
    simd_ ## type *this = chordtbl_cl_rowadr(ct, trow, clen);           \
    for (x = 0; x < vnum-1; x++) {                                      \
	unsigned char *sud_addr = (unsigned char *)(&(up[x+dofs]));	\
	sud_addr += drem * sizeof(type ## _type);			\
        this[x] = simd_ ## op ## _ ## type(up[x], simd_ldu_ ## type(sud_addr)); \
    }                                                                   \
    /* TBD: Finish off with scalar code; ultimately should use simd replicate macros */ \
    type ## _type *s_up = (type ## _type *)up;				\
    type ## _type *s_this = (type ## _type *)this;			\
    x *= SIMD_NUM_ELEMENTS(type);					\
    for ( ; x < ct->length-disp; x++) {					\
	s_this[x] = scalar_ ## op(s_up[x], s_up[x+disp]);		\
    }									\
    for ( ; x < ct->length; x++) {                                      \
        s_this[x] = s_this[ct->length-disp-1];				\
    }                                                                   \
    SIMD_EXIT();							\
}

#endif

/* Generate all the row update functions */

generate_update_row_function(UBYTE, erode, min)
generate_update_row_function(UBYTE, dilate, max)

generate_update_row_function(BYTE, erode, min)
generate_update_row_function(BYTE, dilate, max)

generate_update_row_function(USHORT, erode, min)
generate_update_row_function(USHORT, dilate, max)

generate_update_row_function(SHORT, erode, min)
generate_update_row_function(SHORT, dilate, max)

#ifdef simd_min_ULONG
generate_update_row_function(ULONG, erode, min)
generate_update_row_function(ULONG, dilate, max)
#else
#define simd_update_row_erode_ULONG NULL
#define simd_update_row_dilate_ULONG NULL
#endif

#ifdef simd_min_LONG
generate_update_row_function(LONG, erode, min)
generate_update_row_function(LONG, dilate, max)
#else
#define simd_update_row_erode_LONG NULL
#define simd_update_row_dilate_LONG NULL
#endif

#ifdef simd_min_FLOAT
generate_update_row_function(FLOAT, erode, min)
generate_update_row_function(FLOAT, dilate, max)
#else
#define simd_update_row_erode_FLOAT NULL
#define simd_update_row_dilate_FLOAT NULL
#endif

#ifdef simd_min_DOUBLE
generate_update_row_function(DOUBLE, erode, min)
generate_update_row_function(DOUBLE, dilate, max)
#else
#define simd_update_row_erode_DOUBLE NULL
#define simd_update_row_dilate_DOUBLE NULL
#endif


#else

/*
 * Without simd_cvu_* or simd_ldu_* there is no support for these
 * operators.
 */

#define simd_update_row_dilate_UBYTE NULL
#define simd_update_row_dilate_BYTE NULL
#define simd_update_row_dilate_USHORT NULL
#define simd_update_row_dilate_SHORT NULL
#define simd_update_row_dilate_ULONG NULL
#define simd_update_row_dilate_LONG NULL
#define simd_update_row_dilate_FLOAT NULL
#define simd_update_row_dilate_DOUBLE NULL
#define simd_update_row_erode_UBYTE NULL
#define simd_update_row_erode_BYTE NULL
#define simd_update_row_erode_USHORT NULL
#define simd_update_row_erode_SHORT NULL
#define simd_update_row_erode_ULONG NULL
#define simd_update_row_erode_LONG NULL
#define simd_update_row_erode_FLOAT NULL
#define simd_update_row_erode_DOUBLE NULL

#endif


ct_function_piii ip_simd_update_row_dilate_bytype[IM_TYPEMAX] = {
    [IM_BINARY] = simd_update_row_dilate_UBYTE,
    [IM_UBYTE] = simd_update_row_dilate_UBYTE,
    [IM_BYTE] = simd_update_row_dilate_BYTE,
    [IM_USHORT] = simd_update_row_dilate_USHORT,
    [IM_SHORT] = simd_update_row_dilate_SHORT,
    [IM_ULONG] = simd_update_row_dilate_ULONG,
    [IM_LONG] = simd_update_row_dilate_LONG,
    [IM_FLOAT] = simd_update_row_dilate_FLOAT,
    [IM_DOUBLE] = simd_update_row_dilate_DOUBLE
};

ct_function_piii ip_simd_update_row_erode_bytype[IM_TYPEMAX] = {
    [IM_BINARY] = simd_update_row_erode_UBYTE,
    [IM_UBYTE] = simd_update_row_erode_UBYTE,
    [IM_BYTE] = simd_update_row_erode_BYTE,
    [IM_USHORT] = simd_update_row_erode_USHORT,
    [IM_SHORT] = simd_update_row_erode_SHORT,
    [IM_ULONG] = simd_update_row_erode_ULONG,
    [IM_LONG] = simd_update_row_erode_LONG,
    [IM_FLOAT] = simd_update_row_erode_FLOAT,
    [IM_DOUBLE] = simd_update_row_erode_DOUBLE
};


#ifdef simd_ldu_UBYTE

#define generate_process_image_row_function(tt, opname, op)		\
    static void simd_process_imrow_ ## opname ## _ ## tt(ip_image *im, ip_chordtbl *ct, int row) \
    {									\
	simd_ ## tt *iptr = IM_ROWADR(im, row, v);			\
	ip_strel *se = ct->strel;					\
	int rowlen = im->row_size / sizeof(simd_ ## tt);		\
									\
	for (int x=0; x<rowlen; x++) {					\
	    tt ## _type *ctrow;						\
	    simd_ ## tt val;						\
									\
	    ctrow = se->chords[0].ct_row;				\
	    ctrow += se->chords[0].x + (x*SIMD_NUM_ELEMENTS(tt)) + ct->offset; \
	    val = simd_ldu_ ## tt(ctrow);				\
									\
	    for (int c=1; c<se->num_chords; c++) {			\
		ctrow = se->chords[c].ct_row;				\
		ctrow += se->chords[c].x + (x*SIMD_NUM_ELEMENTS(tt)) + ct->offset; \
		val = simd_ ## op ## _ ## tt(val, simd_ldu_ ## tt(ctrow)); \
	    }								\
	    *iptr++ = val;						\
	}								\
	SIMD_EXIT();							\
    }

generate_process_image_row_function(UBYTE, dilate, max)
generate_process_image_row_function(UBYTE, erode, min)
generate_process_image_row_function(BYTE, dilate, max)
generate_process_image_row_function(BYTE, erode, min)
generate_process_image_row_function(USHORT, dilate, max)
generate_process_image_row_function(USHORT, erode, min)
generate_process_image_row_function(SHORT, dilate, max)
generate_process_image_row_function(SHORT, erode, min)

#ifdef simd_min_ULONG
generate_process_image_row_function(ULONG, dilate, max)
generate_process_image_row_function(ULONG, erode, min)
#else
#define simd_process_imrow_dilate_ULONG NULL
#define simd_process_imrow_erode_ULONG NULL
#endif

#ifdef simd_min_LONG
generate_process_image_row_function(LONG, dilate, max)
generate_process_image_row_function(LONG, erode, min)
#else
#define simd_process_imrow_dilate_LONG NULL
#define simd_process_imrow_erode_LONG NULL
#endif

#ifdef simd_min_FLOAT
generate_process_image_row_function(FLOAT, dilate, max)
generate_process_image_row_function(FLOAT, erode, min)
#else
#define simd_process_imrow_dilate_FLOAT NULL
#define simd_process_imrow_erode_FLOAT NULL
#endif

#ifdef simd_min_DOUBLE
generate_process_image_row_function(DOUBLE, dilate, max)
generate_process_image_row_function(DOUBLE, erode, min)
#else
#define simd_process_imrow_dilate_DOUBLE NULL
#define simd_process_imrow_erode_DOUBLE NULL
#endif

#else

#define simd_process_imrow_dilate_UBYTE NULL
#define simd_process_imrow_erode_UBYTE NULL
#define simd_process_imrow_dilate_BYTE NULL
#define simd_process_imrow_erode_BYTE NULL
#define simd_process_imrow_dilate_USHORT NULL
#define simd_process_imrow_erode_USHORT NULL
#define simd_process_imrow_dilate_SHORT NULL
#define simd_process_imrow_erode_SHORT NULL
#define simd_process_imrow_dilate_ULONG NULL
#define simd_process_imrow_erode_ULONG NULL
#define simd_process_imrow_dilate_LONG NULL
#define simd_process_imrow_erode_LONG NULL
#define simd_process_imrow_dilate_FLOAT NULL
#define simd_process_imrow_erode_FLOAT NULL
#define simd_process_imrow_dilate_DOUBLE NULL
#define simd_process_imrow_erode_DOUBLE NULL

#endif

ct_function_ITi ip_simd_process_imrow_dilate_bytype[IM_TYPEMAX] = {
    [IM_BINARY] = simd_process_imrow_dilate_UBYTE,
    [IM_UBYTE] = simd_process_imrow_dilate_UBYTE,
    [IM_BYTE] = simd_process_imrow_dilate_BYTE,
    [IM_USHORT] = simd_process_imrow_dilate_USHORT,
    [IM_SHORT] = simd_process_imrow_dilate_SHORT,
    [IM_ULONG] = simd_process_imrow_dilate_ULONG,
    [IM_LONG] = simd_process_imrow_dilate_LONG,
    [IM_FLOAT] = simd_process_imrow_dilate_FLOAT,
    [IM_DOUBLE] = simd_process_imrow_dilate_DOUBLE
};

ct_function_ITi ip_simd_process_imrow_erode_bytype[IM_TYPEMAX] = {
    [IM_BINARY] = simd_process_imrow_erode_UBYTE,
    [IM_UBYTE] = simd_process_imrow_erode_UBYTE,
    [IM_BYTE] = simd_process_imrow_erode_BYTE,
    [IM_USHORT] = simd_process_imrow_erode_USHORT,
    [IM_SHORT] = simd_process_imrow_erode_SHORT,
    [IM_ULONG] = simd_process_imrow_erode_ULONG,
    [IM_LONG] = simd_process_imrow_erode_LONG,
    [IM_FLOAT] = simd_process_imrow_erode_FLOAT,
    [IM_DOUBLE] = simd_process_imrow_erode_DOUBLE
};


#endif
