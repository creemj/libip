/*
  Module: simd/stats_simd.c
  Author: M. J. Cree.

  Simd implementations of the stats.c module.

  Copyright (C) 2016 by Michael J. Cree
*/

#include <ip/ip.h>

#include "../function_ops.h"
#include "../internal.h"
#include "simd.h"
#include "stats_simd.h"


/*
 * Returns 1 if the two input images are not the same.
 *
 * To implement an accelerated version of im_differ() requires some means of
 * testing whether a SIMD vector is zero in all elements and branching on
 * that. On some architectures this test cannot be implemented efficiently
 * (e.g. armhf, as it requires a final extract scalar from vector which can
 * incur a substantial CPU stall) thus the SIMD implementation can actually
 * be slower than the generic scalar implementation.  We therefore test for
 * the presence of the simd_vector_zero_UBYTE() macro.
 */

#if defined(simd_create_byte_mask) && defined(simd_vector_zero_UBYTE)

static int ip_simd_image_differ_GENERIC(ip_image *d, ip_image *s)
{
    size_t rowlen = ip_type_size(d->type) * (size_t)d->size.x;
    int steps = rowlen / sizeof(simd_UBYTE);
    int extra = rowlen - steps * sizeof(simd_UBYTE);
    simd_UBYTE mask, res;
    int differs = 0;

    mask = simd_create_byte_mask(extra);
    res = simd_splat_UBYTE(0);

    for (int j=0; j<d->size.y; j++) {
        simd_UBYTE *dptr = IM_ROWADR(d, j, v);
        simd_UBYTE *sptr = IM_ROWADR(s, j, v);

        for (int i=0; i<steps; i++) {
            res = simd_or_UBYTE(res, simd_xor_UBYTE(*sptr++, *dptr++));
        }

        /* Finish off last few pixels of row */
        if (extra) {
            simd_UBYTE dval, sval;

            dval = simd_and_UBYTE(*dptr, mask);
            sval = simd_and_UBYTE(*sptr, mask);
            res = simd_or_UBYTE(res, simd_xor_UBYTE(sval, dval));
        }

        /* If any bit is set in res then images differ */
        if (! simd_vector_zero_UBYTE(res)) {
            differs = 1;
            goto quick_exit;
        }
    }

 quick_exit:

    SIMD_EXIT();
    return differs;
}


improc_function_i_II ip_simd_image_differ = ip_simd_image_differ_GENERIC;

#else

improc_function_i_II ip_simd_image_differ = NULL;

#endif
