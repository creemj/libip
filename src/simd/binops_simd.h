
extern void ip_simd_or(ip_image *d, ip_image *s);
extern void ip_simd_and(ip_image *d, ip_image *s);
extern void ip_simd_xor(ip_image *d, ip_image *s);

extern improc_function_II ip_simd_max_bytype[IM_TYPEMAX];
extern improc_function_II ip_simd_min_bytype[IM_TYPEMAX];
