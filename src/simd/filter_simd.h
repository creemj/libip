extern improc_function_Ikt ip_simd_filter_horiz_difference_bytype[IM_TYPEMAX];
extern improc_function_Ikt ip_simd_filter_vert_difference_bytype[IM_TYPEMAX];
extern improc_function_Ikt ip_simd_filter_horiz_centre_difference_bytype[IM_TYPEMAX];
extern improc_function_Ikt ip_simd_filter_vert_centre_difference_bytype[IM_TYPEMAX];
extern improc_function_Ikt ip_simd_filter_horiz_laplace_difference_bytype[IM_TYPEMAX];
extern improc_function_Ikt ip_simd_filter_vert_laplace_difference_bytype[IM_TYPEMAX];
extern improc_function_Ikt ip_simd_filter_horiz_edge3x3_bytype[IM_TYPEMAX];
extern improc_function_Ikt ip_simd_filter_vert_edge3x3_bytype[IM_TYPEMAX];
extern improc_function_Ikt ip_simd_filter_horiz_mean_inplace_bytype[IM_TYPEMAX];
extern improc_function_Ikt ip_simd_filter_horiz_mean_bytype[IM_TYPEMAX];
extern improc_function_Ikt ip_simd_filter_vert_mean_inplace_bytype[IM_TYPEMAX];
extern improc_function_Ikt ip_simd_filter_vert_mean_bytype[IM_TYPEMAX];
