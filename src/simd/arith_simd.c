/*
   Module: src/simd/arith_simd.c
   Author: M.J.Cree

   Binary arithmetic operators between two images (SIMD implementations).
   Entry points are in arith.c

   Copyright (C) 2012-2013, 2015 Michael J. Cree

*/

#include <ip/ip.h>

#include "../internal.h"
#include "../function_ops.h"

#include "simd.h"
#include "arith_simd.h"


/*
 * Macro to process images with SIMD 'vector' operators.
 */

#define SIMD_PROCESS_IMAGE(d, s, type, op)			\
    {								\
	int length = d->row_size / sizeof(simd_ ## type);	\
	for (int j=0; j<d->size.y; j++) {			\
	    simd_ ## type *dptr = IM_ROWADR(d, j, v);		\
	    simd_ ## type *sptr = IM_ROWADR(s, j, v);		\
	    for (int i=0; i<length; i++) {			\
		PREFETCH_READ_WRITE(dptr+PREFETCH_OFS);		\
		PREFETCH_READ_ONCE(sptr+PREFETCH_OFS);		\
		*dptr = simd_ ## op ## _ ## type(*dptr, *sptr);	\
		dptr++; sptr++;					\
	    }							\
	}							\
    }

#define generate_image_same_function(type, operator)			\
    static void								\
    im_simd_ ## operator ## _same_ ## type(ip_image *d, ip_image *s, int f)	\
    {									\
	SIMD_PROCESS_IMAGE(d, s, type, operator);			\
	SIMD_EXIT();							\
    }

/*
 * Addition operators on a pair of images
 */

/*
 * Generate the functions:
 *    im_simd_add_same_UBYTE()
 *    im_simd_add_same_USHORT()
 *    im_simd_add_same_ULONG()
 *    im_simd_add_same_FLOAT()
 *    im_simd_add_same_DOUBLE()
 */

generate_image_same_function(UBYTE, add);
generate_image_same_function(USHORT, add);
generate_image_same_function(ULONG, add);
#ifdef simd_add_FLOAT
generate_image_same_function(FLOAT, add);
#endif
#ifdef simd_add_DOUBLE
generate_image_same_function(DOUBLE, add);
#endif

improc_function_IIf ip_simd_add_same[IM_TYPEMAX] = {
    [IM_BYTE] = im_simd_add_same_UBYTE,
    [IM_UBYTE] = im_simd_add_same_UBYTE,
    [IM_RGB] = im_simd_add_same_UBYTE,
    [IM_RGBA] = im_simd_add_same_UBYTE,
    [IM_SHORT] = im_simd_add_same_USHORT,
    [IM_USHORT] = im_simd_add_same_USHORT,
    [IM_RGB16] = im_simd_add_same_USHORT,
    [IM_LONG] = im_simd_add_same_ULONG,
    [IM_ULONG] = im_simd_add_same_ULONG,
#ifdef simd_add_FLOAT
    [IM_FLOAT] = im_simd_add_same_FLOAT,
    [IM_COMPLEX] = im_simd_add_same_FLOAT,
#endif
#ifdef simd_add_DOUBLE
    [IM_DOUBLE] = im_simd_add_same_DOUBLE,
    [IM_DCOMPLEX] = im_simd_add_same_DOUBLE,
#endif
};


/*
 * Generate the functions:
 *    im_simd_add_clipped_same_BYTE()
 *    im_simd_add_clipped_same_UBYTE()
 *    im_simd_add_clipped_same_SHORT()
 *    im_simd_add_clipped_same_USHORT()
 *    im_simd_add_clipped_same_LONG()
 *    im_simd_add_clipped_same_ULONG()
 */

generate_image_same_function(UBYTE, add_clipped);
generate_image_same_function(BYTE, add_clipped);
generate_image_same_function(USHORT, add_clipped);
generate_image_same_function(SHORT, add_clipped);
#ifdef simd_add_clipped_ULONG
generate_image_same_function(ULONG, add_clipped);
#endif
#ifdef simd_add_clipped_LONG
generate_image_same_function(LONG, add_clipped);
#endif

improc_function_IIf ip_simd_add_clipped_same[IM_TYPEMAX] = {
    [IM_BYTE] = im_simd_add_clipped_same_BYTE,
    [IM_UBYTE] = im_simd_add_clipped_same_UBYTE,
    [IM_RGB] = im_simd_add_clipped_same_UBYTE,
    [IM_RGBA] = im_simd_add_clipped_same_UBYTE,
    [IM_SHORT] = im_simd_add_clipped_same_SHORT,
    [IM_USHORT] = im_simd_add_clipped_same_USHORT,
    [IM_RGB16] = im_simd_add_clipped_same_USHORT,
#ifdef simd_add_clipped_LONG
    [IM_LONG] = im_simd_add_clipped_same_LONG,
#endif
#ifdef simd_add_clipped_ULONG
    [IM_ULONG] = im_simd_add_clipped_same_ULONG,
#endif
};


/*
 * Have not implemented mixed type addition.
 */

improc_function_IIf ip_simd_add_mixed[IM_TYPEMAX] = {};
improc_function_IIf ip_simd_add_clipped_mixed[IM_TYPEMAX] = {};


/*
 * Subtraction operators on a pair of images
 */

/*
 * Generate the functions:
 *    im_simd_sub_same_UBYTE()
 *    im_simd_sub_same_USHORT()
 *    im_simd_sub_same_ULONG()
 *    im_simd_sub_same_FLOAT()
 *    im_simd_sub_same_DOUBLE()
 */

generate_image_same_function(UBYTE, sub);
generate_image_same_function(USHORT, sub);
generate_image_same_function(ULONG, sub);
#ifdef simd_sub_FLOAT
generate_image_same_function(FLOAT, sub);
#endif
#ifdef simd_sub_DOUBLE
generate_image_same_function(DOUBLE, sub);
#endif

improc_function_IIf ip_simd_sub_same[IM_TYPEMAX] = {
    [IM_BYTE] = im_simd_sub_same_UBYTE,
    [IM_UBYTE] = im_simd_sub_same_UBYTE,
    [IM_RGB] = im_simd_sub_same_UBYTE,
    [IM_RGBA] = im_simd_sub_same_UBYTE,
    [IM_SHORT] = im_simd_sub_same_USHORT,
    [IM_USHORT] = im_simd_sub_same_USHORT,
    [IM_RGB16] = im_simd_sub_same_USHORT,
    [IM_LONG] = im_simd_sub_same_ULONG,
    [IM_ULONG] = im_simd_sub_same_ULONG,
#ifdef simd_sub_FLOAT
    [IM_FLOAT] = im_simd_sub_same_FLOAT,
    [IM_COMPLEX] = im_simd_sub_same_FLOAT,
#endif
#ifdef simd_sub_DOUBLE
    [IM_DOUBLE] = im_simd_sub_same_DOUBLE,
    [IM_DCOMPLEX] = im_simd_sub_same_DOUBLE,
#endif
};


/*
 * Generate the functions:
 *    im_simd_sub_clipped_same_BYTE()
 *    im_simd_sub_clipped_same_UBYTE()
 *    im_simd_sub_clipped_same_SHORT()
 *    im_simd_sub_clipped_same_USHORT()
 *    im_simd_sub_clipped_same_LONG()
 *    im_simd_sub_clipped_same_ULONG()
 */

generate_image_same_function(UBYTE, sub_clipped);
generate_image_same_function(BYTE, sub_clipped);
generate_image_same_function(USHORT, sub_clipped);
generate_image_same_function(SHORT, sub_clipped);
#ifdef simd_sub_clipped_ULONG
generate_image_same_function(ULONG, sub_clipped);
#endif
#ifdef simd_sub_clipped_LONG
generate_image_same_function(LONG, sub_clipped);
#endif

improc_function_IIf ip_simd_sub_clipped_same[IM_TYPEMAX] = {
    [IM_BYTE] = im_simd_sub_clipped_same_BYTE,
    [IM_UBYTE] = im_simd_sub_clipped_same_UBYTE,
    [IM_RGB] = im_simd_sub_clipped_same_UBYTE,
    [IM_RGBA] = im_simd_sub_clipped_same_UBYTE,
    [IM_SHORT] = im_simd_sub_clipped_same_SHORT,
    [IM_USHORT] = im_simd_sub_clipped_same_USHORT,
    [IM_RGB16] = im_simd_sub_clipped_same_USHORT,
#ifdef simd_sub_clipped_LONG
    [IM_LONG] = im_simd_sub_clipped_same_LONG,
#endif
#ifdef simd_sub_clipped_ULONG
    [IM_ULONG] = im_simd_sub_clipped_same_ULONG,
#endif
};


/*
 * Have not implemented mixed type subtraction.
 */

improc_function_IIf ip_simd_sub_mixed[IM_TYPEMAX] = {};
improc_function_IIf ip_simd_sub_clipped_mixed[IM_TYPEMAX] = {};



/*
 * Multiplication operators on a pair of images
 */

/*
 * Generate the functions:
 *    im_simd_mul_same_UBYTE()
 *    im_simd_mul_same_USHORT()
 *    im_simd_mul_same_ULONG()
 *    im_simd_mul_same_FLOAT()
 *    im_simd_mul_same_DOUBLE()
 */

#ifdef simd_mul_UBYTE
generate_image_same_function(UBYTE, mul);
#endif
#ifdef simd_mul_USHORT
generate_image_same_function(USHORT, mul);
#endif
#ifdef simd_mul_ULONG
generate_image_same_function(ULONG, mul);
#endif
#ifdef simd_mul_FLOAT
generate_image_same_function(FLOAT, mul);
#endif
#ifdef simd_mul_DOUBLE
generate_image_same_function(DOUBLE, mul);
#endif
#ifdef simd_mul_COMPLEX
generate_image_same_function(COMPLEX, mul);
#endif
#ifdef simd_mul_DCOMPLEX
generate_image_same_function(DCOMPLEX, mul);
#endif

improc_function_IIf ip_simd_mul_same[IM_TYPEMAX] = {
#ifdef simd_mul_UBYTE
    [IM_BYTE] = im_simd_mul_same_UBYTE,
    [IM_UBYTE] = im_simd_mul_same_UBYTE,
    [IM_RGB] = im_simd_mul_same_UBYTE,
    [IM_RGBA] = im_simd_mul_same_UBYTE,
#endif
#ifdef simd_mul_USHORT
    [IM_SHORT] = im_simd_mul_same_USHORT,
    [IM_USHORT] = im_simd_mul_same_USHORT,
    [IM_RGB16] = im_simd_mul_same_USHORT,
#endif
#ifdef simd_mul_ULONG
    [IM_LONG] = im_simd_mul_same_ULONG,
    [IM_ULONG] = im_simd_mul_same_ULONG,
#endif
#ifdef simd_mul_FLOAT
    [IM_FLOAT] = im_simd_mul_same_FLOAT,
#endif
#ifdef simd_mul_DOUBLE
    [IM_DOUBLE] = im_simd_mul_same_DOUBLE,
#endif
#ifdef simd_mul_COMPLEX
    [IM_COMPLEX] = im_simd_mul_same_COMPLEX,
#endif
#ifdef simd_mul_DCOMPLEX
    [IM_DCOMPLEX] = im_simd_mul_same_DCOMPLEX,
#endif
};


/*
 * Generate the functions:
 *    im_simd_mul_clipped_same_BYTE()
 *    im_simd_mul_clipped_same_UBYTE()
 *    im_simd_mul_clipped_same_SHORT()
 *    im_simd_mul_clipped_same_USHORT()
 *    im_simd_mul_clipped_same_LONG()
 *    im_simd_mul_clipped_same_ULONG()
 */

#ifdef simd_mul_clipped_UBYTE
generate_image_same_function(UBYTE, mul_clipped);
#endif
#ifdef simd_mul_clipped_BYTE
generate_image_same_function(BYTE, mul_clipped);
#endif
#ifdef simd_mul_clipped_USHORT
generate_image_same_function(USHORT, mul_clipped);
#endif
#ifdef simd_mul_clipped_SHORT
generate_image_same_function(SHORT, mul_clipped);
#endif
#ifdef simd_mul_clipped_ULONG
generate_image_same_function(ULONG, mul_clipped);
#endif
#ifdef simd_mul_clipped_LONG
generate_image_same_function(LONG, mul_clipped);
#endif

improc_function_IIf ip_simd_mul_clipped_same[IM_TYPEMAX] = {
#ifdef simd_mul_clipped_BYTE
    [IM_BYTE] = im_simd_mul_clipped_same_BYTE,
#endif
#ifdef simd_mul_clipped_UBYTE
    [IM_UBYTE] = im_simd_mul_clipped_same_UBYTE,
    [IM_RGB] = im_simd_mul_clipped_same_UBYTE,
    [IM_RGBA] = im_simd_mul_clipped_same_UBYTE,
#endif
#ifdef simd_mul_clipped_SHORT
    [IM_SHORT] = im_simd_mul_clipped_same_SHORT,
#endif
#ifdef simd_mul_clipped_USHORT
    [IM_USHORT] = im_simd_mul_clipped_same_USHORT,
    [IM_RGB16] = im_simd_mul_clipped_same_USHORT,
#endif
#ifdef simd_mul_clipped_LONG
    [IM_LONG] = im_simd_mul_clipped_same_LONG,
#endif
#ifdef simd_mul_clipped_ULONG
    [IM_ULONG] = im_simd_mul_clipped_same_ULONG,
#endif
};


/*
 * Have not implemented mixed type multiplication.
 */

improc_function_IIf ip_simd_mul_mixed[IM_TYPEMAX] = {};
improc_function_IIf ip_simd_mul_clipped_mixed[IM_TYPEMAX] = {};


/*
 * Division operators on a pair of images.  Largely unimplemented.
 */

#ifdef simd_div_FLOAT
generate_image_same_function(FLOAT, div);
#endif
#ifdef simd_div_DOUBLE
generate_image_same_function(DOUBLE, div);
#endif

improc_function_IIf ip_simd_div_same[IM_TYPEMAX] = {
#ifdef simd_div_FLOAT
    [IM_FLOAT] = im_simd_div_same_FLOAT,
#endif
#ifdef simd_div_DOUBLE
    [IM_DOUBLE] = im_simd_div_same_DOUBLE
#endif
};
improc_function_IIf ip_simd_div_mixed[IM_TYPEMAX] = {};
improc_function_IIf ip_simd_div_protected_same[IM_TYPEMAX] = {};
improc_function_IIf ip_simd_div_protected_mixed[IM_TYPEMAX] = {};
