/*
  Header: simd/arith_simd.h
  Author: M. J. Cree

  Entry points to arith_simd.c.
  Private to the IP library.

  (C) Michael J. Cree 2013

*/

#include "../function_ops.h"

extern improc_function_Icf ip_simd_addc_function[IM_TYPEMAX];
extern improc_function_Icf ip_simd_addc_clipped_function[IM_TYPEMAX];
extern improc_function_Izf ip_simd_addc_complex_function[IM_TYPEMAX];
extern improc_function_Iccccf ip_simd_addc_rgb_function[IM_TYPEMAX];

extern improc_function_Icf ip_simd_subc_function[IM_TYPEMAX];
extern improc_function_Icf ip_simd_subc_clipped_function[IM_TYPEMAX];
extern improc_function_Izf ip_simd_subc_complex_function[IM_TYPEMAX];
extern improc_function_Iccccf ip_simd_subc_rgb_function[IM_TYPEMAX];

extern improc_function_Icf ip_simd_mulc_function[IM_TYPEMAX];
extern improc_function_Icf ip_simd_mulc_clipped_function[IM_TYPEMAX];
extern improc_function_Izf ip_simd_mulc_complex_function[IM_TYPEMAX];
extern improc_function_Iccccf ip_simd_mulc_rgb_function[IM_TYPEMAX];

extern improc_function_Icf ip_simd_divc_function[IM_TYPEMAX];
extern improc_function_Izf ip_simd_divc_complex_function[IM_TYPEMAX];

extern improc_function_Icf ip_simd_idivc_function[IM_TYPEMAX];
