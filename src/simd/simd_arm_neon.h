/*
   Name: ip/simd_arm_neon.h
   Author: M. J. Cree

   Copyright (C) 2013, 2015-2019 Michael J. Cree

   Arm Neon SIMD support for the IP library.

*/


/*
 * ARM Neon SIMD support
 */

#include <arm_neon.h>

#include <ip/divide.h>

/* Basic vector types */
typedef uint8x16_t simd_UBYTE;
typedef int8x16_t simd_BYTE;
typedef uint16x8_t simd_USHORT;
typedef int16x8_t simd_SHORT;
typedef uint32x4_t simd_ULONG;
typedef int32x4_t simd_LONG;
typedef float32x4_t simd_FLOAT;
typedef float32x4_t simd_COMPLEX;

#ifdef __aarch64__
typedef float64x2_t simd_DOUBLE;
typedef float64x2_t simd_DCOMPLEX;

typedef uint64x2_t simd_ULONG64;
typedef int64x2_t simd_LONG64;
#endif

/*
 * simd_splat
 *
 * For splatting a scalar value across the simd vector.
 */
#define simd_splat_BYTE(c) vdupq_n_s8(c)
#define simd_splat_UBYTE(c) vdupq_n_u8(c)
#define simd_splat_SHORT(c) vdupq_n_s16(c)
#define simd_splat_USHORT(c) vdupq_n_u16(c)
#define simd_splat_LONG(c) vdupq_n_s32(c)
#define simd_splat_ULONG(c) vdupq_n_u32(c)
#define simd_splat_FLOAT(c) vdupq_n_f32(c)
#define simd_splat_COMPLEX(u,v) ((simd_FLOAT){u, v, u, v})
#ifdef __aarch64__
#define simd_splat_DOUBLE(c) vdupq_n_f64(c)
#define simd_splat_DCOMPLEX(u,v) ((simd_DOUBLE){u, v})
#define simd_splat_LONG64(c) vdupq_n_s64(c)
#define simd_splat_ULONG64(c) vdupq_n_u64(c)
#endif

/*
 * Now that the splat routines are defined and, thus, indicate which image
 * types we support in SIMD, now get the rest of the typedefs that are
 * needed for the convert routines below.
 */

#include "simd_td.h"


/*
 * simd_splat_v0
 *
 * Splat the zeroth element across the vector.
 */
#define simd_splat_v0_BYTE(c) vdupq_lane_s8(vget_low_s8(c), 0)
#define simd_splat_v0_UBYTE(c) vdupq_lane_u8(vget_low_u8(c), 0)
#define simd_splat_v0_SHORT(c) vdupq_lane_s16(vget_low_s16(c), 0)
#define simd_splat_v0_USHORT(c) vdupq_lane_u16(vget_low_u16(c), 0)
#define simd_splat_v0_LONG(c) vdupq_lane_s32(vget_low_s32(c), 0)
#define simd_splat_v0_ULONG(c) vdupq_lane_u32(vget_low_u32(c), 0)
#define simd_splat_v0_FLOAT(c) vdupq_lane_f32(vget_low_f32(c), 0)
#ifdef __aarch64__
#define simd_splat_v0_DOUBLE(c) vdupq_lane_f64(vget_low_f64(c), 0)
#endif


/*
 * simd_perm
 *
 * BYTE Permutation of the vector.  (Technically this instruction is more
 * general than a strict permutation...) Arm32 Neon has the table lookup
 * instruction but only operates on a D register.  We use that to implement
 * an equivalent function for a Q register.  Arm64 has a useful CPU
 * instruction to do this directly.
 */
#ifdef __aarch64__

static inline simd_UBYTE simd_perm_UBYTE(simd_UBYTE tbl, simd_UBYTE idx)
{
    return vqtbl1q_u8(tbl, idx);
}

static inline simd_BYTE simd_perm_BYTE(simd_BYTE tbl, simd_BYTE idx)
{
    return vqtbl1q_s8(tbl, (simd_UBYTE)idx);
}

#else

static inline simd_UBYTE simd_perm_UBYTE(simd_UBYTE tbl, simd_UBYTE idx)
{
    uint8x8x2_t xrepack;
    xrepack.val[0] = vget_low_u8(tbl);
    xrepack.val[1] = vget_high_u8(tbl);
    return vcombine_u8(vtbl2_u8(xrepack,vget_low_u8(idx)), vtbl2_u8(xrepack,vget_high_u8(idx)));
}

static inline simd_BYTE simd_perm_BYTE(simd_BYTE tbl, simd_BYTE idx)
{
    int8x8x2_t xrepack;
    xrepack.val[0] = vget_low_s8(tbl);
    xrepack.val[1] = vget_high_s8(tbl);
    return vcombine_s8(vtbl2_s8(xrepack,vget_low_s8(idx)), vtbl2_s8(xrepack,vget_high_s8(idx)));
}

#endif

/*
 * simd_splat_vN
 *
 * Splat the Nth element of the vector across the vector.  The pos argument
 * may not be known at compile time so use the simd_perm instructions above
 * to achieve this.
 */
static inline simd_USHORT _ip_splat_vN_USHORT(simd_USHORT x, int pos)
{
    const simd_USHORT incr = simd_splat_USHORT(0x100);
    simd_USHORT idx = vaddq_u16((simd_USHORT)simd_splat_UBYTE(2*pos), incr);
    return (simd_USHORT)simd_perm_UBYTE((simd_UBYTE)x, (simd_UBYTE)idx);
}
static inline simd_ULONG _ip_splat_vN_ULONG(simd_ULONG x, int pos)
{
    const simd_ULONG incr = simd_splat_ULONG(0x03020100);
    simd_ULONG idx = vaddq_u32((simd_ULONG)simd_splat_UBYTE(4*pos), incr);
    return (simd_ULONG)simd_perm_UBYTE((simd_UBYTE)x, (simd_UBYTE)idx);
}
#define simd_splat_vN_BYTE(x, n)  simd_perm_BYTE(x, simd_splat_BYTE(n))
#define simd_splat_vN_UBYTE(x, n) simd_perm_UBYTE(x, simd_splat_UBYTE(n))
#define simd_splat_vN_SHORT(x, n) ((simd_SHORT)_ip_splat_vN_USHORT((simd_USHORT)x, n))
#define simd_splat_vN_USHORT(x, n) _ip_splat_vN_USHORT(x, n)
#define simd_splat_vN_LONG(x, n) ((simd_LONG)_ip_splat_vN_ULONG((simd_ULONG)x, n))
#define simd_splat_vN_ULONG(x, n) _ip_splat_vN_ULONG(x, n)
#define simd_splat_vN_FLOAT(x, n) ((simd_FLOAT)_ip_splat_vN_ULONG((simd_ULONG)x, n))
#ifdef __aarch64__
static inline simd_DOUBLE _ip_splat_vN_DOUBLE(simd_DOUBLE x, int pos)
{
    const simd_ULONG incr = (simd_ULONG){0x03020100, 0x07060504,
					 0x03020100, 0x07060504};
    simd_ULONG idx = vaddq_u32((simd_ULONG)simd_splat_UBYTE(8*pos), incr);
    return (simd_DOUBLE)simd_perm_UBYTE((simd_UBYTE)x, (simd_UBYTE)idx);
}
#define simd_splat_vN_DOUBLE(x, n) _ip_splat_vN_DOUBLE(x, n)
#endif


/* Masks for multielement image types (complex and the rgb image types) */
#define simd_mask_RGB(r, g, b) ((simd_UBYTE){r, g, b, r, g, b, r, g, b, r, g, b, r, g, b, r})
#define simd_mask_RGBA(r, g, b, a) ((simd_UBYTE){r, g, b, a, r, g, b, a, r, g, b, a, r, g, b, a})
#define simd_mask_RGB16(r, g, b) ((simd_USHORT){r, g, b, r, g, b, r, g})
#define simd_mask_COMPLEX(r, i) ((simd_ULONG){r, i, r, i})
#ifdef __aarch64__
#define simd_mask_DCOMPLEX(r, i) ((simd_ULONG){r, r, i, i})
#endif


/*
 * These ror_RGB routines may not be the most efficient possible.  It might
 * be possible to do better with the VTBL instructions.
 */
#ifdef __aarch64__

static inline simd_UBYTE _ip_simd_ror_RGB(simd_UBYTE a)
{
    const simd_UBYTE b = (simd_UBYTE){1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 13};
    return simd_perm_UBYTE(a, b);
}
static inline simd_USHORT _ip_simd_ror_RGB16(simd_USHORT a)
{
    const simd_UBYTE b = (simd_UBYTE){4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 10, 11, 12, 13};
    return (simd_USHORT)simd_perm_UBYTE((simd_UBYTE)a, b);
}
#else

static inline simd_UBYTE _ip_simd_ror_RGB(simd_UBYTE a)
{
    const simd_UBYTE zero = simd_splat_UBYTE(0);
    const simd_UBYTE mask = (simd_UBYTE){0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0};
    simd_UBYTE t1, t2;
    t1 = vandq_u8(a, mask);
    t1 = vextq_u8(t1, t1, 14); /* shift left t1 by two */
    t2 = vextq_u8(a, zero, 1);	 /* shift right a by one */
    return vorrq_u8(t1, t2);
}

static inline simd_USHORT _ip_simd_ror_RGB16(simd_USHORT a)
{
    const simd_USHORT zero = simd_splat_USHORT(0);
    const simd_USHORT mask = (simd_USHORT){0, 0, 0, 0, 0, -1, -1, 0};
    simd_USHORT t1, t2;
    t1 = vandq_u16(a, mask);
    t1 = vextq_u16(t1, t1, 7); /* shift left t1 by one */
    t2 = vextq_u16(a, zero, 2);	 /* shift right a by two */
    return vorrq_u16(t1, t2);
}

#endif

#define simd_ror_RGB(x) _ip_simd_ror_RGB(x)
#define simd_ror_RGB16(x) _ip_simd_ror_RGB16(x)


/*
 * simd_extract
 *
 * Extract a specified element from a SIMD vector.
 *
 * Note that this is very inefficient (causes CPU stalls) on some Arm
 * architectures.
 */
#define simd_extract_BYTE(v, p) vgetq_lane_s8(v, p)
#define simd_extract_UBYTE(v, p) vgetq_lane_u8(v, p)
#define simd_extract_SHORT(v, p) vgetq_lane_s16(v, p)
#define simd_extract_USHORT(v, p) vgetq_lane_u16(v, p)
#define simd_extract_LONG(v, p) vgetq_lane_s32(v, p)
#define simd_extract_ULONG(v, p) vgetq_lane_u32(v, p)
#define simd_extract_FLOAT(v, p) vgetq_lane_f32(v, p)
#ifdef __aarch64__
#define simd_extract_DOUBLE(v, p) vgetq_lane_f64(v, p)
#endif

/*
 * simd_cvu_imm
 *
 * For constructing an unaligned vector (cvu) from two adjacent vectors. The
 * offset must be known at compile time.
 */
#define simd_cvu_imm_BYTE(l, h, o) vextq_s8(l, h, o)
#define simd_cvu_imm_UBYTE(l, h, o) vextq_u8(l, h, o)
#define simd_cvu_imm_SHORT(l, h, o) vextq_s16(l, h, o)
#define simd_cvu_imm_USHORT(l, h, o) vextq_u16(l, h, o)
#define simd_cvu_imm_LONG(l, h, o) vextq_s32(l, h, o)
#define simd_cvu_imm_ULONG(l, h, o) vextq_u32(l, h, o)
#define simd_cvu_imm_FLOAT(l, h, o) vextq_f32(l, h, o)
#ifdef __aarch64__
#define simd_cvu_imm_DOUBLE(l, h, o) vextq_f64(l, h, o)
#endif

static inline simd_UBYTE _ip_simd_ldu_UBYTE(void *p)
{
    return vld1q_u8((uint8_t *)p);
}
static inline simd_BYTE _ip_simd_ldu_BYTE(void *p)
{
    return vld1q_s8((int8_t *)p);
}
static inline simd_USHORT _ip_simd_ldu_USHORT(void *p)
{
    return vld1q_u16((uint16_t *)p);
}
static inline simd_SHORT _ip_simd_ldu_SHORT(void *p)
{
    return vld1q_s16((int16_t *)p);
}

static inline simd_ULONG _ip_simd_ldu_ULONG(void *p)
{
    return vld1q_u32((uint32_t *)p);
}
static inline simd_LONG _ip_simd_ldu_LONG(void *p)
{
    return vld1q_s32((int32_t *)p);
}
static inline simd_FLOAT _ip_simd_ldu_FLOAT(void *p)
{
    return vld1q_f32((float *)p);
}

#define simd_ldu_UBYTE(p) _ip_simd_ldu_UBYTE(p)
#define simd_ldu_BYTE(p) _ip_simd_ldu_BYTE(p)
#define simd_ldu_USHORT(p) _ip_simd_ldu_USHORT(p)
#define simd_ldu_SHORT(p) _ip_simd_ldu_SHORT(p)
#define simd_ldu_ULONG(p) _ip_simd_ldu_ULONG(p)
#define simd_ldu_LONG(p) _ip_simd_ldu_LONG(p)
#define simd_ldu_FLOAT(p) _ip_simd_ldu_FLOAT(p)

#ifdef __aarch64__
static inline simd_DOUBLE _ip_simd_ldu_DOUBLE(void *p)
{
    return vld1q_f64((double *)p);
}
#define simd_ldu_DOUBLE(p) _ip_simd_ldu_DOUBLE(p)
#endif


/*
 * Transposition macros
 */

static inline simd_USHORT_8 _ip_simd_transpose_USHORT(simd_USHORT_8 x)
{
    uint16x8x2_t a, b;
    uint32x4x2_t c, d, e, f;
    simd_USHORT_8 res;

    a = vtrnq_u16(x.us[0], x.us[1]);
    b = vtrnq_u16(x.us[2], x.us[3]);
    c = vtrnq_u32((uint32x4_t)a.val[0], (uint32x4_t)b.val[0]);
    d = vtrnq_u32((uint32x4_t)a.val[1], (uint32x4_t)b.val[1]);
    a = vtrnq_u16(x.us[4], x.us[5]);
    b = vtrnq_u16(x.us[6], x.us[7]);
    e = vtrnq_u32((uint32x4_t)a.val[0], (uint32x4_t)b.val[0]);
    f = vtrnq_u32((uint32x4_t)a.val[1], (uint32x4_t)b.val[1]);
#ifdef __aarch64__
    res.us[0] = (simd_USHORT)vtrn1q_f64((simd_DOUBLE)c.val[0], (simd_DOUBLE)e.val[0]);
    res.us[4] = (simd_USHORT)vtrn2q_f64((simd_DOUBLE)c.val[0], (simd_DOUBLE)e.val[0]);
    res.us[2] = (simd_USHORT)vtrn1q_f64((simd_DOUBLE)c.val[1], (simd_DOUBLE)e.val[1]);
    res.us[6] = (simd_USHORT)vtrn2q_f64((simd_DOUBLE)c.val[1], (simd_DOUBLE)e.val[1]);
    res.us[1] = (simd_USHORT)vtrn1q_f64((simd_DOUBLE)d.val[0], (simd_DOUBLE)f.val[0]);
    res.us[5] = (simd_USHORT)vtrn2q_f64((simd_DOUBLE)d.val[0], (simd_DOUBLE)f.val[0]);
    res.us[3] = (simd_USHORT)vtrn1q_f64((simd_DOUBLE)d.val[1], (simd_DOUBLE)f.val[1]);
    res.us[7] = (simd_USHORT)vtrn2q_f64((simd_DOUBLE)d.val[1], (simd_DOUBLE)f.val[1]);
#else
    res.us[0] = (uint16x8_t)vcombine_u32(vget_low_u32(c.val[0]), vget_low_u32(e.val[0]));
    res.us[4] = (uint16x8_t)vcombine_u32(vget_high_u32(c.val[0]), vget_high_u32(e.val[0]));
    res.us[2] = (uint16x8_t)vcombine_u32(vget_low_u32(c.val[1]), vget_low_u32(e.val[1]));
    res.us[6] = (uint16x8_t)vcombine_u32(vget_high_u32(c.val[1]), vget_high_u32(e.val[1]));
    res.us[1] = (uint16x8_t)vcombine_u32(vget_low_u32(d.val[0]), vget_low_u32(f.val[0]));
    res.us[5] = (uint16x8_t)vcombine_u32(vget_high_u32(d.val[0]), vget_high_u32(f.val[0]));
    res.us[3] = (uint16x8_t)vcombine_u32(vget_low_u32(d.val[1]), vget_low_u32(f.val[1]));
    res.us[7] = (uint16x8_t)vcombine_u32(vget_high_u32(d.val[1]), vget_high_u32(f.val[1]));
#endif
    return res;
}
#define simd_transpose_USHORT(x) _ip_simd_transpose_USHORT(x)

static inline simd_FLOAT_4 _ip_simd_transpose_FLOAT(simd_FLOAT_4 x)
{
    float32x4x2_t a, b;
    simd_FLOAT_4 res;

    a = vtrnq_f32(x.f[0], x.f[1]);
    b = vtrnq_f32(x.f[2], x.f[3]);
#ifdef __aarch64__
    res.f[0] = (simd_FLOAT)vtrn1q_f64((simd_DOUBLE)a.val[0], (simd_DOUBLE)b.val[0]);
    res.f[2] = (simd_FLOAT)vtrn2q_f64((simd_DOUBLE)a.val[0], (simd_DOUBLE)b.val[0]);
    res.f[1] = (simd_FLOAT)vtrn1q_f64((simd_DOUBLE)a.val[1], (simd_DOUBLE)b.val[1]);
    res.f[3] = (simd_FLOAT)vtrn2q_f64((simd_DOUBLE)a.val[1], (simd_DOUBLE)b.val[1]);
#else
    res.f[0] = vcombine_f32(vget_low_f32(a.val[0]), vget_low_f32(b.val[0]));
    res.f[2] = vcombine_f32(vget_high_f32(a.val[0]), vget_high_f32(b.val[0]));
    res.f[1] = vcombine_f32(vget_low_f32(a.val[1]), vget_low_f32(b.val[1]));
    res.f[3] = vcombine_f32(vget_high_f32(a.val[1]), vget_high_f32(b.val[1]));
#endif
    return res;
}
#define simd_transpose_FLOAT(a) _ip_simd_transpose_FLOAT(a)

static inline simd_ULONG_4 _ip_simd_transpose_ULONG(simd_ULONG_4 x)
{
    uint32x4x2_t a, b;
    simd_ULONG_4 res;

    a = vtrnq_u32(x.ul[0], x.ul[1]);
    b = vtrnq_u32(x.ul[2], x.ul[3]);
#ifdef __aarch64__
    res.ul[0] = (simd_ULONG)vtrn1q_f64((simd_DOUBLE)a.val[0], (simd_DOUBLE)b.val[0]);
    res.ul[2] = (simd_ULONG)vtrn2q_f64((simd_DOUBLE)a.val[0], (simd_DOUBLE)b.val[0]);
    res.ul[1] = (simd_ULONG)vtrn1q_f64((simd_DOUBLE)a.val[1], (simd_DOUBLE)b.val[1]);
    res.ul[3] = (simd_ULONG)vtrn2q_f64((simd_DOUBLE)a.val[1], (simd_DOUBLE)b.val[1]);
#else
    res.ul[0] = vcombine_u32(vget_low_u32(a.val[0]), vget_low_u32(b.val[0]));
    res.ul[2] = vcombine_u32(vget_high_u32(a.val[0]), vget_high_u32(b.val[0]));
    res.ul[1] = vcombine_u32(vget_low_u32(a.val[1]), vget_low_u32(b.val[1]));
    res.ul[3] = vcombine_u32(vget_high_u32(a.val[1]), vget_high_u32(b.val[1]));
#endif
    return res;
}
#define simd_transpose_ULONG(x) _ip_simd_transpose_ULONG(x)


#ifdef __aarch64__
static inline simd_DOUBLE_2 _ip_simd_transpose_DOUBLE(simd_DOUBLE_2 x)
{
    simd_DOUBLE_2 res;

    res.d[0] = vtrn1q_f64(x.d[0], x.d[1]);
    res.d[1] = vtrn2q_f64(x.d[0], x.d[1]);
    return res;
}
#define simd_transpose_DOUBLE(a) _ip_simd_transpose_DOUBLE(a)
#endif

/*
 * simd_interleave
 */

static inline simd_UBYTE_2 _ip_simd_interleave_UBYTE(simd_UBYTE x, simd_UBYTE y)
{
    uint8x16x2_t r;
    simd_UBYTE_2 res;

    r = vzipq_u8(x, y);
    res.ub[0] = r.val[0];
    res.ub[1] = r.val[1];
    return res;
}
#define simd_interleave_UBYTE(x, y) _ip_simd_interleave_UBYTE(x, y)

static inline simd_BYTE_2 _ip_simd_interleave_BYTE(simd_BYTE x, simd_BYTE y)
{
    int8x16x2_t r;
    simd_BYTE_2 res;

    r = vzipq_s8(x, y);
    res.b[0] = r.val[0];
    res.b[1] = r.val[1];
    return res;
}
#define simd_interleave_BYTE(x, y) _ip_simd_interleave_BYTE(x, y)

static inline simd_USHORT_2 _ip_simd_interleave_USHORT(simd_USHORT x, simd_USHORT y)
{
    uint16x8x2_t r;
    simd_USHORT_2 res;

    r = vzipq_u16(x, y);
    res.us[0] = r.val[0];
    res.us[1] = r.val[1];
    return res;
}
#define simd_interleave_USHORT(x, y) _ip_simd_interleave_USHORT(x, y)

static inline simd_SHORT_2 _ip_simd_interleave_SHORT(simd_SHORT x, simd_SHORT y)
{
    int16x8x2_t r;
    simd_SHORT_2 res;

    r = vzipq_s16(x, y);
    res.s[0] = r.val[0];
    res.s[1] = r.val[1];
    return res;
}
#define simd_interleave_SHORT(x, y) _ip_simd_interleave_SHORT(x, y)

static inline simd_ULONG_2 _ip_simd_interleave_ULONG(simd_ULONG x, simd_ULONG y)
{
    uint32x4x2_t r;
    simd_ULONG_2 res;

    r = vzipq_u32(x, y);
    res.ul[0] = r.val[0];
    res.ul[1] = r.val[1];
    return res;
}
#define simd_interleave_ULONG(x, y) _ip_simd_interleave_ULONG(x, y)

static inline simd_LONG_2 _ip_simd_interleave_LONG(simd_LONG x, simd_LONG y)
{
    int32x4x2_t r;
    simd_LONG_2 res;

    r = vzipq_s32(x, y);
    res.l[0] = r.val[0];
    res.l[1] = r.val[1];
    return res;
}
#define simd_interleave_LONG(x, y) _ip_simd_interleave_LONG(x, y)

static inline simd_FLOAT_2 _ip_simd_interleave_FLOAT(simd_FLOAT x, simd_FLOAT y)
{
    float32x4x2_t r;
    simd_FLOAT_2 res;

    r = vzipq_f32(x, y);
    res.f[0] = r.val[0];
    res.f[1] = r.val[1];
    return res;
}
#define simd_interleave_FLOAT(x, y) _ip_simd_interleave_FLOAT(x, y)

#ifdef __aarch64__
static inline simd_DOUBLE_2 _ip_simd_interleave_DOUBLE(simd_DOUBLE x, simd_DOUBLE y)
{
    simd_DOUBLE_2 res;
    res.d[0] = vzip1q_f64(x,y);
    res.d[1] = vzip2q_f64(x,y);
    return res;
}
#define simd_interleave_DOUBLE(x, y) _ip_simd_interleave_DOUBLE(x, y)
#endif

/*
 * Conversion routines for converting one image type to another.
 *
 * simd_cnvrt_DTYPE_STYPE
 *   - convert source type STYPE to type DTYPE
 *
 * simd_cnvrt_clip_DTYPE_STYPE
 *   - convert source type STYPE to type DTYPE with clipping (saturation).
 */

/* Zero extend smaller integer to larger integer type */
static inline simd_USHORT_2 _ip_simd_cnvrt_USHORT_UBYTE(simd_UBYTE a)
{
    simd_USHORT_2 res;
    res.us[0] = vmovl_u8(vget_low_u8(a));
    res.us[1] = vmovl_u8(vget_high_u8(a));
    return res;
}
#define simd_cnvrt_USHORT_UBYTE(a) _ip_simd_cnvrt_USHORT_UBYTE(a)

static inline simd_ULONG_4 _ip_simd_cnvrt_ULONG_UBYTE(simd_UBYTE a)
{
    simd_ULONG_4 res;
    simd_USHORT tmp;
    tmp = vmovl_u8(vget_low_u8(a));
    res.ul[0] = vmovl_u16(vget_low_u16(tmp));
    res.ul[1] = vmovl_u16(vget_high_u16(tmp));
    tmp = vmovl_u8(vget_high_u8(a));
    res.ul[2] = vmovl_u16(vget_low_u16(tmp));
    res.ul[3] = vmovl_u16(vget_high_u16(tmp));
    return res;
}
#define simd_cnvrt_ULONG_UBYTE(a) _ip_simd_cnvrt_ULONG_UBYTE(a)

static inline simd_ULONG_2 _ip_simd_cnvrt_ULONG_USHORT(simd_USHORT a)
{
    simd_ULONG_2 res;
    res.ul[0] = vmovl_u16(vget_low_u16(a));
    res.ul[1] = vmovl_u16(vget_high_u16(a));
    return res;
}
#define simd_cnvrt_ULONG_USHORT(a) _ip_simd_cnvrt_ULONG_USHORT(a)

/* signed extension of integer to integer */
static inline simd_SHORT_2 _ip_simd_cnvrt_SHORT_BYTE(simd_BYTE a)
{
    simd_SHORT_2 res;
    res.s[0] = vmovl_s8(vget_low_s8(a));
    res.s[1] = vmovl_s8(vget_high_s8(a));
    return res;
}
#define simd_cnvrt_SHORT_BYTE(a) _ip_simd_cnvrt_SHORT_BYTE(a)

static inline simd_LONG_4 _ip_simd_cnvrt_LONG_BYTE(simd_BYTE a)
{
    simd_LONG_4 res;
    simd_SHORT tmp;
    tmp = vmovl_s8(vget_low_s8(a));
    res.l[0] = vmovl_s16(vget_low_s16(tmp));
    res.l[1] = vmovl_s16(vget_high_s16(tmp));
    tmp = vmovl_s8(vget_high_s8(a));
    res.l[2] = vmovl_s16(vget_low_s16(tmp));
    res.l[3] = vmovl_s16(vget_high_s16(tmp));
    return res;
}
#define simd_cnvrt_LONG_BYTE(a) _ip_simd_cnvrt_LONG_BYTE(a)

static inline simd_LONG_2 _ip_simd_cnvrt_LONG_SHORT(simd_SHORT a)
{
    simd_LONG_2 res;
    res.l[0] = vmovl_s16(vget_low_s16(a));
    res.l[1] = vmovl_s16(vget_high_s16(a));
    return res;
}
#define simd_cnvrt_LONG_SHORT(a) _ip_simd_cnvrt_LONG_SHORT(a)

/* Truncation of integer to smaller integer types */
static inline simd_UBYTE _ip_simd_cnvrt_UBYTE_USHORT(simd_USHORT a, simd_USHORT b)
{
    return vcombine_u8(vmovn_u16(a), vmovn_u16(b));
}
#define simd_cnvrt_UBYTE_USHORT(a,b) _ip_simd_cnvrt_UBYTE_USHORT(a,b)

static inline simd_USHORT _ip_simd_cnvrt_USHORT_ULONG(simd_ULONG a, simd_ULONG b)
{
    return vcombine_u16(vmovn_u32(a), vmovn_u32(b));
}
#define simd_cnvrt_USHORT_ULONG(a,b) _ip_simd_cnvrt_USHORT_ULONG(a,b)

static inline simd_UBYTE _ip_simd_cnvrt_UBYTE_ULONG(simd_ULONG a, simd_ULONG b, simd_ULONG c, simd_ULONG d)
{
    uint16x8_t l = vcombine_u16(vmovn_u32(a), vmovn_u32(b));
    uint16x8_t h = vcombine_u16(vmovn_u32(c), vmovn_u32(d));
    return vcombine_u8(vmovn_u16(l), vmovn_u16(h));
}
#define simd_cnvrt_UBYTE_ULONG(a,b,c,d) _ip_simd_cnvrt_UBYTE_ULONG(a,b,c,d)

#ifdef __aarch64__
static inline simd_ULONG _ip_simd_cnvrt_ULONG_ULONG64(simd_ULONG64 a, simd_ULONG64 b)
{
    return vcombine_u32(vmovn_u64(a), vmovn_u64(b));
}
#define simd_cnvrt_ULONG_ULONG64(a,b) _ip_simd_cnvrt_ULONG_ULONG64(a,b)
#endif


/* Conversions of BYTE to UBYTE/USHORT/ULONG with clipping */

#define simd_cnvrt_clip_UBYTE_BYTE(a) ((simd_UBYTE)vmaxq_s8(a, vdupq_n_s8(0)))
#define simd_cnvrt_clip_USHORT_BYTE(a) simd_cnvrt_USHORT_UBYTE((simd_UBYTE)vmaxq_s8(a, vdupq_n_s8(0)))
#define simd_cnvrt_clip_ULONG_BYTE(a) simd_cnvrt_ULONG_UBYTE((simd_UBYTE)vmaxq_s8(a, vdupq_n_s8(0)))

/* Conversion of UBYTE to BYTE with clipping */

#define simd_cnvrt_clip_BYTE_UBYTE(a) ((simd_BYTE)vminq_u8(a, vdupq_n_u8(127)))

/* Conversion of SHORT to BYTE/UBYTE/USHORT/ULONG with clipping */

static inline simd_BYTE _ip_simd_cnvrt_clip_BYTE_SHORT(simd_SHORT a, simd_SHORT b)
{
    return vcombine_s8(vqmovn_s16(a), vqmovn_s16(b));
}
#define simd_cnvrt_clip_BYTE_SHORT(a,b) _ip_simd_cnvrt_clip_BYTE_SHORT(a,b)

static inline simd_UBYTE _ip_simd_cnvrt_clip_UBYTE_SHORT(simd_SHORT a, simd_SHORT b)
{
    return vcombine_u8(vqmovun_s16(a), vqmovun_s16(b));
}
#define simd_cnvrt_clip_UBYTE_SHORT(a,b) _ip_simd_cnvrt_clip_UBYTE_SHORT(a,b)

#define simd_cnvrt_clip_USHORT_SHORT(a) ((simd_USHORT)vmaxq_s16(a,vdupq_n_s16(0)))
#define simd_cnvrt_clip_ULONG_SHORT(a) simd_cnvrt_ULONG_USHORT((simd_USHORT)vmaxq_s16(a, vdupq_n_s16(0)))

/* Conversion of USHORT to BYTE/UBYTE/SHORT with clipping */

static inline simd_BYTE _ip_simd_cnvrt_clip_BYTE_USHORT(simd_USHORT a, simd_USHORT b)
{
    const simd_SHORT vzero = vdupq_n_s16(0);
    const simd_SHORT vmax = vdupq_n_s16(127);
    simd_SHORT ta = (simd_SHORT)a;
    simd_SHORT tb = (simd_SHORT)b;
    simd_USHORT sela, selb;

    sela = vcgtq_s16(vzero, ta);
    ta = vbslq_s16(sela, vmax, ta);
    selb = vcgtq_s16(vzero, tb);
    tb = vbslq_s16(selb, vmax, tb);
    return  simd_cnvrt_clip_BYTE_SHORT(ta, tb);
}
#define simd_cnvrt_clip_BYTE_USHORT(a,b)  _ip_simd_cnvrt_clip_BYTE_USHORT(a,b)

static inline simd_UBYTE _ip_simd_cnvrt_clip_UBYTE_USHORT(simd_USHORT a, simd_USHORT b)
{
    return vcombine_u8(vqmovn_u16(a), vqmovn_u16(b));
}
#define simd_cnvrt_clip_UBYTE_USHORT(a,b)  _ip_simd_cnvrt_clip_UBYTE_USHORT(a,b)

#define simd_cnvrt_clip_SHORT_USHORT(a) ((simd_SHORT)vminq_u16(a, vdupq_n_u16(INT16_MAX)))

/* Convert LONG to BYTE/UBYTE/SHORT/USHORT/ULONG with clipping */

static inline simd_BYTE _ip_simd_cnvrt_clip_BYTE_LONG(simd_LONG a, simd_LONG b, simd_LONG c, simd_LONG d)
{
    int16x8_t l = vcombine_s16(vqmovn_s32(a), vqmovn_s32(b));
    int16x8_t h = vcombine_s16(vqmovn_s32(c), vqmovn_s32(d));
    return vcombine_s8(vqmovn_s16(l), vqmovn_s16(h));
}
#define simd_cnvrt_clip_BYTE_LONG(a,b,c,d) _ip_simd_cnvrt_clip_BYTE_LONG(a,b,c,d)

static inline simd_UBYTE _ip_simd_cnvrt_clip_UBYTE_LONG(simd_LONG a, simd_LONG b, simd_LONG c, simd_LONG d)
{
    int16x8_t l = vcombine_s16(vqmovn_s32(a), vqmovn_s32(b));
    int16x8_t h = vcombine_s16(vqmovn_s32(c), vqmovn_s32(d));
    return vcombine_u8(vqmovun_s16(l), vqmovun_s16(h));
}
#define simd_cnvrt_clip_UBYTE_LONG(a,b,c,d) _ip_simd_cnvrt_clip_UBYTE_LONG(a,b,c,d)

static inline simd_SHORT _ip_simd_cnvrt_clip_SHORT_LONG(simd_LONG a, simd_LONG b)
{
    return vcombine_s16(vqmovn_s32(a), vqmovn_s32(b));
}
#define simd_cnvrt_clip_SHORT_LONG(a,b) _ip_simd_cnvrt_clip_SHORT_LONG(a,b)

static inline simd_USHORT _ip_simd_cnvrt_clip_USHORT_LONG(simd_LONG a, simd_LONG b)
{
    return vcombine_u16(vqmovun_s32(a), vqmovun_s32(b));
}
#define simd_cnvrt_clip_USHORT_LONG(a,b) _ip_simd_cnvrt_clip_USHORT_LONG(a,b)

#define simd_cnvrt_clip_ULONG_LONG(a) ((simd_ULONG)vmaxq_s32(a,vdupq_n_s32(0)))

/* Convert ULONG to BYTE/UBYTE/SHORT/USHORT/LONG with clipping */

static inline simd_BYTE _ip_simd_cnvrt_clip_BYTE_ULONG(simd_ULONG a, simd_ULONG b, simd_ULONG c, simd_ULONG d)
{
    const simd_LONG vzero = vdupq_n_s32(0);
    const simd_LONG vmax = vdupq_n_s32(127);
    simd_ULONG sela, selb;
    simd_LONG ta = (simd_LONG)a;
    simd_LONG tb = (simd_LONG)b;
    simd_LONG tc = (simd_LONG)c;
    simd_LONG td = (simd_LONG)d;

    sela = vcgtq_s32(vzero, ta);
    ta = vbslq_s32(sela, vmax, ta);
    selb = vcgtq_s32(vzero, tb);
    tb = vbslq_s32(selb, vmax, tb);
    sela = vcgtq_s32(vzero, tc);
    tc = vbslq_s32(sela, vmax, tc);
    selb = vcgtq_s32(vzero, td);
    td = vbslq_s32(selb, vmax, td);
    return  simd_cnvrt_clip_BYTE_LONG(ta,tb,tc,td);
}
#define simd_cnvrt_clip_BYTE_ULONG(a,b,c,d)  _ip_simd_cnvrt_clip_BYTE_ULONG(a,b,c,d)

static inline simd_UBYTE _ip_simd_cnvrt_clip_UBYTE_ULONG(simd_ULONG a, simd_ULONG b, simd_ULONG c, simd_ULONG d)
{
    uint16x8_t l = vcombine_u16(vqmovn_u32(a), vqmovn_u32(b));
    uint16x8_t h = vcombine_u16(vqmovn_u32(c), vqmovn_u32(d));
    return vcombine_u8(vqmovn_u16(l), vqmovn_u16(h));
}
#define simd_cnvrt_clip_UBYTE_ULONG(a,b,c,d) _ip_simd_cnvrt_clip_UBYTE_ULONG(a,b,c,d)

static inline simd_SHORT _ip_simd_cnvrt_clip_SHORT_ULONG(simd_ULONG a, simd_ULONG b)
{
    const simd_LONG vzero = vdupq_n_s32(0);
    const simd_LONG vmax = vdupq_n_s32(INT16_MAX);
    simd_LONG ta = (simd_LONG)a;
    simd_LONG tb = (simd_LONG)b;
    simd_ULONG sela, selb;

    sela = vcgtq_s32(vzero, ta);
    ta = vbslq_s32(sela, vmax, ta);
    selb = vcgtq_s32(vzero, tb);
    tb = vbslq_s32(selb, vmax, tb);
    return  simd_cnvrt_clip_SHORT_LONG(ta, tb);
}
#define simd_cnvrt_clip_SHORT_ULONG(a,b)  _ip_simd_cnvrt_clip_SHORT_ULONG(a,b)

static inline simd_USHORT _ip_simd_cnvrt_clip_USHORT_ULONG(simd_ULONG a, simd_ULONG b)
{
    return vcombine_u16(vqmovn_u32(a), vqmovn_u32(b));
}
#define simd_cnvrt_clip_USHORT_ULONG(a,b)  _ip_simd_cnvrt_clip_USHORT_ULONG(a,b)

#define simd_cnvrt_clip_LONG_ULONG(a) ((simd_LONG)vminq_u32(a, vdupq_n_u32(INT32_MAX)))

/* Conversion between LONG/ULONG and FLOAT */

#define simd_cnvrt_FLOAT_LONG(a) vcvtq_f32_s32(a)
#define simd_cnvrt_FLOAT_ULONG(a) vcvtq_f32_u32(a)
#define simd_cnvrt_LONG_FLOAT(a) vcvtq_s32_f32(a)
#define simd_cnvrt_ULONG_FLOAT(a) vcvtq_u32_f32(a)

/*
 * Helper function;
 * Add half to each float in the vector in prep for rounding; truncating
 * the result of this function will give a correctly rounded result to x.
 */
static inline simd_FLOAT neon_vhroundf(simd_FLOAT x)
{
    const simd_ULONG signbit = simd_splat_ULONG(0x80000000U);
    simd_FLOAT half = simd_splat_FLOAT(0x1.fffffep-2); /* ohalf = nextafterf(0.5, 0) */
    simd_ULONG sign = vandq_u32((simd_ULONG)x, signbit);
    x = vaddq_f32(x, (simd_FLOAT)vorrq_u32((simd_ULONG)half, sign)); /* x + copysignf(ohalf, x) */
    return x;
}

#ifdef __aarch64__
/* Helper function; add half to each double in the vector in prep for rounding */
static inline simd_DOUBLE neon_vhround(simd_DOUBLE x)
{
    const uint64x2_t signbit = vdupq_n_u64(0x8000000000000000UL);
    simd_DOUBLE half = simd_splat_DOUBLE(0x1.fffffffffffffp-2); /* ohalf = nextafter(0.5, 0) */
    uint64x2_t sign = vandq_u64((uint64x2_t)x, signbit);
    x = vaddq_f64(x, (simd_DOUBLE)vorrq_u64((uint64x2_t)half, sign)); /* x + copysign(ohalf, x) */
    return x;
}
#endif

static inline simd_LONG _ip_simd_cnvrt_rnd_LONG_FLOAT(simd_FLOAT x)
{
    return vcvtq_s32_f32(neon_vhroundf(x));
}
#define simd_cnvrt_rnd_LONG_FLOAT(x) _ip_simd_cnvrt_rnd_LONG_FLOAT(x)

/*
 * Even though this converts to ULONG and on arm the float to uint32_t
 * conversion clips negative values we still manipulate negative float
 * values so that we don't add 0.5 to numbers in the range (-0.5, -0.0] and
 * muck up the conversion of them (see neon_vhroundf()).  A compare for 0
 * and bigger and v_select might be a better option.
 */
static inline simd_ULONG _ip_simd_cnvrt_rnd_ULONG_FLOAT(simd_FLOAT x)
{
    return vcvtq_u32_f32(neon_vhroundf(x));
}
#define simd_cnvrt_rnd_ULONG_FLOAT(x) _ip_simd_cnvrt_rnd_ULONG_FLOAT(x)

#define simd_cnvrt_clip_LONG_FLOAT(x) simd_cnvrt_LONG_FLOAT(x)
#define simd_cnvrt_clip_ULONG_FLOAT(x) simd_cnvrt_ULONG_FLOAT(x)

#define simd_cnvrt_rnd_clip_LONG_FLOAT(x) simd_cnvrt_rnd_LONG_FLOAT(x)
#define simd_cnvrt_rnd_clip_ULONG_FLOAT(x) simd_cnvrt_rnd_ULONG_FLOAT(x)

#ifdef __aarch64__

/* FLOAT/DOUBLE conversions (only on arm64) */

static inline simd_DOUBLE_2 _ip_simd_cnvrt_DOUBLE_FLOAT(simd_FLOAT x)
{
    simd_DOUBLE_2 res;

    res.d[0] = vcvt_f64_f32(vget_low_f32(x));
    res.d[1] = vcvt_high_f64_f32(x);
    return res;
}
#  define simd_cnvrt_DOUBLE_FLOAT(x) _ip_simd_cnvrt_DOUBLE_FLOAT(x)

static inline simd_FLOAT _ip_simd_cnvrt_FLOAT_DOUBLE(simd_DOUBLE a, simd_DOUBLE b)
{
    return vcombine_f32(vcvt_f32_f64(a), vcvt_f32_f64(b));
}
#  define simd_cnvrt_FLOAT_DOUBLE(a, b) _ip_simd_cnvrt_FLOAT_DOUBLE(a, b)

static inline simd_FLOAT _ip_simd_cnvrt_clip_FLOAT_DOUBLE(simd_DOUBLE a, simd_DOUBLE b)
{
    simd_DOUBLE fmax = simd_splat_DOUBLE(FLT_MAX);
    simd_DOUBLE fmin = simd_splat_DOUBLE(-FLT_MAX);
    a = vminq_f64(a, fmax);
    a = vmaxq_f64(a, fmin);
    b = vminq_f64(b, fmax);
    b = vmaxq_f64(b, fmin);
    return vcombine_f32(vcvt_f32_f64(a), vcvt_f32_f64(b));
}
#  define simd_cnvrt_clip_FLOAT_DOUBLE(a, b) _ip_simd_cnvrt_clip_FLOAT_DOUBLE(a, b)

/* (U)LONG / DOUBLE conversions (only on arm64) */

static inline simd_ULONG _ip_simd_cnvrt_ULONG_DOUBLE(simd_DOUBLE a, simd_DOUBLE b)
{
    return vcombine_u32(vmovn_u64((uint64x2_t)vcvtq_s64_f64(a)),
			vmovn_u64((uint64x2_t)vcvtq_s64_f64(b)));
}
#  define simd_cnvrt_ULONG_DOUBLE(a, b) _ip_simd_cnvrt_ULONG_DOUBLE(a, b)

static inline simd_LONG _ip_simd_cnvrt_LONG_DOUBLE(simd_DOUBLE a, simd_DOUBLE b)
{
    return vcombine_s32(vmovn_s64(vcvtq_s64_f64(a)), vmovn_s64(vcvtq_s64_f64(b)));
}
#  define simd_cnvrt_LONG_DOUBLE(a, b) _ip_simd_cnvrt_LONG_DOUBLE(a, b)

static inline int64x2_t neon_rnd_s64_f64(simd_DOUBLE x)
{
    return vcvtq_s64_f64(neon_vhround(x));
}

static inline simd_LONG _ip_simd_cnvrt_rnd_LONG_DOUBLE(simd_DOUBLE a, simd_DOUBLE b)
{
    return vcombine_s32(vmovn_s64(neon_rnd_s64_f64(a)), vmovn_s64(neon_rnd_s64_f64(b)));
}
#  define simd_cnvrt_rnd_LONG_DOUBLE(a, b) _ip_simd_cnvrt_rnd_LONG_DOUBLE(a, b)
#  define simd_cnvrt_rnd_ULONG_DOUBLE(a, b) ((simd_ULONG)_ip_simd_cnvrt_rnd_LONG_DOUBLE(a, b))



static inline simd_DOUBLE_2 _ip_simd_cnvrt_DOUBLE_LONG(simd_LONG a)
{
    simd_DOUBLE_2 res;
    res.d[0] = vcvtq_f64_s64(vmovl_s32(vget_low_s32(a)));
    res.d[1] = vcvtq_f64_s64(vmovl_s32(vget_high_s32(a)));
    return res;
}
#  define simd_cnvrt_DOUBLE_LONG(x) _ip_simd_cnvrt_DOUBLE_LONG(x)

static inline simd_DOUBLE_2 _ip_simd_cnvrt_DOUBLE_ULONG(simd_ULONG a)
{
    simd_DOUBLE_2 res;
    res.d[0] = vcvtq_f64_u64(vmovl_u32(vget_low_u32(a)));
    res.d[1] = vcvtq_f64_u64(vmovl_u32(vget_high_u32(a)));
    return res;
}
#  define simd_cnvrt_DOUBLE_ULONG(x) _ip_simd_cnvrt_DOUBLE_ULONG(x)

/* And a clipping version of (U)LONG from DOUBLE (only on arm64) */

static inline simd_ULONG _ip_simd_cnvrt_clip_ULONG_DOUBLE(simd_DOUBLE a, simd_DOUBLE b)
{
    simd_DOUBLE fmax = simd_splat_DOUBLE((double)UINT32_MAX);
    simd_DOUBLE fmin = simd_splat_DOUBLE(0.0);
    a = vminq_f64(a, fmax);
    a = vmaxq_f64(a, fmin);
    b = vminq_f64(b, fmax);
    b = vmaxq_f64(b, fmin);
    return simd_cnvrt_ULONG_DOUBLE(a, b);
}
#  define simd_cnvrt_clip_ULONG_DOUBLE(a, b) _ip_simd_cnvrt_clip_ULONG_DOUBLE(a, b)

static inline simd_LONG _ip_simd_cnvrt_clip_LONG_DOUBLE(simd_DOUBLE a, simd_DOUBLE b)
{
    simd_DOUBLE fmax = simd_splat_DOUBLE((double)INT32_MAX);
    simd_DOUBLE fmin = simd_splat_DOUBLE((double)INT32_MIN);
    a = vminq_f64(a, fmax);
    a = vmaxq_f64(a, fmin);
    b = vminq_f64(b, fmax);
    b = vmaxq_f64(b, fmin);
    return simd_cnvrt_LONG_DOUBLE(a, b);
}
#define simd_cnvrt_clip_LONG_DOUBLE(a, b) _ip_simd_cnvrt_clip_LONG_DOUBLE(a, b)

static inline simd_ULONG _ip_simd_cnvrt_rnd_clip_ULONG_DOUBLE(simd_DOUBLE a, simd_DOUBLE b)
{
    simd_DOUBLE fmax = simd_splat_DOUBLE((double)UINT32_MAX);
    simd_DOUBLE fmin = simd_splat_DOUBLE(0.0);
    a = vminq_f64(a, fmax);
    a = vmaxq_f64(a, fmin);
    b = vminq_f64(b, fmax);
    b = vmaxq_f64(b, fmin);
    return simd_cnvrt_rnd_ULONG_DOUBLE(a, b);
}
#  define simd_cnvrt_rnd_clip_ULONG_DOUBLE(a, b) _ip_simd_cnvrt_rnd_clip_ULONG_DOUBLE(a, b)

static inline simd_LONG _ip_simd_cnvrt_rnd_clip_LONG_DOUBLE(simd_DOUBLE a, simd_DOUBLE b)
{
    simd_DOUBLE fmax = simd_splat_DOUBLE((double)INT32_MAX);
    simd_DOUBLE fmin = simd_splat_DOUBLE((double)INT32_MIN);
    a = vminq_f64(a, fmax);
    a = vmaxq_f64(a, fmin);
    b = vminq_f64(b, fmax);
    b = vmaxq_f64(b, fmin);
    return simd_cnvrt_rnd_LONG_DOUBLE(a, b);
}
#define simd_cnvrt_rnd_clip_LONG_DOUBLE(a, b) _ip_simd_cnvrt_rnd_clip_LONG_DOUBLE(a, b)

#endif


/* Conversions to/from BINARY */

static inline simd_UBYTE _ip_simd_cnvrt_BINARY_UBYTE(simd_UBYTE x)
{
    const simd_UBYTE vfalse = simd_splat_UBYTE(0);
    return vmvnq_u8(vceqq_u8(x, vfalse));
}
#define simd_cnvrt_BINARY_UBYTE(x) _ip_simd_cnvrt_BINARY_UBYTE(x)

static inline simd_UBYTE _ip_simd_cnvrt_BINARY_USHORT(simd_USHORT a, simd_USHORT b)
{
    const simd_USHORT vfalse = simd_splat_USHORT(0);
    a = vmvnq_u16(vceqq_u16(a, vfalse));
    b = vmvnq_u16(vceqq_u16(b, vfalse));
    return simd_cnvrt_UBYTE_USHORT(a, b);
}
#define simd_cnvrt_BINARY_USHORT(x, y) _ip_simd_cnvrt_BINARY_USHORT(x, y)

static inline simd_UBYTE _ip_simd_cnvrt_BINARY_ULONG(simd_ULONG a, simd_ULONG b,
						     simd_ULONG c, simd_ULONG d)
{
    const simd_ULONG vfalse = simd_splat_ULONG(0);
    a = vmvnq_u32(vceqq_u32(a, vfalse));
    b = vmvnq_u32(vceqq_u32(b, vfalse));
    c = vmvnq_u32(vceqq_u32(c, vfalse));
    d = vmvnq_u32(vceqq_u32(d, vfalse));
    return simd_cnvrt_UBYTE_ULONG(a, b, c, d);
}
#define simd_cnvrt_BINARY_ULONG(x, y, u, v) _ip_simd_cnvrt_BINARY_ULONG(x, y, u, v)



static inline simd_UBYTE _ip_simd_cnvrt_UBYTE_BINARY(simd_UBYTE x, int tval)
{
    const simd_UBYTE vfalse = simd_splat_UBYTE(0);
    simd_UBYTE vtrue = simd_splat_UBYTE(tval);
    return vbslq_u8(x, vtrue, vfalse);
}
#define simd_cnvrt_UBYTE_BINARY(x, tv) _ip_simd_cnvrt_UBYTE_BINARY(x, tv)

static inline simd_USHORT_2 _ip_simd_cnvrt_USHORT_BINARY(simd_UBYTE x, int tval)
{
    const simd_UBYTE vfalse = simd_splat_UBYTE(0);
    simd_UBYTE vtrue = simd_splat_UBYTE(tval);
    x = vbslq_u8(x, vtrue, vfalse);
    return simd_cnvrt_USHORT_UBYTE(x);
}
#define simd_cnvrt_USHORT_BINARY(x, tv) _ip_simd_cnvrt_USHORT_BINARY(x, tv)

static inline simd_ULONG_4 _ip_simd_cnvrt_ULONG_BINARY(simd_UBYTE x, int tval)
{
    const simd_UBYTE vfalse = simd_splat_UBYTE(0);
    simd_UBYTE vtrue = simd_splat_UBYTE(tval);
    x = vbslq_u8(x, vtrue, vfalse);
    return simd_cnvrt_ULONG_UBYTE(x);
}
#define simd_cnvrt_ULONG_BINARY(x, tv) _ip_simd_cnvrt_ULONG_BINARY(x, tv)

/*
 * Colour conversions
 */

#ifdef __aarch64__
/* Repack 3 RGB vectors into 4 RGBA vectors */
static inline simd_UBYTE_4 ip_simd_cnvrt_RGBA_RGB(simd_UBYTE a, simd_UBYTE b, simd_UBYTE c)
{
    simd_UBYTE_4 y;
    const simd_UBYTE sel1 = (simd_UBYTE){ 0,  1,  2,  0,  3,  4,  5,  0,
					  6,  7,  8,  0,  9, 10, 11,  0};
    const simd_UBYTE sel2 = (simd_UBYTE){12, 13, 14,  0, 15, 16, 17,  0,
					 18, 19, 20,  0, 21, 22, 23,  0};
    const simd_UBYTE sel3 = (simd_UBYTE){ 8,  9, 10,  0, 11, 12, 13,  0,
					 14, 15, 16,  0, 17, 18, 19,  0};
    const simd_UBYTE sel4 = (simd_UBYTE){ 4,  5,  6,  0,  7,  8,  9,  0,
					 10, 11, 12,  0, 13, 14, 15,  0};
    const simd_UBYTE set_alpha_255 = (simd_UBYTE){0, 0, 0, 0xff, 0, 0, 0, 0xff,
						  0, 0, 0, 0xff, 0, 0, 0, 0xff};
    y.ub[0] = vorrq_u8(vqtbl1q_u8(a, sel1), set_alpha_255);
    y.ub[1] = vorrq_u8(vqtbl2q_u8((uint8x16x2_t){.val={a, b}}, sel2),
			set_alpha_255);
    y.ub[2] = vorrq_u8(vqtbl2q_u8((uint8x16x2_t){.val={b, c}}, sel3),
			set_alpha_255);
    y.ub[3] = vorrq_u8(vqtbl1q_u8(c, sel4), set_alpha_255);

    return y;
}
#define simd_cnvrt_RGBA_RGB(a,b,c) ip_simd_cnvrt_RGBA_RGB(a,b,c)

/* Repack 4 RGBA vectors into 3 RGB vectors */
static inline simd_UBYTE_3 ip_simd_cnvrt_RGB_RGBA(simd_UBYTE a, simd_UBYTE b,
						  simd_UBYTE c, simd_UBYTE d)
{
    simd_UBYTE_3 y;
    const simd_UBYTE sel1 = (simd_UBYTE){ 0,  1,  2,  4,  5,  6,  8,  9,
					 10, 12, 13, 14, 16, 17, 18, 20};
    const simd_UBYTE sel2 = (simd_UBYTE){ 5,  6,  8,  9, 10, 12, 13, 14,
					 16, 17, 18, 20, 21, 22, 24, 25};
    const simd_UBYTE sel3 = (simd_UBYTE){10, 12, 13, 14, 16, 17, 18, 20,
					 21, 22, 24, 25, 26, 28, 29, 30};

    y.ub[0] = vqtbl2q_u8((uint8x16x2_t){.val={a, b}}, sel1);
    y.ub[1] = vqtbl2q_u8((uint8x16x2_t){.val={b, c}}, sel2);
    y.ub[2] = vqtbl2q_u8((uint8x16x2_t){.val={c, d}}, sel3);

    return y;
}
#define simd_cnvrt_RGB_RGBA(a,b,c,d) ip_simd_cnvrt_RGB_RGBA(a,b,c,d)
#endif

static inline simd_UBYTE ip_simd_cnvrt_RGB_RGB16(simd_USHORT x, simd_USHORT y)
{
    return simd_cnvrt_UBYTE_USHORT(vshrq_n_u16(x, 8),
				   vshrq_n_u16(y, 8));
}
#define simd_cnvrt_RGB_RGB16(x,y) ip_simd_cnvrt_RGB_RGB16(x,y)

static inline simd_USHORT_2 ip_simd_cnvrt_RGB16_RGB(simd_UBYTE x)
{
    simd_USHORT_2 r16 = simd_cnvrt_USHORT_UBYTE(x);
    r16.us[0] = vorrq_u16(r16.us[0], vshlq_n_u16(r16.us[0], 8));
    r16.us[1] = vorrq_u16(r16.us[1], vshlq_n_u16(r16.us[1], 8));
    return r16;
}
#define simd_cnvrt_RGB16_RGB(x) ip_simd_cnvrt_RGB16_RGB(x)


/* Logical operators on SIMD vectors */

#define simd_or_UBYTE(a, b) vorrq_u8(a, b)
#define simd_or_BYTE(a, b) vorrq_s8(a, b)
#define simd_or_USHORT(a, b) vorrq_u16(a, b)
#define simd_or_SHORT(a, b) vorrq_s16(a, b)
#define simd_or_ULONG(a, b) vorrq_u32(a, b)
#define simd_or_LONG(a, b) vorrq_s32(a, b)

#define simd_xor_UBYTE(a, b) veorq_u8(a, b)
#define simd_xor_BYTE(a, b) veorq_s8(a, b)
#define simd_xor_USHORT(a, b) veorq_u16(a, b)
#define simd_xor_SHORT(a, b) veorq_s16(a, b)
#define simd_xor_ULONG(a, b) veorq_u32(a, b)
#define simd_xor_LONG(a, b) veorq_s32(a, b)

#define simd_and_UBYTE(a, b) vandq_u8(a, b)
#define simd_and_BYTE(a, b) vandq_s8(a, b)
#define simd_and_USHORT(a, b) vandq_u16(a, b)
#define simd_and_SHORT(a, b) vandq_s16(a, b)
#define simd_and_ULONG(a, b) vandq_u32(a, b)
#define simd_and_LONG(a, b) vandq_s32(a, b)

#define simd_not_UBYTE(a) vmvnq_u8(a)
#define simd_not_BYTE(a) vmvnq_s8(a)
#define simd_not_USHORT(a) vmvnq_u16(a)
#define simd_not_SHORT(a) vmvnq_s16(a)
#define simd_not_ULONG(a) vmvnq_u32(a)
#define simd_not_LONG(a) vmvnq_s32(a)

/*
 * Shifts
 */

#define simd_shl_BYTE(a, s) vshlq_s8(a, s)
#define simd_shl_UBYTE(a, s) vshlq_u8(a, (int8x16_t)s)
#define simd_shl_SHORT(a, s) vshlq_s16(a, s)
#define simd_shl_USHORT(a, s) vshlq_u16(a, (int16x8_t)s)
#define simd_shl_LONG(a, s) vshlq_s32(a, s)
#define simd_shl_ULONG(a, s) vshlq_u32(a, (int32x4_t)s)

#define simd_shl_imm_BYTE(x,c) vshlq_n_s8(x,c)
#define simd_shl_imm_UBYTE(x,c) vshlq_n_u8(x,c)
#define simd_shl_imm_SHORT(x,c) vshlq_n_s16(x,c)
#define simd_shl_imm_USHORT(x,c) vshlq_n_u16(x,c)
#define simd_shl_imm_LONG(x,c) vshlq_n_s32(x,c)
#define simd_shl_imm_ULONG(x,c) vshlq_n_u32(x,c)

#define simd_shr_imm_BYTE(x,c) ((simd_BYTE)vshrq_n_u8((simd_UBYTE)x,c))
#define simd_shr_imm_UBYTE(x,c) vshrq_n_u8(x,c)
#define simd_shr_imm_SHORT(x,c) vshrq_n_u16(x,c)
#define simd_shr_imm_USHORT(x,c) vshrq_n_u16(x,c)
#define simd_shr_imm_LONG(x,c) vshrq_n_u32(x,c)
#define simd_shr_imm_ULONG(x,c) vshrq_n_u32(x,c)

#define simd_shra_imm_BYTE(x,c) vshrq_n_s8(x,c)
#define simd_shra_imm_UBYTE(x,c) vshrq_n_s8(x,c)
#define simd_shra_imm_SHORT(x,c) vshrq_n_s16(x,c)
#define simd_shra_imm_USHORT(x,c) vshrq_n_s16(x,c)
#define simd_shra_imm_LONG(x,c) vshrq_n_s32(x,c)
#define simd_shra_imm_ULONG(x,c) vshrq_n_s32(x,c)


/* Basic arithmetic operators */

#define simd_add_BYTE(d,s) vaddq_s8(d,s)
#define simd_add_UBYTE(d,s) vaddq_u8(d,s)
#define simd_add_SHORT(d,s) vaddq_s16(d,s)
#define simd_add_USHORT(d,s) vaddq_u16(d,s)
#define simd_add_LONG(d,s) vaddq_s32(d,s)
#define simd_add_ULONG(d,s) vaddq_u32(d,s)
#define simd_add_FLOAT(d,s) vaddq_f32(d,s)

#define simd_add_clipped_BYTE(d,s) vqaddq_s8(d,s)
#define simd_add_clipped_UBYTE(d,s) vqaddq_u8(d,s)
#define simd_add_clipped_SHORT(d,s) vqaddq_s16(d,s)
#define simd_add_clipped_USHORT(d,s) vqaddq_u16(d,s)

#define simd_sub_BYTE(d,s) vsubq_s8(d,s)
#define simd_sub_UBYTE(d,s) vsubq_u8(d,s)
#define simd_sub_SHORT(d,s) vsubq_s16(d,s)
#define simd_sub_USHORT(d,s) vsubq_u16(d,s)
#define simd_sub_LONG(d,s) vsubq_s32(d,s)
#define simd_sub_ULONG(d,s) vsubq_u32(d,s)
#define simd_sub_FLOAT(d,s) vsubq_f32(d,s)

#define simd_sub_clipped_BYTE(d,s) vqsubq_s8(d,s)
#define simd_sub_clipped_UBYTE(d,s) vqsubq_u8(d,s)
#define simd_sub_clipped_SHORT(d,s) vqsubq_s16(d,s)
#define simd_sub_clipped_USHORT(d,s) vqsubq_u16(d,s)

#define simd_mul_BYTE(d,s) vmulq_s8(d,s)
#define simd_mul_UBYTE(d,s) vmulq_u8(d,s)
#define simd_mul_SHORT(d,s) vmulq_s16(d,s)
#define simd_mul_USHORT(d,s) vmulq_u16(d,s)
#define simd_mul_LONG(d,s) vmulq_s32(d,s)
#define simd_mul_ULONG(d,s) vmulq_u32(d,s)
#define simd_mul_FLOAT(d,s) vmulq_f32(d,s)



#ifdef __aarch64__
#define simd_add_DOUBLE(d,s) vaddq_f64(d,s)
#define simd_sub_DOUBLE(d,s) vsubq_f64(d,s)
#define simd_mul_DOUBLE(d,s) vmulq_f64(d,s)
#endif

/* multiplication with clipping */

static inline simd_BYTE _ip_simd_mul_clipped_BYTE(simd_BYTE a, simd_BYTE b)
{
    const simd_SHORT byte_max = simd_splat_SHORT(127);
    const simd_SHORT byte_min = simd_splat_SHORT(-128);
    simd_SHORT lo = vmull_s8(vget_low_s8(a), vget_low_s8(b));
    simd_SHORT hi = vmull_s8(vget_high_s8(a), vget_high_s8(b));
    lo = vminq_s16(lo, byte_max);
    hi = vminq_s16(hi, byte_max);
    lo = vmaxq_s16(lo, byte_min);
    hi = vmaxq_s16(hi, byte_min);
    return (simd_BYTE)simd_cnvrt_UBYTE_USHORT((simd_USHORT)lo, (simd_USHORT)hi);
}
#define simd_mul_clipped_BYTE(d, s) _ip_simd_mul_clipped_BYTE(d, s)

static inline simd_UBYTE _ip_simd_mul_clipped_UBYTE(simd_UBYTE a, simd_UBYTE b)
{
    const simd_USHORT ubyte_max = simd_splat_USHORT(0xff);
    simd_USHORT lo = vmull_u8(vget_low_u8(a), vget_low_u8(b));
    simd_USHORT hi = vmull_u8(vget_high_u8(a), vget_high_u8(b));
    lo = vminq_u16(lo, ubyte_max);
    hi = vminq_u16(hi, ubyte_max);
    return simd_cnvrt_UBYTE_USHORT(lo, hi);
}
#define simd_mul_clipped_UBYTE(d, s) _ip_simd_mul_clipped_UBYTE(d, s)

static inline simd_SHORT _ip_simd_mul_clipped_SHORT(simd_SHORT a, simd_SHORT b)
{
    const simd_LONG short_max = simd_splat_LONG(INT16_MAX);
    const simd_LONG short_min = simd_splat_LONG(INT16_MIN);
    simd_LONG lo = vmull_s16(vget_low_s16(a), vget_low_s16(b));
    simd_LONG hi = vmull_s16(vget_high_s16(a), vget_high_s16(b));
    lo = vminq_s32(lo, short_max);
    hi = vminq_s32(hi, short_max);
    lo = vmaxq_s32(lo, short_min);
    hi = vmaxq_s32(hi, short_min);
    return (simd_SHORT)simd_cnvrt_USHORT_ULONG((simd_ULONG)lo, (simd_ULONG)hi);
}
#define simd_mul_clipped_SHORT(d, s) _ip_simd_mul_clipped_SHORT(d, s)

static inline simd_USHORT _ip_simd_mul_clipped_USHORT(simd_USHORT a, simd_USHORT b)
{
    const simd_ULONG ushort_max = simd_splat_ULONG(UINT16_MAX);
    simd_ULONG lo = vmull_u16(vget_low_u16(a), vget_low_u16(b));
    simd_ULONG hi = vmull_u16(vget_high_u16(a), vget_high_u16(b));
    lo = vminq_u32(lo, ushort_max);
    hi = vminq_u32(hi, ushort_max);
    return simd_cnvrt_USHORT_ULONG(lo, hi);
}
#define simd_mul_clipped_USHORT(d, s) _ip_simd_mul_clipped_USHORT(d, s)


/*
 * Complex multiplication
 *
 * simd_mul - Multiply two complex vectors with interleaved
 *	      real/imaginary components.
 *
 * or the below may be useful when the complex multiplicand is constant thus
 * part of the processing can be performed outside the loop.  Unfortunately
 * they don't seem to give a speed up on Arm64 for reasons I don't
 * understand thus simd_mulcsc is commented out.
 *
 * simd_real - vector of doubled up real components of z.
 * simd_imag - vector of doubled up imaginary components of z.
 * simd_nimag - vector of doubled up imaginary components of z with the even
 *		components negated.
 * simd_mulcsc - multiply complex vector z by a vector whose compenents
 *		separated out into real and imaginary (with the even
 *		components negated).
 */

#ifdef __aarch64__
static inline simd_FLOAT _ip_simd_real_COMPLEX(simd_COMPLEX z)
{
    return vtrn1q_f32(z, z);
}
#define simd_real_COMPLEX(x) _ip_simd_real_COMPLEX(x)

static inline simd_FLOAT _ip_simd_imag_COMPLEX(simd_COMPLEX z)
{
    return vtrn2q_f32(z, z);
}
#define simd_imag_COMPLEX(x) _ip_simd_imag_COMPLEX(x)

static inline simd_FLOAT _ip_simd_nimag_COMPLEX(simd_COMPLEX z)
{
    return vtrn2q_f32(z, vnegq_f32(z));
}
#define simd_nimag_COMPLEX(x) _ip_simd_nimag_COMPLEX(x)

static inline simd_COMPLEX _ip_simd_mulcsc_COMPLEX(simd_COMPLEX z, simd_FLOAT r, simd_FLOAT i)
{
    simd_COMPLEX t1 = simd_mul_FLOAT(z, r);
    simd_COMPLEX t2 = simd_mul_FLOAT(z, i);
    return simd_add_FLOAT(t1, vrev64q_f32(t2));
}
/* #define simd_mulcsc_COMPLEX(a,b,c) _ip_simd_mulcsc_COMPLEX(a,b,c) */

static inline simd_DOUBLE _ip_simd_real_DCOMPLEX(simd_DCOMPLEX z)
{
    return vtrn1q_f64(z, z);
}
#define simd_real_DCOMPLEX(x) _ip_simd_real_DCOMPLEX(x)

static inline simd_DOUBLE _ip_simd_imag_DCOMPLEX(simd_DCOMPLEX z)
{
    return vtrn2q_f64(z, z);
}
#define simd_imag_DCOMPLEX(x) _ip_simd_imag_DCOMPLEX(x)

static inline simd_DOUBLE _ip_simd_nimag_DCOMPLEX(simd_DCOMPLEX z)
{
    return vtrn2q_f64(z, vnegq_f64(z));
}
#define simd_nimag_DCOMPLEX(x) _ip_simd_nimag_DCOMPLEX(x)

static inline simd_DCOMPLEX _ip_simd_mulcsc_DCOMPLEX(simd_DCOMPLEX z, simd_DOUBLE r, simd_DOUBLE i)
{
    simd_DCOMPLEX t1 = simd_mul_DOUBLE(z, r);
    simd_DCOMPLEX t2 = simd_mul_DOUBLE(z, i);
    return simd_add_DOUBLE(t1, vextq_f64(t2, t2, 1));
}
/* #define simd_mulcsc_DCOMPLEX(a,b,c) _ip_simd_mulcsc_DCOMPLEX(a,b,c) */
#endif


static inline simd_COMPLEX _ip_simd_mul_COMPLEX(simd_COMPLEX x, simd_COMPLEX y)
{
    const simd_FLOAT ineg = (simd_FLOAT){0.0F, -0.0F, 0.0F, -0.0F};
    simd_FLOAT t1 = vmulq_f32(x, (simd_FLOAT)veorq_u32((uint32x4_t)y, (uint32x4_t)ineg));
    simd_FLOAT t2 = vmulq_f32(x, vrev64q_f32(y));
    float32x4x2_t t3 = vtrnq_f32(t1, t2);
    return vaddq_f32(t3.val[0], t3.val[1]);
}
#define simd_mul_COMPLEX(a,b) _ip_simd_mul_COMPLEX(a, b)

#ifdef __aarch64__
static inline simd_DCOMPLEX _ip_simd_mul_DCOMPLEX(simd_DCOMPLEX x, simd_DCOMPLEX y)
{
    const simd_DOUBLE ineg = (simd_DOUBLE){0.0, -0.0};
    simd_DOUBLE t1 = vmulq_f64(x, (simd_DOUBLE)veorq_u32((uint32x4_t)y, (uint32x4_t)ineg));
    simd_DOUBLE y_swap;
    simd_DOUBLE t2, t3;

    y_swap = vextq_f64(y, y, 1);
    t2 = vmulq_f64(x, y_swap);
    t3 = vtrn1q_f64(t1, t2);
    t1 = vtrn2q_f64(t1, t2);
    return vaddq_f64(t3, t1);
}
#define simd_mul_DCOMPLEX(a, b) _ip_simd_mul_DCOMPLEX(a, b)
#endif


/*
 * simd_mulhi
 *
 * Return upper N-bits of N-bit by N-bit multiplication.
 */

#define simd_mulhi_SHORT(a, b) vshrq_n_s16(vqdmulhq_s16(a, b), 1)
#define simd_mulhi_LONG(a, b) vshrq_n_s32(vqdmulhq_s32(a, b), 1)

static inline simd_USHORT _ip_simd_mulhi_USHORT(simd_USHORT a, simd_USHORT b)
{
    simd_ULONG mul1, mul2;

    mul1 = vmull_u16(vget_low_u16(a), vget_low_u16(b));
    mul2 = vmull_u16(vget_high_u16(a), vget_high_u16(b));
    return simd_cnvrt_USHORT_ULONG(vshrq_n_u32(mul1, 16), vshrq_n_u32(mul2, 16));
}
#define simd_mulhi_USHORT(a, b) _ip_simd_mulhi_USHORT(a, b)

static inline simd_ULONG _ip_simd_mulhi_ULONG(simd_ULONG a, simd_ULONG b)
{
    uint64x2_t mul1, mul2;

    mul1 = vmull_u32(vget_low_u32(a), vget_low_u32(b));
    mul2 = vmull_u32(vget_high_u32(a), vget_high_u32(b));
    return vcombine_u32(vmovn_u64(vshrq_n_u64(mul1, 32)), vmovn_u64(vshrq_n_u64(mul2, 32)));
}
#define simd_mulhi_ULONG(a, b) _ip_simd_mulhi_ULONG(a, b)


/*
 * simd_fmaddhi
 *
 * Integer fused multiply-add returning high N bits.
 */

static inline simd_USHORT _ip_simd_fmaddhi_USHORT(simd_USHORT a, simd_USHORT b, simd_USHORT c)
{
    uint32x4_t rh, rl;

    rl = vmovl_u16(vget_low_u16(c));
    rh = vmovl_u16(vget_high_u16(c));
    rl = vmlal_u16(rl, vget_low_u16(a), vget_low_u16(b));
    rh = vmlal_u16(rh, vget_high_u16(a), vget_high_u16(b));
    return vcombine_u16(vmovn_u32(vshrq_n_u32(rl,16)), vmovn_u32(vshrq_n_u32(rh,16)));
}
#define simd_fmaddhi_USHORT(a,b,c) _ip_simd_fmaddhi_USHORT(a,b,c)

static inline simd_ULONG _ip_simd_fmaddhi_ULONG(simd_ULONG a, simd_ULONG b, simd_ULONG c)
{
    uint64x2_t rh, rl;

    rl = vmovl_u32(vget_low_u32(c));
    rh = vmovl_u32(vget_high_u32(c));
    rl = vmlal_u32(rl, vget_low_u32(a), vget_low_u32(b));
    rh = vmlal_u32(rh, vget_high_u32(a), vget_high_u32(b));
    return vcombine_u32(vmovn_u64(vshrq_n_u64(rl,32)), vmovn_u64(vshrq_n_u64(rh,32)));
}
#define simd_fmaddhi_ULONG(a,b,c) _ip_simd_fmaddhi_ULONG(a,b,c)


/*
 * simd_imulfN
 *
 * Fixed-point integer multiplication where N=32 for 32/32 fixed-point and
 * N=16 for 16/16 fixed-point.
 *
 * Calculates round(a*c) for integer multiplicand a and fixed-point
 * multiplier c = cw + cf, with cw = floor(c) and cf = 2^N(c - cw). Intended
 * for use when c >= 0.5.  If c < 0.5 consider using the idivfN functions
 * below as they better preserve precision of the fraction.
 */

static inline simd_USHORT _ip_simd_imulf16_USHORT(simd_USHORT a, simd_USHORT cw, simd_USHORT cf)
{
    const simd_ULONG half = vdupq_n_u32(1<<15);
    simd_ULONG x1, x2;

    /* Multiply a by the fraction cf and round to nearest integer */
    x1 = vmlal_u16(half, vget_low_u16(a), vget_low_u16(cf));
    x2 = vmlal_u16(half, vget_high_u16(a), vget_high_u16(cf));

    cf =  vcombine_u16(vshrn_n_u32(x1, 16), vshrn_n_u32(x2, 16));
    /* Multiply a by the whole number part cw */
    cw = vmulq_u16(a, cw);
    /* Add two parts together for final result */
    return simd_add_USHORT(cf, cw);
}
#define simd_imulf16_USHORT(a, b, c) _ip_simd_imulf16_USHORT(a, b, c)

static inline simd_SHORT _ip_simd_imulf16_SHORT(simd_SHORT a, simd_SHORT cw, simd_USHORT cf)
{
    const simd_SHORT zero = vdupq_n_s16(0);
    simd_SHORT rounder = vdupq_n_s16(1<<15);
    simd_SHORT negc, sign;
    simd_LONG x1, x2;
    simd_SHORT res;
    int16x8x2_t r;

    /* rounder = (result < 0) ? (half-1) : half */
    sign = (simd_SHORT)simd_xor_USHORT(vcgtq_s16(zero, cw), vcgtq_s16(zero, a));
    rounder = simd_add_SHORT(rounder, sign);

    /* Low 16-bits of 32-bit signed a * cw */
    cw = vmulq_s16(a, cw);

    /* signed correction to multiply:  negc = (a < 0) ? -cf : 0 */
    negc = simd_and_SHORT(simd_sub_SHORT(zero, (simd_SHORT)cf),
			  (simd_SHORT)vcgtq_s16(zero, a));

    r = vzipq_s16(rounder, negc);

    /* Compute 32-bit result of unsigned a * cf */
    x1 = (simd_LONG)vmlal_u16((simd_ULONG)r.val[0], (uint16x4_t)vget_low_s16(a), vget_low_u16(cf));
    x2 = (simd_LONG)vmlal_u16((simd_ULONG)r.val[1], (uint16x4_t)vget_high_s16(a), vget_high_u16(cf));

    /* Pick off the top 16-bits of a*cf and re-pack */
    res =  vcombine_s16(vshrn_n_s32(x1, 16), vshrn_n_s32(x2, 16));

    /* And back into main result */
    res = simd_add_SHORT(cw, res);

    return res;
}
#define simd_imulf16_SHORT(a, b, c) _ip_simd_imulf16_SHORT(a, b, c)

static inline simd_ULONG _ip_simd_imulf32_ULONG(simd_ULONG a, simd_ULONG cw, simd_ULONG cf)
{
    const uint64x2_t half = vdupq_n_u64(1ULL<<31);
    uint64x2_t x1, x2;

    /* Multiply a by the fraction cf and round to nearest integer */
    x1 = vmlal_u32(half, vget_low_u32(a), vget_low_u32(cf));
    x2 = vmlal_u32(half, vget_high_u32(a), vget_high_u32(cf));

    cf =  vcombine_u32(vshrn_n_u64(x1, 32), vshrn_n_u64(x2, 32));
    /* Multiply a by the whole number part cw */
    cw = vmulq_u32(a, cw);
    /* Add two parts together for final result */
    return simd_add_ULONG(cf, cw);
}
#define simd_imulf32_ULONG(a, b, c) _ip_simd_imulf32_ULONG(a, b, c)

static inline simd_LONG _ip_simd_imulf32_LONG(simd_LONG a, simd_LONG cw, simd_ULONG cf)
{
    const simd_LONG zero = vdupq_n_s32(0);
    simd_LONG rounder = vdupq_n_s32(1<<31);
    simd_LONG negc, sign;
    int64x2_t x1, x2;
    simd_LONG res;
    int32x4x2_t r;

    /* rounder = (result < 0) ? (half-1) : half */
    sign = (simd_LONG)simd_xor_ULONG(vcgtq_s32(zero, cw), vcgtq_s32(zero, a));
    rounder = simd_add_LONG(rounder, sign);

    /* Low 32-bits of 64-bit signed a * cw */
    cw = vmulq_s32(a, cw);

    /* signed correction to multiply:  negc = (a < 0) ? -cf : 0 */
    negc = simd_and_LONG(simd_sub_LONG(zero, (simd_LONG)cf),
			  (simd_LONG)vcgtq_s32(zero, a));

    r = vzipq_s32(rounder, negc);

    /* Compute 64-bit result of unsigned a * cf */
    x1 = (int64x2_t)vmlal_u32((uint64x2_t)r.val[0], (uint32x2_t)vget_low_s32(a), vget_low_u32(cf));
    x2 = (int64x2_t)vmlal_u32((uint64x2_t)r.val[1], (uint32x2_t)vget_high_s32(a), vget_high_u32(cf));

    /* Pick off the top 32-bits of a*cf and re-pack */
    res =  vcombine_s32(vshrn_n_s64(x1, 32), vshrn_n_s64(x2, 32));

    /* And back into main result */
    res = simd_add_LONG(cw, res);

    return res;
}
#define simd_imulf32_LONG(a, b, c) _ip_simd_imulf32_LONG(a, b, c)


/*
 * simd_idiv
 *
 * Integer division by a constant (i.e. reused) divisor
 *
 * We provide simd_rcpc functions to calculate an integer reciprocal of a
 * scalar divisor, and simd_idiv functions to calculate the integer
 * division using the reciprocal from the simd_rcpc functions.
 *
 * The IP library only expects these at (U)SHORT and (U)LONG precision.
 */

static inline struct simd_rcp_USHORT simd_rcpc_USHORT(unsigned int d)
{
    struct ip_uint16_rcp_n17 r = ip_urcp16_n17(d);
    struct simd_rcp_USHORT rcp;
    rcp.m = simd_splat_USHORT(r.m);
    rcp.b = simd_splat_USHORT(r.b);
    /* The negation is to effect a right shift in idiv when using vshlq */
    rcp.s = (simd_USHORT)simd_splat_SHORT(-r.shift);
    return rcp;
}

static inline simd_USHORT _ip_simd_idiv_USHORT(simd_USHORT n,
					       struct simd_rcp_USHORT r)
{
    return vshlq_u16(simd_fmaddhi_USHORT(n, r.m, r.b), (simd_SHORT)r.s);
}
#define simd_idiv_USHORT(n, r) _ip_simd_idiv_USHORT(n, r)


static inline struct simd_rcp_SHORT simd_rcpc_SHORT(int d)
{
    struct ip_int16_rcp r = ip_ircp16(d);
    struct simd_rcp_SHORT rcp;
    rcp.m = simd_splat_SHORT(r.m);
    rcp.b = simd_splat_SHORT(r.dsign);
    /* The negation is to effect a right shift in idiv when using vshlq */
    rcp.s = (simd_SHORT)simd_splat_SHORT(-r.shift);
    return rcp;
}

static inline simd_SHORT _ip_simd_idiv_SHORT(simd_SHORT n,
					       struct simd_rcp_SHORT r)
{
    simd_SHORT q0;
    q0 = simd_add_SHORT(n, simd_mulhi_SHORT(n, r.m));
    q0 = vshlq_s16(q0, r.s);
    q0 = simd_sub_SHORT(q0,  vshrq_n_s16(n, 15));
    return simd_sub_SHORT(simd_xor_SHORT(q0, r.b), r.b);
}
#define simd_idiv_SHORT(n, r) _ip_simd_idiv_SHORT((n), (r))

static inline struct simd_rcp_ULONG simd_rcpc_ULONG(unsigned int d)
{
    struct ip_uint32_rcp_n33 r = ip_urcp32_n33(d);
    struct simd_rcp_ULONG rcp;
    rcp.m = simd_splat_ULONG(r.m);
    rcp.b = simd_splat_ULONG(r.b);
    /* The negation is to effect a right shift in idiv when using vshlq */
    rcp.s = (simd_ULONG)simd_splat_LONG(-r.shift);
    return rcp;
}

static inline simd_ULONG _ip_simd_idiv_ULONG(simd_ULONG n,
					       struct simd_rcp_ULONG r)
{
    return vshlq_u32(simd_fmaddhi_ULONG(n, r.m, r.b), (simd_LONG)r.s);
}
#define simd_idiv_ULONG(n, r) _ip_simd_idiv_ULONG(n, r)

static inline struct simd_rcp_LONG simd_rcpc_LONG(int d)
{
    struct ip_int32_rcp r = ip_ircp32(d);
    struct simd_rcp_LONG rcp;
    rcp.m = simd_splat_LONG(r.m);
    rcp.b = simd_splat_LONG(r.dsign);
    rcp.s = simd_splat_LONG(-r.shift);
    return rcp;
}

static inline simd_LONG _ip_simd_idiv_LONG(simd_LONG n,
					   struct simd_rcp_LONG r)
{
    /* Calculate the integer division by multiplication by reciprocal */
    simd_LONG q0;
    q0 = simd_add_LONG(n, simd_mulhi_LONG(n, r.m));
    q0 = vshlq_s32(q0, r.s);
    q0 = simd_sub_LONG(q0,  vshrq_n_s32(n, 31));
    return simd_sub_LONG(simd_xor_LONG(q0, r.b), r.b);
}
#define simd_idiv_LONG(n, r) _ip_simd_idiv_LONG(n, r)

/*
 * idivfN
 *
 * Integer division with conventional rounding.  These are implemented as a
 * multiply with the reciprocal.  These functions, equivalently, can be
 * thought of as a multiple by c for c < 0.5 such that cf = 2^(N+l)*c where
 * l is the shift required to shift c so that c's first 1 bit is in the MSB
 * of cf (not counting the sign bit), thus we always have an N-bit (or N-1
 * bit if signed) fraction in cf.
 */

static inline simd_USHORT
_ip_simd_idivf16_USHORT(simd_USHORT a, simd_USHORT cf, simd_USHORT shift)
{
    const simd_ULONG half = vdupq_n_u32(1<<15);
    simd_ULONG x1, x2;
    simd_LONG sh, sl;

    sl = (simd_LONG)vmovl_u16(vget_low_u16(shift));
    sh = (simd_LONG)vmovl_u16(vget_high_u16(shift));

    x1 = vmlal_u16(vshlq_u32(half, sl), vget_low_u16(a), vget_low_u16(cf));
    x2 = vmlal_u16(vshlq_u32(half, sh), vget_high_u16(a), vget_high_u16(cf));

    x1 = vshlq_u32(x1, vnegq_s32(sl));
    x2 = vshlq_u32(x2, vnegq_s32(sh));

    return vcombine_u16(vshrn_n_u32(x1, 16), vshrn_n_u32(x2, 16));
}
#define simd_idivf16_USHORT(a, b, s) _ip_simd_idivf16_USHORT(a, b, s)

static inline simd_SHORT _ip_simd_idivf16_SHORT(simd_SHORT a,
                                                simd_SHORT cf, simd_USHORT shift)
{
    const simd_LONG zero = vdupq_n_s32(0);
    simd_LONG rounder = vdupq_n_s32(1<<15);
    simd_LONG x1, x2, rh, rl, sh, sl;
    simd_SHORT res;

    sl = (simd_LONG)vmovl_u16(vget_low_u16(shift));
    sh = (simd_LONG)vmovl_u16(vget_high_u16(shift));

    /* Compute 32-bit result of signed a * cf */
    x1 = vmull_s16(vget_low_s16(a), vget_low_s16(cf));
    x2 = vmull_s16(vget_high_s16(a), vget_high_s16(cf));

    /* Let half = (2^15)<<shift, then rounder = (a*cf < 0) ? (half-1) : half */
    rl = simd_add_LONG(vshlq_s32(rounder, sl), (simd_LONG)vcgtq_s32(zero, x1));
    rh = simd_add_LONG(vshlq_s32(rounder, sh), (simd_LONG)vcgtq_s32(zero, x2));

    /* Add rounder into 32-bit result */
    x1 = simd_add_LONG(x1, rl);
    x2 = simd_add_LONG(x2, rh);

    /* Shift back to get result */
    x1 = vshlq_s32(x1, vnegq_s32(sl));
    x2 = vshlq_s32(x2, vnegq_s32(sh));

    /* Pick off the top 16-bits of a*cf and re-pack */
    res = vcombine_s16(vshrn_n_s32(x1, 16), vshrn_n_s32(x2, 16));

    return res;
}
#define simd_idivf16_SHORT(a, b, s) _ip_simd_idivf16_SHORT(a, b, s)

static inline simd_ULONG
_ip_simd_idivf32_ULONG(simd_ULONG a, simd_ULONG cf, simd_ULONG shift)
{
    const uint64x2_t half = vdupq_n_u64(1ULL<<31);
    uint64x2_t x1, x2;
    int64x2_t sh, sl;

    sl = (int64x2_t)vmovl_u32(vget_low_u32(shift));
    sh = (int64x2_t)vmovl_u32(vget_high_u32(shift));

    x1 = vmlal_u32(vshlq_u64(half, sl), vget_low_u32(a), vget_low_u32(cf));
    x2 = vmlal_u32(vshlq_u64(half, sh), vget_high_u32(a), vget_high_u32(cf));

    /*
     * There is no negate 64-bit instruction but the shift reads the least
     * significant byte of the shift operand only so we can get away with
     * negating 32-bit ints!
     */
    x1 = vshlq_u64(x1, (int64x2_t)vnegq_s32((int32x4_t)sl));
    x2 = vshlq_u64(x2, (int64x2_t)vnegq_s32((int32x4_t)sh));

    return vcombine_u32(vshrn_n_u64(x1, 32), vshrn_n_u64(x2, 32));
}
#define simd_idivf32_ULONG(a, b, s) _ip_simd_idivf32_ULONG(a, b, s)

static inline simd_LONG _ip_simd_idivf32_LONG(simd_LONG a,
					      simd_LONG cf, simd_ULONG shift)
{
    const simd_LONG zero = vdupq_n_s32(0);
    int64x2_t rounder = vdupq_n_s64(1LL<<31);
    int64x2_t x1, x2, rh, rl, sh, sl;
    simd_LONG sign, res;

    sl = (int64x2_t)vmovl_u32(vget_low_u32(shift));
    sh = (int64x2_t)vmovl_u32(vget_high_u32(shift));

    sign = veorq_s32((simd_LONG)vcgtq_s32(zero, a), (simd_LONG)vcgtq_s32(zero, cf));

    /* Compute 64-bit result of signed a * cf */
    x1 = vmull_s32(vget_low_s32(a), vget_low_s32(cf));
    x2 = vmull_s32(vget_high_s32(a), vget_high_s32(cf));

    /* Let half = (2^32)<<shift, then rounder = (a*cf < 0) ? (half-1) : half */
    rl = vaddq_s64(vshlq_s64(rounder, sl), vmovl_s32(vget_low_s32(sign)));
    rh = vaddq_s64(vshlq_s64(rounder, sh), vmovl_s32(vget_high_s32(sign)));

    /* Add rounder into 64-bit result */
    x1 = vaddq_s64(x1, rl);
    x2 = vaddq_s64(x2, rh);

    /* Shift back to get result */
    x1 = vshlq_s64(x1, (int64x2_t)vnegq_s32((simd_LONG)sl));
    x2 = vshlq_s64(x2, (int64x2_t)vnegq_s32((simd_LONG)sh));

    /* Pick off the top 32-bits of a*cf and re-pack */
    res = vcombine_s32(vshrn_n_s64(x1, 32), vshrn_n_s64(x2, 32));

    return res;
}
#define simd_idivf32_LONG(a, b, s) _ip_simd_idivf32_LONG(a, b, s)

/*
 * simd_div - division
 *
 * Only available on Arm64.  We don't bother to implement division with the
 * approximate reciprocal instructions on Arm32 because without a fused
 * multiply-add it is not possible to always achieve final bit correctness
 * (and even with FMA the original reciprocal approximation is so bloody
 * awful that after two steps NR with FMA correction and corrections on the
 * division we still don't get final bit correctness).
 */

#ifdef __aarch64__
#define simd_div_FLOAT(a, b) vdivq_f32(a,b)
#define simd_div_DOUBLE(a, b) vdivq_f64(a,b)
#endif

/*
 * simd_mean - take the mean of two vectors with rounding
 */

#define simd_mean_BYTE(a, b) vrhaddq_s8(a, b)
#define simd_mean_UBYTE(a, b) vrhaddq_u8(a, b)
#define simd_mean_SHORT(a, b) vrhaddq_s16(a, b)
#define simd_mean_USHORT(a, b) vrhaddq_u16(a, b)
#define simd_mean_LONG(a, b) vrhaddq_s32(a, b)
#define simd_mean_ULONG(a, b) vrhaddq_u32(a, b)

static inline simd_FLOAT _ip_simd_mean_FLOAT(simd_FLOAT a, simd_FLOAT b)
{
    const simd_FLOAT vhalf = simd_splat_FLOAT(0.5F);
    return simd_mul_FLOAT(simd_add_FLOAT(a, b), vhalf);
}
#define simd_mean_FLOAT(a, b) _ip_simd_mean_FLOAT(a, b)

#ifdef __aarch64__
static inline simd_DOUBLE _ip_simd_mean_DOUBLE(simd_DOUBLE a, simd_DOUBLE b)
{
    const simd_DOUBLE vhalf = simd_splat_DOUBLE(0.5F);
    return simd_mul_DOUBLE(simd_add_DOUBLE(a, b), vhalf);
}
#define simd_mean_DOUBLE(a, b) _ip_simd_mean_DOUBLE(a, b)
#endif


/* Min/max operators */

#define simd_min_BYTE(d, s) vminq_s8(d, s)
#define simd_min_UBYTE(d, s) vminq_u8(d, s)
#define simd_min_SHORT(d, s) vminq_s16(d, s)
#define simd_min_USHORT(d, s) vminq_u16(d, s)
#define simd_min_LONG(d, s) vminq_s32(d, s)
#define simd_min_ULONG(d, s) vminq_u32(d, s)
#define simd_min_FLOAT(d, s) vminq_f32(d, s)

#define simd_max_BYTE(d, s) vmaxq_s8(d, s)
#define simd_max_UBYTE(d, s) vmaxq_u8(d, s)
#define simd_max_SHORT(d, s) vmaxq_s16(d, s)
#define simd_max_USHORT(d, s) vmaxq_u16(d, s)
#define simd_max_LONG(d, s) vmaxq_s32(d, s)
#define simd_max_ULONG(d, s) vmaxq_u32(d, s)
#define simd_max_FLOAT(d, s) vmaxq_f32(d, s)

#ifdef __aarch64__
#define simd_min_DOUBLE(d, s) vminq_f64(d, s)
#define simd_max_DOUBLE(d, s) vmaxq_f64(d, s)
#endif


/*
 * simd_cmpgt
 * simd_cmpeq
 *
 * Comparison operators.
 */

#define simd_cmpgt_UBYTE(a, b) vcgtq_u8(a, b)
#define simd_cmpgt_BYTE(a, b) ((simd_BYTE)vcgtq_s8(a, b))
#define simd_cmpgt_USHORT(a, b) vcgtq_u16(a, b)
#define simd_cmpgt_SHORT(a, b) ((simd_SHORT)vcgtq_s16(a, b))
#define simd_cmpgt_ULONG(a, b) vcgtq_u32(a, b)
#define simd_cmpgt_LONG(a, b) ((simd_LONG)vcgtq_s32(a, b))
#define simd_cmpgt_FLOAT(a, b) ((simd_FLOAT)vcgtq_f32(a, b))

#define simd_cmplt_UBYTE(a, b) vcltq_u8(a, b)
#define simd_cmplt_BYTE(a, b) ((simd_BYTE)vcltq_s8(a, b))
#define simd_cmplt_USHORT(a, b) vcltq_u16(a, b)
#define simd_cmplt_SHORT(a, b) ((simd_SHORT)vcltq_s16(a, b))
#define simd_cmplt_ULONG(a, b) vcltq_u32(a, b)
#define simd_cmplt_LONG(a, b) ((simd_LONG)vcltq_s32(a, b))
#define simd_cmplt_FLOAT(a, b) ((simd_FLOAT)vcltq_f32(a, b))

#define simd_cmpeq_UBYTE(a, b) vceqq_u8(a, b)
#define simd_cmpeq_BYTE(a, b) ((simd_BYTE)vceqq_s8(a, b))
#define simd_cmpeq_USHORT(a, b) vceqq_u16(a, b)
#define simd_cmpeq_SHORT(a, b) ((simd_SHORT)vceqq_s16(a, b))
#define simd_cmpeq_ULONG(a, b) vceqq_u32(a, b)
#define simd_cmpeq_LONG(a, b) ((simd_LONG)vceqq_s32(a, b))
#define simd_cmpeq_FLOAT(a, b) ((simd_FLOAT)vceqq_f32(a, b))

#ifdef __aarch64__
#define simd_cmpgt_DOUBLE(a, b) ((simd_DOUBLE)vcgtq_f64(a, b))
#define simd_cmplt_DOUBLE(a, b) ((simd_DOUBLE)vcltq_f64(a, b))
#define simd_cmpeq_DOUBLE(a, b) ((simd_DOUBLE)vceqq_f64(a, b))
#endif

/*
 * simd_select
 *
 * Bitwise Selection operator
 */

#define simd_select_UBYTE(s, t, f) vbslq_u8(s, t, f)
#define simd_select_BYTE(s, t, f) vbslq_s8((simd_UBYTE)(s), t, f)
#define simd_select_USHORT(s, t, f) vbslq_u16(s, t, f)
#define simd_select_SHORT(s, t, f) vbslq_s16((simd_USHORT)(s), t, f)
#define simd_select_ULONG(s, t, f) vbslq_u32(s, t, f)
#define simd_select_LONG(s, t, f) vbslq_s32((simd_ULONG)(s), t, f)
#define simd_select_FLOAT(s, t, f) vbslq_f32((simd_ULONG)(s), t, f)

#ifdef __aarch64__
#define simd_select_DOUBLE(s, t, f) vbslq_f64((uint64x2_t)(s), t, f)
#endif


/*
 * Thresholding operators
 */

/* Helper functions: vector element true if low <= s <= high. Also
 *  compresses a short result into byte, and a long result into short.
 */

static inline uint8x8_t v_cmp2_u16(simd_USHORT s, simd_USHORT vlow,  simd_USHORT vhigh)
{
    simd_USHORT t1 = vcleq_u16(vlow, s);
    simd_USHORT t2 = vcleq_u16(s, vhigh);
    return vmovn_u16(vandq_u16(t1, t2));
}

static inline uint8x8_t v_cmp2_s16(simd_SHORT s, simd_SHORT vlow,  simd_SHORT vhigh)
{
    simd_USHORT t1 = vcleq_s16(vlow, s);
    simd_USHORT t2 = vcleq_s16(s, vhigh);
    return vmovn_u16(vandq_u16(t1, t2));
}

static inline uint16x4_t v_cmp2_u32(uint32x4_t s, uint32x4_t vlow,  uint32x4_t vhigh)
{
    simd_ULONG t1 = vcleq_u32(vlow, s);
    simd_ULONG t2 = vcleq_u32(s, vhigh);
    return  vmovn_u32(vandq_u32(t1, t2));
}

static inline uint16x4_t v_cmp2_s32(int32x4_t s, int32x4_t vlow,  int32x4_t vhigh)
{
    simd_ULONG t1 = vcleq_s32(vlow, s);
    simd_ULONG t2 = vcleq_s32(s, vhigh);
    return  vmovn_u32(vandq_u32(t1, t2));
}

static inline uint16x4_t v_cmp2_f32(float32x4_t s, float32x4_t vlow,  float32x4_t vhigh)
{
    simd_ULONG t1 = vcleq_f32(vlow, s);
    simd_ULONG t2 = vcleq_f32(s, vhigh);
    return  vmovn_u32(vandq_u32(t1, t2));
}

#ifdef __aarch64__
static inline uint32x2_t v_cmp2_f64(float64x2_t s, float64x2_t vlow,  float64x2_t vhigh)
{
    uint64x2_t t1 = vcleq_f64(vlow, s);
    uint64x2_t t2 = vcleq_f64(s, vhigh);
    return  vmovn_u64(vandq_u64(t1, t2));
}
#endif

/* On ARM Neon we have no use for the simd_init_threshold_* macros */
#define simd_init_threshold_UBYTE() vres = simd_not_UBYTE(vres);
#define simd_init_threshold_BYTE() vres = simd_not_UBYTE(vres);
#define simd_init_threshold_USHORT() vres = simd_not_UBYTE(vres);
#define simd_init_threshold_SHORT() vres = simd_not_UBYTE(vres);
#define simd_init_threshold_ULONG() vres = simd_not_UBYTE(vres);
#define simd_init_threshold_LONG() vres = simd_not_UBYTE(vres);
#define simd_init_threshold_FLOAT() vres = simd_not_UBYTE(vres);
#ifdef __aarch64__
#define simd_init_threshold_DOUBLE() vres = simd_not_UBYTE(vres);
#endif

static inline simd_UBYTE
_ip_simd_threshold_UBYTE(simd_UBYTE s1, const simd_UBYTE vlow, const simd_UBYTE vhigh,
			 const simd_UBYTE vfalse)
{
    simd_UBYTE t1 = vcleq_u8(vlow, s1);
    simd_UBYTE t2 = vcleq_u8(s1, vhigh);
    t1 = vandq_u8(t1, t2);
    return veorq_u8(t1, vfalse);
}

static inline simd_UBYTE
_ip_simd_threshold_BYTE(simd_BYTE s1, const simd_BYTE vlow, const simd_BYTE vhigh,
			const simd_UBYTE vfalse)
{
    simd_UBYTE t1 = (simd_UBYTE)vcleq_s8(vlow, s1);
    simd_UBYTE t2 = (simd_UBYTE)vcleq_s8(s1, vhigh);
    t1 = vandq_u8(t1, t2);
    return veorq_u8(t1, vfalse);
}

static inline simd_UBYTE
_ip_simd_threshold_USHORT(simd_USHORT s1, simd_USHORT s2,
			  const simd_USHORT vlow, const simd_USHORT vhigh,
			  const simd_UBYTE vfalse)
{
    uint8x8_t t1 = v_cmp2_u16(s1, vlow, vhigh);
    uint8x8_t t2 = v_cmp2_u16(s2, vlow, vhigh);
    return veorq_u8(vcombine_u8(t1,t2), vfalse);
}

static inline simd_UBYTE
_ip_simd_threshold_SHORT(simd_SHORT s1, simd_SHORT s2,
			 const simd_SHORT vlow, const simd_SHORT vhigh,
			 const simd_UBYTE vfalse)
{
    uint8x8_t t1 = v_cmp2_s16(s1, vlow, vhigh);
    uint8x8_t t2 = v_cmp2_s16(s2, vlow, vhigh);
    return veorq_u8(vcombine_u8(t1,t2), vfalse);
}

static inline simd_UBYTE
_ip_simd_threshold_ULONG(simd_ULONG s1, simd_ULONG s2, simd_ULONG s3, simd_ULONG s4,
			 const simd_ULONG vlow, const simd_ULONG vhigh,
			 const simd_UBYTE vfalse)
{
    uint8x8_t t3,t4;
    uint16x4_t t1 = v_cmp2_u32(s1, vlow, vhigh);
    uint16x4_t t2 = v_cmp2_u32(s2, vlow, vhigh);
    t3 = vmovn_u16(vcombine_u16(t1,t2));
    t1 = v_cmp2_u32(s3, vlow, vhigh);
    t2 = v_cmp2_u32(s4, vlow, vhigh);
    t4 = vmovn_u16(vcombine_u16(t1,t2));
    return veorq_u8(vcombine_u8(t3,t4), vfalse);
}

static inline simd_UBYTE
_ip_simd_threshold_LONG(simd_LONG s1, simd_LONG s2, simd_LONG s3, simd_LONG s4,
			const simd_LONG vlow, const simd_LONG vhigh,
			const simd_UBYTE vfalse)
{
    uint8x8_t t3,t4;
    uint16x4_t t1 = v_cmp2_s32(s1, vlow, vhigh);
    uint16x4_t t2 = v_cmp2_s32(s2, vlow, vhigh);
    t3 = vmovn_u16(vcombine_u16(t1,t2));
    t1 = v_cmp2_s32(s3, vlow, vhigh);
    t2 = v_cmp2_s32(s4, vlow, vhigh);
    t4 = vmovn_u16(vcombine_u16(t1,t2));
    return veorq_u8(vcombine_u8(t3,t4), vfalse);
}

static inline simd_UBYTE
_ip_simd_threshold_FLOAT(simd_FLOAT s1, simd_FLOAT s2, simd_FLOAT s3, simd_FLOAT s4,
			 const simd_FLOAT vlow, const simd_FLOAT vhigh,
			 const simd_UBYTE vfalse)
{
    uint8x8_t t3,t4;
    uint16x4_t t1 = v_cmp2_f32(s1, vlow, vhigh);
    uint16x4_t t2 = v_cmp2_f32(s2, vlow, vhigh);
    t3 = vmovn_u16(vcombine_u16(t1,t2));
    t1 = v_cmp2_f32(s3, vlow, vhigh);
    t2 = v_cmp2_f32(s4, vlow, vhigh);
    t4 = vmovn_u16(vcombine_u16(t1,t2));
    return veorq_u8(vcombine_u8(t3,t4), vfalse);
}

#ifdef __aarch64__
static inline simd_UBYTE
_ip_simd_threshold_DOUBLE(simd_DOUBLE s1, simd_DOUBLE s2, simd_DOUBLE s3, simd_DOUBLE s4,
			  simd_DOUBLE s5, simd_DOUBLE s6, simd_DOUBLE s7, simd_DOUBLE s8,
			  const simd_DOUBLE vlow, const simd_DOUBLE vhigh,
			  const simd_UBYTE vfalse)
{
    uint8x8_t t5, t6;
    uint16x4_t t3,t4;
    uint32x2_t t1 = v_cmp2_f64(s1, vlow, vhigh);
    uint32x2_t t2 = v_cmp2_f64(s2, vlow, vhigh);
    t3 = vmovn_u32(vcombine_u32(t1,t2));
    t1 = v_cmp2_f64(s3, vlow, vhigh);
    t2 = v_cmp2_f64(s4, vlow, vhigh);
    t4 = vmovn_u32(vcombine_u32(t1,t2));
    t5 = vmovn_u16(vcombine_u16(t3, t4));
    t1 = v_cmp2_f64(s5, vlow, vhigh);
    t2 = v_cmp2_f64(s6, vlow, vhigh);
    t3 = vmovn_u32(vcombine_u32(t1,t2));
    t1 = v_cmp2_f64(s7, vlow, vhigh);
    t2 = v_cmp2_f64(s8, vlow, vhigh);
    t4 = vmovn_u32(vcombine_u32(t1,t2));
    t6 = vmovn_u16(vcombine_u16(t3, t4));
    return veorq_u8(vcombine_u8(t5,t6), vfalse);
}
#endif

#define simd_threshold_UBYTE _ip_simd_threshold_UBYTE
#define simd_threshold_BYTE _ip_simd_threshold_BYTE
#define simd_threshold_USHORT _ip_simd_threshold_USHORT
#define simd_threshold_SHORT _ip_simd_threshold_SHORT
#define simd_threshold_ULONG _ip_simd_threshold_ULONG
#define simd_threshold_LONG _ip_simd_threshold_LONG
#define simd_threshold_FLOAT _ip_simd_threshold_FLOAT
#ifdef __aarch64__
#define simd_threshold_DOUBLE _ip_simd_threshold_DOUBLE
#endif

/*
 * NEON has a vtbl2_u8() instruction that does a table lookup and returns
 * result in a d (64-bit) register, but it would be nice to have a similar
 * instruction to work on q (128-bit) registers.  This implements it.
 */
static inline simd_UBYTE vtbl2q_u8(simd_UBYTE tbl, simd_UBYTE idx)
{
    uint8x8x2_t xrepack;
    xrepack.val[0] = vget_low_u8(tbl);
    xrepack.val[1] = vget_high_u8(tbl);
    return vcombine_u8(vtbl2_u8(xrepack,vget_low_u8(idx)), vtbl2_u8(xrepack,vget_high_u8(idx)));
}


static inline simd_UBYTE _ip_simd_replicate_right_UBYTE(simd_UBYTE x, int pos)
{
    const simd_UBYTE vinc = (simd_UBYTE){0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
    simd_UBYTE vpos = simd_splat_UBYTE(pos);
    return simd_perm_UBYTE(x, simd_min_UBYTE(vinc, vpos));
}
static inline simd_UBYTE _ip_simd_replicate_left_UBYTE(simd_UBYTE x, int pos)
{
    const simd_UBYTE vinc = (simd_UBYTE){0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
    simd_UBYTE vpos = simd_splat_UBYTE(pos);
    return simd_perm_UBYTE(x, simd_max_UBYTE(vinc, vpos));
}

static inline simd_USHORT _ip_simd_replicate_right_USHORT(simd_USHORT x, int pos)
{
    const simd_USHORT vinc = (simd_USHORT){0x0100, 0x0302, 0x0504, 0x0706,
					   0x0908, 0x0b0a, 0x0d0c, 0x0f0e};

    simd_USHORT vpos = simd_splat_USHORT((pos<<1) + (pos<<9) + 0x0100);
    return (simd_USHORT)simd_perm_UBYTE((simd_UBYTE)x, (simd_UBYTE)simd_min_USHORT(vinc, vpos));
}
static inline simd_USHORT _ip_simd_replicate_left_USHORT(simd_USHORT x, int pos)
{
    const simd_USHORT vinc = (simd_USHORT){0x0100, 0x0302, 0x0504, 0x0706,
					   0x0908, 0x0b0a, 0x0d0c, 0x0f0e};

    simd_USHORT vpos = simd_splat_USHORT((pos<<1) + (pos<<9) + 0x0100);
    return (simd_USHORT)simd_perm_UBYTE((simd_UBYTE)x, (simd_UBYTE)simd_max_USHORT(vinc, vpos));
}

static inline simd_ULONG _ip_simd_replicate_right_ULONG(simd_ULONG x, int pos)
{
    const simd_ULONG vinc = (simd_ULONG){0x03020100, 0x07060504,
					 0x0b0a0908, 0x0f0e0d0c};

    simd_ULONG vpos = simd_splat_ULONG(0x04040404*pos + 0x03020100);
    return (simd_ULONG)simd_perm_UBYTE((simd_UBYTE)x, (simd_UBYTE)simd_min_ULONG(vinc, vpos));
}
static inline simd_ULONG _ip_simd_replicate_left_ULONG(simd_ULONG x, int pos)
{
    const simd_ULONG vinc = (simd_ULONG){0x03020100, 0x07060504,
					 0x0b0a0908, 0x0f0e0d0c};

    simd_ULONG vpos = simd_splat_ULONG(0x04040404*pos + 0x03020100);
    return (simd_ULONG)simd_perm_UBYTE((simd_UBYTE)x, (simd_UBYTE)simd_max_ULONG(vinc, vpos));
}
#ifdef __aarch64__
static inline simd_DOUBLE _ip_simd_replicate_right_DOUBLE(simd_DOUBLE x, int pos)
{
    const uint64x2_t vinc = vdupq_n_u64(0x0706050403020100UL);
    const uint64x2_t vrepl = (uint64x2_t){0UL, 0x0808080808080808UL};
    uint64x2_t vpos = (uint64x2_t)simd_splat_ULONG(-pos);
    vpos = vorrq_u64(vandq_u64(vpos, vrepl), vinc);
    return (simd_DOUBLE)simd_perm_UBYTE((simd_UBYTE)x, (simd_UBYTE)vpos);
}
static inline simd_DOUBLE _ip_simd_replicate_left_DOUBLE(simd_DOUBLE x, int pos)
{
    const uint64x2_t vinc = vdupq_n_u64(0x0f0e0d0c0b0a0908UL);
    const uint64x2_t vrepl = (uint64x2_t){0x0707070707070707UL, 0x0f0f0f0f0f0f0f0fUL};
    uint64x2_t vpos = (uint64x2_t)simd_splat_ULONG(-pos);
    vpos = vandq_u64(vorrq_u64(vpos, vrepl), vinc);
    return (simd_DOUBLE)simd_perm_UBYTE((simd_UBYTE)x, (simd_UBYTE)vpos);
}
#endif

#define simd_replicate_right_UBYTE(x, p) _ip_simd_replicate_right_UBYTE(x, p)
#define simd_replicate_right_BYTE(x, p) ((simd_BYTE)_ip_simd_replicate_right_UBYTE((simd_UBYTE)(x), p))
#define simd_replicate_left_UBYTE(x, p) _ip_simd_replicate_left_UBYTE(x, p)
#define simd_replicate_left_BYTE(x, p) ((simd_BYTE)_ip_simd_replicate_left_UBYTE((simd_UBYTE)(x), p))

#define simd_replicate_right_USHORT(x, p) _ip_simd_replicate_right_USHORT(x, p)
#define simd_replicate_right_SHORT(x, p) ((simd_SHORT)_ip_simd_replicate_right_USHORT((simd_USHORT)x, p))
#define simd_replicate_left_USHORT(x, p) _ip_simd_replicate_left_USHORT(x, p)
#define simd_replicate_left_SHORT(x, p) ((simd_SHORT)_ip_simd_replicate_left_USHORT((simd_USHORT)x, p))

#define simd_replicate_right_ULONG(x, p) _ip_simd_replicate_right_ULONG(x, p)
#define simd_replicate_right_LONG(x, p) ((simd_LONG)_ip_simd_replicate_right_ULONG((simd_ULONG)x, p))
#define simd_replicate_left_ULONG(x, p) _ip_simd_replicate_left_ULONG(x, p)
#define simd_replicate_left_LONG(x, p) ((simd_LONG)_ip_simd_replicate_left_ULONG((simd_ULONG)x, p))

#define simd_replicate_right_FLOAT(x, p) ((simd_FLOAT)_ip_simd_replicate_right_ULONG((simd_ULONG)x, p))
#define simd_replicate_left_FLOAT(x, p) ((simd_FLOAT)_ip_simd_replicate_left_ULONG((simd_ULONG)x, p))

#ifdef __aarch64__
#define simd_replicate_right_DOUBLE(x, p) _ip_simd_replicate_right_DOUBLE(x, p)
#define simd_replicate_left_DOUBLE(x, p) _ip_simd_replicate_left_DOUBLE(x, p)
#endif


/*
 * simd_zero_right
 *
 * zero_right means to zero all elements to the right (i.e. in increasing
 * memory address order) of the specified position pos.  It is like
 * replicate_right but is used to for effecting a zero boundary on the right
 * side of images in filtering operations.
 */

static inline simd_UBYTE _ip_simd_zero_right_UBYTE(simd_UBYTE x, int pos)
{
    const simd_UBYTE vinc = (simd_UBYTE){0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
    simd_UBYTE vpos = simd_splat_UBYTE(pos);
    return simd_and_UBYTE(x, vcleq_u8(vinc, vpos));
}

static inline simd_USHORT _ip_simd_zero_right_USHORT(simd_USHORT x, int pos)
{
    const simd_USHORT vinc = (simd_USHORT){0, 1, 2, 3, 4, 5, 6, 7};
    simd_USHORT vpos = simd_splat_USHORT(pos);
    return simd_and_USHORT(x, vcleq_u16(vinc, vpos));
}

static inline simd_ULONG _ip_simd_zero_right_ULONG(simd_ULONG x, int pos)
{
    const simd_ULONG vinc = (simd_ULONG){0, 1, 2, 3};
    simd_ULONG vpos = simd_splat_ULONG(pos);
    return simd_and_ULONG(x, vcleq_u32(vinc, vpos));
}

static inline simd_FLOAT _ip_simd_zero_right_FLOAT(simd_FLOAT x, int pos)
{
    const simd_ULONG vinc = (simd_ULONG){0, 1, 2, 3};
    simd_ULONG vpos = simd_splat_ULONG(pos);
    return (simd_FLOAT)simd_and_ULONG((simd_ULONG)x, vcleq_u32(vinc, vpos));
}

#define simd_zero_right_UBYTE(x, p) _ip_simd_zero_right_UBYTE(x, p)
#define simd_zero_right_BYTE(x, p) ((simd_BYTE)_ip_simd_zero_right_UBYTE((simd_UBYTE)x, p))
#define simd_zero_right_USHORT(x, p) _ip_simd_zero_right_USHORT(x, p)
#define simd_zero_right_SHORT(x, p) ((simd_SHORT)_ip_simd_zero_right_USHORT((simd_USHORT)x, p))
#define simd_zero_right_ULONG(x, p) _ip_simd_zero_right_ULONG(x, p)
#define simd_zero_right_LONG(x, p) ((simd_LONG)_ip_simd_zero_right_ULONG((simd_ULONG)x, p))
#define simd_zero_right_FLOAT(x, p) _ip_simd_zero_right_FLOAT(x, p)

#ifdef __aarch64__
static inline simd_DOUBLE _ip_simd_zero_right_DOUBLE(simd_DOUBLE x, int pos)
{
    const simd_ULONG vinc = (simd_ULONG){0, 0, 1, 1};
    simd_ULONG vpos = simd_splat_ULONG(pos);
    return (simd_DOUBLE)simd_and_ULONG((simd_ULONG)x, vcleq_u32(vinc, vpos));
}
#define simd_zero_right_DOUBLE(x, p) _ip_simd_zero_right_DOUBLE(x, p)
#endif


static inline simd_UBYTE _ip_simd_zero_left_UBYTE(simd_UBYTE x, int pos)
{
    const simd_UBYTE vinc = (simd_UBYTE){0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
    simd_UBYTE vpos = simd_splat_UBYTE(pos);
    return simd_and_UBYTE(x, vcleq_u8(vpos, vinc));
}

static inline simd_USHORT _ip_simd_zero_left_USHORT(simd_USHORT x, int pos)
{
    const simd_USHORT vinc = (simd_USHORT){0, 1, 2, 3, 4, 5, 6, 7};
    simd_USHORT vpos = simd_splat_USHORT(pos);
    return simd_and_USHORT(x, vcleq_u16(vpos, vinc));
}

static inline simd_ULONG _ip_simd_zero_left_ULONG(simd_ULONG x, int pos)
{
    const simd_ULONG vinc = (simd_ULONG){0, 1, 2, 3};
    simd_ULONG vpos = simd_splat_ULONG(pos);
    return simd_and_ULONG(x, vcleq_u32(vpos, vinc));
}

static inline simd_FLOAT _ip_simd_zero_left_FLOAT(simd_FLOAT x, int pos)
{
    const simd_ULONG vinc = (simd_ULONG){0, 1, 2, 3};
    simd_ULONG vpos = simd_splat_ULONG(pos);
    return (simd_FLOAT)simd_and_ULONG((simd_ULONG)x, vcleq_u32(vpos, vinc));
}

#define simd_zero_left_UBYTE(x, p) _ip_simd_zero_left_UBYTE(x, p)
#define simd_zero_left_BYTE(x, p) ((simd_BYTE)_ip_simd_zero_left_UBYTE((simd_UBYTE)x, p))
#define simd_zero_left_USHORT(x, p) _ip_simd_zero_left_USHORT(x, p)
#define simd_zero_left_SHORT(x, p) ((simd_SHORT)_ip_simd_zero_left_USHORT((simd_USHORT)x, p))
#define simd_zero_left_ULONG(x, p) _ip_simd_zero_left_ULONG(x, p)
#define simd_zero_left_LONG(x, p) ((simd_LONG)_ip_simd_zero_left_ULONG((simd_ULONG)x, p))
#define simd_zero_left_FLOAT(x, p) _ip_simd_zero_left_FLOAT(x, p)

#ifdef __aarch64__
static inline simd_DOUBLE _ip_simd_zero_left_DOUBLE(simd_DOUBLE x, int pos)
{
    const simd_ULONG vinc = (simd_ULONG){0, 0, 1, 1};
    simd_ULONG vpos = simd_splat_ULONG(pos);
    return (simd_DOUBLE)simd_and_ULONG((simd_ULONG)x, vcleq_u32(vpos, vinc));
}
#define simd_zero_left_DOUBLE(x, p) _ip_simd_zero_left_DOUBLE(x, p)
#endif
