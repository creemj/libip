/*
   Module: ip/simd/median_simd.c
   Author: M. J. Cree

   SIMD implementation of median filter for small kernel sizes.

   Copyright (C) 2019 Michael J. Cree
*/

#include <ip/ip.h>

#include "../internal.h"
#include "../function_ops.h"

#include "simd.h"
/* #include "simd-macros.h" */
#include "median_simd.h"


/*
 * Extra types used for returning multiple values.  The macro defines struct
 * v_BYTE_2, v_BYTE_3, etc.
 */

#define v_TYPE_n(tt, n) struct v_##tt##_##n { simd_##tt v[n]; }

v_TYPE_n(BYTE, 2);
v_TYPE_n(BYTE, 3);
v_TYPE_n(UBYTE, 2);
v_TYPE_n(UBYTE, 3);

v_TYPE_n(SHORT, 2);
v_TYPE_n(SHORT, 3);
v_TYPE_n(USHORT, 2);
v_TYPE_n(USHORT, 3);

#ifdef simd_min_LONG
v_TYPE_n(LONG, 2);
v_TYPE_n(LONG, 3);
v_TYPE_n(ULONG, 2);
v_TYPE_n(ULONG, 3);
#endif

#ifdef simd_min_FLOAT
v_TYPE_n(FLOAT, 2);
v_TYPE_n(FLOAT, 3);
#endif

#ifdef simd_min_DOUBLE
v_TYPE_n(DOUBLE, 2);
v_TYPE_n(DOUBLE, 3);
#endif

/* Helper macros */


/*
 * vminmax(x, y)
 *
 * Sort elements in x and y so that x is returned with the minimum and y the
 * maximum at each position in the vector.
 */

#define vminmax(tt, x, y)			\
    do {					\
	simd_##tt t;				\
	t = simd_min_##tt(x, y);		\
	y = simd_max_##tt(x, y);		\
	x = t;					\
    } while (0)


/*
 * vsort3(a, b, c)
 *
 * Sort the elements at the same position in vectors a, b and c so that a
 * contains the smallest value of the three elements, vector b contains the
 * median and vector c contains the maximum.
 */

#define vsort3(tt, a, b, c)			\
    do {					\
	vminmax(tt, a, b);			\
	vminmax(tt, b, c);			\
	vminmax(tt, a, b);			\
    } while(0)

/*
 * vmax3(a, b, c)
 *
 * Return the maximum of a, b and c elementwise.
 */

#define vmax3(tt, a, b, c)			\
    ({						\
	simd_##tt t;				\
	t = simd_max_##tt(a, b);		\
	simd_max_##tt(t, c);			\
    })

/*
 * vmed3(a, b, c)
 *
 * Return the median of a, b and c elementwise.
 */

#define vmed3(tt, a, b, c)			\
    ({						\
	simd_##tt tmin, tmax, tmed;		\
	tmin = simd_min_##tt(a, b);		\
	tmax = simd_max_##tt(a, b);		\
	tmed = simd_min_##tt(tmax, c);		\
	simd_max_##tt(tmin, tmed);		\
    })

/*
 * vmin3(a, b, c)
 *
 * Return the minimum of a, b and c elementwise.
 */

#define vmin3(tt, a, b, c)			\
    ({						\
	simd_##tt t;				\
	t = simd_min_##tt(a, b);		\
	simd_min_##tt(t, c);			\
    })


#define generate_simd_median_3x3(tt) \
    static void _ip_simd_median_3x3_##tt(ip_image *res, ip_image *im, int flag)	\
    {									\
	int rowlen = im->row_size / sizeof(simd_##tt);			\
	simd_##tt *rup, *rcentre;					\
	rup = (flag == FLG_ZEROEXTEND) ? im->extra_rows : IM_ROWADR(im, 0, v); \
	rcentre = IM_ROWADR(im, 0, v);					\
	for (int j=0; j<im->size.y; j++) {				\
	    simd_##tt *rdown, *rresult;					\
	    simd_##tt pup, pcentre, pdown, presult;			\
	    int pos = im->size.x - SIMD_NUM_ELEMENTS(tt)*(rowlen-1) - 1; \
	    								\
	    if (j<im->size.y-1)						\
		rdown = IM_ROWADR(im, j+1, v);				\
	    else							\
		rdown = (flag == FLG_ZEROEXTEND) ? im->extra_rows : IM_ROWADR(im, im->size.y-1, v); \
									\
	    if (flag == FLG_ZEROEXTEND) {				\
		pup = pcentre = pdown = simd_splat_##tt(0);		\
	    }else{							\
		pup = simd_splat_v0_##tt(rup[0]);			\
		pcentre = simd_splat_v0_##tt(rcentre[0]);		\
		pdown = simd_splat_v0_##tt(rdown[0]);			\
	    }								\
	    vsort3(tt, pup, pcentre, pdown);				\
									\
	    rresult = IM_ROWADR(res, j, v);				\
	    presult = simd_splat_##tt(0);  /* STFCU */			\
									\
	    for (int i=0; i<rowlen-1; i++) {				\
		simd_##tt tup, tcentre, tdown, tresult;			\
		simd_##tt max_up, med_centre, min_down;			\
		const int b1 = SIMD_NUM_ELEMENTS(tt) - 1;		\
		const int b2 = SIMD_NUM_ELEMENTS(tt) - 2;		\
									\
		tup = rup[i];						\
		tcentre = rcentre[i];					\
		tdown = rdown[i];					\
									\
		vsort3(tt, tup, tcentre, tdown);			\
		max_up = vmax3(tt, tup, simd_cvu_imm_##tt(pup, tup, b1), \
					   simd_cvu_imm_##tt(pup, tup, b2));\
		med_centre = vmed3(tt, tcentre, simd_cvu_imm_##tt(pcentre, tcentre, b1), \
				   simd_cvu_imm_##tt(pcentre, tcentre, b2)); \
		min_down = vmin3(tt, tdown, simd_cvu_imm_##tt(pdown, tdown, b1),\
				 simd_cvu_imm_##tt(pdown, tdown, b2));	\
		tresult = vmed3(tt, max_up, med_centre, min_down);	\
									\
		if (i>0) {						\
		    rresult[i-1] = simd_cvu_imm_##tt(presult, tresult, 1); \
		}							\
									\
		pup = tup;						\
		pcentre = tcentre;					\
		pdown = tdown;						\
		presult = tresult;					\
	    }								\
									\
	    for (int i=rowlen-1; i<rowlen+1; i++) {			\
		simd_##tt tup, tcentre, tdown, tresult;			\
		simd_##tt max_up, med_centre, min_down;			\
		const int b1 = SIMD_NUM_ELEMENTS(tt) - 1;		\
		const int b2 = SIMD_NUM_ELEMENTS(tt) - 2;		\
									\
		if (i == rowlen-1) {					\
		    if (flag == FLG_ZEROEXTEND) {			\
			tup = simd_zero_right_##tt(rup[rowlen-1], pos); \
			tcentre = simd_zero_right_##tt(rcentre[rowlen-1], pos); \
			tdown = simd_zero_right_##tt(rdown[rowlen-1], pos); \
		    }else{						\
			tup = simd_replicate_right_##tt(rup[rowlen-1], pos); \
			tcentre = simd_replicate_right_##tt(rcentre[rowlen-1], pos); \
			tdown = simd_replicate_right_##tt(rdown[rowlen-1], pos); \
		    }							\
		}else{							\
		    if (flag == FLG_ZEROEXTEND) {			\
			tup = tcentre = tdown = simd_splat_##tt(0);	\
		    }else{						\
			tup = simd_replicate_left_##tt(rup[rowlen-1], pos); \
			tcentre = simd_replicate_left_##tt(rcentre[rowlen-1], pos); \
			tdown = simd_replicate_left_##tt(rdown[rowlen-1], pos); \
		    }							\
		}							\
									\
		vsort3(tt, tup, tcentre, tdown);			\
		max_up = vmax3(tt, tup, simd_cvu_imm_##tt(pup, tup, b1), \
			       simd_cvu_imm_##tt(pup, tup, b2));	\
		med_centre = vmed3(tt, tcentre, simd_cvu_imm_##tt(pcentre, tcentre, b1), \
				   simd_cvu_imm_##tt(pcentre, tcentre, b2)); \
		min_down = vmin3(tt, tdown, simd_cvu_imm_##tt(pdown, tdown, b1), \
				 simd_cvu_imm_##tt(pdown, tdown, b2));	\
		tresult = vmed3(tt, max_up, med_centre, min_down);	\
									\
		rresult[i-1] = simd_cvu_imm_##tt(presult, tresult, 1);	\
									\
		pup = tup;						\
		pcentre = tcentre;					\
		pdown = tdown;						\
		presult = tresult;					\
	    }								\
									\
	    rup = rcentre;						\
	    rcentre = rdown;						\
	}								\
	SIMD_EXIT();							\
    }


generate_simd_median_3x3(UBYTE)
generate_simd_median_3x3(BYTE)
generate_simd_median_3x3(USHORT)
generate_simd_median_3x3(SHORT)
#ifdef simd_min_ULONG
generate_simd_median_3x3(ULONG)
generate_simd_median_3x3(LONG)
#endif
#ifdef simd_min_FLOAT
generate_simd_median_3x3(FLOAT)
#endif
#ifdef simd_min_DOUBLE
generate_simd_median_3x3(DOUBLE)
#endif

improc_function_IIf ip_simd_median_3x3_bytype[IM_TYPEMAX] = {
    [IM_UBYTE] = _ip_simd_median_3x3_UBYTE,
    [IM_BYTE] = _ip_simd_median_3x3_BYTE,
    [IM_USHORT] = _ip_simd_median_3x3_USHORT,
    [IM_SHORT] = _ip_simd_median_3x3_SHORT,
#ifdef simd_min_ULONG
    [IM_ULONG] = _ip_simd_median_3x3_ULONG,
    [IM_LONG] = _ip_simd_median_3x3_LONG,
#endif
#ifdef simd_min_FLOAT
    [IM_FLOAT] = _ip_simd_median_3x3_FLOAT,
#endif
#ifdef simd_min_DOUBLE
    [IM_DOUBLE] = _ip_simd_median_3x3_DOUBLE
#endif
};



/*
 * vsort5(a, b, c, d, e)
 *
 * Sort the elements at the same position in vectors a, b, c, d and e so
 * that a contains the smallest value of the three elements, etc.
 */

#define vsort5(tt, a, b, c, d, e)		\
    do {					\
	vminmax(tt, a, b);			\
	vminmax(tt, d, e);			\
	vminmax(tt, b, c);			\
	vminmax(tt, a, b);			\
	vminmax(tt, c, e);			\
	vminmax(tt, a, c);			\
	vminmax(tt, b, d);			\
	vminmax(tt, a, b);			\
	vminmax(tt, c, d);			\
    } while(0)


/*
 * vmed5(a, b, c, d, e)
 *
 * Return median (elementwise) of vectors a, b, c, d and e.
 */

#define vmed5(tt, a, b, c, d, e)		\
    ({						\
	simd_##tt wa, wb, wc, wd, we;		\
	wa = a; wb = b; wc =c; wd = d; we = e;	\
	vminmax(tt, wa, wb);			\
	vminmax(tt, wd, we);			\
	vminmax(tt, wb, wc);			\
	vminmax(tt, wa, wb);			\
	wc = simd_min_##tt(wc, we);		\
	wc = simd_max_##tt(wa, wc);		\
	wd = simd_max_##tt(wb, wd);		\
	simd_min_##tt(wc, wd);			\
    })

#define vmax2of5(tt, a, b, c, d, e)		\
    ({						\
	struct v_##tt##_2 res;			\
	simd_##tt wa, wb, wc, wd, we;		\
	wa = a; wb = b; wc =c; wd = d; we = e;	\
	vminmax(tt, wa, wb);			\
	vminmax(tt, wd, we);			\
	vminmax(tt, wb, wc);			\
	wb = simd_max_##tt(wa, wb);		\
	vminmax(tt, wc, we);			\
	wd = simd_max_##tt(wb, wd);		\
	wd = simd_max_##tt(wc, wd);		\
	res.v[0] = wd;				\
	res.v[1] = we;				\
	res;					\
    })

#define vmax3of5(tt, a, b, c, d, e)		\
    ({						\
	struct v_##tt##_3 res;			\
	simd_##tt wa, wb, wc, wd, we;		\
	wa = a; wb = b; wc =c; wd = d; we = e;	\
	vminmax(tt, wa, wb);			\
	vminmax(tt, wd, we);			\
	vminmax(tt, wb, wc);			\
	vminmax(tt, wa, wb);			\
	vminmax(tt, wc, we);			\
	wc = simd_max_##tt(wa, wc);		\
	wd = simd_max_##tt(wb, wd);		\
	vminmax(tt, wc, wd);			\
	res.v[0] = wc;				\
	res.v[1] = wd;				\
	res.v[2] = we;				\
	res;					\
    })

#define vmed3of5(tt, a, b, c, d, e)		\
    ({						\
	struct v_##tt##_3 res;			\
	simd_##tt wa, wb, wc, wd, we;		\
	wa = a; wb = b; wc =c; wd = d; we = e;	\
	vminmax(tt, wa, wb);			\
	vminmax(tt, wd, we);			\
	vminmax(tt, wb, wc);			\
	vminmax(tt, wa, wb);			\
	wc = simd_min_##tt(wc, we);		\
	vminmax(tt, wa, wc);			\
	vminmax(tt, wb, wd);			\
	wb = simd_max_##tt(wa, wb);		\
	vminmax(tt, wc, wd);			\
	res.v[0] = wb;				\
	res.v[1] = wc;				\
	res.v[2] = wd;				\
	res;					\
    })

#define vmin3of5(tt, a, b, c, d, e)		\
    ({						\
	struct v_##tt##_3 res;			\
	simd_##tt wa, wb, wc, wd, we;		\
	wa = a; wb = b; wc =c; wd = d; we = e;	\
	vminmax(tt, wb, wa);			\
	vminmax(tt, we, wd);			\
	vminmax(tt, wc, wb);			\
	vminmax(tt, wb, wa);			\
	vminmax(tt, we, wc);			\
	wc = simd_min_##tt(wa, wc);		\
	wd = simd_min_##tt(wb, wd);		\
	vminmax(tt, wd, wc);			\
	res.v[0] = we;				\
	res.v[1] = wd;				\
	res.v[2] = wc;				\
	res;					\
    })

#define vmin2of5(tt, a, b, c, d, e)		\
    ({						\
	struct v_##tt##_2 res;			\
	simd_##tt wa, wb, wc, wd, we;		\
	wa = a; wb = b; wc =c; wd = d; we = e;	\
	vminmax(tt, wb, wa);			\
	vminmax(tt, we, wd);			\
	vminmax(tt, wc, wb);			\
	wb = simd_min_##tt(wa, wb);		\
	vminmax(tt, we, wc);			\
	wd = simd_min_##tt(wb, wd);		\
	wd = simd_min_##tt(wc, wd);		\
	res.v[0] = we;				\
	res.v[1] = wd;				\
	res;					\
    })

#define vmax4(tt, a, b, c, d)			\
    ({						\
	simd_##tt t1, t2;			\
	t1 = simd_max_##tt(a, b);		\
	t2 = simd_max_##tt(c, d);		\
	simd_max_##tt(t1, t2);			\
    })

#define vmin4(tt, a, b, c, d)			\
    ({						\
	simd_##tt t1, t2;			\
	t1 = simd_min_##tt(a, b);		\
	t2 = simd_min_##tt(c, d);		\
	simd_min_##tt(t1, t2);			\
    })



#define generate_simd_median_5x5(tt) \
    static void _ip_simd_median_5x5_##tt(ip_image *res, ip_image *im, int flag)	\
    {									\
	int rowlen = im->row_size / sizeof(simd_##tt);			\
	simd_##tt *r1, *r2, *r3, *r4;					\
	r1 = r2 = (flag == FLG_ZEROEXTEND) ? im->extra_rows : IM_ROWADR(im, 0, v); \
	r3 = IM_ROWADR(im, 0, v);					\
	r4 = IM_ROWADR(im, 1, v);					\
	for (int j=0; j<im->size.y; j++) {				\
	    simd_##tt *r5, *rresult;					\
	    simd_##tt pr1, pr2, pr3, pr4, pr5, presult;			\
	    int pos = im->size.x - SIMD_NUM_ELEMENTS(tt)*(rowlen-1) - 1; \
	    								\
	    if (j<im->size.y-2)						\
		r5 = IM_ROWADR(im, j+2, v);				\
	    else							\
		r5 = (flag == FLG_ZEROEXTEND) ? im->extra_rows : IM_ROWADR(im, im->size.y-1, v); \
									\
	    if (flag == FLG_ZEROEXTEND) {				\
		pr1 = pr2 = pr3 = pr4 = pr5 = simd_splat_##tt(0);	\
	    }else{							\
		pr1 = simd_splat_v0_##tt(r1[0]);			\
		pr2 = simd_splat_v0_##tt(r2[0]);			\
		pr3 = simd_splat_v0_##tt(r3[0]);			\
		pr4 = simd_splat_v0_##tt(r4[0]);			\
		pr5 = simd_splat_v0_##tt(r5[0]);			\
	    }								\
	    vsort5(tt, pr1, pr2, pr3, pr4, pr5);			\
									\
	    rresult = IM_ROWADR(res, j, v);				\
	    presult = simd_splat_##tt(0);  /* STFCU */			\
									\
	    for (int i=0; i<rowlen-1; i++) {				\
		simd_##tt tr1, tr2, tr3, tr4, tr5, tresult;		\
		struct v_##tt##_2 sr1, sr5;				\
		struct v_##tt##_3 sr2, sr3, sr4;			\
		simd_##tt vtmin, vtmed, vtmax;				\
		const int b1 = SIMD_NUM_ELEMENTS(tt) - 1;		\
		const int b2 = SIMD_NUM_ELEMENTS(tt) - 2;		\
		const int b3 = SIMD_NUM_ELEMENTS(tt) - 3;		\
		const int b4 = SIMD_NUM_ELEMENTS(tt) - 4;		\
									\
		tr1 = r1[i];						\
		tr2 = r2[i];						\
		tr3 = r3[i];						\
		tr4 = r4[i];						\
		tr5 = r5[i];						\
									\
		vsort5(tt, tr1, tr2, tr3, tr4, tr5);			\
		sr1 = vmax2of5(tt, tr1,					\
			       simd_cvu_imm_##tt(pr1, tr1, b1),		\
			       simd_cvu_imm_##tt(pr1, tr1, b2),		\
			       simd_cvu_imm_##tt(pr1, tr1, b3),		\
			       simd_cvu_imm_##tt(pr1, tr1, b4));	\
		sr2 = vmax3of5(tt, tr2,					\
			       simd_cvu_imm_##tt(pr2, tr2, b1),		\
			       simd_cvu_imm_##tt(pr2, tr2, b2),		\
			       simd_cvu_imm_##tt(pr2, tr2, b3),		\
			       simd_cvu_imm_##tt(pr2, tr2, b4));	\
		sr3 = vmed3of5(tt, tr3,					\
			       simd_cvu_imm_##tt(pr3, tr3, b1),		\
			       simd_cvu_imm_##tt(pr3, tr3, b2),		\
			       simd_cvu_imm_##tt(pr3, tr3, b3),		\
			       simd_cvu_imm_##tt(pr3, tr3, b4));	\
		sr4 = vmin3of5(tt, tr4,					\
			       simd_cvu_imm_##tt(pr4, tr4, b1),		\
			       simd_cvu_imm_##tt(pr4, tr4, b2),		\
			       simd_cvu_imm_##tt(pr4, tr4, b3),		\
			       simd_cvu_imm_##tt(pr4, tr4, b4));	\
		sr5 = vmin2of5(tt, tr5,					\
			       simd_cvu_imm_##tt(pr5, tr5, b1),		\
			       simd_cvu_imm_##tt(pr5, tr5, b2),		\
			       simd_cvu_imm_##tt(pr5, tr5, b3),		\
			       simd_cvu_imm_##tt(pr5, tr5, b4));	\
		vtmin = vmax4(tt, sr1.v[0], sr2.v[0], sr3.v[0], sr4.v[0]); \
		vtmed = vmed5(tt, sr1.v[1], sr2.v[1], sr3.v[1], sr4.v[1], sr5.v[0]); \
		vtmax = vmin4(tt, sr2.v[2], sr3.v[2], sr4.v[2], sr5.v[1]); \
		tresult = vmed3(tt, vtmin, vtmed, vtmax);		\
									\
		if (i>0) {						\
		    rresult[i-1] = simd_cvu_imm_##tt(presult, tresult, 2); \
		}							\
									\
		pr1 = tr1;						\
		pr2 = tr2;						\
		pr3 = tr3;						\
		pr4 = tr4;						\
		pr5 = tr5;						\
		presult = tresult;					\
	    }								\
									\
	    for (int i=rowlen-1; i<rowlen+1; i++) {			\
		simd_##tt tr1, tr2, tr3, tr4, tr5, tresult;		\
		struct v_##tt##_2 sr1, sr5;				\
		struct v_##tt##_3 sr2, sr3, sr4;			\
		simd_##tt vtmin, vtmed, vtmax;				\
		const int b1 = SIMD_NUM_ELEMENTS(tt) - 1;		\
		const int b2 = SIMD_NUM_ELEMENTS(tt) - 2;		\
		const int b3 = SIMD_NUM_ELEMENTS(tt) - 3;		\
		const int b4 = SIMD_NUM_ELEMENTS(tt) - 4;		\
									\
		if (i == rowlen-1) {					\
		    if (flag == FLG_ZEROEXTEND) {			\
			tr1 = simd_zero_right_##tt(r1[rowlen-1], pos); \
			tr2 = simd_zero_right_##tt(r2[rowlen-1], pos); \
			tr3 = simd_zero_right_##tt(r3[rowlen-1], pos); \
			tr4 = simd_zero_right_##tt(r4[rowlen-1], pos); \
			tr5 = simd_zero_right_##tt(r5[rowlen-1], pos); \
		    }else{						\
			tr1 = simd_replicate_right_##tt(r1[rowlen-1], pos); \
			tr2 = simd_replicate_right_##tt(r2[rowlen-1], pos); \
			tr3 = simd_replicate_right_##tt(r3[rowlen-1], pos); \
			tr4 = simd_replicate_right_##tt(r4[rowlen-1], pos); \
			tr5 = simd_replicate_right_##tt(r5[rowlen-1], pos); \
		    }							\
		}else{							\
		    if (flag == FLG_ZEROEXTEND) {			\
			tr1 = tr2 = tr3 = tr4 = tr5 = simd_splat_##tt(0); \
		    }else{						\
			tr1 = simd_replicate_left_##tt(r1[rowlen-1], pos); \
			tr2 = simd_replicate_left_##tt(r2[rowlen-1], pos); \
			tr3 = simd_replicate_left_##tt(r3[rowlen-1], pos); \
			tr4 = simd_replicate_left_##tt(r4[rowlen-1], pos); \
			tr5 = simd_replicate_left_##tt(r5[rowlen-1], pos); \
		    }							\
		}							\
									\
		vsort5(tt, tr1, tr2, tr3, tr4, tr5);			\
		sr1 = vmax2of5(tt, tr1,					\
			       simd_cvu_imm_##tt(pr1, tr1, b1),		\
			       simd_cvu_imm_##tt(pr1, tr1, b2),		\
			       simd_cvu_imm_##tt(pr1, tr1, b3),		\
			       simd_cvu_imm_##tt(pr1, tr1, b4));	\
		sr2 = vmax3of5(tt, tr2,					\
			       simd_cvu_imm_##tt(pr2, tr2, b1),		\
			       simd_cvu_imm_##tt(pr2, tr2, b2),		\
			       simd_cvu_imm_##tt(pr2, tr2, b3),		\
			       simd_cvu_imm_##tt(pr2, tr2, b4));	\
		sr3 = vmed3of5(tt, tr3,					\
			       simd_cvu_imm_##tt(pr3, tr3, b1),		\
			       simd_cvu_imm_##tt(pr3, tr3, b2),		\
			       simd_cvu_imm_##tt(pr3, tr3, b3),		\
			       simd_cvu_imm_##tt(pr3, tr3, b4));	\
		sr4 = vmin3of5(tt, tr4,					\
			       simd_cvu_imm_##tt(pr4, tr4, b1),		\
			       simd_cvu_imm_##tt(pr4, tr4, b2),		\
			       simd_cvu_imm_##tt(pr4, tr4, b3),		\
			       simd_cvu_imm_##tt(pr4, tr4, b4));	\
		sr5 = vmin2of5(tt, tr5,					\
			       simd_cvu_imm_##tt(pr5, tr5, b1),		\
			       simd_cvu_imm_##tt(pr5, tr5, b2),		\
			       simd_cvu_imm_##tt(pr5, tr5, b3),		\
			       simd_cvu_imm_##tt(pr5, tr5, b4));	\
		vtmin = vmax4(tt, sr1.v[0], sr2.v[0], sr3.v[0], sr4.v[0]); \
		vtmed = vmed5(tt, sr1.v[1], sr2.v[1], sr3.v[1], sr4.v[1], sr5.v[0]); \
		vtmax = vmin4(tt, sr2.v[2], sr3.v[2], sr4.v[2], sr5.v[1]); \
		tresult = vmed3(tt, vtmin, vtmed, vtmax);		\
									\
		rresult[i-1] = simd_cvu_imm_##tt(presult, tresult, 2);	\
									\
		pr1 = tr1;						\
		pr2 = tr2;						\
		pr3 = tr3;						\
		pr4 = tr4;						\
		pr5 = tr5;						\
		presult = tresult;					\
	    }								\
									\
	    r1 = r2;							\
	    r2 = r3;							\
	    r3 = r4;							\
	    r4 = r5;							\
	}								\
	SIMD_EXIT();							\
    }


/*
 * Note that we need at least four elements in the vector for this algorithm
 * hence the reason we test the vector size for DOUBLE.
 */

generate_simd_median_5x5(UBYTE)
generate_simd_median_5x5(BYTE)
generate_simd_median_5x5(USHORT)
generate_simd_median_5x5(SHORT)
#ifdef simd_min_ULONG
generate_simd_median_5x5(ULONG)
generate_simd_median_5x5(LONG)
#endif
#ifdef simd_min_FLOAT
generate_simd_median_5x5(FLOAT)
#endif
#if defined(simd_min_DOUBLE) && SIMD_VECTOR_SIZE >= 32
generate_simd_median_5x5(DOUBLE)
#endif

improc_function_IIf ip_simd_median_5x5_bytype[IM_TYPEMAX] = {
    [IM_UBYTE] = _ip_simd_median_5x5_UBYTE,
    [IM_BYTE] = _ip_simd_median_5x5_BYTE,
    [IM_USHORT] = _ip_simd_median_5x5_USHORT,
    [IM_SHORT] = _ip_simd_median_5x5_SHORT,
#ifdef simd_min_ULONG
    [IM_ULONG] = _ip_simd_median_5x5_ULONG,
    [IM_LONG] = _ip_simd_median_5x5_LONG,
#endif
#ifdef simd_min_FLOAT
    [IM_FLOAT] = _ip_simd_median_5x5_FLOAT,
#endif
#if defined(simd_min_DOUBLE) && SIMD_VECTOR_SIZE >= 32
    [IM_DOUBLE] = _ip_simd_median_5x5_DOUBLE
#endif
};
