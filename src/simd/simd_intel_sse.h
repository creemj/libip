/*
   Name: ip/simd/intel_sse.h
   Author: M. J. Cree

   Copyright (C) 2012-2013, 2015-2017 Michael J. Cree

   SIMD support for the IP library.

   This defines the Intel SSE2/SSE3 support.

*/


/*
 * Intel SSE/SSE2/SSE3 SIMD support
 */
/*
#define HAVE_INTEL_SSE3
#define HAVE_INTEL_SSSE3
#define HAVE_INTEL_SSE41
*/

#include <ip/divide.h>


#include <xmmintrin.h>
#include <emmintrin.h>

#ifdef HAVE_INTEL_SSE3
#include <pmmintrin.h>
#endif
#ifdef HAVE_INTEL_SSSE3
#include <tmmintrin.h>
#endif
#ifdef HAVE_INTEL_SSE41
#include <smmintrin.h>
#endif
#ifdef HAVE_INTEL_SSE42
#include <nmmintrin.h>
#endif


/* Basic SIMD types */
typedef __m128i simd_UBYTE;
typedef __m128i simd_BYTE;
typedef __m128i simd_USHORT;
typedef __m128i simd_SHORT;
typedef __m128i simd_ULONG;
typedef __m128i simd_LONG;
typedef __m128  simd_FLOAT;
typedef __m128d simd_DOUBLE;
typedef __m128  simd_COMPLEX;
typedef __m128d simd_DCOMPLEX;

/*
 * We define ULONG64/LONG64 for some intermediate calculations that enable
 * 32/32 fixed-point arithmetc.
 */
typedef __m128i simd_ULONG64;
typedef __m128i simd_LONG64;


/*
 * simd_splat
 *
 * For splatting a scalar value across the simd vector.
 */

#define simd_splat_UBYTE(c)  _mm_set1_epi8(c)
#define simd_splat_BYTE(c)   _mm_set1_epi8(c)
#define simd_splat_USHORT(c) _mm_set1_epi16(c)
#define simd_splat_SHORT(c)  _mm_set1_epi16(c)
#define simd_splat_ULONG(c)  _mm_set1_epi32(c)
#define simd_splat_LONG(c)   _mm_set1_epi32(c)
#define simd_splat_FLOAT(c)  _mm_set1_ps(c)
#define simd_splat_DOUBLE(c) _mm_set1_pd(c)

#define simd_splat_COMPLEX(u,v)  _mm_set_ps(v,u,v,u)
#define simd_splat_DCOMPLEX(u,v) _mm_set_pd(v,u)

#define simd_splat_ULONG64(c)  _mm_set1_epi64x(c)
#define simd_splat_LONG64(c)   _mm_set1_epi64x(c)

/*
 * Now that the splat routines are defined and, thus, indicate which image
 * types we support in SIMD, now get the rest of the typedefs that are
 * needed for the convert routines below.
 */

#include "simd_td.h"

/*
 * simd_splat_v0
 *
 * Splat the zeroth element across the whole vector.
 */
#ifdef HAVE_INTEL_SSSE3
#define simd_splat_v0_BYTE(x) _mm_shuffle_epi8(x, _mm_setzero_si128())
#define simd_splat_v0_UBYTE(x)_mm_shuffle_epi8(x, _mm_setzero_si128())
#else
#define simd_splat_v0_BYTE(x) _mm_shuffle_epi32(_mm_shufflelo_epi16(_mm_unpacklo_epi8(x, x), 0), 0)
#define simd_splat_v0_UBYTE(x) _mm_shuffle_epi32(_mm_shufflelo_epi16(_mm_unpacklo_epi8(x, x), 0), 0)
#endif
#define simd_splat_v0_SHORT(x) _mm_shuffle_epi32(_mm_shufflelo_epi16(x, 0), 0)
#define simd_splat_v0_USHORT(x)_mm_shuffle_epi32(_mm_shufflelo_epi16(x, 0), 0)
#define simd_splat_v0_LONG(x) _mm_shuffle_epi32(x,0)
#define simd_splat_v0_ULONG(x) _mm_shuffle_epi32(x,0)
#define simd_splat_v0_FLOAT(x) _mm_shuffle_ps(x,x,0)
#define simd_splat_v0_DOUBLE(x) _mm_shuffle_pd(x,x,0)

/*
 * simd_mask
 *
 * Masks for multielement image types (complex and the rgb image types)
 */
#define simd_mask_COMPLEX(r, i) _mm_set_epi32(i, r, i, r)
#define simd_mask_DCOMPLEX(r, i) _mm_set_epi32(i, i, r, r)
#define simd_mask_RGB(r, g, b) _mm_set_epi8(r, b, g, r, b, g, r, b, g, r, b, g, r, b, g, r)
#define simd_mask_RGBA(r, g, b, a) _mm_set_epi8(a, b, g, r, a, b, g, r, a, b, g, r, a, b, g, r)
#define simd_mask_RGB16(r, g, b) _mm_set_epi16(g, r, b, g, r, b, g, r)


/*
 * simd_extract
 *
 * Extract the specified element from a vector.
 *
 * SSE does not support this well; we resort to a memory access via a union
 * to get to the element.  Hopefully the compiler can sometimes optimise the
 * memory access away.  Even SSE4.1, which provides an extract intrinsic is
 * useless for our purposes because the element position must be known at
 * compile time, which goes to show that Intel SSE has significant annoying
 * limitations.
 */

union ip_simd_access {
    __m128i v;
    uint8_t ub[16];
    uint16_t us[8];
    uint32_t ul[4];
    float f[4];
    double d[2];
};

static inline int _ip_simd_extract_UBYTE(simd_UBYTE u, const int pos)
{
    union ip_simd_access x;
    x.v = u;
    return x.ub[pos];
}
#define simd_extract_BYTE(v, p) _ip_simd_extract_UBYTE(v, p)
#define simd_extract_UBYTE(v, p) _ip_simd_extract_UBYTE(v, p)

static inline int _ip_simd_extract_USHORT(simd_USHORT u, const int pos)
{
    union ip_simd_access x;
    x.v = u;
    return x.us[pos];
}
#define simd_extract_SHORT(v, p) _ip_simd_extract_USHORT(v, p)
#define simd_extract_USHORT(v, p) _ip_simd_extract_USHORT(v, p)

static inline uint32_t _ip_simd_extract_ULONG(simd_ULONG u, const int pos)
{
    union ip_simd_access x;
    x.v = u;
    return x.ul[pos];
}
#define simd_extract_LONG(v, p) ((int32_t)_ip_simd_extract_ULONG(v, p))
#define simd_extract_ULONG(v, p) _ip_simd_extract_ULONG(v, p)

static inline float _ip_simd_extract_FLOAT(simd_FLOAT u, const int pos)
{
    union ip_simd_access x;
    x.v = (__m128i)u;
    return x.f[pos];
}
#define simd_extract_FLOAT(v, p) _ip_simd_extract_FLOAT(v, p)

static inline double _ip_simd_extract_DOUBLE(simd_DOUBLE u, const int pos)
{
    union ip_simd_access x;
    x.v = (__m128i)u;
    return x.d[pos];
}
#define simd_extract_DOUBLE(v, p) _ip_simd_extract_DOUBLE(v, p)

/*
 * simd_splat_vN
 *
 * Splat Nth element of vector to whole vector.  Uses extract if don't have
 * SSSE3, in which case it is inefficient as it goes through memory.
 */

#ifdef HAVE_INTEL_SSSE3
static inline simd_USHORT _ip_splat_vN_USHORT(simd_USHORT x, int pos)
{
    const simd_USHORT incr = simd_splat_USHORT(0x100);
    simd_UBYTE idx = _mm_add_epi8(simd_splat_UBYTE(2*pos), incr);
    return _mm_shuffle_epi8(x, idx);
}
static inline simd_ULONG _ip_splat_vN_ULONG(simd_ULONG x, int pos)
{
    const simd_ULONG incr = simd_splat_ULONG(0x03020100);
    simd_UBYTE idx = _mm_add_epi8(simd_splat_UBYTE(4*pos), incr);
    return _mm_shuffle_epi8(x, idx);
}
static inline simd_DOUBLE _ip_splat_vN_DOUBLE(simd_DOUBLE x, int pos)
{
    const simd_ULONG incr = _mm_setr_epi32(0x03020100, 0x07060504,
					    0x03020100, 0x07060504);
    simd_UBYTE idx = _mm_add_epi8(simd_splat_UBYTE(8*pos), incr);
    return (simd_DOUBLE)_mm_shuffle_epi8((simd_UBYTE)x, idx);
}
#define simd_splat_vN_BYTE(x, n) _mm_shuffle_epi8(x, simd_splat_BYTE(n))
#define simd_splat_vN_UBYTE(x, n) _mm_shuffle_epi8(x, simd_splat_UBYTE(n))
#define simd_splat_vN_SHORT(x, n) _ip_splat_vN_USHORT(x, n)
#define simd_splat_vN_USHORT(x, n) _ip_splat_vN_USHORT(x, n)
#define simd_splat_vN_LONG(x, n) _ip_splat_vN_ULONG(x, n)
#define simd_splat_vN_ULONG(x, n) _ip_splat_vN_ULONG(x, n)
#define simd_splat_vN_FLOAT(x, n) (simd_FLOAT)_ip_splat_vN_ULONG((simd_ULONG)x, n)
#define simd_splat_vN_DOUBLE(x, n) _ip_splat_vN_DOUBLE(x, n)
#else
#define simd_splat_vN_BYTE(x, n) simd_splat_BYTE(simd_extract_BYTE(x, n))
#define simd_splat_vN_UBYTE(x, n) simd_splat_BYTE(simd_extract_UBYTE(x, n))
#define simd_splat_vN_SHORT(x, n) simd_splat_SHORT(simd_extract_SHORT(x, n))
#define simd_splat_vN_USHORT(x, n) simd_splat_USHORT(simd_extract_USHORT(x, n))
#define simd_splat_vN_LONG(x, n) simd_splat_LONG(simd_extract_LONG(x, n))
#define simd_splat_vN_ULONG(x, n) simd_splat_ULONG(simd_extract_ULONG(x, n))
#define simd_splat_vN_FLOAT(x, n) simd_splat_FLOAT(simd_extract_FLOAT(x, n))
#define simd_splat_vN_DOUBLE(x, n) simd_splat_DOUBLE(simd_extract_DOUBLE(x, n))
#endif


/*
 * simd_cvu_imm
 *
 * For constructing an unaligned vector (cvu) from two adjacent vectors; the
 * offset must be known at compile time.  Efficient with SSSE3, much less so
 * without.
 */

#ifdef HAVE_INTEL_SSSE3
/*
 * simd_cvu_imm:  SSSE3 implementation
 */
#define simd_cvu_imm_UBYTE(l,h,o) _mm_alignr_epi8(h, l, o)
#define simd_cvu_imm_BYTE(l,h,o) _mm_alignr_epi8(h, l, o)
#define simd_cvu_imm_USHORT(l,h,o) _mm_alignr_epi8(h, l, 2*(o))
#define simd_cvu_imm_SHORT(l,h,o) _mm_alignr_epi8(h, l, 2*(o))
#define simd_cvu_imm_ULONG(l,h,o) _mm_alignr_epi8(h, l, 4*(o))
#define simd_cvu_imm_LONG(l,h,o) _mm_alignr_epi8(h, l, 4*(o))
#define simd_cvu_imm_FLOAT(l,h,o) \
    ((simd_FLOAT)_mm_alignr_epi8((__m128i)h, (__m128i)l, 4*(o)))
#define simd_cvu_imm_DOUBLE(l,h,o) \
    ((simd_DOUBLE)_mm_alignr_epi8((__m128i)h, (__m128i)l, 8*(o)))
#else

#ifdef __clang__
/*
 * simd_cvu_imm:  Clang SSE2 implementation
 *
 * Useless clang errors out on the immediate args despite the use of a const
 * int, so we make these macros and hope for the best.
 */
#define simd_cvu_imm_UBYTE(l,r,ofs) \
    _mm_or_si128(_mm_srli_si128(l, ofs), _mm_slli_si128(r, 16-(ofs)))
#define simd_cvu_imm_BYTE(l,r,o) simd_cvu_imm_UBYTE(l,r,o)

#define simd_cvu_imm_USHORT(l,r,ofs) \
    _mm_or_si128(_mm_srli_si128(l, 2*(ofs)), _mm_slli_si128(r, 16-(2*(ofs))))
#define simd_cvu_imm_SHORT(l,r,o) simd_cvu_imm_USHORT(l,r,o)

#define simd_cvu_imm_ULONG(l,r,ofs) \
    _mm_or_si128(_mm_srli_si128(l, 4*(ofs)), _mm_slli_si128(r, 16-(4*(ofs))))
#define simd_cvu_imm_LONG(l,r,o) simd_cvu_imm_ULONG(l,r,o)

#define simd_cvu_imm_FLOAT(l,r,o) \
    ((simd_FLOAT)simd_cvu_imm_ULONG((simd_ULONG)(l), (simd_ULONG)(r), o))

#define simd_cvu_imm_DOUBLE(l,r,ofs) \
    ((simd_DOUBLE)_mm_or_si128(_mm_srli_si128((__m128i)(l), 8*(ofs)), \
			       _mm_slli_si128((__m128i)(r), 16-(8*(ofs)))))

#else
/*
 * simd_cvu_imm: SSE2 implementation for gcc, etc.
 *
 * Better definitions that any compiler with at least half a brain cell can
 * compile.
 */
static inline simd_UBYTE _ip_simd_cvu_imm_UBYTE(simd_UBYTE l, simd_UBYTE h, const int ofs)
{
    const unsigned char hofs = (unsigned char)16-(unsigned char)ofs;
    return _mm_or_si128(_mm_srli_si128(l, ofs), _mm_slli_si128(h, hofs));
}
#define simd_cvu_imm_UBYTE(l,r,o) _ip_simd_cvu_imm_UBYTE(l,r,o)
#define simd_cvu_imm_BYTE(l,r,o) _ip_simd_cvu_imm_UBYTE(l,r,o)
static inline simd_USHORT _ip_simd_cvu_imm_USHORT(simd_USHORT l, simd_USHORT h, const int ofs)
{
    const unsigned char tofs = 2*ofs;
    const unsigned char hofs = (unsigned char)16-(2*ofs);
    return _mm_or_si128(_mm_srli_si128(l, tofs), _mm_slli_si128(h, hofs));
}
#define simd_cvu_imm_USHORT(l,r,o) _ip_simd_cvu_imm_USHORT(l,r,o)
#define simd_cvu_imm_SHORT(l,r,o) _ip_simd_cvu_imm_USHORT(l,r,o)
static inline simd_ULONG _ip_simd_cvu_imm_ULONG(simd_ULONG l, simd_ULONG h, const int ofs)
{
    const unsigned char tofs = 4*ofs;
    const unsigned char hofs = (unsigned char)16-(4*ofs);
    return _mm_or_si128(_mm_srli_si128(l, tofs), _mm_slli_si128(h, hofs));
}
#define simd_cvu_imm_ULONG(l,r,o) _ip_simd_cvu_imm_ULONG(l,r,o)
#define simd_cvu_imm_LONG(l,r,o) _ip_simd_cvu_imm_ULONG(l,r,o)
#define simd_cvu_imm_FLOAT(l,r,o) \
    ((simd_FLOAT)_ip_simd_cvu_imm_ULONG((simd_ULONG)l, (simd_ULONG)r, o))
static inline simd_DOUBLE _ip_simd_cvu_imm_DOUBLE(simd_DOUBLE l, simd_DOUBLE h, const int ofs)
{
    const unsigned char tofs = 8*ofs;
    const unsigned char hofs = (unsigned char)16-tofs;
    return (simd_DOUBLE)_mm_or_si128(_mm_srli_si128((__m128i)l, tofs), _mm_slli_si128((__m128i)h, hofs));
}
#define simd_cvu_imm_DOUBLE(l,r,o) _ip_simd_cvu_imm_DOUBLE(l,r,o)
#endif
#endif

/*
 * simd_cvu
 *
 * CVU with an offset only known at runtime.
 *
 * Not implemented on Intel SSE. The problem is that Intel SSE, having been
 * implemented by nincompoops, only includes 128 bit shifts and 128 bit cvu
 * with an immediate operand, i.e., the shifts/alignment must be known at
 * compile time.
 */
#ifdef DONT_INCLUDE
/* But will eventually depende on SSSE3 */
static inline simd_UBYTE _ip_simd_cvu_UBYTE(simd_UBYTE l, simd_UBYTE r, int ofs)
{
    simd_UBYTE vinc = _mm_setr_epi8(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15);
    simd_UBYTE left = _mm_shuffle_epi8(l, _mm_add_epi8(vinc, simd_splat_UBYTE(ofs+0x70)));
    simd_UBYTE right = _mm_shuffle_epi8(r, _mm_sub_epi8(vinc, simd_splat_UBYTE(ofs-16)));
    return _mm_or_si128(left, right);
}
#define simd_cvu_UBYTE(l,r,o) _ip_simd_cvu_UBYTE(l,r,o)
#endif


/*
 * simd_ldu
 *
 * Load unaligned vector from arbitrary memory address.
 *
 * Some code, on finding no simd_cvu_TYPE macros, will attempt to use
 * simd_ldu_TYPE macros instead.
 */

#ifdef HAVE_INTEL_SSE3
#define simd_ldu_UBYTE(m) _mm_lddqu_si128((__m128i *)(m))
#define simd_ldu_BYTE(m) _mm_lddqu_si128((__m128i *)(m))
#define simd_ldu_SHORT(m) _mm_lddqu_si128((__m128i *)(m))
#define simd_ldu_USHORT(m) _mm_lddqu_si128((__m128i *)(m))
#define simd_ldu_ULONG(m) _mm_lddqu_si128((__m128i *)(m))
#define simd_ldu_LONG(m) _mm_lddqu_si128((__m128i *)(m))
#define simd_ldu_FLOAT(m) ((simd_FLOAT)_mm_lddqu_si128((__m128i *)(m)))
#define simd_ldu_DOUBLE(m) ((simd_DOUBLE)_mm_lddqu_si128((__m128i *)(m)))
#else
#define simd_ldu_UBYTE(m) _mm_loadu_si128((__m128i *)(m))
#define simd_ldu_BYTE(m) _mm_loadu_si128((__m128i *)(m))
#define simd_ldu_SHORT(m) _mm_loadu_si128((__m128i *)(m))
#define simd_ldu_USHORT(m) _mm_loadu_si128((__m128i *)(m))
#define simd_ldu_ULONG(m) _mm_loadu_si128((__m128i *)(m))
#define simd_ldu_LONG(m) _mm_loadu_si128((__m128i *)(m))
#define simd_ldu_FLOAT(m) _mm_loadu_ps((float *)(m))
#define simd_ldu_DOUBLE(m) _mm_loadu_pd((double *)(m))
#endif

/*
 * Helper function: select bits of a where corresponding bit of sel is true
 * else select bit of b.  This is a common CPU instruction but Intel do not
 * appear to implement it until SSE4.1.
 */
#ifdef HAVE_INTEL_SSE41

static inline __m128i v_select(__m128i sel, __m128i a, __m128i b)
{
    return _mm_blendv_epi8(b, a, sel);
}

static inline __m128 v_selectf(__m128 sel, __m128 a, __m128 b)
{
    return _mm_blendv_ps(b, a, sel);
}

static inline __m128d v_selectd(__m128d sel, __m128d a, __m128d b)
{
    return _mm_blendv_pd(b, a, sel);
}

#else

static inline __m128i v_select(__m128i sel, __m128i a, __m128i b)
{
    return _mm_or_si128(_mm_and_si128(sel, a), _mm_andnot_si128(sel, b));
}

static inline __m128 v_selectf(__m128 sel, __m128 a, __m128 b)
{
    __m128d dsel = (__m128d)sel;
    __m128d da = (__m128d)a;
    __m128d db = (__m128d)b;
    return (__m128)_mm_or_pd(_mm_and_pd(dsel, da), _mm_andnot_pd(dsel, db));
}

static inline __m128d v_selectd(__m128d sel, __m128d a, __m128d b)
{
    return _mm_or_pd(_mm_and_pd(sel, a), _mm_andnot_pd(sel, b));
}

#endif


/*
 * Helper functions for SSE2. Implement some of the missing max/min
 * functions.
 */
#ifndef HAVE_INTEL_SSE41
static inline simd_BYTE sse2_v_min_epi8(simd_BYTE a, simd_BYTE b)
{
    return v_select(_mm_cmpgt_epi8(a,b), b, a);
}
static inline simd_BYTE sse2_v_max_epi8(simd_BYTE a, simd_BYTE b)
{
    return v_select(_mm_cmpgt_epi8(a,b), a, b);
}

static inline simd_USHORT sse2_v_min_epu16(simd_USHORT a, simd_USHORT b)
{
    simd_USHORT sel = _mm_cmpeq_epi16(_mm_subs_epu16(a, b), _mm_setzero_si128());
    return v_select(sel, a, b);
}
static inline simd_USHORT sse2_v_max_epu16(simd_USHORT a, simd_USHORT b)
{
    simd_USHORT sel = _mm_cmpeq_epi16(_mm_subs_epu16(a, b), _mm_setzero_si128());
    return v_select(sel, b, a);
}

static inline simd_LONG sse2_v_min_epi32(simd_LONG a, simd_LONG b)
{
    return v_select(_mm_cmpgt_epi32(a,b), b, a);
}
static inline simd_LONG sse2_v_max_epi32(simd_LONG a, simd_LONG b)
{
    return v_select(_mm_cmpgt_epi32(a,b), a, b);
}

static inline simd_ULONG sse2_v_min_epu32(simd_ULONG a, simd_ULONG b)
{
    const simd_ULONG ofs = simd_splat_ULONG(0x80000000);
    return v_select(_mm_cmpgt_epi32(_mm_sub_epi32(a, ofs), _mm_sub_epi32(b, ofs)), b, a);
}
static inline simd_ULONG sse2_v_max_epu32(simd_ULONG a, simd_ULONG b)
{
    const simd_ULONG ofs = simd_splat_ULONG(0x80000000);
    return v_select(_mm_cmpgt_epi32(_mm_sub_epi32(a, ofs), _mm_sub_epi32(b, ofs)), a, b);
}

#else

#define sse2_v_min_epi8(a, b) _mm_min_epi8(a, b)
#define sse2_v_max_epi8(a, b) _mm_max_epi8(a, b)
#define sse2_v_min_epu16(a, b) _mm_min_epu16(a, b)
#define sse2_v_max_epiu16(a, b) _mm_max_epu16(a, b)
#define sse2_v_min_epi32(a, b) _mm_min_epi32(a, b)
#define sse2_v_max_epi32(a, b) _mm_max_epi32(a, b)
#define sse2_v_min_epu32(a, b) _mm_min_epu32(a, b)
#define sse2_v_max_epu32(a, b) _mm_max_epu32(a, b)

#endif

/*
 * Transpose functions.
 */

static inline simd_USHORT_8 _ip_simd_transpose_USHORT(simd_USHORT_8 x)
{
    simd_USHORT a, b, e1, e2, e3, e4, e5, e6, e7, e8;
    a = _mm_unpacklo_epi16(x.us[0], x.us[1]);
    b = _mm_unpacklo_epi16(x.us[2], x.us[3]);
    e1 = _mm_unpacklo_epi32(a, b);
    e2 = _mm_unpackhi_epi32(a, b);
    a = _mm_unpackhi_epi16(x.us[0], x.us[1]);
    b = _mm_unpackhi_epi16(x.us[2], x.us[3]);
    e3 = _mm_unpacklo_epi32(a, b);
    e4 = _mm_unpackhi_epi32(a, b);
    a = _mm_unpacklo_epi16(x.us[4], x.us[5]);
    b = _mm_unpacklo_epi16(x.us[6], x.us[7]);
    e5 = _mm_unpacklo_epi32(a, b);
    e6 = _mm_unpackhi_epi32(a, b);
    a = _mm_unpackhi_epi16(x.us[4], x.us[5]);
    b = _mm_unpackhi_epi16(x.us[6], x.us[7]);
    e7 = _mm_unpacklo_epi32(a, b);
    e8 = _mm_unpackhi_epi32(a, b);
    x.us[0] = _mm_unpacklo_epi64(e1, e5);
    x.us[1] = _mm_unpackhi_epi64(e1, e5);
    x.us[2] = _mm_unpacklo_epi64(e2, e6);
    x.us[3] = _mm_unpackhi_epi64(e2, e6);
    x.us[4] = _mm_unpacklo_epi64(e3, e7);
    x.us[5] = _mm_unpackhi_epi64(e3, e7);
    x.us[6] = _mm_unpacklo_epi64(e4, e8);
    x.us[7] = _mm_unpackhi_epi64(e4, e8);
    return x;
}
#define simd_transpose_USHORT(x) _ip_simd_transpose_USHORT(x)


static inline simd_ULONG sse_movehl_epi32(simd_ULONG a, simd_ULONG b)
{
    return (simd_ULONG)_mm_movehl_ps((simd_FLOAT)a, (simd_FLOAT)b);
}

static inline simd_ULONG sse_movelh_epi32(simd_ULONG a, simd_ULONG b)
{
    return (simd_ULONG)_mm_movelh_ps((simd_FLOAT)a, (simd_FLOAT)b);
}

static inline simd_ULONG_4 _ip_simd_transpose_ULONG(simd_ULONG_4 x)
{
    simd_ULONG a, b, c, d;
    a = _mm_unpacklo_epi32(x.ul[0], x.ul[1]);
    c = _mm_unpacklo_epi32(x.ul[2], x.ul[3]);
    b = _mm_unpackhi_epi32(x.ul[0], x.ul[1]);
    d = _mm_unpackhi_epi32(x.ul[2], x.ul[3]);
    x.ul[0] = sse_movelh_epi32(a, c);
    x.ul[1] = sse_movehl_epi32(c, a);
    x.ul[2] = sse_movelh_epi32(b, d);
    x.ul[3] = sse_movehl_epi32(d, b);

    return x;
}
#define simd_transpose_ULONG(x) _ip_simd_transpose_ULONG(x)



static inline simd_FLOAT_4 _ip_simd_transpose_FLOAT(simd_FLOAT_4 x)
{
    _MM_TRANSPOSE4_PS(x.f[0], x.f[1], x.f[2], x.f[3]);
    return x;
}
#define simd_transpose_FLOAT(x) _ip_simd_transpose_FLOAT(x)


static inline simd_DOUBLE_2 _ip_simd_transpose_DOUBLE(simd_DOUBLE_2 x)
{
    simd_DOUBLE_2 res;

    res.d[0] = _mm_shuffle_pd(x.d[0], x.d[1], 0);
    res.d[1] = _mm_shuffle_pd(x.d[0], x.d[1], 3);
    return res;
}
#define simd_transpose_DOUBLE(x) _ip_simd_transpose_DOUBLE(x)

/*
 * simd_interleave
 */

static inline simd_UBYTE_2 _ip_simd_interleave_UBYTE(simd_UBYTE x, simd_UBYTE y)
{
    simd_UBYTE_2 res;

    res.ub[0] = _mm_unpacklo_epi8(x, y);
    res.ub[1] = _mm_unpackhi_epi8(x, y);
    return res;
}
#define simd_interleave_UBYTE(a, b) _ip_simd_interleave_UBYTE(a, b)

static inline simd_BYTE_2 _ip_simd_interleave_BYTE(simd_BYTE x, simd_BYTE y)
{
    simd_BYTE_2 res;

    res.b[0] = _mm_unpacklo_epi8(x, y);
    res.b[1] = _mm_unpackhi_epi8(x, y);
    return res;
}
#define simd_interleave_BYTE(a, b) _ip_simd_interleave_BYTE(a, b)

static inline simd_USHORT_2 _ip_simd_interleave_USHORT(simd_USHORT x, simd_USHORT y)
{
    simd_USHORT_2 res;

    res.us[0] = _mm_unpacklo_epi16(x, y);
    res.us[1] = _mm_unpackhi_epi16(x, y);
    return res;
}
#define simd_interleave_USHORT(a, b) _ip_simd_interleave_USHORT(a, b)

static inline simd_SHORT_2 _ip_simd_interleave_SHORT(simd_SHORT x, simd_SHORT y)
{
    simd_SHORT_2 res;

    res.s[0] = _mm_unpacklo_epi16(x, y);
    res.s[1] = _mm_unpackhi_epi16(x, y);
    return res;
}
#define simd_interleave_SHORT(a, b) _ip_simd_interleave_SHORT(a, b)

static inline simd_ULONG_2 _ip_simd_interleave_ULONG(simd_ULONG x, simd_ULONG y)
{
    simd_ULONG_2 res;

    res.ul[0] = _mm_unpacklo_epi32(x, y);
    res.ul[1] = _mm_unpackhi_epi32(x, y);
    return res;
}
#define simd_interleave_ULONG(a, b) _ip_simd_interleave_ULONG(a, b)

static inline simd_LONG_2 _ip_simd_interleave_LONG(simd_LONG x, simd_LONG y)
{
    simd_LONG_2 res;

    res.l[0] = _mm_unpacklo_epi32(x, y);
    res.l[1] = _mm_unpackhi_epi32(x, y);
    return res;
}
#define simd_interleave_LONG(a, b) _ip_simd_interleave_LONG(a, b)

static inline simd_FLOAT_2 _ip_simd_interleave_FLOAT(simd_FLOAT x, simd_FLOAT y)
{
    simd_FLOAT_2 res;

    res.f[0] = _mm_unpacklo_ps(x, y);
    res.f[1] = _mm_unpackhi_ps(x, y);
    return res;
}
#define simd_interleave_FLOAT(a, b) _ip_simd_interleave_FLOAT(a, b)

static inline simd_DOUBLE_2 _ip_simd_interleave_DOUBLE(simd_DOUBLE x, simd_DOUBLE y)
{
    simd_DOUBLE_2 res;

    res.d[0] = _mm_unpacklo_pd(x, y);
    res.d[1] = _mm_unpackhi_pd(x, y);
    return res;
}
#define simd_interleave_DOUBLE(a, b) _ip_simd_interleave_DOUBLE(a, b)




/*
 * Comparison of SIMD vector as a whole.
 */

#ifdef HAVE_INTEL_SSE41

static inline int _ip_simd_vector_zero_UBYTE(simd_UBYTE x)
{
    return _mm_test_all_zeros(x, x);
}

static inline int _ip_simd_vector_equal_UBYTE(simd_UBYTE x, simd_UBYTE y)
{
    return _ip_simd_vector_zero_UBYTE(_mm_sub_epi8(x, y));
}

#define simd_vector_zero_UBYTE(x) _ip_simd_vector_zero_UBYTE(x)
#define simd_vector_equal_UBYTE(x,y) _ip_simd_vector_equal_UBYTE(x,y)

#endif

/*
 * Create a byte mask for masking out padding on last vector in image row.
 */
static inline simd_UBYTE _ip_simd_create_byte_mask(int numbytes)
{
    const simd_UBYTE vinc = _mm_setr_epi8(0, 1, 2, 3, 4, 5, 6, 7,
					  8, 9, 10, 11, 12, 13, 14, 15);
    simd_UBYTE vnumb = simd_splat_UBYTE(numbytes);
    return _mm_cmpgt_epi8(vnumb, vinc);
}
#define simd_create_byte_mask(x) _ip_simd_create_byte_mask(x)



/*
 * Conversion routines for converting one image type to another.
 *
 * simd_cnvrt_DTYPE_STYPE
 *   - convert source type STYPE to type DTYPE
 *
 * simd_cnvrt_clip_DTYPE_STYPE
 *   - convert source type STYPE to type DTYPE with clipping (saturation).
 *
 * There are three sections below:
 *   COMMON - Implementations that are common to all levels of SSE.
 *   SSE4.1 - Implementations that rely on having SSE41.
 *   SSE2   - Implementations that are used only if SSE41 is not available.
 */

/*
 * COMMON: conversions implemented in SSE2 only with no special SSE4.1
 * implementation below.
 *
 * truncation:
 *   simd_cnvrt_UBYTE_USHORT
 *   simd_cnvrt_UBYTE_ULONG
 * with clipping:
 *   simd_cnvrt_clip_BYTE_UBYTE
 *   simd_cnvrt_clip_BYTE_SHORT
 *   simd_cnvrt_clip_UBYTE_SHORT
 *   simd_cnvrt_clip_USHORT_SHORT
 *   simd_cnvrt_clip_ULONG_SHORT
 *   simd_cnvrt_clip_BYTE_USHORT
 *   simd_cnvrt_clip_UBYTE_USHORT
 *   simd_cnvrt_clip_BYTE_LONG
 *   simd_cnvrt_clip_SHORT_LONG
 *   simd_cnvrt_clip_SHORT_ULONG
 * to/from BINARY
 *   simd_cnvrt_BINARY_UBYTE
 *   simd_cnvrt_BINARY_USHORT
 *   simd_cnvrt_BINARY_ULONG
 */

/* COMMON truncate larger integer type to smaller */
static inline simd_UBYTE _ip_simd_cnvrt_UBYTE_USHORT(simd_USHORT a, simd_USHORT b)
{
    const simd_USHORT mask = _mm_setr_epi16(0x00ff, 0x00ff, 0x00ff, 0x00ff,
					   0x00ff, 0x00ff, 0x00ff, 0x00ff);

    a = _mm_and_si128(a, mask);
    b = _mm_and_si128(b, mask);
    return _mm_packus_epi16(a, b);
}
#define simd_cnvrt_UBYTE_USHORT(a, b) _ip_simd_cnvrt_UBYTE_USHORT(a, b)

static inline simd_UBYTE _ip_simd_cnvrt_UBYTE_ULONG(simd_ULONG a, simd_ULONG b, simd_ULONG c, simd_ULONG d)
{
    const simd_USHORT mask = _mm_setr_epi32(0x000000ff, 0x000000ff, 0x000000ff, 0x000000ff);

    a = _mm_and_si128(a, mask);
    b = _mm_and_si128(b, mask);
    c = _mm_and_si128(c, mask);
    d = _mm_and_si128(d, mask);
    a = _mm_packus_epi16(a, b);
    b = _mm_packus_epi16(c, d);
    return _mm_packus_epi16(a, b);
}
#define simd_cnvrt_UBYTE_ULONG(a, b, c, d) _ip_simd_cnvrt_UBYTE_ULONG(a, b, c, d)

/* COMMON convert UBYTE to other types with clipping */

#define simd_cnvrt_clip_BYTE_UBYTE(a) _mm_min_epu8(a, _mm_set1_epi8(127))

/* COMMON: convert SHORT to other types with clipping */
#define simd_cnvrt_clip_BYTE_SHORT(a,b) _mm_packs_epi16(a,b)
#define simd_cnvrt_clip_UBYTE_SHORT(a,b) _mm_packus_epi16(a,b)
#define simd_cnvrt_clip_USHORT_SHORT(a) _mm_max_epi16(a, _mm_setzero_si128())
#define simd_cnvrt_clip_ULONG_SHORT(a) simd_cnvrt_ULONG_USHORT(_mm_max_epi16(a, _mm_setzero_si128()))

/* COMMON: convert USHORT to other types with clipping */

static inline simd_BYTE _ip_simd_cnvrt_clip_BYTE_USHORT(simd_USHORT a, simd_USHORT b)
{
    const simd_USHORT vzero = _mm_setzero_si128();
    const simd_USHORT vmax = _mm_set1_epi16(127);
    simd_USHORT sela, selb;

    sela = _mm_cmplt_epi16(a, vzero);
    a = v_select(sela, vmax, a);
    selb = _mm_cmplt_epi16(b, vzero);
    b = v_select(selb, vmax, b);
    return _mm_packs_epi16(a,b);
}
#define simd_cnvrt_clip_BYTE_USHORT(a,b) _ip_simd_cnvrt_clip_BYTE_USHORT(a,b)

static inline simd_UBYTE _ip_simd_cnvrt_clip_UBYTE_USHORT(simd_USHORT a, simd_USHORT b)
{
    const simd_USHORT vzero = _mm_setzero_si128();
    const simd_USHORT vmax = _mm_set1_epi16(255);
    simd_USHORT sela, selb;

    sela = _mm_cmplt_epi16(a, vzero);
    a = v_select(sela, vmax, a);
    selb = _mm_cmplt_epi16(b, vzero);
    b = v_select(selb, vmax, b);
    return _mm_packus_epi16(a,b);
}
#define simd_cnvrt_clip_UBYTE_USHORT(a,b) _ip_simd_cnvrt_clip_UBYTE_USHORT(a,b)

/* COMMON convert LONG to other types with clipping */

#define simd_cnvrt_clip_BYTE_LONG(a,b,c,d) \
    _mm_packs_epi16(_mm_packs_epi32(a,b),_mm_packs_epi32(c,d))
#define simd_cnvrt_clip_SHORT_LONG(a,b) _mm_packs_epi32(a,b)

/* COMMON convert ULONG to other types with clipping */

static inline simd_SHORT _ip_simd_cnvrt_clip_SHORT_ULONG(simd_ULONG a, simd_ULONG b)
{
    const simd_ULONG vzero = _mm_setzero_si128();
    const simd_ULONG vmax = _mm_set1_epi32(0x7fff);
    simd_ULONG sela, selb;

    sela = _mm_cmplt_epi32(a, vzero);
    a = v_select(sela, vmax, a);
    selb = _mm_cmplt_epi32(b, vzero);
    b = v_select(selb, vmax, b);
    return _mm_packs_epi32(a,b);
}
#define simd_cnvrt_clip_SHORT_ULONG(a,b) _ip_simd_cnvrt_clip_SHORT_ULONG(a,b)

/* COMMON convert UBYTE/USHORT/ULONG to BINARY */
static inline simd_UBYTE _ip_simd_cnvrt_BINARY_UBYTE(simd_UBYTE x)
{
    const simd_UBYTE vfalse = simd_splat_UBYTE(0);
    const simd_UBYTE vtrue = simd_splat_UBYTE(255);
    return _mm_andnot_si128(_mm_cmpeq_epi8(x, vfalse), vtrue);
}
#define simd_cnvrt_BINARY_UBYTE(x) _ip_simd_cnvrt_BINARY_UBYTE(x)

static inline simd_UBYTE _ip_simd_cnvrt_BINARY_USHORT(simd_USHORT a, simd_USHORT b)
{
    const simd_USHORT vfalse = simd_splat_USHORT(0);
    const simd_USHORT vtrue = simd_splat_USHORT(0xffff);
    a = _mm_andnot_si128(_mm_cmpeq_epi16(a, vfalse), vtrue);
    b = _mm_andnot_si128(_mm_cmpeq_epi16(b, vfalse), vtrue);
    return _mm_packs_epi16(a, b);
}
#define simd_cnvrt_BINARY_USHORT(x, y) _ip_simd_cnvrt_BINARY_USHORT(x, y)

static inline simd_UBYTE _ip_simd_cnvrt_BINARY_ULONG(simd_ULONG a, simd_ULONG b,
						     simd_ULONG c, simd_ULONG d)
{
    const simd_ULONG vfalse = simd_splat_ULONG(0);
    const simd_ULONG vtrue = simd_splat_ULONG(0xffffffff);
    a = _mm_andnot_si128(_mm_cmpeq_epi32(a, vfalse), vtrue);
    b = _mm_andnot_si128(_mm_cmpeq_epi32(b, vfalse), vtrue);
    c = _mm_andnot_si128(_mm_cmpeq_epi32(c, vfalse), vtrue);
    d = _mm_andnot_si128(_mm_cmpeq_epi32(d, vfalse), vtrue);
    return _mm_packs_epi16(_mm_packs_epi32(a, b),_mm_packs_epi32(c, d));
}
#define simd_cnvrt_BINARY_ULONG(x, y, u, v) _ip_simd_cnvrt_BINARY_ULONG(x, y, u, v)


#ifdef HAVE_INTEL_SSE41

/*
 * SSE4.1 - functions implemented with SSE4.1 extensions
 *
 * zero extension:
 *   simd_cnvrt_USHORT_UBYTE
 *   simd_cnvrt_ULONG_UBYTE
 *   simd_cnvrt_ULONG_USHORT
 * signed extension:
 *   simd_cnvrt_SHORT_BYTE
 *   simd_cnvrt_LONG_BYTE
 *   simd_cnvrt_LONG_SHORT
 * truncation:
 *   simd_cnvrt_USHORT_ULONG
 * with clipping:
 *   simd_cnvrt_clip_UBYTE_BYTE
 *   simd_cnvrt_clip_USHORT_BYTE
 *   simd_cnvrt_clip_ULONG_BYTE
 *   simd_cnvrt_clip_SHORT_USHORT
 *   simd_cnvrt_clip_UBYTE_LONG
 *   simd_cnvrt_clip_USHORT_LONG
 *   simd_cnvrt_clip_ULONG_LONG
 *   simd_cnvrt_clip_BYTE_ULONG
 *   simd_cnvrt_clip_UBYTE_ULONG
 *   simd_cnvrt_clip_USHORT_ULONG
 *   simd_cnvrt_clip_LONG_ULONG
 */

/* SSE4.1 zero extend smaller types to larger types. */

static inline simd_USHORT_2 _ip_simd_cnvrt_USHORT_UBYTE(simd_UBYTE a)
{
    simd_USHORT_2 res;

    res.us[0] = _mm_cvtepu8_epi16(a);
    res.us[1] = _mm_cvtepu8_epi16(_mm_srli_si128(a, 8));
    return res;
}
#define simd_cnvrt_USHORT_UBYTE(x) _ip_simd_cnvrt_USHORT_UBYTE(x)

static inline simd_ULONG_4 _ip_simd_cnvrt_ULONG_UBYTE(simd_UBYTE a)
{
    simd_ULONG_4 res;

    res.ul[0] = _mm_cvtepu8_epi32(a);
    res.ul[1] = _mm_cvtepu8_epi32(_mm_srli_si128(a, 4));
    res.ul[2] = _mm_cvtepu8_epi32(_mm_srli_si128(a, 8));
    res.ul[3] = _mm_cvtepu8_epi32(_mm_srli_si128(a, 12));
    return res;
}
#define simd_cnvrt_ULONG_UBYTE(x) _ip_simd_cnvrt_ULONG_UBYTE(x)

static inline simd_ULONG_2 _ip_simd_cnvrt_ULONG_USHORT(simd_USHORT a)
{
    simd_ULONG_2 res;

    res.ul[0] = _mm_cvtepu16_epi32(a);
    res.ul[1] = _mm_cvtepu16_epi32(_mm_srli_si128(a, 8));
    return res;
}
#define simd_cnvrt_ULONG_USHORT(x) _ip_simd_cnvrt_ULONG_USHORT(x)


/* SSE4.1 signed extension of smaller type to larger. */

static inline simd_SHORT_2 _ip_simd_cnvrt_SHORT_BYTE(simd_BYTE a)
{
    simd_SHORT_2 res;

    res.s[0] = _mm_cvtepi8_epi16(a);
    res.s[1] = _mm_cvtepi8_epi16(_mm_srli_si128(a, 8));
    return res;
}
#define simd_cnvrt_SHORT_BYTE(x) _ip_simd_cnvrt_SHORT_BYTE(x)

static inline simd_LONG_4 _ip_simd_cnvrt_LONG_BYTE(simd_BYTE a)
{
    simd_LONG_4 res;

    res.l[0] = _mm_cvtepi8_epi32(a);
    res.l[1] = _mm_cvtepi8_epi32(_mm_srli_si128(a, 4));
    res.l[2] = _mm_cvtepi8_epi32(_mm_srli_si128(a, 8));
    res.l[3] = _mm_cvtepi8_epi32(_mm_srli_si128(a, 12));
    return res;
}
#define simd_cnvrt_LONG_BYTE(x) _ip_simd_cnvrt_LONG_BYTE(x)

static inline simd_LONG_2 _ip_simd_cnvrt_LONG_SHORT(simd_SHORT a)
{
    simd_LONG_2 res;

    res.l[0] = _mm_cvtepi16_epi32(a);
    res.l[1] = _mm_cvtepi16_epi32(_mm_srli_si128(a, 8));
    return res;
}
#define simd_cnvrt_LONG_SHORT(x) _ip_simd_cnvrt_LONG_SHORT(x)

/* SSE4.1 truncate larger type to shorter */

static inline simd_USHORT _ip_simd_cnvrt_USHORT_ULONG(simd_ULONG a, simd_ULONG b)
{
    const simd_ULONG mask = _mm_setr_epi32(0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff);

    a = _mm_and_si128(a, mask);
    b = _mm_and_si128(b, mask);
    return _mm_packus_epi32(a, b);
}
#define simd_cnvrt_USHORT_ULONG(a, b) _ip_simd_cnvrt_USHORT_ULONG(a, b)

/*
 * SSE4.1 convert clip (saturate) routines
 */

/* SSE4.1 convert signed byte to other types with clipping */

#define simd_cnvrt_clip_UBYTE_BYTE(a) _mm_max_epi8(a, _mm_setzero_si128())
#define simd_cnvrt_clip_USHORT_BYTE(a) \
    simd_cnvrt_USHORT_UBYTE(_mm_max_epi8(a, _mm_setzero_si128()))
#define simd_cnvrt_clip_ULONG_BYTE(a) \
    simd_cnvrt_ULONG_UBYTE(_mm_max_epi8(a, _mm_setzero_si128()))

/* SSE4.1 convert unsigned short to other types with clipping */

#define simd_cnvrt_clip_SHORT_USHORT(a) _mm_min_epu16(a, _mm_set1_epi16(INT16_MAX))

/* SSE4.1 convert signed long to other types with clipping */

#define simd_cnvrt_clip_ULONG_LONG(a) _mm_max_epi32(a, _mm_setzero_si128())
#define simd_cnvrt_clip_USHORT_LONG(a,b) _mm_packus_epi32(a,b)
#define simd_cnvrt_clip_UBYTE_LONG(a,b,c,d) \
    _ip_simd_cnvrt_clip_UBYTE_USHORT(_mm_packus_epi32(a,b), _mm_packus_epi32(c,d))

/* SSE4.1 convert unsigned long to other types with clipping */

static inline simd_BYTE _ip_simd_cnvrt_clip_BYTE_ULONG(simd_ULONG a, simd_ULONG b,
						       simd_ULONG c, simd_ULONG d)
{
    const simd_ULONG max_byte = simd_splat_ULONG(0x7f);
    simd_ULONG ta, tb, tc, td;

    ta = _mm_min_epu32(a, max_byte);
    tb = _mm_min_epu32(b, max_byte);
    tc = _mm_min_epu32(c, max_byte);
    td = _mm_min_epu32(d, max_byte);
    return (simd_BYTE)simd_cnvrt_UBYTE_ULONG(ta, tb, tc, td);
}
#define simd_cnvrt_clip_BYTE_ULONG(a,b,c,d) _ip_simd_cnvrt_clip_BYTE_ULONG(a,b,c,d)

static inline simd_UBYTE _ip_simd_cnvrt_clip_UBYTE_ULONG(simd_ULONG a, simd_ULONG b,
							 simd_ULONG c, simd_ULONG d)
{
    const simd_ULONG max_ubyte = simd_splat_ULONG(0xff);
    simd_ULONG ta, tb, tc, td;

    ta = _mm_min_epu32(a, max_ubyte);
    tb = _mm_min_epu32(b, max_ubyte);
    tc = _mm_min_epu32(c, max_ubyte);
    td = _mm_min_epu32(d, max_ubyte);
    return (simd_BYTE)simd_cnvrt_UBYTE_ULONG(ta, tb, tc, td);
}
#define simd_cnvrt_clip_UBYTE_ULONG(a,b,c,d) _ip_simd_cnvrt_clip_UBYTE_ULONG(a,b,c,d)

#define simd_cnvrt_clip_LONG_ULONG(a) _mm_min_epu32(a, _mm_set1_epi32(INT32_MAX))

static inline simd_USHORT _ip_simd_cnvrt_clip_USHORT_ULONG(simd_ULONG a, simd_ULONG b)
{
    const simd_ULONG vzero = _mm_setzero_si128();
    const simd_ULONG vmax = _mm_set1_epi32(0xffff);
    simd_ULONG sela, selb;

    sela = _mm_cmplt_epi32(a, vzero);
    a = v_select(sela, vmax, a);
    selb = _mm_cmplt_epi32(b, vzero);
    b = v_select(selb, vmax, b);
    return _mm_packus_epi32(a,b);
}
#define simd_cnvrt_clip_USHORT_ULONG(a,b) _ip_simd_cnvrt_clip_USHORT_ULONG(a,b)

#else

/*
 * SSE2 conversion implementations; can be compiled with SSE2 only, however
 * less efficient than the SSE4.1 implementations above.
 *
 * zero extension:
 *   simd_cnvrt_USHORT_UBYTE
 *   simd_cnvrt_ULONG_UBYTE
 *   simd_cnvrt_ULONG_USHORT
 * signed extension:
 *   simd_cnvrt_SHORT_BYTE
 *   simd_cnvrt_LONG_BYTE
 *   simd_cnvrt_LONG_SHORT
 * with clipping:
 *   simd_cnvrt_clip_UBYTE_BYTE
 *   simd_cnvrt_clip_USHORT_BYTE
 *   simd_cnvrt_clip_ULONG_BYTE
 *   simd_cnvrt_clip_SHORT_USHORT
 *   simd_cnvrt_clip_UBYTE_LONG
 *   simd_cnvrt_clip_USHORT_LONG
 *   simd_cnvrt_clip_ULONG_LONG
 *   simd_cnvrt_clip_BYTE_ULONG
 *   simd_cnvrt_clip_UBYTE_ULONG
 *   simd_cnvrt_clip_USHORT_ULONG
 *   simd_cnvrt_clip_LONG_ULONG
 */

/* SSE2 zero extend smaller integer to larger integer */

static inline simd_USHORT_2 _ip_simd_cnvrt_USHORT_UBYTE(simd_UBYTE a)
{
    simd_USHORT_2 res;

    res.us[0] = _mm_unpacklo_epi8(a, _mm_setzero_si128());
    res.us[1] = _mm_unpackhi_epi8(a, _mm_setzero_si128());
    return res;
}
#define simd_cnvrt_USHORT_UBYTE(x) _ip_simd_cnvrt_USHORT_UBYTE(x)

static inline simd_ULONG_4 _ip_simd_cnvrt_ULONG_UBYTE(simd_UBYTE a)
{
    simd_ULONG_4 res;
    simd_USHORT_2 s;
    const simd_USHORT vzero = _mm_setzero_si128();

    s = simd_cnvrt_USHORT_UBYTE(a);
    res.ul[0] = _mm_unpacklo_epi16(s.us[0], vzero);
    res.ul[1] = _mm_unpackhi_epi16(s.us[0], vzero);
    res.ul[2] = _mm_unpacklo_epi16(s.us[1], vzero);
    res.ul[3] = _mm_unpackhi_epi16(s.us[1], vzero);
    return res;
}
#define simd_cnvrt_ULONG_UBYTE(x) _ip_simd_cnvrt_ULONG_UBYTE(x)

static inline simd_ULONG_2 _ip_simd_cnvrt_ULONG_USHORT(simd_USHORT a)
{
    simd_ULONG_2 res;
    const simd_USHORT vzero = _mm_setzero_si128();

    res.ul[0] = _mm_unpacklo_epi16(a, vzero);
    res.ul[1] = _mm_unpackhi_epi16(a, vzero);
    return res;
}

#define simd_cnvrt_ULONG_USHORT(x) _ip_simd_cnvrt_ULONG_USHORT(x)

/* SSE2 sign extend smaller integer to larger integer */

static inline simd_SHORT_2 _ip_simd_cnvrt_SHORT_BYTE(simd_BYTE a)
{
    simd_SHORT_2 res;
    simd_BYTE sign;

    sign = _mm_cmplt_epi8(a, _mm_setzero_si128());
    res.s[0] = _mm_unpacklo_epi8(a, sign);
    res.s[1] = _mm_unpackhi_epi8(a, sign);
    return res;
}
#define simd_cnvrt_SHORT_BYTE(x) _ip_simd_cnvrt_SHORT_BYTE(x)

static inline simd_LONG_4 _ip_simd_cnvrt_LONG_BYTE(simd_BYTE a)
{
    simd_LONG_4 res;
    simd_BYTE sign;
    simd_SHORT_2 s;

    s = simd_cnvrt_SHORT_BYTE(a);
    sign = _mm_cmplt_epi16(s.s[0], _mm_setzero_si128());
    res.l[0] = _mm_unpacklo_epi16(s.s[0], sign);
    res.l[1] = _mm_unpackhi_epi16(s.s[0], sign);
    sign = _mm_cmplt_epi16(s.s[1], _mm_setzero_si128());
    res.l[2] = _mm_unpacklo_epi16(s.s[1], sign);
    res.l[3] = _mm_unpackhi_epi16(s.s[1], sign);
    return res;
}
#define simd_cnvrt_LONG_BYTE(x) _ip_simd_cnvrt_LONG_BYTE(x)

static inline simd_LONG_2 _ip_simd_cnvrt_LONG_SHORT(simd_SHORT a)
{
    simd_LONG_2 res;
    simd_SHORT sign;

    sign = _mm_cmplt_epi16(a, _mm_setzero_si128());
    res.l[0] = _mm_unpacklo_epi16(a, sign);
    res.l[1] = _mm_unpackhi_epi16(a, sign);
    return res;
}
#define simd_cnvrt_LONG_SHORT(x) _ip_simd_cnvrt_LONG_SHORT(x)

/* SSE2: truncate large type to smaller */

static inline simd_USHORT _ip_simd_cnvrt_USHORT_ULONG(simd_ULONG a, simd_ULONG b)
{
    a = _mm_srai_epi32(_mm_slli_epi32(a, 16), 16);
    b = _mm_srai_epi32(_mm_slli_epi32(b, 16), 16);
    return _mm_packs_epi32(a, b);
}
#define simd_cnvrt_USHORT_ULONG(a, b) _ip_simd_cnvrt_USHORT_ULONG(a, b)

/* SSE2: Convert BYTE to other types with clipping */

#define simd_cnvrt_clip_UBYTE_BYTE(a) sse2_v_max_epi8(a, _mm_setzero_si128())
#define simd_cnvrt_clip_USHORT_BYTE(a) \
    simd_cnvrt_USHORT_UBYTE(sse2_v_max_epi8(a, _mm_setzero_si128()))
#define simd_cnvrt_clip_ULONG_BYTE(a) \
    simd_cnvrt_ULONG_UBYTE(sse2_v_max_epi8(a, _mm_setzero_si128()))

/* SSE2: Convert USHORT to other types with clipping */

#define simd_cnvrt_clip_SHORT_USHORT(a) sse2_v_min_epu16(a, _mm_set1_epi16(INT16_MAX))

/* SSE2: convert LONG to other types with clipping */

static inline simd_UBYTE _ip_simd_cnvrt_clip_UBYTE_LONG(simd_LONG a, simd_LONG b,
							simd_LONG c, simd_LONG d)
{
    const simd_LONG max_ubyte = simd_splat_LONG(0xff);
    const simd_LONG min_ubyte = _mm_setzero_si128();
    simd_LONG ta, tb, tc, td;

    ta = sse2_v_max_epi32(a, min_ubyte);
    tb = sse2_v_max_epi32(b, min_ubyte);
    tc = sse2_v_max_epi32(c, min_ubyte);
    td = sse2_v_max_epi32(d, min_ubyte);
    ta = sse2_v_min_epi32(ta, max_ubyte);
    tb = sse2_v_min_epi32(tb, max_ubyte);
    tc = sse2_v_min_epi32(tc, max_ubyte);
    td = sse2_v_min_epi32(td, max_ubyte);
    return simd_cnvrt_UBYTE_ULONG(ta, tb, tc, td);
}
#define simd_cnvrt_clip_UBYTE_LONG(a,b,c,d) _ip_simd_cnvrt_clip_UBYTE_LONG(a,b,c,d)

static inline simd_USHORT _ip_simd_cnvrt_clip_USHORT_LONG(simd_LONG a, simd_LONG b)
{
    const simd_LONG max_ushort = simd_splat_ULONG(0xffff);
    const simd_LONG zero = _mm_setzero_si128();
    simd_LONG ta, tb;

    ta = sse2_v_max_epi32(a, zero);
    tb = sse2_v_max_epi32(b, zero);
    ta = sse2_v_min_epi32(ta, max_ushort);
    tb = sse2_v_min_epi32(tb, max_ushort);
    return simd_cnvrt_USHORT_ULONG(ta, tb);
}
#define simd_cnvrt_clip_USHORT_LONG(a, b) _ip_simd_cnvrt_clip_USHORT_LONG(a, b)

#define simd_cnvrt_clip_ULONG_LONG(a) sse2_v_max_epi32(a, _mm_setzero_si128())

/* SSE2 convert ULONG to other types with clipping */
static inline simd_BYTE _ip_simd_cnvrt_clip_BYTE_ULONG(simd_ULONG a, simd_ULONG b,
						       simd_ULONG c, simd_ULONG d)
{
    const simd_LONG max_byte = simd_splat_ULONG(0x7f);
    const simd_LONG zero = _mm_setzero_si128();
    simd_LONG ta, tb, tc, td;

    ta = v_select(_mm_cmplt_epi32(a, zero), max_byte, a);
    tb = v_select(_mm_cmplt_epi32(b, zero), max_byte, b);
    tc = v_select(_mm_cmplt_epi32(c, zero), max_byte, c);
    td = v_select(_mm_cmplt_epi32(d, zero), max_byte, d);
    ta = sse2_v_min_epi32(ta, max_byte);
    tb = sse2_v_min_epi32(tb, max_byte);
    tc = sse2_v_min_epi32(tc, max_byte);
    td = sse2_v_min_epi32(td, max_byte);
    return (simd_BYTE)simd_cnvrt_UBYTE_ULONG(ta, tb, tc, td);
}
#define simd_cnvrt_clip_BYTE_ULONG(a,b,c,d) _ip_simd_cnvrt_clip_BYTE_ULONG(a,b,c,d)

static inline simd_UBYTE _ip_simd_cnvrt_clip_UBYTE_ULONG(simd_ULONG a, simd_ULONG b,
							 simd_ULONG c, simd_ULONG d)
{
    const simd_LONG max_ubyte = simd_splat_ULONG(0xff);
    const simd_LONG zero = _mm_setzero_si128();
    simd_LONG ta, tb, tc, td;

    ta = v_select(_mm_cmplt_epi32(a, zero), max_ubyte, a);
    tb = v_select(_mm_cmplt_epi32(b, zero), max_ubyte, b);
    tc = v_select(_mm_cmplt_epi32(c, zero), max_ubyte, c);
    td = v_select(_mm_cmplt_epi32(d, zero), max_ubyte, d);
    ta = sse2_v_min_epi32(ta, max_ubyte);
    tb = sse2_v_min_epi32(tb, max_ubyte);
    tc = sse2_v_min_epi32(tc, max_ubyte);
    td = sse2_v_min_epi32(td, max_ubyte);
    return simd_cnvrt_UBYTE_ULONG(ta, tb, tc, td);
}
#define simd_cnvrt_clip_UBYTE_ULONG(a,b,c,d) _ip_simd_cnvrt_clip_UBYTE_ULONG(a,b,c,d)

static inline simd_USHORT _ip_simd_cnvrt_clip_USHORT_ULONG(simd_ULONG a, simd_ULONG b)
{
    const simd_ULONG max_ushort = simd_splat_ULONG(0xffff);
    const simd_ULONG zero = _mm_setzero_si128();
    simd_ULONG ta, tb;

    ta = v_select(_mm_cmplt_epi32(a, zero), max_ushort, a);
    tb = v_select(_mm_cmplt_epi32(b, zero), max_ushort, b);
    ta = sse2_v_min_epi32(ta, max_ushort);
    tb = sse2_v_min_epi32(tb, max_ushort);
    return simd_cnvrt_USHORT_ULONG(ta, tb);
}
#define simd_cnvrt_clip_USHORT_ULONG(a, b) _ip_simd_cnvrt_clip_USHORT_ULONG(a, b)

static inline simd_LONG _ip_simd_cnvrt_clip_LONG_ULONG(simd_ULONG a)
{
    const simd_LONG max_long = simd_splat_ULONG(INT32_MAX);
    const simd_LONG zero = _mm_setzero_si128();
    simd_LONG ta;

    ta = v_select(_mm_cmplt_epi32(a, zero), max_long, a);
    return sse2_v_min_epi32(ta, max_long);
}
#define simd_cnvrt_clip_LONG_ULONG(a) _ip_simd_cnvrt_clip_LONG_ULONG(a)

#endif


/*
 * Back to COMMON conversion implementations, namely conversions to/from
 * floating point and conversions from BINARY.
 */

/*
 * Integer (32-bit) to float conversions and back again.
 */

/* COMMON: LONG/FLOAT conversions */
#define simd_cnvrt_FLOAT_LONG(x) _mm_cvtepi32_ps(x)
#define simd_cnvrt_LONG_FLOAT(x) _mm_cvttps_epi32(x)

/*
 * Converting float to integer with rounding.
 * We want conventional rounding, not IEEE rounding, thus the
 * Intel intrinsics for rounding conversions from float to integer
 * don't suffice.
 */
static inline simd_LONG _ip_simd_cnvrt_rnd_LONG_FLOAT(simd_FLOAT x)
{
    const simd_FLOAT signbit = (simd_FLOAT)simd_splat_ULONG(0x80000000U);
    simd_FLOAT half = simd_splat_FLOAT(0x1.fffffep-2); /* ohalf = nextafterf(0.5, 0) */
    simd_FLOAT sign = _mm_and_ps(x, signbit);
    x = _mm_add_ps(x, _mm_or_ps(half, sign));    /* x + copysignf(ohalf, x) */
    return _mm_cvttps_epi32(x);
}
#define simd_cnvrt_rnd_LONG_FLOAT(x) _ip_simd_cnvrt_rnd_LONG_FLOAT(x)

/* Helper function: convert double to a half-length long with rounding. */
static inline __m64 sse2_rnd_LONG_DOUBLE(simd_DOUBLE x)
{
    const simd_DOUBLE signbit = (__m128d)_mm_set1_epi64x(0x8000000000000000UL);
    simd_DOUBLE half = simd_splat_DOUBLE(0x1.fffffffffffffp-2); /* ohalf = nextafter(0.5, 0) */
    simd_DOUBLE sign = _mm_and_pd(x, signbit);
    x = _mm_add_pd(x, _mm_or_pd(half, sign));		/* x + copysign(ohalf, x) */
    return _mm_cvttpd_pi32(x);
}

static inline simd_LONG _ip_simd_cnvrt_rnd_LONG_DOUBLE(simd_DOUBLE x, simd_DOUBLE y)
{
    __m64 left = sse2_rnd_LONG_DOUBLE(x);
    __m64 right = sse2_rnd_LONG_DOUBLE(y);
    return _mm_set_epi64(right, left);
}
#define simd_cnvrt_rnd_LONG_DOUBLE(x,y) _ip_simd_cnvrt_rnd_LONG_DOUBLE(x,y)



/*
 * The conversion of FLOAT to LONG with clipping is subtle, because the
 * value of INT32_MAX is not exactly representable in single precision
 * floating point.  If it is assigned to the float it becomes INT32_MAX+1
 * (the nearest available float value) which then becomes INT32_MIN when
 * converted to int32_t!  We therefore use a comparison and vselect() to
 * assign INT32_MAX to the result vector when clipping at the max value.
 */

static inline simd_LONG _ip_simd_cnvrt_clip_LONG_FLOAT(simd_FLOAT x)
{
    __m128i toobig;
    simd_LONG res;
    /* INT32_MIN is exactly representable in float so this is safe */
    x = _mm_max_ps(x, _mm_set1_ps(INT32_MIN));
    /* But INT32_MAX is not... */
    toobig = (__m128i)_mm_cmpge_ps(x, _mm_set1_ps(INT32_MAX));
    res = simd_cnvrt_LONG_FLOAT(x);
    return v_select(toobig, _mm_set1_epi32(INT32_MAX), res);
}
#define simd_cnvrt_clip_LONG_FLOAT(x) _ip_simd_cnvrt_clip_LONG_FLOAT(x)

static inline simd_LONG _ip_simd_cnvrt_rnd_clip_LONG_FLOAT(simd_FLOAT x)
{
    __m128i toobig;
    simd_LONG res;
    x = _mm_max_ps(x, _mm_set1_ps(INT32_MIN));
    toobig = (__m128i)_mm_cmpge_ps(x, _mm_set1_ps(INT32_MAX));
    res = simd_cnvrt_rnd_LONG_FLOAT(x);
    return v_select(toobig, _mm_set1_epi32(INT32_MAX), res);
}
#define simd_cnvrt_rnd_clip_LONG_FLOAT(x) _ip_simd_cnvrt_rnd_clip_LONG_FLOAT(x)

/* COMMON: LONG/DOUBLE conversions */
static inline simd_DOUBLE_2 _ip_simd_cnvrt_DOUBLE_LONG(simd_LONG x)
{
    simd_DOUBLE_2 res;

    res.d[0] = _mm_cvtepi32_pd(x);
    res.d[1] = _mm_cvtepi32_pd(_mm_srli_si128(x, 8));
    return res;
}

#define simd_cnvrt_DOUBLE_LONG(x) _ip_simd_cnvrt_DOUBLE_LONG(x)

static inline simd_LONG _ip_simd_cnvrt_LONG_DOUBLE(simd_DOUBLE x, simd_DOUBLE y)
{
    return _mm_or_si128(_mm_cvttpd_epi32(x), _mm_slli_si128(_mm_cvttpd_epi32(y), 8));
}
#define simd_cnvrt_LONG_DOUBLE(x,y) _ip_simd_cnvrt_LONG_DOUBLE(x,y)

static inline simd_LONG _ip_simd_cnvrt_clip_LONG_DOUBLE(simd_DOUBLE x, simd_DOUBLE y)
{
    x = _mm_min_pd(x, _mm_set1_pd(INT32_MAX));
    x = _mm_max_pd(x, _mm_set1_pd(INT32_MIN));
    y = _mm_min_pd(y, _mm_set1_pd(INT32_MAX));
    y = _mm_max_pd(y, _mm_set1_pd(INT32_MIN));
    return simd_cnvrt_LONG_DOUBLE(x, y);
}
#define simd_cnvrt_clip_LONG_DOUBLE(x,y) _ip_simd_cnvrt_clip_LONG_DOUBLE(x,y)

static inline simd_LONG _ip_simd_cnvrt_rnd_clip_LONG_DOUBLE(simd_DOUBLE x, simd_DOUBLE y)
{
    const simd_DOUBLE vint32_max = _mm_set1_pd(INT32_MAX);
    const simd_DOUBLE vint32_min = _mm_set1_pd(INT32_MIN);
    x = _mm_min_pd(x, vint32_max);
    x = _mm_max_pd(x, vint32_min);
    y = _mm_min_pd(y, vint32_max);
    y = _mm_max_pd(y, vint32_min);
    return simd_cnvrt_rnd_LONG_DOUBLE(x, y);
}
#define simd_cnvrt_rnd_clip_LONG_DOUBLE(x,y) _ip_simd_cnvrt_rnd_clip_LONG_DOUBLE(x,y)

/* COMMON: FLOAT/DOUBLE conversions */
static inline simd_DOUBLE_2 _ip_simd_cnvrt_DOUBLE_FLOAT(simd_FLOAT x)
{
    simd_DOUBLE_2 res;

    res.d[0] = _mm_cvtps_pd(x);
    res.d[1] = _mm_cvtps_pd((simd_FLOAT)_mm_srli_si128((__m128i)x, 8));
    return res;
}

#define simd_cnvrt_DOUBLE_FLOAT(x) _ip_simd_cnvrt_DOUBLE_FLOAT(x)

static inline simd_FLOAT _ip_simd_cnvrt_FLOAT_DOUBLE(simd_DOUBLE x, simd_DOUBLE y)
{
    return _mm_or_ps(_mm_cvtpd_ps(x), (__m128)_mm_slli_si128((__m128i)_mm_cvtpd_ps(y), 8));
}

#define simd_cnvrt_FLOAT_DOUBLE(x,y) _ip_simd_cnvrt_FLOAT_DOUBLE(x,y)

static inline simd_FLOAT _ip_simd_cnvrt_clip_FLOAT_DOUBLE(simd_DOUBLE x, simd_DOUBLE y)
{
    x = _mm_max_pd(x, _mm_set1_pd(-FLT_MAX));
    x = _mm_min_pd(x, _mm_set1_pd(FLT_MAX));
    y = _mm_max_pd(y, _mm_set1_pd(-FLT_MAX));
    y = _mm_min_pd(y, _mm_set1_pd(FLT_MAX));
    return simd_cnvrt_FLOAT_DOUBLE(x,y);
}

#define simd_cnvrt_clip_FLOAT_DOUBLE(x,y) _ip_simd_cnvrt_clip_FLOAT_DOUBLE(x,y)

/* COMMON: conversions from BINARY */

static inline simd_UBYTE _ip_simd_cnvrt_UBYTE_BINARY(simd_UBYTE x, int tval)
{
    const simd_UBYTE vfalse = simd_splat_UBYTE(0);
    simd_UBYTE vtrue = simd_splat_UBYTE(tval);
    return v_select(x, vtrue, vfalse);
}
#define simd_cnvrt_UBYTE_BINARY(x, tv) _ip_simd_cnvrt_UBYTE_BINARY(x, tv)

static inline simd_USHORT_2 _ip_simd_cnvrt_USHORT_BINARY(simd_UBYTE x, int tval)
{
    const simd_UBYTE vfalse = simd_splat_UBYTE(0);
    simd_UBYTE vtrue = simd_splat_UBYTE(tval);
    x = v_select(x, vtrue, vfalse);
    return simd_cnvrt_USHORT_UBYTE(x);
}
#define simd_cnvrt_USHORT_BINARY(x, tv) _ip_simd_cnvrt_USHORT_BINARY(x, tv)

static inline simd_ULONG_4 _ip_simd_cnvrt_ULONG_BINARY(simd_UBYTE x, int tval)
{
    const simd_UBYTE vfalse = simd_splat_UBYTE(0);
    simd_UBYTE vtrue = simd_splat_UBYTE(tval);
    x = v_select(x, vtrue, vfalse);
    return simd_cnvrt_ULONG_UBYTE(x);
}
#define simd_cnvrt_ULONG_BINARY(x, tv) _ip_simd_cnvrt_ULONG_BINARY(x, tv)

/*
 * Colour (RGB) conversions
 */

#ifdef HAVE_INTEL_SSSE3
/* Repack 3 RGB vectors into 4 RGBA vectors */
static inline simd_UBYTE_4 ip_simd_cnvrt_RGBA_RGB(simd_UBYTE a, simd_UBYTE b, simd_UBYTE c)
{
    simd_UBYTE_4 y;
    const simd_UBYTE sel_a = _mm_set_epi8(0x80, 11, 10, 9, 0x80, 8, 7, 6,
					  0x80,  5,  4, 3, 0x80, 2, 1, 0);
    const simd_UBYTE sel_b1 = _mm_set_epi8(0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80,
					   0x80, 0x80, 0x80,   15, 0x80,   14,   13,   12);
    const simd_UBYTE sel_b2 = _mm_set_epi8(0x80,    7,    6,    5, 0x80,    4,    3,    2,
					   0x80,    1,    0, 0x80, 0x80, 0x80, 0x80, 0x80);
    const simd_UBYTE sel_c1 = _mm_set_epi8(0x80, 0x80, 0x80, 0x80, 0x80, 0x80,   15,   14,
					   0x80,   13,   12,   11, 0x80,   10,    9,    8);
    const simd_UBYTE sel_c2 = _mm_set_epi8(0x80,    3,    2,    1, 0x80,    0, 0x80, 0x80,
					   0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80);
    const simd_UBYTE sel_d = _mm_set_epi8(0x80, 15, 14, 13, 0x80, 12, 11, 10,
					  0x80, 9,   8,  7, 0x80,  6,  5,  4);
    const simd_UBYTE set_alpha_255 = _mm_set_epi8(0xff, 0, 0, 0, 0xff, 0, 0, 0,
						  0xff, 0, 0, 0, 0xff, 0, 0, 0);

    y.ub[0] = _mm_shuffle_epi8(a, sel_a);
    y.ub[0] = _mm_or_si128(y.ub[0], set_alpha_255);
    y.ub[1] = _mm_or_si128(_mm_shuffle_epi8(a, sel_b1),
			  _mm_shuffle_epi8(b, sel_b2));
    y.ub[1] = _mm_or_si128(y.ub[1], set_alpha_255);
    y.ub[2] = _mm_or_si128(_mm_shuffle_epi8(b, sel_c1),
			  _mm_shuffle_epi8(c, sel_c2));
    y.ub[2] = _mm_or_si128(y.ub[2], set_alpha_255);
    y.ub[3] = _mm_shuffle_epi8(c, sel_d);
    y.ub[3] = _mm_or_si128(y.ub[3], set_alpha_255);

    return y;
}
#define simd_cnvrt_RGBA_RGB(a,b,c) ip_simd_cnvrt_RGBA_RGB(a,b,c)

/* Repack 4 RGBA vectors into 3 RGB vectors */
static inline simd_UBYTE_3 ip_simd_cnvrt_RGB_RGBA(simd_UBYTE a, simd_UBYTE b,
						  simd_UBYTE c, simd_UBYTE d)
{
    simd_UBYTE_3 y;
    const simd_UBYTE sel_a1 = _mm_set_epi8(-1, -1, -1, -1, 14, 13, 12, 10,
					    9,  8,  6,  5,  4,  2,  1,  0);
    const simd_UBYTE sel_a2 = _mm_set_epi8( 4,  2,  1,  0, -1, -1, -1, -1,
					   -1, -1, -1, -1, -1, -1, -1, -1);

    const simd_UBYTE sel_b1 = _mm_set_epi8(-1, -1, -1, -1, -1, -1, -1, -1,
					   14, 13, 12, 10,  9,  8,  6,  5);
    const simd_UBYTE sel_b2 = _mm_set_epi8( 9,  8,  6,  5,  4,  2,  1,  0,
					   -1, -1, -1, -1, -1, -1, -1, -1);

    const simd_UBYTE sel_c1 = _mm_set_epi8(-1, -1, -1, -1, -1, -1, -1, -1,
					   -1, -1, -1, -1, 14, 13, 12, 10);
    const simd_UBYTE sel_c2 = _mm_set_epi8(14, 13, 12, 10,  9,  8,  6,  5,
					    4,  2,  1,  0, -1, -1, -1, -1);

    y.ub[0] = _mm_or_si128(_mm_shuffle_epi8(a, sel_a1),
			   _mm_shuffle_epi8(b, sel_a2));
    y.ub[1] = _mm_or_si128(_mm_shuffle_epi8(b, sel_b1),
			   _mm_shuffle_epi8(c, sel_b2));
    y.ub[2] = _mm_or_si128(_mm_shuffle_epi8(c, sel_c1),
			   _mm_shuffle_epi8(d, sel_c2));


    return y;
}
#define simd_cnvrt_RGB_RGBA(a,b,c,d) ip_simd_cnvrt_RGB_RGBA(a,b,c,d)
#endif

static inline simd_UBYTE ip_simd_cnvrt_RGB_RGB16(simd_USHORT x, simd_USHORT y)
{
    return simd_cnvrt_UBYTE_USHORT(_mm_srli_epi16(x, 8),
				   _mm_srli_epi16(y, 8));
}
#define simd_cnvrt_RGB_RGB16(x,y) ip_simd_cnvrt_RGB_RGB16(x,y)

static inline simd_USHORT_2 ip_simd_cnvrt_RGB16_RGB(simd_UBYTE x)
{
    simd_USHORT_2 r16 = simd_cnvrt_USHORT_UBYTE(x);
    r16.us[0] = _mm_or_si128(r16.us[0], _mm_slli_epi16(r16.us[0], 8));
    r16.us[1] = _mm_or_si128(r16.us[1], _mm_slli_epi16(r16.us[1], 8));
    return r16;
}
#define simd_cnvrt_RGB16_RGB(x) ip_simd_cnvrt_RGB16_RGB(x)



/* Logical operators on the SIMD vectors */
#define simd_or_UBYTE(a,b) _mm_or_si128(a,b)
#define simd_or_BYTE(a,b) _mm_or_si128(a,b)
#define simd_or_USHORT(a,b) _mm_or_si128(a,b)
#define simd_or_SHORT(a,b) _mm_or_si128(a,b)
#define simd_or_ULONG(a,b) _mm_or_si128(a,b)
#define simd_or_LONG(a,b) _mm_or_si128(a,b)

#define simd_and_UBYTE(a,b) _mm_and_si128(a,b)
#define simd_and_BYTE(a,b) _mm_and_si128(a,b)
#define simd_and_USHORT(a,b) _mm_and_si128(a,b)
#define simd_and_SHORT(a,b) _mm_and_si128(a,b)
#define simd_and_ULONG(a,b) _mm_and_si128(a,b)
#define simd_and_LONG(a,b) _mm_and_si128(a,b)

#define simd_xor_UBYTE(a,b) _mm_xor_si128(a,b)
#define simd_xor_BYTE(a,b) _mm_xor_si128(a,b)
#define simd_xor_USHORT(a,b) _mm_xor_si128(a,b)
#define simd_xor_SHORT(a,b) _mm_xor_si128(a,b)
#define simd_xor_ULONG(a,b) _mm_xor_si128(a,b)
#define simd_xor_LONG(a,b) _mm_xor_si128(a,b)

#define simd_not_UBYTE(a) _mm_andnot_si128(a, simd_splat_UBYTE(-1))
#define simd_not_BYTE(a) _mm_andnot_si128(a, simd_splat_BYTE(-1))
#define simd_not_USHORT(a) _mm_andnot_si128(a, simd_splat_USHORT(-1))
#define simd_not_SHORT(a) _mm_andnot_si128(a, simd_splat_SHORT(-1))
#define simd_not_ULONG(a) _mm_andnot_si128(a, simd_splat_ULONG(-1))
#define simd_not_LONG(a) _mm_andnot_si128(a, simd_splat_LONG(-1))

/*
 * Shifts
 */

/* Per element shifts */
#define simd_shlc_SHORT(x,s) _mm_sll_epi16(x,s)
#define simd_shlc_USHORT(x,s) _mm_sll_epi16(x,s)
#define simd_shlc_LONG(x,s) _mm_sll_epi32(x,s)
#define simd_shlc_ULONG(x,s) _mm_sll_epi32(x,s)

#define simd_shl_imm_SHORT(x,c) _mm_slli_epi16(x,c)
#define simd_shl_imm_USHORT(x,c) _mm_slli_epi16(x,c)
#define simd_shl_imm_LONG(x,c) _mm_slli_epi32(x,c)
#define simd_shl_imm_ULONG(x,c) _mm_slli_epi32(x,c)

#define simd_shr_imm_SHORT(x,c) _mm_srli_epi16(x,c)
#define simd_shr_imm_USHORT(x,c) _mm_srli_epi16(x,c)
#define simd_shr_imm_LONG(x,c) _mm_srli_epi32(x,c)
#define simd_shr_imm_ULONG(x,c) _mm_srli_epi32(x,c)

#define simd_shra_imm_SHORT(x,c) _mm_srai_epi16(x,c)
#define simd_shra_imm_USHORT(x,c) _mm_srai_epi16(x,c)
#define simd_shra_imm_LONG(x,c) _mm_srai_epi32(x,c)
#define simd_shra_imm_ULONG(x,c) _mm_srai_epi32(x,c)


#ifdef HAVE_INTEL_SSSE3
static inline __m128i _ip_simd_ror_RGB(__m128i a)
{
    const __m128i b = _mm_set_epi8(13, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1);
    return _mm_shuffle_epi8(a, b);
}
static inline __m128i _ip_simd_ror_RGB16(__m128i a)
{
    const __m128i b = _mm_set_epi8(13, 12, 11, 10, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4);
    return _mm_shuffle_epi8(a, b);
}
#else
static inline __m128i _ip_simd_ror_RGB(__m128i a)
{
    const __m128i mask = _mm_set_epi8(0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
    __m128i t = _mm_slli_si128(_mm_and_si128(a, mask), 2);
    return _mm_or_si128(_mm_srli_si128(a, 1), t);
}
static inline __m128i _ip_simd_ror_RGB16(__m128i a)
{
    const __m128i mask = _mm_set_epi16(0, -1, -1, 0, 0, 0, 0, 0);
    __m128i t = _mm_slli_si128(_mm_and_si128(a, mask), 2);
    return _mm_or_si128(_mm_srli_si128(a, 4), t);
}
#endif

#define simd_ror_RGB(x) _ip_simd_ror_RGB(x)
#define simd_ror_RGB16(x) _ip_simd_ror_RGB16(x)




/* Basic arithmetic operators */
/* Some still to do in multiplication and division */

/* addition */
#define simd_add_UBYTE(d,s)  _mm_add_epi8(d,s)
#define simd_add_BYTE(d,s)   _mm_add_epi8(d,s)
#define simd_add_USHORT(d,s) _mm_add_epi16(d,s)
#define simd_add_SHORT(d,s)  _mm_add_epi16(d,s)
#define simd_add_ULONG(d,s)  _mm_add_epi32(d,s)
#define simd_add_LONG(d,s)   _mm_add_epi32(d,s)
#define simd_add_ULONG64(d,s)  _mm_add_epi64(d,s)
#define simd_add_LONG64(d,s)   _mm_add_epi64(d,s)
#define simd_add_FLOAT(d,s)  _mm_add_ps(d,s)
#define simd_add_DOUBLE(d,s) _mm_add_pd(d,s)
#define simd_add_COMPLEX(d,s) _mm_add_ps(d,s)
#define simd_add_DCOMPLEX(d,s) _mm_add_pd(d,s)

/* addition with saturation */
#define simd_add_clipped_UBYTE(d,s) _mm_adds_epu8(d,s)
#define simd_add_clipped_BYTE(d,s) _mm_adds_epi8(d,s)
#define simd_add_clipped_USHORT(d,s) _mm_adds_epu16(d,s)
#define simd_add_clipped_SHORT(d,s) _mm_adds_epi16(d,s)

/* subtraction */
#define simd_sub_UBYTE(d,s)  _mm_sub_epi8(d,s)
#define simd_sub_BYTE(d,s)   _mm_sub_epi8(d,s)
#define simd_sub_USHORT(d,s) _mm_sub_epi16(d,s)
#define simd_sub_SHORT(d,s)  _mm_sub_epi16(d,s)
#define simd_sub_ULONG(d,s)  _mm_sub_epi32(d,s)
#define simd_sub_LONG(d,s)   _mm_sub_epi32(d,s)
#define simd_sub_ULONG64(d,s)  _mm_sub_epi64(d,s)
#define simd_sub_LONG64(d,s)   _mm_sub_epi64(d,s)
#define simd_sub_FLOAT(d,s)  _mm_sub_ps(d,s)
#define simd_sub_DOUBLE(d,s) _mm_sub_pd(d,s)
#define simd_sub_COMPLEX(d,s)  _mm_sub_ps(d,s)
#define simd_sub_DCOMPLEX(d,s) _mm_sub_pd(d,s)

/* subtraction with saturation */
#define simd_sub_clipped_UBYTE(d,s)  _mm_subs_epu8(d,s)
#define simd_sub_clipped_BYTE(d,s)   _mm_subs_epi8(d,s)
#define simd_sub_clipped_USHORT(d,s) _mm_subs_epu16(d,s)
#define simd_sub_clipped_SHORT(d,s)  _mm_subs_epi16(d,s)

/* multiplication */
static inline simd_UBYTE _ip_simd_mul_UBYTE(simd_UBYTE d, simd_UBYTE s)
{
    simd_USHORT_2 td, ts;
    td = simd_cnvrt_USHORT_UBYTE(d);
    ts = simd_cnvrt_USHORT_UBYTE(s);
    td.us[0] = _mm_mullo_epi16(td.us[0], ts.us[0]);
    td.us[1] = _mm_mullo_epi16(td.us[1], ts.us[1]);
    return simd_cnvrt_UBYTE_USHORT(td.us[0], td.us[1]);
}
#define simd_mul_BYTE(d,s) _ip_simd_mul_UBYTE(d,s)
#define simd_mul_UBYTE(d,s) _ip_simd_mul_UBYTE(d,s)
#define simd_mul_USHORT(d,s) _mm_mullo_epi16(d,s)
#define simd_mul_SHORT(d,s)  _mm_mullo_epi16(d,s)
#ifdef HAVE_INTEL_SSE41
#define simd_mul_ULONG(d,s) _mm_mullo_epi32(d,s)
#define simd_mul_LONG(d,s)  _mm_mullo_epi32(d,s)
#else
static inline simd_ULONG _ip_simd_mul_ULONG(simd_ULONG d, simd_ULONG s)
{
    simd_ULONG x1, x2;
    const simd_ULONG mask = _mm_set_epi32(0, 0xffffffff, 0, 0xffffffff);
    x1 = _mm_mul_epu32(d, s);
    x2 = _mm_mul_epu32(_mm_srli_si128(d, 4), _mm_srli_si128(s,4));
    x1 = _mm_and_si128(x1, mask);
    x2 = _mm_and_si128(x2, mask);
    return  _mm_or_si128(x1, _mm_slli_si128(x2, 4));
}
#define simd_mul_LONG(d,s) _ip_simd_mul_ULONG(d,s)
#define simd_mul_ULONG(d,s) _ip_simd_mul_ULONG(d,s)
#endif

#define simd_mul_FLOAT(d,s)  _mm_mul_ps(d,s)
#define simd_mul_DOUBLE(d,s) _mm_mul_pd(d,s)

#ifdef HAVE_INTEL_SSE3
static inline __m128 __ip_multiply_COMPLEX(__m128 d, __m128 s)
{
    __m128 t1, t2, t3;		    /* N.B. d = [d2i d2r d1i d1r], s likewise */

    t1 = _mm_moveldup_ps(d);	    /* [d2r d2r d1r d1r] */
    t2 = _mm_movehdup_ps(d);	    /* [d2i d2i d1i d1i] */
    t3 = _mm_shuffle_ps(s, s, 0xb1); /* [s2r s2i s1r s1i] */
    t1 = _mm_mul_ps(t1, s);	    /* [d2r*s2i d2r*s2r d1r*s1i d1r*s1r] */
    t2 = _mm_mul_ps(t2, t3);	    /* [d2i*s2r d2i*s2i d1i*s1r d1i*s1i] */
    return _mm_addsub_ps(t1, t2); /* add/sub to get complex multiplication */
}
#define simd_mul_COMPLEX(d,s) __ip_multiply_COMPLEX(d,s)

static inline __m128d __ip_multiply_DCOMPLEX(__m128d d, __m128d s)
{
    __m128d t1, t2, t3;		    /* N.B. d = [di dr], s likewise */

    t1 = _mm_movedup_pd(d);	    /* [dr dr] */
    t2 = _mm_shuffle_pd(d, d, 0x3); /* [di di] */
    t3 = _mm_shuffle_pd(s, s, 0x1); /* [sr si] */
    t1 = _mm_mul_pd(t1, s);	    /* [dr*si dr*sr] */
    t2 = _mm_mul_pd(t2, t3);	    /* [di*sr di*si] */
    return _mm_addsub_pd(t1, t2); /* add/sub to get complex multiplication */
}
#define simd_mul_DCOMPLEX(d,s) __ip_multiply_DCOMPLEX(d,s)
#endif

/* multiplication with saturation */
static inline simd_UBYTE _ip_simd_mul_clipped_UBYTE(simd_UBYTE d, simd_UBYTE s)
{
    simd_USHORT_2 td, ts;
    const simd_USHORT ubyte_max = simd_splat_USHORT(0xff);
    td = simd_cnvrt_USHORT_UBYTE(d);
    ts = simd_cnvrt_USHORT_UBYTE(s);
    td.us[0] = _mm_mullo_epi16(td.us[0], ts.us[0]);
    td.us[1] = _mm_mullo_epi16(td.us[1], ts.us[1]);
#ifdef HAVE_INTEL_SSE41
    td.us[0] = _mm_min_epu16(td.us[0], ubyte_max);
    td.us[1] = _mm_min_epu16(td.us[1], ubyte_max);
#else
    td.us[0] = sse2_v_min_epu16(td.us[0], ubyte_max);
    td.us[1] = sse2_v_min_epu16(td.us[1], ubyte_max);
#endif
    return simd_cnvrt_UBYTE_USHORT(td.us[0], td.us[1]);
}
#define simd_mul_clipped_UBYTE(d,s) _ip_simd_mul_clipped_UBYTE(d,s)

static inline simd_BYTE _ip_simd_mul_clipped_BYTE(simd_BYTE d, simd_BYTE s)
{
    simd_SHORT_2 td, ts;
    const simd_SHORT byte_max = simd_splat_SHORT(127);
    const simd_SHORT byte_min = simd_splat_SHORT(-128);
    td = simd_cnvrt_SHORT_BYTE(d);
    ts = simd_cnvrt_SHORT_BYTE(s);
    td.s[0] = _mm_mullo_epi16(td.s[0], ts.s[0]);
    td.s[1] = _mm_mullo_epi16(td.s[1], ts.s[1]);
    td.s[0] = _mm_min_epi16(td.s[0], byte_max);
    td.s[1] = _mm_min_epi16(td.s[1], byte_max);
    td.s[0] = _mm_max_epi16(td.s[0], byte_min);
    td.s[1] = _mm_max_epi16(td.s[1], byte_min);
    return simd_cnvrt_UBYTE_USHORT(td.s[0], td.s[1]);
}
#define simd_mul_clipped_BYTE(d,s) _ip_simd_mul_clipped_BYTE(d,s)

static inline simd_USHORT _ip_simd_mul_clipped_USHORT(simd_USHORT d, simd_USHORT s)
{
    simd_SHORT hi = _mm_mulhi_epu16(d, s);
    simd_SHORT lo = _mm_mullo_epi16(d, s);
    simd_SHORT good = _mm_cmpeq_epi16(hi, _mm_setzero_si128());
    return v_select(good, lo, simd_splat_USHORT(UINT16_MAX));
}
#define simd_mul_clipped_USHORT(d,s) _ip_simd_mul_clipped_USHORT(d,s)

static inline simd_SHORT _ip_simd_mul_clipped_SHORT(simd_SHORT d, simd_SHORT s)
{
    simd_SHORT under;
    simd_SHORT hi = _mm_mulhi_epi16(d, s);
    simd_SHORT lo = _mm_mullo_epi16(d, s);
    simd_SHORT hi_neg = _mm_cmpgt_epi16(_mm_setzero_si128(), hi);
    simd_SHORT hi_zero = _mm_cmpeq_epi16(_mm_setzero_si128(), hi);
    simd_SHORT hi_minus1 = _mm_cmpeq_epi16(simd_splat_SHORT(-1), hi);
    simd_SHORT over = _mm_cmplt_epi16(_mm_setzero_si128(), hi);
    simd_SHORT lo_neg = _mm_cmpgt_epi16(_mm_setzero_si128(), lo);
    over = _mm_or_si128(over, _mm_and_si128(hi_zero, lo_neg));
    under = _mm_or_si128(_mm_andnot_si128(hi_minus1, hi_neg), _mm_andnot_si128(lo_neg, hi_minus1));
    lo = v_select(under, simd_splat_SHORT(INT16_MIN), lo);
    lo = v_select(over, simd_splat_SHORT(INT16_MAX), lo);
    return lo;
 }
#define simd_mul_clipped_SHORT(d,s) _ip_simd_mul_clipped_SHORT(d,s)


/*
 * simd_mulhi
 *
 * Return upper N-bits of N-bit by N-bit multiplication.
 */

#define simd_mulhi_USHORT(a,b) _mm_mulhi_epu16(a,b)
#define simd_mulhi_SHORT(a,b) _mm_mulhi_epi16(a,b)

static inline simd_ULONG _ip_simd_mulhi_ULONG(simd_ULONG a, simd_ULONG b)
{
    simd_ULONG x1, x2, res;

    x1 = _mm_mul_epu32(a, b);
    x2 = _mm_mul_epu32(_mm_srli_si128(a, 4), _mm_srli_si128(b, 4));
#ifdef HAVE_INTEL_SSE41
    res = _mm_blend_epi16(_mm_srli_si128(x1,4), x2, 0xcc);
#else
    const simd_ULONG mask = _mm_set_epi32(0xffffffff, 0, 0xffffffff, 0);
    x1 = _mm_and_si128(x1, mask);
    x2 = _mm_and_si128(x2, mask);
    res =  _mm_or_si128(x2, _mm_srli_si128(x1, 4));
#endif
    return res;
}
#define simd_mulhi_ULONG(a,b) _ip_simd_mulhi_ULONG(a,b)

#ifdef HAVE_INTEL_SSE41
static inline simd_LONG _ip_simd_mulhi_LONG(simd_LONG a, simd_LONG b)
{
    simd_LONG x1, x2;

    x1 = _mm_mul_epi32(a, b);
    x2 = _mm_mul_epi32(_mm_srli_si128(a, 4), _mm_srli_si128(b, 4));
    return _mm_blend_epi16(_mm_srli_si128(x1, 4), x2, 0xcc);
}
#else
/*
 * Let s(x) = 1 if x < 0; 0 otherwise, then
 *   mulhi_LONG(a,b) = mulhi_ULONG(a,b) - s(a)b - s(b)a.
 *
 * This is only two cycles slower then mulhi_ULONG because the two terms to
 * subtract can be calculated in the shadow of mulhi_ULONG, leaving only the
 * two substractions themselves to be performed at the completion of the
 * mulhi_ULONG.
 */
static inline simd_LONG _ip_simd_mulhi_LONG(simd_LONG a, simd_LONG b)
{
    simd_LONG sel;
    simd_LONG umulh = simd_mulhi_ULONG(a, b);
    const simd_LONG zero = simd_splat_LONG(0);

    sel = _mm_cmplt_epi32(a, zero);
    umulh = simd_sub_LONG(umulh, v_select(sel, b, zero));
    sel = _mm_cmplt_epi32(b, zero);
    return simd_sub_LONG(umulh, v_select(sel, a, zero));
}
#endif
#define simd_mulhi_LONG(a,b) _ip_simd_mulhi_LONG(a,b)

/*
 * simd_fmaddhi
 *
 * Integer fused multiply-add return hi-bits.
 */

static inline simd_ULONG _ip_simd_fmaddhi_ULONG(simd_ULONG a, simd_ULONG b, simd_ULONG c)
{
    simd_ULONG x1, x2, res;
    const simd_ULONG mask  = _mm_set_epi32(0, -1, 0, -1);

    x1 = _mm_mul_epu32(a, b);
    x2 = _mm_mul_epu32(_mm_srli_si128(a, 4), _mm_srli_si128(b, 4));
    x1 = _mm_add_epi64(x1, simd_and_ULONG(c, mask));
    x2 = _mm_add_epi64(x2, simd_and_ULONG(_mm_srli_si128(c, 4), mask));
    x1 = _mm_srli_si128(x1, 4);
#ifdef HAVE_INTEL_SSE41
    res = _mm_blend_epi16(x1, x2, 0xcc);
#else
    res = _mm_or_si128(_mm_and_si128(mask, x1), _mm_andnot_si128(mask, x2));
#endif
    return res;
}
#define simd_fmaddhi_ULONG(a, b, c) _ip_simd_fmaddhi_ULONG(a, b, c)



/*
 * simd_imulfN
 *
 * Fixed-point integer multiplication where N=32 for 32/32 fixed-point and
 * N=16 for 16/16 fixed-point.
 *
 * Calculates round(a*c) for integer multiplicand a and fixed-point
 * multiplier c = cw + cf, with cw = floor(c) and cf = 2^N(c - cw). Intended
 * for use when c >= 0.5.  If c < 0.5 consider using the idivfN functions
 * below as they better preserve precision of the fraction.
 */

#ifdef HAVE_INTEL_SSE41
static inline simd_USHORT _ip_simd_imulf16_USHORT(simd_USHORT a, simd_USHORT cw, simd_USHORT cf)
{
    const simd_ULONG half = _mm_set1_epi32(1<<15);
    simd_USHORT sh, sl;
    simd_ULONG x1, x2;

    /* Multiply a by the fraction cf and round to nearest integer */
    sh = _mm_mulhi_epu16(a, cf);
    sl = _mm_mullo_epi16(a, cf);
    x1 = _mm_unpacklo_epi16(sl, sh);
    x2 = _mm_unpackhi_epi16(sl, sh);
    x1 = simd_add_ULONG(x1, half);
    x2 = simd_add_ULONG(x2, half);
    cf =  _mm_packus_epi32(_mm_srli_epi32(x1, 16), _mm_srli_epi32(x2, 16));
    /* Multiply a by the whole number part cw */
    cw = _mm_mullo_epi16(a, cw);
    /* Add two parts together for final result */
    return simd_add_USHORT(cf, cw);
}
#define simd_imulf16_USHORT(a, b, c) _ip_simd_imulf16_USHORT(a, b, c)

static inline simd_SHORT _ip_simd_imulf16_SHORT(simd_SHORT a, simd_USHORT cw, simd_USHORT cf)
{
    const simd_SHORT zero = _mm_set1_epi16(0);
    simd_LONG rounder = _mm_set1_epi16(1<<15);
    simd_LONG x1, x2, rh, rl, negc, sign;
    simd_SHORT res;

    /*
     * See comment below in _ip_simd_imulf32_LONG() as to why sign is
     * calculated in this manner.
     */
    sign = simd_xor_SHORT(_mm_cmpgt_epi16(zero, cw), _mm_cmpgt_epi16(zero, a));
    /* rounder = (result < 0) ? (half-1) : half */
    rounder = simd_add_SHORT(rounder, sign);

    /* Low 16-bits of 32-bit unsigned a * cw */
    cw = _mm_mullo_epi16(a, cw);

    /* signed correction to multiply:  negc = (a < 0) ? -cf : 0 */
    negc = simd_and_SHORT(simd_sub_SHORT(zero, cf), _mm_cmpgt_epi16(zero, a));

    /* Compute 32-bit result of unsigned a * cf */
    rh = _mm_mulhi_epu16(a, cf);
    rl = _mm_mullo_epi16(a, cf);
    x1 = _mm_unpacklo_epi16(rl, rh);
    x2 = _mm_unpackhi_epi16(rl, rh);

    /* Unpack rounder into some format as x1/x2 */
    rl = _mm_unpacklo_epi16(rounder, negc);
    rh = _mm_unpackhi_epi16(rounder, negc);

    /* Add in rounder and correction to turn unsigned mul to signed */
    x1 = simd_add_LONG(x1, rl);
    x2 = simd_add_LONG(x2, rh);

    /* Pick off the top 16-bits of a*cf and re-pack */
    cf = _mm_packus_epi32(_mm_srli_epi32(x1, 16), _mm_srli_epi32(x2, 16));

    /* And back into main result */
    res = simd_add_SHORT(cf, cw);

    return res;
}
#define simd_imulf16_SHORT(a, b, c) _ip_simd_imulf16_SHORT(a, b, c)


static inline simd_ULONG _ip_simd_imulf32_ULONG(simd_ULONG a, simd_ULONG cw, simd_ULONG cf)
{
    const simd_ULONG64 half = _mm_set1_epi64x(1L<<31);
    simd_ULONG64 x1, x2;

    /* Multiply a by the fraction cf and round to nearest integer */
    x1 = _mm_mul_epu32(a, cf);
    x2 = _mm_mul_epu32(_mm_srli_si128(a, 4), _mm_srli_si128(cf, 4));
    x1 = simd_add_ULONG64(x1, half);
    x2 = simd_add_ULONG64(x2, half);
    cf = _mm_blend_epi16(_mm_srli_si128(x1, 4), x2, 0xcc);
    /* Multiply a by the whole number part cw */
    cw = _mm_mullo_epi32(a, cw);
    /* Add two parts together for final result */
   return simd_add_ULONG(cf, cw);
}
#define simd_imulf32_ULONG(a, b, c) _ip_simd_imulf32_ULONG(a, b, c)

static inline simd_LONG _ip_simd_imulf32_LONG(simd_LONG a, simd_ULONG cw, simd_ULONG cf)
{
    const simd_LONG64 zero = _mm_set1_epi64x(0);
    simd_LONG64 rounder = _mm_set1_epi32(1<<31);
    simd_LONG64 x1, x2, rh, rl, negc, sign;
    simd_LONG res;

    /*
     * The rounder depends on the sign of the result.  It is tempting to use
     * cw calculated with the mullo below but cw can be zero which looses
     * the sign, hence this way immediately below to get the result sign,
     * and it's better because it gets the sign result quicker than the
     * mullo below so calculating the rounder etc., can happen earlier
     * giving an overall speedup.
     */
    sign = simd_xor_LONG(_mm_cmpgt_epi32(zero, cw), _mm_cmpgt_epi32(zero, a));
    /* rounder = (result < 0) ? (half-1) : half */
    rounder = simd_add_LONG(rounder, sign);

    /* Low 32-bits of 64-bit unsigned a * cw */
    cw = _mm_mullo_epi32(a, cw);

    /* signed correction to multiply:  negc = (a < 0) ? -cf : 0 */
    negc = simd_and_LONG(simd_sub_LONG(zero, cf), _mm_cmpgt_epi32(zero, a));

    /* Compute 64-bit result of unsigned a * cf */
    x1 = _mm_mul_epu32(a, cf);
    x2 = _mm_mul_epu32(_mm_srli_si128(a,4), _mm_srli_si128(cf, 4));

    /* Repack rounder/negc as (negc<<32 | rounder) */
    rl = _mm_blend_epi16(_mm_slli_si128(negc, 4), rounder, 0x33);
    rh = _mm_blend_epi16(negc, _mm_srli_si128(rounder, 4), 0x33);

    /* Add in rounder and correction to turn unsigned mul to signed */
    x1 = simd_add_LONG64(x1, rl);
    x2 = simd_add_LONG64(x2, rh);

    /* Unpack x1/x2 */
    cf = _mm_blend_epi16(_mm_srli_si128(x1, 4), x2, 0xcc);

    /* And back into main result */
    res = simd_add_LONG(cf, cw);

    return res;
}
#define simd_imulf32_LONG(a, b, c) _ip_simd_imulf32_LONG(a, b, c)
#endif


/* division */
#define simd_div_FLOAT(d,s)  _mm_div_ps(d,s)
#define simd_div_DOUBLE(d,s) _mm_div_pd(d,s)


/*
 * simd_rcpc
 * simd_idiv
 *
 * Integer division by a constant (i.e. reused) divisor
 *
 * We provide simd_rcpc functions to calculate a vector integer reciprocal
 * of a scalar divisor, and simd_idiv functions to calculate the integer
 * division using the reciprocal from the simd_rcpc functions.
 *
 * The IP library only expects these at (U)SHORT and (U)LONG precision.
 */

static inline struct simd_rcp_USHORT simd_rcpc_USHORT(unsigned int d)
{
    struct ip_uint16_rcp_n16 r = ip_urcp16_n16(d);
    struct simd_rcp_USHORT rcp;
    rcp.m = simd_splat_USHORT(r.m);
    rcp.b = simd_splat_USHORT(r.shift_1);
    rcp.s = _mm_set1_epi64x((int64_t)r.shift);
    return rcp;
}

static inline simd_USHORT _ip_simd_idiv_USHORT(simd_USHORT n,
					       struct simd_rcp_USHORT r)
{
    simd_USHORT x1, t;
    /*
     * Calculate the integer division by multiplication by reciprocal.
     * Note that this implementation exploits an optimisation that gives
     * incorrect results for a divisor of 1.
     */
    t = simd_mulhi_USHORT(n, r.m);
    x1 = _mm_srli_epi16(simd_sub_USHORT(n, t), 1);
    return _mm_srl_epi16(simd_add_USHORT(t, x1), r.s);
}
#define simd_idiv_USHORT(n, r) _ip_simd_idiv_USHORT(n, r)

static inline struct simd_rcp_SHORT simd_rcpc_SHORT(int d)
{
    struct ip_int16_rcp r = ip_ircp16(d);
    struct simd_rcp_SHORT rcp;
    rcp.m = simd_splat_SHORT(r.m);
    rcp.b = simd_splat_SHORT(r.dsign);
    rcp.s = _mm_set1_epi64x((int64_t)r.shift);
    return rcp;
}

static inline simd_SHORT _ip_simd_idiv_SHORT(simd_SHORT n,
					   struct simd_rcp_SHORT r)
{
    simd_SHORT q0;
    q0 = simd_add_SHORT(n, simd_mulhi_SHORT(n, r.m));
    q0 = _mm_sra_epi16(q0, r.s);
    q0 = simd_sub_SHORT(q0,  _mm_srai_epi16(n, 15));
    return simd_sub_SHORT(simd_xor_SHORT(q0, r.b), r.b);
}
#define simd_idiv_SHORT(n, r) _ip_simd_idiv_SHORT(n, r)

static inline struct simd_rcp_ULONG simd_rcpc_ULONG(unsigned int d)
{
    struct ip_uint32_rcp_n33 r = ip_urcp32_n33(d);
    struct simd_rcp_ULONG rcp;
    rcp.m = simd_splat_ULONG(r.m);
    rcp.b = simd_splat_ULONG(r.b);
    rcp.s = _mm_set1_epi64x((int64_t)r.shift);
    return rcp;
}

static inline simd_ULONG _ip_simd_idiv_ULONG(simd_ULONG n,
					     struct simd_rcp_ULONG r)
{
    return _mm_srl_epi32(simd_fmaddhi_ULONG(n, r.m, r.b), r.s);
}
#define simd_idiv_ULONG(n, r) _ip_simd_idiv_ULONG(n, r)

static inline struct simd_rcp_LONG simd_rcpc_LONG(int d)
{
    struct ip_int32_rcp r = ip_ircp32(d);
    struct simd_rcp_LONG rcp;
    rcp.m = simd_splat_LONG(r.m);
    rcp.b = simd_splat_LONG(r.dsign);
    rcp.s = _mm_set1_epi64x((int64_t)r.shift);
    return rcp;
}

static inline simd_LONG _ip_simd_idiv_LONG(simd_LONG n,
					   struct simd_rcp_LONG r)
{
    simd_LONG q0;
    q0 = simd_add_LONG(n, simd_mulhi_LONG(n, r.m));
    q0 = _mm_sra_epi32(q0, r.s);
    q0 = simd_sub_LONG(q0,  _mm_srai_epi32(n, 31));
    return simd_sub_LONG(simd_xor_LONG(q0, r.b), r.b);
}
#define simd_idiv_LONG(n, r) _ip_simd_idiv_LONG(n, r)

/*
 * idivfN
 *
 * Integer division with conventional rounding.  Not implemented in SSE as
 * there is no appropriate CPU shift instruction.
 */


/*
 * simd_mean - take the mean of two vectors with rounding
 */

static inline simd_BYTE _ip_simd_mean_BYTE(simd_BYTE a, simd_BYTE b)
{
    simd_SHORT_2 sa, sb;
    simd_SHORT sel;
    const simd_SHORT zero = _mm_setzero_si128();
    const simd_SHORT one = simd_splat_SHORT(1);
    sa = simd_cnvrt_SHORT_BYTE(a);
    sb = simd_cnvrt_SHORT_BYTE(b);
    sa.s[0] = _mm_add_epi16(sa.s[0], sb.s[0]);
    sa.s[1] = _mm_add_epi16(sa.s[1], sb.s[1]);
    sel = _mm_cmplt_epi16(sa.s[0], zero);
    sa.s[0] = v_select(sel, sa.s[0], simd_add_SHORT(sa.s[0], one));
    sel = _mm_cmplt_epi16(sa.s[1], zero);
    sa.s[1] = v_select(sel, sa.s[1], simd_add_SHORT(sa.s[1], one));
    sa.s[0] = _mm_srai_epi16(sa.s[0], 1);
    sa.s[1] = _mm_srai_epi16(sa.s[1], 1);
    return simd_cnvrt_UBYTE_USHORT(sa.s[0], sa.s[1]);
}
#define simd_mean_BYTE(a,b) _ip_simd_mean_BYTE(a, b)
#define simd_mean_UBYTE(a,b) _mm_avg_epu8(a, b)

static inline simd_SHORT _ip_simd_mean_SHORT(simd_SHORT a, simd_SHORT b)
{
    simd_LONG_2 sa, sb;
    simd_LONG sel;
    const simd_LONG zero = _mm_setzero_si128();
    const simd_LONG one = simd_splat_LONG(1);
    sa = simd_cnvrt_LONG_SHORT(a);
    sb = simd_cnvrt_LONG_SHORT(b);
    sa.l[0] = _mm_add_epi32(sa.l[0], sb.l[0]);
    sa.l[1] = _mm_add_epi32(sa.l[1], sb.l[1]);
    sel = _mm_cmplt_epi32(sa.l[0], zero);
    sa.l[0] = v_select(sel, sa.l[0], simd_add_LONG(sa.l[0], one));
    sel = _mm_cmplt_epi32(sa.l[1], zero);
    sa.l[1] = v_select(sel, sa.l[1], simd_add_LONG(sa.l[1], one));
    sa.l[0] = _mm_srai_epi32(sa.l[0], 1);
    sa.l[1] = _mm_srai_epi32(sa.l[1], 1);
    return simd_cnvrt_USHORT_ULONG(sa.l[0], sa.l[1]);
}
#define simd_mean_SHORT(a,b) _ip_simd_mean_SHORT(a, b)
#define simd_mean_USHORT(a,b) _mm_avg_epu16(a, b)

static inline simd_LONG _ip_simd_mean_LONG(simd_LONG a, simd_LONG b)
{
    simd_LONG sel;
    const simd_LONG zero = _mm_setzero_si128();
    const simd_LONG one = simd_splat_LONG(1);
    a = _mm_add_epi32(a, b);
    sel = _mm_cmplt_epi32(a, zero);
    a = v_select(sel, a, simd_add_LONG(a, one));
    return _mm_srai_epi32(a, 1);
}
#define simd_mean_LONG(a,b) _ip_simd_mean_LONG(a,b)

static inline simd_ULONG _ip_simd_mean_ULONG(simd_ULONG a, simd_ULONG b)
{
    a = _mm_add_epi32(a, b);
    a = _mm_add_epi32(a, simd_splat_ULONG(1));
    return _mm_srli_epi32(a, 1);
}
#define simd_mean_ULONG(a,b) _ip_simd_mean_ULONG(a,b)

#define simd_mean_FLOAT(a,b) _mm_mul_ps(_mm_add_ps(a,b), simd_splat_FLOAT(0.5F))
#define simd_mean_DOUBLE(a,b) _mm_mul_pd(_mm_add_pd(a,b), simd_splat_DOUBLE(0.5))



/*
 * simd_min
 * simd_max
 *
 * Elementwise Min/Max; without SSE41 cannot support a full complement of
 * vector types.
 *
 * To do: implement for SSE2 all types using comparison and v_select.
 */
#define simd_min_UBYTE(d, s) _mm_min_epu8(d, s)
#define simd_min_SHORT(d, s) _mm_min_epi16(d, s)
#ifdef HAVE_INTEL_SSE41
#define simd_min_BYTE(d, s) _mm_min_epi8(d, s)
#define simd_min_USHORT(d, s) _mm_min_epu16(d, s)
#define simd_min_LONG(d, s) _mm_min_epi32(d, s)
#define simd_min_ULONG(d, s) _mm_min_epu32(d, s)
#else
#define simd_min_BYTE(d, s) sse2_v_min_epi8(d, s)
#define simd_min_USHORT(d, s) sse2_v_min_epu16(d, s)
#define simd_min_LONG(d, s) sse2_v_min_epi32(d, s)
#define simd_min_ULONG(d, s) sse2_v_min_epu32(d, s)
#endif
#define simd_min_FLOAT(d,s) _mm_min_ps(d, s)
#define simd_min_DOUBLE(d,s) _mm_min_pd(d, s)

#define simd_max_UBYTE(d, s) _mm_max_epu8(d, s)
#define simd_max_SHORT(d, s) _mm_max_epi16(d, s)
#ifdef HAVE_INTEL_SSE41
#define simd_max_BYTE(d, s) _mm_max_epi8(d, s)
#define simd_max_USHORT(d, s) _mm_max_epu16(d, s)
#define simd_max_LONG(d, s) _mm_max_epi32(d, s)
#define simd_max_ULONG(d, s) _mm_max_epu32(d, s)
#else
#define simd_max_BYTE(d, s) sse2_v_max_epi8(d, s)
#define simd_max_USHORT(d, s) sse2_v_max_epu16(d, s)
#define simd_max_LONG(d, s) sse2_v_max_epi32(d, s)
#define simd_max_ULONG(d, s) sse2_v_max_epu32(d, s)
#endif
#define simd_max_FLOAT(d,s) _mm_max_ps(d, s)
#define simd_max_DOUBLE(d,s) _mm_max_pd(d, s)



/*
 * simd_cmpgt
 * simd_cmpeq
 *
 * Comparison operators.
 */

static inline simd_UBYTE _ip_simd_cmpgt_UBYTE(simd_UBYTE a, simd_UBYTE b)
{
    const simd_UBYTE bias = simd_splat_UBYTE(0x80);
    return _mm_cmpgt_epi8(simd_sub_UBYTE(a, bias), simd_sub_UBYTE(b, bias));
}

static inline simd_USHORT _ip_simd_cmpgt_USHORT(simd_USHORT a, simd_USHORT b)
{
    const simd_USHORT bias = simd_splat_USHORT(0x8000);
    return _mm_cmpgt_epi16(simd_sub_USHORT(a, bias), simd_sub_USHORT(b, bias));
}

static inline simd_ULONG _ip_simd_cmpgt_ULONG(simd_ULONG a, simd_ULONG b)
{
    const simd_ULONG bias = simd_splat_ULONG(0x80000000);
    return _mm_cmpgt_epi32(simd_sub_ULONG(a, bias), simd_sub_ULONG(b, bias));
}

#define simd_cmpgt_UBYTE(a, b) _ip_simd_cmpgt_UBYTE(a, b)
#define simd_cmpgt_BYTE(a, b) _mm_cmpgt_epi8(a, b)
#define simd_cmpgt_USHORT(a, b) _ip_simd_cmpgt_USHORT(a, b)
#define simd_cmpgt_SHORT(a, b) _mm_cmpgt_epi16(a, b)
#define simd_cmpgt_ULONG(a, b) _ip_simd_cmpgt_ULONG(a, b)
#define simd_cmpgt_LONG(a, b) _mm_cmpgt_epi32(a, b)
#define simd_cmpgt_FLOAT(a, b) _mm_cmpgt_ps(a, b)
#define simd_cmpgt_DOUBLE(a, b) _mm_cmpgt_pd(a, b)

#define simd_cmplt_UBYTE(a, b) simd_cmpgt_UBYTE(b, a)
#define simd_cmplt_BYTE(a, b) simd_cmpgt_BYTE(b, a)
#define simd_cmplt_USHORT(a, b) simd_cmpgt_USHORT(b, a)
#define simd_cmplt_SHORT(a, b) simd_cmpgt_SHORT(b, a)
#define simd_cmplt_ULONG(a, b) simd_cmpgt_ULONG(b, a)
#define simd_cmplt_LONG(a, b) simd_cmpgt_LONG(b, a)
#define simd_cmplt_FLOAT(a, b) _mm_cmplt_ps(a, b)
#define simd_cmplt_DOUBLE(a, b) _mm_cmplt_pd(a, b)

#define simd_cmpeq_UBYTE(a, b) _mm_cmpeq_epi8(a, b)
#define simd_cmpeq_BYTE(a, b) _mm_cmpeq_epi8(a, b)
#define simd_cmpeq_USHORT(a, b) _mm_cmpeq_epi16(a, b)
#define simd_cmpeq_SHORT(a, b) _mm_cmpeq_epi16(a, b)
#define simd_cmpeq_ULONG(a, b) _mm_cmpeq_epi32(a, b)
#define simd_cmpeq_LONG(a, b) _mm_cmpeq_epi32(a, b)
#define simd_cmpeq_FLOAT(a, b) _mm_cmpeq_ps(a, b)
#define simd_cmpeq_DOUBLE(a, b) _mm_cmpeq_pd(a, b)

/*
 * simd_select
 *
 * Bitwise Selection operator
 */

#define simd_select_UBYTE(s, a, b) v_select(s, a, b)
#define simd_select_BYTE(s, a, b) v_select(s, a, b)
#define simd_select_USHORT(s, a, b) v_select(s, a, b)
#define simd_select_SHORT(s, a, b) v_select(s, a, b)
#define simd_select_ULONG(s, a, b) v_select(s, a, b)
#define simd_select_LONG(s, a, b) v_select(s, a, b)
#define simd_select_FLOAT(s, a, b) v_selectf(s, a, b)
#define simd_select_DOUBLE(s, a, b) v_selectd(s, a, b)



/*
 * Thresholding operators
 *
 * They consist of a simd_threshold_init_<type> macro that can be used to
 * set up necessary variables before entering the processing loop, and a
 * simd_threshold_<type> macro that does the actual pixel processing within
 * the loop and uses any guff set up by simd_threshold_init_<type>.
 */


/*
 * Helper functions: simd element _false_ if low <= s <= high.
 * i.e., these routines return an inverted result.
 */

static inline __m128i v_cmp2_s8(__m128i vs, __m128i vlow, __m128i vhigh)
{
    __m128i t1, t2;
    t1 = _mm_cmpgt_epi8(vlow, vs);
    t2 = _mm_cmpgt_epi8(vs, vhigh);
    return _mm_or_si128(t2, t1);
}
static inline __m128i v_cmp2_s16(__m128i vs, __m128i vlow, __m128i vhigh)
{
    __m128i t1, t2;
    t1 = _mm_cmpgt_epi16(vlow, vs);
    t2 = _mm_cmpgt_epi16(vs, vhigh);
    return _mm_or_si128(t2, t1);
}
static inline __m128i v_cmp2_s32(__m128i vs, __m128i vlow, __m128i vhigh)
{
    __m128i t1, t2;
    t1 = _mm_cmpgt_epi32(vlow, vs);
    t2 = _mm_cmpgt_epi32(vs, vhigh);
    return _mm_or_si128(t2, t1);
}
static inline __m128i v_cmp2_f(__m128 vs, __m128 vlow, __m128 vhigh)
{
    __m128i t1, t2;
    t1 = _mm_castps_si128(_mm_cmpgt_ps(vlow, vs));
    t2 = _mm_castps_si128(_mm_cmpgt_ps(vs, vhigh));
    return _mm_or_si128(t2, t1);
}
static inline __m128i v_cmp2_d(__m128d vs, __m128d vlow, __m128d vhigh)
{
    __m128i t1, t2;
    t1 = _mm_castpd_si128(_mm_cmpgt_pd(vlow, vs));
    t2 = _mm_castpd_si128(_mm_cmpgt_pd(vs, vhigh));
    return _mm_or_si128(t2, t1);
}


/*
 * simd_threshold
 * simd_init_threshold
 *
 * Thresholding operators for threshold a value between low limit and high
 * limit.  simd_init_threshold is used to initialise variables (before
 * running image row loop) if need be to enable simd_threshold to work.
 *
 * We exploit simd_init_threshold on SSE to implement thresholding on
 * unsigned pixel types.  The addition of 0x80 to all unsigned bytes in the
 * SIMD vector enables us to do unsigned comparisons with the signed SIMD
 * comparison CPU instruction (which appears to be all that is available).
 */
#define simd_init_threshold_UBYTE()			\
    const simd_UBYTE sofs = simd_splat_UBYTE(0x80);	\
    vlow = simd_add_UBYTE(vlow, sofs);			\
    vhigh = simd_add_UBYTE(vhigh, sofs);

#define simd_threshold_UBYTE(s, vlow, vhigh, vres) \
    ({simd_UBYTE t1;			\
      t1 = simd_add_UBYTE(s, sofs);	\
      t1 = v_cmp2_s8(t1, vlow, vhigh);	\
      _mm_xor_si128(t1, vres);})

/* Have signed compare so no initialisation guff */
#define simd_init_threshold_BYTE()
static inline simd_UBYTE
_ip_simd_threshold_BYTE(simd_BYTE s, const simd_BYTE vlow, const simd_BYTE vhigh,
			const simd_UBYTE vres)
{
    simd_BYTE t1 = v_cmp2_s8(s, vlow, vhigh);
    return _mm_xor_si128(t1, vres);
}
#define simd_threshold_BYTE _ip_simd_threshold_BYTE

/* ushort needs an init function to transform short into ushort */
#define simd_init_threshold_USHORT()                    \
    const simd_USHORT sofs = simd_splat_USHORT(0x8000);	\
    vlow = simd_add_USHORT(vlow, sofs);			\
    vhigh = simd_add_USHORT(vhigh, sofs);

#define simd_threshold_USHORT(s1, s2, vlow, vhigh, vres)	\
    ({simd_USHORT t1, t2;					\
	t1 = simd_add_USHORT(s1, sofs);				\
	t2 = simd_add_USHORT(s2, sofs);				\
	t1 = v_cmp2_s16(t1, vlow, vhigh);			\
	t2 = v_cmp2_s16(t2, vlow, vhigh);			\
	t1 = _mm_packs_epi16(t1, t2);				\
	_mm_xor_si128(t1, vres);})

/* short simpler since short simd operators exist */
#define simd_init_threshold_SHORT()

static inline simd_UBYTE
_ip_simd_threshold_SHORT(simd_SHORT s1, simd_SHORT s2,
			 const simd_SHORT vlow, const simd_SHORT vhigh,
			 const simd_UBYTE vres)
{
    __m128i t1 = v_cmp2_s16(s1, vlow, vhigh);
    __m128i t2 = v_cmp2_s16(s2, vlow, vhigh);
    t1 = _mm_packs_epi16(t1, t2);
    return _mm_xor_si128(t1, vres);
}
#define simd_threshold_SHORT _ip_simd_threshold_SHORT

/* Must be a more efficient way of implementing these no doubt */
#define simd_init_threshold_ULONG()			\
    const simd_ULONG sofs = simd_splat_ULONG(0x8000);	\
    vlow = simd_add_ULONG(vlow, sofs);			\
    vhigh = simd_add_ULONG(vhigh, sofs);

#define simd_threshold_ULONG(s1, s2, s3, s4, vlow, vhigh, vres)	\
    ({__m128i t1, t2, t3;					\
    t1 = simd_add_ULONG(s1, sofs);				\
    t2 = simd_add_ULONG(s2, sofs);				\
    t1 = v_cmp2_s32(t1, vlow, vhigh);				\
    t2 = v_cmp2_s32(t2, vlow, vhigh);				\
    t1 = _mm_packs_epi32(t1, t2);				\
    t2 = simd_add_ULONG(s3, sofs);				\
    t3 = simd_add_ULONG(s4, sofs);				\
    t2 = v_cmp2_s32(t2, vlow, vhigh);				\
    t3 = v_cmp2_s32(t3, vlow, vhigh);				\
    t2 = _mm_packs_epi32(t2, t3);				\
    t1 = _mm_packs_epi16(t1, t2);				\
    _mm_xor_si128(t1, vres);})


static inline simd_UBYTE
_ip_simd_threshold_LONG(simd_LONG s1, simd_LONG s2, simd_LONG s3, simd_LONG s4,
			const simd_LONG vlow, const simd_LONG vhigh,
			const simd_UBYTE vres)
{
    __m128i t1, t2, t3;
    t1 = v_cmp2_s32(s1, vlow, vhigh);
    t2 = v_cmp2_s32(s2, vlow, vhigh);
    t1 = _mm_packs_epi32(t1, t2);
    t2 = v_cmp2_s32(s3, vlow, vhigh);
    t3 = v_cmp2_s32(s4, vlow, vhigh);
    t2 = _mm_packs_epi32(t2, t3);
    t1 = _mm_packs_epi16(t1, t2);
    return _mm_xor_si128(t1, vres);
}
#define simd_init_threshold_LONG()
#define simd_threshold_LONG _ip_simd_threshold_LONG


static inline simd_UBYTE
_ip_simd_threshold_FLOAT(simd_FLOAT s1, simd_FLOAT s2, simd_FLOAT s3, simd_FLOAT s4,
			 const simd_FLOAT vlow, const simd_FLOAT vhigh,
			 const simd_UBYTE vres)
{
    __m128i t1, t2, t3;
    t1 = v_cmp2_f(s1, vlow, vhigh);
    t2 = v_cmp2_f(s2, vlow, vhigh);
    t1 = _mm_packs_epi32(t1, t2);
    t2 = v_cmp2_f(s3, vlow, vhigh);
    t3 = v_cmp2_f(s4, vlow, vhigh);
    t2 = _mm_packs_epi32(t2, t3);
    t1 = _mm_packs_epi16(t1, t2);
    return _mm_xor_si128(t1, vres);
}
#define simd_init_threshold_FLOAT()
#define simd_threshold_FLOAT _ip_simd_threshold_FLOAT

static inline simd_UBYTE
_ip_simd_threshold_DOUBLE(simd_DOUBLE s1, simd_DOUBLE s2, simd_DOUBLE s3, simd_DOUBLE s4,
			  simd_DOUBLE s5, simd_DOUBLE s6, simd_DOUBLE s7, simd_DOUBLE s8,
			  const simd_DOUBLE vlow, const simd_DOUBLE vhigh,
			  const simd_UBYTE vres)
{
    __m128i t1, t2, t3, t4;
    t1 = v_cmp2_d(s1, vlow, vhigh);
    t2 = v_cmp2_d(s2, vlow, vhigh);
    t1 = _mm_packs_epi32(t1, t2);
    t2 = v_cmp2_d(s3, vlow, vhigh);
    t3 = v_cmp2_d(s4, vlow, vhigh);
    t2 = _mm_packs_epi32(t2, t3);
    t1 = _mm_packs_epi32(t1, t2);
    t2 = v_cmp2_d(s5, vlow, vhigh);
    t3 = v_cmp2_d(s6, vlow, vhigh);
    t2 = _mm_packs_epi32(t2, t3);
    t3 = v_cmp2_d(s7, vlow, vhigh);
    t4 = v_cmp2_d(s8, vlow, vhigh);
    t3 = _mm_packs_epi32(t3, t4);
    t2 = _mm_packs_epi32(t2, t3);
    t1 = _mm_packs_epi16(t1, t2);
    return _mm_xor_si128(t1, vres);
}
#define simd_init_threshold_DOUBLE()
#define simd_threshold_DOUBLE _ip_simd_threshold_DOUBLE


/*
 * simd_replicate_right
 * simd_replicate_left
 *
 * Replicate the Nth element in the vector to all elements to the right (or
 * left) of the Nth element.  Useful for managing image edges.  Right means
 * replicate in the direction of increasing memory address, i.e. along an
 * image row in increasing pixel order.
 */

#ifdef HAVE_INTEL_SSSE3

static inline simd_UBYTE _ip_simd_replicate_right_UBYTE(simd_UBYTE x, int pos)
{
    const simd_UBYTE vinc = _mm_setr_epi8(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15);
    simd_UBYTE vpos = simd_splat_UBYTE(pos);
    return _mm_shuffle_epi8(x, _mm_min_epu8(vinc, vpos));
}
static inline simd_UBYTE _ip_simd_replicate_left_UBYTE(simd_UBYTE x, int pos)
{
    const simd_UBYTE vinc = _mm_setr_epi8(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15);
    simd_UBYTE vpos = simd_splat_UBYTE(pos);
    return _mm_shuffle_epi8(x, _mm_max_epu8(vinc, vpos));
}
static inline simd_USHORT _ip_simd_replicate_right_USHORT(simd_USHORT x, int pos)
{
    const simd_USHORT vinc = _mm_setr_epi16(0x0100, 0x0302, 0x0504, 0x0706,
					    0x0908, 0x0b0a, 0x0d0c, 0x0f0e);
    simd_USHORT vpos = simd_splat_USHORT((pos<<1) + (pos<<9) + 0X0100);
    return _mm_shuffle_epi8(x, _mm_min_epi16(vinc, vpos));
}
static inline simd_USHORT _ip_simd_replicate_left_USHORT(simd_USHORT x, int pos)
{
    const simd_USHORT vinc = _mm_setr_epi16(0x0100, 0x0302, 0x0504, 0x0706,
					    0x0908, 0x0b0a, 0x0d0c, 0x0f0e);
    simd_USHORT vpos = simd_splat_USHORT((pos<<1) + (pos<<9) + 0X0100);
    return _mm_shuffle_epi8(x, _mm_max_epi16(vinc, vpos));
}
static inline simd_ULONG _ip_simd_replicate_right_ULONG(simd_ULONG x, int pos)
{
    const simd_ULONG vinc = _mm_setr_epi32(0x03020100, 0x07060504,
					    0x0b0a0908, 0x0f0e0d0c);
    simd_ULONG vpos = simd_splat_ULONG(0x04040404*pos + 0x03020100);
    return _mm_shuffle_epi8(x, sse2_v_min_epi32(vinc, vpos));
}
static inline simd_ULONG _ip_simd_replicate_left_ULONG(simd_ULONG x, int pos)
{
    const simd_ULONG vinc = _mm_setr_epi32(0x03020100, 0x07060504,
					    0x0b0a0908, 0x0f0e0d0c);
    simd_ULONG vpos = simd_splat_ULONG(0x04040404*pos + 0x03020100);
    return _mm_shuffle_epi8(x, sse2_v_max_epi32(vinc, vpos));
}

#else

static inline simd_UBYTE _ip_simd_replicate_right_UBYTE(simd_UBYTE x, int pos)
{
    const simd_UBYTE vinc = _mm_setr_epi8(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15);
    simd_UBYTE vpos = simd_splat_UBYTE(pos);
    simd_UBYTE sel = _mm_cmplt_epi8(vinc, vpos);
    return v_select(sel, x, simd_splat_UBYTE(simd_extract_UBYTE(x, pos)));
}
static inline simd_UBYTE _ip_simd_replicate_left_UBYTE(simd_UBYTE x, int pos)
{
    const simd_UBYTE vinc = _mm_setr_epi8(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15);
    simd_UBYTE vpos = simd_splat_UBYTE(pos);
    simd_UBYTE sel = _mm_cmpgt_epi8(vinc, vpos);
    return v_select(sel, x, simd_splat_UBYTE(simd_extract_UBYTE(x, pos)));
}
static inline simd_USHORT _ip_simd_replicate_right_USHORT(simd_USHORT x, int pos)
{
    const simd_USHORT vinc = _mm_setr_epi16(0, 1, 2, 3, 4, 5, 6, 7);
    simd_USHORT vpos = simd_splat_USHORT(pos);
    simd_USHORT sel = _mm_cmplt_epi16(vinc, vpos);
    return v_select(sel, x, simd_splat_USHORT(simd_extract_USHORT(x, pos)));
}
static inline simd_USHORT _ip_simd_replicate_left_USHORT(simd_USHORT x, int pos)
{
    const simd_USHORT vinc = _mm_setr_epi16(0, 1, 2, 3, 4, 5, 6, 7);
    simd_USHORT vpos = simd_splat_USHORT(pos);
    simd_USHORT sel = _mm_cmpgt_epi16(vinc, vpos);
    return v_select(sel, x, simd_splat_USHORT(simd_extract_USHORT(x, pos)));
}
static inline simd_ULONG _ip_simd_replicate_right_ULONG(simd_ULONG x, int pos)
{
    const simd_ULONG vinc = _mm_setr_epi32(0, 1, 2, 3);
    simd_ULONG vpos = simd_splat_ULONG(pos);
    simd_ULONG sel = _mm_cmplt_epi32(vinc, vpos);
    return v_select(sel, x, simd_splat_ULONG(simd_extract_ULONG(x, pos)));
}
static inline simd_ULONG _ip_simd_replicate_left_ULONG(simd_ULONG x, int pos)
{
    const simd_ULONG vinc = _mm_setr_epi32(0, 1, 2, 3);
    simd_ULONG vpos = simd_splat_ULONG(pos);
    simd_ULONG sel = _mm_cmpgt_epi32(vinc, vpos);
    return v_select(sel, x, simd_splat_ULONG(simd_extract_ULONG(x, pos)));
}
#endif

static inline simd_DOUBLE _ip_simd_replicate_right_DOUBLE(simd_DOUBLE x, int pos)
{
    simd_DOUBLE replicated = _mm_shuffle_pd(x, x, 0);
    simd_LONG sel = simd_splat_LONG(-pos);
    return v_selectd((simd_DOUBLE)sel, x, replicated);
}
static inline simd_DOUBLE _ip_simd_replicate_left_DOUBLE(simd_DOUBLE x, int pos)
{
    simd_DOUBLE replicated = _mm_shuffle_pd(x, x, 3);
    simd_LONG sel = simd_splat_LONG(-pos);
    return v_selectd((simd_DOUBLE)sel, replicated, x);
}


#define simd_replicate_right_UBYTE(x, p) _ip_simd_replicate_right_UBYTE(x, p)
#define simd_replicate_right_BYTE(x, p) _ip_simd_replicate_right_UBYTE(x, p)
#define simd_replicate_left_UBYTE(x, p) _ip_simd_replicate_left_UBYTE(x, p)
#define simd_replicate_left_BYTE(x, p) _ip_simd_replicate_left_UBYTE(x, p)

#define simd_replicate_right_USHORT(x, p) _ip_simd_replicate_right_USHORT(x, p)
#define simd_replicate_right_SHORT(x, p) _ip_simd_replicate_right_USHORT(x, p)
#define simd_replicate_left_USHORT(x, p) _ip_simd_replicate_left_USHORT(x, p)
#define simd_replicate_left_SHORT(x, p) _ip_simd_replicate_left_USHORT(x, p)

#define simd_replicate_right_ULONG(x, p) _ip_simd_replicate_right_ULONG(x, p)
#define simd_replicate_right_LONG(x, p) _ip_simd_replicate_right_ULONG(x, p)
#define simd_replicate_left_ULONG(x, p) _ip_simd_replicate_left_ULONG(x, p)
#define simd_replicate_left_LONG(x, p) _ip_simd_replicate_left_ULONG(x, p)

#define simd_replicate_right_FLOAT(x, p) \
    ((simd_FLOAT)_ip_simd_replicate_right_ULONG((simd_ULONG)x, p))
#define simd_replicate_left_FLOAT(x, p) \
    ((simd_FLOAT)_ip_simd_replicate_left_ULONG((simd_ULONG)x, p))

#define simd_replicate_right_DOUBLE(x, p) _ip_simd_replicate_right_DOUBLE(x, p)
#define simd_replicate_left_DOUBLE(x, p) _ip_simd_replicate_left_DOUBLE(x, p)

/*
 * simd_zero_right
 *
 * zero_right means to zero all elements to the right (i.e. in increasing
 * memory address order) of the specified position pos.  It is like
 * replicate_right but is used to for effecting a zero boundary on the right
 * side of images in filtering operations.
 */

static inline simd_UBYTE _ip_simd_zero_right_UBYTE(simd_UBYTE x, int pos)
{
    const simd_UBYTE vinc = _mm_setr_epi8(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15);
    simd_UBYTE vpos = simd_splat_UBYTE(pos);
    return _mm_andnot_si128(_mm_cmpgt_epi8(vinc, vpos), x);
}

static inline simd_USHORT _ip_simd_zero_right_USHORT(simd_USHORT x, int pos)
{
    const simd_USHORT vinc = _mm_setr_epi16(0, 1, 2, 3, 4, 5, 6, 7);
    simd_USHORT vpos = simd_splat_USHORT(pos);
    return _mm_andnot_si128(_mm_cmpgt_epi16(vinc, vpos), x);
}

static inline simd_ULONG _ip_simd_zero_right_ULONG(simd_ULONG x, int pos)
{
    const simd_ULONG vinc = _mm_setr_epi32(0, 1, 2, 3);
    simd_ULONG vpos = simd_splat_ULONG(pos);
    return _mm_andnot_si128(_mm_cmpgt_epi32(vinc, vpos), x);
}

static inline simd_FLOAT _ip_simd_zero_right_FLOAT(simd_FLOAT x, int pos)
{
    const simd_ULONG vinc = _mm_setr_epi32(0, 1, 2, 3);
    simd_ULONG vpos = simd_splat_ULONG(pos);
    return _mm_andnot_ps((__m128)_mm_cmpgt_epi32(vinc, vpos), x);
}

static inline simd_DOUBLE _ip_simd_zero_right_DOUBLE(simd_DOUBLE x, int pos)
{
    const simd_ULONG vinc = _mm_setr_epi32(0, 0, 1, 1);
    simd_ULONG vpos = simd_splat_ULONG(pos);
    return _mm_andnot_pd((__m128d)_mm_cmpgt_epi32(vinc, vpos), x);
}

#define simd_zero_right_UBYTE(x, p) _ip_simd_zero_right_UBYTE(x, p)
#define simd_zero_right_BYTE(x, p) _ip_simd_zero_right_UBYTE(x, p)
#define simd_zero_right_USHORT(x, p) _ip_simd_zero_right_USHORT(x, p)
#define simd_zero_right_SHORT(x, p) _ip_simd_zero_right_USHORT(x, p)
#define simd_zero_right_ULONG(x, p) _ip_simd_zero_right_ULONG(x, p)
#define simd_zero_right_LONG(x, p) _ip_simd_zero_right_ULONG(x, p)
#define simd_zero_right_FLOAT(x, p) _ip_simd_zero_right_FLOAT(x, p)
#define simd_zero_right_DOUBLE(x, p) _ip_simd_zero_right_DOUBLE(x, p)

static inline simd_UBYTE _ip_simd_zero_left_UBYTE(simd_UBYTE x, int pos)
{
    const simd_UBYTE vinc = _mm_setr_epi8(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15);
    simd_UBYTE vpos = simd_splat_UBYTE(pos);
    return _mm_andnot_si128(_mm_cmpgt_epi8(vpos, vinc), x);
}

static inline simd_USHORT _ip_simd_zero_left_USHORT(simd_USHORT x, int pos)
{
    const simd_USHORT vinc = _mm_setr_epi16(0, 1, 2, 3, 4, 5, 6, 7);
    simd_USHORT vpos = simd_splat_USHORT(pos);
    return _mm_andnot_si128(_mm_cmpgt_epi16(vpos, vinc), x);
}

static inline simd_ULONG _ip_simd_zero_left_ULONG(simd_ULONG x, int pos)
{
    const simd_ULONG vinc = _mm_setr_epi32(0, 1, 2, 3);
    simd_ULONG vpos = simd_splat_ULONG(pos);
    return _mm_andnot_si128(_mm_cmpgt_epi32(vpos, vinc), x);
}

static inline simd_FLOAT _ip_simd_zero_left_FLOAT(simd_FLOAT x, int pos)
{
    const simd_ULONG vinc = _mm_setr_epi32(0, 1, 2, 3);
    simd_ULONG vpos = simd_splat_ULONG(pos);
    return _mm_andnot_ps((__m128)_mm_cmpgt_epi32(vpos, vinc), x);
}

static inline simd_DOUBLE _ip_simd_zero_left_DOUBLE(simd_DOUBLE x, int pos)
{
    const simd_ULONG vinc = _mm_setr_epi32(0, 0, 1, 1);
    simd_ULONG vpos = simd_splat_ULONG(pos);
    return _mm_andnot_pd((__m128d)_mm_cmpgt_epi32(vpos, vinc), x);
}

#define simd_zero_left_UBYTE(x, p) _ip_simd_zero_left_UBYTE(x, p)
#define simd_zero_left_BYTE(x, p) _ip_simd_zero_left_UBYTE(x, p)
#define simd_zero_left_USHORT(x, p) _ip_simd_zero_left_USHORT(x, p)
#define simd_zero_left_SHORT(x, p) _ip_simd_zero_left_USHORT(x, p)
#define simd_zero_left_ULONG(x, p) _ip_simd_zero_left_ULONG(x, p)
#define simd_zero_left_LONG(x, p) _ip_simd_zero_left_ULONG(x, p)
#define simd_zero_left_FLOAT(x, p) _ip_simd_zero_left_FLOAT(x, p)
#define simd_zero_left_DOUBLE(x, p) _ip_simd_zero_left_DOUBLE(x, p)
