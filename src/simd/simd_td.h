/*
   Name: ip/simd/simd_td.h
   Author: Michael J. Cree

   Copyright (C) 2015, 2018 Michael J. Cree

   Some typedefs needed for SIMD code.
*/


/*
 * These typedefs enable multiple vectors to be returned from an arch SIMD
 * processing macros/inlines.  For example, a macro that processes byte data
 * but returns short data, needs two vectors to represent the returned
 * result for each input vector processed.
 */

/* It is assumed that every arch supports UBYTE/SHORT/USHORT */

typedef struct {
    simd_BYTE b[2];
} simd_BYTE_2;

typedef struct {
    simd_UBYTE ub[2];
} simd_UBYTE_2;

typedef struct {
    simd_UBYTE ub[3];
} simd_UBYTE_3;

typedef struct {
    simd_UBYTE ub[4];
} simd_UBYTE_4;

typedef struct {
    simd_USHORT us[2];
} simd_USHORT_2;

typedef struct {
    simd_SHORT s[2];
} simd_SHORT_2;

typedef struct {
    simd_USHORT us[4];
} simd_USHORT_4;

typedef struct {
    simd_USHORT us[8];
} simd_USHORT_8;

typedef simd_UBYTE simd_RGB;
typedef simd_UBYTE simd_RGBA;
typedef simd_USHORT simd_RGB16;

typedef simd_UBYTE_3 simd_RGB_3;
typedef simd_UBYTE_4 simd_RGBA_4;
typedef simd_USHORT_2 simd_RGB16_2;

/*
 * LONG, FLOAT and DOUBLE may not be provided on an arch.  The splat macros
 * provide the necessary test.
 */

#ifdef simd_splat_ULONG
typedef struct {
    simd_ULONG ul[2];
} simd_ULONG_2;

typedef struct {
    simd_ULONG ul[4];
} simd_ULONG_4;

typedef struct {
    simd_ULONG ul[8];
} simd_ULONG_8;
#endif

#ifdef simd_splat_LONG
typedef struct {
    simd_LONG l[2];
} simd_LONG_2;

typedef struct {
    simd_LONG l[4];
} simd_LONG_4;
#endif

#ifdef simd_splat_FLOAT
typedef struct {
    simd_FLOAT f[2];
} simd_FLOAT_2;

typedef struct {
    simd_FLOAT f[4];
} simd_FLOAT_4;

typedef struct {
    simd_FLOAT f[8];
} simd_FLOAT_8;
#endif


#ifdef simd_splat_DOUBLE
typedef struct {
    simd_DOUBLE d[2];
} simd_DOUBLE_2;
typedef struct {
    simd_DOUBLE d[4];
} simd_DOUBLE_4;
typedef struct {
    simd_DOUBLE d[8];
} simd_DOUBLE_8;
#endif


#ifdef simd_splat_COMPLEX
typedef struct {
    simd_COMPLEX c[2];
} simd_COMPLEX_2;
typedef struct {
    simd_COMPLEX c[4];
} simd_COMPLEX_4;
#endif


#ifdef simd_splat_DCOMPLEX
typedef struct {
    simd_DCOMPLEX dc[2];
} simd_DCOMPLEX_2;
#endif


/*
 * integer reciprocal structures.
 *
 * The internal representation of these is architecture dependent.  Field b
 * is 0 or m if ip_rcpX_X is used and the shift value of 0 or 1 if
 * ip_rcpX_(X+1) is used.
 *
 * The shift values (s and possibly b) are architecture dependent.
 */

struct simd_rcp_USHORT {
    simd_USHORT m, b, s;
};

struct simd_rcp_SHORT {
    simd_SHORT m, b, s;
};

#ifdef simd_splat_ULONG
struct simd_rcp_ULONG {
    simd_ULONG m, b, s;
};

struct simd_rcp_LONG {
    simd_LONG m, b, s;
};
#endif
