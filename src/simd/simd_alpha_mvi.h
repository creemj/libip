/*
   Name: ip/simd/simd_alpha_mvi.h
   Author: M. J. Cree

   Copyright (C) 2013, 2015-2019 by Michael J. Cree

   SIMD support for the IP library.

   Alpha MVI support.

*/

#include <stdint.h>

/* Define the MVI and other useful assembler instructions */
#define minub8 __builtin_alpha_minub8
#define minuw4 __builtin_alpha_minuw4
#define maxub8 __builtin_alpha_maxub8
#define maxuw4 __builtin_alpha_maxuw4

#define minsb8 __builtin_alpha_minsb8
#define minsw4 __builtin_alpha_minsw4
#define maxsb8 __builtin_alpha_maxsb8
#define maxsw4 __builtin_alpha_maxsw4

#define pkwb __builtin_alpha_pkwb
#define pklb __builtin_alpha_pklb
#define unpkbw __builtin_alpha_unpkbw
#define unpkbl __builtin_alpha_unpkbl

#define cmpbge __builtin_alpha_cmpbge
#define zap __builtin_alpha_zap
#define zapnot __builtin_alpha_zapnot

#define extbl __builtin_alpha_extbl
#define extbh __builtin_alpha_extbh
#define insbl __builtin_alpha_insbl
#define insbh __builtin_alpha_insbh

#define extwl __builtin_alpha_extwl
#define extwh __builtin_alpha_extwh
#define inswl __builtin_alpha_inswl
#define inswh __builtin_alpha_inswh

#define extll __builtin_alpha_extll
#define extlh __builtin_alpha_extlh
#define insll __builtin_alpha_insll
#define inslh __builtin_alpha_inslh

#define extql __builtin_alpha_extql
#define extqh __builtin_alpha_extqh
#define insql __builtin_alpha_insql
#define insqh __builtin_alpha_insqh

#define ldq_u(p)        (*(const uint64_t *) (((uint64_t) (p)) & ~7UL))

/* Prefetching gives an advantage */
#ifdef PREFETCH_READ_ONCE
#undef PREFETCH_READ_ONCE
#endif
#define PREFETCH_READ_ONCE(x) __builtin_prefetch(x, 0, 0);

#ifdef PREFETCH_READ_WRITE
#undef PREFETCH_READ_WRITE
#endif
#define PREFETCH_READ_WRITE(x) __builtin_prefetch(x, 1, 1);

#ifdef PREFETCH_OFS
#undef PREFETCH_OFS
#endif
#define PREFETCH_OFS 8

/* Basic vector types */
typedef unsigned long simd_UBYTE;
typedef unsigned long simd_BYTE;
typedef unsigned long simd_USHORT;
typedef unsigned long simd_SHORT;
typedef unsigned long simd_ULONG;
typedef unsigned long simd_LONG;


static inline unsigned long __alpha_splat_ubyte(uint8_t c)
{
    unsigned long t = c | (((unsigned long)c)<<8);
    t = t | (t<<16);
    return t | (t<<32);
}
static inline unsigned long __alpha_splat_ushort(uint16_t c)
{
    unsigned long t = c | (((unsigned long)c)<<16);
    return t | (t<<32);
}
static inline unsigned long __alpha_splat_ulong(uint32_t c)
{
  return c | (((unsigned long)c)<<32);
}


#define simd_splat_UBYTE(c) __alpha_splat_ubyte(c)
#define simd_splat_BYTE(c) __alpha_splat_ubyte(c)
#define simd_splat_USHORT(c) __alpha_splat_ushort(c)
#define simd_splat_SHORT(c) __alpha_splat_ushort(c)
#define simd_splat_ULONG(c) __alpha_splat_ulong(c)
#define simd_splat_LONG(c) __alpha_splat_ulong(c)

/*
 * Now that the splat routines are defined and, thus, indicate which image
 * types we support in SIMD, now get the rest of the typedefs that are
 * needed for the convert routines below.
 */

#include "simd_td.h"

#define simd_splat_v0_UBYTE(c) simd_splat_UBYTE((c) & 0xff)
#define simd_splat_v0_BYTE(c) simd_splat_UBYTE((c) & 0xff)
#define simd_splat_v0_USHORT(c) simd_splat_USHORT((c) & 0xffff)
#define simd_splat_v0_SHORT(c) simd_splat_USHORT((c) & 0xffff)
#define simd_splat_v0_ULONG(c) simd_splat_ULONG((c) & 0xffffffff)
#define simd_splat_v0_LONG(c) simd_splat_ULONG((c) & 0xffffffff)

#define simd_splat_vN_BYTE(c,n) simd_splat_UBYTE(extbl(c, n))
#define simd_splat_vN_UBYTE(c,n) simd_splat_UBYTE(extbl(c, n))
#define simd_splat_vN_SHORT(c,n) simd_splat_USHORT(extwl(c, 2*(n)))
#define simd_splat_vN_USHORT(c,n) simd_splat_USHORT(extwl(c, 2*(n)))
#define simd_splat_vN_LONG(c,n) simd_splat_ULONG(extll(c, 4*(n)))
#define simd_splat_vN_ULONG(c,n) simd_splat_ULONG(extll(c, 4*(n)))

/* Masks/Splatting for multielement image types (only the rgb image types) */
static inline unsigned long _alpha_splat_rgb(uint8_t r, uint8_t g, uint8_t b)
{
    unsigned long t = ((unsigned long)b << 16) | ((unsigned long)g << 8) | (unsigned long)r;
    return (t << 48) | (t << 24) | t;
}
static inline unsigned long _alpha_splat_rgb16(uint16_t r, uint16_t g, uint16_t b)
{
    unsigned long t = ((unsigned long)b << 32) | ((unsigned long)g << 16) | (unsigned long)r;
    return (t << 48) | t;
}
static inline unsigned long _alpha_splat_rgba(uint8_t r, uint8_t g, uint8_t b, uint8_t a)
{
    unsigned long t = ((unsigned long)a << 24) | ((unsigned long)b << 16)
	| ((unsigned long)g << 8) | (unsigned long)r;
    return (t << 32) | t;
}

#define simd_mask_RGB(r, g, b) _alpha_splat_rgb(r, g, b)
#define simd_mask_RGB16(r, g, b) _alpha_splat_rgb16(r, g, b)
#define simd_mask_RGBA(r, g, b, a) _alpha_splat_rgba(r, g, b, a)

static inline unsigned long _alpha_ror_RGB(unsigned long a)
{
    unsigned long tmp = zapnot(a, 0x0c) << 32;
    return (a >> 16) | tmp;
}

static inline unsigned long _alpha_ror_RGB16(unsigned long a)
{
    unsigned long tmp = zapnot(a, 0x0c) << 32;
    return (a >> 16) | tmp;
}

#define simd_ror_RGB(x) _alpha_ror_RGB(x)
#define simd_ror_RGB16(x) _alpha_ror_RGB16(x)

/*
 * Extract a specified element from a SIMD vector
 */

#define simd_extract_BYTE(v, p) extbl(v, p)
#define simd_extract_UBYTE(v, p) extbl(v, p)
#define simd_extract_SHORT(v, p) extwl(v, 2*(p))
#define simd_extract_USHORT(v, p) extwl(v, 2*(p))
#define simd_extract_LONG(v, p) extll(v, 4*(p))
#define simd_extract_ULONG(v, p) extll(v, 4*(p))

/*
 * For selecting an unaligned vector from two adjacent vectors
 */

static inline simd_UBYTE _ip_simd_cvu_UBYTE(simd_UBYTE l, simd_UBYTE h, int ofs)
{
    return ofs ? extql(l,ofs) | extqh(h, ofs) : l;
}

static inline simd_USHORT _ip_simd_cvu_USHORT(simd_USHORT l, simd_USHORT h, int ofs)
{
    return _ip_simd_cvu_UBYTE(l, h, 2*ofs);
}

static inline simd_ULONG _ip_simd_cvu_ULONG(simd_ULONG l, simd_ULONG h, int ofs)
{
    return _ip_simd_cvu_UBYTE(l, h, 4*ofs);
}

#define simd_cvu_imm_UBYTE(l, h, o)  _ip_simd_cvu_UBYTE(l, h, o)
#define simd_cvu_imm_BYTE(l, h, o)  _ip_simd_cvu_UBYTE(l, h, o)
#define simd_cvu_imm_USHORT(l, h, o)  _ip_simd_cvu_USHORT(l, h, o)
#define simd_cvu_imm_SHORT(l, h, o)  _ip_simd_cvu_USHORT(l, h, o)
#define simd_cvu_imm_ULONG(l, h, o)  _ip_simd_cvu_ULONG(l, h, o)
#define simd_cvu_imm_LONG(l, h, o)  _ip_simd_cvu_ULONG(l, h, o)

#if 0
#define simd_cvu_UBYTE(l, h, o)  _ip_simd_cvu_UBYTE(l, h, o)
#define simd_cvu_BYTE(l, h, o)  _ip_simd_cvu_UBYTE(l, h, o)
#define simd_cvu_USHORT(l, h, o)  _ip_simd_cvu_USHORT(l, h, o)
#define simd_cvu_SHORT(l, h, o)  _ip_simd_cvu_USHORT(l, h, o)
#define simd_cvu_ULONG(l, h, o)  _ip_simd_cvu_ULONG(l, h, o)
#define simd_cvu_LONG(l, h, o)  _ip_simd_cvu_ULONG(l, h, o)
#endif

static inline simd_UBYTE _ip_simd_ldu_UBYTE(void *adr)
{
    uint64_t unaligned = (uint64_t)adr;
    simd_UBYTE l = ldq_u(unaligned);
    simd_UBYTE h = ldq_u(unaligned+7);
    return _ip_simd_cvu_UBYTE(l, h, unaligned & 7);
}

#define simd_ldu_UBYTE(a) _ip_simd_ldu_UBYTE(a)
#define simd_ldu_BYTE(a) ((simd_BYTE)_ip_simd_ldu_UBYTE(a))
#define simd_ldu_USHORT(a) ((simd_USHORT)_ip_simd_ldu_UBYTE(a))
#define simd_ldu_SHORT(a) ((simd_SHORT)_ip_simd_ldu_UBYTE(a))

/*
 * Select bits between two vectors
 * v_select() is used when sel is a bitwise pattern.
 * alpha_v_select() is only used internally to this header file when the
 * lowest eight bits in sel specify the bytes (as from cmpbge()).
 */
static inline unsigned long alpha_v_select(unsigned long sel, unsigned long a, unsigned long b)
{
    return zapnot(a, sel) | zap(b, sel);
}

static inline unsigned long v_select(unsigned long sel, unsigned long a, unsigned long b)
{
    return (a & sel) | (b & ~sel);
}


/*
 * Conversion routines for converting one image type to another.
 *
 * simd_cnvrt_DTYPE_STYPE
 *   - convert source type STYPE to type DTYPE
 *
 * simd_cnvrt_clip_DTYPE_STYPE
 *   - convert source type STYPE to type DTYPE with clipping (saturation).
 */

static inline simd_USHORT_2 _ip_simd_cnvrt_USHORT_UBYTE(simd_UBYTE a)
{
    simd_USHORT_2 res;
    res.us[0] = unpkbw(a);
    res.us[1] = unpkbw(a >> 32);
    return res;
}

#define simd_cnvrt_USHORT_UBYTE(x) _ip_simd_cnvrt_USHORT_UBYTE(x)

static inline simd_SHORT _alpha_signext_bytew(simd_SHORT x)
{
    unsigned long s = cmpbge(x, 0xff80ff80ff80ff80UL);
    unsigned long signs = zapnot(~0UL, s<<1);
    return x | signs;
}

static inline simd_SHORT_2 _ip_simd_cnvrt_SHORT_BYTE(simd_BYTE a)
{
    simd_SHORT_2 res;
    res.s[0] = _alpha_signext_bytew(unpkbw(a));
    res.s[1] = _alpha_signext_bytew(unpkbw(a >> 32));
    return res;
}

#define simd_cnvrt_SHORT_BYTE(a) _ip_simd_cnvrt_SHORT_BYTE(a)

static inline simd_ULONG_4 _ip_simd_cnvrt_ULONG_UBYTE(simd_UBYTE a)
{
    simd_ULONG_4 res;
    res.ul[0] = unpkbl(a);
    res.ul[1] = unpkbl(a >> 16);
    res.ul[2] = unpkbl(a >> 32);
    res.ul[3] = unpkbl(a >> 48);
    return res;
}

#define simd_cnvrt_ULONG_UBYTE(x) _ip_simd_cnvrt_ULONG_UBYTE(x)

#define simd_cnvrt_UBYTE_USHORT(a,b) (pkwb(a) | (pkwb(b)<<32))
#define simd_cnvrt_UBYTE_ULONG(a,b,c,d)				\
    (pklb(a) | (pklb(b)<<16) | (pklb(c)<<32) | (pklb(d)<<48))


#define simd_cnvrt_clip_UBYTE_BYTE(a) maxsb8(a, 0)
#define simd_cnvrt_clip_BYTE_UBYTE(a) minub8(a, 0x7f7f7f7f7f7f7f7fUL)

#define simd_cnvrt_clip_USHORT_SHORT(a) maxsw4(a, 0)
#define simd_cnvrt_clip_SHORT_USHORT(a) minuw4(a, 0x7fff7fff7fff7fffUL)

#define simd_cnvrt_clip_USHORT_BYTE(a) _ip_simd_cnvrt_USHORT_UBYTE(maxsb8(a,0))
#define simd_cnvrt_clip_ULONG_BYTE(a) _ip_simd_cnvrt_ULONG_UBYTE(maxsb8(a,0))

#define simd_cnvrt_clip_UBYTE_USHORT(a,b) \
    simd_cnvrt_UBYTE_USHORT(minuw4(a,0x00ff00ff00ff00ffUL), minuw4(b,0x00ff00ff00ff00ffUL))
#define simd_cnvrt_clip_BYTE_USHORT(a,b) \
    simd_cnvrt_UBYTE_USHORT(minuw4(a,0x007f007f007f007fUL), minuw4(b,0x007f007f007f007fUL))

static inline simd_UBYTE _ip_simd_cnvrt_clip_UBYTE_SHORT(simd_SHORT a, simd_SHORT b)
{
    a = maxsw4(a, 0);
    b = maxsw4(b, 0);
    a = minsw4(a, 0x00ff00ff00ff00ffUL);
    b = minsw4(b, 0x00ff00ff00ff00ffUL);
    return simd_cnvrt_UBYTE_USHORT(a,b);
}

static inline simd_UBYTE _ip_simd_cnvrt_clip_BYTE_SHORT(simd_SHORT a, simd_SHORT b)
{
    a = maxsw4(a, 0xff80ff80ff80ff80UL);
    b = maxsw4(b, 0xff80ff80ff80ff80UL);
    a = minsw4(a, 0x007f007f007f007fUL);
    b = minsw4(b, 0x007f007f007f007fUL);
    return simd_cnvrt_UBYTE_USHORT(a,b);
}

#define simd_cnvrt_clip_UBYTE_SHORT(a,b) _ip_simd_cnvrt_clip_UBYTE_SHORT(a,b)
#define simd_cnvrt_clip_BYTE_SHORT(a,b) _ip_simd_cnvrt_clip_BYTE_SHORT(a,b)

/* Conversions with BINARY */

static inline simd_UBYTE _ip_simd_cnvrt_BINARY_UBYTE(simd_UBYTE x)
{
    const simd_UBYTE vzero = 0UL;
    const simd_UBYTE vtrue = 0xffffffffffffffffUL;
    return zap(vtrue, cmpbge(vzero, x));
}
#define simd_cnvrt_BINARY_UBYTE(x) _ip_simd_cnvrt_BINARY_UBYTE(x)

static inline simd_UBYTE alpha_wzapnz(simd_USHORT x)
{
    const simd_UBYTE vzero = 0UL;
    const simd_UBYTE vtrue = 0xffffffffffffffffUL;
    x = ((x & 0xff00ff00ff00ff00UL)>>8) | ((x & 0x00ff00ff00ff00ffUL)<<8) | x;
    return zap(vtrue, cmpbge(vzero, x));
}

static inline simd_UBYTE _ip_simd_cnvrt_BINARY_USHORT(simd_USHORT a, simd_USHORT b)
{
    a = alpha_wzapnz(a);
    b = alpha_wzapnz(b);
    return simd_cnvrt_UBYTE_USHORT(a, b);
}
#define simd_cnvrt_BINARY_USHORT(x, y) _ip_simd_cnvrt_BINARY_USHORT(x, y)

static inline simd_UBYTE alpha_lzapnz(simd_ULONG x)
{
    uint64_t low, high;
    low = ((x & 0xffffffffUL) > 0) ? 0xffffffffUL : 0UL;
    high = ((x & 0xffffffff00000000UL) > 0) ? 0xffffffff00000000UL : 0UL;
    return low | high;
}

static inline simd_UBYTE _ip_simd_cnvrt_BINARY_ULONG(simd_ULONG a, simd_ULONG b,
						     simd_ULONG c, simd_ULONG d)
{
    a = alpha_lzapnz(a);
    b = alpha_lzapnz(b);
    c = alpha_lzapnz(c);
    d = alpha_lzapnz(d);
    return simd_cnvrt_UBYTE_ULONG(a, b, c, d);
}
#define simd_cnvrt_BINARY_ULONG(x, y, u, v) _ip_simd_cnvrt_BINARY_ULONG(x, y, u, v)

static inline simd_UBYTE _ip_simd_cnvrt_UBYTE_BINARY(simd_UBYTE x, int tval)
{
    const simd_UBYTE vfalse = 0UL;
    simd_UBYTE vtrue = simd_splat_UBYTE(tval);
    return v_select(x, vtrue, vfalse);
}
#define simd_cnvrt_UBYTE_BINARY(x, tv) _ip_simd_cnvrt_UBYTE_BINARY(x, tv)

static inline simd_USHORT_2 _ip_simd_cnvrt_USHORT_BINARY(simd_UBYTE x, int tval)
{
    const simd_UBYTE vfalse = 0UL;
    simd_UBYTE vtrue = simd_splat_UBYTE(tval);
    x = v_select(x, vtrue, vfalse);
    return simd_cnvrt_USHORT_UBYTE(x);
}
#define simd_cnvrt_USHORT_BINARY(x, tv) _ip_simd_cnvrt_USHORT_BINARY(x, tv)

static inline simd_ULONG_4 _ip_simd_cnvrt_ULONG_BINARY(simd_UBYTE x, int tval)
{
    const simd_UBYTE vfalse = simd_splat_UBYTE(0);
    simd_UBYTE vtrue = simd_splat_UBYTE(tval);
    x = v_select(x, vtrue, vfalse);
    return simd_cnvrt_ULONG_UBYTE(x);
}
#define simd_cnvrt_ULONG_BINARY(x, tv) _ip_simd_cnvrt_ULONG_BINARY(x, tv)

static inline simd_UBYTE ip_simd_cnvrt_RGB_RGB16(simd_USHORT x, simd_USHORT y)
{
    const simd_UBYTE lower_byte_mask = 0x00ff00ff00ff00ffUL;
    return simd_cnvrt_UBYTE_USHORT((x >> 8) & lower_byte_mask,
				   (y >> 8) & lower_byte_mask);
}
#define simd_cnvrt_RGB_RGB16(x,y) ip_simd_cnvrt_RGB_RGB16(x,y)

static inline simd_USHORT_2 ip_simd_cnvrt_RGB16_RGB(simd_UBYTE x)
{
    simd_USHORT_2 r16 = simd_cnvrt_USHORT_UBYTE(x);
    r16.us[0] = r16.us[0] | (r16.us[0]<<8);
    r16.us[1] = r16.us[1] | (r16.us[1]<<8);
    return r16;
}
#define simd_cnvrt_RGB16_RGB(x) ip_simd_cnvrt_RGB16_RGB(x)


/*
 * Logical operators
 */

#define simd_or_UBYTE(a,b) ((a) | (b))
#define simd_or_BYTE(a,b) ((a) | (b))
#define simd_or_USHORT(a,b) ((a) | (b))
#define simd_or_SHORT(a,b) ((a) | (b))
#define simd_or_ULONG(a,b) ((a) | (b))
#define simd_or_LONG(a,b) ((a) | (b))

#define simd_and_UBYTE(a,b) ((a) & (b))
#define simd_and_BYTE(a,b) ((a) & (b))
#define simd_and_USHORT(a,b) ((a) & (b))
#define simd_and_SHORT(a,b) ((a) & (b))
#define simd_and_ULONG(a,b) ((a) & (b))
#define simd_and_LONG(a,b) ((a) & (b))

#define simd_xor_UBYTE(a,b) ((a) ^ (b))
#define simd_xor_BYTE(a,b) ((a) ^ (b))
#define simd_xor_USHORT(a,b) ((a) ^ (b))
#define simd_xor_SHORT(a,b) ((a) ^ (b))
#define simd_xor_ULONG(a,b) ((a) ^ (b))
#define simd_xor_LONG(a,b) ((a) ^ (b))

#define simd_not_UBYTE(a) (~(a))
#define simd_not_BYTE(a) (~(a))
#define simd_not_USHORT(a) (~(a))
#define simd_not_SHORT(a) (~(a))
#define simd_not_ULONG(a) (~(a))
#define simd_not_LONG(a) (~(a))

/* Sign bits -- used in operators below */
#define VBSM 0x8080808080808080UL
#define VSSM 0x8000800080008000UL
#define VLSM 0x8000000080000000UL


/* Basic arithmetic operators */
/* A beginning and may have errors/bugs */
static inline simd_UBYTE _ip_simd_add_UBYTE(simd_UBYTE d, simd_UBYTE s)
{
    unsigned long sc = s, dc = d;
    unsigned long signs = (d ^ s) & VBSM;
    dc &= ~VBSM;
    sc &= ~VBSM;
    return (dc + sc) ^ signs;
}
#define simd_add_BYTE(d,s) _ip_simd_add_UBYTE(d,s)
#define simd_add_UBYTE(d,s) _ip_simd_add_UBYTE(d,s)

static inline simd_USHORT _ip_simd_add_USHORT(simd_USHORT d, simd_USHORT s)
{
    unsigned long sc = s, dc = d;
    unsigned long signs = (d ^ s) & VSSM;
    dc &= ~VSSM;
    sc &= ~VSSM;
    return (dc + sc) ^ signs;
}
#define simd_add_SHORT(d,s) _ip_simd_add_USHORT(d,s)
#define simd_add_USHORT(d,s) _ip_simd_add_USHORT(d,s)

static inline simd_ULONG _ip_simd_add_ULONG(simd_ULONG d, simd_ULONG s)
{
    unsigned long sc = s, dc = d;
    unsigned long signs = (d ^ s) & VLSM;
    dc &= ~VLSM;
    sc &= ~VLSM;
    return (dc + sc) ^ signs;
}
#define simd_add_LONG(d,s) _ip_simd_add_ULONG(d,s)
#define simd_add_ULONG(d,s) _ip_simd_add_ULONG(d,s)

static inline simd_UBYTE _ip_simd_sub_UBYTE(simd_UBYTE d, simd_UBYTE s)
{
    unsigned long sc = s, dc = d;
    unsigned long signs = (d ^ s ^ VBSM) & VBSM;
    dc |= VBSM;
    sc &= ~VBSM;
    return (dc - sc) ^ signs;
}
#define simd_sub_BYTE(d,s) _ip_simd_sub_UBYTE(d,s)
#define simd_sub_UBYTE(d,s) _ip_simd_sub_UBYTE(d,s)

static inline simd_USHORT _ip_simd_sub_USHORT(simd_USHORT d, simd_USHORT s)
{
    unsigned long sc = s, dc = d;
    unsigned long signs = (d ^ s ^ VSSM) & VSSM;
    dc |= VSSM;
    sc &= ~VSSM;
    return (dc - sc) ^ signs;
}
#define simd_sub_SHORT(d,s) _ip_simd_sub_USHORT(d,s)
#define simd_sub_USHORT(d,s) _ip_simd_sub_USHORT(d,s)

static inline simd_ULONG _ip_simd_sub_ULONG(simd_ULONG d, simd_ULONG s)
{
    unsigned long sc = s, dc = d;
    unsigned long signs = (d ^ s ^ VLSM) & VLSM;
    dc |= VLSM;
    sc &= ~VLSM;
    return (dc - sc) ^ signs;
}
#define simd_sub_LONG(d,s) _ip_simd_sub_ULONG(d,s)
#define simd_sub_ULONG(d,s) _ip_simd_sub_ULONG(d,s)

static inline simd_BYTE _ip_simd_add_clipped_BYTE(simd_BYTE d, simd_BYTE s)
{
    unsigned long tm;
    unsigned long t = d^(~VBSM);
    unsigned long tp = minsb8(s,t);
    t = simd_add_BYTE(t, 0x0101010101010101UL);
    tm = maxsb8(s, t);
    t = cmpbge(d, VBSM);
    t = zap(tp, t) | zapnot(tm, t);
    return simd_add_BYTE(d, t);
}
#define simd_add_clipped_BYTE(d,s) _ip_simd_add_clipped_BYTE(d,s)
#define simd_add_clipped_UBYTE(d,s) ((d) + minub8((s), ~(d)))

static inline simd_SHORT _ip_simd_add_clipped_SHORT(simd_SHORT d, simd_SHORT s)
{
    unsigned long tm;
    unsigned long t = d^(~VSSM);
    unsigned long tp = minsw4(s, t);
    unsigned long pmsw = cmpbge(d, VSSM) & 0xaa;
    t = simd_add_SHORT(t, 0x0001000100010001UL);
    tm = maxsw4(s, t);
    pmsw = (pmsw>>1) | pmsw;
    t = zap(tp, pmsw) | zapnot(tm, pmsw);
    return simd_add_SHORT(d, t);
}
#define simd_add_clipped_SHORT(d,s) _ip_simd_add_clipped_SHORT(d,s)
#define simd_add_clipped_USHORT(d,s) ((d) + minuw4((s), ~(d)))

static inline simd_BYTE _ip_simd_sub_clipped_BYTE(simd_BYTE d, simd_BYTE s)
{
    unsigned long tp;
    unsigned long t = d^VBSM;
    unsigned long tm = minsb8(s, t);
    t = simd_add_BYTE(t, 0x0101010101010101UL);
    tp = maxsb8(s, t);
    t = cmpbge(d, VBSM);
    t = zap(tp, t) | zapnot(tm, t);
    return simd_sub_BYTE(d, t);
}
#define simd_sub_clipped_BYTE(d,s) _ip_simd_sub_clipped_BYTE(d,s)
#define simd_sub_clipped_UBYTE(d,s) ((d) - minub8((s), (d)))

static inline simd_SHORT _ip_simd_sub_clipped_SHORT(simd_SHORT d, simd_SHORT s)
{
    unsigned long tp;
    unsigned long t = d^VSSM;
    unsigned long tm = minsw4(s, t);
    unsigned long pmsw = cmpbge(d, VSSM) & 0xaa;
    t = simd_add_SHORT(t, 0x0001000100010001UL);
    tp = maxsw4(s, t);
    pmsw = (pmsw>>1) | pmsw;
    t = zap(tp, pmsw) | zapnot(tm, pmsw);
    return simd_sub_SHORT(d, t);
}
#define simd_sub_clipped_SHORT(d,s) _ip_simd_sub_clipped_SHORT(d,s)
#define simd_sub_clipped_USHORT(d,s) ((d) - minuw4((s), (d)))


/* Still deciding on best ubyte multiplication */
#if 1
static inline simd_UBYTE _ip_simd_mul_UBYTE(simd_UBYTE d, simd_UBYTE s)
{
    unsigned long t0, t1, t2, t3, t4, t5, t6, t7;
    t0 = d*s;
    t1 = extbl(d, 1) * extbl(s, 1);
    t2 = extbl(d, 2) * extbl(s, 2);
    t3 = extbl(d, 3) * extbl(s, 3);
    t4 = extbl(d, 4) * extbl(s, 4);
    t5 = extbl(d, 5) * extbl(s, 5);
    t6 = extbl(d, 6) * extbl(s, 6);
    t7 = extbl(d, 7) * extbl(s, 7);
    return insbl(t0, 0) | insbl(t2, 2) | insbl(t4, 4) | insbl(t6, 6)
	| insbl(t1, 1) | insbl(t3, 3) | insbl(t5, 5) | insbl(t7, 7);
}
#else
#define MULBMASK 0x0000000000ff0000ffUL
static inline simd_UBYTE _ip_simd_mul_UBYTE(simd_UBYTE d, simd_UBYTE s)
{
    unsigned long t0, t1, t2, t3, t4;
    t0 = (d&MULBMASK)*(s&MULBMASK);
    t1 = ((d>>1)&MULBMASK)*((s>>1)&MULBMASK);
    t2 = ((d>>2)&MULBMASK)*((s>>2)&MULBMASK);
    t3 = extbl(d, 6) * extbl(s, 6);
    t4 = extbl(d, 7) * extbl(s, 7);
    return insbl(t0,0) | (extbl(t0,6)<<3) | insbl(t1,1) | (extbl(t1,6)<<4)
      | insbl(t2,3) | (extbl(t2,6)<<5) | insbl(t3,6) | insbl(t4,7);
}
#endif
#define simd_mul_UBYTE(d, s) _ip_simd_mul_UBYTE(d,s)
#define simd_mul_BYTE(d, s) _ip_simd_mul_UBYTE(d, s)

static inline simd_USHORT _ip_simd_mul_USHORT(simd_USHORT d, simd_USHORT s)
{
    unsigned long t0, t2, t4, t6;
    t0 = d*s;
    t2 = extwl(d, 2) * extwl(s, 2);
    t4 = extwl(d, 4) * extwl(s, 4);
    t6 = extwl(d, 6) * extwl(s, 6);
    return inswl(t0, 0) | inswl(t2, 2) | inswl(t4, 4) | inswl(t6, 6);
}
#define simd_mul_USHORT(d, s) _ip_simd_mul_USHORT(d,s)
#define simd_mul_SHORT(d, s) _ip_simd_mul_USHORT(d, s)

static inline simd_ULONG _ip_simd_mul_ULONG(simd_ULONG d, simd_USHORT s)
{
    unsigned long t0, t4;
    t0 = d*s;
    t4 = extll(d,4) * extll(s,4);
    return insll(t0,0) | insll(t4,4);
}
#define simd_mul_ULONG(d,s) _ip_simd_mul_ULONG(d,s)
#define simd_mul_LONG(d,s) _ip_simd_mul_ULONG(d,s)

static inline simd_UBYTE _ip_simd_mul_clipped_UBYTE(simd_UBYTE d, simd_UBYTE s)
{
    unsigned long t0, t1, t2, t3, t4, t5, t6, t7;
    t0 = (d&0xff)*(s&0xff);
    t1 = extbl(d, 1) * extbl(s, 1);
    t2 = extbl(d, 2) * extbl(s, 2);
    t0 = (t0 > 255) ? 255 : t0;
    t3 = extbl(d, 3) * extbl(s, 3);
    t1 = (t1 > 255) ? 255 : t1;
    t4 = extbl(d, 4) * extbl(s, 4);
    t2 = (t2 > 255) ? 255 : t2;
    t5 = extbl(d, 5) * extbl(s, 5);
    t3 = (t3 > 255) ? 255 : t3;
    t6 = extbl(d, 6) * extbl(s, 6);
    t4 = (t4 > 255) ? 255 : t4;
    t7 = extbl(d, 7) * extbl(s, 7);
    t5 = (t5 > 255) ? 255 : t5;
    t6 = (t6 > 255) ? 255 : t6;
    t7 = (t7 > 255) ? 255 : t7;
    return insbl(t0, 0) | insbl(t2, 2) | insbl(t4, 4) | insbl(t6, 6)
	| insbl(t1, 1) | insbl(t3, 3) | insbl(t5, 5) | insbl(t7, 7);
}
#define simd_mul_clipped_UBYTE(d, s) _ip_simd_mul_clipped_UBYTE(d,s)

static inline simd_USHORT _ip_simd_mul_clipped_USHORT(simd_USHORT d, simd_USHORT s)
{
    unsigned long t0, t2, t4, t6;
    t0 = (d&0xffff)*(s&0xffff);
    t2 = extwl(d, 2) * extwl(s, 2);
    t0 = (t0 > 65535) ? 65535 : t0;
    t4 = extwl(d, 4) * extwl(s, 4);
    t2 = (t2 > 65535) ? 65535 : t2;
    t6 = extwl(d, 6) * extwl(s, 6);
    t4 = (t4 > 65535) ? 65535 : t4;
    t6 = (t6 > 65535) ? 65535 : t6;
    return inswl(t0, 0) | inswl(t2, 2) | inswl(t4, 4) | inswl(t6, 6);
 }
#define simd_mul_clipped_USHORT(d, s) _ip_simd_mul_clipped_USHORT(d,s)


/*
 * Min/Max operators
 */

#define simd_min_UBYTE(d, s) ((simd_UBYTE)minub8(d, s))
#define simd_min_BYTE(d, s) ((simd_BYTE)minsb8(d, s))
#define simd_min_USHORT(d, s) ((simd_USHORT)minuw4(d, s))
#define simd_min_SHORT(d, s) ((simd_SHORT)minsw4(d, s))

#define simd_max_UBYTE(d, s) ((simd_UBYTE)maxub8(d, s))
#define simd_max_BYTE(d, s) ((simd_BYTE)maxsb8(d, s))
#define simd_max_USHORT(d, s) ((simd_USHORT)maxuw4(d, s))
#define simd_max_SHORT(d, s) ((simd_SHORT)maxsw4(d, s))


/* Comparison operators; rather limited on Alpha */

static inline simd_UBYTE _ip_simd_cmplt_UBYTE(simd_UBYTE a, simd_UBYTE b)
{
    return zap(-1ULL, cmpbge(a, b));
}

static inline simd_BYTE _ip_simd_cmplt_BYTE(simd_BYTE a, simd_BYTE b)
{
    const simd_BYTE sbit = 0x8080808080808080ULL;
    return _ip_simd_cmplt_UBYTE(a^sbit, b^sbit);
}

#define simd_cmplt_BYTE(a, b) _ip_simd_cmplt_BYTE(a, b)
#define simd_cmplt_UBYTE(a, b) _ip_simd_cmplt_UBYTE(a, b)

static inline simd_UBYTE _ip_simd_cmpeq_UBYTE(simd_UBYTE a, simd_UBYTE b)
{
    return zapnot(-1ULL, cmpbge(a, b) & cmpbge(b, a));
}

#define simd_cmpeq_BYTE(a, b) _ip_simd_cmpeq_UBYTE(a, b)
#define simd_cmpeq_UBYTE(a, b) _ip_simd_cmpeq_UBYTE(a, b)

static inline simd_UBYTE _ip_simd_cmpgt_UBYTE(simd_UBYTE a, simd_UBYTE b)
{
    return zap(-1ULL, cmpbge(b, a));
}

static inline simd_BYTE _ip_simd_cmpgt_BYTE(simd_BYTE a, simd_BYTE b)
{
    const simd_BYTE sbit = 0x8080808080808080ULL;
    return _ip_simd_cmpgt_UBYTE(a^sbit, b^sbit);
}

#define simd_cmpgt_BYTE(a, b) _ip_simd_cmpgt_BYTE(a, b)
#define simd_cmpgt_UBYTE(a, b) _ip_simd_cmpgt_UBYTE(a, b)

/*
 * Select operators
 */

#define simd_select_BYTE(s, a, b) v_select(s, a, b)
#define simd_select_UBYTE(s, a, b) v_select(s, a, b)
#define simd_select_SHORT(s, a, b) v_select(s, a, b)
#define simd_select_USHORT(s, a, b) v_select(s, a, b)
#define simd_select_LONG(s, a, b) v_select(s, a, b)
#define simd_select_ULONG(s, a, b) v_select(s, a, b)


/*
 * Thresholding operators
 */

static inline simd_UBYTE
_ip_simd_threshold_UBYTE(simd_UBYTE s1, const simd_UBYTE vlow, const simd_UBYTE vhigh,
			 const simd_UBYTE vfalse)
{
    uint64_t mask;
    uint64_t res;
    mask = cmpbge(s1, vlow);
    res = zapnot(0xFFFFFFFFFFFFFFFFUL, mask);
    mask = cmpbge(vhigh, s1);
    res = zapnot(res, mask);
    return res ^ vfalse;
}

#define simd_init_threshold_UBYTE() vres ^= -1UL;
#define simd_threshold_UBYTE _ip_simd_threshold_UBYTE


#define simd_init_threshold_BYTE()			\
    const simd_BYTE sofs = 0x8080808080808080ULL;	\
    vlow = simd_xor_BYTE(vlow, sofs);			\
    vhigh = simd_xor_BYTE(vhigh, sofs);			\
    vres ^= -1UL;

#define simd_threshold_BYTE(s1, vlow, vhigh, vfalse)		\
    ({simd_UBYTE t1;						\
	t1 = simd_xor_UBYTE(s1, sofs);				\
	_ip_simd_threshold_UBYTE(t1, vlow, vhigh, vfalse);})

/*
 * Unfortunately there is no cmpwge (compare word greater and equal)
 * instruction otherwise this would be easy.   But we can construct
 * one out of cmpbge, minuw4, and a shift and logical and.
 *
 * The threshold USHORT function below gives an almost 3x speed up over
 * scalar code.  Loop unrolling could possibly give another factor of two
 * speed up but currently the generic threshold simd code (threshold_simd.c)
 * does not support loop unrolling.  I am not sure whether any other
 * architecture would get such gains from loop unrolling so at this stage am
 * not attempting it.
 */

static inline simd_USHORT
_ip_simd_threshold_USHORT(simd_USHORT s1, simd_USHORT s2,
			  const simd_USHORT vlow, const simd_USHORT vhigh,
			  const simd_UBYTE vfalse)
{
    const uint64_t sval = 0xFFFFFFFFFFFFFFFFUL;
    uint64_t t1;
    uint64_t t2;
    uint64_t t3;
    uint64_t t4;;
    t1 = cmpbge(minuw4(vlow, s1), vlow);
    t2 = cmpbge(minuw4(vlow, s2), vlow);
    t3 = cmpbge(minuw4(vhigh, s1), s1);
    t1 = (t1 >> 1) & t1;
    t2 = (t2 >> 1) & t2;
    t4 = cmpbge(minuw4(vhigh, s2), s2);
    t3 = (t3 >> 1) & t3;
    t4 = (t4 >> 1) & t4;
    t1 = pkwb(zapnot(sval, t1 & t3));
    t2 = pkwb(zapnot(sval, t2 & t4));
    return (t1 | (t2<<32)) ^ vfalse;
}

#define simd_init_threshold_USHORT() vres ^= -1UL;
#define simd_threshold_USHORT _ip_simd_threshold_USHORT

#define simd_init_threshold_SHORT()			\
    const simd_USHORT sofs = simd_splat_USHORT(0x8000);	\
    vlow = simd_xor_USHORT(vlow, sofs);			\
    vhigh = simd_xor_USHORT(vhigh, sofs);		\
    vres ^= -1UL;

#define simd_threshold_SHORT(s1, s2, vlow, vhigh, vfalse)	\
    ({simd_USHORT t1, t2;					\
	t1 = simd_xor_USHORT(s1, sofs);				\
	t2 = simd_xor_USHORT(s2, sofs);				\
	_ip_simd_threshold_USHORT(t1, t2, vlow, vhigh, vfalse);})


/*
 * For replicating one value of the vector to all other elements one side of
 * the value. Useful for managing image edges.  Right means replicate in the
 * direction of increasing memory address, i.e. along an image row in
 * increasing pixel order.
 */

static inline simd_UBYTE _ip_simd_replicate_right_UBYTE(simd_UBYTE x, int pos)
{
    const simd_UBYTE vinc = 0x0706050403020100UL;
    simd_UBYTE vpos = simd_splat_UBYTE(pos);
    unsigned long sel = cmpbge(vpos, vinc);
    return  alpha_v_select(sel, x, simd_splat_vN_UBYTE(x, pos));
}

static inline simd_UBYTE _ip_simd_replicate_left_UBYTE(simd_UBYTE x, int pos)
{
    const simd_UBYTE vinc = 0x0706050403020100UL;
    simd_UBYTE vpos = simd_splat_UBYTE(pos);
    unsigned long sel = cmpbge(vinc, vpos);
    return  alpha_v_select(sel, x, simd_splat_vN_UBYTE(x, pos));
}

static inline simd_USHORT _ip_simd_replicate_right_USHORT(simd_USHORT x, int pos)
{
    const simd_UBYTE vinc = 0x0303020201010000UL;
    simd_UBYTE vpos = simd_splat_UBYTE(pos);
    unsigned long sel = cmpbge(vpos, vinc);
    return  alpha_v_select(sel, x, simd_splat_vN_USHORT(x, pos));
}
static inline simd_USHORT _ip_simd_replicate_left_USHORT(simd_USHORT x, int pos)
{
    const simd_UBYTE vinc = 0x0303020201010000UL;
    simd_UBYTE vpos = simd_splat_UBYTE(pos);
    unsigned long sel = cmpbge(vinc, vpos);
    return  alpha_v_select(sel, x, simd_splat_vN_USHORT(x, pos));
}

static inline simd_ULONG _ip_simd_replicate_right_ULONG(simd_ULONG x, int pos)
{
    simd_ULONG replicated = simd_splat_ULONG(x & 0xffffffffUL);
    simd_ULONG sel = (simd_ULONG)(-((long)pos));
    return  v_select(sel, x, replicated);
}
static inline simd_ULONG _ip_simd_replicate_left_ULONG(simd_ULONG x, int pos)
{
    simd_ULONG replicated = simd_splat_ULONG(simd_extract_ULONG(x,1));
    simd_ULONG sel = (simd_ULONG)(-((long)pos));
    return  v_select(sel, replicated, x);
}


#define simd_replicate_right_UBYTE(x, p) _ip_simd_replicate_right_UBYTE(x, p)
#define simd_replicate_right_BYTE(x, p) _ip_simd_replicate_right_UBYTE(x, p)

#define simd_replicate_left_UBYTE(x, p) _ip_simd_replicate_left_UBYTE(x, p)
#define simd_replicate_left_BYTE(x, p) _ip_simd_replicate_left_UBYTE(x, p)

#define simd_replicate_right_USHORT(x, p) _ip_simd_replicate_right_USHORT(x, p)
#define simd_replicate_right_SHORT(x, p) _ip_simd_replicate_right_USHORT(x, p)

#define simd_replicate_left_USHORT(x, p) _ip_simd_replicate_left_USHORT(x, p)
#define simd_replicate_left_SHORT(x, p) _ip_simd_replicate_left_USHORT(x, p)

#define simd_replicate_right_ULONG(x, p) _ip_simd_replicate_right_ULONG(x, p)
#define simd_replicate_right_LONG(x, p) _ip_simd_replicate_right_ULONG(x, p)

#define simd_replicate_left_ULONG(x, p) _ip_simd_replicate_left_ULONG(x, p)
#define simd_replicate_left_LONG(x, p) _ip_simd_replicate_left_ULONG(x, p)

/*
 * zero_right means to zero all elements to the right (i.e. in increasing
 * memory address order) of the specified position pos.  It is like
 * replicate_right but is used to for effecting a zero boundary on the right
 * of images in filtering operations.
 */

static inline simd_UBYTE _ip_simd_zero_right_UBYTE(simd_UBYTE x, int pos)
{
    const simd_UBYTE vinc = 0x0706050403020100UL;
    simd_UBYTE vpos = simd_splat_UBYTE(pos);
    unsigned long sel = cmpbge(vpos, vinc);
    return  alpha_v_select(sel, x, 0UL);
}

static inline simd_USHORT _ip_simd_zero_right_USHORT(simd_USHORT x, int pos)
{
    const simd_UBYTE vinc = 0x0303020201010000UL;
    simd_UBYTE vpos = simd_splat_UBYTE(pos);
    unsigned long sel = cmpbge(vpos, vinc);
    return  alpha_v_select(sel, x, 0UL);
}

static inline simd_ULONG _ip_simd_zero_right_ULONG(simd_ULONG x, int pos)
{
    if (!pos) {
	x = x & 0x00000000ffffffffULL;
    }
    return x;
}

#define simd_zero_right_UBYTE(x, p) _ip_simd_zero_right_UBYTE(x, p)
#define simd_zero_right_BYTE(x, p) _ip_simd_zero_right_UBYTE(x, p)
#define simd_zero_right_USHORT(x, p) _ip_simd_zero_right_USHORT(x, p)
#define simd_zero_right_SHORT(x, p) _ip_simd_zero_right_USHORT(x, p)
#define simd_zero_right_ULONG(x, p) _ip_simd_zero_right_ULONG(x, p)
#define simd_zero_right_LONG(x, p) _ip_simd_zero_right_ULONG(x, p)


static inline simd_UBYTE _ip_simd_zero_left_UBYTE(simd_UBYTE x, int pos)
{
    const simd_UBYTE vinc = 0x0706050403020100UL;
    simd_UBYTE vpos = simd_splat_UBYTE(pos);
    unsigned long sel = cmpbge(vinc, vpos);
    return  alpha_v_select(sel, x, 0UL);
}

static inline simd_USHORT _ip_simd_zero_left_USHORT(simd_USHORT x, int pos)
{
    const simd_UBYTE vinc = 0x0303020201010000UL;
    simd_UBYTE vpos = simd_splat_UBYTE(pos);
    unsigned long sel = cmpbge(vinc, vpos);
    return  alpha_v_select(sel, x, 0UL);
}

static inline simd_ULONG _ip_simd_zero_left_ULONG(simd_ULONG x, int pos)
{
    if (pos) {
	x = x & 0xffffffff00000000ULL;
    }
    return x;
}

#define simd_zero_left_UBYTE(x, p) _ip_simd_zero_left_UBYTE(x, p)
#define simd_zero_left_BYTE(x, p) _ip_simd_zero_left_UBYTE(x, p)
#define simd_zero_left_USHORT(x, p) _ip_simd_zero_left_USHORT(x, p)
#define simd_zero_left_SHORT(x, p) _ip_simd_zero_left_USHORT(x, p)
#define simd_zero_left_ULONG(x, p) _ip_simd_zero_left_ULONG(x, p)
#define simd_zero_left_LONG(x, p) _ip_simd_zero_left_ULONG(x, p)
