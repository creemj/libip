/*
 * median_simd.h
 *
 * Access to the SIMD versions of the median filter.
 */

extern improc_function_IIf ip_simd_median_3x3_bytype[IM_TYPEMAX];
extern improc_function_IIf ip_simd_median_5x5_bytype[IM_TYPEMAX];
