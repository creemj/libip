/*
   Module:  simd-macros.h
   Author:  Michael J. Cree

   Copyright (C) 2016 Michael J. Cree

   Macros useful for implementing simd image operators.

*/


/*
 * FILTER_HORIZ_2PT_FORWARD_INPLACE(im, type, op)
 *
 * Filter an image with a 2pt horizontal forward kernel, i.e. the filter is
 * applied between the current pixel and the one to its right.
 *
 * im - the image to process
 * type - the image type this filter targets (UBYTE, ...)
 * op - the simd operator to use.
 *
 * Some useful operators to use for op are:
 *    simd_min - implements 2-pt morphological erosion
 *    simd_max - implements 2-pt morphological dilation
 *    simd_sub - implements 2-pt forward difference
 *    simd_mean - implements 2-pt horizontal mean filter
 *
 * The image boundary is handled by replicating edge pixels outwards.
 */

#define FILTER_HORIZ_2PT_FORWARD_INPLACE(im, type, op)			\
    do {								\
	int rowlen = im->row_size / sizeof(simd_##type) - 1;		\
        for (int j=0; j<im->size.y; j++) {				\
	    int pos;							\
	    simd_##type this, next, offby1;				\
	    simd_##type *iptr = IM_ROWADR(im, j, v);			\
	    this = next = iptr[0];					\
	    for (int i=0; i<rowlen; i++) {				\
		this = next;						\
		next = iptr[i+1];					\
		offby1 = simd_cvu_imm_##type(this, next, 1);		\
		iptr[i] = op##_##type(offby1, this);			\
	    }								\
	    pos = im->size.x - SIMD_NUM_ELEMENTS(type)*rowlen-1;	\
	    if (pos >= 0) {						\
		this = simd_replicate_right_##type(next, pos);		\
		next = simd_replicate_left_##type(this, pos);		\
		offby1 = simd_cvu_imm_##type(this, next, 1);		\
		iptr[rowlen] = op##_##type(offby1, this);		\
	    }								\
	}								\
    } while (0)



/*
 * FILTER_HORIZ_2PT_BACKWARD_INPLACE(im, type, op)
 *
 * Filter an image with a 2pt horizontal backward kernel, i.e. the filter is
 * applied between the current pixel and the one to its left.
 *
 * im - the image to process
 * type - the image type this filter targets (UBYTE, ...)
 * op - the simd operator to use.
 *
 * Some useful operators to use for op are:
 *    simd_sub - implements 2-pt backward difference
 *
 * The image boundary is handled by replicating edge pixels outwards.
 */

#define FILTER_HORIZ_2PT_BACKWARD_INPLACE(im, type, op)			\
    do {								\
	int rowlen = im->row_size / sizeof(simd_##type);		\
        for (int j=0; j<im->size.y; j++) {				\
	    simd_##type this, prev;					\
	    simd_##type *iptr = IM_ROWADR(im, j, v);			\
	    prev = simd_splat_v0_##type(iptr[0]);			\
	    for (int i=0; i<rowlen; i++) {				\
		simd_##type offby1;					\
		this = iptr[i];						\
		offby1 = simd_cvu_imm_##type(prev, this,		\
					     SIMD_NUM_ELEMENTS(type)-1); \
		iptr[i] = op##_##type(this, offby1);			\
		prev = this;						\
	    }								\
	}								\
    } while (0)


/*
 * FILTER_HORIZ_3PT_CENTRE_INPLACE(im, type, op)
 *
 * Filter an image with a 3pt horizontal centred kernel.
 *
 * im - the image to process
 * type - the image type this filter targets (UBYTE, ...)
 * op - the simd operator to use.
 *
 * Note that the simd operator op takes three arguments, the vector back by
 * 1 pixel, the current vector and the vector forward by 1 pixel.
 *
 * The image boundary is handled by replicating edge pixels outwards.
 */

#define FILTER_HORIZ_3PT_CENTRE_INPLACE(im, type, op)			\
    do {								\
	int rowlen = im->row_size / sizeof(simd_##type);		\
        for (int j=0; j<im->size.y; j++) {				\
	    simd_##type *iptr = IM_ROWADR(im, j, v);			\
	    simd_##type prev, this, next, back1, forward1;		\
	    int pos;							\
	    this = iptr[0];						\
	    prev = simd_splat_v0_##type(this);				\
	    for (int i=0; i<rowlen-1; i++) {				\
		next = iptr[i+1];					\
		forward1 = simd_cvu_imm_##type(this, next, 1);		\
		back1 = simd_cvu_imm_##type(prev, this, SIMD_NUM_ELEMENTS(type)-1); \
		iptr[i] = op##_##type(back1, this, forward1);		\
		prev = this;						\
		this = next;						\
	    }								\
	    pos = im->size.x - SIMD_NUM_ELEMENTS(type)*(rowlen-1) - 1;	\
	    if (pos >= 0) {						\
		this = simd_replicate_right_##type(this, pos);		\
		next = simd_replicate_left_##type(this, pos);		\
		forward1 = simd_cvu_imm_##type(this, next, 1);		\
		back1 = simd_cvu_imm_##type(prev, this, SIMD_NUM_ELEMENTS(type)-1); \
		iptr[rowlen-1] = op##_##type(back1, this, forward1);	\
	    }								\
	}								\
    } while (0)
