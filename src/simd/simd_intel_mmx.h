/*
   Name: ip/simd/intel_mmx.h
   Author: M. J. Cree

   Copyright (C) 2019 Michael J. Cree

   SIMD support for the IP library.

   This defines the Intel MMX support.

*/


/*
 * Intel MMX SIMD support
 *
 * The levels of support are:
 *
 * #define HAVE_INTEL_MMX     (use MMX only)
 * #define HAVE_INTEL_MMXSSE  (use MMX plus 64-bit SIMD in SSE)
 */


#include <ip/divide.h>

#include <mmintrin.h>
#ifdef HAVE_INTEL_MMXSSE
#include <emmintrin.h>
#include <xmmintrin.h>
#endif

#define SIMD_EXIT() _mm_empty()


/* Basic SIMD types */
typedef __m64 simd_UBYTE;
typedef __m64 simd_BYTE;
typedef __m64 simd_USHORT;
typedef __m64 simd_SHORT;
typedef __m64 simd_ULONG;
typedef __m64 simd_LONG;

#define simd_splat_UBYTE(c)  _mm_set1_pi8(c)
#define simd_splat_BYTE(c)   _mm_set1_pi8(c)
#define simd_splat_USHORT(c) _mm_set1_pi16(c)
#define simd_splat_SHORT(c)  _mm_set1_pi16(c)
#define simd_splat_ULONG(c)  _mm_set1_pi32(c)
#define simd_splat_LONG(c)   _mm_set1_pi32(c)

/*
 * Now that the splat routines are defined and, thus, indicate which image
 * types we support in SIMD, now get the rest of the typedefs that are
 * needed for the convert routines below.
 */

#include "simd_td.h"


static inline __m64 _ip_simd_mmx_splat_v0_BYTE(__m64 x)
{
    __m64 tmp = _mm_unpacklo_pi8(x, x);
    tmp = _mm_unpacklo_pi16(tmp, tmp);
    return _mm_unpacklo_pi32(tmp, tmp);
}
#define simd_splat_v0_BYTE(x)  _ip_simd_mmx_splat_v0_BYTE(x)
#define simd_splat_v0_UBYTE(x)  _ip_simd_mmx_splat_v0_BYTE(x)

#ifdef HAVE_INTEL_MMXSSE
/* MMX + SSE */

#define simd_splat_v0_SHORT(x)  _mm_shuffle_pi16(x, 0)
#define simd_splat_v0_USHORT(x) _mm_shuffle_pi16(x, 0)

#else
/* MMX only */

static inline __m64 _ip_simd_mmx_splat_v0_SHORT(__m64 x)
{
    __m64 tmp = _mm_unpacklo_pi16(x, x);
    return _mm_unpacklo_pi32(tmp, tmp);
}
#define simd_splat_v0_SHORT(x)  _ip_simd_mmx_splat_v0_SHORT(x)
#define simd_splat_v0_USHORT(x)  _ip_simd_mmx_splat_v0_SHORT(x)

#endif

#define simd_splat_v0_LONG(x)  _mm_unpacklo_pi32(x, x)
#define simd_splat_v0_ULONG(x) _mm_unpacklo_pi32(x, x)


/*
 * simd_extract
 *
 * Extract the specified element from a vector.
 *
 * MMX does not support this well; we resort to a memory access via a union
 * to get to the element.  Hopefully the compiler can sometimes optimise the
 * memory access away.
 */

union ip_simd_access {
    __m64 v;
    uint8_t ub[8];
    uint16_t us[4];
    uint32_t ul[2];
};

static inline int _ip_simd_extract_UBYTE(simd_UBYTE u, const int pos)
{
    union ip_simd_access x;
    x.v = u;
    return x.ub[pos];
}
#define simd_extract_BYTE(v, p) _ip_simd_extract_UBYTE(v, p)
#define simd_extract_UBYTE(v, p) _ip_simd_extract_UBYTE(v, p)

static inline int _ip_simd_extract_USHORT(simd_USHORT u, const int pos)
{
    union ip_simd_access x;
    x.v = u;
    return x.us[pos];
}
#define simd_extract_SHORT(v, p) _ip_simd_extract_USHORT(v, p)
#define simd_extract_USHORT(v, p) _ip_simd_extract_USHORT(v, p)

static inline uint32_t _ip_simd_extract_ULONG(simd_ULONG u, const int pos)
{
    union ip_simd_access x;
    x.v = u;
    return x.ul[pos];
}
#define simd_extract_LONG(v, p) ((int32_t)_ip_simd_extract_ULONG(v, p))
#define simd_extract_ULONG(v, p) _ip_simd_extract_ULONG(v, p)


#define simd_splat_vN_BYTE(x, n) simd_splat_BYTE(simd_extract_BYTE(x, n))
#define simd_splat_vN_UBYTE(x, n) simd_splat_BYTE(simd_extract_UBYTE(x, n))
#define simd_splat_vN_SHORT(x, n) simd_splat_SHORT(simd_extract_SHORT(x, n))
#define simd_splat_vN_USHORT(x, n) simd_splat_USHORT(simd_extract_USHORT(x, n))
#define simd_splat_vN_LONG(x, n) simd_splat_LONG(simd_extract_LONG(x, n))
#define simd_splat_vN_ULONG(x, n) simd_splat_ULONG(simd_extract_ULONG(x, n))



/*
 * simd_mask
 *
 * Masks for multielement image types (complex and the rgb image types)
 */
#define simd_mask_RGB(r, g, b) _mm_set_pi8(g, r, b, g, r, b, g, r)
#define simd_mask_RGBA(r, g, b, a) _mm_set_pi8(a, b, g, r, a, b, g, r)
#define simd_mask_RGB16(r, g, b) _mm_set_pi16(r, b, g, r)

/*
 * simd_cvu_imm: MMX implementation
 */

static inline simd_UBYTE _ip_simd_cvu_imm_UBYTE(simd_UBYTE l, simd_UBYTE h, const int ofs)
{
    const unsigned char tofs = 8*ofs;
    const unsigned char hofs = (unsigned char)64-tofs;
    return _mm_or_si64(_mm_srli_si64(l, tofs), _mm_slli_si64(h, hofs));
}
#define simd_cvu_imm_UBYTE(l,r,o) _ip_simd_cvu_imm_UBYTE(l,r,o)
#define simd_cvu_imm_BYTE(l,r,o) _ip_simd_cvu_imm_UBYTE(l,r,o)
static inline simd_USHORT _ip_simd_cvu_imm_USHORT(simd_USHORT l, simd_USHORT h, const int ofs)
{
    const unsigned char tofs = 16*ofs;
    const unsigned char hofs = (unsigned char)64-tofs;
    return _mm_or_si64(_mm_srli_si64(l, tofs), _mm_slli_si64(h, hofs));
}
#define simd_cvu_imm_USHORT(l,r,o) _ip_simd_cvu_imm_USHORT(l,r,o)
#define simd_cvu_imm_SHORT(l,r,o) _ip_simd_cvu_imm_USHORT(l,r,o)
static inline simd_ULONG _ip_simd_cvu_imm_ULONG(simd_ULONG l, simd_ULONG h, const int ofs)
{
    const unsigned char tofs = 32*ofs;
    const unsigned char hofs = (unsigned char)64-tofs;
    return _mm_or_si64(_mm_srli_si64(l, tofs), _mm_slli_si64(h, hofs));
}
#define simd_cvu_imm_ULONG(l,r,o) _ip_simd_cvu_imm_ULONG(l,r,o)
#define simd_cvu_imm_LONG(l,r,o) _ip_simd_cvu_imm_ULONG(l,r,o)


/*
 * simd_ldu
 *
 * Load unaligned vector from arbitrary memory address.
 *
 * Some code, on finding no simd_cvu_TYPE macros, will attempt to use
 * simd_ldu_TYPE macros instead.
 */

static inline simd_UBYTE _ip_simd_ldu_UBYTE(void *adr)
{
    simd_UBYTE *left;
    size_t unaligned = (size_t)adr;
    left = (simd_UBYTE *)(unaligned & ~(0x07));
    int shift = 8*(unaligned & 0x07);
    __m64 lshift = _mm_cvtsi32_si64(shift);
    __m64 rshift = _mm_cvtsi32_si64(64-shift);
    return _mm_or_si64(_mm_srl_si64(left[0], lshift), _mm_sll_si64(left[1], rshift));
}
#define simd_ldu_UBYTE(a) _ip_simd_ldu_UBYTE(a)
#define simd_ldu_BYTE(a)   ((simd_BYTE)_ip_simd_ldu_UBYTE(a))
#define simd_ldu_USHORT(a) ((simd_USHORT)_ip_simd_ldu_UBYTE(a))
#define simd_ldu_SHORT(a)  ((simd_SHORT)_ip_simd_ldu_UBYTE(a))
#define simd_ldu_ULONG(a) ((simd_ULONG)_ip_simd_ldu_UBYTE(a))
#define simd_ldu_LONG(a)  ((simd_LONG)_ip_simd_ldu_UBYTE(a))



/*
 * Helper function: selects bits between vectors a and b based on control sel.
 */

static inline __m64 v_select(__m64 sel, __m64 a, __m64 b)
{
    return _mm_or_si64(_mm_and_si64(sel, a), _mm_andnot_si64(sel, b));
}

/*
 * Helper functions for missing min/max operators
 */

static inline simd_BYTE v_min_pi8(simd_BYTE a, simd_BYTE b)
{
    return v_select(_mm_cmpgt_pi8(a,b), b, a);
}
static inline simd_BYTE v_max_pi8(simd_BYTE a, simd_BYTE b)
{
    return v_select(_mm_cmpgt_pi8(a,b), a, b);
}
static inline simd_USHORT v_min_pu16(simd_USHORT a, simd_USHORT b)
{
    simd_USHORT sel = _mm_cmpeq_pi16(_mm_subs_pu16(a, b), _mm_setzero_si64());
    return v_select(sel, a, b);
}
static inline simd_USHORT v_max_pu16(simd_USHORT a, simd_USHORT b)
{
    simd_USHORT sel = _mm_cmpeq_pi16(_mm_subs_pu16(a, b), _mm_setzero_si64());
    return v_select(sel, b, a);
}
static inline simd_LONG v_min_pi32(simd_LONG a, simd_LONG b)
{
    return v_select(_mm_cmpgt_pi32(a,b), b, a);
}
static inline simd_LONG v_max_pi32(simd_LONG a, simd_LONG b)
{
    return v_select(_mm_cmpgt_pi32(a,b), a, b);
}

#ifdef HAVE_INTEL_MMXSSE
#define v_min_pu8(a, b) _mm_min_pu8(a,b)
#define v_min_pi16(a, b) _mm_min_pi16(a,b)
#define v_max_pu8(a, b) _mm_max_pu8(a,b)
#define v_max_pi16(a, b) _mm_max_pi16(a,b)
#else
static inline simd_UBYTE v_min_pu8(simd_UBYTE a, simd_UBYTE b)
{
    simd_UBYTE sel = _mm_cmpeq_pi8(_mm_subs_pu8(a, b), _mm_setzero_si64());
    return v_select(sel, a, b);
}
static inline simd_BYTE v_min_pi16(simd_BYTE a, simd_BYTE b)
{
    return v_select(_mm_cmpgt_pi16(a,b), b, a);
}
static inline simd_UBYTE v_max_pu8(simd_UBYTE a, simd_UBYTE b)
{
    simd_UBYTE sel = _mm_cmpeq_pi8(_mm_subs_pu8(a, b), _mm_setzero_si64());
    return v_select(sel, b, a);
}
static inline simd_BYTE v_max_pi16(simd_BYTE a, simd_BYTE b)
{
    return v_select(_mm_cmpgt_pi16(a,b), a, b);
}
#endif


/*
 * Transpose functions.
 */

static inline simd_USHORT_4 _ip_simd_transpose_USHORT(simd_USHORT_4 x)
{
    simd_USHORT a, b, c, d;
    a = _mm_unpacklo_pi16(x.us[0], x.us[1]);
    c = _mm_unpacklo_pi16(x.us[2], x.us[3]);
    b = _mm_unpacklo_pi32(a, c);
    c = _mm_unpackhi_pi32(a, c);
    a = _mm_unpackhi_pi16(x.us[0], x.us[1]);
    d = _mm_unpackhi_pi16(x.us[2], x.us[3]);
    x.us[0] = b;
    x.us[1] = c;
    x.us[2] = _mm_unpacklo_pi32(a, d);
    x.us[3] = _mm_unpackhi_pi32(a, d);

    return x;
}
#define simd_transpose_USHORT(x) _ip_simd_transpose_USHORT(x)

static inline simd_ULONG_2 _ip_simd_transpose_ULONG(simd_ULONG_2 x)
{
    simd_ULONG a;
    a = _mm_unpacklo_pi32(x.ul[0], x.ul[1]);
    x.ul[1] = _mm_unpackhi_pi32(x.ul[0], x.ul[1]);
    x.ul[0] = a;

    return x;
}
#define simd_transpose_ULONG(x) _ip_simd_transpose_ULONG(x)


/*
 * Conversion routines for converting one image type to another.
 *
 * simd_cnvrt_DTYPE_STYPE
 *   - convert source type STYPE to type DTYPE
 *
 * simd_cnvrt_clip_DTYPE_STYPE
 *   - convert source type STYPE to type DTYPE with clipping (saturation).
 */

/* Zero extend smaller integer to larger integer */

static inline simd_USHORT_2 _ip_simd_cnvrt_USHORT_UBYTE(simd_UBYTE a)
{
    simd_USHORT_2 res;

    res.us[0] = _mm_unpacklo_pi8(a, _mm_setzero_si64());
    res.us[1] = _mm_unpackhi_pi8(a, _mm_setzero_si64());
    return res;
}
#define simd_cnvrt_USHORT_UBYTE(x) _ip_simd_cnvrt_USHORT_UBYTE(x)

static inline simd_ULONG_4 _ip_simd_cnvrt_ULONG_UBYTE(simd_UBYTE a)
{
    simd_ULONG_4 res;
    simd_USHORT_2 s;
    const simd_USHORT vzero = _mm_setzero_si64();

    s = simd_cnvrt_USHORT_UBYTE(a);
    res.ul[0] = _mm_unpacklo_pi16(s.us[0], vzero);
    res.ul[1] = _mm_unpackhi_pi16(s.us[0], vzero);
    res.ul[2] = _mm_unpacklo_pi16(s.us[1], vzero);
    res.ul[3] = _mm_unpackhi_pi16(s.us[1], vzero);
    return res;
}
#define simd_cnvrt_ULONG_UBYTE(x) _ip_simd_cnvrt_ULONG_UBYTE(x)

static inline simd_ULONG_2 _ip_simd_cnvrt_ULONG_USHORT(simd_USHORT a)
{
    simd_ULONG_2 res;
    const simd_USHORT vzero = _mm_setzero_si64();

    res.ul[0] = _mm_unpacklo_pi16(a, vzero);
    res.ul[1] = _mm_unpackhi_pi16(a, vzero);
    return res;
}

#define simd_cnvrt_ULONG_USHORT(x) _ip_simd_cnvrt_ULONG_USHORT(x)

/* SSE2 sign extend smaller integer to larger integer */

static inline simd_SHORT_2 _ip_simd_cnvrt_SHORT_BYTE(simd_BYTE a)
{
    simd_SHORT_2 res;
    simd_BYTE sign;

    sign = _mm_cmpgt_pi8(_mm_setzero_si64(), a);
    res.s[0] = _mm_unpacklo_pi8(a, sign);
    res.s[1] = _mm_unpackhi_pi8(a, sign);
    return res;
}
#define simd_cnvrt_SHORT_BYTE(x) _ip_simd_cnvrt_SHORT_BYTE(x)

static inline simd_LONG_4 _ip_simd_cnvrt_LONG_BYTE(simd_BYTE a)
{
    simd_LONG_4 res;
    simd_BYTE sign;
    simd_SHORT_2 s;

    s = simd_cnvrt_SHORT_BYTE(a);
    sign = _mm_cmpgt_pi8(_mm_setzero_si64(), s.s[0]);
    res.l[0] = _mm_unpacklo_pi16(s.s[0], sign);
    res.l[1] = _mm_unpackhi_pi16(s.s[0], sign);
    sign = _mm_cmpgt_pi8(_mm_setzero_si64(), s.s[1]);
    res.l[2] = _mm_unpacklo_pi16(s.s[1], sign);
    res.l[3] = _mm_unpackhi_pi16(s.s[1], sign);
    return res;
}
#define simd_cnvrt_LONG_BYTE(x) _ip_simd_cnvrt_LONG_BYTE(x)

static inline simd_LONG_2 _ip_simd_cnvrt_LONG_SHORT(simd_SHORT a)
{
    simd_LONG_2 res;
    simd_SHORT sign;

    sign = _mm_cmpgt_pi16(_mm_setzero_si64(), a);
    res.l[0] = _mm_unpacklo_pi16(a, sign);
    res.l[1] = _mm_unpackhi_pi16(a, sign);
    return res;
}
#define simd_cnvrt_LONG_SHORT(x) _ip_simd_cnvrt_LONG_SHORT(x)

/* Truncate larger integer type to smaller */
static inline simd_UBYTE _ip_simd_cnvrt_UBYTE_USHORT(simd_USHORT a, simd_USHORT b)
{
    const simd_USHORT mask = simd_splat_USHORT(0x00ff);

    a = _mm_and_si64(a, mask);
    b = _mm_and_si64(b, mask);
    return _mm_packs_pu16(a, b);
}
#define simd_cnvrt_UBYTE_USHORT(a, b) _ip_simd_cnvrt_UBYTE_USHORT(a, b)

static inline simd_UBYTE _ip_simd_cnvrt_UBYTE_ULONG(simd_ULONG a, simd_ULONG b, simd_ULONG c, simd_ULONG d)
{
    const simd_USHORT mask = simd_splat_ULONG(0x000000ff);

    a = _mm_and_si64(a, mask);
    b = _mm_and_si64(b, mask);
    c = _mm_and_si64(c, mask);
    d = _mm_and_si64(d, mask);
    a = _mm_packs_pu16(a, b);
    b = _mm_packs_pu16(c, d);
    return _mm_packs_pu16(a, b);
}
#define simd_cnvrt_UBYTE_ULONG(a, b, c, d) _ip_simd_cnvrt_UBYTE_ULONG(a, b, c, d)

static inline simd_USHORT _ip_simd_cnvrt_USHORT_ULONG(simd_ULONG a, simd_ULONG b)
{
    a = _mm_srai_pi32(_mm_slli_pi32(a, 16), 16);
    b = _mm_srai_pi32(_mm_slli_pi32(b, 16), 16);
    return _mm_packs_pi32(a, b);
}
#define simd_cnvrt_USHORT_ULONG(a, b) _ip_simd_cnvrt_USHORT_ULONG(a, b)

/* Convert BYTE to other types with clipping */

#define simd_cnvrt_clip_UBYTE_BYTE(a) v_max_pi8(a, _mm_setzero_si64())
#define simd_cnvrt_clip_USHORT_BYTE(a) \
    simd_cnvrt_USHORT_UBYTE(v_max_pi8(a, _mm_setzero_si64()))
#define simd_cnvrt_clip_ULONG_BYTE(a) \
    simd_cnvrt_ULONG_UBYTE(v_max_pi8(a, _mm_setzero_si64()))

/* Convert UBYTE to other types with clipping */

#define simd_cnvrt_clip_BYTE_UBYTE(a) v_min_pu8(a, _mm_set1_pi8(127))

/* Cconvert SHORT to other types with clipping */
#define simd_cnvrt_clip_BYTE_SHORT(a,b) _mm_packs_pi16(a,b)
#define simd_cnvrt_clip_UBYTE_SHORT(a,b) _mm_packs_pu16(a, b)
#define simd_cnvrt_clip_USHORT_SHORT(a) v_max_pi16(a, _mm_setzero_si64())
#define simd_cnvrt_clip_ULONG_SHORT(a) simd_cnvrt_ULONG_USHORT(v_max_pi16(a, _mm_setzero_si64()))

/* Convert USHORT to other types with clipping */

static inline simd_BYTE _ip_simd_cnvrt_clip_BYTE_USHORT(simd_USHORT a, simd_USHORT b)
{
    const simd_USHORT vzero = _mm_setzero_si64();
    const simd_USHORT vmax = simd_splat_USHORT(127);
    simd_USHORT sela, selb;

    sela = _mm_or_si64(_mm_cmpgt_pi16(a, vzero), _mm_cmpeq_pi16(a, vzero));
    a = v_select(sela, a, vmax);
    selb = _mm_or_si64(_mm_cmpgt_pi16(b, vzero), _mm_cmpeq_pi16(b, vzero));
    b = v_select(selb, b, vmax);
    return _mm_packs_pi16(a,b);
}
#define simd_cnvrt_clip_BYTE_USHORT(a,b) _ip_simd_cnvrt_clip_BYTE_USHORT(a,b)

static inline simd_UBYTE _ip_simd_cnvrt_clip_UBYTE_USHORT(simd_USHORT a, simd_USHORT b)
{
    const simd_USHORT vzero = _mm_setzero_si64();
    const simd_USHORT vmax = simd_splat_USHORT(255);
    simd_USHORT sela, selb;

    sela = _mm_or_si64(_mm_cmpgt_pi16(a, vzero), _mm_cmpeq_pi16(a, vzero));
    a = v_select(sela, a, vmax);
    selb = _mm_or_si64(_mm_cmpgt_pi16(b, vzero), _mm_cmpeq_pi16(b, vzero));
    b = v_select(selb, b, vmax);
    return _mm_packs_pu16(a,b);
}
#define simd_cnvrt_clip_UBYTE_USHORT(a,b) _ip_simd_cnvrt_clip_UBYTE_USHORT(a,b)

#define simd_cnvrt_clip_SHORT_USHORT(a) v_min_pu16(a, _mm_set1_pi16(INT16_MAX))

/* Convert LONG to other types with clipping */

#define simd_cnvrt_clip_BYTE_LONG(a,b,c,d) \
    _mm_packs_pi16(_mm_packs_pi32(a,b),_mm_packs_pi32(c,d))

/* Convert LONG to other types with clipping */

static inline simd_UBYTE _ip_simd_cnvrt_clip_UBYTE_LONG(simd_LONG a, simd_LONG b,
							simd_LONG c, simd_LONG d)
{
    const simd_LONG max_ubyte = simd_splat_LONG(0xff);
    const simd_LONG min_ubyte = _mm_setzero_si64();
    simd_LONG ta, tb, tc, td;

    ta = v_max_pi32(a, min_ubyte);
    tb = v_max_pi32(b, min_ubyte);
    tc = v_max_pi32(c, min_ubyte);
    td = v_max_pi32(d, min_ubyte);
    ta = v_min_pi32(ta, max_ubyte);
    tb = v_min_pi32(tb, max_ubyte);
    tc = v_min_pi32(tc, max_ubyte);
    td = v_min_pi32(td, max_ubyte);
    return simd_cnvrt_UBYTE_ULONG(ta, tb, tc, td);
}
#define simd_cnvrt_clip_UBYTE_LONG(a,b,c,d) _ip_simd_cnvrt_clip_UBYTE_LONG(a,b,c,d)

#define simd_cnvrt_clip_SHORT_LONG(a,b) _mm_packs_pi32(a,b)

static inline simd_USHORT _ip_simd_cnvrt_clip_USHORT_LONG(simd_LONG a, simd_LONG b)
{
    const simd_LONG max_ushort = simd_splat_ULONG(0xffff);
    const simd_LONG zero = _mm_setzero_si64();
    simd_LONG ta, tb;

    ta = v_max_pi32(a, zero);
    tb = v_max_pi32(b, zero);
    ta = v_min_pi32(ta, max_ushort);
    tb = v_min_pi32(tb, max_ushort);
    return simd_cnvrt_USHORT_ULONG(ta, tb);
}
#define simd_cnvrt_clip_USHORT_LONG(a, b) _ip_simd_cnvrt_clip_USHORT_LONG(a, b)

#define simd_cnvrt_clip_ULONG_LONG(a) v_max_pi32(a, _mm_setzero_si64())

/* Convert ULONG to other types with clipping */

static inline simd_BYTE _ip_simd_cnvrt_clip_BYTE_ULONG(simd_ULONG a, simd_ULONG b,
						       simd_ULONG c, simd_ULONG d)
{
    const simd_LONG max_byte = simd_splat_ULONG(0x7f);
    const simd_LONG zero = _mm_setzero_si64();
    simd_LONG ta, tb, tc, td;

    ta = v_select(_mm_cmpgt_pi32(zero, a), max_byte, a);
    tb = v_select(_mm_cmpgt_pi32(zero, b), max_byte, b);
    tc = v_select(_mm_cmpgt_pi32(zero, c), max_byte, c);
    td = v_select(_mm_cmpgt_pi32(zero, d), max_byte, d);
    ta = v_min_pi32(ta, max_byte);
    tb = v_min_pi32(tb, max_byte);
    tc = v_min_pi32(tc, max_byte);
    td = v_min_pi32(td, max_byte);
    return (simd_BYTE)simd_cnvrt_UBYTE_ULONG(ta, tb, tc, td);
}
#define simd_cnvrt_clip_BYTE_ULONG(a,b,c,d) _ip_simd_cnvrt_clip_BYTE_ULONG(a,b,c,d)

static inline simd_UBYTE _ip_simd_cnvrt_clip_UBYTE_ULONG(simd_ULONG a, simd_ULONG b,
							 simd_ULONG c, simd_ULONG d)
{
    const simd_LONG max_ubyte = simd_splat_ULONG(0xff);
    const simd_LONG zero = _mm_setzero_si64();
    simd_LONG ta, tb, tc, td;

    ta = v_select(_mm_cmpgt_pi32(zero, a), max_ubyte, a);
    tb = v_select(_mm_cmpgt_pi32(zero, b), max_ubyte, b);
    tc = v_select(_mm_cmpgt_pi32(zero, c), max_ubyte, c);
    td = v_select(_mm_cmpgt_pi32(zero, d), max_ubyte, d);
    ta = v_min_pi32(ta, max_ubyte);
    tb = v_min_pi32(tb, max_ubyte);
    tc = v_min_pi32(tc, max_ubyte);
    td = v_min_pi32(td, max_ubyte);
    return simd_cnvrt_UBYTE_ULONG(ta, tb, tc, td);
}
#define simd_cnvrt_clip_UBYTE_ULONG(a,b,c,d) _ip_simd_cnvrt_clip_UBYTE_ULONG(a,b,c,d)


static inline simd_SHORT _ip_simd_cnvrt_clip_SHORT_ULONG(simd_ULONG a, simd_ULONG b)
{
    const simd_ULONG vzero = _mm_setzero_si64();
    const simd_ULONG vmax = _mm_set1_pi32(0x7fff);
    simd_ULONG sela, selb;

    sela = _mm_cmpgt_pi32(vzero, a);
    a = v_select(sela, vmax, a);
    selb = _mm_cmpgt_pi32(vzero, b);
    b = v_select(selb, vmax, b);
    return _mm_packs_pi32(a,b);
}
#define simd_cnvrt_clip_SHORT_ULONG(a,b) _ip_simd_cnvrt_clip_SHORT_ULONG(a,b)

static inline simd_USHORT _ip_simd_cnvrt_clip_USHORT_ULONG(simd_ULONG a, simd_ULONG b)
{
    const simd_ULONG max_ushort = simd_splat_ULONG(0xffff);
    const simd_ULONG zero = _mm_setzero_si64();
    simd_ULONG ta, tb;

    ta = v_select(_mm_cmpgt_pi32(zero, a), max_ushort, a);
    tb = v_select(_mm_cmpgt_pi32(zero, b), max_ushort, b);
    ta = v_min_pi32(ta, max_ushort);
    tb = v_min_pi32(tb, max_ushort);
    return simd_cnvrt_USHORT_ULONG(ta, tb);
}
#define simd_cnvrt_clip_USHORT_ULONG(a, b) _ip_simd_cnvrt_clip_USHORT_ULONG(a, b)

static inline simd_LONG _ip_simd_cnvrt_clip_LONG_ULONG(simd_ULONG a)
{
    const simd_LONG max_long = simd_splat_ULONG(INT32_MAX);
    const simd_LONG zero = _mm_setzero_si64();
    simd_LONG ta;

    ta = v_select(_mm_cmpgt_pi32(zero, a), max_long, a);
    return v_min_pi32(ta, max_long);
}
#define simd_cnvrt_clip_LONG_ULONG(a) _ip_simd_cnvrt_clip_LONG_ULONG(a)

/* Convert UBYTE/USHORT/ULONG to BINARY */
static inline simd_UBYTE _ip_simd_cnvrt_BINARY_UBYTE(simd_UBYTE x)
{
    const simd_UBYTE vfalse = simd_splat_UBYTE(0);
    const simd_UBYTE vtrue = simd_splat_UBYTE(255);
    return _mm_andnot_si64(_mm_cmpeq_pi8(x, vfalse), vtrue);
}
#define simd_cnvrt_BINARY_UBYTE(x) _ip_simd_cnvrt_BINARY_UBYTE(x)

static inline simd_UBYTE _ip_simd_cnvrt_BINARY_USHORT(simd_USHORT a, simd_USHORT b)
{
    const simd_USHORT vfalse = simd_splat_USHORT(0);
    const simd_USHORT vtrue = simd_splat_USHORT(0xffff);
    a = _mm_andnot_si64(_mm_cmpeq_pi16(a, vfalse), vtrue);
    b = _mm_andnot_si64(_mm_cmpeq_pi16(b, vfalse), vtrue);
    return _mm_packs_pi16(a, b);
}
#define simd_cnvrt_BINARY_USHORT(x, y) _ip_simd_cnvrt_BINARY_USHORT(x, y)

static inline simd_UBYTE _ip_simd_cnvrt_BINARY_ULONG(simd_ULONG a, simd_ULONG b,
						     simd_ULONG c, simd_ULONG d)
{
    const simd_ULONG vfalse = simd_splat_ULONG(0);
    const simd_ULONG vtrue = simd_splat_ULONG(0xffffffff);
    a = _mm_andnot_si64(_mm_cmpeq_pi32(a, vfalse), vtrue);
    b = _mm_andnot_si64(_mm_cmpeq_pi32(b, vfalse), vtrue);
    c = _mm_andnot_si64(_mm_cmpeq_pi32(c, vfalse), vtrue);
    d = _mm_andnot_si64(_mm_cmpeq_pi32(d, vfalse), vtrue);
    return _mm_packs_pi16(_mm_packs_pi32(a, b),_mm_packs_pi32(c, d));
}
#define simd_cnvrt_BINARY_ULONG(x, y, u, v) _ip_simd_cnvrt_BINARY_ULONG(x, y, u, v)


/*
 * Logical operators on the SIMD vectors
 */

#define simd_or_UBYTE(a,b) _mm_or_si64(a,b)
#define simd_or_BYTE(a,b) _mm_or_si64(a,b)
#define simd_or_USHORT(a,b) _mm_or_si64(a,b)
#define simd_or_SHORT(a,b) _mm_or_si64(a,b)
#define simd_or_ULONG(a,b) _mm_or_si64(a,b)
#define simd_or_LONG(a,b) _mm_or_si64(a,b)

#define simd_and_UBYTE(a,b) _mm_and_si64(a,b)
#define simd_and_BYTE(a,b) _mm_and_si64(a,b)
#define simd_and_USHORT(a,b) _mm_and_si64(a,b)
#define simd_and_SHORT(a,b) _mm_and_si64(a,b)
#define simd_and_ULONG(a,b) _mm_and_si64(a,b)
#define simd_and_LONG(a,b) _mm_and_si64(a,b)

#define simd_xor_UBYTE(a,b) _mm_xor_si64(a,b)
#define simd_xor_BYTE(a,b) _mm_xor_si64(a,b)
#define simd_xor_USHORT(a,b) _mm_xor_si64(a,b)
#define simd_xor_SHORT(a,b) _mm_xor_si64(a,b)
#define simd_xor_ULONG(a,b) _mm_xor_si64(a,b)
#define simd_xor_LONG(a,b) _mm_xor_si64(a,b)

#define simd_not_UBYTE(a) _mm_andnot_si64(a, simd_splat_UBYTE(-1))
#define simd_not_BYTE(a) _mm_andnot_si64(a, simd_splat_BYTE(-1))
#define simd_not_USHORT(a) _mm_andnot_si64(a, simd_splat_USHORT(-1))
#define simd_not_SHORT(a) _mm_andnot_si64(a, simd_splat_SHORT(-1))
#define simd_not_ULONG(a) _mm_andnot_si64(a, simd_splat_ULONG(-1))
#define simd_not_LONG(a) _mm_andnot_si64(a, simd_splat_LONG(-1))


/*
 * Shifts
 */

#define simd_shlc_SHORT(x,s) _mm_sll_pi16(x,s)
#define simd_shlc_USHORT(x,s) _mm_sll_pi16(x,s)
#define simd_shlc_LONG(x,s) _mm_sll_pi32(x,s)
#define simd_shlc_ULONG(x,s) _mm_sll_pi32(x,s)

#define simd_shl_imm_SHORT(x,c) _mm_slli_pi16(x,c)
#define simd_shl_imm_USHORT(x,c) _mm_slli_pi16(x,c)
#define simd_shl_imm_LONG(x,c) _mm_slli_pi32(x,c)
#define simd_shl_imm_ULONG(x,c) _mm_slli_pi32(x,c)

#define simd_shr_imm_SHORT(x,c) _mm_srli_pi16(x,c)
#define simd_shr_imm_USHORT(x,c) _mm_srli_pi16(x,c)
#define simd_shr_imm_LONG(x,c) _mm_srli_pi32(x,c)
#define simd_shr_imm_ULONG(x,c) _mm_srli_pi32(x,c)

#define simd_shra_imm_SHORT(x,c) _mm_srai_pi16(x,c)
#define simd_shra_imm_USHORT(x,c) _mm_srai_pi16(x,c)
#define simd_shra_imm_LONG(x,c) _mm_srai_pi32(x,c)
#define simd_shra_imm_ULONG(x,c) _mm_srai_pi32(x,c)

/*
 * Basic arithmetic
 */

/* addition */
#define simd_add_UBYTE(d,s)  _mm_add_pi8(d,s)
#define simd_add_BYTE(d,s)   _mm_add_pi8(d,s)
#define simd_add_USHORT(d,s) _mm_add_pi16(d,s)
#define simd_add_SHORT(d,s)  _mm_add_pi16(d,s)
#define simd_add_ULONG(d,s)  _mm_add_pi32(d,s)
#define simd_add_LONG(d,s)   _mm_add_pi32(d,s)

/* addition with saturation */
#define simd_add_clipped_UBYTE(d,s) _mm_adds_pu8(d,s)
#define simd_add_clipped_BYTE(d,s) _mm_adds_pi8(d,s)
#define simd_add_clipped_USHORT(d,s) _mm_adds_pu16(d,s)
#define simd_add_clipped_SHORT(d,s) _mm_adds_pi16(d,s)

/* subtraction */
#define simd_sub_UBYTE(d,s)  _mm_sub_pi8(d,s)
#define simd_sub_BYTE(d,s)   _mm_sub_pi8(d,s)
#define simd_sub_USHORT(d,s) _mm_sub_pi16(d,s)
#define simd_sub_SHORT(d,s)  _mm_sub_pi16(d,s)
#define simd_sub_ULONG(d,s)  _mm_sub_pi32(d,s)
#define simd_sub_LONG(d,s)   _mm_sub_pi32(d,s)

/* subtraction with saturation */
#define simd_sub_clipped_UBYTE(d,s) _mm_subs_pu8(d,s)
#define simd_sub_clipped_BYTE(d,s) _mm_subs_pi8(d,s)
#define simd_sub_clipped_USHORT(d,s) _mm_subs_pu16(d,s)
#define simd_sub_clipped_SHORT(d,s) _mm_subs_pi16(d,s)

/* multiplication */
#define simd_mul_USHORT(d,s) _mm_mullo_pi16(d,s)
#define simd_mul_SHORT(d,s)  _mm_mullo_pi16(d,s)

/*
 * simd_mulhi
 *
 * Return upper N-bits of N-bit by N-bit multiplication.
 */

#ifdef HAVE_INTEL_MMXSSE
#define simd_mulhi_USHORT(a,b) _mm_mulhi_pu16(a,b)
#endif
#define simd_mulhi_SHORT(a,b) _mm_mulhi_pi16(a,b)


/*
 * simd_rcpc
 * simd_idiv
 *
 * Integer division by a constant (i.e. reused) divisor
 *
 * We provide simd_rcpc functions to calculate a vector integer reciprocal
 * of a scalar divisor, and simd_idiv functions to calculate the integer
 * division using the reciprocal from the simd_rcpc functions.
 *
 * The IP library only expects these at (U)SHORT and (U)LONG precision.
 */

#ifdef HAVE_INTEL_MMXSSE
static inline struct simd_rcp_USHORT simd_rcpc_USHORT(unsigned int d)
{
    /*
     * The ip_urcp16_n16() function may use floating-point therefore reset
     * the MMX registers for floating point.
     */
    _mm_empty();
    struct ip_uint16_rcp_n16 r = ip_urcp16_n16(d);
    struct simd_rcp_USHORT rcp;
    rcp.m = simd_splat_USHORT(r.m);
    rcp.b = simd_splat_USHORT(r.shift_1);
    rcp.s = _mm_cvtsi32_si64(r.shift);
    return rcp;
}

static inline simd_USHORT _ip_simd_idiv_USHORT(simd_USHORT n,
					       struct simd_rcp_USHORT r)
{
    simd_USHORT x1, t;
    /*
     * Calculate the integer division by multiplication by reciprocal.
     * Note that this implementation exploits an optimisation that gives
     * incorrect results for a divisor of 1.
     */
    t = simd_mulhi_USHORT(n, r.m);
    x1 = _mm_srli_pi16(simd_sub_USHORT(n, t), 1);
    return _mm_srl_pi16(simd_add_USHORT(t, x1), r.s);
}
#define simd_idiv_USHORT(n, r) _ip_simd_idiv_USHORT(n, r)
#endif

static inline struct simd_rcp_SHORT simd_rcpc_SHORT(int d)
{
    /*
     * The ip_ircp16() function may use floating-point therefore reset the
     * MMX registers for floating point.
     */
    _mm_empty();
    struct ip_int16_rcp r = ip_ircp16(d);
    struct simd_rcp_SHORT rcp;
    rcp.m = simd_splat_SHORT(r.m);
    rcp.b = simd_splat_SHORT(r.dsign);
    rcp.s = _mm_cvtsi32_si64(r.shift);
    return rcp;
}

static inline simd_SHORT _ip_simd_idiv_SHORT(simd_SHORT n,
					   struct simd_rcp_SHORT r)
{
    simd_SHORT q0;
    q0 = simd_add_SHORT(n, simd_mulhi_SHORT(n, r.m));
    q0 = _mm_sra_pi16(q0, r.s);
    q0 = simd_sub_SHORT(q0,  _mm_srai_pi16(n, 15));
    return simd_sub_SHORT(simd_xor_SHORT(q0, r.b), r.b);
}
#define simd_idiv_SHORT(n, r) _ip_simd_idiv_SHORT(n, r)


/*
 * simd_min
 * simd_max
 *
 * Elementwise Min/Max.
 */

#define simd_min_BYTE(d, s) v_min_pi8(d, s)
#define simd_min_UBYTE(d, s) v_min_pu8(d, s)
#define simd_min_SHORT(d, s) v_min_pi16(d, s)
#define simd_min_USHORT(d, s) v_min_pu16(d, s)

#define simd_max_BYTE(d, s) v_max_pi8(d, s)
#define simd_max_UBYTE(d, s) v_max_pu8(d, s)
#define simd_max_SHORT(d, s) v_max_pi16(d, s)
#define simd_max_USHORT(d, s) v_max_pu16(d, s)


/*
 * Thresholding operators
 *
 * They consist of a simd_threshold_init_<type> macro that can be used to
 * set up necessary variables before entering the processing loop, and a
 * simd_threshold_<type> macro that does the actual pixel processing within
 * the loop and uses any guff set up by simd_threshold_init_<type>.
 */


/*
 * Helper functions: simd element _false_ if low <= s <= high.
 * i.e., these routines return an inverted result.
 */

static inline __m64 v_cmp2_s8(__m64 vs, __m64 vlow, __m64 vhigh)
{
    __m64 t1, t2;
    t1 = _mm_cmpgt_pi8(vlow, vs);
    t2 = _mm_cmpgt_pi8(vs, vhigh);
    return _mm_or_si64(t2, t1);
}
static inline __m64 v_cmp2_s16(__m64 vs, __m64 vlow, __m64 vhigh)
{
    __m64 t1, t2;
    t1 = _mm_cmpgt_pi16(vlow, vs);
    t2 = _mm_cmpgt_pi16(vs, vhigh);
    return _mm_or_si64(t2, t1);
}
static inline __m64 v_cmp2_s32(__m64 vs, __m64 vlow, __m64 vhigh)
{
    __m64 t1, t2;
    t1 = _mm_cmpgt_pi32(vlow, vs);
    t2 = _mm_cmpgt_pi32(vs, vhigh);
    return _mm_or_si64(t2, t1);
}


/*
 * simd_threshold
 * simd_init_threshold
 *
 * Thresholding operators for threshold a value between low limit and high
 * limit.  simd_init_threshold is used to initialise variables (before
 * running image row loop) if need be to enable simd_threshold to work.
 *
 * We exploit simd_init_threshold on SSE to implement thresholding on
 * unsigned pixel types.  The addition of 0x80 to all unsigned bytes in the
 * SIMD vector enables us to do unsigned comparisons with the signed SIMD
 * comparison CPU instruction (which appears to be all that is available).
 */

#define simd_init_threshold_UBYTE()			\
    const simd_UBYTE sofs = simd_splat_UBYTE(0x80);	\
    vlow = simd_add_UBYTE(vlow, sofs);			\
    vhigh = simd_add_UBYTE(vhigh, sofs);

#define simd_threshold_UBYTE(s, vlow, vhigh, vres) \
    ({simd_UBYTE t1;			\
      t1 = simd_add_UBYTE(s, sofs);	\
      t1 = v_cmp2_s8(t1, vlow, vhigh);	\
      _mm_xor_si64(t1, vres);})

/* Have signed compare so no initialisation guff */
#define simd_init_threshold_BYTE()

static inline simd_UBYTE
_ip_simd_threshold_BYTE(simd_BYTE s, const simd_BYTE vlow, const simd_BYTE vhigh,
			const simd_UBYTE vres)
{
    simd_BYTE t1 = v_cmp2_s8(s, vlow, vhigh);
    return _mm_xor_si64(t1, vres);
}
#define simd_threshold_BYTE _ip_simd_threshold_BYTE

/* ushort needs an init function to transform short into ushort */
#define simd_init_threshold_USHORT()                    \
    const simd_USHORT sofs = simd_splat_USHORT(0x8000);	\
    vlow = simd_add_USHORT(vlow, sofs);			\
    vhigh = simd_add_USHORT(vhigh, sofs);

#define simd_threshold_USHORT(s1, s2, vlow, vhigh, vres)	\
    ({simd_USHORT t1, t2;					\
	t1 = simd_add_USHORT(s1, sofs);				\
	t2 = simd_add_USHORT(s2, sofs);				\
	t1 = v_cmp2_s16(t1, vlow, vhigh);			\
	t2 = v_cmp2_s16(t2, vlow, vhigh);			\
	t1 = _mm_packs_pi16(t1, t2);				\
	_mm_xor_si64(t1, vres);})

/* short simpler since short simd operators exist */
#define simd_init_threshold_SHORT()

static inline simd_UBYTE
_ip_simd_threshold_SHORT(simd_SHORT s1, simd_SHORT s2,
			 const simd_SHORT vlow, const simd_SHORT vhigh,
			 const simd_UBYTE vres)
{
    __m64 t1 = v_cmp2_s16(s1, vlow, vhigh);
    __m64 t2 = v_cmp2_s16(s2, vlow, vhigh);
    t1 = _mm_packs_pi16(t1, t2);
    return _mm_xor_si64(t1, vres);
}
#define simd_threshold_SHORT _ip_simd_threshold_SHORT

/* Must be a more efficient way of implementing these no doubt */
#define simd_init_threshold_ULONG()			\
    const simd_ULONG sofs = simd_splat_ULONG(0x8000);	\
    vlow = simd_add_ULONG(vlow, sofs);			\
    vhigh = simd_add_ULONG(vhigh, sofs);

#define simd_threshold_ULONG(s1, s2, s3, s4, vlow, vhigh, vres)	\
    ({__m64 t1, t2, t3;						\
    t1 = simd_add_ULONG(s1, sofs);				\
    t2 = simd_add_ULONG(s2, sofs);				\
    t1 = v_cmp2_s32(t1, vlow, vhigh);				\
    t2 = v_cmp2_s32(t2, vlow, vhigh);				\
    t1 = _mm_packs_pi32(t1, t2);				\
    t2 = simd_add_ULONG(s3, sofs);				\
    t3 = simd_add_ULONG(s4, sofs);				\
    t2 = v_cmp2_s32(t2, vlow, vhigh);				\
    t3 = v_cmp2_s32(t3, vlow, vhigh);				\
    t2 = _mm_packs_pi32(t2, t3);				\
    t1 = _mm_packs_pi16(t1, t2);				\
    _mm_xor_si64(t1, vres);})


static inline simd_UBYTE
_ip_simd_threshold_LONG(simd_LONG s1, simd_LONG s2, simd_LONG s3, simd_LONG s4,
			const simd_LONG vlow, const simd_LONG vhigh,
			const simd_UBYTE vres)
{
    __m64 t1, t2, t3;
    t1 = v_cmp2_s32(s1, vlow, vhigh);
    t2 = v_cmp2_s32(s2, vlow, vhigh);
    t1 = _mm_packs_pi32(t1, t2);
    t2 = v_cmp2_s32(s3, vlow, vhigh);
    t3 = v_cmp2_s32(s4, vlow, vhigh);
    t2 = _mm_packs_pi32(t2, t3);
    t1 = _mm_packs_pi16(t1, t2);
    return _mm_xor_si64(t1, vres);
}
#define simd_init_threshold_LONG()
#define simd_threshold_LONG _ip_simd_threshold_LONG



/*
 * simd_replicate_right
 * simd_replicate_left
 *
 * Replicate the Nth element in the vector to all elements to the right (or
 * left) of the Nth element.  Useful for managing image edges.  Right means
 * replicate in the direction of increasing memory address, i.e. along an
 * image row in increasing pixel order.
 */

static inline simd_UBYTE _ip_simd_replicate_right_UBYTE(simd_UBYTE x, int pos)
{
    const simd_UBYTE vinc = _mm_setr_pi8(0, 1, 2, 3, 4, 5, 6, 7);
    simd_UBYTE vpos = simd_splat_UBYTE(pos);
    simd_UBYTE sel = simd_or_UBYTE(_mm_cmpgt_pi8(vinc, vpos), _mm_cmpeq_pi8(vinc, vpos));
    return v_select(sel, simd_splat_UBYTE(simd_extract_UBYTE(x, pos)), x);
}
static inline simd_UBYTE _ip_simd_replicate_left_UBYTE(simd_UBYTE x, int pos)
{
    const simd_UBYTE vinc = _mm_setr_pi8(0, 1, 2, 3, 4, 5, 6, 7);
    simd_UBYTE vpos = simd_splat_UBYTE(pos);
    simd_UBYTE sel = _mm_cmpgt_pi8(vinc, vpos);
    return v_select(sel, x, simd_splat_UBYTE(simd_extract_UBYTE(x, pos)));
}
static inline simd_USHORT _ip_simd_replicate_right_USHORT(simd_USHORT x, int pos)
{
    const simd_USHORT vinc = _mm_setr_pi16(0, 1, 2, 3);
    simd_USHORT vpos = simd_splat_USHORT(pos);
    simd_USHORT sel = simd_or_USHORT(_mm_cmpgt_pi16(vinc, vpos), _mm_cmpeq_pi16(vinc, vpos));
    return v_select(sel, simd_splat_USHORT(simd_extract_USHORT(x, pos)), x);
}
static inline simd_USHORT _ip_simd_replicate_left_USHORT(simd_USHORT x, int pos)
{
    const simd_USHORT vinc = _mm_setr_pi16(0, 1, 2, 3);
    simd_USHORT vpos = simd_splat_USHORT(pos);
    simd_USHORT sel = _mm_cmpgt_pi16(vinc, vpos);
    return v_select(sel, x, simd_splat_USHORT(simd_extract_USHORT(x, pos)));
}
static inline simd_ULONG _ip_simd_replicate_right_ULONG(simd_ULONG x, int pos)
{
    const simd_ULONG vinc = _mm_setr_pi32(0, 1);
    simd_ULONG vpos = simd_splat_ULONG(pos);
    simd_ULONG sel = simd_or_ULONG(_mm_cmpgt_pi32(vinc, vpos), _mm_cmpeq_pi32(vinc, vpos));
    return v_select(sel, simd_splat_ULONG(simd_extract_ULONG(x, pos)), x);
}
static inline simd_ULONG _ip_simd_replicate_left_ULONG(simd_ULONG x, int pos)
{
    const simd_ULONG vinc = _mm_setr_pi32(0, 1);
    simd_ULONG vpos = simd_splat_ULONG(pos);
    simd_ULONG sel = _mm_cmpgt_pi32(vinc, vpos);
    return v_select(sel, x, simd_splat_ULONG(simd_extract_ULONG(x, pos)));
}

#define simd_replicate_right_UBYTE(x, p) _ip_simd_replicate_right_UBYTE(x, p)
#define simd_replicate_right_BYTE(x, p) _ip_simd_replicate_right_UBYTE(x, p)
#define simd_replicate_left_UBYTE(x, p) _ip_simd_replicate_left_UBYTE(x, p)
#define simd_replicate_left_BYTE(x, p) _ip_simd_replicate_left_UBYTE(x, p)

#define simd_replicate_right_USHORT(x, p) _ip_simd_replicate_right_USHORT(x, p)
#define simd_replicate_right_SHORT(x, p) _ip_simd_replicate_right_USHORT(x, p)
#define simd_replicate_left_USHORT(x, p) _ip_simd_replicate_left_USHORT(x, p)
#define simd_replicate_left_SHORT(x, p) _ip_simd_replicate_left_USHORT(x, p)

#define simd_replicate_right_ULONG(x, p) _ip_simd_replicate_right_ULONG(x, p)
#define simd_replicate_right_LONG(x, p) _ip_simd_replicate_right_ULONG(x, p)
#define simd_replicate_left_ULONG(x, p) _ip_simd_replicate_left_ULONG(x, p)
#define simd_replicate_left_LONG(x, p) _ip_simd_replicate_left_ULONG(x, p)


/*
 * simd_zero_right
 *
 * zero_right means to zero all elements to the right (i.e. in increasing
 * memory address order) of the specified position pos.  It is like
 * replicate_right but is used to for effecting a zero boundary on the right
 * side of images in filtering operations.
 */

static inline simd_UBYTE _ip_simd_zero_right_UBYTE(simd_UBYTE x, int pos)
{
    const simd_UBYTE vinc = _mm_setr_pi8(0, 1, 2, 3, 4, 5, 6, 7);
    simd_UBYTE vpos = simd_splat_UBYTE(pos);
    return _mm_andnot_si64(_mm_cmpgt_pi8(vinc, vpos), x);
}

static inline simd_USHORT _ip_simd_zero_right_USHORT(simd_USHORT x, int pos)
{
    const simd_USHORT vinc = _mm_setr_pi16(0, 1, 2, 3);
    simd_USHORT vpos = simd_splat_USHORT(pos);
    return _mm_andnot_si64(_mm_cmpgt_pi16(vinc, vpos), x);
}

static inline simd_ULONG _ip_simd_zero_right_ULONG(simd_ULONG x, int pos)
{
    const simd_ULONG vinc = _mm_setr_pi32(0, 1);
    simd_ULONG vpos = simd_splat_ULONG(pos);
    return _mm_andnot_si64(_mm_cmpgt_pi32(vinc, vpos), x);
}

#define simd_zero_right_UBYTE(x, p) _ip_simd_zero_right_UBYTE(x, p)
#define simd_zero_right_BYTE(x, p) _ip_simd_zero_right_UBYTE(x, p)
#define simd_zero_right_USHORT(x, p) _ip_simd_zero_right_USHORT(x, p)
#define simd_zero_right_SHORT(x, p) _ip_simd_zero_right_USHORT(x, p)
#define simd_zero_right_ULONG(x, p) _ip_simd_zero_right_ULONG(x, p)
#define simd_zero_right_LONG(x, p) _ip_simd_zero_right_ULONG(x, p)

static inline simd_UBYTE _ip_simd_zero_left_UBYTE(simd_UBYTE x, int pos)
{
    const simd_UBYTE vinc = _mm_setr_pi8(0, 1, 2, 3, 4, 5, 6, 7);
    simd_UBYTE vpos = simd_splat_UBYTE(pos);
    return _mm_andnot_si64(_mm_cmpgt_pi8(vpos, vinc), x);
}

static inline simd_USHORT _ip_simd_zero_left_USHORT(simd_USHORT x, int pos)
{
    const simd_USHORT vinc = _mm_setr_pi16(0, 1, 2, 3);
    simd_USHORT vpos = simd_splat_USHORT(pos);
    return _mm_andnot_si64(_mm_cmpgt_pi16(vpos, vinc), x);
}

static inline simd_ULONG _ip_simd_zero_left_ULONG(simd_ULONG x, int pos)
{
    const simd_ULONG vinc = _mm_setr_pi32(0, 1);
    simd_ULONG vpos = simd_splat_ULONG(pos);
    return _mm_andnot_si64(_mm_cmpgt_pi32(vpos, vinc), x);
}

#define simd_zero_left_UBYTE(x, p) _ip_simd_zero_left_UBYTE(x, p)
#define simd_zero_left_BYTE(x, p) _ip_simd_zero_left_UBYTE(x, p)
#define simd_zero_left_USHORT(x, p) _ip_simd_zero_left_USHORT(x, p)
#define simd_zero_left_SHORT(x, p) _ip_simd_zero_left_USHORT(x, p)
#define simd_zero_left_ULONG(x, p) _ip_simd_zero_left_ULONG(x, p)
#define simd_zero_left_LONG(x, p) _ip_simd_zero_left_ULONG(x, p)
