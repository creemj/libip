/*
   Module: src/simd/image_simd.c
   Author: M.J.Cree

   SIMD implementation for the im_set_av() routine found in image.c

   Copyright (C) 2007, 2015, 2016 Michael J. Cree

*/

#include <ip/ip.h>

#include "../internal.h"
#include "../function_ops.h"

#include "simd.h"
#include "image_simd.h"


/*
 * Copy aligned memory of a buffer size that is an integer number times the
 * simd vector size by simd vector.
 */

static void ip_simd_copy_mem(void *dest, void *src, size_t size)
{
    simd_UBYTE *sptr, *dptr;

    dptr = (simd_UBYTE *)dest;
    sptr = (simd_UBYTE *)src;
    size /= sizeof(simd_UBYTE);
    for (size_t j=0; j<size; j++) {
	*dptr++ = *sptr++;
    }
}


/*
 * Copy image data from one image to another of the same type.  The unused
 * flag is to make this routine compatible with the convert.c SIMD routines
 * API where it is also used.
 */

void ip_simd_copy_image_data(ip_image *dest, ip_image *src, int flag)
{
    if ((src->status & IP_IMDATA_ONE_ARRAY) &&
	(dest->status & IP_IMDATA_ONE_ARRAY)) {
	/* Can copy image data as one array (we end up copying
	 * the row padding--if any--too)
	 */
	ip_simd_copy_mem(dest->imbuff, src->imbuff, (size_t)src->row_size*src->size.y);
    }else{
	/* Must copy image data row by row */
	for (int j=0; j<src->size.y; j++) {
	    ip_simd_copy_mem(IM_ROWADR(dest,j,v),IM_ROWADR(src,j,v),
		   src->size.x * ip_image_type_info[src->type].typesize);
	}
    }
}



static void simd_im_set_basic(ip_image *im, simd_UBYTE v)
{
    int rowlen = im->row_size / sizeof(simd_UBYTE);

    for (int j=0; j<im->size.y; j++) {
	simd_UBYTE *ip = IM_ROWADR(im, j, v);
	for (int i=0; i<rowlen; i++) {
	    *ip++ = v;
	}
    }
}


static void simd_im_set_masked(ip_image *im, simd_UBYTE v, const simd_UBYTE mask)
{
    int rowlen = im->row_size / sizeof(simd_UBYTE);
    v = simd_and_UBYTE(v, simd_not_UBYTE(mask));

    for (int j=0; j<im->size.y; j++) {
	simd_UBYTE *ip = IM_ROWADR(im, j, v);
	for (int i=0; i<rowlen; i++) {
	    simd_UBYTE tmp;
	    PREFETCH_READ_WRITE(ip+PREFETCH_OFS);
	    tmp = simd_and_UBYTE(*ip, mask);
	    *ip++ = simd_or_UBYTE(v, tmp);
	}
    }
}

#define generate_im_set_int_function(type, basetype)			\
    static void simd_im_set_av_ ## type(ip_image *im, const ip_anyval av, int flag) \
    {									\
	const basetype val = (basetype)(int64_t)av.ull[0];		\
	simd_im_set_basic(im, (simd_UBYTE)simd_splat_ ## type(val));	\
	SIMD_EXIT();							\
    }

#define generate_im_set_float_function(type, basetype)			\
    static void simd_im_set_av_ ## type(ip_image *im, const ip_anyval av, int flag) \
    {									\
	const basetype val = (basetype)av.d[0];				\
	simd_im_set_basic(im, (simd_UBYTE)simd_splat_ ## type(val));	\
	SIMD_EXIT();							\
    }

#define generate_im_set_complex_function(type, basetype)		\
    static void simd_im_set_av_ ## type(ip_image *im, const ip_anyval av, int flag) \
    {									\
	const basetype real = (basetype)av.d[0];			\
	const basetype imag = (basetype)av.d[1];			\
	const simd_UBYTE cv = (simd_UBYTE)simd_splat_ ## type(real, imag); \
	if (flag == NO_FLAG) {						\
	    simd_im_set_basic(im, cv);					\
	}else{								\
	    simd_UBYTE mask;						\
	    if (flag & FLG_SREAL) {					\
		mask = (simd_UBYTE)simd_mask_ ## type(0, -1);		\
	    }else{							\
		mask = (simd_UBYTE)simd_mask_ ## type(-1, 0);		\
	    }								\
	    simd_im_set_masked(im, cv, mask);				\
	}								\
	SIMD_EXIT();							\
    }

#define generate_im_set_rgb_function(type, basett)				\
static void simd_im_set_av_ ## type(ip_image *im, const ip_anyval av, int flag) \
{									\
    int rowlen = im->row_size / sizeof(simd_##basett);			\
    if (flag == NO_FLAG) {						\
	for (int j=0; j<im->size.y; j++) {				\
	    simd_##basett cv = simd_mask_ ## type (av.ul[0], av.ul[1], av.ul[2]); \
	    simd_##basett *ip = IM_ROWADR(im, j, v);			\
	    for (int i=0; i<rowlen; i++) {				\
		*ip++ = cv;						\
		cv = simd_ror_ ## type(cv);				\
	    }								\
	}								\
    }else{								\
	simd_##basett vmask;						\
	vmask = (simd_##basett)simd_mask_ ## type((flag & FLG_SRED) ? 0 : -1, \
						  (flag & FLG_SGREEN) ? 0 : -1, \
						  (flag & FLG_SBLUE) ? 0 : -1); \
 	for (int j=0; j<im->size.y; j++) {				\
	    simd_##basett cv = simd_mask_ ## type(av.ul[0], av.ul[1], av.ul[2]); \
	    simd_##basett *ip = IM_ROWADR(im, j, v);			\
	    simd_##basett mask = vmask;					\
	    cv = simd_and_##basett(cv, simd_not_##basett(mask));	\
	    for (int i=0; i<rowlen; i++) {				\
		simd_##basett tmp;					\
		PREFETCH_READ_WRITE(ip+PREFETCH_OFS);			\
		tmp = simd_and_##basett(*ip, mask);			\
		*ip++ = simd_or_##basett(cv, tmp);			\
		cv = simd_ror_ ## type(cv);				\
		mask = simd_ror_ ## type(mask);				\
	    }								\
	}								\
    }									\
    SIMD_EXIT();							\
}



generate_im_set_int_function(BYTE, int8_t)
generate_im_set_int_function(UBYTE, uint8_t)
generate_im_set_int_function(SHORT, int16_t)
generate_im_set_int_function(USHORT, uint16_t)
generate_im_set_int_function(LONG, int32_t)
generate_im_set_int_function(ULONG, uint32_t)

#ifdef simd_splat_FLOAT
generate_im_set_float_function(FLOAT, float)
#endif
#ifdef simd_splat_DOUBLE
generate_im_set_float_function(DOUBLE, double)
#endif
#ifdef simd_splat_COMPLEX
generate_im_set_complex_function(COMPLEX, float)
#endif
#ifdef simd_splat_DCOMPLEX
generate_im_set_complex_function(DCOMPLEX, double)
#endif

#ifdef simd_ror_RGB
generate_im_set_rgb_function(RGB, UBYTE);
generate_im_set_rgb_function(RGB16, USHORT);
#endif

static void simd_im_set_av_RGBA(ip_image *im, const ip_anyval av, int flag)
{
    const simd_UBYTE cv = simd_mask_RGBA(av.ul[0], av.ul[1], av.ul[2], av.ul[3]);
    if (flag == NO_FLAG) {
	simd_im_set_basic(im, cv);
    }else{
	simd_UBYTE mask;
	mask = (simd_UBYTE)simd_mask_RGBA((flag & FLG_SRED) ? 0 : -1,
					  (flag & FLG_SGREEN) ? 0 : -1,
					  (flag & FLG_SBLUE) ? 0 : -1,
					  (flag & FLG_SALPHA) ? 0 : -1);
	simd_im_set_masked(im, cv, mask);
    }
    SIMD_EXIT();
}


improc_function_Iaf ip_simd_set_bytype[IM_TYPEMAX] = {
    [IM_BYTE] = simd_im_set_av_BYTE,
    [IM_UBYTE] = simd_im_set_av_UBYTE,
    [IM_SHORT] = simd_im_set_av_SHORT,
    [IM_USHORT] = simd_im_set_av_USHORT,
    [IM_LONG] = simd_im_set_av_LONG,
    [IM_ULONG] = simd_im_set_av_ULONG,
#ifdef simd_splat_FLOAT
    [IM_FLOAT] = simd_im_set_av_FLOAT,
#endif
#ifdef simd_splat_DOUBLE
    [IM_DOUBLE] = simd_im_set_av_DOUBLE,
#endif
#ifdef simd_splat_COMPLEX
    [IM_COMPLEX] = simd_im_set_av_COMPLEX,
#endif
#ifdef simd_splat_DCOMPLEX
    [IM_DCOMPLEX] = simd_im_set_av_DCOMPLEX,
#endif
#ifdef simd_ror_RGB
    [IM_RGB] = simd_im_set_av_RGB,
    [IM_RGB16] = simd_im_set_av_RGB16,
#endif
    [IM_RGBA] = simd_im_set_av_RGBA,
    [IM_BINARY] = simd_im_set_av_UBYTE,
    [IM_PALETTE] = simd_im_set_av_UBYTE
};
