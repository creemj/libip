/*
   Module: src/simd/convert_simd.c
   Author: M. J. Cree

   SIMD implementation of conversion of image types.

   Copyright (C) 2015, 2016 Michael J. Cree
*/


#include <ip/ip.h>

#include "../internal.h"
#include "../function_ops.h"

#include "simd.h"
#include "image_simd.h"
#include "convert_simd.h"


#define generate_simd_convert(dt, st, nom)				\
    static void ip_simd_convert_##nom##dt##_##st(ip_image *dest, ip_image *src, int unused) \
    {									\
	int length = src->row_size / sizeof(simd_##st);			\
	for (int j=0; j<dest->size.y; j++) {				\
	    simd_##dt *dd = IM_ROWADR(dest, j, v);			\
	    simd_##st *ss = IM_ROWADR(src, j, v);			\
	    for (int i=0; i<length; i++) {				\
		*dd++ = simd_cnvrt_##nom##dt##_##st(*ss++);		\
	    }								\
	}								\
	SIMD_EXIT();							\
    }

#define generate_simd_convert_s2(dt, st, nom)				\
    static void ip_simd_convert_##nom##dt##_##st(ip_image *dest, ip_image *src, int unused) \
    {									\
	int length = src->row_size / sizeof(simd_##st);			\
	int extraone = length & 1;					\
	length >>= 1;							\
	for (int j=0; j<dest->size.y; j++) {				\
	    simd_##dt *dd = IM_ROWADR(dest, j, v);			\
	    simd_##st *ss = IM_ROWADR(src, j, v);			\
	    for (int i=0; i<length; i++) {				\
		*dd++ = simd_cnvrt_##nom##dt##_##st(ss[0], ss[1]);	\
		ss += 2;						\
	    }								\
	    if (extraone)						\
		*dd = simd_cnvrt_##nom##dt##_##st(*ss, simd_splat_##st(0)); \
	}								\
	SIMD_EXIT();							\
    }

#define generate_simd_convert_s4(dt, st, nom)				\
    static void ip_simd_convert_##nom##dt##_##st(ip_image *dest, ip_image *src, int unused) \
    {									\
	int length = src->row_size / sizeof(simd_##st);			\
	int extras = length & 3;					\
	length >>= 2;							\
	for (int j=0; j<dest->size.y; j++) {				\
	    simd_##dt *dd = IM_ROWADR(dest, j, v);			\
	    simd_##st *ss = IM_ROWADR(src, j, v);			\
	    for (int i=0; i<length; i++) {				\
		*dd++ = simd_cnvrt_##nom##dt##_##st(ss[0], ss[1], ss[2], ss[3]); \
		ss += 4;						\
	    }								\
	    if (extras) {						\
		simd_##st vzero = simd_splat_##st(0);			\
		*dd = simd_cnvrt_##nom##dt##_##st(ss[0],		\
						   extras > 1 ? ss[1] : vzero, \
						   extras > 2 ? ss[2] : vzero, \
						   vzero);		\
	    }								\
	}								\
	SIMD_EXIT();							\
    }

#define generate_simd_convert_s8(dt, st, nom)				\
    static void ip_simd_convert_##nom##dt##_##st(ip_image *dest, ip_image *src, int unused) \
    {									\
	int length = src->row_size / sizeof(simd_##st);			\
	int extras = length & 7;					\
	length >>= 3;							\
	for (int j=0; j<dest->size.y; j++) {				\
	    simd_##dt *dd = IM_ROWADR(dest, j, v);			\
	    simd_##st *ss = IM_ROWADR(src, j, v);			\
	    for (int i=0; i<length; i++) {				\
		*dd++ = simd_cnvrt_##nom##dt##_##st(ss[0], ss[1], ss[2], ss[3],  \
						    ss[4], ss[5], ss[6], ss[7]); \
		ss += 8;						\
	    }								\
	    if (extras) {						\
		simd_##st vzero = simd_splat_##st(0);			\
		*dd = simd_cnvrt_##nom##dt##_##st(ss[0],		\
						   extras > 1 ? ss[1] : vzero, \
						   extras > 2 ? ss[2] : vzero, \
						   extras > 3 ? ss[3] : vzero, \
						   extras > 4 ? ss[4] : vzero, \
						   extras > 5 ? ss[5] : vzero, \
						   extras > 6 ? ss[6] : vzero, \
						   vzero);		\
	    }								\
	}								\
	SIMD_EXIT();							\
    }



#define generate_simd_convert_d2(dt, tt, st, nom)			\
    static void ip_simd_convert_##nom##dt##_##st(ip_image *dest, ip_image *src, int unused) \
    {									\
	int length = dest->row_size / sizeof(simd_##dt);		\
	int extraone = length & 1;					\
	length >>= 1;							\
	for (int j=0; j<dest->size.y; j++) {				\
	    simd_##dt##_2 res;						\
	    simd_##dt *dd = IM_ROWADR(dest, j, v);			\
	    simd_##st *ss = IM_ROWADR(src, j, v);			\
	    for (int i=0; i<length; i++) {				\
		res = simd_cnvrt_##nom##dt##_##st(*ss++);		\
		dd[0] = res.tt[0];					\
		dd[1] = res.tt[1];					\
		dd += 2;						\
	    }								\
	    if (extraone) {						\
		res = simd_cnvrt_##nom##dt##_##st(*ss);			\
		dd[0] = res.tt[0];					\
	    }								\
	}								\
	SIMD_EXIT();							\
    }

#define generate_simd_convert_d4(dt, tt, st, nom)			\
    static void ip_simd_convert_##nom##dt##_##st(ip_image *dest, ip_image *src, int unused) \
    {									\
	int length = dest->row_size / sizeof(simd_##dt);		\
	int extras = length & 3;					\
	length >>= 2;							\
	for (int j=0; j<dest->size.y; j++) {				\
	    simd_##dt##_4 res;						\
	    simd_##dt *dd = IM_ROWADR(dest, j, v);			\
	    simd_##st *ss = IM_ROWADR(src, j, v);			\
	    for (int i=0; i<length; i++) {				\
		res = simd_cnvrt_##nom##dt##_##st(*ss++);		\
		dd[0] = res.tt[0];					\
		dd[1] = res.tt[1];					\
		dd[2] = res.tt[2];					\
		dd[3] = res.tt[3];					\
		dd += 4;						\
	    }								\
	    if (extras) {						\
		res = simd_cnvrt_##nom##dt##_##st(*ss);			\
		dd[0] = res.tt[0];					\
		if (extras>1) dd[1] = res.tt[1];			\
		if (extras>2) dd[2] = res.tt[2];			\
	    }								\
	}								\
	SIMD_EXIT();							\
    }

#define generate_simd_convert_d8(dt, tt, st, nom)			\
    static void ip_simd_convert_##nom##dt##_##st(ip_image *dest, ip_image *src, int unused) \
    {									\
	int length = dest->row_size / sizeof(simd_##dt);		\
	int extras = length & 7;					\
	length >>= 3;							\
	for (int j=0; j<dest->size.y; j++) {				\
	    simd_##dt##_8 res;						\
	    simd_##dt *dd = IM_ROWADR(dest, j, v);			\
	    simd_##st *ss = IM_ROWADR(src, j, v);			\
	    for (int i=0; i<length; i++) {				\
		res = simd_cnvrt_##nom##dt##_##st(*ss++);		\
		dd[0] = res.tt[0];					\
		dd[1] = res.tt[1];					\
		dd[2] = res.tt[2];					\
		dd[3] = res.tt[3];					\
		dd[4] = res.tt[4];					\
		dd[5] = res.tt[5];					\
		dd[6] = res.tt[6];					\
		dd[7] = res.tt[7];					\
		dd += 8;						\
	    }								\
	    if (extras) {						\
		res = simd_cnvrt_##nom##dt##_##st(*ss);			\
		dd[0] = res.tt[0];					\
		if (extras>1) dd[1] = res.tt[1];			\
		if (extras>2) dd[2] = res.tt[2];			\
		if (extras>3) dd[3] = res.tt[3];			\
		if (extras>4) dd[4] = res.tt[4];			\
		if (extras>5) dd[5] = res.tt[5];			\
		if (extras>6) dd[6] = res.tt[6];			\
	    }								\
	}								\
	SIMD_EXIT();							\
    }


#define generate_simd_convert_s3d4(dt, tt, st, nom)			\
    static void ip_simd_convert_##nom##dt##_##st(ip_image *dest, ip_image *src, int unused) \
    {									\
	int length = src->row_size / sizeof(simd_##st);			\
	int sextras = length % 3;					\
	int dextras = (dest->row_size / sizeof(simd_##dt)) & 3;		\
	if (!sextras && dextras) {					\
	    sextras += 3;						\
	    length -= 3;						\
	}								\
	length /= 3;							\
	for (int j=0; j<dest->size.y; j++) {				\
	    simd_##dt##_4 res;						\
	    simd_##dt *dd = IM_ROWADR(dest, j, v);			\
	    simd_##st *ss = IM_ROWADR(src, j, v);			\
	    for (int i=0; i<length; i++) {				\
		res = simd_cnvrt_##nom##dt##_##st(ss[0], ss[1], ss[2]); \
		dd[0] = res.tt[0];					\
		dd[1] = res.tt[1];					\
		dd[2] = res.tt[2];					\
		dd[3] = res.tt[3];					\
		ss += 3;						\
		dd += 4;						\
	    }								\
	    if (sextras) {						\
		simd_##st vzero = simd_splat_##st(0);			\
		res = simd_cnvrt_##nom##dt##_##st(ss[0],		\
						  sextras > 1 ? ss[1] : vzero, \
						  sextras > 2 ? ss[2] : vzero); \
		dd[0] = res.tt[0];					\
		if (dextras>1) dd[1] = res.tt[1];			\
		if (dextras>2) dd[2] = res.tt[2];			\
	    }								\
	}								\
	SIMD_EXIT();							\
    }

#define generate_simd_convert_s4d3(dt, tt, st, nom)			\
    static void ip_simd_convert_##nom##dt##_##st(ip_image *dest, ip_image *src, int unused) \
    {									\
	int length = src->row_size / sizeof(simd_##st);			\
	int dlength = dest->row_size / sizeof(simd_##dt);		\
	int sextras = length & 3;					\
	length /= 4;							\
	int dextras = dlength - 3*length;				\
	if (dextras < 0) {						\
	    sextras += 4;						\
	    dextras += 3;						\
	    length -= 1;						\
	}								\
	for (int j=0; j<dest->size.y; j++) {				\
	    simd_##dt##_3 res;						\
	    simd_##dt *dd = IM_ROWADR(dest, j, v);			\
	    simd_##st *ss = IM_ROWADR(src, j, v);			\
	    for (int i=0; i<length; i++) {				\
		res = simd_cnvrt_##nom##dt##_##st(ss[0], ss[1], ss[2], ss[3]); \
		dd[0] = res.tt[0];					\
		dd[1] = res.tt[1];					\
		dd[2] = res.tt[2];					\
		ss += 4;						\
		dd += 3;						\
	    }								\
	    if (sextras) {						\
		simd_##st vzero = simd_splat_##st(0);			\
		res = simd_cnvrt_##nom##dt##_##st(ss[0],		\
						  sextras > 1 ? ss[1] : vzero, \
						  sextras > 2 ? ss[2] : vzero, \
						  sextras > 3 ? ss[3] : vzero); \
		dd[0] = res.tt[0];					\
		if (dextras>1) dd[1] = res.tt[1];			\
		if (dextras>2) dd[2] = res.tt[2];			\
	    }								\
	}								\
	SIMD_EXIT();							\
    }

//	printf("(%d,%d) -> %d %d %d %d\n", src->size.x, src->size.y, length, dlength, sextras, dextras);

/* Conversions to binary */

#define simd_BINARY simd_UBYTE

#ifdef simd_cnvrt_BINARY_UBYTE
generate_simd_convert(BINARY, UBYTE, )
#else
#define ip_simd_convert_BINARY_UBYTE NULL
#endif

#ifdef simd_cnvrt_BINARY_USHORT
generate_simd_convert_s2(BINARY, USHORT, )
#else
#define ip_simd_convert_BINARY_USHORT NULL
#endif

#ifdef simd_cnvrt_BINARY_ULONG
generate_simd_convert_s4(BINARY, ULONG, )
#else
#define ip_simd_convert_BINARY_ULONG NULL
#endif

#ifdef simd_cnvrt_BINARY_FLOAT
generate_simd_convert_s4(BINARY, FLOAT, )
#else
#define ip_simd_convert_BINARY_FLOAT NULL
#endif

#ifdef simd_cnvrt_BINARY_DOUBLE
generate_simd_convert_s8(BINARY, DOUBLE, )
#else
#define ip_simd_convert_BINARY_DOUBLE NULL
#endif


/*
 * Conversions from binary.  The flag argument is needed so we can't use the
 * macros defined above.
 */

#ifdef simd_cnvrt_UBYTE_BINARY
static void ip_simd_convert_UBYTE_BINARY(ip_image *dest, ip_image *src, int tval)
{
    int length = src->row_size / sizeof(simd_UBYTE);
    for (int j=0; j<dest->size.y; j++) {
	simd_UBYTE *dd = IM_ROWADR(dest, j, v);
	simd_UBYTE *ss = IM_ROWADR(src, j, v);
	for (int i=0; i<length; i++) {
	    *dd++ = simd_cnvrt_UBYTE_BINARY(*ss++, tval);
	}
    }
    SIMD_EXIT();
}
#else
#define ip_simd_convert_UBYTE_BINARY NULL
#endif

#ifdef simd_cnvrt_USHORT_BINARY
static void ip_simd_convert_USHORT_BINARY(ip_image *dest, ip_image *src, int tval)
{
    int length = dest->row_size / sizeof(simd_USHORT);
    int extraone = length & 1;
    length >>= 1;
    for (int j=0; j<dest->size.y; j++) {
	simd_USHORT_2 res;
	simd_USHORT *dd = IM_ROWADR(dest, j, v);
	simd_UBYTE *ss = IM_ROWADR(src, j, v);
	for (int i=0; i<length; i++) {
	    res = simd_cnvrt_USHORT_BINARY(*ss++, tval);
	    dd[0] = res.us[0];
	    dd[1] = res.us[1];
	    dd += 2;
	}
	if (extraone) {
	    res = simd_cnvrt_USHORT_BINARY(*ss, tval);
	    dd[0] = res.us[0];
	}
    }
    SIMD_EXIT();
}
#else
#define ip_simd_convert_USHORT_BINARY NULL
#endif

#ifdef simd_cnvrt_ULONG_BINARY
static void ip_simd_convert_ULONG_BINARY(ip_image *dest, ip_image *src, int tval)
{
    int length = dest->row_size / sizeof(simd_ULONG);
    int extras = length & 3;
    length >>= 2;
    for (int j=0; j<dest->size.y; j++) {
	simd_ULONG_4 res;
	simd_ULONG *dd = IM_ROWADR(dest, j, v);
	simd_UBYTE *ss = IM_ROWADR(src, j, v);
	for (int i=0; i<length; i++) {
	    res = simd_cnvrt_ULONG_BINARY(*ss++, tval);
	    dd[0] = res.ul[0];
	    dd[1] = res.ul[1];
	    dd[2] = res.ul[2];
	    dd[3] = res.ul[3];
	    dd += 4;
	}
	if (extras) {
	    res = simd_cnvrt_ULONG_BINARY(*ss, tval);
	    dd[0] = res.ul[0];
	    if (extras>1) dd[1] = res.ul[1];
	    if (extras>2) dd[2] = res.ul[2];
	}
    }
    SIMD_EXIT();
}
#else
#define ip_simd_convert_ULONG_BINARY NULL
#endif

/*
 * We use simd_cnvrt_FLOAT_LONG() here because on Intel it is significantly
 * faster than simd_cnvrt_FLOAT_ULONG().
 */
#if defined(simd_cnvrt_FLOAT_LONG) && defined(simd_cnvrt_ULONG_BINARY)
static void ip_simd_convert_FLOAT_BINARY(ip_image *dest, ip_image *src, int tval)
{
    int length = dest->row_size / sizeof(simd_FLOAT);
    int extras = length & 3;
    length >>= 2;
    for (int j=0; j<dest->size.y; j++) {
	simd_ULONG_4 res;
	simd_FLOAT *dd = IM_ROWADR(dest, j, v);
	simd_UBYTE *ss = IM_ROWADR(src, j, v);
	for (int i=0; i<length; i++) {
	    res = simd_cnvrt_ULONG_BINARY(*ss++, tval);
	    dd[0] = simd_cnvrt_FLOAT_LONG((simd_LONG)res.ul[0]);
	    dd[1] = simd_cnvrt_FLOAT_LONG((simd_LONG)res.ul[1]);
	    dd[2] = simd_cnvrt_FLOAT_LONG((simd_LONG)res.ul[2]);
	    dd[3] = simd_cnvrt_FLOAT_LONG((simd_LONG)res.ul[3]);
	    dd += 4;
	}
	if (extras) {
	    res = simd_cnvrt_ULONG_BINARY(*ss, tval);
	    dd[0] = simd_cnvrt_FLOAT_LONG((simd_LONG)res.ul[0]);
	    if (extras>1) dd[1] = simd_cnvrt_FLOAT_LONG((simd_LONG)res.ul[1]);
	    if (extras>2) dd[2] = simd_cnvrt_FLOAT_LONG((simd_LONG)res.ul[2]);
	}
    }
    SIMD_EXIT();
}
#else
#define ip_simd_convert_FLOAT_BINARY NULL
#endif

/* Truncation (down casting) of integer types */

#ifdef simd_cnvrt_UBYTE_USHORT
generate_simd_convert_s2(UBYTE, USHORT,)
#else
#define ip_simd_convert_UBYTE_USHORT NULL
#endif

#ifdef simd_cnvrt_USHORT_ULONG
generate_simd_convert_s2(USHORT, ULONG,)
#else
#define ip_simd_convert_USHORT_ULONG NULL
#endif

#ifdef simd_cnvrt_UBYTE_ULONG
generate_simd_convert_s4(UBYTE, ULONG,)
#else
#define ip_simd_convert_UBYTE_ULONG NULL
#endif

/* Sign extension (up casting) of signed integer types */

#ifdef simd_cnvrt_SHORT_BYTE
generate_simd_convert_d2(SHORT, s, BYTE,)
#else
#define ip_simd_convert_SHORT_BYTE NULL
#endif

#ifdef simd_cnvrt_LONG_SHORT
generate_simd_convert_d2(LONG, l, SHORT, )
#else
#define ip_simd_convert_LONG_SHORT NULL
#endif

#ifdef simd_cnvrt_LONG_BYTE
generate_simd_convert_d4(LONG, l, BYTE, )
#else
#define ip_simd_convert_LONG_BYTE NULL
#endif

/* Zero extension (up casting) of unsigned integer types */

#ifdef simd_cnvrt_USHORT_UBYTE
generate_simd_convert_d2(USHORT, us, UBYTE, )
#else
#define ip_simd_convert_USHORT_UBYTE NULL
#endif

#ifdef simd_cnvrt_ULONG_USHORT
generate_simd_convert_d2(ULONG, ul, USHORT, )
#else
#define ip_simd_convert_ULONG_USHORT NULL
#endif

#ifdef simd_cnvrt_ULONG_UBYTE
generate_simd_convert_d4(ULONG, ul, UBYTE, )
#else
#define ip_simd_convert_ULONG_UBYTE NULL
#endif

/* LONG/FLOAT conversions */

#ifdef simd_cnvrt_FLOAT_LONG
generate_simd_convert(FLOAT, LONG, )
#else
#define ip_simd_convert_FLOAT_LONG NULL
#endif

#ifdef simd_cnvrt_LONG_FLOAT
generate_simd_convert(LONG, FLOAT, )
#else
#define ip_simd_convert_LONG_FLOAT NULL
#endif

#ifdef simd_cnvrt_FLOAT_ULONG
generate_simd_convert(FLOAT, ULONG, )
#else
#define ip_simd_convert_FLOAT_ULONG NULL
#endif

#ifdef simd_cnvrt_ULONG_FLOAT
generate_simd_convert(ULONG, FLOAT, )
#else
#define ip_simd_convert_ULONG_FLOAT NULL
#endif

/*
 * Conversions of short integers to FLOAT
 *
 * N.B. We prefer simd_cnvrt_FLOAT_LONG() over simd_cnvrt_FLOAT_ULONG() if
 * possible as an arch is more likely to implement the former than the
 * latter.
 */

#if defined(simd_cnvrt_FLOAT_LONG) && defined(simd_cnvrt_LONG_BYTE)
static inline simd_FLOAT_4 simd_cnvrt_FLOAT_BYTE(simd_BYTE x)
{
    simd_LONG_4 longs;
    simd_FLOAT_4 res;

    longs = simd_cnvrt_LONG_BYTE(x);
    res.f[0] = simd_cnvrt_FLOAT_LONG(longs.l[0]);
    res.f[1] = simd_cnvrt_FLOAT_LONG(longs.l[1]);
    res.f[2] = simd_cnvrt_FLOAT_LONG(longs.l[2]);
    res.f[3] = simd_cnvrt_FLOAT_LONG(longs.l[3]);
    return res;
}
generate_simd_convert_d4(FLOAT, f, BYTE, )
#else
#define ip_simd_convert_FLOAT_BYTE NULL
#endif

#if defined(simd_cnvrt_FLOAT_LONG) && defined (simd_cnvrt_ULONG_UBYTE)
static inline simd_FLOAT_4 simd_cnvrt_FLOAT_UBYTE(simd_UBYTE x)
{
    simd_ULONG_4 longs;
    simd_FLOAT_4 res;

    longs = simd_cnvrt_ULONG_UBYTE(x);
    res.f[0] = simd_cnvrt_FLOAT_LONG((simd_LONG)longs.ul[0]);
    res.f[1] = simd_cnvrt_FLOAT_LONG((simd_LONG)longs.ul[1]);
    res.f[2] = simd_cnvrt_FLOAT_LONG((simd_LONG)longs.ul[2]);
    res.f[3] = simd_cnvrt_FLOAT_LONG((simd_LONG)longs.ul[3]);
    return res;
}
generate_simd_convert_d4(FLOAT, f, UBYTE, )
#else
#define ip_simd_convert_FLOAT_UBYTE NULL
#endif

#if defined(simd_cnvrt_FLOAT_LONG) && defined(simd_cnvrt_LONG_SHORT)
static inline simd_FLOAT_2 simd_cnvrt_FLOAT_SHORT(simd_SHORT x)
{
    simd_LONG_2 longs;
    simd_FLOAT_2 res;

    longs = simd_cnvrt_LONG_SHORT(x);
    res.f[0] = simd_cnvrt_FLOAT_LONG(longs.l[0]);
    res.f[1] = simd_cnvrt_FLOAT_LONG(longs.l[1]);
    return res;
}
generate_simd_convert_d2(FLOAT, f, SHORT, )
#else
#define ip_simd_convert_FLOAT_SHORT NULL
#endif


#if defined(simd_cnvrt_FLOAT_LONG) && defined(simd_cnvrt_ULONG_USHORT)
static inline simd_FLOAT_2 simd_cnvrt_FLOAT_USHORT(simd_USHORT x)
{
    simd_ULONG_2 longs;
    simd_FLOAT_2 res;

    longs = simd_cnvrt_ULONG_USHORT(x);
    res.f[0] = simd_cnvrt_FLOAT_LONG((simd_LONG)longs.ul[0]);
    res.f[1] = simd_cnvrt_FLOAT_LONG((simd_LONG)longs.ul[1]);
    return res;
}
generate_simd_convert_d2(FLOAT, f, USHORT, )
#else
#define ip_simd_convert_FLOAT_USHORT NULL
#endif

#if defined(simd_cnvrt_LONG_FLOAT) && defined(simd_cnvrt_UBYTE_ULONG)
static inline simd_UBYTE simd_cnvrt_UBYTE_FLOAT(simd_FLOAT a, simd_FLOAT b,
						simd_FLOAT c, simd_FLOAT d)
{
    return simd_cnvrt_UBYTE_ULONG((simd_ULONG)simd_cnvrt_LONG_FLOAT(a),
				  (simd_ULONG)simd_cnvrt_LONG_FLOAT(b),
				  (simd_ULONG)simd_cnvrt_LONG_FLOAT(c),
				  (simd_ULONG)simd_cnvrt_LONG_FLOAT(d));
}
generate_simd_convert_s4(UBYTE, FLOAT, )
#else
#define ip_simd_convert_UBYTE_FLOAT NULL
#endif

#if defined(simd_cnvrt_LONG_FLOAT) && defined(simd_cnvrt_USHORT_ULONG)
static inline simd_USHORT simd_cnvrt_USHORT_FLOAT(simd_FLOAT a, simd_FLOAT b)
{
    return simd_cnvrt_USHORT_ULONG((simd_ULONG)simd_cnvrt_LONG_FLOAT(a),
				   (simd_ULONG)simd_cnvrt_LONG_FLOAT(b));
}
generate_simd_convert_s2(USHORT, FLOAT, )
#else
#define ip_simd_convert_USHORT_FLOAT NULL
#endif


/* LONG/DOUBLE conversions */

#ifdef simd_cnvrt_DOUBLE_LONG
generate_simd_convert_d2(DOUBLE, d, LONG, )
#else
#define ip_simd_convert_DOUBLE_LONG NULL
#endif

#ifdef simd_cnvrt_LONG_DOUBLE
generate_simd_convert_s2(LONG, DOUBLE, )
#else
#define ip_simd_convert_LONG_DOUBLE NULL
#endif

#ifdef simd_cnvrt_DOUBLE_ULONG
generate_simd_convert_d2(DOUBLE, d, ULONG, )
#else
#define ip_simd_convert_DOUBLE_ULONG NULL
#endif

#ifdef simd_cnvrt_ULONG_DOUBLE
generate_simd_convert_s2(ULONG, DOUBLE, )
#else
#define ip_simd_convert_ULONG_DOUBLE NULL
#endif

/*
 * Convert shorter ints (BYTE/SHORT) to/from DOUBLE.
 */

#if defined(simd_cnvrt_DOUBLE_LONG) && defined(simd_cnvrt_LONG_BYTE)
static inline simd_DOUBLE_8 simd_cnvrt_DOUBLE_BYTE(simd_BYTE x)
{
    simd_LONG_4 longs;
    simd_DOUBLE_2 doubles;
    simd_DOUBLE_8 res;

    longs = simd_cnvrt_LONG_BYTE(x);
    doubles = simd_cnvrt_DOUBLE_LONG(longs.l[0]);
    res.d[0] = doubles.d[0];
    res.d[1] = doubles.d[1];
    doubles = simd_cnvrt_DOUBLE_LONG(longs.l[1]);
    res.d[2] = doubles.d[0];
    res.d[3] = doubles.d[1];
    doubles = simd_cnvrt_DOUBLE_LONG(longs.l[2]);
    res.d[4] = doubles.d[0];
    res.d[5] = doubles.d[1];
    doubles = simd_cnvrt_DOUBLE_LONG(longs.l[3]);
    res.d[6] = doubles.d[0];
    res.d[7] = doubles.d[1];
    return res;
}
generate_simd_convert_d8(DOUBLE, d, BYTE, )
#else
#define ip_simd_convert_DOUBLE_BYTE NULL
#endif

#if defined(simd_cnvrt_DOUBLE_LONG) && defined(simd_cnvrt_ULONG_UBYTE)
static inline simd_DOUBLE_8 simd_cnvrt_DOUBLE_UBYTE(simd_UBYTE x)
{
    simd_ULONG_4 longs;
    simd_DOUBLE_2 doubles;
    simd_DOUBLE_8 res;

    longs = simd_cnvrt_ULONG_UBYTE(x);
    doubles = simd_cnvrt_DOUBLE_LONG((simd_LONG)longs.ul[0]);
    res.d[0] = doubles.d[0];
    res.d[1] = doubles.d[1];
    doubles = simd_cnvrt_DOUBLE_LONG((simd_LONG)longs.ul[1]);
    res.d[2] = doubles.d[0];
    res.d[3] = doubles.d[1];
    doubles = simd_cnvrt_DOUBLE_LONG((simd_LONG)longs.ul[2]);
    res.d[4] = doubles.d[0];
    res.d[5] = doubles.d[1];
    doubles = simd_cnvrt_DOUBLE_LONG((simd_LONG)longs.ul[3]);
    res.d[6] = doubles.d[0];
    res.d[7] = doubles.d[1];
    return res;
}
generate_simd_convert_d8(DOUBLE, d, UBYTE, )
#else
#define ip_simd_convert_DOUBLE_UBYTE NULL
#endif

#if defined(simd_cnvrt_DOUBLE_LONG) && defined(simd_cnvrt_LONG_SHORT)
static inline simd_DOUBLE_4 simd_cnvrt_DOUBLE_SHORT(simd_SHORT x)
{
    simd_LONG_2 longs;
    simd_DOUBLE_2 doubles;
    simd_DOUBLE_4 res;

    longs = simd_cnvrt_LONG_SHORT(x);
    doubles = simd_cnvrt_DOUBLE_LONG(longs.l[0]);
    res.d[0] = doubles.d[0];
    res.d[1] = doubles.d[1];
    doubles = simd_cnvrt_DOUBLE_LONG(longs.l[1]);
    res.d[2] = doubles.d[0];
    res.d[3] = doubles.d[1];
    return res;
}
generate_simd_convert_d4(DOUBLE, d, SHORT, )
#else
#define ip_simd_convert_DOUBLE_SHORT NULL
#endif

#if defined(simd_cnvrt_DOUBLE_LONG) && defined(simd_cnvrt_ULONG_USHORT)
static inline simd_DOUBLE_4 simd_cnvrt_DOUBLE_USHORT(simd_USHORT x)
{
    simd_ULONG_2 longs;
    simd_DOUBLE_2 doubles;
    simd_DOUBLE_4 res;

    longs = simd_cnvrt_ULONG_USHORT(x);
    doubles = simd_cnvrt_DOUBLE_LONG((simd_LONG)longs.ul[0]);
    res.d[0] = doubles.d[0];
    res.d[1] = doubles.d[1];
    doubles = simd_cnvrt_DOUBLE_LONG((simd_LONG)longs.ul[1]);
    res.d[2] = doubles.d[0];
    res.d[3] = doubles.d[1];
    return res;
}
generate_simd_convert_d4(DOUBLE, d, USHORT, )
#else
#define ip_simd_convert_DOUBLE_USHORT NULL
#endif

#if defined(simd_cnvrt_LONG_DOUBLE) && defined(simd_cnvrt_UBYTE_ULONG)
static inline simd_UBYTE simd_cnvrt_UBYTE_DOUBLE(simd_DOUBLE a, simd_DOUBLE b,
						 simd_DOUBLE c, simd_DOUBLE d,
						 simd_DOUBLE e, simd_DOUBLE f,
						 simd_DOUBLE g, simd_DOUBLE h)
{
    simd_ULONG lu, lv, lx, ly;
    lu = (simd_ULONG)simd_cnvrt_LONG_DOUBLE(a, b);
    lv = (simd_ULONG)simd_cnvrt_LONG_DOUBLE(c, d);
    lx = (simd_ULONG)simd_cnvrt_LONG_DOUBLE(e, f);
    ly = (simd_ULONG)simd_cnvrt_LONG_DOUBLE(g, h);

    return simd_cnvrt_UBYTE_ULONG(lu, lv, lx, ly);
}
generate_simd_convert_s8(UBYTE, DOUBLE, )
#else
#define ip_simd_convert_UBYTE_DOUBLE NULL
#endif

#if defined(simd_cnvrt_LONG_DOUBLE) && defined(simd_cnvrt_USHORT_ULONG)
static inline simd_USHORT simd_cnvrt_USHORT_DOUBLE(simd_DOUBLE a, simd_DOUBLE b,
						   simd_DOUBLE c, simd_DOUBLE d)
{
    simd_ULONG lx, ly;
    lx = (simd_ULONG)simd_cnvrt_LONG_DOUBLE(a, b);
    ly = (simd_ULONG)simd_cnvrt_LONG_DOUBLE(c, d);

    return simd_cnvrt_USHORT_ULONG(lx, ly);
}
generate_simd_convert_s4(USHORT, DOUBLE, )
#else
#define ip_simd_convert_USHORT_DOUBLE NULL
#endif


/* FLOAT/DOUBLE conversions */

#ifdef simd_cnvrt_DOUBLE_FLOAT
generate_simd_convert_d2(DOUBLE, d, FLOAT, )
#else
#define ip_simd_convert_DOUBLE_FLOAT NULL
#endif

#ifdef simd_cnvrt_FLOAT_DOUBLE
generate_simd_convert_s2(FLOAT, DOUBLE, )
#else
#define ip_simd_convert_FLOAT_DOUBLE NULL
#endif


/*
 * Float/Complex conversions
 */

#define generate_convert_to_complex(dt, st, tt)\
    static void ip_simd_convert_##dt##_##st(ip_image *dest, ip_image *src, int flag) \
    {									\
	int length = dest->row_size / sizeof(simd_##dt);		\
	int extraone = length & 1;					\
	length >>= 1;							\
	for (int j=0; j<dest->size.y; j++) {				\
	    const simd_##st vzero = simd_splat_##st(0.0);		\
	    simd_##dt *dd = IM_ROWADR(dest, j, v);			\
	    simd_##st *ss = IM_ROWADR(src, j, v);			\
	    if (flag == FLG_IMAG) {					\
		simd_##st##_2 xx;					\
		for (int i=0; i<length; i++) {				\
		    xx = simd_interleave_##st(vzero, *ss++);		\
		    *dd++ = (simd_##dt)xx.tt[0];			\
		    *dd++ = (simd_##dt)xx.tt[1];			\
		}							\
		if (extraone) {						\
		    xx = simd_interleave_##st(vzero, *ss);		\
		    *dd = (simd_##dt)xx.tt[0];				\
		}							\
	    }else{							\
		simd_##st##_2 xx;					\
		for (int i=0; i<length; i++) {				\
		    xx = simd_interleave_##st(*ss++, vzero);		\
		    *dd++ = (simd_##dt)xx.tt[0];			\
		    *dd++ = (simd_##dt)xx.tt[1];			\
		}							\
		if (extraone) {						\
		    xx = simd_interleave_##st(*ss, vzero);		\
		    *dd = (simd_##dt)xx.tt[0];				\
		}							\
	    }								\
	}								\
    }

#if defined(simd_splat_COMPLEX) && defined(simd_interleave_FLOAT)
generate_convert_to_complex(COMPLEX, FLOAT, f)
#else
#define ip_simd_convert_COMPLEX_FLOAT NULL
#endif

#if defined(simd_splat_DCOMPLEX) && defined(simd_interleave_DOUBLE)
generate_convert_to_complex(DCOMPLEX, DOUBLE, d)
#else
#define ip_simd_convert_DCOMPLEX_DOUBLE NULL
#endif

/*
 * Colour conversions
 */

#define simd_splat_RGB simd_splat_UBYTE
#define simd_splat_RGBA simd_splat_UBYTE
#define simd_splat_RGB16 simd_splat_USHORT

#ifdef simd_cnvrt_RGBA_RGB
generate_simd_convert_s3d4(RGBA, ub, RGB, )
#else
#define ip_simd_convert_RGBA_RGB NULL
#endif

#ifdef simd_cnvrt_RGB_RGBA
generate_simd_convert_s4d3(RGB, ub, RGBA, )
#else
#define ip_simd_convert_RGB_RGBA NULL
#endif

#ifdef simd_cnvrt_RGB_RGB16
generate_simd_convert_s2(RGB, RGB16, )
#else
#define ip_simd_convert_RGB_RGB16 NULL
#endif

#ifdef simd_cnvrt_RGB16_RGB
generate_simd_convert_d2(RGB16, us, RGB, )
#else
#define ip_simd_convert_RGB16_RGB NULL
#endif

static improc_function_IIf convert_to_BINARY[IM_TYPEMAX] = {
    [IM_BYTE] = ip_simd_convert_BINARY_UBYTE,
    [IM_UBYTE] = ip_simd_convert_BINARY_UBYTE,
    [IM_SHORT] = ip_simd_convert_BINARY_USHORT,
    [IM_USHORT] = ip_simd_convert_BINARY_USHORT,
    [IM_LONG] = ip_simd_convert_BINARY_ULONG,
    [IM_ULONG] = ip_simd_convert_BINARY_ULONG,
    [IM_FLOAT] = ip_simd_convert_BINARY_FLOAT,
    [IM_DOUBLE] = ip_simd_convert_BINARY_DOUBLE,
};

static improc_function_IIf convert_to_BYTE[IM_TYPEMAX] = {
    [IM_BINARY] = ip_simd_convert_UBYTE_BINARY,
    [IM_UBYTE] = ip_simd_copy_image_data,
    [IM_SHORT] = ip_simd_convert_UBYTE_USHORT,
    [IM_USHORT] = ip_simd_convert_UBYTE_USHORT,
    [IM_LONG] = ip_simd_convert_UBYTE_ULONG,
    [IM_ULONG] = ip_simd_convert_UBYTE_ULONG,
    [IM_FLOAT] = ip_simd_convert_UBYTE_FLOAT,
    [IM_DOUBLE] = ip_simd_convert_UBYTE_DOUBLE
};

static improc_function_IIf convert_to_UBYTE[IM_TYPEMAX] = {
    [IM_BINARY] = ip_simd_convert_UBYTE_BINARY,
    [IM_BYTE] = ip_simd_copy_image_data,
    [IM_SHORT] = ip_simd_convert_UBYTE_USHORT,
    [IM_USHORT] = ip_simd_convert_UBYTE_USHORT,
    [IM_LONG] = ip_simd_convert_UBYTE_ULONG,
    [IM_ULONG] = ip_simd_convert_UBYTE_ULONG,
    [IM_FLOAT] = ip_simd_convert_UBYTE_FLOAT,
    [IM_DOUBLE] = ip_simd_convert_UBYTE_DOUBLE
};

static improc_function_IIf convert_to_SHORT[IM_TYPEMAX] = {
    [IM_BINARY] = ip_simd_convert_USHORT_BINARY,
    [IM_BYTE] = ip_simd_convert_SHORT_BYTE,
    [IM_UBYTE] = ip_simd_convert_USHORT_UBYTE,
    [IM_USHORT] = ip_simd_copy_image_data,
    [IM_LONG] = ip_simd_convert_USHORT_ULONG,
    [IM_ULONG] = ip_simd_convert_USHORT_ULONG,
    [IM_FLOAT] = ip_simd_convert_USHORT_FLOAT,
    [IM_DOUBLE] = ip_simd_convert_USHORT_DOUBLE
};

static improc_function_IIf convert_to_USHORT[IM_TYPEMAX] = {
    [IM_BINARY] = ip_simd_convert_USHORT_BINARY,
    [IM_BYTE] = ip_simd_convert_SHORT_BYTE,
    [IM_UBYTE] = ip_simd_convert_USHORT_UBYTE,
    [IM_SHORT] = ip_simd_copy_image_data,
    [IM_LONG] = ip_simd_convert_USHORT_ULONG,
    [IM_ULONG] = ip_simd_convert_USHORT_ULONG,
    [IM_FLOAT] = ip_simd_convert_USHORT_FLOAT,
    [IM_DOUBLE] = ip_simd_convert_USHORT_DOUBLE
};

static improc_function_IIf convert_to_LONG[IM_TYPEMAX] = {
    [IM_BINARY] = ip_simd_convert_ULONG_BINARY,
    [IM_BYTE] = ip_simd_convert_LONG_BYTE,
    [IM_UBYTE] = ip_simd_convert_ULONG_UBYTE,
    [IM_SHORT] = ip_simd_convert_LONG_SHORT,
    [IM_USHORT] = ip_simd_convert_ULONG_USHORT,
    [IM_ULONG] = ip_simd_copy_image_data,
    [IM_FLOAT] = ip_simd_convert_LONG_FLOAT,
    [IM_DOUBLE] = ip_simd_convert_LONG_DOUBLE,
};

static improc_function_IIf convert_to_ULONG[IM_TYPEMAX] = {
    [IM_BINARY] = ip_simd_convert_ULONG_BINARY,
    [IM_BYTE] = ip_simd_convert_LONG_BYTE,
    [IM_UBYTE] = ip_simd_convert_ULONG_UBYTE,
    [IM_SHORT] = ip_simd_convert_LONG_SHORT,
    [IM_USHORT] = ip_simd_convert_ULONG_USHORT,
    [IM_LONG] = ip_simd_copy_image_data,
    [IM_FLOAT] = ip_simd_convert_ULONG_FLOAT,
    [IM_DOUBLE] = ip_simd_convert_ULONG_DOUBLE,
};

static improc_function_IIf convert_to_FLOAT[IM_TYPEMAX] = {
    [IM_BINARY] = ip_simd_convert_FLOAT_BINARY,
    [IM_BYTE] = ip_simd_convert_FLOAT_BYTE,
    [IM_UBYTE] = ip_simd_convert_FLOAT_UBYTE,
    [IM_SHORT] = ip_simd_convert_FLOAT_SHORT,
    [IM_USHORT] = ip_simd_convert_FLOAT_USHORT,
    [IM_LONG] = ip_simd_convert_FLOAT_LONG,
    [IM_ULONG] = ip_simd_convert_FLOAT_ULONG,
    [IM_DOUBLE] = ip_simd_convert_FLOAT_DOUBLE
};

static improc_function_IIf convert_to_DOUBLE[IM_TYPEMAX] = {
    [IM_BYTE] = ip_simd_convert_DOUBLE_BYTE,
    [IM_UBYTE] = ip_simd_convert_DOUBLE_UBYTE,
    [IM_SHORT] = ip_simd_convert_DOUBLE_SHORT,
    [IM_USHORT] = ip_simd_convert_DOUBLE_USHORT,
    [IM_LONG] = ip_simd_convert_DOUBLE_LONG,
    [IM_ULONG] = ip_simd_convert_DOUBLE_ULONG,
    [IM_FLOAT] = ip_simd_convert_DOUBLE_FLOAT
};

static improc_function_IIf convert_to_COMPLEX[IM_TYPEMAX] = {
    [IM_FLOAT] = ip_simd_convert_COMPLEX_FLOAT,
    [IM_DCOMPLEX] = ip_simd_convert_FLOAT_DOUBLE
};

static improc_function_IIf convert_to_DCOMPLEX[IM_TYPEMAX] = {
    [IM_DOUBLE] = ip_simd_convert_DCOMPLEX_DOUBLE,
    [IM_COMPLEX] = ip_simd_convert_DOUBLE_FLOAT
};

static improc_function_IIf convert_to_RGB[IM_TYPEMAX] = {
    [IM_RGBA] = ip_simd_convert_RGB_RGBA,
    [IM_RGB16] = ip_simd_convert_RGB_RGB16
};

static improc_function_IIf convert_to_RGBA[IM_TYPEMAX] = {
    [IM_RGB] = ip_simd_convert_RGBA_RGB
};

static improc_function_IIf convert_to_RGB16[IM_TYPEMAX] = {
    [IM_RGB] = ip_simd_convert_RGB16_RGB
};

improc_function_IIf *ip_simd_convert[IM_TYPEMAX] = {
    [IM_BINARY] = convert_to_BINARY,
    [IM_BYTE] = convert_to_BYTE,
    [IM_UBYTE] = convert_to_UBYTE,
    [IM_SHORT] = convert_to_SHORT,
    [IM_USHORT] = convert_to_USHORT,
    [IM_LONG] = convert_to_LONG,
    [IM_ULONG] = convert_to_ULONG,
    [IM_FLOAT] = convert_to_FLOAT,
    [IM_DOUBLE] = convert_to_DOUBLE,
    [IM_COMPLEX] = convert_to_COMPLEX,
    [IM_DCOMPLEX] = convert_to_DCOMPLEX,
    [IM_RGB] = convert_to_RGB,
    [IM_RGB16] = convert_to_RGB16,
    [IM_RGBA] = convert_to_RGBA,
};



/*
 * Conversion with clipping; relevant for down-casting or casting signed
 * to/from unsigned in other cases.
 */

/* Conversion of (u)byte to (u)byte with clipping */

#ifdef simd_cnvrt_clip_UBYTE_BYTE
generate_simd_convert(UBYTE, BYTE, clip_)
#else
#define ip_simd_convert_clip_UBYTE_BYTE NULL
#endif

#ifdef simd_cnvrt_clip_BYTE_UBYTE
generate_simd_convert(BYTE, UBYTE, clip_)
#else
#define ip_simd_convert_clip_BYTE_UBYTE NULL
#endif

/* Conversion of (u)short to (u)byte with clipping */

#ifdef simd_cnvrt_clip_UBYTE_USHORT
generate_simd_convert_s2(UBYTE, USHORT, clip_)
#else
#define ip_simd_convert_clip_UBYTE_USHORT NULL
#endif

#ifdef simd_cnvrt_clip_UBYTE_SHORT
generate_simd_convert_s2(UBYTE, SHORT, clip_)
#else
#define ip_simd_convert_clip_UBYTE_SHORT NULL
#endif

#ifdef simd_cnvrt_clip_BYTE_USHORT
generate_simd_convert_s2(BYTE, USHORT, clip_)
#else
#define ip_simd_convert_clip_BYTE_USHORT NULL
#endif

#ifdef simd_cnvrt_clip_BYTE_SHORT
generate_simd_convert_s2(BYTE, SHORT, clip_)
#else
#define ip_simd_convert_clip_BYTE_SHORT NULL
#endif

/* Conversion of (u)long to (u)byte with clipping */

#ifdef simd_cnvrt_clip_UBYTE_ULONG
generate_simd_convert_s4(UBYTE, ULONG, clip_)
#else
#define ip_simd_convert_clip_UBYTE_ULONG NULL
#endif

#ifdef simd_cnvrt_clip_UBYTE_LONG
generate_simd_convert_s4(UBYTE, LONG, clip_)
#else
#define ip_simd_convert_clip_UBYTE_LONG NULL
#endif

#ifdef simd_cnvrt_clip_BYTE_ULONG
generate_simd_convert_s4(BYTE, ULONG, clip_)
#else
#define ip_simd_convert_clip_BYTE_ULONG NULL
#endif

#ifdef simd_cnvrt_clip_BYTE_LONG
generate_simd_convert_s4(BYTE, LONG, clip_)
#else
#define ip_simd_convert_clip_BYTE_LONG NULL
#endif

/* Conversion of (signed) byte to (unsigned) ushort with clipping */

#ifdef simd_cnvrt_clip_USHORT_BYTE
generate_simd_convert_d2(USHORT, us, BYTE, clip_)
#else
#define ip_simd_convert_clip_USHORT_BYTE NULL
#endif

/* Conversion of short to ushort with clipping or vice versa */

#ifdef simd_cnvrt_clip_USHORT_SHORT
generate_simd_convert(USHORT, SHORT, clip_)
#else
#define ip_simd_convert_clip_USHORT_SHORT NULL
#endif

#ifdef simd_cnvrt_clip_SHORT_USHORT
generate_simd_convert(SHORT, USHORT, clip_)
#else
#define ip_simd_convert_clip_SHORT_USHORT NULL
#endif

/* Conversion of (u)long to (u)short with clipping */

#ifdef simd_cnvrt_clip_USHORT_ULONG
generate_simd_convert_s2(USHORT, ULONG, clip_)
#else
#define ip_simd_convert_clip_USHORT_ULONG NULL
#endif

#ifdef simd_cnvrt_clip_USHORT_LONG
generate_simd_convert_s2(USHORT, LONG, clip_)
#else
#define ip_simd_convert_clip_USHORT_LONG NULL
#endif

#ifdef simd_cnvrt_clip_SHORT_ULONG
generate_simd_convert_s2(SHORT, ULONG, clip_)
#else
#define ip_simd_convert_clip_SHORT_ULONG NULL
#endif

#ifdef simd_cnvrt_clip_SHORT_LONG
generate_simd_convert_s2(SHORT, LONG, clip_)
#else
#define ip_simd_convert_clip_SHORT_LONG NULL
#endif

/* Conversion of (signed) byte to (unsigned) ulong with clipping */

#ifdef simd_cnvrt_clip_ULONG_BYTE
generate_simd_convert_d4(ULONG, ul, BYTE, clip_)
#else
#define ip_simd_convert_clip_ULONG_BYTE NULL
#endif

/* Conversion of (signed) short to (unsigned) ulong with clipping */

#ifdef simd_cnvrt_clip_ULONG_SHORT
generate_simd_convert_d2(ULONG, ul, SHORT, clip_)
#else
#define ip_simd_convert_clip_ULONG_SHORT NULL
#endif

/* Conversion of long to ulong with clipping or vice versa */

#ifdef simd_cnvrt_clip_ULONG_LONG
generate_simd_convert(ULONG, LONG, clip_)
#else
#define ip_simd_convert_clip_ULONG_LONG NULL
#endif

#ifdef simd_cnvrt_clip_LONG_ULONG
generate_simd_convert(LONG, ULONG, clip_)
#else
#define ip_simd_convert_clip_LONG_ULONG NULL
#endif

/* FLOAT/DOUBLE to long conversions with clipping */

#ifdef simd_cnvrt_clip_LONG_FLOAT
generate_simd_convert(LONG, FLOAT, clip_)
#else
#define ip_simd_convert_clip_LONG_FLOAT NULL
#endif

#ifdef simd_cnvrt_clip_ULONG_FLOAT
generate_simd_convert(ULONG, FLOAT, clip_)
#else
#define ip_simd_convert_clip_ULONG_FLOAT NULL
#endif

#ifdef simd_cnvrt_clip_LONG_DOUBLE
generate_simd_convert_s2(LONG, DOUBLE, clip_)
#else
#define ip_simd_convert_clip_LONG_DOUBLE NULL
#endif

#ifdef simd_cnvrt_clip_ULONG_DOUBLE
generate_simd_convert_s2(ULONG, DOUBLE, clip_)
#else
#define ip_simd_convert_clip_ULONG_DOUBLE NULL
#endif

/*
 * FLOAT to shorter ints with clipping.  Some of these are not as efficient
 * as they could be as there is double clipping: first the FLOAT is clipped
 * to LONG then the LONG is clipped to the shorter integer.
 */

#if defined(simd_cnvrt_clip_LONG_FLOAT) && defined(simd_cnvrt_clip_UBYTE_LONG)
static inline simd_UBYTE simd_cnvrt_clip_UBYTE_FLOAT(simd_FLOAT a, simd_FLOAT b,
						     simd_FLOAT c, simd_FLOAT d)
{
    return simd_cnvrt_clip_UBYTE_LONG(simd_cnvrt_clip_LONG_FLOAT(a),
				      simd_cnvrt_clip_LONG_FLOAT(b),
				      simd_cnvrt_clip_LONG_FLOAT(c),
				      simd_cnvrt_clip_LONG_FLOAT(d));
}
generate_simd_convert_s4(UBYTE, FLOAT, clip_)
#else
#define ip_simd_convert_clip_UBYTE_FLOAT NULL
#endif


#if defined(simd_cnvrt_clip_LONG_FLOAT) && defined(simd_cnvrt_clip_BYTE_LONG)
static inline simd_BYTE simd_cnvrt_clip_BYTE_FLOAT(simd_FLOAT a, simd_FLOAT b,
						   simd_FLOAT c, simd_FLOAT d)
{
    return simd_cnvrt_clip_BYTE_LONG(simd_cnvrt_clip_LONG_FLOAT(a),
				     simd_cnvrt_clip_LONG_FLOAT(b),
				     simd_cnvrt_clip_LONG_FLOAT(c),
				     simd_cnvrt_clip_LONG_FLOAT(d));
}
generate_simd_convert_s4(BYTE, FLOAT, clip_)
#else
#define ip_simd_convert_clip_BYTE_FLOAT NULL
#endif


#if defined(simd_cnvrt_clip_LONG_FLOAT) && defined(simd_cnvrt_clip_USHORT_LONG)
static inline simd_USHORT simd_cnvrt_clip_USHORT_FLOAT(simd_FLOAT a, simd_FLOAT b)
{
    return simd_cnvrt_clip_USHORT_LONG(simd_cnvrt_clip_LONG_FLOAT(a),
				       simd_cnvrt_clip_LONG_FLOAT(b));
}
generate_simd_convert_s2(USHORT, FLOAT, clip_)
#else
#define ip_simd_convert_clip_USHORT_FLOAT NULL
#endif


#if defined(simd_cnvrt_clip_LONG_FLOAT) && defined(simd_cnvrt_clip_SHORT_LONG)
static inline simd_SHORT simd_cnvrt_clip_SHORT_FLOAT(simd_FLOAT a, simd_FLOAT b)
{
    return simd_cnvrt_clip_SHORT_LONG(simd_cnvrt_clip_LONG_FLOAT(a),
				      simd_cnvrt_clip_LONG_FLOAT(b));
}
generate_simd_convert_s2(SHORT, FLOAT, clip_)
#else
#define ip_simd_convert_clip_SHORT_FLOAT NULL
#endif

/* DOUBLE to shorter ints with clipping */

#if defined(simd_cnvrt_clip_LONG_DOUBLE) && defined(simd_cnvrt_clip_UBYTE_LONG)
static inline simd_UBYTE simd_cnvrt_clip_UBYTE_DOUBLE(simd_DOUBLE a, simd_DOUBLE b,
						      simd_DOUBLE c, simd_DOUBLE d,
						      simd_DOUBLE e, simd_DOUBLE f,
						      simd_DOUBLE g, simd_DOUBLE h)
{
    return simd_cnvrt_clip_UBYTE_LONG(simd_cnvrt_clip_LONG_DOUBLE(a, b),
				      simd_cnvrt_clip_LONG_DOUBLE(c, d),
				      simd_cnvrt_clip_LONG_DOUBLE(e, f),
				      simd_cnvrt_clip_LONG_DOUBLE(g, h));
}
generate_simd_convert_s8(UBYTE, DOUBLE, clip_)
#else
#define ip_simd_convert_clip_UBYTE_DOUBLE NULL
#endif


#if defined(simd_cnvrt_clip_LONG_DOUBLE) && defined(simd_cnvrt_clip_BYTE_LONG)
static inline simd_BYTE simd_cnvrt_clip_BYTE_DOUBLE(simd_DOUBLE a, simd_DOUBLE b,
						      simd_DOUBLE c, simd_DOUBLE d,
						      simd_DOUBLE e, simd_DOUBLE f,
						      simd_DOUBLE g, simd_DOUBLE h)
{
    return simd_cnvrt_clip_BYTE_LONG(simd_cnvrt_clip_LONG_DOUBLE(a, b),
				      simd_cnvrt_clip_LONG_DOUBLE(c, d),
				      simd_cnvrt_clip_LONG_DOUBLE(e, f),
				      simd_cnvrt_clip_LONG_DOUBLE(g, h));
}
generate_simd_convert_s8(BYTE, DOUBLE, clip_)
#else
#define ip_simd_convert_clip_BYTE_DOUBLE NULL
#endif


#if defined(simd_cnvrt_clip_LONG_DOUBLE) && defined(simd_cnvrt_clip_USHORT_LONG)
static inline simd_USHORT simd_cnvrt_clip_USHORT_DOUBLE(simd_DOUBLE a, simd_DOUBLE b,
							simd_DOUBLE c, simd_DOUBLE d)
{
    return simd_cnvrt_clip_USHORT_LONG(simd_cnvrt_clip_LONG_DOUBLE(a, b),
				       simd_cnvrt_clip_LONG_DOUBLE(c, d));
}
generate_simd_convert_s4(USHORT, DOUBLE, clip_)
#else
#define ip_simd_convert_clip_USHORT_DOUBLE NULL
#endif


#if defined(simd_cnvrt_clip_LONG_DOUBLE) && defined(simd_cnvrt_clip_SHORT_LONG)
static inline simd_SHORT simd_cnvrt_clip_SHORT_DOUBLE(simd_DOUBLE a, simd_DOUBLE b,
						      simd_DOUBLE c, simd_DOUBLE d)
{
    return simd_cnvrt_clip_SHORT_LONG(simd_cnvrt_clip_LONG_DOUBLE(a, b),
				       simd_cnvrt_clip_LONG_DOUBLE(c, d));
}
generate_simd_convert_s4(SHORT, DOUBLE, clip_)
#else
#define ip_simd_convert_clip_SHORT_DOUBLE NULL
#endif


/* DOUBLE to FLOAT conversion with clipping */

#ifdef simd_cnvrt_clip_FLOAT_DOUBLE
generate_simd_convert_s2(FLOAT, DOUBLE, clip_)
#else
#define ip_simd_convert_clip_FLOAT_DOUBLE NULL
#endif


static improc_function_IIf convert_clip_to_BYTE[IM_TYPEMAX] = {
    [IM_UBYTE] = ip_simd_convert_clip_BYTE_UBYTE,
    [IM_SHORT] = ip_simd_convert_clip_BYTE_SHORT,
    [IM_USHORT] = ip_simd_convert_clip_BYTE_USHORT,
    [IM_LONG] = ip_simd_convert_clip_BYTE_LONG,
    [IM_ULONG] = ip_simd_convert_clip_BYTE_ULONG,
    [IM_FLOAT] = ip_simd_convert_clip_BYTE_FLOAT,
    [IM_DOUBLE] = ip_simd_convert_clip_BYTE_DOUBLE,
};

static improc_function_IIf convert_clip_to_UBYTE[IM_TYPEMAX] = {
    [IM_BYTE] = ip_simd_convert_clip_UBYTE_BYTE,
    [IM_SHORT] = ip_simd_convert_clip_UBYTE_SHORT,
    [IM_USHORT] = ip_simd_convert_clip_UBYTE_USHORT,
    [IM_LONG] = ip_simd_convert_clip_UBYTE_LONG,
    [IM_ULONG] = ip_simd_convert_clip_UBYTE_ULONG,
    [IM_FLOAT] = ip_simd_convert_clip_UBYTE_FLOAT,
    [IM_DOUBLE] = ip_simd_convert_clip_UBYTE_DOUBLE,
};

static improc_function_IIf convert_clip_to_SHORT[IM_TYPEMAX] = {
    [IM_BYTE] = ip_simd_convert_SHORT_BYTE,
    [IM_UBYTE] = ip_simd_convert_USHORT_UBYTE,
    [IM_USHORT] = ip_simd_convert_clip_SHORT_USHORT,
    [IM_LONG] = ip_simd_convert_clip_SHORT_LONG,
    [IM_ULONG] = ip_simd_convert_clip_SHORT_ULONG,
    [IM_FLOAT] = ip_simd_convert_clip_SHORT_FLOAT,
    [IM_DOUBLE] = ip_simd_convert_clip_SHORT_DOUBLE,
};

static improc_function_IIf convert_clip_to_USHORT[IM_TYPEMAX] = {
    [IM_BYTE] = ip_simd_convert_clip_USHORT_BYTE,
    [IM_UBYTE] = ip_simd_convert_USHORT_UBYTE,
    [IM_SHORT] = ip_simd_convert_clip_USHORT_SHORT,
    [IM_LONG] = ip_simd_convert_clip_USHORT_LONG,
    [IM_ULONG] = ip_simd_convert_clip_USHORT_ULONG,
    [IM_FLOAT] = ip_simd_convert_clip_USHORT_FLOAT,
    [IM_DOUBLE] = ip_simd_convert_clip_USHORT_DOUBLE,
};

static improc_function_IIf convert_clip_to_LONG[IM_TYPEMAX] = {
    [IM_BYTE] = ip_simd_convert_LONG_BYTE,
    [IM_UBYTE] = ip_simd_convert_ULONG_UBYTE,
    [IM_SHORT] = ip_simd_convert_LONG_SHORT,
    [IM_USHORT] = ip_simd_convert_ULONG_USHORT,
    [IM_ULONG] = ip_simd_convert_clip_LONG_ULONG,
    [IM_FLOAT] = ip_simd_convert_clip_LONG_FLOAT,
    [IM_DOUBLE] = ip_simd_convert_clip_LONG_DOUBLE,
};

static improc_function_IIf convert_clip_to_ULONG[IM_TYPEMAX] = {
    [IM_BYTE] = ip_simd_convert_clip_ULONG_BYTE,
    [IM_UBYTE] = ip_simd_convert_ULONG_UBYTE,
    [IM_SHORT] = ip_simd_convert_clip_ULONG_SHORT,
    [IM_USHORT] = ip_simd_convert_ULONG_USHORT,
    [IM_LONG] = ip_simd_convert_clip_ULONG_LONG,
    [IM_FLOAT] = ip_simd_convert_clip_ULONG_FLOAT,
    [IM_DOUBLE] = ip_simd_convert_clip_ULONG_DOUBLE,
};

static improc_function_IIf convert_clip_to_FLOAT[IM_TYPEMAX] = {
    [IM_BYTE] = ip_simd_convert_FLOAT_BYTE,
    [IM_UBYTE] = ip_simd_convert_FLOAT_UBYTE,
    [IM_SHORT] = ip_simd_convert_FLOAT_SHORT,
    [IM_USHORT] = ip_simd_convert_FLOAT_USHORT,
    [IM_LONG] = ip_simd_convert_FLOAT_LONG,
    [IM_ULONG] = ip_simd_convert_FLOAT_ULONG,
    [IM_DOUBLE] = ip_simd_convert_clip_FLOAT_DOUBLE,
};

improc_function_IIf *ip_simd_convert_clipped[IM_TYPEMAX] = {
    [IM_BYTE] = convert_clip_to_BYTE,
    [IM_UBYTE] = convert_clip_to_UBYTE,
    [IM_SHORT] = convert_clip_to_SHORT,
    [IM_USHORT] = convert_clip_to_USHORT,
    [IM_LONG] = convert_clip_to_LONG,
    [IM_ULONG] = convert_clip_to_ULONG,
    [IM_FLOAT] = convert_clip_to_FLOAT,
    [IM_DOUBLE] = convert_to_DOUBLE
};


/*
 * Conversion from FLOAT to integer with conventional rounding.
 */

#if defined(simd_cnvrt_rnd_LONG_FLOAT) && defined(simd_cnvrt_UBYTE_ULONG)
static inline simd_UBYTE simd_cnvrt_rnd_UBYTE_FLOAT(simd_FLOAT a, simd_FLOAT b,
						    simd_FLOAT c, simd_FLOAT d)
{
    return simd_cnvrt_UBYTE_ULONG((simd_ULONG)simd_cnvrt_rnd_LONG_FLOAT(a),
				  (simd_ULONG)simd_cnvrt_rnd_LONG_FLOAT(b),
				  (simd_ULONG)simd_cnvrt_rnd_LONG_FLOAT(c),
				  (simd_ULONG)simd_cnvrt_rnd_LONG_FLOAT(d));
}
generate_simd_convert_s4(UBYTE, FLOAT, rnd_)
#else
#define ip_simd_convert_rnd_UBYTE_FLOAT NULL
#endif


#if defined(simd_cnvrt_rnd_LONG_FLOAT) && defined(simd_cnvrt_USHORT_ULONG)
static inline simd_USHORT simd_cnvrt_rnd_USHORT_FLOAT(simd_FLOAT a, simd_FLOAT b)
{
    return simd_cnvrt_USHORT_ULONG((simd_ULONG)simd_cnvrt_rnd_LONG_FLOAT(a),
				   (simd_ULONG)simd_cnvrt_rnd_LONG_FLOAT(b));
}
generate_simd_convert_s2(USHORT, FLOAT, rnd_)
#else
#define ip_simd_convert_rnd_USHORT_FLOAT NULL
#endif


#ifdef simd_cnvrt_rnd_LONG_FLOAT
generate_simd_convert(LONG, FLOAT, rnd_)
#else
#define ip_simd_convert_rnd_LONG_FLOAT NULL
#endif


#ifdef simd_cnvrt_rnd_ULONG_FLOAT
generate_simd_convert(ULONG, FLOAT, rnd_)
#else
#define ip_simd_convert_rnd_ULONG_FLOAT NULL
#endif

/*
 * Conversion from DOUBLE to integer with conventional rounding
 */

#if defined(simd_cnvrt_rnd_LONG_DOUBLE) && defined(simd_cnvrt_UBYTE_ULONG)
static inline simd_UBYTE simd_cnvrt_rnd_UBYTE_DOUBLE(simd_DOUBLE a, simd_DOUBLE b,
						     simd_DOUBLE c, simd_DOUBLE d,
						     simd_DOUBLE e, simd_DOUBLE f,
						     simd_DOUBLE g, simd_DOUBLE h)
{
    simd_ULONG lu, lv, lx, ly;
    lu = (simd_ULONG)simd_cnvrt_rnd_LONG_DOUBLE(a, b);
    lv = (simd_ULONG)simd_cnvrt_rnd_LONG_DOUBLE(c, d);
    lx = (simd_ULONG)simd_cnvrt_rnd_LONG_DOUBLE(e, f);
    ly = (simd_ULONG)simd_cnvrt_rnd_LONG_DOUBLE(g, h);

    return simd_cnvrt_UBYTE_ULONG(lu, lv, lx, ly);
}
generate_simd_convert_s8(UBYTE, DOUBLE, rnd_)
#else
#define ip_simd_convert_rnd_UBYTE_DOUBLE NULL
#endif


#if defined(simd_cnvrt_rnd_LONG_DOUBLE) && defined(simd_cnvrt_USHORT_ULONG)
static inline simd_USHORT simd_cnvrt_rnd_USHORT_DOUBLE(simd_DOUBLE a, simd_DOUBLE b,
						       simd_DOUBLE c, simd_DOUBLE d)
{
    simd_ULONG lx, ly;
    lx = (simd_ULONG)simd_cnvrt_rnd_LONG_DOUBLE(a, b);
    ly = (simd_ULONG)simd_cnvrt_rnd_LONG_DOUBLE(c, d);

    return simd_cnvrt_USHORT_ULONG(lx, ly);
}
generate_simd_convert_s4(USHORT, DOUBLE, rnd_)
#else
#define ip_simd_convert_rnd_USHORT_DOUBLE NULL
#endif


#ifdef simd_cnvrt_rnd_LONG_DOUBLE
generate_simd_convert_s2(LONG, DOUBLE, rnd_)
#else
#define ip_simd_convert_rnd_LONG_DOUBLE NULL
#endif


#ifdef simd_cnvrt_rnd_ULONG_DOUBLE
generate_simd_convert_s2(ULONG, DOUBLE, rnd_)
#else
#define ip_simd_convert_rnd_ULONG_DOUBLE NULL
#endif


/*
 * And the function lookup tables for rounding conversions.
 */

static improc_function_IIf convert_round_to_BYTE[IM_TYPEMAX] = {
    [IM_FLOAT] = ip_simd_convert_rnd_UBYTE_FLOAT,
    [IM_DOUBLE] = ip_simd_convert_rnd_UBYTE_DOUBLE,
};

static improc_function_IIf convert_round_to_UBYTE[IM_TYPEMAX] = {
    [IM_FLOAT] = ip_simd_convert_rnd_UBYTE_FLOAT,
    [IM_DOUBLE] = ip_simd_convert_rnd_UBYTE_DOUBLE,
};

static improc_function_IIf convert_round_to_SHORT[IM_TYPEMAX] = {
    [IM_FLOAT] = ip_simd_convert_rnd_USHORT_FLOAT,
    [IM_DOUBLE] = ip_simd_convert_rnd_USHORT_DOUBLE,
};

static improc_function_IIf convert_round_to_USHORT[IM_TYPEMAX] = {
    [IM_FLOAT] = ip_simd_convert_rnd_USHORT_FLOAT,
    [IM_DOUBLE] = ip_simd_convert_rnd_USHORT_DOUBLE,
};

static improc_function_IIf convert_round_to_LONG[IM_TYPEMAX] = {
    [IM_FLOAT] = ip_simd_convert_rnd_LONG_FLOAT,
    [IM_DOUBLE] = ip_simd_convert_rnd_LONG_DOUBLE,
};

static improc_function_IIf convert_round_to_ULONG[IM_TYPEMAX] = {
    [IM_FLOAT] = ip_simd_convert_rnd_ULONG_FLOAT,
    [IM_DOUBLE] = ip_simd_convert_rnd_ULONG_DOUBLE,
};


improc_function_IIf *ip_simd_convert_round[IM_TYPEMAX] = {
    [IM_BYTE] = convert_round_to_BYTE,
    [IM_UBYTE] = convert_round_to_UBYTE,
    [IM_SHORT] = convert_round_to_SHORT,
    [IM_USHORT] = convert_round_to_USHORT,
    [IM_LONG] = convert_round_to_LONG,
    [IM_ULONG] = convert_round_to_ULONG
};


/*
 * Rounding of float/double to integer with clipping (saturation).
 */

/*
 * Conversion from FLOAT to integer with rounding and clipping.
 */

#if defined(simd_cnvrt_rnd_clip_LONG_FLOAT) && defined(simd_cnvrt_clip_UBYTE_LONG)
static inline simd_UBYTE simd_cnvrt_rnd_clip_UBYTE_FLOAT(simd_FLOAT a, simd_FLOAT b,
							 simd_FLOAT c, simd_FLOAT d)
{
    return simd_cnvrt_clip_UBYTE_LONG(simd_cnvrt_rnd_clip_LONG_FLOAT(a),
				      simd_cnvrt_rnd_clip_LONG_FLOAT(b),
				      simd_cnvrt_rnd_clip_LONG_FLOAT(c),
				      simd_cnvrt_rnd_clip_LONG_FLOAT(d));
}
generate_simd_convert_s4(UBYTE, FLOAT, rnd_clip_)
#else
#define ip_simd_convert_rnd_clip_UBYTE_FLOAT NULL
#endif


#if defined(simd_cnvrt_rnd_clip_LONG_FLOAT) && defined(simd_cnvrt_clip_BYTE_LONG)
static inline simd_BYTE simd_cnvrt_rnd_clip_BYTE_FLOAT(simd_FLOAT a, simd_FLOAT b,
							simd_FLOAT c, simd_FLOAT d)
{
    return simd_cnvrt_clip_BYTE_LONG(simd_cnvrt_rnd_clip_LONG_FLOAT(a),
				     simd_cnvrt_rnd_clip_LONG_FLOAT(b),
				     simd_cnvrt_rnd_clip_LONG_FLOAT(c),
				     simd_cnvrt_rnd_clip_LONG_FLOAT(d));
}
generate_simd_convert_s4(BYTE, FLOAT, rnd_clip_)
#else
#define ip_simd_convert_rnd_clip_BYTE_FLOAT NULL
#endif


#if defined(simd_cnvrt_rnd_clip_LONG_FLOAT) && defined(simd_cnvrt_clip_USHORT_LONG)
static inline simd_USHORT simd_cnvrt_rnd_clip_USHORT_FLOAT(simd_FLOAT a, simd_FLOAT b)
{
    return simd_cnvrt_clip_USHORT_LONG(simd_cnvrt_rnd_clip_LONG_FLOAT(a),
				       simd_cnvrt_rnd_clip_LONG_FLOAT(b));
}
generate_simd_convert_s2(USHORT, FLOAT, rnd_clip_)
#else
#define ip_simd_convert_rnd_clip_USHORT_FLOAT NULL
#endif


#if defined(simd_cnvrt_rnd_clip_LONG_FLOAT) && defined(simd_cnvrt_clip_SHORT_LONG)
static inline simd_SHORT simd_cnvrt_rnd_clip_SHORT_FLOAT(simd_FLOAT a, simd_FLOAT b)
{
    return simd_cnvrt_clip_SHORT_LONG(simd_cnvrt_rnd_clip_LONG_FLOAT(a),
				      simd_cnvrt_rnd_clip_LONG_FLOAT(b));
}
generate_simd_convert_s2(SHORT, FLOAT, rnd_clip_)
#else
#define ip_simd_convert_rnd_clip_SHORT_FLOAT NULL
#endif


#ifdef simd_cnvrt_rnd_clip_LONG_FLOAT
generate_simd_convert(LONG, FLOAT, rnd_clip_)
#else
#define ip_simd_convert_rnd_clip_LONG_FLOAT NULL
#endif


#ifdef simd_cnvrt_rnd_clip_ULONG_FLOAT
generate_simd_convert(ULONG, FLOAT, rnd_clip_)
#else
#define ip_simd_convert_rnd_clip_ULONG_FLOAT NULL
#endif


/*
 * Conversion from DOUBLE to integer with rounding and clipping.
 */

#if defined(simd_cnvrt_rnd_clip_ULONG_DOUBLE) && defined(simd_cnvrt_clip_UBYTE_ULONG)
static inline simd_UBYTE simd_cnvrt_rnd_clip_UBYTE_DOUBLE(simd_DOUBLE a, simd_DOUBLE b,
							  simd_DOUBLE c, simd_DOUBLE d,
							  simd_DOUBLE e, simd_DOUBLE f,
							  simd_DOUBLE g, simd_DOUBLE h)
{
    simd_ULONG lu, lv, lx, ly;
    lu = (simd_ULONG)simd_cnvrt_rnd_clip_ULONG_DOUBLE(a, b);
    lv = (simd_ULONG)simd_cnvrt_rnd_clip_ULONG_DOUBLE(c, d);
    lx = (simd_ULONG)simd_cnvrt_rnd_clip_ULONG_DOUBLE(e, f);
    ly = (simd_ULONG)simd_cnvrt_rnd_clip_ULONG_DOUBLE(g, h);

    return simd_cnvrt_clip_UBYTE_ULONG(lu, lv, lx, ly);
}
generate_simd_convert_s8(UBYTE, DOUBLE, rnd_clip_)
#else
#define ip_simd_convert_rnd_clip_UBYTE_DOUBLE NULL
#endif


#if defined(simd_cnvrt_rnd_clip_LONG_DOUBLE) && defined(simd_cnvrt_clip_BYTE_LONG)
static inline simd_BYTE simd_cnvrt_rnd_clip_BYTE_DOUBLE(simd_DOUBLE a, simd_DOUBLE b,
							simd_DOUBLE c, simd_DOUBLE d,
							simd_DOUBLE e, simd_DOUBLE f,
							simd_DOUBLE g, simd_DOUBLE h)
{
    simd_LONG lu, lv, lx, ly;
    lu = simd_cnvrt_rnd_clip_LONG_DOUBLE(a, b);
    lv = simd_cnvrt_rnd_clip_LONG_DOUBLE(c, d);
    lx = simd_cnvrt_rnd_clip_LONG_DOUBLE(e, f);
    ly = simd_cnvrt_rnd_clip_LONG_DOUBLE(g, h);

    return simd_cnvrt_clip_BYTE_LONG(lu, lv, lx, ly);
}
generate_simd_convert_s8(BYTE, DOUBLE, rnd_clip_)
#else
#define ip_simd_convert_rnd_clip_BYTE_DOUBLE NULL
#endif


#if defined(simd_cnvrt_rnd_clip_ULONG_DOUBLE) && defined(simd_cnvrt_clip_USHORT_ULONG)
static inline simd_USHORT simd_cnvrt_rnd_clip_USHORT_DOUBLE(simd_DOUBLE a, simd_DOUBLE b,
							    simd_DOUBLE c, simd_DOUBLE d)
{
    simd_ULONG lx, ly;
    lx = (simd_ULONG)simd_cnvrt_rnd_clip_ULONG_DOUBLE(a, b);
    ly = (simd_ULONG)simd_cnvrt_rnd_clip_ULONG_DOUBLE(c, d);

    return simd_cnvrt_clip_USHORT_ULONG(lx, ly);
}
generate_simd_convert_s4(USHORT, DOUBLE, rnd_clip_)
#else
#define ip_simd_convert_rnd_clip_USHORT_DOUBLE NULL
#endif


#if defined(simd_cnvrt_rnd_clip_LONG_DOUBLE) && defined(simd_cnvrt_clip_SHORT_LONG)
static inline simd_SHORT simd_cnvrt_rnd_clip_SHORT_DOUBLE(simd_DOUBLE a, simd_DOUBLE b,
							  simd_DOUBLE c, simd_DOUBLE d)
{
    simd_LONG lx, ly;
    lx = simd_cnvrt_rnd_clip_LONG_DOUBLE(a, b);
    ly = simd_cnvrt_rnd_clip_LONG_DOUBLE(c, d);

    return simd_cnvrt_clip_SHORT_LONG(lx, ly);
}
generate_simd_convert_s4(SHORT, DOUBLE, rnd_clip_)
#else
#define ip_simd_convert_rnd_clip_SHORT_DOUBLE NULL
#endif


#ifdef simd_cnvrt_rnd_clip_LONG_DOUBLE
generate_simd_convert_s2(LONG, DOUBLE, rnd_clip_)
#else
#define ip_simd_convert_rnd_clip_LONG_DOUBLE NULL
#endif


#ifdef simd_cnvrt_rnd_clip_ULONG_DOUBLE
generate_simd_convert_s2(ULONG, DOUBLE, rnd_clip_)
#else
#define ip_simd_convert_rnd_clip_ULONG_DOUBLE NULL
#endif


/*
 * And the function lookup tables for rounded and clipped conversions.
 */

static improc_function_IIf convert_round_clip_to_BYTE[IM_TYPEMAX] = {
    [IM_FLOAT] = ip_simd_convert_rnd_clip_BYTE_FLOAT,
    [IM_DOUBLE] = ip_simd_convert_rnd_clip_BYTE_DOUBLE,
};

static improc_function_IIf convert_round_clip_to_UBYTE[IM_TYPEMAX] = {
    [IM_FLOAT] = ip_simd_convert_rnd_clip_UBYTE_FLOAT,
    [IM_DOUBLE] = ip_simd_convert_rnd_clip_UBYTE_DOUBLE,
};

static improc_function_IIf convert_round_clip_to_SHORT[IM_TYPEMAX] = {
    [IM_FLOAT] = ip_simd_convert_rnd_clip_SHORT_FLOAT,
    [IM_DOUBLE] = ip_simd_convert_rnd_clip_SHORT_DOUBLE,
};

static improc_function_IIf convert_round_clip_to_USHORT[IM_TYPEMAX] = {
    [IM_FLOAT] = ip_simd_convert_rnd_clip_USHORT_FLOAT,
    [IM_DOUBLE] = ip_simd_convert_rnd_clip_USHORT_DOUBLE,
};

static improc_function_IIf convert_round_clip_to_LONG[IM_TYPEMAX] = {
    [IM_FLOAT] = ip_simd_convert_rnd_clip_LONG_FLOAT,
    [IM_DOUBLE] = ip_simd_convert_rnd_clip_LONG_DOUBLE,
};

static improc_function_IIf convert_round_clip_to_ULONG[IM_TYPEMAX] = {
    [IM_FLOAT] = ip_simd_convert_rnd_clip_ULONG_FLOAT,
    [IM_DOUBLE] = ip_simd_convert_rnd_clip_ULONG_DOUBLE,
};


improc_function_IIf *ip_simd_convert_round_clipped[IM_TYPEMAX] = {
    [IM_BYTE] = convert_round_clip_to_BYTE,
    [IM_UBYTE] = convert_round_clip_to_UBYTE,
    [IM_SHORT] = convert_round_clip_to_SHORT,
    [IM_USHORT] = convert_round_clip_to_USHORT,
    [IM_LONG] = convert_round_clip_to_LONG,
    [IM_ULONG] = convert_round_clip_to_ULONG
};

