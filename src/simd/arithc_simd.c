/*
   Module: src/simd/arithc_simd.c
   Author: M.J.Cree

   Binary arithmetic operators between an image and a constant (SIMD
   implementations).
   Entry points are in arithc.c

   Copyright (C) 2012-2013, 2016-2017 Michael J. Cree

*/

#include <math.h>

#include <ip/ip.h>

#include "../internal.h"
#include "simd.h"
#include "arithc_simd.h"


/*
 * Macro to process images with SIMD 'vector' operators.
 */

#define SIMD_PROCESS_IMAGE(d, c, type, op)			\
    for (int j=0; j<d->size.y; j++) {				\
	int length = d->row_size / sizeof(simd_  ## type);	\
	simd_ ## type *dptr = IM_ROWADR(d, j, v);		\
	for (int i=0; i<length; i++) {				\
	    PREFETCH_READ_WRITE(dptr+PREFETCH_OFS);		\
	    *dptr = simd_ ## op ## _ ## type(*dptr, c);		\
	    dptr++;						\
	}							\
    }

/* And the equivalent for doing integer fixed point arithmetic */
#define SIMD_PROCESS_IMAGE_FIXED(d, cw, cf, type, op)		\
    for (int j=0; j<d->size.y; j++) {				\
	int length = d->row_size / sizeof(simd_  ## type);	\
	simd_ ## type *dptr = IM_ROWADR(d, j, v);		\
	for (int i=0; i<length; i++) {				\
	    PREFETCH_READ_WRITE(dptr+PREFETCH_OFS);		\
	    *dptr = simd_ ## op ## _ ## type(*dptr, cw, cf);	\
	    dptr++;						\
	}							\
    }

#define SIMD_PROCESS_IMAGE_FIXED_SHIFTED(d, cf, shift, type, op)	\
    for (int j=0; j<d->size.y; j++) {					\
	int length = d->row_size / sizeof(simd_  ## type);		\
	simd_ ## type *dptr = IM_ROWADR(d, j, v);			\
	for (int i=0; i<length; i++) {					\
	    PREFETCH_READ_WRITE(dptr+PREFETCH_OFS);			\
	    *dptr = simd_ ## op ## _ ## type(*dptr, cf, shift);		\
	    dptr++;							\
	}								\
    }

/* Round c before processing image */
#define generate_image_cround_function(type, operator)			\
    static void								\
    im_simd_ ## operator ## _cround_ ## type(ip_image *d, double c, int f) \
    {									\
	const simd_ ## type cval = simd_splat_ ## type((type ## _type)lround(c)); \
	SIMD_PROCESS_IMAGE(d, cval, type, operator);			\
	SIMD_EXIT();							\
    }

/* Use c exactly as is to process image */
#define generate_image_casis_function(type, operator)                   \
    static void                                                         \
    im_simd_ ## operator ## _casis_ ## type(ip_image *d, double c, int f) \
    {                                                                   \
	const simd_ ## type cval = simd_splat_ ## type((type ## _type)c); \
        SIMD_PROCESS_IMAGE(d, cval, type, operator);			\
	SIMD_EXIT();							\
    }

/* Convert c to a 32/32 fixed point integer representation */
#define generate_image_cfix32_function(type, ut, operator)		\
    static void                                                         \
    im_simd_ ## operator ## _cfixed_ ## type(ip_image *d, double c, int f) \
    {                                                                   \
	const uint64_t cval = (uint64_t)(int64_t)llround((double)(1ULL<<32) * c); \
	const simd_##ut cw = simd_splat_##ut(cval >> 32);		\
	const simd_ULONG cf = simd_splat_ULONG(cval & 0xffffffff);	\
        SIMD_PROCESS_IMAGE_FIXED(d, cw, cf, type, operator);		\
	SIMD_EXIT();							\
    }

/* Convert c to a 16/16 fixed point integer representation */
#define generate_image_cfix16_function(type, ut, operator)		\
    static void                                                         \
    im_simd_ ## operator ## _cfixed_ ## type(ip_image *d, double c, int f) \
    {                                                                   \
	const uint32_t cval = (uint32_t)(int32_t)round((double)(1<<16) * c); \
	const simd_##ut cw = simd_splat_##ut(cval >> 16);		\
	const simd_USHORT cf = simd_splat_USHORT(cval & 0xffff);	\
        SIMD_PROCESS_IMAGE_FIXED(d, cw, cf, type, operator);		\
	SIMD_EXIT();							\
    }

/* Convert c to a 32/32 fixed point integer representation with a shift */
#define generate_image_cfix32_shift_function(type, ut, operator)	\
    static void                                                         \
    im_simd_ ## operator ## _cfixed_ ## type(ip_image *d, double c, int f) \
    {                                                                   \
	const int l = (int)ceil(-log2(fabs(c))) - 2;			\
	const uint64_t cval = (uint64_t)(int64_t)llround((double)(1ULL<<32) * c * pow(2,l)); \
	const simd_##ut cf = simd_splat_##ut(cval & 0xffffffff);	\
	const simd_ULONG shift = simd_splat_ULONG(l);			\
        SIMD_PROCESS_IMAGE_FIXED_SHIFTED(d, cf, shift, type, operator); \
	SIMD_EXIT();							\
    }

/* Convert c to a 16/16 fixed point integer representation with a shift */
#define generate_image_cfix16_shift_function(type, ut, operator)	\
    static void                                                         \
    im_simd_ ## operator ## _cfixed_ ## type(ip_image *d, double c, int f) \
    {                                                                   \
	const int l = (int)ceil(-log2(fabs(c))) - 2;			\
	const uint32_t cval = (uint32_t)(int32_t)round((double)(1<<16) * c * pow(2,l)); \
	const simd_##ut cf = simd_splat_##ut(cval & 0xffff);		\
	const simd_USHORT shift = simd_splat_USHORT(l);			\
        SIMD_PROCESS_IMAGE_FIXED_SHIFTED(d, cf, shift, type, operator); \
	SIMD_EXIT();							\
    }

/* Use real floating point operator (add, sub) to process complex image */
#define generate_image_complex_as_float_function(type, floattype, operator) \
    static void								\
    im_simd_ ## operator ## _complex_ ## type(ip_image *d, ip_dcomplex z, int f) \
    {									\
	simd_ ## type zs = simd_splat_ ## type((floattype ## _type)z.r, (floattype ## _type)z.i); \
	SIMD_PROCESS_IMAGE(d, zs, floattype, operator);			\
	SIMD_EXIT();							\
    }

/* Use complex operator (mul, div) to process complex image */
#define generate_image_complex_function(type, floattype, operator)	\
    static void								\
    im_simd_ ## operator ## _complex_ ## type(ip_image *d, ip_dcomplex z, int f) \
    {									\
	simd_ ## type zs = simd_splat_ ## type((floattype ## _type)z.r, (floattype ## _type)z.i); \
	SIMD_PROCESS_IMAGE(d, zs, type, operator);			\
	SIMD_EXIT();							\
    }

#define generate_image_complex_mulcsc_function(type, floattype)		\
    static void								\
    im_simd_mul_complex_ ## type(ip_image *d, ip_dcomplex z, int f) \
    {									\
	const simd_##type vz = simd_splat_##type(z.r, z.i);		\
	const simd_##floattype vr = simd_real_##type(vz);		\
	const simd_##floattype vi = simd_nimag_##type(vz);		\
	for (int j=0; j<d->size.y; j++) {				\
	    int length = d->row_size / sizeof(simd_##type);		\
	    simd_##type *dptr = IM_ROWADR(d, j, v);			\
	    for (int i=0; i<length; i++) {				\
		PREFETCH_READ_WRITE(dptr+PREFETCH_OFS);			\
		*dptr = simd_mulcsc_##type(*dptr, vr, vi);		\
		 dptr++;						\
	    }								\
	}								\
	SIMD_EXIT();							\
    }

/* Calculate integer reciprocal of c before processing image. */
#define generate_image_idivc_function(type, operator)		\
    static void							\
    im_simd_idivc_##type(ip_image *d, double c, int f)		\
    {								\
	struct simd_rcp_##type cval = simd_rcpc_##type(c);	\
	SIMD_PROCESS_IMAGE(d, cval, type, operator);		\
	SIMD_EXIT();						\
    }



/*
 * SIMD accelerated im_addc() integer and real functionality
 */

generate_image_cround_function(UBYTE, add)
generate_image_cround_function(USHORT, add)

#ifdef simd_add_ULONG
generate_image_cround_function(ULONG, add)
#else
#define im_simd_add_cround_ULONG NULL
#endif

#ifdef simd_add_FLOAT
generate_image_casis_function(FLOAT, add)
#endif
#ifdef simd_add_DOUBLE
generate_image_casis_function(DOUBLE, add)
#endif

improc_function_Icf ip_simd_addc_function[IM_TYPEMAX] = {
    [IM_BYTE] = im_simd_add_cround_UBYTE,
    [IM_UBYTE] = im_simd_add_cround_UBYTE,
    [IM_SHORT] = im_simd_add_cround_USHORT,
    [IM_USHORT] = im_simd_add_cround_USHORT,
    [IM_LONG] = im_simd_add_cround_ULONG,
    [IM_ULONG] = im_simd_add_cround_ULONG,
#ifdef simd_add_FLOAT
    [IM_FLOAT] = im_simd_add_casis_FLOAT,
#endif
#ifdef simd_add_DOUBLE
    [IM_DOUBLE] = im_simd_add_casis_DOUBLE,
#endif
    [IM_PALETTE] = im_simd_add_cround_UBYTE
};


/*
 * SIMD accelerated im_addc() integer functionality with FLG_CLIP.
 */

generate_image_cround_function(BYTE, add_clipped)
generate_image_cround_function(UBYTE, add_clipped)
generate_image_cround_function(SHORT, add_clipped)
generate_image_cround_function(USHORT, add_clipped)

improc_function_Icf ip_simd_addc_clipped_function[IM_TYPEMAX] = {
    [IM_BYTE] = im_simd_add_clipped_cround_BYTE,
    [IM_UBYTE] = im_simd_add_clipped_cround_UBYTE,
    [IM_SHORT] = im_simd_add_clipped_cround_SHORT,
    [IM_USHORT] = im_simd_add_clipped_cround_USHORT,
};


/*
 * SIMD accelerated im_addc() complex image functionality.
 */

#if defined(simd_add_FLOAT) && defined(simd_splat_COMPLEX)
generate_image_complex_as_float_function(COMPLEX, FLOAT, add)
#endif
#if defined(simd_add_DOUBLE) && defined(simd_splat_DCOMPLEX)
generate_image_complex_as_float_function(DCOMPLEX, DOUBLE, add)
#endif

improc_function_Izf ip_simd_addc_complex_function[IM_TYPEMAX] = {
#if defined(simd_add_FLOAT) && defined(simd_splat_COMPLEX)
    [IM_COMPLEX] = im_simd_add_complex_COMPLEX,
#endif
#if defined(simd_add_DOUBLE) && defined(simd_splat_DCOMPLEX)
    [IM_DCOMPLEX] = im_simd_add_complex_DCOMPLEX
#endif
};


/* RGB image addc function not implemented yet */
improc_function_Iccccf ip_simd_addc_rgb_function[IM_TYPEMAX] = {};


/*
 * SIMD accelerated im_subc() functionality.
 */

generate_image_cround_function(UBYTE, sub)
generate_image_cround_function(USHORT, sub)
generate_image_cround_function(ULONG, sub)
#ifdef simd_add_FLOAT
generate_image_casis_function(FLOAT, sub)
#endif
#ifdef simd_add_DOUBLE
generate_image_casis_function(DOUBLE, sub)
#endif

improc_function_Icf ip_simd_subc_function[IM_TYPEMAX] = {
    [IM_BYTE] = im_simd_sub_cround_UBYTE,
    [IM_UBYTE] = im_simd_sub_cround_UBYTE,
    [IM_SHORT] = im_simd_sub_cround_USHORT,
    [IM_USHORT] = im_simd_sub_cround_USHORT,
    [IM_LONG] = im_simd_sub_cround_ULONG,
    [IM_ULONG] = im_simd_sub_cround_ULONG,
#ifdef simd_sub_FLOAT
    [IM_FLOAT] = im_simd_sub_casis_FLOAT,
#endif
#ifdef simd_sub_DOUBLE
    [IM_DOUBLE] = im_simd_sub_casis_DOUBLE,
#endif
    [IM_PALETTE] = im_simd_sub_cround_UBYTE
};

generate_image_cround_function(BYTE, sub_clipped)
generate_image_cround_function(UBYTE, sub_clipped)
generate_image_cround_function(SHORT, sub_clipped)
generate_image_cround_function(USHORT, sub_clipped)

improc_function_Icf ip_simd_subc_clipped_function[IM_TYPEMAX] = {
    [IM_BYTE] = im_simd_sub_clipped_cround_BYTE,
    [IM_UBYTE] = im_simd_sub_clipped_cround_UBYTE,
    [IM_SHORT] = im_simd_sub_clipped_cround_SHORT,
    [IM_USHORT] = im_simd_sub_clipped_cround_USHORT,
};


/* RGB image subc function not implemented yet */
improc_function_Iccccf ip_simd_subc_rgb_function[IM_TYPEMAX] = {};


/*
 * Simd accelerated im_mulc()/im_divc() integer multiplication functionality
 * for arguments c >= 0.5.
 */

/*
 * Unsigned mulc operators
 */

#ifdef simd_imulf32_ULONG

#define simd_mulc_ULONG(a,b,c) simd_imulf32_ULONG(a,b,c)

static inline simd_USHORT simd_mulc_USHORT(simd_USHORT a, simd_ULONG cw, simd_ULONG cf)
{
    simd_ULONG_2 x = simd_cnvrt_ULONG_USHORT(a);
    x.ul[0] = simd_mulc_ULONG(x.ul[0], cw, cf);
    x.ul[1] = simd_mulc_ULONG(x.ul[1], cw, cf);
    return simd_cnvrt_USHORT_ULONG(x.ul[0], x.ul[1]);
}

static inline simd_USHORT simd_mulc_clipped_USHORT(simd_USHORT a, simd_ULONG cw, simd_ULONG cf)
{
    const simd_ULONG ushort_max  = simd_splat_ULONG(UINT16_MAX);
    simd_ULONG_2 x = simd_cnvrt_ULONG_USHORT(a);
    x.ul[0] = simd_min_ULONG(simd_mulc_ULONG(x.ul[0], cw, cf), ushort_max);
    x.ul[1] = simd_min_ULONG(simd_mulc_ULONG(x.ul[1], cw, cf), ushort_max);
    return simd_cnvrt_USHORT_ULONG(x.ul[0], x.ul[1]);
}

generate_image_cfix32_function(USHORT, ULONG, mulc)
generate_image_cfix32_function(USHORT, ULONG, mulc_clipped)
generate_image_cfix32_function(ULONG, ULONG, mulc)

#else
#define im_simd_mulc_cfixed_USHORT NULL
#define im_simd_mulc_clipped_cfixed_USHORT NULL
#define im_simd_mulc_cfixed_ULONG NULL
#endif


/*
 * Prefer a 16/16 fixed point format for UBYTE but will use 32/32 if that is
 * all there is.
 */

#ifdef simd_imulf16_USHORT

static inline simd_UBYTE simd_mulc_UBYTE(simd_UBYTE a, simd_USHORT cw, simd_USHORT cf)
{
    simd_USHORT_2 x = simd_cnvrt_USHORT_UBYTE(a);
    x.us[0] = simd_imulf16_USHORT(x.us[0], cw, cf);
    x.us[1] = simd_imulf16_USHORT(x.us[1], cw, cf);
    return simd_cnvrt_UBYTE_USHORT(x.us[0], x.us[1]);
}
static inline simd_UBYTE simd_mulc_clipped_UBYTE(simd_UBYTE a, simd_USHORT cw, simd_USHORT cf)
{
    simd_USHORT_2 x = simd_cnvrt_USHORT_UBYTE(a);
    x.us[0] = simd_imulf16_USHORT(x.us[0], cw, cf);
    x.us[1] = simd_imulf16_USHORT(x.us[1], cw, cf);
    return simd_cnvrt_clip_UBYTE_USHORT(x.us[0], x.us[1]);
}
generate_image_cfix16_function(UBYTE, USHORT, mulc)
generate_image_cfix16_function(UBYTE, USHORT, mulc_clipped)

#else

#  ifdef simd_imulf32_ULONG

static inline simd_UBYTE simd_mulc_UBYTE(simd_UBYTE a, simd_ULONG cw, simd_ULONG cf)
{
    simd_USHORT_2 x = simd_cnvrt_USHORT_UBYTE(a);
    x.us[0] = simd_mulc_USHORT(x.us[0], cw, cf);
    x.us[1] = simd_mulc_USHORT(x.us[1], cw, cf);
    return simd_cnvrt_UBYTE_USHORT(x.us[0], x.us[1]);
}
static inline simd_UBYTE simd_mulc_clipped_UBYTE(simd_UBYTE a, simd_ULONG cw, simd_ULONG cf)
{
    simd_USHORT_2 x = simd_cnvrt_USHORT_UBYTE(a);
    x.us[0] = simd_mulc_USHORT(x.us[0], cw, cf);
    x.us[1] = simd_mulc_USHORT(x.us[1], cw, cf);
    return simd_cnvrt_clip_UBYTE_USHORT(x.us[0], x.us[1]);
}
generate_image_cfix32_function(UBYTE, ULONG, mulc)
generate_image_cfix32_function(UBYTE, ULONG, mulc_clipped)

#  else

#define im_simd_mulc_cfixed_UBYTE NULL
#define im_simd_mulc_clipped_cfixed_UBYTE NULL

#  endif
#endif

/*
 * Signed mulc operators
 */

#ifdef simd_imulf32_LONG

#define simd_mulc_LONG(a,b,c) simd_imulf32_LONG(a,b,c)

static inline simd_SHORT simd_mulc_SHORT(simd_SHORT a, simd_LONG cw, simd_ULONG cf)
{
    simd_LONG_2 x = simd_cnvrt_LONG_SHORT(a);
    x.l[0] = simd_mulc_LONG(x.l[0], cw, cf);
    x.l[1] = simd_mulc_LONG(x.l[1], cw, cf);
    return (simd_SHORT)simd_cnvrt_USHORT_ULONG((simd_ULONG)x.l[0], (simd_ULONG)x.l[1]);
}

static inline simd_SHORT simd_mulc_clipped_SHORT(simd_SHORT a, simd_LONG cw, simd_ULONG cf)
{
    const simd_LONG short_max = simd_splat_LONG(INT16_MAX);
    const simd_LONG short_min = simd_splat_LONG(INT16_MIN);
    simd_LONG_2 x = simd_cnvrt_LONG_SHORT(a);
    x.l[0] = simd_mulc_LONG(x.l[0], cw, cf);
    x.l[1] = simd_mulc_LONG(x.l[1], cw, cf);
    x.l[0] = simd_max_LONG(simd_min_LONG(x.l[0], short_max), short_min);
    x.l[1] = simd_max_LONG(simd_min_LONG(x.l[1], short_max), short_min);
    return (simd_SHORT)simd_cnvrt_USHORT_ULONG((simd_ULONG)x.l[0], (simd_ULONG)x.l[1]);
}

generate_image_cfix32_function(SHORT, LONG, mulc)
generate_image_cfix32_function(SHORT, LONG, mulc_clipped)
generate_image_cfix32_function(LONG, LONG, mulc)

#else
#define im_simd_mulc_cfixed_SHORT NULL
#define im_simd_mulc_clipped_cfixed_SHORT NULL
#define im_simd_mulc_cfixed_LONG NULL
#endif


#ifdef simd_imulf16_SHORT

static inline simd_BYTE simd_mulc_BYTE(simd_BYTE a, simd_SHORT cw, simd_USHORT cf)
{
    simd_SHORT_2 x = simd_cnvrt_SHORT_BYTE(a);
    x.s[0] = simd_imulf16_SHORT(x.s[0], cw, cf);
    x.s[1] = simd_imulf16_SHORT(x.s[1], cw, cf);
    return (simd_BYTE)simd_cnvrt_UBYTE_USHORT((simd_USHORT)x.s[0], (simd_USHORT)x.s[1]);
}
static inline simd_BYTE simd_mulc_clipped_BYTE(simd_BYTE a, simd_SHORT cw, simd_USHORT cf)
{
    simd_SHORT_2 x = simd_cnvrt_SHORT_BYTE(a);
    x.s[0] = simd_imulf16_SHORT(x.s[0], cw, cf);
    x.s[1] = simd_imulf16_SHORT(x.s[1], cw, cf);
    return simd_cnvrt_clip_BYTE_SHORT(x.s[0], x.s[1]);
}

generate_image_cfix16_function(BYTE, SHORT, mulc)
generate_image_cfix16_function(BYTE, SHORT, mulc_clipped)

#else

#  ifdef simd_imulf32_SHORT

static inline simd_BYTE simd_mulc_BYTE(simd_BYTE a, simd_LONG cw, simd_ULONG cf)
{
    simd_SHORT_2 x = simd_cnvrt_SHORT_BYTE(a);
    x.s[0] = simd_mulc_SHORT(x.s[0], cw, cf);
    x.s[1] = simd_mulc_SHORT(x.s[1], cw, cf);
    return (simd_BYTE)simd_cnvrt_UBYTE_USHORT((simd_UBYTE)x.s[0], (simd_BYTE)x.s[1]);
}
static inline simd_BYTE simd_mulc_clip_BYTE(simd_BYTE a, simd_LONG cw, simd_ULONG cf)
{
    simd_SHORT_2 x = simd_cnvrt_SHORT_BYTE(a);
    x.s[0] = simd_mulc_SHORT(x.s[0], cw, cf);
    x.s[1] = simd_mulc_SHORT(x.s[1], cw, cf);
    return (simd_BYTE)simd_cnvrt_clip_UBYTE_USHORT((simd_UBYTE)x.s[0], (simd_BYTE)x.s[1]);
}

generate_image_cfix32_function(BYTE, LONG, mulc)
generate_image_cfix32_function(BYTE, LONG, mulc_clipped)

#  else
#  define im_simd_mulc_cfixed_BYTE NULL
#  define im_simd_mulc_clipped_cfixed_BYTE NULL
#  endif
#endif

/*
 * SIMD accelerated im_mulc() real (floating-point) functionality.
 */

#ifdef simd_mul_FLOAT
generate_image_casis_function(FLOAT, mul)
#endif
#ifdef simd_mul_DOUBLE
generate_image_casis_function(DOUBLE, mul)
#endif

/*
 * Note that for integral types the mulc function implements multiplication
 * with a constant greater than 0.5 only.  This is called by both im_mulc()
 * (when muliplier is >= 0.5) and im_divc() (when divisor is < 2).
 */

improc_function_Icf ip_simd_mulc_function[IM_TYPEMAX] = {
    [IM_UBYTE] = im_simd_mulc_cfixed_UBYTE,
    [IM_BYTE] = im_simd_mulc_cfixed_BYTE,
    [IM_USHORT] = im_simd_mulc_cfixed_USHORT,
    [IM_SHORT] = im_simd_mulc_cfixed_SHORT,
    [IM_ULONG] = im_simd_mulc_cfixed_ULONG,
    [IM_LONG] = im_simd_mulc_cfixed_LONG,
#ifdef simd_mul_FLOAT
    [IM_FLOAT] = im_simd_mul_casis_FLOAT,
#endif
#ifdef simd_mul_DOUBLE
    [IM_DOUBLE] = im_simd_mul_casis_DOUBLE
#endif
};


improc_function_Icf ip_simd_mulc_clipped_function[IM_TYPEMAX] = {
    [IM_UBYTE] = im_simd_mulc_clipped_cfixed_UBYTE,
    [IM_BYTE] = im_simd_mulc_clipped_cfixed_BYTE,
    [IM_USHORT] = im_simd_mulc_clipped_cfixed_USHORT,
    [IM_SHORT] = im_simd_mulc_clipped_cfixed_SHORT
};



/* SIMD im_mulc() complex multiplication by a constant */

#ifdef simd_mulcsc_COMPLEX
generate_image_complex_mulcsc_function(COMPLEX, FLOAT)
#else
#  ifdef simd_mul_COMPLEX
generate_image_complex_function(COMPLEX, FLOAT, mul)
#  endif
#endif

#ifdef simd_mulcsc_DCOMPLEX
generate_image_complex_mulcsc_function(DCOMPLEX, DOUBLE);
#else
#  ifdef simd_mul_DCOMPLEX
generate_image_complex_function(DCOMPLEX, DOUBLE, mul);
#  endif
#endif

improc_function_Izf ip_simd_mulc_complex_function[IM_TYPEMAX] = {
#if defined(simd_mul_COMPLEX) || defined(simd_mulcsc_COMPLEX)
    [IM_COMPLEX] = im_simd_mul_complex_COMPLEX,
#endif
#if defined(simd_mul_DCOMPLEX) || defined(simd_mulcsc_DCOMPLEX)
    [IM_DOUBLE] = im_simd_mul_complex_DCOMPLEX
#endif
};


/* Not implemented yet. */
improc_function_Iccccf ip_simd_mulc_rgb_function[IM_TYPEMAX] = {};



/*
 * SIMD accelerated im_divc() functionality with FLG_INTDIV, i.e., integer
 * division by an integer divisor with truncation towards zero.
 */

/*
 * The SIMD arch header does not provide for (U)BYTE integer
 * reciprocals/division.  We convert (U)BYTE data to (U)SHORT to perform the
 * division.
 */

#if defined(simd_idiv_USHORT) && defined(simd_cnvrt_USHORT_UBYTE)

static inline simd_UBYTE simd_idiv_UBYTE(simd_UBYTE n, struct simd_rcp_USHORT r)
{
    simd_USHORT_2 nn = simd_cnvrt_USHORT_UBYTE(n);
    nn.us[0] = simd_idiv_USHORT(nn.us[0], r);
    nn.us[1] = simd_idiv_USHORT(nn.us[1], r);
    return simd_cnvrt_UBYTE_USHORT(nn.us[0], nn.us[1]);
}

static void im_simd_idivc_UBYTE(ip_image *d, double c, int f)
{
    struct simd_rcp_USHORT cval = simd_rcpc_USHORT(c);
    SIMD_PROCESS_IMAGE(d, cval, UBYTE, idiv);
    SIMD_EXIT();
}

#else
#define im_simd_idivc_UBYTE NULL
#endif

#if defined(simd_idiv_SHORT) && defined(simd_cnvrt_SHORT_BYTE)

static inline simd_BYTE simd_idiv_BYTE(simd_BYTE n, struct simd_rcp_SHORT r)
{
    simd_SHORT_2 nn = simd_cnvrt_SHORT_BYTE(n);
    nn.s[0] = simd_idiv_SHORT(nn.s[0], r);
    nn.s[1] = simd_idiv_SHORT(nn.s[1], r);
    return (simd_BYTE)simd_cnvrt_UBYTE_USHORT((simd_USHORT)nn.s[0], (simd_USHORT)nn.s[1]);
}

static void im_simd_idivc_BYTE(ip_image *d, double c, int f)
{
    struct simd_rcp_SHORT cval = simd_rcpc_SHORT(c);
    SIMD_PROCESS_IMAGE(d, cval, BYTE, idiv);
    SIMD_EXIT();
}

#else
#define im_simd_idivc_BYTE NULL
#endif


/*
 * The arch header may provide for the other integral type divisions so this
 * is easier here.
 */

#ifdef simd_idiv_USHORT
generate_image_idivc_function(USHORT, idiv)
#else
#define im_simd_idivc_USHORT NULL
#endif

#ifdef simd_idiv_SHORT
generate_image_idivc_function(SHORT, idiv)
#else
#define im_simd_idivc_SHORT NULL
#endif

#ifdef simd_idiv_ULONG
generate_image_idivc_function(ULONG, idiv)
#else
#define im_simd_idivc_ULONG NULL
#endif

#ifdef simd_idiv_LONG
generate_image_idivc_function(LONG, idiv)
#else
#define im_simd_idivc_LONG NULL
#endif

improc_function_Icf ip_simd_idivc_function[IM_TYPEMAX] = {
    [IM_UBYTE] = im_simd_idivc_UBYTE,
    [IM_BYTE] = im_simd_idivc_BYTE,
    [IM_USHORT] = im_simd_idivc_USHORT,
    [IM_SHORT] = im_simd_idivc_SHORT,
    [IM_ULONG] = im_simd_idivc_ULONG,
    [IM_LONG] = im_simd_idivc_LONG,
};

/*
 * SIMD accelerated im_divc() functionality.
 */


#ifdef simd_idivf32_ULONG

#define simd_divc_ULONG(a,b,s) simd_idivf32_ULONG(a,b,s)

static inline simd_USHORT simd_divc_USHORT(simd_USHORT a, simd_ULONG cf, simd_ULONG s)
{
    simd_ULONG_2 x = simd_cnvrt_ULONG_USHORT(a);
    x.ul[0] = simd_divc_ULONG(x.ul[0], cf, s);
    x.ul[1] = simd_divc_ULONG(x.ul[1], cf, s);
    return simd_cnvrt_USHORT_ULONG(x.ul[0], x.ul[1]);
}

generate_image_cfix32_shift_function(USHORT, ULONG, divc)
generate_image_cfix32_shift_function(ULONG, ULONG, divc)

#else
#define im_simd_divc_cfixed_USHORT NULL
#define im_simd_divc_cfixed_ULONG NULL
#endif


#ifdef simd_idivf16_USHORT

static inline simd_UBYTE simd_divc_UBYTE(simd_UBYTE a, simd_USHORT cf, simd_USHORT s)
{
    simd_USHORT_2 x = simd_cnvrt_USHORT_UBYTE(a);
    x.us[0] = simd_idivf16_USHORT(x.us[0], cf, s);
    x.us[1] = simd_idivf16_USHORT(x.us[1], cf, s);
    return simd_cnvrt_UBYTE_USHORT(x.us[0], x.us[1]);
}
generate_image_cfix16_shift_function(UBYTE, USHORT, divc)

#else
#  ifdef simd_idivf32_ULONG
static inline simd_UBYTE simd_divc_UBYTE(simd_UBYTE a, simd_ULONG cf, simd_ULONG s)
{
    simd_USHORT_2 x = simd_cnvrt_USHORT_UBYTE(a);
    x.us[0] = simd_divc_USHORT(x.us[0], cf, s);
    x.us[1] = simd_divc_USHORT(x.us[1], cf, s);
    return simd_cnvrt_UBYTE_USHORT(x.us[0], x.us[1]);
}
generate_image_cfix32_shift_function(UBYTE, ULONG, divc)
#  else
#  define im_simd_divc_cfixed_UBYTE NULL
#  endif
#endif

#ifdef simd_idivf32_LONG

#define simd_divc_LONG(a,c,s) simd_idivf32_LONG(a,c,s)

static inline simd_SHORT simd_divc_SHORT(simd_SHORT a, simd_LONG cf, simd_ULONG s)
{
    simd_LONG_2 x = simd_cnvrt_LONG_SHORT(a);
    x.l[0] = simd_divc_LONG(x.l[0], cf, s);
    x.l[1] = simd_divc_LONG(x.l[1], cf, s);
    return (simd_SHORT)simd_cnvrt_USHORT_ULONG((simd_ULONG)x.l[0], (simd_ULONG)x.l[1]);
}

generate_image_cfix32_shift_function(SHORT, LONG, divc)
generate_image_cfix32_shift_function(LONG, LONG, divc)

#else
#define im_simd_divc_cfixed_SHORT NULL
#define im_simd_divc_cfixed_LONG NULL
#endif


#ifdef simd_idivf16_SHORT

static inline simd_BYTE simd_divc_BYTE(simd_BYTE a, simd_SHORT cf, simd_USHORT s)
{
    simd_SHORT_2 x = simd_cnvrt_SHORT_BYTE(a);
    x.s[0] = simd_idivf16_SHORT(x.s[0], cf, s);
    x.s[1] = simd_idivf16_SHORT(x.s[1], cf, s);
    return (simd_BYTE)simd_cnvrt_UBYTE_USHORT((simd_USHORT)x.s[0], (simd_USHORT)x.s[1]);
}
generate_image_cfix16_shift_function(BYTE, SHORT, divc)

#else
#  ifdef simd_idivf32_LONG
static inline simd_BYTE simd_divc_BYTE(simd_BYTE a, simd_LONG cf, simd_ULONG s)
{
    simd_SHORT_2 x = simd_cnvrt_SHORT_BYTE(a);
    x.s[0] = simd_divc_SHORT(x.s[0], cf, s);
    x.s[1] = simd_divc_SHORT(x.s[1], cf, s);
    return simd_cnvrt_BYTE_SHORT(x.s[0], x.s[1]);
}
generate_image_cfix32_shift_function(BYTE, LONG, divc)
#  else
#  define im_simd_divc_cfixed_BYTE NULL
#  endif
#endif


/* Real (floating-point) division. */

#ifdef simd_div_FLOAT
generate_image_casis_function(FLOAT, div)
#endif
#ifdef simd_div_DOUBLE
generate_image_casis_function(DOUBLE, div)
#endif

improc_function_Icf ip_simd_divc_function[IM_TYPEMAX] = {
    [IM_UBYTE] = im_simd_divc_cfixed_UBYTE,
    [IM_BYTE] = im_simd_divc_cfixed_BYTE,
    [IM_USHORT] = im_simd_divc_cfixed_USHORT,
    [IM_SHORT] = im_simd_divc_cfixed_SHORT,
    [IM_ULONG] = im_simd_divc_cfixed_ULONG,
    [IM_LONG] = im_simd_divc_cfixed_LONG,
#ifdef simd_div_FLOAT
    [IM_FLOAT] = im_simd_div_casis_FLOAT,
#endif
#ifdef simd_div_DOUBLE
    [IM_DOUBLE] = im_simd_div_casis_DOUBLE
#endif
};

/* Complex currently not implemented */

improc_function_Izf ip_simd_divc_complex_function[IM_TYPEMAX] = {};
