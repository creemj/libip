/*

   Module: ip/debug.c
   Author: M.J.Cree

   Routines for dumping images, etc. Useful for debugging.

   Copyright (C) 1995-2002, 2013, 2015-2016, 2019 Michael J. Cree

ENTRY POINTS:

   ip_dump_hist()
   ip_dump_image()
   ip_dump_roi()
   ip_dump_stats()

*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>

#include <ip/ip.h>
#include <ip/stats.h>
#include <ip/hist.h>

#include "internal.h"
#include "utils.h"



/*

NAME:

   ip_dump_image() \- Dump image information to stdout

PROTOTYPE:

   #include <ip/ip.h>

   void  ip_dump_image( file, im, flag )
   FILE *file;
   ip_image *im;
   int flag;

DESCRIPTION:

   Dump particulars on an image.  Usually used for debugging purposes.

   If flag bit DBG_STATS is set also prints out image statistics.  If
   flag bit DBG_VERBOSE is set also prints image data - not a good
   idea to do on large images!

SEE ALSO:

   ip_alloc_image()

*/

void ip_dump_image(FILE *f, ip_image *im, int flag)
{
    ip_stats *st;
    int i,j;

    fprintf(f,"Image at %p: ",im);
    fprintf(f,"%s (%d,%d), data at = %p.\n",
	   ip_image_type_info[im->type].name,
	   im->size.x,im->size.y,im->imbuff);
    if (flag & DBG_STATS) {
	st = ip_alloc_stats(0);
	im_stats(im, st, NULL);
	ip_dump_stats(f, st);
	ip_free_stats(st);
    }
    if (flag & DBG_VERBOSE) {
	static int fl[IM_TYPEMAX] = {0, 32, 16, 16, 8, 8, 4, 4, 8, 8, 4, 4, 16, 8, 8, 3};
	static char *bstr[2] = { ".", "T" };

	for (j=0; j<im->size.y; j++) {
	    fprintf(f,"%3d:", j);
	    for (i=0; i<im->size.x; i++) {
		if (i != 0 && i % fl[im->type] == 0) fprintf(f,"    ");
		switch (im->type) {
		case IM_PALETTE:
		case IM_UBYTE:
		    fprintf(f," %3u", IM_PIXEL(im, i, j, ub));
		    break;
		case IM_BYTE:
		    fprintf(f," %3u", IM_PIXEL(im, i, j, b));
		    break;
		case IM_USHORT:
		    fprintf(f," %6u", IM_PIXEL(im, i, j, us));
		    break;
		case IM_SHORT:
		    fprintf(f," %6d", IM_PIXEL(im, i, j, s));
		    break;
		case IM_ULONG:
		    fprintf(f," %12u", IM_PIXEL(im, i, j, ul));
		    break;
		case IM_LONG:
		    fprintf(f," %12d", IM_PIXEL(im, i, j, l));
		    break;
		case IM_FLOAT:
		    fprintf(f," %8.3g", IM_PIXEL(im, i, j, f));
		    break;
		case IM_DOUBLE:
		    fprintf(f," %8.3g", IM_PIXEL(im, i, j, d));
		    break;
		case IM_COMPLEX:
		    fprintf(f," %8.3g%ci%-8.3g",
			   IM_PIXEL(im, i, j, c).r,
			   "-+"[ IM_PIXEL(im, i, j, c).i >= 0],
			   fabs(IM_PIXEL(im, i, j, c).i));
		    break;
		case IM_DCOMPLEX:
		    fprintf(f," %8.3g%ci%-8.3g",
			   IM_PIXEL(im, i, j, dc).r,
			   "-+"[ IM_PIXEL(im, i, j, dc).i >= 0],
			   fabs(IM_PIXEL(im, i, j, dc).i));
		    break;
		case IM_BINARY:
		    fprintf(f," %s", bstr[IM_PIXEL(im, i, j, b) != FALSE]);
		    break;
		case IM_RGB:
		    fprintf(f," (%3d,%3d,%3d)", IM_PIXEL(im, i, j, r).r,
			   IM_PIXEL(im, i, j, r).g, IM_PIXEL(im, i, j, r).b);
		    break;
		case IM_RGBA:
		    fprintf(f," (%3d,%3d,%3d,%3d)", IM_PIXEL(im, i, j, ra).r,
			    IM_PIXEL(im, i, j, ra).g, IM_PIXEL(im, i, j, ra).b,
			    IM_PIXEL(im, i, j, ra).a);
		    break;
		case IM_RGB16:
		    fprintf(f," (%5d,%5d,%5d)", IM_PIXEL(im, i, j, r16).r,
			   IM_PIXEL(im, i, j, r16).g, IM_PIXEL(im, i, j, r16).b);
		    break;
		default:
		    break;
		}
		if (i % fl[im->type] == fl[im->type]-1) fprintf(f,"\n");
	    }
	    if ((i-1) % fl[im->type] != fl[im->type]-1) fprintf(f,"\n");
	}
    }
    fflush(stdout);
}




/*

NAME:

   ip_dump_roi() \- Dump information about a ROI to stdout.

PROTOTYPE:

   #include <ip/ip.h>

   void  ip_dump_roi( file, roi )
   FILE *file;
   ip_roi *roi;

DESCRIPTION:

   Dump the particulars of the region-of-interest 'roi'.

SEE ALSO:

   ip_alloc_roi()

*/

void ip_dump_roi(FILE *f, ip_roi *roi)
{
    fprintf(f,"ROI at %p:\n", roi);
    if (roi->type == ROI_RECTANGLE) {
	fprintf(f,"  type = %d [ROI_RECTANGLE]\n",roi->type);
	fprintf(f,"  origin = (%d,%d),   size = (%d,%d)\n",
	       roi->rbox.origin.x,roi->rbox.origin.y,
	       roi->rbox.size.x,roi->rbox.size.y);
    }else{
	fprintf(f,"  type = %d [Unrecognised!]\n",roi->type);
    }
}




/*

NAME:

   ip_dump_stats() \- Dump stats structure to stdout

PROTOTYPE:

   #include <ip/ip.h>

   void  ip_dump_stats( file, st )
   FILE *file;
   ip_stats *st;

DESCRIPTION:

   Dump details of stats structure 'st' to stdout.

SEE ALSO:

   ip_alloc_stats()        im_stats()    im_extrema()

*/


void ip_dump_stats(FILE *f, ip_stats *st)
{
    if (type_complex(st->type))
	fprintf(f,"Image stats (magnitude part):\n");
    else if (type_clrim(st->type))
	fprintf(f,"Image stats (intensity part):\n");
    else
	fprintf(f,"Image stats:\n");

    fprintf(f,"  min = %g at (%d,%d),  max = %g at (%d,%d)\n",
	    st->min, st->minpt.x, st->minpt.y, st->max, st->maxpt.x, st->maxpt.y);
    fprintf(f,"  sum = %g,  rms = %g,  mean = %g,  stddev = %g\n",
	    st->sum, st->rms, st->mean, st->stddev);

    if (st->infinities)
	fprintf(f,"  WARNING: image contains %d infinites or NANs.\n", st->infinities);

    if (type_complex(st->type)) {

	fprintf(f,"Image stats (real part):\n");
	fprintf(f,"  min = %g at (%d,%d),  max = %g at (%d,%d)\n",
		st->cmin.r, st->cminpt_r.x, st->cminpt_r.y,
		st->cmax.r, st->cmaxpt_r.x, st->cmaxpt_r.y);
	fprintf(f,"  sum = %g,  rms = %g,  mean = %g,  stddev = %g\n",
		st->csum.r, st->crms.r, st->cmean.r, st->cstddev.r);

	fprintf(f,"Image stats (imag part):\n");
	fprintf(f,"  min = %g at (%d,%d),  max = %g at (%d,%d)\n",
		st->cmin.i, st->cminpt_i.x, st->cminpt_i.y,
		st->cmax.i, st->cmaxpt_i.x, st->cmaxpt_i.y);
	fprintf(f,"  sum = %g,  rms = %g,  mean = %g,  stddev = %g\n",
		st->csum.i, st->crms.i, st->cmean.i, st->cstddev.i);

    }else if (type_clrim(st->type)) {

	fprintf(f," Red:       min at (%d,%d) = %g, max at (%d,%d) = %g.\n",
		st->rminpt_r.x, st->rminpt_r.y, st->rmin.r,
		st->rmaxpt_r.x, st->rmaxpt_r.y, st->rmax.r);
	fprintf(f," Green:     min at (%d,%d) = %g, max at (%d,%d) = %g.\n",
		st->rminpt_g.x, st->rminpt_g.y, st->rmin.g,
		st->rmaxpt_g.x, st->rmaxpt_g.y, st->rmax.g);
	fprintf(f," Blue:      min at (%d,%d) = %g, max at (%d,%d) = %g.\n",
		st->rminpt_b.x, st->rminpt_b.y, st->rmin.b,
		st->rmaxpt_b.x, st->rmaxpt_b.y, st->rmax.b);
	fprintf(f," Sum:    (%g, %g, %g)\n",
		st->rsum.r, st->rsum.g, st->rsum.b);
	fprintf(f," Mean:      (%g, %g, %g)\n",
	       st->rmean.r, st->rmean.g, st->rmean.b);
	fprintf(f," Stddev:    (%g, %g, %g)\n",
	       st->rstddev.r, st->rstddev.g, st->rstddev.b);
	fprintf(f," RMS:       (%g, %g, %g)\n",
		st->rrms.r, st->rrms.g, st->rrms.b);
	if (st->type == IM_PALETTE) {
	    fprintf(f," Pen:       min = %d,  max = %d\n", st->penmin, st->penmax);
	}
    }
}



/*

NAME:

   ip_dump_hist() \- Dump histogram object to stdout

PROTOTYPE:

   #include <ip/ip.h>

   void  ip_dump_hist( file, h, flag )
   FILE *file;
   ip_hist *h;
   int flag;

DESCRIPTION:

   Dump data in histogram to stdout.  If 'flag' set to FLG_VERBOSE
   then prints out all histogram data, otherwise just gives histogram
   number of bins and range, but not counts in the bins.

SEE ALSO:

   ip_alloc_hist()     im_hist()

*/

static const char *htype_string(int htype)
{
    static const char *strs[] = {
	"HIST_COMPONENTS", "HIST_MAGPHASE", "HIST_INTENSITY", "HIST_LAYERED", "UNKNOWN"
    };

    if (htype < 0 || htype > 3)
	htype = 4;

    return strs[htype];
}


static void pretty_print_hist_part(FILE *f, ip_hist *h, int p)
{
    double width;
    ip_hist_part *hp = &h->hp[p];

    fprintf(f, "  Low bin = %g,  high bin = %g,  num bins = %d,  width = %g",
	   hp->low, hp->high, hp->numbins, hp->width);
    width = (hp->high - hp->low) / (double)hp->numbins;
    if (hp->width != width)
	fprintf(f, "  ... But width should be %g!", width);
    if (h->htype == HIST_LAYERED)
	fprintf(f, "\n  Number of bins in layer: %d", hp->layer_numbins);
    fprintf(f,"\n  Pixels under = %d,   pixels over = %d,   NaNs = %d\n",
	    hp->under, hp->over, hp->nan);
    fprintf(f, "  Total counts in valid bins = %ld.", ip_hist_total_count(h, p));
    /* TBD: ip_hist_entropy() not implemented for LAYERED histograms */
    if (h->htype != HIST_LAYERED)
	fprintf(f, "  Entropy = %g.\n", ip_hist_entropy(h, p));
    else
	fprintf(f, "\n");

    if (hp->numbins < 72) {
	int max = 0;
	for (int j=0; j<hp->numbins; j++) {
	    if (hp->bins[j] > max)
		max = hp->bins[j];
	}

	for (int j=23; j>=1; j--) {
	    int thresh = (j*max) / 24;
	    fprintf(f, "   |");
	    for (int i=0; i<hp->numbins; i++) {
		if (hp->bins[i] > thresh)
		    fputc('*', f);
		else
		    fputc(' ', f);
	    }
	    fputc('\n', f);
	}
	fprintf(f, "    ");
	for (int i=0; i<hp->numbins; i++)
	    fputc('-', f);
	fputc('\n', f);
    }else{
	/* TBD: Need to implement verbose print of layered histogram */
	if (h->htype != HIST_LAYERED) {
	    int64_t sum = 0;
	    for (int i=0; i<hp->numbins; i++) {
		sum += hp->bins[i];
		fprintf(f,"%3d: [%g - %g) [%" PRId64 "] %d\n", i,
			hp->low+i*width, hp->low+(i+1)*width, sum, hp->bins[i]);
	    }
	}
    }
}



void ip_dump_hist(FILE *f, ip_hist *h, int flag)
{
    fprintf(f,"Hist at %p:\n",h);
    fprintf(f,"  Type %d [%s] performed on image type: %s\n",
	    h->htype, htype_string(h->htype),
	    h->imtype >= 0 ? ip_type_name(h->imtype) : "[Uninitialised]");
    fprintf(f,"  Number of histogram parts: %d\n", h->num_parts);
    if ((h->htype >= 0) && (flag & DBG_VERBOSE)) {
	for (int j=0; j<h->num_parts; j++)
	    pretty_print_hist_part(f, h, j);
    }
}
