/*

   Module: ip/filter.c
   Author: M. J. Cree

   Copyright (C) 1995-1997, 1999, 2013-2018 Michael J. Cree

   Linear filtering operations on images.

ENTRY POINTS:

   ip_alloc_kernel()
   ip_free_kernel()
   ip_kernel_valid()
   ip_kernel_dump()
   im_filter()
   im_clear_border()

*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <limits.h>

#include <ip/ip.h>
#include <ip/filter.h>

#include "internal.h"
#include "filterint.h"
#include "utils.h"

#ifdef HAVE_SIMD
#include "simd/filter_simd.h"
#endif

/*
 * Predefined kernels:
 *
 * First are 1D line differencing kernels.
 */

static const double kernel_forward_diff[2] = {-1, 1};
static const double kernel_centre_diff[3] = {-1, 0, 1};
static const double kernel_laplace_diff[3] = {1, -2, 1};


/*
 * Separable 3x3 kernels.
 */

static const double kernel_h_sobel[3] = {1, 2, 1};
static const double kernel_v_sobel[3] = {-1, 0, 1};

static const double kernel_h_prewitt[3] = {1, 1, 1};
static const double kernel_v_prewitt[3] = {-1, 0, 1};


/*
 * ip_alloc_tile()
 *
 * Allocate a tile so that we can filter part of an image in one block.  The
 * idea is to choose the tile size so that the block of memory can fit
 * entirely in L1 (or L2 at worst) cache.
 *
 * Args:
 *   im   - image that will be processed with this tile.
 *   size - 2d size of the tile wanted.
 *   flag
 *        - FLG_ROWBUF if want one row of working memory.
 *        - FLG_TILEBUF if want one tile of working memory.
 *
 * The row or tile buffer (working memory) is allocated at LONG type
 * (i.e. 32bit signed pixels) if processing a (U)BYTE or (U)SHORT image, to
 * ensure no loss of precision in calculations.  Otherwise the tile is
 * allocated in the type of the image.
 */

static ip_tile *ip_alloc_tile(ip_image *im, ip_coord2d size, int flag)
{
    ip_tile *tile;

    ip_log_routine(__func__);

    if ((tile = ip_malloc(sizeof(*tile))) == NULL)
	return NULL;

    tile->origin = (ip_coord2d){0, 0};
    tile->size = size;
    tile->tbuf = NULL;

    if (flag) {
	ip_coord2d bufsize;
	int tiletype;

	if (flag == TILE_ROWBUF) {
	    /* Row buffer big enough to hold either a column or a row */
	    int maxsize = MAX(im->size.x, im->size.y);
	    tiletype = im->type;
	    bufsize = (ip_coord2d){maxsize, 1};
	}else{
	    if (im->type == IM_BYTE || im->type == IM_UBYTE
		|| im->type == IM_SHORT || im->type == IM_USHORT)
		tiletype = IM_LONG;
	    else
		tiletype = im->type;
	    bufsize = size;
	}

	if ((tile->tbuf = ip_alloc_image(tiletype, bufsize)) == NULL) {
	    ip_free(tile);
	    return NULL;
	}

	if (flag == TILE_ROWBUF)
	    memcpy(IM_ROWADR(tile->tbuf, 0, v), IM_ROWADR(im, 0, v), im->row_size);
    }

    ip_pop_routine();
    return tile;
}


/*
 * ip_free_tile()
 *
 * Free all resources associated with a previously allocated tile.
 */

static void ip_free_tile(ip_tile *tile)
{
    ip_log_routine(__func__);

    if (tile) {
	if (tile->tbuf) ip_free_image(tile->tbuf);
	ip_free(tile);
    }

    ip_pop_routine();
}


static ip_coord2d tile_choose_good_size(ip_image *im, ip_kernel *kern)
{
    ip_coord2d tsize;
    int rowsize, nrows, reqrows;

    /* Minimum number of rows required in tile */

    reqrows = kern->size.y + 3;
    if (reqrows > im->size.y)
	reqrows = im->size.y;

    /*
     * There is one image row and one tile row required to be in cache per
     * row of image processed.  For (U)BYTE and (U)SHORT images the tile
     * rows are maintained at int precision, thus use more memory.
     */

    if (ip_type_size(im->type) == 1)
	rowsize = 5*im->row_size;
    else if (ip_type_size(im->type) == 2)
	rowsize = 3*im->row_size;
    else
	rowsize = 2*im->row_size;

    /* Find out how many of these rows fit into cache */

    nrows = ip_l1_cache_size / rowsize;
    if (nrows < (reqrows+1)) {
	nrows = ip_l2_cache_size / rowsize;
    }
    if (nrows < (reqrows+1))
	nrows = reqrows;

    if (nrows > im->size.y)
	nrows = im->size.y;

    tsize.y = nrows;
    tsize.x = im->size.x;

    return tsize;
}



/*
 * init_kernel_hfilter()
 * init_kernel_vfilter()
 *
 * Helper functions to initialise 1d horizontal and vertical kernels.  Also
 * used to initialise both horizontal and vertical parts of separable
 * kernels.
 */

static int init_kernel_hfilter(ip_kernel *k, int length, const double *coeffs)
{
    k->type = KERNEL_HORIZONTAL;
    k->size = (ip_coord2d){length, 1};
    k->hfilter = coeffs;

    return 1;
}


static int init_kernel_vfilter(ip_kernel *k, int length, const double *coeffs)
{
    k->type = KERNEL_VERTICAL;
    k->size = (ip_coord2d){1, length};
    k->vfilter = coeffs;

    return 1;
}


/*
 * finish_kernel()
 *
 * Finish up allocation of a kernel.  In particular ensure the k->filter
 * coefficients are all allocated whatever the kernel type.  If the kernels
 * are 1d or separable these coefficients are not actually used in this
 * module, however the test code in the test suite uses these coeffs in the
 * baseline reference implementation.
 */

static int finish_kernel(ip_kernel *k)
{
    double *coeffs;
    int ktype = k->type & 0xff;

    if (ktype  == KERNEL_HORIZONTAL) {
	k->filter = k->hfilter;
    }else if (ktype == KERNEL_VERTICAL) {
	k->filter = k->vfilter;
    }else if (ktype == KERNEL_SEPARABLE) {
	if ((coeffs = ip_malloc(sizeof(double) * k->size.x * k->size.y)) == NULL)
	    return 0;
	k->type |= KERNEL_OWN_KERNEL;
	for (int j=0; j<k->size.y; j++)
	    for (int i=0; i<k->size.x; i++)
		coeffs[j*k->size.x+i] = k->vfilter[j] * k->hfilter[i];
	k->filter = coeffs;
    }

    return 1;
}


/*
 * init_kernel_1d()
 *
 * Initialise a 1d vertical or horizontal kernel.
 *
 * Args:
 *  k      - the allocated but incompletely initialised kernel
 *  gtype  - General type of kernel (vertical/horizontal)
 *  length - Length of 1d kernel (and number of coeffs provided).
 *  coeffs - The coefficients of the kernel.
 */

static int init_kernel_1d(ip_kernel *k, int gtype, int length, const double *coeffs)
{
    if (gtype == IP_KERNEL_VERTICAL) {
	init_kernel_vfilter(k, length, coeffs);
    }else if (gtype == IP_KERNEL_HORIZONTAL) {
	init_kernel_hfilter(k, length, coeffs);
    }else{
	ip_log_error(ERR_PARM_BAD);
	ip_pop_routine();
	return 0;
    }
    return 1;
}


/*
 * init_kernel_sep3x3()
 *
 * Initialise a 3x3 2D kernel that can be separated into two 1D kernel, one
 * in the horizontal direction and one in the vertical direction.
 *
 * Args:
 *  k      - the allocated but imcompletely initialised kernel
 *  type   - the type of kernel (Sobel, Prewitt, etc.)
 *  gtype  - the general type (vertical, horizontal).
 */

static int init_kernel_sep3x3(ip_kernel *k, int type, int gtype)
{
    int bad = 0;
    const double *hfilter, *vfilter;
    if (type == IP_KERNEL_SOBEL) {
	if (gtype == IP_KERNEL_VERTICAL) {
	    hfilter = kernel_h_sobel;
	    vfilter = kernel_v_sobel;
	}else if (gtype == IP_KERNEL_HORIZONTAL) {
	    hfilter = kernel_v_sobel;
	    vfilter = kernel_h_sobel;
	}else
	    bad = 1;
    }else if (type == IP_KERNEL_PREWITT) {
	if (gtype == IP_KERNEL_VERTICAL) {
	    hfilter = kernel_h_prewitt;
	    vfilter = kernel_v_prewitt;
	}else if (gtype == IP_KERNEL_HORIZONTAL) {
	    hfilter = kernel_v_prewitt;
	    vfilter = kernel_h_prewitt;
	}else
	    bad = 1;
    }
    if (bad) {
	ip_log_error(ERR_PARM_BAD);
	ip_pop_routine();
	return 0;
    }

    init_kernel_hfilter(k, 3, hfilter);
    init_kernel_vfilter(k, 3, vfilter);
    k->size = (ip_coord2d){3,3};
    k->type = KERNEL_SEPARABLE;

    return 1;
}



static int init_kernel_mean(ip_kernel *k, int Xsize, int Ysize)
{
    size_t numcoeffs;

    if (NOT ip_parm_inrange("Xsize", Xsize, 1, 1000000))
	return 0;

    if (NOT ip_parm_inrange("Ysize", Ysize, 1, 1000000))
	return 0;

    numcoeffs = (size_t)Xsize * (size_t)Ysize;
    if (numcoeffs > INT_MAX) {
	ip_log_error(ERR_PARM_BAD);
	ip_pop_routine();
	return 0;
    }

    k->type = KERNEL_MEAN;
    k->size = (ip_coord2d){Xsize, Ysize};

    return 1;
}


/*

NAME:

   ip_alloc_kernel()

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/filter.h>

   ip_kernel * ip_alloc_kernel( type, ... )
   int type;

DESCRIPTION:

   Allocates a kernel for linear filtering an image.  Set `type' to be the
   required kernel type.  The types are enumerated below.

   1D Linear filters.  The kernel type must be ORed with either
   IP_KERNEL_HORIZONTAL or IP_KERNEL_VERTICAL.  For some this chooses the
   direction of the filter and for others it switches an otherwise 2D filter
   to be specifically a linear 1D filter.

\& IP_KERNEL_FORWARD_DIFF	1D 2-pt forward difference (1st derivative).
				Can be vertical or horizontal.

\& IP_KERNEL_BACKAWARD_DIFF	1D 2-pt backward difference (1st derivative).
				Can be vertical or horizontal.

\& IP_KERNEL_CENTRE_DIFF	1D 3-pt centre difference (1st derivative).
				Can be vertical or horizontal.

\& IP_KERNEL_LAPLACE_DIFF	1D 3-pt Laplace difference (2nd derivative).
				Can be vertical or horizontal.

\& IP_KERNEL_SOBEL		The 3x3 Sobel approximation to the first
				derivative.  Can be vertical or horizontal.

\& IP_KERNEL_PREWITT		The 3x3 Prewitt approximation to the first
				derivative.  Can be vertical or horizontal.

\& IP_KERNEL_MEAN		The 1D mean filter.  Takes one argument, an
				int specifying the length of the kernel.

\& IP_KERNEL_GAUSSIAN		The 1D Gaussian (normal) filter.  Takes two
				arguments, an int specifying the length of the
				kernel, a double specifying the height of the
				Gaussian and a double specifying the standard
				deviation in pixels of the Gaussian.

   OR 1D kernel types with IP_KERNEL_VERTICAL or IP_KERNEL_HORIZONTAL to get
   a vertical or horizontally orientated kernel.  Note that vertical means
   that the derivative is calculated in the vertical direction (which is the
   convention for differencing but not for edge detection) thus a vertical
   Sobel operator detects horizontal edges.

   2D filters:

\& IP_KERNEL_MEAN		The 2D MxN mean filter.  Takes two arguments,
				both ints, the first is M (x-size) and the
				second is N (y-size), together specifying the
				size of the kernel.

\& IP_KERNEL_GAUSSIAN		The circularly symmetric 2D Gaussian filter.
				Takes three arguments, an int M specifying
				a square kernel of MxM pixels, a double
				specifying the height of the Gaussian and a
				double specifying the standard deviation in
				pixels of the Gaussian.

ERRORS:

   ERR_PARM_BAD
   ERR_NOT_IMPLEMENTED
   ERR_OUT_OF_MEM

SEE ALSO:

   ip_free_kernel()               ip_kernel_dump()
   im_filter()

*/

ip_kernel *ip_alloc_kernel(int type, ...)
{
    va_list ap;
    ip_kernel *kernel;
    int gtype;
    int iarg1, iarg2;
    /*     double darg1, darg2; */

    ip_log_routine(__func__);

    if ((kernel = ip_malloc(sizeof(*kernel))) == NULL)
	return NULL;
    kernel->hfilter = NULL;
    kernel->vfilter = NULL;
    kernel->filter = NULL;
    kernel->reqtype = type;

    va_start(ap, type);

    gtype = type & IP_KERNEL_GTYPE_MASK;
    type &= ~IP_KERNEL_GTYPE_MASK;

    switch (type) {
    case IP_KERNEL_FORWARD_DIFF:
    case IP_KERNEL_BACKWARD_DIFF:
	if (NOT init_kernel_1d(kernel, gtype, 2, kernel_forward_diff))
	    goto iak_error;
	if (type == IP_KERNEL_BACKWARD_DIFF)
	    kernel->type |= KERNEL_BACKWARDS;
	break;

    case IP_KERNEL_CENTRE_DIFF:
	if (NOT init_kernel_1d(kernel, gtype, 3, kernel_centre_diff))
	    goto iak_error;
	break;

    case IP_KERNEL_LAPLACE_DIFF:
	if (NOT init_kernel_1d(kernel, gtype, 3, kernel_laplace_diff))
	    goto iak_error;
	break;

    case IP_KERNEL_SOBEL:
    case IP_KERNEL_PREWITT:
	if (NOT init_kernel_sep3x3(kernel, type, gtype))
	    goto iak_error;
	break;

    case IP_KERNEL_MEAN:
	if (gtype & IP_KERNEL_VERTICAL) {
	    iarg1 = 1;
	    iarg2 = va_arg(ap, int);
	}else if (gtype & IP_KERNEL_HORIZONTAL) {
	    iarg1 = va_arg(ap, int);
	    iarg2 = 1;
	}else{
	    iarg1 = va_arg(ap, int);
	    iarg2 = va_arg(ap, int);
	}
	if (NOT init_kernel_mean(kernel, iarg1, iarg2))
	    goto iak_error;
	break;

    default:
	ip_log_error(ERR_NOT_IMPLEMENTED);
	ip_pop_routine();
	goto iak_error;
    }

    if (NOT finish_kernel(kernel))
	goto iak_error;

    va_end(ap);
    ip_pop_routine();
    return kernel;

 iak_error:
    va_end(ap);
    ip_free(kernel);
    return NULL;
}




/*

NAME:

   ip_free_kernel()

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/filter.h>

   int  ip_free_kernel( kern )
   ip_kernel *kern;

DESCRIPTION:

   Release all resources associated with a previously allocated kernel.

ERRORS:

   ERR_BAD_MEM_FREE

SEE ALSO:

   ip_alloc_kernel()

*/

int ip_free_kernel(ip_kernel *kern)
{
    ip_log_routine(__func__);

    if (kern) {
	if (kern->type & KERNEL_OWN_KERNEL)
	    ip_free((void *)kern->filter);
	ip_free(kern);
    }

    ip_pop_routine();
    return ip_error;
}



/*

NAME:

   ip_kernel_valid()

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/filter.h>

   int  ip_kernel_valid( kern )
   ip_kernel *kern;

DESCRIPTION:

   Check if the kernel mets some basic validation checks.  If it doesn't
   flags ERR_INVALID_KERNEL, pops the top routine off the routine stack, and
   returns FALSE.  Mainly intended for internal library use.

ERRORS:

   ERR_INVALID_KERNEL

*/


int ip_kernel_valid(ip_kernel *kern)
{
    int valid = 1;
    if (kern) {
	/* We check type is valid */
	int ktype = kern->type & 0xff;
	if (ktype < 1 || ktype > KERNEL_MAX_VALID_TYPE)
	    valid = 0;
	if (ktype & ~(KERNEL_BACKWARDS | KERNEL_OWN_KERNEL | 0xff))
	    valid = 0;
	if (kern->size.x < 1 || kern->size.y < 1)
	    valid = 0;
    }else{
	/* NULL kern pointer ! */
	valid = 0;
    }

    if (NOT valid) {
	ip_log_error(ERR_INVALID_KERNEL);
	ip_pop_routine();
    }

    return valid;
}



/*

NAME:

   ip_kernel_dump()

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/filter.h>

   void  ip_kernel_dump( f, kern )
   FILE *f;
   ip_kernel *kern;

DESCRIPTION:

   Dumps to the file stream `f' a text representation of the kernel `kern'.
   Primarily intended for debugging purposes.

SEE ALSO:

   ip_alloc_kernel()

*/

void ip_kernel_dump(FILE *f, ip_kernel *kern)
{
    char *types[] = {
	[KERNEL_HORIZONTAL] = "Horizontal",
	[KERNEL_VERTICAL] = "Vertical",
	[KERNEL_SEPARABLE] = "Separable",
	[KERNEL_2D] = "2D"
    };
    char *reqtypes[] = {
	[IP_KERNEL_FORWARD_DIFF] = "Forward difference",
	[IP_KERNEL_BACKWARD_DIFF] = "Backward difference",
	[IP_KERNEL_CENTRE_DIFF] = "Centre difference",
	[IP_KERNEL_LAPLACE_DIFF] = "Laplace difference",
	[IP_KERNEL_MEAN] = "Mean",
	[IP_KERNEL_GAUSSIAN] = "Gaussian",
	[7] = "invalid",
	[8] = "invalid",
	[9] = "invalid",
	[IP_KERNEL_PREWITT] = "Prewitt",
	[IP_KERNEL_SOBEL] = "Sobel",
	[IP_KERNEL_LAPLACIAN] = "Laplacian",
	[IP_KERNEL_LOG] = "LoG"
    };
    int req_type = kern->reqtype & 0xff;
    int req_gtype = kern->reqtype & IP_KERNEL_GTYPE_MASK;
    int type = kern->type & 0xff;

    fprintf(f,"Kernel at %p:\n", kern);
    if (type >= KERNEL_HORIZONTAL && type <= KERNEL_2D)
	fprintf(f,"  type: %d [%s", kern->type, types[type]);
    else
	fprintf(f,"  type: %d [invalid", kern->type);
    if (kern->type & KERNEL_BACKWARDS)
	fprintf(f,"|Backwards");
    if (kern->type & KERNEL_OWN_KERNEL)
	fprintf(f,"|OwnKernel");
    fprintf(f,"]\n");
    if (req_type >= IP_KERNEL_FORWARD_DIFF && req_type <= IP_KERNEL_LOG)
	fprintf(f,"  reqtype: %d [%s", kern->reqtype, reqtypes[req_type]);
    else
	fprintf(f,"  reqtype: %d [invalid", kern->reqtype);
    if (req_gtype != 0) {
	if (req_gtype & IP_KERNEL_VERTICAL)
	    fprintf(f,"|Vertical");
	if (req_gtype & IP_KERNEL_HORIZONTAL)
	    fprintf(f,"|Horizontal");
	if (req_gtype & IP_KERNEL_2D)
	    fprintf(f,"|2D");
    }
    fprintf(f,"]\n");
    fprintf(f,"  size: (%d,%d)\n", kern->size.x, kern->size.y);
    if (kern->hfilter) {
	fprintf(f,"  hfilter:");
	for (int j=0; j<kern->size.x; j++)
	    fprintf(f," %g", kern->hfilter[j]);
	fprintf(f,"\n");
    }
    if (kern->vfilter) {
	fprintf(f,"  vfilter:");
	for (int j=0; j<kern->size.y; j++)
	    fprintf(f," %g", kern->vfilter[j]);
	fprintf(f,"\n");
    }
    if (kern->filter) {
	fprintf(f,"  filter:");
	for (int j=0; j<kern->size.y; j++) {
	    for (int i=0; i<kern->size.x; i++) {
		fprintf(f," %g", kern->filter[j*kern->size.x+i]);
	    }
	    fprintf(f,"\n         ");
	}
	fprintf(f,"\n");
    }else{
	fprintf(f, "  filter: <NULL>\n");
    }
}



/*
 * In the filter macro definitions below tt is the type of the image to be
 * processed and intt is the internal type to use for calculations.  This
 * feature is used to enable UBYTE images to be processed at int precision
 * before returning a UBYTE result, for example.
 */


/*
 * Horizontal line filtering with 2-pt kernel (mainly for differencing)
 * Implements both forward differencing and backwards differencing.
 *
 * Converting the coefficients of the kernel to the required integer type
 * needs care particularly for the ULONG case because the coefficients can
 * be negative, in which case we want their two's complement representation.
 * On some 32-bit arches special CPU instructions to convert double to
 * uint32_t clip negative numbers to zero defeating our desire, hence the
 * need to convert via int64_t first.
 */

#define int_type int

#define generate_filter_horiz2_inplace(tt, intt, coeffconv)		\
    static void filter_horiz2_inplace_##tt(ip_image *im, ip_kernel *kern, ip_tile *tile) \
    {									\
	intt##_type c1, c2;						\
	c1 = (coeffconv)kern->hfilter[0];				\
	c2 = (coeffconv)kern->hfilter[1];				\
	for (int j=tile->origin.y; j<tile->size.y; j++) {		\
	    tt##_type *impix = IM_ROWADR(im, j, v);			\
	    if (kern->type & KERNEL_BACKWARDS) {			\
		intt##_type this, prev;					\
		prev = (intt##_type)impix[0];				\
		for (int i=0; i<im->size.x; i++) {			\
		    this = (intt##_type)impix[i];			\
		    impix[i] = (tt##_type)(c1*prev + c2*this);		\
		    prev = this;					\
		}							\
	    }else{							\
		intt##_type this, next;					\
		next = 0; /* STFCU */					\
		this = (intt##_type)impix[0];				\
		for (int i=0; i<im->size.x-1; i++) {			\
		    next = impix[i+1];					\
		    impix[i] = c1*(intt##_type)this + c2*(intt##_type)next; \
		    this = next;					\
		}							\
		impix[im->size.x-1] = c1*(intt##_type)this + c2*(intt##_type)next; \
	    }								\
	}								\
    }


generate_filter_horiz2_inplace(UBYTE, int, int)
generate_filter_horiz2_inplace(BYTE, int, int)
generate_filter_horiz2_inplace(USHORT, int, int)
generate_filter_horiz2_inplace(SHORT, int, int)
generate_filter_horiz2_inplace(LONG, LONG, int32_t)
generate_filter_horiz2_inplace(ULONG, ULONG, int64_t)
generate_filter_horiz2_inplace(FLOAT, FLOAT, float)
generate_filter_horiz2_inplace(DOUBLE, DOUBLE, double)

static improc_function_Ikt filter_horiz2_inplace_bytype[IM_TYPEMAX] = {
    [IM_UBYTE] = filter_horiz2_inplace_UBYTE,
    [IM_BYTE] = filter_horiz2_inplace_BYTE,
    [IM_USHORT] = filter_horiz2_inplace_USHORT,
    [IM_SHORT] = filter_horiz2_inplace_SHORT,
    [IM_ULONG] = filter_horiz2_inplace_ULONG,
    [IM_LONG] = filter_horiz2_inplace_LONG,
    [IM_FLOAT] = filter_horiz2_inplace_FLOAT,
    [IM_DOUBLE] = filter_horiz2_inplace_DOUBLE,
};


/*
 * Horizontal line filtering with a 3-pt kernel (mainly for centre diffs,
 * and laplace diffs).
 */

#define generate_filter_horiz3_inplace(tt, intt, coeffconv)		\
    static void filter_horiz3_inplace_##tt(ip_image *im, ip_kernel *kern, ip_tile *tile) \
    {									\
	intt##_type c1 = (coeffconv)kern->hfilter[0];			\
	intt##_type c2 = (coeffconv)kern->hfilter[1];			\
	intt##_type c3 = (coeffconv)kern->hfilter[2];			\
	for (int j=tile->origin.y; j<tile->size.y; j++) {		\
	    tt##_type *impix = IM_ROWADR(im, j, v);			\
	    intt##_type next, this, prev;				\
	    next = 0; /* STFCU */					\
	    prev = this = (intt##_type)impix[0];			\
	    for (int i=0; i<im->size.x-1; i++) {			\
		next = (intt##_type)impix[i+1];				\
		impix[i] = (tt##_type)(c1*prev + c2*this + c3*next);	\
		prev = this;						\
		this = next;						\
	    }								\
	    impix[im->size.x-1] = (tt##_type)(c1*prev + c2*this + c3*next); \
	}								\
    }


generate_filter_horiz3_inplace(UBYTE, int, int)
generate_filter_horiz3_inplace(BYTE, int, int)
generate_filter_horiz3_inplace(USHORT, int, int)
generate_filter_horiz3_inplace(SHORT, int, int)
generate_filter_horiz3_inplace(LONG, LONG, int32_t)
generate_filter_horiz3_inplace(ULONG, ULONG, int64_t)
generate_filter_horiz3_inplace(FLOAT, FLOAT, float)
generate_filter_horiz3_inplace(DOUBLE, DOUBLE, double)

static improc_function_Ikt filter_horiz3_inplace_bytype[IM_TYPEMAX] = {
    [IM_UBYTE] = filter_horiz3_inplace_UBYTE,
    [IM_BYTE] = filter_horiz3_inplace_BYTE,
    [IM_USHORT] = filter_horiz3_inplace_USHORT,
    [IM_SHORT] = filter_horiz3_inplace_SHORT,
    [IM_ULONG] = filter_horiz3_inplace_ULONG,
    [IM_LONG] = filter_horiz3_inplace_LONG,
    [IM_FLOAT] = filter_horiz3_inplace_FLOAT,
    [IM_DOUBLE] = filter_horiz3_inplace_DOUBLE,
};


/*
 * Vertical line filtering with 2-pt kernel (mainly for differencing)
 * Implements both forward differencing and backwards differencing.
 */

#define generate_filter_vert2_inplace(tt, intt, coeffconv)		\
    static void filter_vert2_inplace_##tt(ip_image *im, ip_kernel *kern, ip_tile *tile)	\
    {									\
	intt##_type c1 = (coeffconv)kern->vfilter[0];			\
	intt##_type c2 = (coeffconv)kern->vfilter[1];			\
	if (kern->type & KERNEL_BACKWARDS) {				\
	    tt##_type *up, *down;					\
	    for (int j=(tile->size.y+tile->origin.y)-1; j>=tile->origin.y; j--) { \
		down = IM_ROWADR(im, j, v);				\
		if ((j-1) >= 0)						\
		    up = IM_ROWADR(im, j-1, v);				\
		else							\
		    up = down;						\
		for (int i=0; i<im->size.x; i++) {			\
		    down[i] = c1*(intt##_type)up[i] + c2*(intt##_type)down[i]; \
		}							\
	    }								\
	}else{								\
	    tt##_type *up, *down;					\
	    for (int j=tile->origin.y; j<tile->size.y; j++) {		\
		up = IM_ROWADR(im, j, v);				\
		if ((j+1) < im->size.y)					\
		    down = IM_ROWADR(im, j+1, v);			\
		else							\
		    down = up;						\
		for (int i=0; i<im->size.x; i++) {			\
		    up[i] = c1*(intt##_type)up[i] + c2*(intt##_type)down[i]; \
		}							\
	    }								\
	}								\
    }


generate_filter_vert2_inplace(UBYTE, int, int)
generate_filter_vert2_inplace(BYTE, int, int)
generate_filter_vert2_inplace(USHORT, int, int)
generate_filter_vert2_inplace(SHORT, int, int)
generate_filter_vert2_inplace(LONG, LONG, int32_t)
generate_filter_vert2_inplace(ULONG, ULONG, int64_t)
generate_filter_vert2_inplace(FLOAT, FLOAT, float)
generate_filter_vert2_inplace(DOUBLE, DOUBLE, double)


static improc_function_Ikt filter_vert2_inplace_bytype[IM_TYPEMAX] = {
    [IM_UBYTE] = filter_vert2_inplace_UBYTE,
    [IM_BYTE] = filter_vert2_inplace_BYTE,
    [IM_USHORT] = filter_vert2_inplace_USHORT,
    [IM_SHORT] = filter_vert2_inplace_SHORT,
    [IM_ULONG] = filter_vert2_inplace_ULONG,
    [IM_LONG] = filter_vert2_inplace_LONG,
    [IM_FLOAT] = filter_vert2_inplace_FLOAT,
    [IM_DOUBLE] = filter_vert2_inplace_DOUBLE,
};



/*
 * Vertical line filtering with a 3-pt kernel (mainly for centre diffs,
 * and laplace diffs).
 */

#define generate_filter_vert3_inplace(tt, inttt, coeffconv)		\
    static void filter_vert3_inplace_##tt(ip_image *im, ip_kernel *kern, ip_tile *tile)	\
    {									\
	tt##_type *this = IM_ROWADR(im, tile->origin.y, v);		\
	tt##_type *prev = IM_ROWADR(tile->tbuf, 0, v);			\
	inttt##_type c1 = (coeffconv)kern->vfilter[0];			\
	inttt##_type c2 = (coeffconv)kern->vfilter[1];			\
	inttt##_type c3 = (coeffconv)kern->vfilter[2];			\
	for (int j=tile->origin.y; j<tile->size.y; j++) {		\
	    tt##_type *next;						\
	    if (j<im->size.y-1)						\
		next = IM_ROWADR(im, j+1, v);				\
	    else							\
		next = this;						\
	    for (int i=0; i<im->size.x; i++) {				\
		tt##_type tval = this[i];				\
		tt##_type nval = (tt##_type)(c1*prev[i] + c2*tval + c3*next[i]); \
		prev[i] = tval;						\
		this[i] = nval;						\
	    }								\
	    this = next;						\
	}								\
    }


generate_filter_vert3_inplace(BYTE, int, int)
generate_filter_vert3_inplace(UBYTE, int, int)
generate_filter_vert3_inplace(SHORT, int, int)
generate_filter_vert3_inplace(USHORT, int, int)
generate_filter_vert3_inplace(LONG, LONG, int32_t)
generate_filter_vert3_inplace(ULONG, ULONG, int64_t)
generate_filter_vert3_inplace(FLOAT, FLOAT, float)
generate_filter_vert3_inplace(DOUBLE, DOUBLE, double)


static improc_function_Ikt filter_vert3_inplace_bytype[IM_TYPEMAX] = {
    [IM_BYTE] = filter_vert3_inplace_BYTE,
    [IM_UBYTE] = filter_vert3_inplace_UBYTE,
    [IM_SHORT] = filter_vert3_inplace_SHORT,
    [IM_USHORT] = filter_vert3_inplace_USHORT,
    [IM_LONG] = filter_vert3_inplace_LONG,
    [IM_ULONG] = filter_vert3_inplace_ULONG,
    [IM_FLOAT] = filter_vert3_inplace_FLOAT,
    [IM_DOUBLE] = filter_vert3_inplace_DOUBLE,
};



/*
 * filter_image_1d()
 *
 * Filter an image with a 1D horizontal or vertical kernel. Mainly chooses
 * which filter implemented with the above macros to use.
 */

static int filter_image_1d(ip_image *im, ip_kernel *kern)
{
    ip_tile *tile;
    improc_function_Ikt *fn = NULL;
    int completed = 0;
    int ktype = kern->type & 0xff;
    int tile_flag = 0;

    /* 1D horizontal filtering */
    if (ktype == KERNEL_HORIZONTAL) {
	if (kern->size.x == 2) {
#ifdef HAVE_SIMD
	    if (IP_USE_HWACCEL && kern->hfilter[0] == -1 && kern->hfilter[1] == 1
				&& ip_simd_filter_horiz_difference_bytype[im->type]) {
		fn = ip_simd_filter_horiz_difference_bytype;
	    }else{
		fn = filter_horiz2_inplace_bytype;
	    }
#else
	    fn = filter_horiz2_inplace_bytype;
#endif
	}else if (kern->size.x == 3) {
#ifdef HAVE_SIMD
	    if (IP_USE_HWACCEL) {
		if (kern->hfilter[0] == -1 && kern->hfilter[1] == 0 && kern->hfilter[2] == 1
				&& ip_simd_filter_horiz_centre_difference_bytype[im->type]) {
		    fn = ip_simd_filter_horiz_centre_difference_bytype;
		}else if (kern->hfilter[0] == 1 && kern->hfilter[1] == -2 && kern->hfilter[2] == 1
				&& ip_simd_filter_horiz_laplace_difference_bytype[im->type]) {
		    fn = ip_simd_filter_horiz_laplace_difference_bytype;
		}else{
		    fn = filter_horiz3_inplace_bytype;
		}
	    }else{
		fn = filter_horiz3_inplace_bytype;
	    }
#else
	    fn = filter_horiz3_inplace_bytype;
#endif
	}
    }

    /* 1D vertical filtering */
    if (ktype == KERNEL_VERTICAL) {
	if (kern->size.y == 2) {
#ifdef HAVE_SIMD
	    if (IP_USE_HWACCEL && kern->vfilter[0] == -1 && kern->vfilter[1] == 1
				&& ip_simd_filter_vert_difference_bytype[im->type]) {
		fn = ip_simd_filter_vert_difference_bytype;
	    }else{
		fn = filter_vert2_inplace_bytype;
	    }
#else
	    fn = filter_vert2_inplace_bytype;
#endif
	}else if (kern->size.y == 3) {
	    tile_flag = TILE_ROWBUF;
#ifdef HAVE_SIMD
	    if (IP_USE_HWACCEL) {
		if (kern->vfilter[0] == -1 && kern->vfilter[1] == 0 && kern->vfilter[2] == 1
				&& ip_simd_filter_vert_centre_difference_bytype[im->type]) {
		    fn = ip_simd_filter_vert_centre_difference_bytype;
		}else if (kern->vfilter[0] == 1 && kern->vfilter[1] == -2 && kern->vfilter[2] == 1
				&& ip_simd_filter_vert_laplace_difference_bytype[im->type]) {
		    fn = ip_simd_filter_vert_laplace_difference_bytype;
		}else
		    fn = filter_vert3_inplace_bytype;
	    }else{
		fn = filter_vert3_inplace_bytype;
	    }
#else
	    fn = filter_vert3_inplace_bytype;
#endif
	}
    }

    /* Filter the image if operation supported */
    if (fn && fn[im->type]) {
	if ((tile = ip_alloc_tile(im, im->size, tile_flag)) != NULL) {
	    fn[im->type](im, kern, tile);
	    ip_free_tile(tile);
	    completed = 1;
	}
    }else{
	ip_log_error(ERR_NOT_IMPLEMENTED);
    }

    return completed;
}


/*
 * Implementation of 3x3 separable filters.  By separable we mean that the
 * kernel can be split into a horizontal 3-pt filter followed by a vertical
 * 3-pt filter
 *
 * It is proposed that this is more efficient than calculating a 3x3 kernel
 * directly. It turns out that is not true for the Sobel and Prewitt filters
 * which have a number of zeros and ones in the kernel so can be simplified
 * even further.
 *
 * The processing is carried out in tiles (currently the tiles are the width
 * of the image but a fraction of the height of the image) that can fit
 * entirely within cache.  The image is processed with the horizontal filter
 * into the tile memory buffer with filter_horiz3_TYPE() then from the tile
 * memory buffer back to the image with filter_vert3_TYPE().
 *
 * The horizontal row filter fills the tile memory buffer.  The vertical
 * (column) filter then writes back to the image to one less row.  The
 * bottom two rows of the tile memory buffer are needed for processing the
 * next tile so tile_switch_row_pointers() is used to switch the bottom two
 * rows of the tile memory buffer to the top of the tile memory buffer and
 * the next horizontal tile filter skips those top two rows and starts
 * calculation from the third row of the tile.  And so on.
 */



/*
 * Horizontal 3-pixel line filter from Image to tile buffer.  Used in first
 * step (horizontal filter) to implement separable 3x3 filters.  If at top
 * of image filters every row of the tile.  If tile is not at top of image
 * starts from the third row because the bottom two rows of the previously
 * calculated tile are now the top two rows of this tile.
 */

#define generate_filter_horiz3(tt, outtt, coeffconv)			\
    static void filter_horiz3_##tt(ip_image *im, ip_kernel *kern, ip_tile *tile) \
    {									\
	int startrow;							\
	outtt##_type c1 = (coeffconv)kern->hfilter[0];			\
	outtt##_type c2 = (coeffconv)kern->hfilter[1];			\
	outtt##_type c3 = (coeffconv)kern->hfilter[2];			\
	startrow = tile->origin.y ? 2 : 0;				\
	for (int j=startrow; j<tile->size.y; j++) {			\
	    tt##_type *impix = IM_ROWADR(im, j+tile->origin.y, v);	\
	    outtt##_type *tpix = IM_ROWADR(tile->tbuf, j, v);		\
	    outtt##_type next, this, prev;				\
	    next = 0; /* STFCU */					\
	    prev = this = (outtt##_type)impix[0];			\
	    for (int i=0; i<im->size.x-1; i++) {			\
		next = (outtt##_type)impix[i+1];			\
		tpix[i] = c1*prev + c2*this + c3*next;			\
		prev = this;						\
		this = next;						\
	    }								\
	    tpix[im->size.x-1] = c1*prev + c2*this + c3*next;		\
	}								\
    }

generate_filter_horiz3(UBYTE, int, int)
generate_filter_horiz3(BYTE, int, int)
generate_filter_horiz3(USHORT, int, int)
generate_filter_horiz3(SHORT, int, int)
generate_filter_horiz3(LONG, LONG, int32_t)
generate_filter_horiz3(ULONG, ULONG, int64_t)
generate_filter_horiz3(FLOAT, FLOAT, float)
generate_filter_horiz3(DOUBLE, DOUBLE, double)

improc_function_Ikt filter_horiz3_bytype[IM_TYPEMAX] = {
    [IM_UBYTE] = filter_horiz3_UBYTE,
    [IM_BYTE] = filter_horiz3_BYTE,
    [IM_USHORT] = filter_horiz3_USHORT,
    [IM_SHORT] = filter_horiz3_SHORT,
    [IM_ULONG] = filter_horiz3_ULONG,
    [IM_LONG] = filter_horiz3_LONG,
    [IM_FLOAT] = filter_horiz3_FLOAT,
    [IM_DOUBLE] = filter_horiz3_DOUBLE,
};


/*
 * Vertical 3-pixel line filter from tile buffer to image.  Used in second
 * step (vertical filter) to implement separable 3x3 filters.
 */

#define generate_filter_vert3(tt, intt, coeffconv)			\
    static void filter_vert3_##tt(ip_image *im, ip_kernel *kern, ip_tile *tile) \
    {									\
	int start_row = tile->origin.y ? 1 : 0;				\
	intt##_type *this = IM_ROWADR(tile->tbuf, start_row, v);	\
	intt##_type *prev = IM_ROWADR(tile->tbuf, 0, v);		\
	intt##_type c1 = (coeffconv)kern->vfilter[0];			\
	intt##_type c2 = (coeffconv)kern->vfilter[1];			\
	intt##_type c3 = (coeffconv)kern->vfilter[2];			\
	for (int j=start_row; j<tile->size.y-1; j++) {			\
	    intt##_type *next = IM_ROWADR(tile->tbuf, j+1, v);		\
	    tt##_type *impix = IM_ROWADR(im, j+tile->origin.y, v);	\
	    for (int i=0; i<im->size.x; i++) {				\
		impix[i] = (tt##_type)(c1*prev[i] + c2*this[i] + c3*next[i]); \
	    }								\
	    prev = this;						\
	    this = next;						\
	}								\
	if (tile->origin.y + tile->size.y == im->size.y) {		\
	    tt##_type *impix = IM_ROWADR(im, im->size.y-1, v);		\
	    for (int i=0; i<im->size.x; i++) {				\
		impix[i] = (tt##_type)(c1*prev[i] + c2*this[i] + c3*this[i]); \
	    }								\
	}								\
    }


generate_filter_vert3(BYTE, int, int)
generate_filter_vert3(UBYTE, int, int)
generate_filter_vert3(SHORT, int, int)
generate_filter_vert3(USHORT, int, int)
generate_filter_vert3(LONG, LONG, int32_t)
generate_filter_vert3(ULONG, ULONG, int64_t)
generate_filter_vert3(FLOAT, FLOAT, float)
generate_filter_vert3(DOUBLE, DOUBLE, double)


improc_function_Ikt filter_vert3_bytype[IM_TYPEMAX] = {
    [IM_BYTE] = filter_vert3_BYTE,
    [IM_UBYTE] = filter_vert3_UBYTE,
    [IM_SHORT] = filter_vert3_SHORT,
    [IM_USHORT] = filter_vert3_USHORT,
    [IM_LONG] = filter_vert3_LONG,
    [IM_ULONG] = filter_vert3_ULONG,
    [IM_FLOAT] = filter_vert3_FLOAT,
    [IM_DOUBLE] = filter_vert3_DOUBLE,
};



/*
 * The Sobel and Prewitt filters, even though are separable 3x3 filters, can
 * be simplified even further.  This code exploits the fact that there are
 * many zeros and ones in the kernel to avoid calculations.  It is an
 * inplace filter, but does requires a one-row buffer in the tile to store
 * intermediate results.
 *
 * It is assumed that the tile covers the whole image.  Processing is by row
 * order.
 *
 * The vertical filter is over two times faster than the general 3x3
 * separable filter implementation.  The horizontal filter is not quite so
 * fast due to it requiring some extra recalculations.  These possibly could
 * be removed by processing columnwise in tiles but I can't be bothered
 * doing it.  A SIMD implementation is a better solution.
 */

#define generate_filter_vert_edge3x3(tt, inttt, coeffconv)		\
    static void filter_vert_edge3x3_##tt(ip_image *im, ip_kernel *kern, ip_tile *tile) \
    {									\
	tt##_type *this = IM_ROWADR(im, tile->origin.y, v);		\
	tt##_type *prev = IM_ROWADR(tile->tbuf, 0, v);			\
	inttt##_type c = (coeffconv)kern->hfilter[1];			\
	for (int j=tile->origin.y; j<tile->size.y; j++) {		\
	    tt##_type *next;						\
	    inttt##_type pdiff, tdiff, ndiff;				\
	    if (j<im->size.y-1)						\
		next = IM_ROWADR(im, j+1, v);				\
	    else							\
		next = this;						\
	    /* Column 0 */						\
	    pdiff = (inttt##_type)next[0] - (inttt##_type)prev[0];	\
	    tdiff = (inttt##_type)next[0] - (inttt##_type)prev[0];	\
	    ndiff = (inttt##_type)next[1] - (inttt##_type)prev[1];	\
	    prev[0] = this[0];						\
	    this[0] =  pdiff + c*tdiff + ndiff;				\
	    pdiff = tdiff;						\
	    tdiff = ndiff;						\
	    /* Column 1 to N-2 */					\
	    for (int i=1; i<im->size.x-1; i++) {			\
		ndiff = (inttt##_type)next[i+1] - (inttt##_type)prev[i+1]; \
		prev[i] = this[i];					\
		this[i] = pdiff + c*tdiff + ndiff;			\
		pdiff = tdiff;						\
		tdiff = ndiff;						\
	    }								\
	    /* Column N-1 (i.e. the last one) */			\
	    prev[im->size.x-1] = this[im->size.x-1];			\
	    this[im->size.x-1] = pdiff + c*tdiff + ndiff;		\
	    this = next;						\
	}								\
    }

generate_filter_vert_edge3x3(BYTE, int, int)
generate_filter_vert_edge3x3(UBYTE, int, int)
generate_filter_vert_edge3x3(SHORT, int, int)
generate_filter_vert_edge3x3(USHORT, int, int)
generate_filter_vert_edge3x3(LONG, LONG, int32_t)
generate_filter_vert_edge3x3(ULONG, ULONG, int64_t)
generate_filter_vert_edge3x3(FLOAT, FLOAT, float)
generate_filter_vert_edge3x3(DOUBLE, DOUBLE, double)


static improc_function_Ikt filter_vert_edge3x3_bytype[IM_TYPEMAX] = {
    [IM_BYTE] = filter_vert_edge3x3_BYTE,
    [IM_UBYTE] = filter_vert_edge3x3_UBYTE,
    [IM_SHORT] = filter_vert_edge3x3_SHORT,
    [IM_USHORT] = filter_vert_edge3x3_USHORT,
    [IM_LONG] = filter_vert_edge3x3_LONG,
    [IM_ULONG] = filter_vert_edge3x3_ULONG,
    [IM_FLOAT] = filter_vert_edge3x3_FLOAT,
    [IM_DOUBLE] = filter_vert_edge3x3_DOUBLE,
};



#define generate_filter_horiz_edge3x3(tt, inttt, coeffconv)		\
    static void filter_horiz_edge3x3_##tt(ip_image *im, ip_kernel *kern, ip_tile *tile) \
    {									\
	tt##_type *this = IM_ROWADR(im, tile->origin.y, v);		\
	tt##_type *prev = IM_ROWADR(tile->tbuf, 0, v);			\
	inttt##_type c = (coeffconv)kern->vfilter[1];			\
	for (int j=tile->origin.y; j<tile->size.y; j++) {		\
	    tt##_type *next;						\
	    inttt##_type pdiff, tdiff, ndiff, pval;			\
	    if (j<im->size.y-1)						\
		next = IM_ROWADR(im, j+1, v);				\
	    else							\
		next = this;						\
	    /* Column 0 */						\
	    pdiff = (inttt##_type)prev[1] - (inttt##_type)prev[0];	\
	    tdiff = (inttt##_type)this[1] - (inttt##_type)this[0];	\
	    ndiff = (inttt##_type)next[1] - (inttt##_type)next[0];	\
	    pval =  pdiff + c*tdiff + ndiff;				\
	    /* Column 1 to N-2 */					\
	    for (int i=1; i<im->size.x-1; i++) {			\
		pdiff = (inttt##_type)prev[i+1] - (inttt##_type)prev[i-1]; \
		tdiff = (inttt##_type)this[i+1] - (inttt##_type)this[i-1]; \
		ndiff = (inttt##_type)next[i+1] - (inttt##_type)next[i-1]; \
		prev[i-1] = this[i-1];					\
		this[i-1] = pval;					\
		pval = pdiff + c*tdiff + ndiff;				\
	    }								\
	    /* Column N-1 (i.e. the last one) */			\
	    int i = im->size.x - 1;					\
	    pdiff = (inttt##_type)prev[i] - (inttt##_type)prev[i-1];	\
	    tdiff = (inttt##_type)this[i] - (inttt##_type)this[i-1];	\
	    ndiff = (inttt##_type)next[i] - (inttt##_type)next[i-1];	\
	    prev[i-1] = this[i-1];					\
	    this[i-1] = pval;						\
	    prev[i] = this[i];						\
	    this[i] = pdiff + c*tdiff + ndiff;				\
	    this = next;						\
	}								\
    }

generate_filter_horiz_edge3x3(BYTE, int, int)
generate_filter_horiz_edge3x3(UBYTE, int, int)
generate_filter_horiz_edge3x3(SHORT, int, int)
generate_filter_horiz_edge3x3(USHORT, int, int)
generate_filter_horiz_edge3x3(LONG, LONG, int32_t)
generate_filter_horiz_edge3x3(ULONG, ULONG, int64_t)
generate_filter_horiz_edge3x3(FLOAT, FLOAT, float)
generate_filter_horiz_edge3x3(DOUBLE, DOUBLE, double)


static improc_function_Ikt filter_horiz_edge3x3_bytype[IM_TYPEMAX] = {
    [IM_BYTE] = filter_horiz_edge3x3_BYTE,
    [IM_UBYTE] = filter_horiz_edge3x3_UBYTE,
    [IM_SHORT] = filter_horiz_edge3x3_SHORT,
    [IM_USHORT] = filter_horiz_edge3x3_USHORT,
    [IM_LONG] = filter_horiz_edge3x3_LONG,
    [IM_ULONG] = filter_horiz_edge3x3_ULONG,
    [IM_FLOAT] = filter_horiz_edge3x3_FLOAT,
    [IM_DOUBLE] = filter_horiz_edge3x3_DOUBLE,
};


/*
 * tile_switch_row_pointers()
 *
 * Switch the bottom num rows of the tile memory buffer with the top num
 * rows, to enable already calculated rows on the previous tile to be
 * shifted to the top of the next tile avoiding recalculation.
 */

static void tile_switch_row_pointers(ip_tile *tile, int num)
{
    ip_image *im = tile->tbuf;

    for (int j=0; j<num; j++) {
	void *ptr = im->imrow.v[j];
	im->imrow.v[j] = im->imrow.v[im->size.y-num+j];
	im->imrow.v[im->size.y-num+j] = ptr;
    }
}



/*
 * filter_image_sep3x3()
 *
 * Filter images with a separable 3x3 kernel.  If the kernel is a Sobel or
 * Prewitt kernel use the specially coded edge3x3 variant of the filters
 * defined above.  Otherwise use the less efficient tiled horiz3/vert3
 * variants of the filters.
 */

static void filter_image_sep3x3(ip_image *im, ip_kernel *kern)
{
    ip_coord2d tsize;
    ip_tile *tile;
    int tmp, done;
    improc_function_Ikt fhoriz;
    improc_function_Ikt fvert;

    tmp = kern->reqtype & 0xff;
    if (tmp == IP_KERNEL_SOBEL || tmp == IP_KERNEL_PREWITT) {
	/*
	 * Treat Sobel and Prewitt kernels specially as they have a number
	 * of zeros and ones in the kernel thus can be simplified for a more
	 * efficient implementation.
	 */
	if (kern->reqtype & IP_KERNEL_VERTICAL) {
#ifdef HAVE_SIMD
	    if (IP_USE_HWACCEL && ip_simd_filter_vert_edge3x3_bytype[im->type])
		fhoriz = ip_simd_filter_vert_edge3x3_bytype[im->type];
	    else
		fhoriz = filter_vert_edge3x3_bytype[im->type];
#else
	    fhoriz = filter_vert_edge3x3_bytype[im->type];
#endif
	}else{
#ifdef HAVE_SIMD
	    if (IP_USE_HWACCEL && ip_simd_filter_horiz_edge3x3_bytype[im->type])
		fhoriz = ip_simd_filter_horiz_edge3x3_bytype[im->type];
	    else
		fhoriz = filter_horiz_edge3x3_bytype[im->type];
#else
	    fhoriz = filter_horiz_edge3x3_bytype[im->type];
#endif
	}

	if (fhoriz) {
	    if ((tile = ip_alloc_tile(im, im->size, TILE_ROWBUF))) {
		fhoriz(im, kern, tile);
		ip_free_tile(tile);
	    }
	    return;
	}

	/*
	 * If didn't find an operator in bytype arrays above then fall
	 * through to general 3x3 separable filter code below.
	 */
    }

    /* Choose the appropriate filters according to image type */
    fhoriz = filter_horiz3_bytype[im->type];
    fvert = filter_vert3_bytype[im->type];

    if (fhoriz == NULL || fvert == NULL) {
	ip_log_error(ERR_NOT_IMPLEMENTED);
	return;
    }

    tsize = tile_choose_good_size(im, kern);
    if ((tile = ip_alloc_tile(im, tsize, TILE_TILEBUF)) == NULL)
	return;

    /* Process the image by tiles */
    tile->origin = (ip_coord2d){0,0};
    done = 0;
    while (NOT done) {
	if (tile->size.y > (im->size.y - tile->origin.y)) {
	    tile->size.y = im->size.y - tile->origin.y;
	    done = 1;
	}
	fhoriz(im, kern, tile);
	fvert(im, kern, tile);
	tile_switch_row_pointers(tile, 2);
	tile->origin.y += tile->size.y - 2;
    }

    ip_free_tile(tile);
}



/*
 * Efficient implementation of the NxM mean filter.
 *
 * For 1D filters (Nx1 or 1xM kernels) we can filter the image per pixel in
 * O(1) time by adding one pixel and removing one pixel from the kernel each
 * time it shifts to the next pixel to process.  To achieve an in-place
 * filter a row buffer is used.
 *
 * The 1D filter in the vertical direction is a little trickier to get good
 * cache utilisation.  As usual we process along rows, but to save on
 * calculation we use in addition to a row buffer (to hold the last value of
 * the kernel for each column) we need a tile buffer to hold source image
 * values until they are subtracted off the kernels.
 *
 * The NxM filter uses a seperable approach to filtering to get an O(1)
 * implementation.
 */

#ifdef HAVE_EFFICIENT_IDIV

/*
 * The arch has an efficient integer divide instruction that the compiler
 * should know about.  Just use standard C expressions for integer division.
 */

/* Effect unsigned integer division with rounding */
#define UDIV_INIT(dt, d) dt##_type divisor = d;
#define UDIV(k, r) (((k) + (r)) / divisor)

/* Effect signed integer division with rounding */
#define IDIV_INIT(dt, d) dt##_type divisor = d;
#define IDIV(k, r) (((k) + (((k) >= 0) ? (r) : -(r))) / divisor)

#else

/*
 * The arch does not have an efficient integer divide instruction, thus
 * inefficient code results if use standard C integer divide expressions.
 * Use our own divide function in which we calculate the reciprocal of the
 * denominator once for the image and use integer multiplication with the
 * numerator at each pixel to effect the divide.
 */

#include <ip/divide.h>

#define UDIV_INIT(dt, d) struct ip_uint32_rcp divisor = ip_urcp32(d);
#define UDIV(k, r) ip_udiv32((k)+(r), divisor)

#define IDIV_INIT(dt, d) struct ip_int32_rcp divisor = ip_ircp32(d);
#define IDIV(k, r) ip_idiv32u((k) + (((k)>=0) ? (r) : -(r)), divisor);

#endif

/* floating point division always without rounding */
#define FDIV_INIT(dt, d) dt##_type divisor = 1.0/(d);
#define FDIV(k, r) ((k) * divisor)

#define generate_filter_mean_horiz_inplace(tt, inttt, divider_init, divider) \
    static void filter_mean_horiz_inplace_##tt(ip_image *im, ip_kernel *kern, ip_tile *tile) \
    {									\
	int len = kern->size.x;						\
	int ridx = len / 2;						\
	int lidx = (len-1) / 2;						\
	tt##_type *tpix = IM_ROWADR(tile->tbuf, 0, v);			\
	if (len == 2) {							\
	    divider_init(inttt, 2);					\
	    for (int j=0; j<im->size.y; j++) {				\
		inttt##_type this, next, sum;				\
		tt##_type *impix = IM_ROWADR(im, j, v);			\
		this = (inttt##_type)impix[0];				\
		next = 0; /* STFCU */					\
		for (int i=0; i<im->size.x-1; i++) {			\
		    next = impix[i+1];					\
		    sum = this + next;					\
		    impix[i] = divider(sum, 1);				\
		    this = next;					\
		}							\
		sum = this + impix[im->size.x-1];			\
		impix[im->size.x-1] = divider(sum, 1);			\
	    }								\
	}else{								\
	    divider_init(inttt, len);					\
	    for (int j=0; j<im->size.y; j++) {				\
		inttt##_type ksum;					\
		tt##_type *impix = IM_ROWADR(im, j, v);			\
		/* Initialise kernel at 0th column */			\
		ksum = lidx*impix[0];					\
		for (int i=0; i<ridx; i++) {				\
		    ksum += impix[i];					\
		}							\
		tpix[0] = impix[0];					\
		/* Process left edge while kernel is not completely in row. */ \
		for (int i=0; i<lidx; i++) {				\
		    tt##_type this = impix[i];				\
		    ksum += impix[i+ridx];				\
		    impix[i] = divider(ksum, ridx);			\
		    ksum -= tpix[0];					\
		    tpix[i] = this;					\
		}							\
		/* Process along row while kernel completely in row. */	\
		for (int i=lidx; i<im->size.x-ridx; i++) {		\
		    tt##_type this = impix[i];				\
		    ksum += impix[i+ridx];				\
		    impix[i] = divider(ksum, ridx);			\
		    ksum -= tpix[i-lidx];				\
		    tpix[i] = this;					\
		}							\
		/* Process right edge where kernel is off edge of row. */ \
		for (int i=im->size.x-ridx; i<im->size.x; i++) {	\
		    tt##_type this = impix[i];				\
		    ksum += impix[im->size.x-1];			\
		    impix[i] = divider(ksum, ridx);			\
		    ksum -= tpix[i-lidx];				\
		    tpix[i] = this;					\
		}							\
	    }								\
	}								\
    }


generate_filter_mean_horiz_inplace(BYTE, int, IDIV_INIT, IDIV)
generate_filter_mean_horiz_inplace(UBYTE, int, UDIV_INIT, UDIV)
generate_filter_mean_horiz_inplace(SHORT, int, IDIV_INIT, IDIV)
generate_filter_mean_horiz_inplace(USHORT, int, UDIV_INIT, UDIV)
generate_filter_mean_horiz_inplace(LONG, LONG, IDIV_INIT, IDIV)
generate_filter_mean_horiz_inplace(ULONG, ULONG, UDIV_INIT, UDIV)
generate_filter_mean_horiz_inplace(FLOAT, FLOAT, FDIV_INIT, FDIV)
generate_filter_mean_horiz_inplace(DOUBLE, DOUBLE, FDIV_INIT, FDIV)

static improc_function_Ikt filter_mean_horiz_inplace_bytype[IM_TYPEMAX] = {
    [IM_BYTE] = filter_mean_horiz_inplace_BYTE,
    [IM_UBYTE] = filter_mean_horiz_inplace_UBYTE,
    [IM_SHORT] = filter_mean_horiz_inplace_SHORT,
    [IM_USHORT] = filter_mean_horiz_inplace_USHORT,
    [IM_LONG] = filter_mean_horiz_inplace_LONG,
    [IM_ULONG] = filter_mean_horiz_inplace_ULONG,
    [IM_FLOAT] = filter_mean_horiz_inplace_FLOAT,
    [IM_DOUBLE] = filter_mean_horiz_inplace_DOUBLE,
};



#define generate_filter_mean_horiz(tt, inttt)				\
    static void filter_mean_horiz_##tt(ip_image *im, ip_kernel *kern, ip_tile *tile) \
    {									\
	int len = kern->size.x;						\
	int ridx = len / 2;						\
	int lidx = (len-1) / 2;						\
	int start_row = tile->origin.y ? kern->size.y-2 : 0;		\
	for (int j=start_row; j<tile->size.y; j++) {			\
	    inttt##_type ksum;						\
	    tt##_type *impix = IM_ROWADR(im, j+tile->origin.y, v);	\
	    inttt##_type *tpix = IM_ROWADR(tile->tbuf, j, v);		\
	    /* Initialise kernel at 0th column */			\
	    ksum = lidx*impix[0];					\
	    for (int i=0; i<ridx; i++) {				\
		ksum += impix[i];					\
	    }								\
	    /* Process left edge while kernel is not completely in row. */ \
	    for (int i=0; i<lidx; i++) {				\
		ksum += impix[i+ridx];					\
		tpix[i] = ksum;						\
		ksum -= impix[0];					\
	    }								\
	    /* Process along row while kernel completely in row. */	\
	    for (int i=lidx; i<im->size.x-ridx; i++) {			\
		ksum += impix[i+ridx];					\
		tpix[i] = ksum;						\
		ksum -= impix[i-lidx];					\
	    }								\
	    /* Process right edge where kernel is off edge of row. */	\
	    for (int i=im->size.x-ridx; i<im->size.x; i++) {		\
		ksum += impix[im->size.x-1];				\
		tpix[i] = ksum;						\
		ksum -= impix[i-lidx];					\
	    }								\
	}								\
    }


generate_filter_mean_horiz(BYTE, int)
generate_filter_mean_horiz(UBYTE, int)
generate_filter_mean_horiz(SHORT, int)
generate_filter_mean_horiz(USHORT, int)
generate_filter_mean_horiz(LONG, LONG)
generate_filter_mean_horiz(ULONG, ULONG)
generate_filter_mean_horiz(FLOAT, FLOAT)
generate_filter_mean_horiz(DOUBLE, DOUBLE)

static improc_function_Ikt filter_mean_horiz_bytype[IM_TYPEMAX] = {
    [IM_BYTE] = filter_mean_horiz_BYTE,
    [IM_UBYTE] = filter_mean_horiz_UBYTE,
    [IM_SHORT] = filter_mean_horiz_SHORT,
    [IM_USHORT] = filter_mean_horiz_USHORT,
    [IM_LONG] = filter_mean_horiz_LONG,
    [IM_ULONG] = filter_mean_horiz_ULONG,
    [IM_FLOAT] = filter_mean_horiz_FLOAT,
    [IM_DOUBLE] = filter_mean_horiz_DOUBLE,
};



/*
 * For the vertical mean filtering we process down columns but to improve
 * cache locality and reduce re-accessing image row addresses we process
 * eight columns simultaneously. (Eight columns gives an improvement over
 * four on systems tested.)
 *
 * Helper macros:
 * INIT_KSUMS_WITH_ZERO() - initialise all the kernel sums to zero.
 *
 * INIT_KSUMS_WITH_VAL() - initialise all kernel sums from x array.  Note
 *    that we do not wrap the x argument in parentheses so that can call
 *    INIT_KSUMS_WITH_VAL(num*x) and get num times the x array value.
 *
 * ADD_TO_KSUMS() - Update kernels with newest pixel values.
 * SUB_TO_KSUMS() - for subtracting off the oldest pixel values.
 *
 * SET_PIXELS_FROM_KSUMS() - write result back to image pixels after
 *    dividing kernel sums by number of pixels.
 *
 * SET_TILE_PIXELS(x, y) - a direct copy of array y (the image pixels)
 *    to array x (the tile pixels) to save pixel values needed later.
 */

#if 0
#define INIT_KSUMS_WITH_ZERO()			\
    ksum1 = ksum2 = ksum3 = ksum4 = 0;
#define INIT_KSUMS_WITH_VAL(x)			\
    ksum1 = x[i+0]; ksum2 = x[i+1]; ksum3 = x[i+2]; ksum4 = x[i+3];
#define ADD_TO_KSUMS(x)				\
    ksum1 += x[i+0]; ksum2 += x[i+1]; ksum3 += x[i+2]; ksum4 += x[i+3];
#define SUB_FROM_KSUMS(x, i)			\
    ksum1 -= x[i+0]; ksum2 -= x[i+1]; ksum3 -= x[i+2]; ksum4 -= x[i+3];
#define SET_PIXELS_FROM_KSUMS(x)		\
    x[i+0] = divider(ksum1, rounder);		\
    x[i+1] = divider(ksum2, rounder);		\
    x[i+2] = divider(ksum3, rounder);		\
    x[i+3] = divider(ksum4, rounder);
#endif

#if 1
#define INIT_KSUMS_WITH_ZERO()			\
    ksum1 = ksum2 = ksum3 = ksum4 = ksum5 = ksum6 = ksum7 = ksum8 = 0;
#define INIT_KSUMS_WITH_VAL(x)			\
    ksum1 = x[i+0]; ksum2 = x[i+1]; ksum3 = x[i+2]; ksum4 = x[i+3]; \
    ksum5 = x[i+4]; ksum6 = x[i+5]; ksum7 = x[i+6]; ksum8 = x[i+7];
#define ADD_TO_KSUMS(x)				\
    ksum1 += x[i+0]; ksum2 += x[i+1]; ksum3 += x[i+2]; ksum4 += x[i+3]; \
    ksum5 += x[i+4]; ksum6 += x[i+5]; ksum7 += x[i+6]; ksum8 += x[i+7];
#define SUB_FROM_KSUMS(x, i)			\
    ksum1 -= x[i+0]; ksum2 -= x[i+1]; ksum3 -= x[i+2]; ksum4 -= x[i+3]; \
    ksum5 -= x[i+4]; ksum6 -= x[i+5]; ksum7 -= x[i+6]; ksum8 -= x[i+7];
#define SET_PIXELS_FROM_KSUMS(x, divider, rounder)	\
    x[i+0] = divider(ksum1, rounder);			\
    x[i+1] = divider(ksum2, rounder);			\
    x[i+2] = divider(ksum3, rounder);			\
    x[i+3] = divider(ksum4, rounder);			\
    x[i+4] = divider(ksum5, rounder);			\
    x[i+5] = divider(ksum6, rounder);			\
    x[i+6] = divider(ksum7, rounder);			\
    x[i+7] = divider(ksum8, rounder);
#define SET_TILE_PIXELS(x, y)				\
    x[0] = y[i+0]; x[1] = y[i+1]; x[2] = y[i+2]; x[3] = y[i+3];	\
    x[4] = y[i+4]; x[5] = y[i+5]; x[6] = y[i+6]; x[7] = y[i+7];
#endif


/*
 * Inplace 1xM vertical mean filter.  A tile that is KSUMS wide (typically
 * eight kernel sums) and half the height of the kernel is required to hold
 * pixel values until they have been subtracted off the kernel.
 *
 * Processing is down rows so to get better cache usage processes the image
 * in blocks of the image width and 256 rows deep for kernel sizes < 128
 * pixels and is the full image y size whan kernel size is > 128 pixels.
 * Currently this is hardcoded but seems a reasonable compromise.
 */
#define generate_filter_mean_vert_inplace(tt, inttt, divider_init, divider) \
    static void filter_mean_vert_inplace_##tt(ip_image *im, ip_kernel *kern, ip_tile *tile) \
    {									\
	const int len = kern->size.y;					\
	const int hlen = len/2;						\
	const int ridx = hlen;						\
	const int lidx = (len-1) / 2;					\
	ip_image *tbuf = tile->tbuf;					\
	divider_init(inttt, (inttt##_type)len);				\
	/* Process along the rows eight pixels at a time */		\
	for (int i=0; i<(im->size.x & ~7); i+=8) {			\
	    inttt##_type ksum1, ksum2, ksum3, ksum4;			\
	    inttt##_type ksum5, ksum6, ksum7, ksum8;			\
	    tt##_type *impix, *dpix, *upix;				\
	    tt##_type *tpix;						\
	    /* Initialise kernel at 0th row */				\
	    dpix = IM_ROWADR(im, 0, v);					\
	    INIT_KSUMS_WITH_VAL(lidx*dpix);				\
	    for (int j=0; j<ridx; j++) {				\
		dpix = IM_ROWADR(im, j, v);				\
		ADD_TO_KSUMS(dpix);					\
	    }								\
	    /* Process top edge while kernel is not completely in column. */ \
	    upix = IM_ROWADR(tbuf, 0, v);				\
	    for (int j=0; j<lidx; j++) {				\
		dpix = IM_ROWADR(im, j+ridx, v);			\
		impix = IM_ROWADR(im, j, v);				\
		tpix = IM_ROWADR(tbuf, j, v);				\
		ADD_TO_KSUMS(dpix);					\
		SET_TILE_PIXELS(tpix, impix);				\
		SET_PIXELS_FROM_KSUMS(impix, divider, hlen);		\
		SUB_FROM_KSUMS(upix, 0);				\
	    }								\
	    /* Process down columns while kernel completely in image. */ \
	    for (int j=lidx; j<im->size.y-ridx; j++) {			\
		upix = IM_ROWADR(tbuf, j-lidx, v);			\
		dpix = IM_ROWADR(im, j+ridx, v);			\
		impix = IM_ROWADR(im, j, v);				\
		tpix = IM_ROWADR(tbuf, j, v);				\
		ADD_TO_KSUMS(dpix);					\
		SET_TILE_PIXELS(tpix, impix);				\
		SET_PIXELS_FROM_KSUMS(impix, divider, hlen);		\
		SUB_FROM_KSUMS(upix, 0);				\
	    }								\
	    /* Process bottom edge where kernel is off bottom of column. */ \
	    dpix = IM_ROWADR(im, im->size.y-1, v);			\
	    for (int j=im->size.y-ridx; j<im->size.y; j++) {		\
		upix = IM_ROWADR(tbuf, j-lidx, v);			\
		impix = IM_ROWADR(im, j, v);				\
		ADD_TO_KSUMS(dpix);					\
		SET_PIXELS_FROM_KSUMS(impix, divider, hlen);		\
		SUB_FROM_KSUMS(upix, 0);				\
	    }								\
	}								\
	/* Finish off last few columns */				\
	for (int i=(im->size.x & ~7); i<im->size.x; i++) {		\
	    inttt##_type ksum;						\
	    tt##_type *impix, *dpix, *upix, *tpix;			\
	    /* Initialise kernel at 0th row */				\
	    dpix = IM_ROWADR(im, 0, v);					\
	    ksum = lidx*dpix[i];					\
	    for (int j=0; j<ridx; j++) {				\
		dpix = IM_ROWADR(im, j, v);				\
		ksum += dpix[i];					\
	    }								\
	    /* Process top edge while kernel is not completely in column. */ \
	    upix = IM_ROWADR(tbuf, 0, v);				\
	    for (int j=0; j<lidx; j++) {				\
		dpix = IM_ROWADR(im, j+ridx, v);			\
		impix = IM_ROWADR(im, j, v);				\
		tpix = IM_ROWADR(tbuf, j, v);				\
		ksum += dpix[i];					\
		tpix[0] = impix[i];					\
		impix[i] = divider(ksum, hlen);				\
		ksum -= upix[0];					\
	    }								\
	    /* Process along column while kernel completely in tile column. */ \
	    for (int j=lidx; j<tile->size.y-ridx; j++) {		\
		upix = IM_ROWADR(tbuf, j-lidx, v);			\
		dpix = IM_ROWADR(im, j+ridx, v);			\
		impix = IM_ROWADR(im, j, v);				\
		tpix = IM_ROWADR(tbuf, j, v);				\
		ksum += dpix[i];					\
		tpix[0] = impix[i];					\
		impix[i] = divider(ksum, hlen);				\
		ksum -= upix[0];					\
	    }								\
	    /* Process bottom edge where kernel is off bottom of column. */ \
	    dpix = IM_ROWADR(im, im->size.y-1, v);			\
	    for (int j=im->size.y-ridx; j<im->size.y; j++) {		\
		upix = IM_ROWADR(tbuf, j-lidx, v);			\
		impix = IM_ROWADR(im, j, v);				\
		ksum += dpix[i];					\
		impix[i] = divider(ksum, hlen);				\
		ksum -= upix[0];					\
	    }								\
	}								\
    }



#define generate_filter_mean_vert(tt, inttt, divider_init, divider)	\
    static void filter_mean_vert_##tt(ip_image *im, ip_kernel *kern, ip_tile *tile) \
    {									\
	int len = kern->size.y;						\
	int ridx = len / 2;						\
	int lidx = (len-1) / 2;						\
	ip_image *tbuf = tile->tbuf;					\
	int hsize = (kern->size.x * kern->size.y);			\
	divider_init(inttt, (inttt##_type)hsize);			\
	hsize /= 2;							\
	for (int i=0; i<(im->size.x & ~7); i+=8) {			\
	    inttt##_type ksum1, ksum2, ksum3, ksum4;			\
	    inttt##_type ksum5, ksum6, ksum7, ksum8;			\
	    tt##_type *impix;						\
	    inttt##_type *dpix, *upix;					\
	    /* Initialise kernel at 0th row */				\
	    if (tile->origin.y == 0) {					\
		dpix = IM_ROWADR(tbuf, 0, v);				\
		INIT_KSUMS_WITH_VAL(lidx*dpix);				\
		for (int j=0; j<ridx; j++) {				\
		    dpix = IM_ROWADR(tbuf, j, v);			\
		    ADD_TO_KSUMS(dpix);					\
		}							\
		/* Process top edge while kernel is not completely in column. */ \
		upix = IM_ROWADR(tbuf, 0, v);				\
		for (int j=0; j<lidx; j++) {				\
		    dpix = IM_ROWADR(tbuf, j+ridx, v);			\
		    impix = IM_ROWADR(im, tile->origin.y+j, v);		\
		    ADD_TO_KSUMS(dpix);					\
		    SET_PIXELS_FROM_KSUMS(impix, divider, hsize);	\
		    SUB_FROM_KSUMS(upix, i);				\
		}							\
	    }else{							\
		INIT_KSUMS_WITH_ZERO();					\
		for (int j=0; j<len-1; j++) {				\
		    dpix = IM_ROWADR(tbuf, j, v);			\
		    ADD_TO_KSUMS(dpix);					\
		}							\
	    }								\
	    /* Process along column while kernel completely in tile column. */ \
	    for (int j=lidx; j<tile->size.y-ridx; j++) {		\
		upix = IM_ROWADR(tbuf, j-lidx, v);			\
		dpix = IM_ROWADR(tbuf, j+ridx, v);			\
		impix = IM_ROWADR(im, tile->origin.y+j, v);		\
		ADD_TO_KSUMS(dpix);					\
		SET_PIXELS_FROM_KSUMS(impix, divider, hsize);		\
		SUB_FROM_KSUMS(upix, i);					\
	    }								\
	    /* Process bottom edge where kernel is off bottom of column. */ \
	    if (tile->origin.y + tile->size.y == im->size.y) {	\
		dpix = IM_ROWADR(tbuf, tile->size.y-1, v);		\
		for (int j=tile->size.y-ridx; j<tile->size.y; j++) {	\
		    upix = IM_ROWADR(tbuf, j-lidx, v);			\
		    impix = IM_ROWADR(im, tile->origin.y+j, v);		\
		    ADD_TO_KSUMS(dpix);					\
		    SET_PIXELS_FROM_KSUMS(impix, divider, hsize);	\
		    SUB_FROM_KSUMS(upix, i);				\
		}							\
	    }								\
	}								\
	/* Finish off last few columns */				\
	for (int i=(im->size.x & ~7); i<im->size.x; i++) {		\
	    inttt##_type ksum;						\
	    tt##_type *impix;						\
	    inttt##_type *dpix, *upix;					\
	    /* Initialise kernel at 0th row */				\
	    if (tile->origin.y == 0) {					\
		dpix = IM_ROWADR(tbuf, 0, v);				\
		ksum = lidx*dpix[i];					\
		for (int j=0; j<ridx; j++) {				\
		    dpix = IM_ROWADR(tbuf, j, v);			\
		    ksum += dpix[i];					\
		}							\
		/* Process top edge while kernel is not completely in column. */ \
		upix = IM_ROWADR(tbuf, 0, v);				\
		for (int j=0; j<lidx; j++) {				\
		    dpix = IM_ROWADR(tbuf, j+ridx, v);			\
		    impix = IM_ROWADR(im, tile->origin.y+j, v);		\
		    ksum += dpix[i];					\
		    impix[i] = divider(ksum, hsize);			\
		    ksum -= upix[i];					\
		}							\
	    }else{							\
		ksum = 0;						\
		for (int j=0; j<len-1; j++) {				\
		    dpix = IM_ROWADR(tbuf, j, v);			\
		    ksum += dpix[i];					\
		}							\
	    }								\
	    /* Process along column while kernel completely in tile column. */ \
	    for (int j=lidx; j<tile->size.y-ridx; j++) {		\
		upix = IM_ROWADR(tbuf, j-lidx, v);			\
		dpix = IM_ROWADR(tbuf, j+ridx, v);			\
		impix = IM_ROWADR(im, tile->origin.y+j, v);		\
		ksum += dpix[i];					\
		impix[i] = divider(ksum, hsize);			\
		ksum -= upix[i];					\
	    }								\
	    /* Process bottom edge where kernel is off bottom of column. */ \
	    if (tile->origin.y + tile->size.y == im->size.y) {	\
		dpix = IM_ROWADR(tbuf, tile->size.y-1, v);		\
		for (int j=tile->size.y-ridx; j<tile->size.y; j++) {	\
		    upix = IM_ROWADR(tbuf, j-lidx, v);			\
		    impix = IM_ROWADR(im, tile->origin.y+j, v);		\
		    ksum += dpix[i];					\
		    impix[i] = divider(ksum, hsize);			\
		    ksum -= upix[i];					\
		}							\
	    }								\
	}								\
    }


generate_filter_mean_vert_inplace(BYTE, int, IDIV_INIT, IDIV)
generate_filter_mean_vert_inplace(UBYTE, int, UDIV_INIT, UDIV)
generate_filter_mean_vert_inplace(SHORT, int, IDIV_INIT, IDIV)
generate_filter_mean_vert_inplace(USHORT, int, UDIV_INIT, UDIV)
generate_filter_mean_vert_inplace(LONG, LONG, IDIV_INIT, IDIV)
generate_filter_mean_vert_inplace(ULONG, ULONG, UDIV_INIT, UDIV)
generate_filter_mean_vert_inplace(FLOAT, FLOAT, FDIV_INIT, FDIV)
generate_filter_mean_vert_inplace(DOUBLE, DOUBLE, FDIV_INIT, FDIV)

static improc_function_Ikt filter_mean_vert_inplace_bytype[IM_TYPEMAX] = {
    [IM_BYTE] = filter_mean_vert_inplace_BYTE,
    [IM_UBYTE] = filter_mean_vert_inplace_UBYTE,
    [IM_SHORT] = filter_mean_vert_inplace_SHORT,
    [IM_USHORT] = filter_mean_vert_inplace_USHORT,
    [IM_LONG] = filter_mean_vert_inplace_LONG,
    [IM_ULONG] = filter_mean_vert_inplace_ULONG,
    [IM_FLOAT] = filter_mean_vert_inplace_FLOAT,
    [IM_DOUBLE] = filter_mean_vert_inplace_DOUBLE,
};

generate_filter_mean_vert(BYTE, int, IDIV_INIT, IDIV)
generate_filter_mean_vert(UBYTE, int, UDIV_INIT, UDIV)
generate_filter_mean_vert(SHORT, int, IDIV_INIT, IDIV)
generate_filter_mean_vert(USHORT, int, UDIV_INIT, UDIV)
generate_filter_mean_vert(LONG, LONG, IDIV_INIT, IDIV)
generate_filter_mean_vert(ULONG, ULONG, UDIV_INIT, UDIV)
generate_filter_mean_vert(FLOAT, FLOAT, FDIV_INIT, FDIV)
generate_filter_mean_vert(DOUBLE, DOUBLE, FDIV_INIT, FDIV)

static improc_function_Ikt filter_mean_vert_bytype[IM_TYPEMAX] = {
    [IM_BYTE] = filter_mean_vert_BYTE,
    [IM_UBYTE] = filter_mean_vert_UBYTE,
    [IM_SHORT] = filter_mean_vert_SHORT,
    [IM_USHORT] = filter_mean_vert_USHORT,
    [IM_LONG] = filter_mean_vert_LONG,
    [IM_ULONG] = filter_mean_vert_ULONG,
    [IM_FLOAT] = filter_mean_vert_FLOAT,
    [IM_DOUBLE] = filter_mean_vert_DOUBLE,
};



static void filter_image_mean(ip_image *im, ip_kernel *kern)
{
    ip_tile *tile;
    improc_function_Ikt mean_filter = NULL;

    if (kern->size.y == 1) {
	/* Horizontal mean filter */
#ifdef HAVE_SIMD
	if (IP_USE_HWACCEL && ip_simd_filter_horiz_mean_inplace_bytype[im->type])
	    mean_filter = ip_simd_filter_horiz_mean_inplace_bytype[im->type];
#endif
	if (! mean_filter)
	    mean_filter = filter_mean_horiz_inplace_bytype[im->type];

	if (mean_filter && (tile = ip_alloc_tile(im, im->size, TILE_ROWBUF))) {
	    mean_filter(im, kern, tile);
	    ip_free_tile(tile);
	}
    }else if (kern->size.x == 1) {
	/* Vertical mean filter */
#ifdef HAVE_SIMD
	if (IP_USE_HWACCEL && ip_simd_filter_vert_mean_inplace_bytype[im->type]) {
	    mean_filter = ip_simd_filter_vert_mean_inplace_bytype[im->type];
	    tile = ip_alloc_tile(im,
				 (ip_coord2d){im->size.y*SIMD_VECTOR_SIZE/ip_type_size(im->type), 4},
				 TILE_TILEBUF);
	}
#endif
	if (! mean_filter) {
	    mean_filter = filter_mean_vert_inplace_bytype[im->type];
	    tile = ip_alloc_tile(im, (ip_coord2d){8, im->size.y}, TILE_TILEBUF);
	}

	if (mean_filter && tile) {
	    mean_filter(im, kern, tile);
	}
	if (tile)
	    ip_free_tile(tile);
    }else{
	improc_function_Ikt fhoriz, fvert;
	ip_coord2d tsize;
	int done;
	int swsize;

	/* Choose the appropriate filters according to image type */
	fhoriz = filter_mean_horiz_bytype[im->type];
	fvert = filter_mean_vert_bytype[im->type];

	if (fhoriz == NULL || fvert == NULL) {
	    ip_log_error(ERR_NOT_IMPLEMENTED);
	    return;
	}

	tsize = tile_choose_good_size(im, kern);
	if ((tile = ip_alloc_tile(im, tsize, TILE_TILEBUF)) == NULL)
	    return;

	/* Process the image by tiles */
	tile->origin = (ip_coord2d){0,0};
	done = 0;
	swsize = kern->size.y-1;
	while (NOT done) {
	    if (tile->size.y > (im->size.y - tile->origin.y)) {
		tile->size.y = im->size.y - tile->origin.y;
		done = 1;
	    }
	    fhoriz(im, kern, tile);
	    fvert(im, kern, tile);
	    tile_switch_row_pointers(tile, swsize);
	    tile->origin.y += tile->size.y - swsize;
	}

	ip_free_tile(tile);
    }

}



/*

NAME:

   im_filter()

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/filter.h>

   int  im_filter( im, kern, flag )
   ip_image *im;
   ip_kernel *kern;
   int flag;

DESCRIPTION:

   In place filter the image `im' with the kernel `kern'.  See
   ip_alloc_kernel() for the description of possible filtering kernels.
   Argument `flag' is currently unused.  Set it to NO_FLAG.

ERRORS:

   ERR_BAD_TYPE
   ERR_OUT_OF_MEM

SEE ALSO:

   ip_alloc_kernel()

*/


int im_filter(ip_image *im, ip_kernel *kern, int flag)
{
    int ktype = kern->type & 0xff;

    ip_log_routine(__func__);
    ip_error = OK;

    if (NOT ip_kernel_valid(kern))
	return ip_error;

    if (kern->size.x == 1 && kern->size.y == 1) {
	ip_pop_routine();
	return OK;
    }

    /* Check image */
    if (NOT image_valid(im))
	return ip_error;

    if (NOT (image_type_integer(im) || image_type_real(im))) {
	ip_log_error(ERR_BAD_TYPE);
	ip_pop_routine();
	return ip_error;
    }

    /* Check kernel is smaller than image */
    if (NOT ip_point_in_image("kernel.size", kern->size, im))
	return ip_error;

    if (((ktype == KERNEL_HORIZONTAL) &&  (kern->size.x == 2 || kern->size.x == 3))
	|| ((ktype == KERNEL_VERTICAL) && (kern->size.y == 2 || kern->size.y == 3))) {

	filter_image_1d(im, kern);

    }else if ((ktype == KERNEL_SEPARABLE) && (kern->size.x == 3 && kern->size.y == 3)) {

	filter_image_sep3x3(im, kern);

    }else if (ktype == KERNEL_MEAN) {

	filter_image_mean(im, kern);

    }else{

	ip_log_error(ERR_NOT_IMPLEMENTED);

    }

    ip_pop_routine();
    return ip_error;
}


/*

NAME:

   im_clear_border() \- Clear a border region in image

PROTOTYPE:

   #include <ip/ip.h>

   void  im_clear_border( im, width )
   ip_image *im;
   int width;

DESCRIPTION:

   Zero border of width 'width' around image.

   N.B. This routine assumes that floating-point zero is represented
   by a sequence of zeros. This is true for IEEE float and double types.

ERRORS:

*/


void im_clear_border(ip_image *im, int width)
{
    int skip = ip_type_size(im->type);

    /* Top and bottom borders */

    for (int j=0; j<width; j++)
	memset(IM_ROWADR(im, j, v), 0, im->row_size);
    for (int j=im->size.y-width; j<im->size.y; j++)
	memset(IM_ROWADR(im, j, v), 0, im->row_size);

    /* Do left and right borders row by row. */

    for (int j=width; j<im->size.y-width; j++) {
	char *ptr = IM_ROWADR(im, j, v);
	memset(ptr, 0, width * skip);
	memset(ptr+(im->size.x-width)*skip, 0, width * skip);
    }
}
