/*
   Module: ip/binary.c
   Author: M.J.Cree

   Operations involving binary images

   Copyright (C) 1996-1997, 1999, 2002, 2007-2008, 2018 Michael J. Cree

ENTRY POINTS:

   im_binary_to_ubyte()
   im_ubyte_to_binary()
   im_mask()

*/


#include <stdlib.h>

#include <ip/ip.h>

#include "internal.h"
#include "utils.h"


/*

NAME:

   im_binary_to_ubyte() \- Convert BINARY image to UBYTE

PROTOTYPE:

   #include <ip/ip.h>

   int  im_binary_to_ubyte( im, val )
   ip_image *im;
   int val;

DESCRIPTION:

   Convert the BINARY image 'im' to UBYTE in place, that is, no copying of
   the image takes place.  On entry 'im' is a BINARY image and on exit it is
   a 'UBYTE' image with zeros wherever the BINARY image has value FALSE, and
   with the pixel value 'val' wherever the BINARY image has value TRUE.

ERRORS:

   ERR_BAD_TYE

SEE ALSO:

   im_ubyte_to_binary()
   im_convert()

*/

int im_binary_to_ubyte(ip_image *im, int val)
{
    ip_log_routine("im_binary_to_ubyte");

    if (im->type != IM_BINARY) {
	ip_log_error(ERR_BAD_TYPE);
    }else{
	im->type = IM_UBYTE;
	if (val != 255) {
	    uint8_t nval = (uint8_t)val;
	    for (int j=0; j<im->size.y; j++) {
		uint8_t *sp = IM_ROWADR(im, j, v);

		for (int i=0; i<im->size.x; i++) {
		    if (*sp)
			*sp = nval;
		    sp++;
		}
	    }
	}
    }

    ip_pop_routine();
    return ip_error;
}



/*

NAME:

   im_ubyte_to_binary() \- Convert UBYTE image to BINARY image

PROTOTYPE:

   #include <ip/ip.h>

   int  im_ubyte_to_binary( im )
   ip_image *im;

DESCRIPTION:

   Convert the UBYTE image 'im' to BINARY in place, that is, no copying of
   the image takes place.  On entry 'im' is a UBYTE image and on exit it is
   a BINARY image with FALSE wherever the UBYTE image had a zero, and is set
   to TRUE wherever the UBYTE image was non-zero.

ERRORS:

   ERR_BAD_TYPE

SEE ALSO:

   im_binary_to_ubyte()
   im_convert()

*/

int im_ubyte_to_binary(ip_image *im)
{
    ip_log_routine("im_ubyte_to_binary");

    if (im->type != IM_UBYTE) {
	ip_log_error(ERR_BAD_TYPE);
    }else{

	im->type = IM_BINARY;
	for (int j=0; j<im->size.y; j++) {
	    uint8_t *sp = IM_ROWADR(im, j, v);

	    for (int i=0; i<im->size.x; i++) {
		if (*sp)
		    *sp = 255;
		sp++;
	    }
	}
    }

    ip_pop_routine();
    return ip_error;
}




/*

NAME:

   im_mask() \- Mask/Merge BINARY image into another image

PROTOTYPE:

   #include <ip/ip.h>

   int  im_mask( im, mask, val, flag )
   ip_image *im;
   ip_image *mask;
   double val;
   int flag;

DESCRIPTION:

   Mask the image 'im' with the BINARY image 'mask'.  Wherever 'mask'
   has a logical FALSE, 'val' is put into the image otherwise the
   image is left alone.

   If 'flag' is FLG_INVERSE then 'val' is put into the image at those
   positions where the mask has BINARY TRUE.

   This is effectively a generalised ANDing operation, in that it allows
   more control over what is put into the image where an AND ''TRUE''
   occurs, and it allows the BINARY image to be applied to images other
   than BINARY images.

   For a COMPLEX image 'im' the flag may be ORed with one of:

\&   FLG_REAL Only apply operation to real part of image.
\&   FLG_IMAG Only Apply operation to imaginary part of image.

\&-   For an RGB image 'im' the flag may be ORed with one of:

\&   FLG_RED   Only apply operation to red part of image.
\&   FLG_GREEN Only apply operation to green part of image.
\&   FLG_BLUE  Only apply operation to blue part of image.

BUGS:

   The FLG_REAL, FLR_RED, etc. flags don''t work correctly in that they
   zero the other parts of the image when modifying the specified
   part.

ERRORS:

   ERR_BAD_TYPE
   ERR_NOT_SAME_SIZE
   ERR_BAD_PARAMETER

SEE ALSO:

   im_and()

*/


#define IMROW_MASK(t,v) {\
   uint8_t *mp;\
   t *ip;\
   t tval;\
   mp = im_rowadr(mask,j);\
   ip = im_rowadr(im,j);\
   tval = v;\
   for (int i=0; i<size.x; i++) {\
      if (*mp++ == mval)\
	 *ip = tval;\
      ip++;\
   }\
}



int im_mask(ip_image *im, ip_image *mask, double val, int flag)
{
   ip_coord2d size;
   ip_rgb clr;
#if 0
   ip_complex cval;
#endif
   uint8_t mval;

   ip_log_routine("im_mask");

   if (NOT images_same_size(im,mask))
      return ip_error;
   if (mask->type != IM_BINARY) {
      ip_log_error(ERR_BAD_TYPE);
      ip_pop_routine();
      return ip_error;
   }
   if (NOT ip_value_in_image("value", val, im))
      return ip_error;

   /* Mask value to look for */

   mval = 0;			/* Normal operation-FALSE points masked off. */
   if (flag & FLG_INVERSE) {
      mval = 255;		/* Inverse operation-TRUE points masked off. */
   }

   flag &= ~FLG_INVERSE;
   if (NOT ip_flag_ok(flag, FLG_REAL, FLG_IMAG, FLG_GREEN, FLG_BLUE, FLG_RED,
		   LASTFLAG)) {
      return ip_error;
   }

   /* To shut the *#!&* compiler warnings up */

   clr.r = clr.g = clr.b = 0;

   /* Initialise values into correct types */

   size = im->size;
   val = ROUND(val);
   switch (im->type) {
    case IM_BINARY:
      val = val ? 255 : 0;
      break;
#if 0
    case IM_COMPLEX:
      cval.r = val;
      cval.i = val;
      if (flag == FLG_IMAG) {
	 cval.r = 0.0;
	 cval.i = val;
      }
      if (flag == FLG_REAL) {
	 cval.r = val;
	 cval.i = 0.0;
      }
      break;
#endif
    case IM_RGB:
       clr.r = clr.g = clr.b = (uint8_t)val;
      if (flag == FLG_RED)
	 clr.g = clr.b = 0;
      if (flag == FLG_GREEN)
	 clr.r = clr.b = 0;
      if (flag == FLG_BLUE)
	 clr.r = clr.g = 0;
      break;
   default:
      ;
   }

   /* Do the masking operation */

   for (int j=0; j<size.y; j++) {

      switch (im->type) {
       case IM_BINARY:
       case IM_UBYTE:
       case IM_PALETTE:
	 IMROW_MASK(uint8_t,val);
	 break;

       case IM_USHORT:
	 IMROW_MASK(uint16_t,val);
	 break;
       case IM_SHORT:
	 IMROW_MASK(int16_t,val);
	 break;
       case IM_LONG:
	 IMROW_MASK(int32_t,val);
	 break;
       case IM_FLOAT:
	 IMROW_MASK(float,val);
	 break;
       case IM_DOUBLE:
	 IMROW_MASK(double,val);
	 break;
#if 0
       case IM_COMPLEX:
	 IMROW_MASK(ip_complex,cval);
	 break;
#endif
       case IM_RGB:
	 IMROW_MASK(ip_rgb,clr);
	 break;
      default:
	 if (j == 0)
	    ip_log_error(ERR_NOT_IMPLEMENTED);
	 break;
      }
   }

   ip_pop_routine();
   return ip_error;
}
