/*
   Module: morphchord.c
   Author: M.J.Cree

   Copyright (C) 2015 Michael J. Cree.

   Morphological operations by chord decomposition.  This module provides
   the basic tools such as the chord table construction.

   Based on:

   Urbach and Wilkinson, 2008, "Efficient 2-D Grayscale Morphological
   Transformations with Arbitrary Flag Structuring Elements," IEEE
   Trans. Im. Proc., vol. 17, pp. 1-8.

*/

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include <ip/ip.h>
#include <ip/morph.h>

#include "internal.h"
#include "utils.h"
#include "morphint.h"

#ifdef HAVE_SIMD
#include "simd/morph_simd.h"
#endif

ip_chordtbl *ip_alloc_chord_table(ip_image *im, ip_strel *strel, int);
void ip_free_chord_table(ip_chordtbl *ct);
void ip_chord_table_update(ip_chordtbl *ct, ip_image *im);
void ip_chord_table_dump(FILE *f, ip_chordtbl *ct, int flag);

/*
 * The pad_row_<type>() functions are used to pad the start of the table row
 * before left edge of the image with the leftmost image pixel value, and to
 * pad the end of the table row after the right edge of the image with the
 * rightmost image pixel value.
 */

#define generate_pad_row_function(type) \
    static void pad_row_ ## type(void *tblrow, int start, int len, int from) \
    {									\
	type ## _type *rowp = tblrow;					\
	rowp += start;							\
	for (int j=0; j<len; j++)					\
	    rowp[j] = rowp[from-start];					\
    }

generate_pad_row_function(UBYTE)
generate_pad_row_function(USHORT)
generate_pad_row_function(ULONG)
generate_pad_row_function(DOUBLE)


static ct_function_piii pad_row_bytype[IM_TYPEMAX] = {
    [IM_BINARY] = pad_row_UBYTE,
    [IM_UBYTE] = pad_row_UBYTE,
    [IM_BYTE] = pad_row_UBYTE,
    [IM_USHORT] = pad_row_USHORT,
    [IM_SHORT] = pad_row_USHORT,
    [IM_ULONG] = pad_row_ULONG,
    [IM_LONG] = pad_row_ULONG,
    [IM_FLOAT] = pad_row_ULONG,
    [IM_DOUBLE] = pad_row_DOUBLE,
};


/*
 * The update_row_<operator>_<type>() functions calculate the chord table
 * row for the unique chord length index l from the entries in the table row
 * for chord length index l-1.  The operator in the function name (opname)
 * is one of dilation (hence the maximum is used) or erosion (thus the
 * minimum is used).
 */

#define generate_update_row_function(type, opname, op)	\
static void update_row_ ## opname ## _ ## type(void *ctptr, int trow, int clen, int disp)\
{									\
    int x;								\
    ip_chordtbl *ct = (ip_chordtbl *)ctptr;				\
    type ## _type *tblpt_up = chordtbl_cl_rowadr(ct, trow, clen-1);	\
    type ## _type *tblpt = chordtbl_cl_rowadr(ct, trow, clen);		\
    for (x = 0; x < ct->length-disp; x++) {				\
	tblpt[x] = op(tblpt_up[x], tblpt_up[x+disp]);			\
    }									\
    for ( ; x < ct->length; x++) {					\
	tblpt[x] = tblpt[ct->length-disp-1];				\
    }									\
}

generate_update_row_function(UBYTE, dilation, MAX)
generate_update_row_function(BYTE, dilation, MAX)
generate_update_row_function(USHORT, dilation, MAX)
generate_update_row_function(SHORT, dilation, MAX)
generate_update_row_function(ULONG, dilation, MAX)
generate_update_row_function(LONG, dilation, MAX)
generate_update_row_function(FLOAT, dilation, MAX)
generate_update_row_function(DOUBLE, dilation, MAX)

generate_update_row_function(UBYTE, erosion, MIN)
generate_update_row_function(BYTE, erosion, MIN)
generate_update_row_function(USHORT, erosion, MIN)
generate_update_row_function(SHORT, erosion, MIN)
generate_update_row_function(ULONG, erosion, MIN)
generate_update_row_function(LONG, erosion, MIN)
generate_update_row_function(FLOAT, erosion, MIN)
generate_update_row_function(DOUBLE, erosion, MIN)


ct_function_piii update_row_dilation_bytype[IM_TYPEMAX] = {
    [IM_BINARY] = update_row_dilation_UBYTE,
    [IM_UBYTE] = update_row_dilation_UBYTE,
    [IM_BYTE] = update_row_dilation_BYTE,
    [IM_USHORT] = update_row_dilation_USHORT,
    [IM_SHORT] = update_row_dilation_SHORT,
    [IM_ULONG] = update_row_dilation_ULONG,
    [IM_LONG] = update_row_dilation_LONG,
    [IM_FLOAT] = update_row_dilation_FLOAT,
    [IM_DOUBLE] = update_row_dilation_DOUBLE,
};

ct_function_piii update_row_erosion_bytype[IM_TYPEMAX] = {
    [IM_BINARY] = update_row_erosion_UBYTE,
    [IM_UBYTE] = update_row_erosion_UBYTE,
    [IM_BYTE] = update_row_erosion_BYTE,
    [IM_USHORT] = update_row_erosion_USHORT,
    [IM_SHORT] = update_row_erosion_SHORT,
    [IM_ULONG] = update_row_erosion_ULONG,
    [IM_LONG] = update_row_erosion_LONG,
    [IM_FLOAT] = update_row_erosion_FLOAT,
    [IM_DOUBLE] = update_row_erosion_DOUBLE,
};



    
static void copy_image_row(ip_chordtbl *ct, int trow, ip_image *im, int irow)
{
    char *tblrow = CHORDTBL_ROWADR(ct, trow);
    void *imrow = IM_ROWADR(im, irow, v);

    /* Copy actual image row */
    memcpy(tblrow + ct->offset*ip_type_size(im->type), imrow, im->size.x*ip_type_size(im->type));
    /* Copy first value into padding at start of row */
    pad_row_bytype[im->type](tblrow, 0, ct->offset, ct->offset);
    /* Copy last value into padding at end of row */
    pad_row_bytype[im->type](tblrow, ct->offset+im->size.x, ct->length-ct->offset-im->size.x,
			     ct->offset+im->size.x-1);
}


#define generate_copy_image_col_function(type, tt)			\
    static void copy_image_col_ ## type(void *cptr, ip_image *im, int icol) \
    {									\
	type ## _type *tblrow = cptr;					\
									\
	for (int j=0; j<im->size.y; j++) {				\
	    *tblrow++ = IM_PIXEL(im, icol, j, tt);			\
	}								\
    }

generate_copy_image_col_function(UBYTE, ub);
generate_copy_image_col_function(USHORT, us);
generate_copy_image_col_function(ULONG, ul);
generate_copy_image_col_function(DOUBLE, d);

ct_function_pIi copy_image_col_bytype[IM_TYPEMAX] = {
    [IM_BINARY] = copy_image_col_UBYTE,
    [IM_UBYTE] = copy_image_col_UBYTE,
    [IM_BYTE] = copy_image_col_UBYTE,
    [IM_USHORT] = copy_image_col_USHORT,
    [IM_SHORT] = copy_image_col_USHORT,
    [IM_ULONG] = copy_image_col_ULONG,
    [IM_LONG] = copy_image_col_ULONG,
    [IM_FLOAT] = copy_image_col_ULONG,
    [IM_DOUBLE] = copy_image_col_DOUBLE,
};


static void copy_image_col(ip_chordtbl *ct, int trow, ip_image *im, int icol)
{
    char *tblrow = CHORDTBL_ROWADR(ct, trow);

    /* Ouch, this is likely to be inefficient due to cache misses galore */
    /* Copy actual image column */

    copy_image_col_bytype[ct->type](tblrow+ct->offset*ip_type_size(im->type), im, icol);
    /* Copy first value into padding at start of row */
    pad_row_bytype[im->type](tblrow, 0, ct->offset, ct->offset);
    /* Copy last value into padding at end of row */
    pad_row_bytype[im->type](tblrow, ct->offset+im->size.y, ct->length-ct->offset-im->size.y,
			     ct->offset+im->size.y-1);
}


static void calculate_row_for_each_unique_length(ip_chordtbl *ct, int trow)
{ 
    ip_strel *se = ct->strel;
    ct_function_piii row_update = NULL;

    /*
     * Choose the most appropriate row update operator, preferring a SIMD
     * implementation if possible.
     */
#ifdef HAVE_SIMD
    if (IP_USE_HWACCEL) {
	if (ct->operator) {
	    if (ip_simd_update_row_dilate_bytype[ct->type])
		row_update = ip_simd_update_row_dilate_bytype[ct->type];
	}else{
	    if (ip_simd_update_row_erode_bytype[ct->type])
		row_update = ip_simd_update_row_erode_bytype[ct->type];
	}
    }
#endif
    if (row_update == NULL) {
	/* No SIMD operator exists, use the default scalar code */
	if (ct->operator)
	    row_update = update_row_dilation_bytype[ct->type];
	else
	    row_update = update_row_erosion_bytype[ct->type];
    }

    /* For each unique chord length do */
    for (int j=1; j<se->num_unique; j++) {
	int d = se->ulength[j] - se->ulength[j-1];
	/* Dilation/Erosion by recursion to already calculated shorter chords */
	row_update(ct, trow, j, d);
    }
}   



static void ip_chord_table_init(ip_chordtbl *ct, ip_image *im)
{
    ip_strel *se = ct->strel;
    
    for (int r=0; r<ct->numrows; r++) {
	if (se->direction == 0) {
	    /* The chords are horizontal lines; process image by row */
	    int y = r + se->bbox.origin.y;
	    if (y < 0) y = 0;
	    if (y >= im->size.y) y = im->size.y - 1;
	    copy_image_row(ct, r, im, y);
	    ct->row = se->bbox.origin.y + ct->numrows - 1;
	}else{
	    /* The chords are vertical lines; process image by column */
	    int x = r + se->bbox.origin.x;
	    if (x < 0) x = 0;
	    if (x >= im->size.x) x = im->size.x - 1;
	    copy_image_col(ct, r, im, x);
	    ct->row = se->bbox.origin.x + ct->numrows - 1;
	}

	calculate_row_for_each_unique_length(ct, r);
    }
}


/*
 * Put pointers to chord table row into the structuring element chords to
 * save on repetitive calculation when calling ip_morph_process_image_row()
 * below.
 */
static void ip_strel_chord_ptrs_init(ip_strel *se, ip_chordtbl *ct)
{
    if (se->direction) {
	/* Process by columns */
	int trow_ofs = -se->bbox.origin.x;
	for (int j=0; j<se->num_chords; j++)
	    se->chords[j].ct_row =
		chordtbl_cl_rowadr(ct, se->chords[j].x + trow_ofs, se->chords[j].ulidx);
    }else{
	/* Process by rows */
	int trow_ofs = -se->bbox.origin.y;
	for (int j=0; j<se->num_chords; j++) {
	    se->chords[j].ct_row =
		chordtbl_cl_rowadr(ct, se->chords[j].y + trow_ofs, se->chords[j].ulidx);
	}
    }
}


void ip_chord_table_update(ip_chordtbl *ct, ip_image *im)
{
    /* Shift each row of the table up by one. */
    void *tmp = ct->ctrow[0];
    for (int j=0; j<ct->numrows-1; j++) {
	ct->ctrow[j] = ct->ctrow[j+1];
    }

    /* Point bottom row to the old top which we reuse for new data. */
    ct->ctrow[ct->numrows-1] = tmp;

    /* Have to recalculate ct row pointers in each chord */
    ip_strel_chord_ptrs_init(ct->strel, ct);

    /* Calculate bottom row */
    ct->row++;
    if (ct->strel->direction == 0)
	copy_image_row(ct, ct->numrows-1, im, (ct->row < im->size.y) ? ct->row : im->size.y-1);
    else
	copy_image_col(ct, ct->numrows-1, im, (ct->row < im->size.x) ? ct->row : im->size.x-1);

    calculate_row_for_each_unique_length(ct, ct->numrows-1);
}




ip_chordtbl *ip_alloc_chord_table(ip_image *im, ip_strel *strel, int operator)
{
    size_t tblrows, row_length, offset, row_size, tbl_size;
    ip_chordtbl *ct = NULL;

    ip_log_routine(__func__);

    if (strel->direction) {
	/* Vertical chords; thus table row is an extended column of image */
	tblrows = strel->bbox.size.x;
	row_length = im->size.y + strel->bbox.size.y -1;
	offset = -strel->bbox.origin.y;
    }else{
	/* Horizontal chords; thus table row is an extended row of the image */
	tblrows = strel->bbox.size.y;
	row_length = im->size.x + strel->bbox.size.x -1;
	offset = -strel->bbox.origin.x;
    }

    /* Pad every row to a multiple of the SIMD vector size */
    row_size = PADDED_ROW_SIZE(row_length * ip_type_size(im->type));
    tbl_size = tblrows * row_size * (size_t)strel->num_unique;

    if ((ct = ip_malloc(sizeof(*ct))) == NULL)
	goto exit_act;

    ct->type = (ip_image_type)im->type;
    ct->numrows = tblrows;
    ct->length = row_length;
    ct->row_size = row_size;
    ct->offset = offset;
    ct->strel = strel;
    ct->operator = operator;
    ct->table = NULL;
    ct->ctrow = NULL;

    if ((ct->table = ip_mallocx_align(tbl_size, ROW_ALIGNMENT)) == NULL)
	goto exit_act;

    if ((ct->ctrow = ip_mallocx(sizeof(void *) * tblrows)) == NULL)
	goto exit_act;

    for (int j=0; j<tblrows; j++)
	ct->ctrow[j] = (void *)(ct->table + j * row_size * strel->num_unique);

    /*
     * Initialise the chord table with first rows (columns) of image
     * data.
     */
    ip_chord_table_init(ct, im);

    /*
     * Put each chord's pointer into the chord table into the structuring
     * element.  This is for efficiency when running
     * ip_morph_process_image_row().
     */
    ip_strel_chord_ptrs_init(strel, ct);

    ip_pop_routine();
    return ct;

 exit_act:
    if (ct) {
	if (ct->ctrow)
	    ip_free(ct->ctrow);
	if (ct->table)
	    ip_free(ct->table);
	ip_free(ct);
    }
    ip_pop_routine();
    return NULL;
}



void ip_free_chord_table(ip_chordtbl *ct)
{
    if (ct) {
	if (ct->ctrow)
	    ip_free(ct->ctrow);
	if (ct->table)
	    ip_free(ct->table);
	ip_free(ct);
    }
}



#define generate_process_image_row_function(tt, opname, op)		\
    static void process_image_row_ ## opname ## _ ## tt(ip_image *im, ip_chordtbl *ct, int row) \
    {									\
	tt ## _type *iptr = IM_ROWADR(im, row, v);			\
	ip_strel *se = ct->strel;					\
									\
	for (int x=0; x<im->size.x; x++) {				\
	    tt ## _type *ctrow;						\
	    tt ## _type val;						\
									\
	    ctrow = se->chords[0].ct_row;				\
	    val = ctrow[se->chords[0].x + x + ct->offset];		\
									\
	    for (int c=1; c<se->num_chords; c++) {			\
		ctrow = se->chords[c].ct_row;				\
		val = op(val, ctrow[se->chords[c].x + x + ct->offset]);	\
	    }								\
	    *iptr++ = val;						\
	}								\
    }

generate_process_image_row_function(UBYTE, erosion, MIN)
generate_process_image_row_function(BYTE, erosion, MIN)
generate_process_image_row_function(USHORT, erosion, MIN)
generate_process_image_row_function(SHORT, erosion, MIN)
generate_process_image_row_function(ULONG, erosion, MIN)
generate_process_image_row_function(LONG, erosion, MIN)
generate_process_image_row_function(FLOAT, erosion, MIN)
generate_process_image_row_function(DOUBLE, erosion, MIN)

generate_process_image_row_function(UBYTE, dilation, MAX)
generate_process_image_row_function(BYTE, dilation, MAX)
generate_process_image_row_function(USHORT, dilation, MAX)
generate_process_image_row_function(SHORT, dilation, MAX)
generate_process_image_row_function(ULONG, dilation, MAX)
generate_process_image_row_function(LONG, dilation, MAX)
generate_process_image_row_function(FLOAT, dilation, MAX)
generate_process_image_row_function(DOUBLE, dilation, MAX)


ct_function_ITi erode_image_row_bytype[IM_TYPEMAX] = {
    [IM_BINARY] = process_image_row_erosion_UBYTE,
    [IM_UBYTE] = process_image_row_erosion_UBYTE,
    [IM_BYTE] = process_image_row_erosion_BYTE,
    [IM_USHORT] = process_image_row_erosion_USHORT,
    [IM_SHORT] = process_image_row_erosion_SHORT,
    [IM_ULONG] = process_image_row_erosion_ULONG,
    [IM_LONG] = process_image_row_erosion_LONG,
    [IM_FLOAT] = process_image_row_erosion_FLOAT,
    [IM_DOUBLE] = process_image_row_erosion_DOUBLE
};    

ct_function_ITi dilate_image_row_bytype[IM_TYPEMAX] = {
    [IM_BINARY] = process_image_row_dilation_UBYTE,
    [IM_UBYTE] = process_image_row_dilation_UBYTE,
    [IM_BYTE] = process_image_row_dilation_BYTE,
    [IM_USHORT] = process_image_row_dilation_USHORT,
    [IM_SHORT] = process_image_row_dilation_SHORT,
    [IM_ULONG] = process_image_row_dilation_ULONG,
    [IM_LONG] = process_image_row_dilation_LONG,
    [IM_FLOAT] = process_image_row_dilation_FLOAT,
    [IM_DOUBLE] = process_image_row_dilation_DOUBLE
};    


#define generate_process_image_col_function(type, tt, opname, op)	\
    static void process_image_col_ ## opname ## _ ## type(ip_image *im, ip_chordtbl *ct, int col) \
    {									\
	ip_strel *se = ct->strel;					\
									\
	for (int j=0; j<im->size.y; j++) {				\
	    type ## _type *ctrow;					\
	    type ## _type val;						\
									\
	    ctrow = se->chords[0].ct_row;				\
	    val = ctrow[se->chords[0].y + j + ct->offset];		\
									\
	    for (int c=1; c<se->num_chords; c++) {			\
		ctrow = se->chords[c].ct_row;				\
		val = op(val, ctrow[se->chords[c].y + j + ct->offset]);	\
	    }								\
	    IM_PIXEL(im, col, j, tt) = val;				\
	}								\
    }



generate_process_image_col_function(UBYTE, ub, erosion, MIN)
generate_process_image_col_function(BYTE, b, erosion, MIN)
generate_process_image_col_function(USHORT, us, erosion, MIN)
generate_process_image_col_function(SHORT, s, erosion, MIN)
generate_process_image_col_function(ULONG, ul, erosion, MIN)
generate_process_image_col_function(LONG, l, erosion, MIN)
generate_process_image_col_function(FLOAT, f, erosion, MIN)
generate_process_image_col_function(DOUBLE, d, erosion, MIN)

generate_process_image_col_function(UBYTE, ub, dilation, MAX)
generate_process_image_col_function(BYTE, b, dilation, MAX)
generate_process_image_col_function(USHORT, us, dilation, MAX)
generate_process_image_col_function(SHORT, s, dilation, MAX)
generate_process_image_col_function(ULONG, ul, dilation, MAX)
generate_process_image_col_function(LONG, l, dilation, MAX)
generate_process_image_col_function(FLOAT, f, dilation, MAX)
generate_process_image_col_function(DOUBLE, d, dilation, MAX)


ct_function_ITi erode_image_col_bytype[IM_TYPEMAX] = {
    [IM_BINARY] = process_image_col_erosion_UBYTE,
    [IM_UBYTE] = process_image_col_erosion_UBYTE,
    [IM_BYTE] = process_image_col_erosion_BYTE,
    [IM_USHORT] = process_image_col_erosion_USHORT,
    [IM_SHORT] = process_image_col_erosion_SHORT,
    [IM_ULONG] = process_image_col_erosion_ULONG,
    [IM_LONG] = process_image_col_erosion_LONG,
    [IM_FLOAT] = process_image_col_erosion_FLOAT,
    [IM_DOUBLE] = process_image_col_erosion_DOUBLE
};    

ct_function_ITi dilate_image_col_bytype[IM_TYPEMAX] = {
    [IM_BINARY] = process_image_col_dilation_UBYTE,
    [IM_UBYTE] = process_image_col_dilation_UBYTE,
    [IM_BYTE] = process_image_col_dilation_BYTE,
    [IM_USHORT] = process_image_col_dilation_USHORT,
    [IM_SHORT] = process_image_col_dilation_SHORT,
    [IM_ULONG] = process_image_col_dilation_ULONG,
    [IM_LONG] = process_image_col_dilation_LONG,
    [IM_FLOAT] = process_image_col_dilation_FLOAT,
    [IM_DOUBLE] = process_image_col_dilation_DOUBLE
};    


int im_morph_chord_erode(ip_image *im, ip_strel *se)
{
    ip_chordtbl *ct;
    int num_to_do = se->direction ? im->size.x : im->size.y;
    ct_function_ITi process_image_line = NULL;

    ip_log_routine(__func__);

    ct = ip_alloc_chord_table(im, se, 0);
    if (!ct) {
	ip_pop_routine();
	return ip_error;
    }

    /* Choose the best operator for writing the result back to image */
#ifdef HAVE_SIMD
    if (IP_USE_HWACCEL) {
	if (se->direction)
	    process_image_line = NULL; /* Not implemented. */
	else
	    process_image_line = ip_simd_process_imrow_erode_bytype[ct->type];
    }
#endif

    if (process_image_line == NULL) {
	/* No SIMD operator exists, use the default scalar code */
	if (se->direction)
	    process_image_line = erode_image_col_bytype[ct->type];
	else
	    process_image_line = erode_image_row_bytype[ct->type];
    }

    /*
     * Apply erosion operator to image row by row (or column by column if
     * vertical chords)
     */
    process_image_line(im, ct, 0);
    for (int row=1; row<num_to_do; row++) {
	ip_chord_table_update(ct, im);
	process_image_line(im, ct, row);
    }
    ip_free_chord_table(ct);

    ip_pop_routine();
    return OK;
}


int im_morph_chord_dilate(ip_image *im, ip_strel *se)
{
    ip_chordtbl *ct;
    int num_to_do = se->direction ? im->size.x : im->size.y;
    ct_function_ITi process_image_line = NULL;

    ip_log_routine(__func__);

    ip_strel_reflect(se);
    ct = ip_alloc_chord_table(im, se, 1);
    if (!ct) {
	ip_pop_routine();
	return ip_error;
    }

    /* Choose the best operator for writing the result back to image */
#ifdef HAVE_SIMD
    if (IP_USE_HWACCEL) {
	if (se->direction)
	    process_image_line = NULL; /* Not implemented. */
	else
	    process_image_line = ip_simd_process_imrow_dilate_bytype[ct->type];
    }
#endif

    if (process_image_line == NULL) {
	/* No SIMD operator exists, use the default scalar code */
	if (se->direction)
	    process_image_line = dilate_image_col_bytype[ct->type];
	else
	    process_image_line = dilate_image_row_bytype[ct->type];
    }

    /*
     * Apply dilation operator to image row by row (or column by column if
     * vertical chords)
     */
    process_image_line(im, ct, 0);
    for (int row=1; row<num_to_do; row++) {
	ip_chord_table_update(ct, im);
	process_image_line(im, ct, row);
    }

    ip_free_chord_table(ct);
    ip_strel_reflect(se);

    ip_pop_routine();
    return OK;
}




/*
 * Debugging functions follow
 */

#define generate_out_table_value_function(type, fmt)			\
    static void out_table_value_ ## type(FILE *f, ip_chordtbl *ct, int row, int lidx, int xpos) \
    {									\
	type ## _type *vptr = chordtbl_cl_rowadr(ct, row, lidx);	\
	fprintf(f, " " fmt, vptr[xpos]);				\
    }

generate_out_table_value_function(UBYTE, "%3u")
generate_out_table_value_function(BYTE, "%3d")
generate_out_table_value_function(USHORT, "%5u")
generate_out_table_value_function(SHORT, "%5d")
generate_out_table_value_function(ULONG, "%5u")
generate_out_table_value_function(LONG, "%5d")
generate_out_table_value_function(FLOAT, "%6.2g")
generate_out_table_value_function(DOUBLE, "%6.2g")

ct_function_fTiii out_table_value_bytype[IM_TYPEMAX] = {
    [IM_BINARY] = out_table_value_UBYTE,
    [IM_UBYTE] = out_table_value_UBYTE,
    [IM_BYTE] = out_table_value_BYTE,
    [IM_USHORT] = out_table_value_USHORT,
    [IM_SHORT] = out_table_value_SHORT,
    [IM_ULONG] = out_table_value_ULONG,
    [IM_LONG] = out_table_value_LONG,
    [IM_FLOAT] = out_table_value_FLOAT,
    [IM_DOUBLE] = out_table_value_DOUBLE
};


void ip_chord_table_dump(FILE *f, ip_chordtbl *ct, int flag)
{
    ip_strel *se = ct->strel;
    static char *op_name[] = {
	"erosion", "dilation"
    };

    fprintf(f, "Chord table for %s image at %p:\n", ip_type_name(ct->type), ct);
    fprintf(f, "  %s with structuring element at %p.\n", op_name[ct->operator], se);
    fprintf(f, "  table has %d rows of length %d and offset %d to edge of image.\n",
	    ct->numrows, ct->length, ct->offset);
    fprintf(f, "  last row in image that table was updated from: %d", ct->row);

    if (out_table_value_bytype[ct->type]) {
	for (int r=0; r<ct->numrows; r++) {
	    for (int l=0; l<se->num_unique; l++) {
		fprintf(f, "\n  Row %d, length %d: at %p",
			r, se->ulength[l], chordtbl_cl_rowadr(ct, r, l));
		if (flag & DBG_VERBOSE) {
		    for (int x=0; x<ct->length; x++) {
			if (x % 16 == 0)
			    fprintf(f, "\n    ");
			out_table_value_bytype[ct->type](f, ct, r, l, x);
		    }
		}
	    }
	}
    }else
	fprintf(f, "    invalid image type to print table contents.\n");
    fputc('\n', f);
}
