/*
   Module: ip/png.c
   Author: M.J.Cree

   Copyright (C) 2001-2007, 2015 Michael J. Cree

   Routines for reading in and saving PNG images.
*/


#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <time.h>
/*
 * png.h below includes setjmp.h and doesn't like it being loaded
 * beforehand.
 */
#if 0
#include <setjmp.h>
#endif

#define IPINTERNAL
#include <ip/ip.h>
#include <ip/file.h>
#include <ip/png.h>

#include "internal.h"
#include "utils.h"

#include <png.h>


static ip_image *rd_png_image(ip_png_handle *ph, int first_row, int nrows);
static int wr_png_image(const char *fname, ip_image *im, int flag);


/*

NAME:

   open_png_image() \- Open PNG image file ready for reading

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/png.h>

   ip_png_handle *open_png_image( filename )
   char * filename

DESCRIPTION:

   Open PNG image corresponding to 'filename'. Returns PNG handle if
   successfully opened or alternatively NULL if any errors occur. One
   can check image type and size in the PNG handle. This routine does
   not read in the actual image data, only the PNG image header.

   Filename may be a NULL pointer, in which case stdin is used as the file
   handle.

   Example of reading in a PNG image file:

\|   char *str;
\|   ip_png_handle *ph;
\|
\|   ph = open_png_image("fred");
\|   im = read_png_image(ph);
\|   close_png_image(ph);

ERRORS:

   ERR_NO_STDIN - Can't read from stdin
   ERR_OUT_OF_MEM
   ERR_PNG - libpng generated error
   ERR_INVALID_PNG_TYPE - Can't read that type of PNG image

SEE ALSO:

   open_png_image()   read_png_image()   read_png_imagex()
   read_png_tag()     close_png_image()
   write_png_mage()  write_png_imagex()
   ip_read_image()

*/

ip_png_handle *open_png_image(const char * fname)
{
   unsigned char header[4];
   ip_png_handle *ph;
   FILE *f;

   ip_log_routine("open_png_image");

   f = NULL;
   ph = NULL;

   /* Open disc file and read in first part in anticipation
      of PNG ip_image */

   if (fname) {
      if ((f = fopen(fname,"rb")) == NULL) {
         ip_log_error(ERR_FILE_OPEN,fname);
         goto exit_opi;
      }
   }else{
      f = stdin;
      fname = ip_msg_stdin;
   }

   if (fread(header,1,4,f) != 4) {
      ip_log_error(ERR_SHORT_FILE,fname);
      goto exit_opi;
   }

   ph = open_png_imagex(f, fname, header, NO_FLAG);

 exit_opi:			/* Error exit */

   if ((ph == NULL) && f && (f != stdin))
      fclose(f);

   ip_pop_routine();
   return ph;
}



ip_png_handle *open_png_imagex(FILE *f, const char *fname, unsigned char hdr[4],
                             int flag)
{
   ip_png_handle *ph;
   png_uint_32 height, width;

   ip_log_routine("open_png_imagex");

   ph = NULL;

   /* Allocate PNG handle. */

   if ((ph = ip_mallocx(sizeof(ip_png_handle))) == NULL) {
      goto exit_opix;
   }
   ph->f = f;
   ph->fname = fname;
   ph->png = NULL;
   ph->info = NULL;

   if (NOT png_check_sig(hdr, 4)) {
      ip_log_error(ERR_INVALID_PNG_TYPE,fname);
      goto exit_opix;
   }

   /* Pretty sure we've got a PNG file now, so go for establishing full
      connection of file with PNG library. */

   if ((ph->png = png_create_read_struct(PNG_LIBPNG_VER_STRING,
					 NULL,NULL,NULL)) == NULL) {
      ip_log_error(ERR_OUT_OF_MEM);
      goto exit_opix;
   }

   if ((ph->info = png_create_info_struct(ph->png)) == NULL) {
      ip_log_error(ERR_OUT_OF_MEM);
      goto exit_opix;
   }

   if (setjmp(png_jmpbuf(ph->png))) {
      ip_log_error(ERR_PNG,ph->fname);
      goto exit_opix;
   }

   png_init_io(ph->png, ph->f);
   png_set_sig_bytes(ph->png,4);

   /* Now read in PNG info chunk */

   png_read_info(ph->png, ph->info);

   png_get_IHDR(ph->png, ph->info, &width, &height, &ph->bps,
		&ph->ptype, &ph->interlace, &ph->comp, &ph->filter);

   ph->size.x = width;
   ph->size.y = height;

   /* Now map PNG image to IP library image type. */

   ph->type = -1;		/* An invalid type */
   switch (ph->ptype) {
   case PNG_COLOR_TYPE_GRAY:	/* Greyscale image */
   case PNG_COLOR_TYPE_GRAY_ALPHA:
      if (ph->bps == 1) {
	 ph->type = IM_BINARY;
      }else if (ph->bps <= 8) {
	 ph->type = IM_UBYTE;
      }else if (ph->bps == 16) {
	 ph->type = IM_USHORT;
      }
      break;
   case PNG_COLOR_TYPE_RGB:	/* RGB colour image */
   case PNG_COLOR_TYPE_RGB_ALPHA:
      if (ph->bps == 8)
	 ph->type = IM_RGB;
      else if (ph->bps == 16)
	 ph->type = IM_RGB16;
      break;
   case PNG_COLOR_TYPE_PALETTE: /* Palette colour image */
      ph->type = IM_PALETTE;
      break;
   }

   if (ph->type == -1) {
      ip_log_error(ERR_INVALID_PNG_TYPE,ph->fname);
      goto exit_opix;
   }

 exit_opix:

   if (ip_error != OK && ph != NULL) {
      if (ph->png) {
	 png_destroy_read_struct(&ph->png, &ph->info, NULL);
      }
      ip_free(ph);
      ph = NULL;
   }

   ip_pop_routine();
   return ph;
}




/*

NAME:

   read_png_image() \- Read in image data from opened PNG file

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/png.h>

   ip_image * read_png_image( handle )
   ip_png_handle * handle;

DESCRIPTION:

   Reads in PNG image corresponding to 'handle' from the
   disk. Returns the image data with image an opened image handle. One
   neads to call open_png_image() to get a valid PNG handle before
   calling this routine.  Returns NULL if an error occurs.

ERRORS:

   ERR_BAD_PNG_FILE - PNG file is not fully PNG compliant
   ERR_INVALID_PNG_TYPE - Unsupported PNG image file type
   ERR_PNG - libpng reported an error
   ERR_OUT_OF_MEM

SEE ALSO:

   open_png_image()   read_png_imagex()
   read_png_tag()     close_png_image()
   write_png_image()  write_png_imagex()
   ip_read_image()

*/

ip_image *read_png_image(ip_png_handle *ph)
{
   ip_log_routine("read_png_image");
   return  rd_png_image(ph, 0, ph->size.y);
}



/*

NAME:

   read_png_imagex() \- Read in part/whole of a PNG image

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/png.h>

   ip_image * read_png_imagex( handle, first_row, nrows )
   ip_png_handle * handle;
   int first_row;
   int nrows;

DESCRIPTION:

   Reads in png image rows corresponding to 'handle' from the
   disk. Returns an image. One needs to call open_png_image() to get
   a valid 'handle' before calling this routine.  Returns NULL if an
   error occurs.

   The first row of an image is numbered zero. Hence to read only the
   first row of an image specify 'first_row'=0 and 'nrows'=1.

   The image returned has 'nrows' rows in it.

ERRORS:

   ERR_OOR_PARAMETER
   ERR_BAD_PNG_FILE - PNG file is not fully PNG compliant
   ERR_INVALID_PNG_TYPE - Unsupported PNG image file type
   ERR_PNG - libpng reported an error
   ERR_OUT_OF_MEM

SEE ALSO:

   open_png_image()   read_png_image()
   read_png_tag()     close_png_image()
   write_png_image()  write_png_imagex()
   ip_read_image()

*/


ip_image *read_png_imagex(ip_png_handle *ph, int first_row, int nrows)
{
   ip_log_routine("read_png_imagex");
   return  rd_png_image(ph, first_row, nrows);
}



static ip_image *rd_png_image(ip_png_handle *ph, int first_row, int nrows)
{
   ip_image *im;
   png_colorp palette;
   int numpal;
   png_color_16 black;
   png_color_16p background;

   /* ip_log_routine() 'sposed to be done by calling routine.  */

   /* Check first_row and nrows are within range */

   if (NOT ip_parm_inrange("first_row", first_row, 0, ph->size.y-1))
      return NULL;

   if (NOT ip_parm_inrange("number_rows", nrows, 1, ph->size.y-first_row))
      return NULL;

   /* Allocate image */

   if ((im = ip_alloc_image(ph->type, (ip_coord2d){ph->size.x, nrows})) == NULL)
      goto exit_rpi;

   /* In case of PNG library error */

   if (setjmp(png_jmpbuf(ph->png))) {
      ip_log_error(ERR_PNG,ph->fname);
      goto exit_rpi;
   }

   /* SPecify PNG library transformations
      ie.

      Grey    1,2,4 bit -> 8 bit.
      Palette 1,2,4 bit -> 8 bit.

      If small endian machine do byte switch on 16 bit samples
   */

   if (ph->bps < 8)
      png_set_packing(ph->png);

   if (ph->ptype == PNG_COLOR_TYPE_GRAY && ph->bps < 8)
      png_set_expand(ph->png);

#ifdef HAVE_LITTLE_ENDIAN
   if (ph->bps == 16)
      png_set_swap(ph->png);
#endif

   /* For images with an alpha channel, composite the image
      with the background colour.  (Default background is black.) */

   if (png_get_bKGD(ph->png, ph->info, &background))
      png_set_background(ph->png, background,
			 PNG_BACKGROUND_GAMMA_FILE, 1, 1.0);
   else{
      black.red = black.green = black.blue = black.gray = 0;
      black.index = 0;
      png_set_background(ph->png, &black,
			 PNG_BACKGROUND_GAMMA_SCREEN, 0, 1.0);
   }

   png_read_update_info(ph->png, ph->info);

   /* Get colour palette for palette images. */

   if (im->type == IM_PALETTE) {
      int i;

      png_get_PLTE(ph->png, ph->info, &palette, &numpal);
      for (i=0; i<numpal && i<256; i++) {
	 im->palette[i].r = palette[i].red;
	 im->palette[i].g = palette[i].green;
	 im->palette[i].b = palette[i].blue;
      }
      for ( ; i<256; i++) {
	 im->palette[i].r = im->palette[i].g = im->palette[i].b = 0;
      }
      free(palette);
   }

   numpal = png_get_rowbytes(ph->png, ph->info);
   if (numpal != im->size.x*ip_image_type_info[im->type].typesize) {
      ip_log_error(ERR_ALGORITHM_FAULT,
		"Image rowlen [%d] mismatches PNG rowlen [%d].",
		im->size.x*ip_image_type_info[im->type].type,numpal);
      goto exit_rpi;
   }

   png_read_image(ph->png,(png_bytep *)im->imrow.v);

   png_read_end(ph->png,NULL);

 exit_rpi:

   if (ip_error != OK) {
      ip_free_image(im);
      im = NULL;
   }

   ip_pop_routine();
   return im;
}



/*

NAME:

   close_png_image() \- Close previously opened PNG image file

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/png.h>

   void close_png_image( handle )
   ip_png_handle *handle;

DESCRIPTION:

   Closes png file associated with 'handle'. 'Handle' is not valid
   anymore after calling close_png_image(). Safe to call with NULL
   'handle'.

SEE ALSO:

   open_png_image()

*/

void close_png_image(ip_png_handle *ph)
{
   ip_log_routine("close_png_image");

   if (ph) {
      if (ph->png) {
	 if (setjmp(png_jmpbuf(ph->png))) {
	    ip_log_error(ERR_PNG,ph->fname);
	 }else{
	    png_destroy_read_struct(&ph->png, &ph->info, NULL);
	 }
      }
      if (ph->f && ph->f != stdin) {
	 if (fclose(ph->f) != 0)
            ip_log_error(ERR_FILE_CLOSE,ph->fname);
      }
      ip_free(ph);
   }
   ip_pop_routine();
}




/*

NAME:

   write_png_image() \- Write image as a PNG file

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/png.h>

   int write_png_image( filename, im )
   const char * filename;
   ip_image * im;

DESCRIPTION:

   Writes the image 'im' as a PNG file to 'filename'. Any error conditions
   returned.  If 'filename' is NULL then 'im' is written to stdout.

LIMITATIONS:

   Only writes of BINARY, BYTE, SHORT, PALETTE, RGB and RGB16 images
   supported.

ERRORS:

   ERR_UNSUPPORTED_PNG_WRITE
   ERR_OUT_OF_MEM
   ERR_FILE_OPEN
   ERR_FILEIO
   ERR_FILE_CLOSE

SEE ALSO:

   open_png_image()   open_png_imagex()
   read_png_image()   read_png_imagex()
   close_png_image()
   write_png_imagex()
   ip_read_image()

*/


int write_png_image(const char *fname, ip_image *im)
{
   ip_log_routine("write_png_image");
   return wr_png_image(fname, im, NO_FLAG);
}



/*

NAME:

   write_png_imagex() \- Write image as a PNG file

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/png.h>

   int write_png_imagex( filename, im, flag )
   const char * filename;
   ip_image * im;
   int flag;

DESCRIPTION:

   Writes the image 'im' as a PNG file to 'filename'. Any error conditions
   returned.  If 'filename' is NULL then 'im' is written to stdout.

LIMITATIONS:

   Only writes of BINARY, BYTE, SHORT, PALETTE, RGB and RGB16 images
   supported.

ERRORS:

   ERR_UNSUPPORTED_PNG_WRITE
   ERR_OUT_OF_MEM
   ERR_FILE_OPEN
   ERR_FILEIO
   ERR_FILE_CLOSE

SEE ALSO:

   open_png_image()   open_png_imagex()
   read_png_image()   read_png_imagex()
   close_png_image()
   write_png_image()
   ip_read_image()

*/


int write_png_imagex(const char *fname, ip_image *im, int flag)
{
   ip_log_routine("write_png_imagex");
   return wr_png_image(fname, im, flag);
}


static int wr_png_image(const char *fname, ip_image *im, int flag)
{
   FILE *f;
   png_structp png;
   png_infop info;
   int ptype[IM_TYPETOTAL] = {
      -1,			/* Placeholder */
      PNG_COLOR_TYPE_GRAY,	/* IM_BINARY */
      -1,			/* IM_BYTE */
      PNG_COLOR_TYPE_GRAY,	/* IM_UBYTE */
      -1,			/* IM_SHORT */
      PNG_COLOR_TYPE_GRAY,	/* IM_USHORT */
      -1,			/* IM_LONG */
      -1,			/* IM_ULONG */
      -1,			/* IM_FLOAT */
      -1,			/* IM_DOUBLE */
      -1,			/* IM_COMPLEX */
      -1,			/* IM_DCOMPLEX */
      PNG_COLOR_TYPE_PALETTE,	/* IM_PALETTE */
      PNG_COLOR_TYPE_RGB,	/* IM_RGB */
      -1,			/* IM_RGBA */
      PNG_COLOR_TYPE_RGB	/* IM_RGB16 */
      -1,			/* IM_LONG64 */
      -1,			/* IM_ULONG64 */
   };
   int bps[IM_TYPETOTAL] = {
      -1,			/* Placeholder */
      1,			/* IM_BINARY */
      -1,			/* IM_BYTE */
      8,			/* IM_UBYTE */
      -1,			/* IM_SHORT */
      16,			/* IM_USHORT */
      -1,			/* IM_LONG */
      -1,			/* IM_ULONG */
      -1,			/* IM_FLOAT */
      -1,			/* IM_DOUBLE */
      -1,			/* IM_COMPLEX */
      -1,			/* IM_DCOMPLEX */
      8,			/* IM_PALETTE */
      8,			/* IM_RGB */
      -1,			/* IM_RGBA */
      16			/* IM_RGB16 */
      -1,			/* IM_LONG64 */
      -1,			/* IM_ULONG64 */
   };

   if (fname == NULL) {
      f = stdout;
      fname = ip_msg_stdout;
   }else{
      f = NULL;
   }

   png = NULL;
   info = NULL;

   /* No such thing as complex or double png images. */

   if (ptype[im->type] == -1) {
      ip_log_error(ERR_UNSUPPORTED_PNG_WRITE,fname);
      goto exit_wpi;
   }

   /* Open file for writing */

   errno = 0;
   if (f != stdout) {
      if ((f = fopen(fname,"wb")) == NULL) {
         ip_log_error(ERR_FILE_OPEN,fname);
         goto exit_wpi;
      }
   }

   /* Initialise png handles */

   png = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
   info = png_create_info_struct(png);

   if (png == NULL || info == NULL) {
      ip_log_error(ERR_OUT_OF_MEM);
      goto exit_wpi;
   }

   /* Hangle libpng errors */

   if (setjmp(png_jmpbuf(png))) {
      ip_log_error(ERR_PNG,fname);
      goto exit_wpi;
   }

   png_init_io(png, f);

   /* Tell PNG library the necessary imago info and transformations */

   png_set_IHDR(png, info, im->size.x, im->size.y, bps[im->type],
		ptype[im->type],PNG_INTERLACE_NONE,
		PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT);

   if (im->type == IM_PALETTE) {
      png_set_PLTE(png, info, (png_colorp)im->palette, 256);
   }

   png_write_info(png, info);

#ifdef HAVE_LITTLE_ENDIAN
   if (im->type == IM_SHORT || im->type == IM_RGB16) {
      png_set_swap(png);	/* Byte swap 16 bit data */
   }
#endif

   if (im->type == IM_BINARY) {
      png_set_packing(png);	/* Pack binary images */
   }

   png_write_image(png, (png_bytepp)im->imrow.v);

   png_write_end(png, NULL);

 exit_wpi:

   if (png)
      png_destroy_write_struct(&png, &info);

   if (f && f != stdout) {
      if (fclose(f) != 0 && ip_error == OK) {
         ip_log_error(ERR_FILE_CLOSE, fname);
      }
   }

   ip_pop_routine();

   return ip_error;
}
