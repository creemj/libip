/*
   Module: ip/raw.c
   Author: M. J. Cree

   Copyright (C) 1993-2007, 2014 Michael J. Cree

   Routines for reading in and writing out raw images.

*/

#include <stdlib.h>
#include <stdio.h>
#include <errno.h>

#include <ip/ip.h>
#include <ip/file.h>
#include <ip/raw.h>

#include "internal.h"
#include "utils.h"

/*

NAME:

   write_raw_image() \- Write raw image to file

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/raw.h>

   int write_raw_image( filename, im )
   const char * filename;
   ip_image * im;

DESCRIPTION:

   Writes image as raw file to filename. Any error conditions returned.  If
   filename is NULL then writes to stdout.

LIMITATIONS:

   PALETTE image is written as a BYTE image (that is, the palette is
   not saved.)
   BINARY image write not implemented.

ERRORS:

   ERR_FILE_OPEN
   ERR_FILEIO
   ERR_FILE_CLOSE
   check errno for more specific information on error.

   ERR_NOT_IMPLEMENTED - for BINARY images.

*/

int write_raw_image(const char *fname, ip_image *im)
{
    FILE *f = NULL;

    ip_log_routine(__func__);

    if (im->type == IM_BINARY) {
	ip_log_error(ERR_NOT_IMPLEMENTED);
	goto exit_wri;
    }

    /* Open file for writing. */
    errno = 0;
    if (fname) {
	if ((f = fopen(fname, "wb")) == NULL) {
	    ip_log_error(ERR_FILE_OPEN, fname);
	    goto exit_wri;
	}
    }else{
	f = stdout;
	fname = ip_msg_stdout;
    }

    /* Write image out row by row */
    for (int j=0; j<im->size.y; j++) {
	void *row = IM_ROWADR(im, j, v);
	size_t typesize = ip_image_type_info[im->type].typesize;

	if (fwrite(row, typesize, im->size.y, f) != im->size.y) {
	    ip_log_error(ERR_FILEIO,fname);
	    goto exit_wri;
	}
    }

 exit_wri:
    if (f && f != stdout)
	if (fclose(f) != 0)
	    ip_log_error(ERR_FILE_CLOSE,fname);

    ip_pop_routine();
    return ip_error;
}




/*

NAME:

   read_raw_image() \- Read in raw image from file

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/raw.h>

   ip_image * read_raw_image( fname, type, Xsize, Ysize )
   const char * fname;
   int type;
   int Xsize, int Ysize;

DESCRIPTION:

   Reads in raw image corresponding to fname from the disk. Returns the
   image data with image an opened image handle.  If fname is NULL then
   reads from stdin.  Returns NULL if an error occurs.

   This routine is defined in the header file as a static inline function
   calling read_raw_imagex(fname, type, Xsize, Ysize, 0, 0, 0, NO_FLAG).

*/



/*

NAME:

   read_raw_imagex() \- Read in raw image from file

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/raw.h>

   ip_image * read_raw_imagex( fname, type, Nx, Ny, 
			       headskip, rowskip, itemskip, flag )
   const char *fname;
   int type;
   int Nx, Ny;
   long headskip, rowskip, itemskip;
   int flag;

DESCRIPTION:

   This performs the same function as read_raw_image() but is a much more
   general interface. If headskip is non-zero, then headskip bytes are
   skipped before reading in the image data proper.  If rowskip is non-zero
   then rowskip bytes are skipped in between reading each row of image.  If
   fname is NULL then reads from stdin.

   Set headskip, rowskip and/or itemskip to zero to disable these features.

   The itemskip feature is not implemented; always set itemskip to zero.

   The flag is currently unused; always set flag to be NO_FLAG.

LIMITATIONS:

   Itemskip operation not implemented.  Set itemskip to zero.

   Headskip and rowskip implemented with fseek().  The use of skips may
   cause errors when reading a non-seekable file (such as a pipe).

   PALETTE images read in as BYTE images and palette is set to grey since
   can't read in the associated palette.

BUGS:

   BINARY images only correctly read if rowsize is a multiple of eight.
   This should be fixed!

ERRORS:

   ERR_NOT_IMPLEMENTED (for bad itemskip or flag).
   ERR_FILE_OPEN
   ERR_FILEIO_OR_TOO_SHORT (read errno to verify whether I/O error occurred)
   ERR_FILE_CLOSE

*/


ip_image *read_raw_imagex(const char * fname, int type, int Nx, int Ny,
			  long headskip, long rowskip, long itemskip, int flag)
{
    ip_image *im;
    FILE *f;

    ip_log_routine("read_raw_image");

    if (itemskip || flag != NO_FLAG) {
	ip_log_error(ERR_NOT_IMPLEMENTED);
	ip_pop_routine();
	return NULL;
    }

    /* Allocate image */

    if ((im = ip_alloc_image(type, (ip_coord2d){Nx, Ny})) == NULL) {
	ip_pop_routine();
	return NULL;
    }

    errno = 0;
    if (fname) {
	if ((f = fopen(fname,"rb")) == NULL) {
	    ip_log_error(ERR_FILE_OPEN, fname);
	    goto exit_rri;
	}
    }else{
	f = stdin;
	fname = ip_msg_stdin;
    }

    /* Skip header */

    if (headskip != 0) {
	if (fseek(f, headskip, SEEK_SET) != 0) {
	    ip_log_error(ERR_FILEIO_OR_TOO_SHORT, fname);
	    goto exit_rri;
	}
    }

    /* Read in image. */

    if (im->type == IM_BINARY) {
	/*
         * Binary images
	 * TBD this code is not sufficiently general and requires
         *     reworking/improvement.
	 */
	uint8_t *rowbuf, *iptr;
	int mask;

	if ((rowbuf = ip_mallocx(im->size.x/8)) == NULL) {
	    goto exit_rri;
	}

	for (int j=0; j<im->size.y; j++) {
	    if (rowskip) {
		long pos = (long)headskip + j*((ip_image_type_info[type].typesize*(long)Ny+7)/8+rowskip);
		if (fseek(f, pos, SEEK_SET) != 0) {
		    ip_log_error(ERR_FILEIO_OR_TOO_SHORT, fname);
		    goto exit_rri;
		}
	    }
	    if (fread(rowbuf, 1, im->size.x/8, f) != im->size.x/8) {
		ip_log_error(ERR_FILEIO_OR_TOO_SHORT, fname);
		goto exit_rri;
	    }
	    mask = 0x80;
	    iptr = im_rowadr(im, j);
	    for (int i=0; i<im->size.x; i++) {
		*iptr++ = (rowbuf[i/8] & mask) ? 255 : 0;
		mask >>= 1;
		if (mask == 0) mask = 0x80;
	    }
	}

	ip_free(rowbuf);

    }else{
	/* All other image types */

	for (int j=0; j<Ny; j++) {
	    if (rowskip) {
		if (fseek(f, rowskip * ip_image_type_info[type].typesize, SEEK_CUR) != 0) {
		    ip_log_error(ERR_FILEIO_OR_TOO_SHORT, fname);
		    goto exit_rri;
		}
	    }
	    if (fread(IM_ROWADR(im, j, v), ip_image_type_info[type].typesize, Nx, f) != Nx) {
		ip_log_error(ERR_FILEIO_OR_TOO_SHORT, fname);
		goto exit_rri;
	    }
	}

    }

 exit_rri:
    if (f && f != stdin)
	if (fclose(f) != 0)
	    ip_log_error(ERR_FILE_CLOSE, fname);

    if (ip_error != OK) {
	ip_free_image(im);
	im = NULL;
    }

    ip_pop_routine();
    return im;
}
