/*
   Module: ip/utils.c
   Author: M.J.Cree

   Copyright (C) 1995-1998, 2007, 2013, 2015-2016 Michael J. Cree

   Some useful utility routines of the IP library.

*/

/* to get posix_memalign() definition */
#define _XOPEN_SOURCE 600

#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <ip/ip.h>

#include "internal.h"
#include "utils.h"

#ifdef IP_DEBUG
#include <mjc/malloc.h>
#endif


/*

NAME:

   eucldist() \- Euclidean distance between two coordinates

PROTOTYPE:

   #include <ip/ip.h>

   double  eucldist( x, y )
   ip_coord2d x;
   ip_coord2d y;

DESCRIPTION:

   Calculates the Euclidean distance between the two coordinates 'x' and
   'y'.

   Defined as an inlined function in utils.h.

*/

/* Defined as static inlined in utils.h */
#if 0
double eucldist(ip_coord2d x, ip_coord2d y)
{
    int xs,ys;

    xs = y.x - x.x;
    ys = y.y - x.y;
    return sqrt((double)xs * xs + ys * ys);
}
#endif


/*

NAME:

   ip_malloc() \- ip library memory allocation routine

PROTOTYPE:

   #include <ip/ip.h>

   void * ip_malloc( size )
   size_t size;

DESCRIPTION:

   All memory allocations that the ip library performs go through this
   routine or one of ip_mallocx() or ip_realloc().

   If the memory allocation fails this routine flags the error
   ERR_OUT_OF_MEM and calls ip_pop_routine().

ERRORS:

   ERR_OUT_OF_MEM

SEE ALSO:

   ip_mallocx()   ip_mallocx_align()     ip_realloc()
   ip_mallocx_with_row_alignment()       ip_free()

*/

void *ip_malloc(size_t size)
{
    void *ptr;

    if (ip_verbose & 0x02) {
	fflush(stdout);
	ip_printlog();
	fprintf(stderr,": malloc size = %lu",(unsigned long)size);
    }
    if ((ptr = malloc(size)) == NULL) {
	ip_log_error(ERR_OUT_OF_MEM);
	ip_pop_routine();
    }
    if (ip_verbose & 0x02) {
	fprintf(stderr," at %p\n",ptr);
    }
    return ptr;
}



/*

NAME:

   ip_mallocx() \- ip library memory allocation routine

PROTOTYPE:

   #include <ip/ip.h>

   void * ip_mallocx( size )
   size_t size;

DESCRIPTION:

   All memory allocations that the ip library does go through one of
   ip_malloc, ip_mallocx() or ip_realloc().

   Ip_mallocx() is identical to ip_malloc() except it doesn''t call
   ip_pop_routine() if the memory allocations fails.

ERRORS:

   ERR_OUT_OF_MEM

SEE ALSO:

   ip_malloc()    ip_mallocx_align()     ip_realloc()
   ip_mallocx_with_row_alignment()       ip_free()

*/

void *ip_mallocx(size_t size)
{
    void *ptr;

    if (ip_verbose & 0x02) {
	fflush(stdout);
	ip_printlog();
	fprintf(stderr,": malloc size = %lu",(unsigned long)size);
    }
    if ((ptr = malloc(size)) == NULL) {
	ip_log_error(ERR_OUT_OF_MEM);
    }
    if (ip_verbose & 0x02) {
	fprintf(stderr," at %p\n",ptr);
    }
    return ptr;
}



/*

NAME:

   ip_mallocx_align() \- ip library memory allocation routine

PROTOTYPE:

   #include <ip/ip.h>

   void * ip_mallocx_align( size, alignment )
   size_t size;
   size_t alignment;

DESCRIPTION:

   All memory allocations that the ip library does go through one of
   ip_malloc, ip_mallocx(), ip_mallocx_align() or ip_realloc().

   Ip_mallocx_align() is identical to ip_malloc() except that it does not
   call ip_pop_routine() on failure and that the alignment of the memory
   allocation can be specified.  Alignment must be a power of 2 and greater
   than or equal to sizeof(void *).

ERRORS:

   ERR_OUT_OF_MEM

SEE ALSO:

   ip_malloc()    ip_mallocx()           ip_realloc()
   ip_mallocx_with_row_alignment()       ip_free()

*/

void *ip_mallocx_align(size_t size, size_t alignment)
{
    void *ptr;

    if (ip_verbose & 0x02) {
	fflush(stdout);
	ip_printlog();
	fprintf(stderr,": malloc size = %lu",(unsigned long)size);
    }
    if (posix_memalign(&ptr, alignment, size)) {
	ip_log_error(ERR_OUT_OF_MEM);
    }
    if (ip_verbose & 0x02) {
	fprintf(stderr," at %p\n",ptr);
    }
    return ptr;
}



/*

NAME:

   ip_mallocx_with_row_alignment() \- ip library memory allocation routine

PROTOTYPE:

   #include <ip/ip.h>

   void * ip_mallocx_with_row_alignment( size )
   size_t size;

DESCRIPTION:

   Ip_mallocx_with_row_alignment() calls ip_mallocx() or ip_mallocx_align()
   as appropriate to compilation conditions.  In particular it aligns
   sufficiently for efficient and safe SIMD vector access to the allocated
   memory.

   Defined as an inlined function in utils.h.

ERRORS:

   ERR_OUT_OF_MEM

SEE ALSO:

   ip_malloc()    ip_mallocx()    ip_mallocx_align()    ip_realloc()
   ip_free()

*/



/*

NAME:

   ip_realloc() \- ip libary memory re-allocation routine

PROTOTYPE:

   #include <ip/ip.h>

   void * ip_realloc( ptr, size )
   void *ptr;
   size_t size;

DESCRIPTION:

   All memory allocations that the ip library does go through
   one of ip_malloc, ip_mallocx() or ip_realloc().

   If the memory re-allocation fails this routine flags the error
   ERR_OUT_OF_MEM and calls ip_pop_routine().

ERRORS:

   ERR_OUT_OF_MEM

SEE ALSO:

   ip_malloc()   ip_mallocx()           ip_mallocx_align()
   ip_mallocx_with_row_alignment()      ip_free()

*/


void *ip_realloc(void *ptr, size_t size)
{
    if (ip_verbose & 0x02) {
	fflush(stdout);
	ip_printlog();
	fprintf(stderr,": realloc mem at %p, new size = %lu\n",
			ptr,(unsigned long)size);
    }
    if ((ptr = realloc(ptr,size)) == NULL && size != 0) {
	ip_log_error(ERR_OUT_OF_MEM);
	ip_pop_routine();
    }
    return ptr;
}


void *ip_reallocx(void *ptr, size_t size)
{
    if (ip_verbose & 0x02) {
	fflush(stdout);
	ip_printlog();
	fprintf(stderr,": realloc mem at %p, new size = %lu\n",
			ptr,(unsigned long)size);
    }
    if ((ptr = realloc(ptr,size)) == NULL && size != 0) {
	ip_log_error(ERR_OUT_OF_MEM);
    }
    return ptr;
}


/*

NAME:

   ip_free() \- ip library free memory routine

PROTOTYPE:

   #include <ip/ip.h>

   void  ip_free( p )
   void *p;

DESCRIPTION:

   All ip library memory allocations are freed via this routine.  If a
   NULL pointer is past this routine flags an error.

ERRORS:

   ERR_BAD_MEM_FREE

SEE ALSO:

   ip_malloc()   ip_mallocx()   ip_mallocx_align()   ip_realloc()
   ip_mallocx_with_row_alignment()

*/


void ip_free(void *p)
{
    if (ip_verbose & 0x02) {
	fflush(stdout);
	ip_printlog();
	fprintf(stderr,": free mem at %p\n",p);
    }
    if (p)
	free(p);
    else
	ip_log_error(ERR_BAD_MEM_FREE);
}




/*

NAME:

   ip_strdup() \- Duplicates a string using ip malloc routines

PROTOTYPE:

   #include <ip/ip.h>

   char * ip_strdup( src )
   char *src;

DESCRIPTION:

   Allocates memory using ip malloc routines for a copy of the string 'src'.
   Copies 'src' into the newly allocated memory and returns its address.

   Returns NULL if memory allocation fails.  Uses routine ip_malloc() to do
   memory allocation so one routine is popped off routine stack if a memory
   allocation failure occurs.

ERRORS:

   ERR_OUT_OF_MEM

SEE ALSO:

   ip_strdupx()   ip_malloc()

*/

char *ip_strdup(char *src)
{
    char *dest;

    if ((dest = ip_malloc(strlen(src)+1)) != NULL)
	strcpy(dest,src);
    return dest;
}



/*

NAME:

   ip_strdupx() \- Duplicates a string using ip malloc routines

PROTOTYPE:

   #include <ip/ip.h>

   char * ip_strdupx( src )
   char *src;

DESCRIPTION:

   Allocates memory using ip malloc routines for a copy of the string 'src'.
   Copies 'src' into the newly allocated memory and returns its address.

   Returns NULL if memory allocation fails.  Uses routine ip_mallocx() to do
   memory allocation so no routines are popped off routine stack.

ERRORS:

   ERR_OUT_OF_MEM

SEE ALSO:

   ip_strdup()   ip_mallocx()

*/


char *ip_strdupx(char *src)
{
    char *dest;

    if ((dest = ip_mallocx(strlen(src)+1)) != NULL)
	strcpy(dest,src);
    return dest;
}


/*

 NAME:

   ip_value_splat() \- Splat a scalar value into an unsigned long

PROTOTYPE:

   #include <ip/ip.h>

   char * ip_value_splat(type, val, val_splat)
   ip_image_type type;
   double val;
   unsigned long *val_splat;

DESCRIPTION:

   Splat a scalar value of image type 'type' across an unsigned long.  For
   example, if 'type' is IM_UBYTE then the value 'val' is copied across to
   all bytes of an unsigned long (four copies if 32 bit and eight copies of
   'val' if an unsigned long is 64 bits) and written to the memory address
   pointed to by val_splat.

   Returns TRUE if the value 'val' could be splatted into 'val_splat' and
   FALSE otherwise.  In particular UBYTE, BYTE, USHORT, SHORT, ULONG and
   LONG can always be splatted (though on a 32 bit architecture only one
   copy of a LONG results from the splat).  FLOAT can always be splatted but
   DOUBLE can only be splatted on a 64 bit architecture (with only one copy
   of the double resulting). BINARY and PALETTE are splat as UBYTE.

   No check is made that 'val' is in range of the specified image type.  If
   it is not, then unexpected values of 'val_splat' may result.

   Only scalar types are splatted, thus RGB types and COMPLEX do not splat
   and FALSE is returned.
*/

/* Macros used in ip_value_splat() */
#if MACHINE_NATURAL_SIZE == 4
#define SPLAT_SHORT(x) ((x) | ((x)<<16))
#define SPLAT_LONG(x) (x)
#else
#define SPLAT_SHORT(x) ((x) | ((x)<<16) | ((x)<<32) | ((x)<<48))
#define SPLAT_LONG(x) ((x) | ((x)<<32))
#endif
#define SPLAT_BYTE(x) SPLAT_SHORT((x) | ((x)<<8))


int ip_value_splat(int type, double val, unsigned long *val_splat)
{
    int succeeded = 1;
    union {
	signed long sval;
	unsigned long uval;
#if MACHINE_NATURAL_SIZE == 4
	float fval;
#else
	float fval[2];
	double dval;
	ip_complex cval;
#endif
    } vals;

    switch (type) {
    case IM_BINARY:
    case IM_PALETTE:
    case IM_UBYTE:
	vals.uval = (unsigned long)ROUND(val);
	vals.uval &= 0xff;
	*val_splat = SPLAT_BYTE(vals.uval);
	break;
    case IM_BYTE:
	vals.sval = (signed long)ROUND(val);
	vals.uval &= 0xff;
	*val_splat = SPLAT_BYTE(vals.uval);
	break;
    case IM_USHORT:
	vals.uval = (unsigned long)ROUND(val);
	vals.uval &= 0xffff;
	*val_splat = SPLAT_SHORT(vals.uval);
	break;
    case IM_SHORT:
	vals.sval = (signed long)ROUND(val);
	vals.uval &= 0xffff;
	*val_splat = SPLAT_SHORT(vals.uval);
	break;
    case IM_ULONG:
	vals.uval = (unsigned long)ROUND(val);
	vals.uval &= 0xffffffff;
	*val_splat = SPLAT_LONG(vals.uval);
	break;
    case IM_LONG:
	vals.sval = (signed long)ROUND(val);
	vals.uval &= 0xffffffff;
	*val_splat = SPLAT_LONG(vals.uval);
	break;
#if MACHINE_NATURAL_SIZE == 4
    case IM_FLOAT:
	vals.fval = (float)val;
	*val_splat = vals.uval;
	break;
#else
    case IM_FLOAT:
	vals.fval[0] = vals.fval[1] = (float)val;
	*val_splat = vals.uval;
	break;
    case IM_DOUBLE:
	vals.dval = val;
	*val_splat = vals.uval;
	break;
#endif
    default:
	succeeded = 0;
	break;
    }

    return succeeded;
}
