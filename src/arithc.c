/*
   Module: ip/arithc.c
   Author: M.J.Cree

   Arithmetic functions on an image involving the image and a constant.
   See arith.c for binary arithmetic between two images.

   Copyright (C) 1995-2013, 2015, 2017 Michael J. Cree

ENTRY POINTS:

   im_addc()
   im_addc_complex()
   im_addc_rgb()
   im_subc()
   im_subc_complex()
   im_subc_rgb()
   im_mulc()
   im_mulc_complex()
   im_mulc_rgb()
   im_divc()
   im_divc_complex()

*/

#include <stdlib.h>
#include <math.h>

#include "internal.h"

#include <ip/ip.h>
#include <ip/complex.h>
#include <ip/divide.h>

#include "utils.h"
#include "function_ops.h"

#ifdef HAVE_SIMD
#include "simd/arithc_simd.h"
#endif

/*
 * Macros to implement poor man's SIMD.  These provide faster addition and
 * subtraction on BYTE and SHORT orientated images particularly on 64 bit
 * arches.  Performance gains may be less impressive (maybe even losses?) on
 * 32 bit arches.
 *
 * Method 1 seems to be fastest on most arches.
 */

#define USE_FAST_CODE_METHOD_1

#ifdef USE_FAST_CODE_METHOD_1
/* First method */

#define IMROW_FAST_ADDC(d, a1, a2, row, len)	\
    unsigned long *dptr;			\
    dptr = IM_ROWADR(d, row, v);		\
    for (int i=0; i<len; i++) {			\
	unsigned long dc = *dptr;		\
	unsigned long signs = (a1 ^ dc) & m1;	\
	dc &= m2;				\
        *dptr++ = (dc + a2) ^ signs;		\
    }


#define IMROW_FAST_SUBC(d, a1, a2, row, len)	\
    unsigned long *dptr;			\
    dptr = IM_ROWADR(d, row, v);		\
    for (int i=0; i<len; i++) {			\
        unsigned long dc = *dptr;		\
	unsigned long signs = (a1 ^ dc) & m1;	\
	dc |= m1;				\
        *dptr++ = (dc - a2) ^ signs;		\
    }
#else
/* Second method */

/* Pass a1 = c & m1;  a2 = c & m2; on entry to macro */

#define IMROW_FAST_ADDC(d, a1, a2, row, len)	\
    unsigned long *dptr;			\
    unsigned long w1, w2;                       \
    dptr = IM_ROWADR(d, row, v);                \
    for (int i=0; i<len; i++) {                 \
        w1 = w2 = *dptr;                        \
        w1 &= m1;                               \
        w2 &= m2;                               \
        w1 += a1;                               \
        w2 += a2;                               \
        w1 &= m1;                               \
        w2 &= m2;                               \
        *dptr++ = w1 | w2;                      \
    }

/* Pass a1 = c & m1;  a2 = c & m2; on entry to macro */

#define IMROW_FAST_SUBC(d, a1 , a2, row, len)	\
    unsigned long *dptr;			\
    unsigned long w1, w2;                       \
    dptr = IM_ROWADR(d, row, v);                \
    for (int i=0; i<len; i++) {                 \
        w1 = w2 = *dptr;                        \
        w1 |= m2;                               \
        w2 |= m1;                               \
        w1 -= a1;                               \
        w2 -= a2;                               \
        w1 &= m1;                               \
        w2 &= m2;                               \
        *dptr++ = w1 | w2;                      \
    }

#endif


/*
 * Macros to process image pixelwise (PROCESS_IMAGE)
 */

#define PROCESS_IMAGE(d, c, t, op) {					\
	int rowlen = ip_type_size(d->type) / ip_type_size(IM_ ## t);	\
	rowlen *= d->size.x;						\
	for (int j=0; j<d->size.y; j++) {				\
	    t ## _type *dptr = IM_ROWADR(d, j, v);			\
	    for (int i=0; i<rowlen; i++) {				\
		*dptr = pixel_ ## op ## _ ## t(*dptr, c);		\
		dptr++;							\
	    }								\
	}								\
    }

#define PROCESS_IMAGE_with_SHIFT(d, c, sh, t, op) {			\
	int rowlen = ip_type_size(d->type) / ip_type_size(IM_ ## t);	\
	rowlen *= d->size.x;						\
	for (int j=0; j<d->size.y; j++) {				\
	    t ## _type *dptr = IM_ROWADR(d, j, v);			\
	    for (int i=0; i<rowlen; i++) {				\
		*dptr = pixel_ ## op ## _ ## t(*dptr, c, sh);		\
		dptr++;							\
	    }								\
	}								\
    }

/*
 * For defining a static inline function being a pixel operator to use in
 * generate_image_same_function() above.
 */
#define generate_pixel_op(type, opname, op)			    \
    static inline type ## _type					    \
    pixel_ ## opname ## _ ## type(type ## _type d, type ## _type c) \
    {								    \
	return (op);						    \
    }


/*
 * Definition of pixel operators for the multiplication of an integer pixel
 * value by a non-integral value with fixed point arithmetic.
 *
 * On a 64-bit system we provide for a 32 whole number and a 32 bit
 * fraction.
 *
 * On 32-bit systems, when processing (U)BYTE data we allow precision to
 * slip as low as 16-bits whole number/16-bits fraction to restrict
 * calculations to 32-bits, thus get the best compromise between precision
 * and speed.
 *
 * First the definition of constants, being the value of one-half and unity
 * in the fixed point representation, and the best method of rounding the
 * floating-point value to the fixed point precision.
 *
 * The rounding is sutble.  The use of lround() is good on 64-bit arches and
 * ensures we convert the full precision (53-bits) of the double precision
 * number into a 64-bit fixed-point integer.  But it can be a problem on
 * 32-bit arches even though long is 32-bits and we do want to convert to a
 * 32-bit number.  The problem is that it converts to signed long (32-bits)
 * only and may produce incorrect results when we really want to convert to
 * unsigned long (32-bits).  Hence we use round() below on the 24 and 16-bit
 * fraction fixed-point round macros, and leave the conversion to an integer
 * type to be put into the generate macros further below.
 */

#ifdef HAVE_64BIT
#define FIXED_32_HALF (1UL << 31)
#define FIXED_32_ONE (1UL << 32)
#define FIXED_32_ROUND(x) lround(x)
#else
#define FIXED_32_HALF (1ULL << 31)
#define FIXED_32_ONE (1ULL << 32)
#define FIXED_32_ROUND(x) llround(x)
#define FIXED_24_HALF (1UL << 23)
#define FIXED_24_ONE (1UL << 24)
#define FIXED_24_ROUND(x) round(x)
#define FIXED_16_HALF (1UL << 15)
#define FIXED_16_ONE (1UL << 16)
#define FIXED_16_ROUND(x) round(x)
#endif


/*
 * Fixed point multiplication operator.  First 32-bit whole number plus
 * 32-bit fraction.  This is a nice representation to use on 64-bit arches,
 * and may be effective on some 32-bit arches if they have a 32-bit umulhi
 * equivalent instruction or a fused integer muliply-add that returns the
 * upper 32-bits.
 *
 * Let the multiplier passed in the im_mulc() routine be val.  Then: If val
 * >= 0.5 then let cval = val and use the imulf pixel operators.  If val <
 * 0.5 then let cval = 1/val and use the idivf pixel operators.
 *
 * The imulf operators are designed such that 32-bit numbers when muliplied
 * by numbers greater than 1 are truncated at the 32-bits whole number and
 * always have 32-bits of fraction in the calculation.  If used with
 * multipliers less than 0.5 the imulf operator loses precision, i.e., there
 * are fewer than 32-bits of fraction calculated. The idivf operators, in
 * contrast, are designed for multipliers less than 0.5 and do not lose
 * precision because of an extra shift included in the calculation.  (The
 * idivf operators will work for cval in [0.5, 1.0) but the shift is zero so
 * might as well use imulf for better efficiency.)  The idivf operators are
 * effectively floating-point operators with 32-bits of precision but
 * calculated with integer arithmetic only.
 *
 * Case: val >= 0.5, thus cval = val, and the imulf operator is used.
 *
 * Let the multiplier c = round(FIXED_32_ONE * cval) be precomputed before
 * calling the pixel_imulf macros.
 *
 * For unsigned data we perform a 32-bit fused integer multiply-add
 * (efficiently achieved on 64-bit systems with 64-bit arithmetic).  To
 * achieve conventional rounding FIXED_32_HALF is the added value in the
 * fused multiply-add.  Taking the top 32-bits gives the well rounded (and
 * truncated if whole number of result is larger than 32-bits) result.
 *
 * Signed fixed-point multiplication is more complicated, because the shift
 * by 32-bits to get the whole number bit takes the floor(multiply-add
 * result), but conventional rounding is always towards zero.  Adding
 * one-half to round the result always works for positive results, but only
 * works almost always for negative results, the exception being any result
 * exactly half between two whole numbers is rounded in the wrong direction.
 * This is fixed by adding the greatest representable number less than
 * one-half (i.e. FIXED_32_HALF-1) when rounding negative numbers.
 *
 * Case: val < 0.5; thus cval = 1/val, and the idivf operator is used.
 *
 * Assume |val| >= 1/UINT32_MAX.  Let shift be the number of leading zeros
 * of the fractional part of |val| and c = round(FIXED_32_ONE * cval *
 * 2^shift).  Then |c| is a number between (UINT32_MAX+1)/2+1 and UINT32_MAX
 * inclusive, and along with shift is precomputed before calling the idivf
 * pixel operator.
 *
 * For both unsigned and signed data the calculation is the same as the
 * respective idivf operator except that the value of the rounder (one-half
 * for non-negative results) must be shifted to the position of the 2^-1
 * bit, and the final result is shifted right by shift.  For signed data the
 * final shift is arithmetical to preserve the sign bit.
 */

#define generate_pixel_imulf_fixed_u32(type)				\
    static inline type ## _type						\
    pixel_imulf_ ## type(type ## _type d, uint64_t c)			\
    {									\
	uint64_t umula = (uint64_t)d * c + FIXED_32_HALF;		\
	return (type ## _type)(uint32_t)(umula >> 32);			\
    }

#if 1
/*
 * Alternative ways of implementing the rounder on signed multiplication.
 * This one is faster on Intel because the gcc compiler avoids the
 * conditional by using a lea instruction.
 */
#define generate_pixel_imulf_fixed_s32(type)				\
    static inline type ## _type						\
    pixel_imulf_ ## type(type ## _type d, uint64_t c)			\
    {									\
	int64_t smul = (int64_t)d * (int64_t)c;				\
	int64_t rounder = (smul >=0 ) ? FIXED_32_HALF : (FIXED_32_HALF-1); \
	return (type ## _type)(int32_t)((smul + rounder) >> 32);	\
    }
#else
/*
 * This is faster on Arm64 but slower on Intel because the gcc compiler
 * fails to exploit the optimisation with the lea instruction.
 */
#define generate_pixel_imulf_fixed_s32(type)				\
    static inline type ## _type						\
    pixel_imulf_ ## type(type ## _type d, uint64_t c)			\
    {									\
	uint64_t d64 = (uint64_t)(int64_t)d;				\
	uint64_t xsign = (d64 ^ c) >> 63;				\
	return (type ## _type)(uint32_t)((d64*c + (FIXED_32_HALF - xsign)) >> 32); \
    }
#endif

/*
 * If the multiplication constant c is less than one, mulc_fixed_*() loses
 * precision. The following include a shift to implement an effective
 * floating-point multiplication with integer arithmetic only.
 */

#define generate_pixel_idivf_fixed_u32(type)				\
    static inline type ## _type						\
    pixel_idivf_ ## type(type ## _type d, uint64_t c, int shift)	\
    {									\
	uint64_t umula = (uint64_t)d * c + (FIXED_32_HALF<<shift);	\
	uint32_t t = (uint32_t)(umula >> 32);				\
	return (type##_type)(t >> shift);				\
    }

#define generate_pixel_idivf_fixed_s32(type)				\
    static inline type ## _type						\
    pixel_idivf_ ## type(type ## _type d, uint64_t c, int shift)	\
    {									\
	int64_t smul = (int64_t)d * (int64_t)c;					\
	int64_t rounder = (smul>=0) ? (FIXED_32_HALF<<shift) : (FIXED_32_HALF<<shift)-1; \
	int32_t t = (int32_t)((smul + rounder) >> 32);			\
	return (type##_type)(t >> shift);				\
    }


/*
 * But if the arch is limited to 32-bit calculation we define also an 8-bit
 * whole number, 24-bit fraction representation to perform multiplications
 * on (U)BYTE data, and for clipped operation we need extra bits in the
 * whole number result so sacrifice precision even further for a 16-bit
 * whole number, 16-bit fraction multiplication.
 *
 * For (U)SHORT and (U)LONG we fall back to the 32/32 fixed-point
 * representation even on 32-bit arches to give sufficient precision in the
 * calculation.
 */

#ifndef HAVE_64BIT
#define generate_pixel_imulf_fixed_u24(type)				\
    static inline type ## _type						\
    pixel_imulf_ ## type(type ## _type d, uint32_t c)			\
    {									\
	return (type ## _type)(((uint32_t)d * c + FIXED_24_HALF) >> 24); \
    }

#define generate_pixel_imulf_fixed_s16(type)				\
    static inline type ## _type						\
    pixel_imulf_ ## type(type ## _type d, uint32_t c)			\
    {									\
	int32_t smul = (int32_t)((uint32_t)((int32_t)d) * c);		\
	int32_t rounder = (smul >=0 ) ? FIXED_16_HALF : (FIXED_16_HALF-1); \
	return (type ## _type)((smul + rounder) >> 16);			\
    }

#define generate_pixel_idivf_fixed_u24(type)				\
    static inline type ## _type						\
    pixel_idivf_ ## type(type ## _type d, uint32_t c, int shift)	\
    {									\
	uint32_t t = ((uint32_t)d * c + (FIXED_24_HALF<<shift)) >> 24;	\
	return (type##_type)(t >> shift);				\
    }
#define generate_pixel_idivf_fixed_s16(type)				\
    static inline type ## _type						\
    pixel_idivf_ ## type(type ## _type d, uint32_t c, int shift)	\
    {									\
	int32_t smul = (int32_t)((uint32_t)((int32_t)d) * c);		\
	int32_t rounder = (smul >= 0) ? (FIXED_16_HALF<<shift) : (FIXED_16_HALF<<shift)-1; \
	int32_t t = (smul + rounder) >> 16;				\
	return (type##_type)(t >> shift);				\
    }
#endif


/*
 * General purpose macros to create functions that process the image of the
 * specified type with the specified operator.
 */

/* Rounds the passed constant (c) before applying operator */
#define generate_image_cround_function(type, operator)			\
    static void								\
    im_ ## operator ## _cround_ ## type(ip_image *d, double c, int f)	\
    {									\
	const type ## _type cval = (type ## _type)lround(c);		\
	PROCESS_IMAGE(d, cval, type, operator);				\
    }

/* Uses the passed constant (c) as is in applying the operator */
#define generate_image_casis_function(type, operator)			\
    static void								\
    im_ ## operator ## _casis_ ## type(ip_image *d, double c, int f)	\
    {									\
	const type ## _type cval = (type ## _type)c;			\
	PROCESS_IMAGE(d, cval, type, operator);				\
    }

/* For fixed point multiplication: This routine works best when c >= 0.5 */
#define generate_image_cfixed_32_function(type, operator)		\
    static void								\
    im_ ## operator ## _cfixed_ ## type(ip_image *d, double c, int f)	\
    {									\
	const uint64_t cval = (uint64_t)(int64_t)FIXED_32_ROUND((double)FIXED_32_ONE * c); \
	PROCESS_IMAGE(d, cval, type, operator);				\
    }

/* For fixed point multiplication:  assumed c < 0.5 for this routine */
#define generate_image_cfixed_32_shifted_function(type, operator)	\
    static void								\
    im_ ## operator ## _cfixed_ ## type(ip_image *d, double c, int f)	\
    {									\
	int l = (int)ceil(-log2(fabs(c))) - 2;				\
	const uint64_t cval = (uint64_t)(int64_t)FIXED_32_ROUND((double)FIXED_32_ONE * c * pow(2,l)); \
	PROCESS_IMAGE_with_SHIFT(d, cval, l, type, operator);		\
    }

#ifndef HAVE_64BIT
/* Should only be used for unsigned byte (UBYTE) */
#define generate_image_cfixed_24_function(type, operator)		\
    static void								\
    im_ ## operator ## _cfixed_ ## type(ip_image *d, double c, int f)	\
    {									\
	const uint32_t cval = (uint32_t)FIXED_24_ROUND((double)FIXED_24_ONE * c); \
	PROCESS_IMAGE(d, cval, type, operator);				\
    }
/* Should only be used for unsigned byte (UBYTE) */
#define generate_image_cfixed_24_shifted_function(type, operator)	\
    static void								\
    im_ ## operator ## _cfixed_ ## type(ip_image *d, double c, int f)	\
    {									\
	int l = (int)ceil(-log2(fabs(c))) - 2;				\
	const uint32_t cval = (uint32_t)FIXED_24_ROUND((double)FIXED_24_ONE * c * pow(2,l)); \
	PROCESS_IMAGE_with_SHIFT(d, cval, l, type, operator);		\
    }
#define generate_image_cfixed_16_function(type, operator)		\
    static void								\
    im_ ## operator ## _cfixed_ ## type(ip_image *d, double c, int f)	\
    {									\
	const uint32_t cval = (uint32_t)(int32_t)FIXED_16_ROUND((double)FIXED_16_ONE * c); \
	PROCESS_IMAGE(d, cval, type, operator);				\
    }
#define generate_image_cfixed_16_shifted_function(type, operator)	\
    static void								\
    im_ ## operator ## _cfixed_ ## type(ip_image *d, double c, int f)	\
    {									\
	int l = (int)ceil(-log2(fabs(c))) - 2;				\
	const uint32_t cval = (uint32_t)(int32_t)FIXED_16_ROUND((double)FIXED_16_ONE * c * pow(2,l)); \
	PROCESS_IMAGE_with_SHIFT(d, cval, l, type, operator);		\
    }
#endif

/* For processing complex images; c is complex */
#define generate_image_complex_function(type, operator)			\
    static void								\
    im_ ## operator ## _complex_ ## type(ip_image *d, type ## _type c)	\
    {									\
	PROCESS_IMAGE(d, c, type, operator);				\
    }

/* For processing RGB(A) images; c is (r,g,b,a) */
#define generate_image_rgb_function(type, operator)			\
    static void								\
    im_ ## operator ## _rgb_ ## type(ip_image *d, double r, double g, double b, double a) \
    {									\
	type ## _type c = ip_rgb_alloc_const_ ## type(r, g, b, a);	\
	PROCESS_IMAGE(d, c, type, operator);				\
    }


/*
 * For converting separately passed RGB components into an ip_rgb like
 * structure.
 */

#define ip_rgb_alloc_const_RGB(r,g,b,a) \
    (ip_rgb){(uint8_t)r, (uint8_t)g, (uint8_t)b}
#define ip_rgb_alloc_const_RGBA(r,g,b,a) \
    (ip_rgba){(uint8_t)r, (uint8_t)g, (uint8_t)b, (uint8_t)a}
#define ip_rgb_alloc_const_RGB16(r,g,b,a) \
    (ip_lrgb){(uint16_t)r, (uint16_t)g, (uint16_t)b}


/*
 * Addition operators for each image type.  Note that signed integer
 * operators can be calculated with their unsigned counterpart (i.e. we
 * assume two's complement signed representation).
 */
generate_pixel_op(UBYTE, addc, d + c)
generate_pixel_op(USHORT, addc, d + c)
generate_pixel_op(ULONG, addc, d + c)
generate_pixel_op(FLOAT, addc, d + c)
generate_pixel_op(DOUBLE, addc, d + c)
generate_pixel_op(COMPLEX, addc, ip_complex_add(d,c))
generate_pixel_op(DCOMPLEX, addc, ip_dcomplex_add(d,c))
generate_pixel_op(RGB, addc, ((ip_rgb){d.r+c.r, d.g+c.g, d.b+c.b}));
generate_pixel_op(RGBA, addc, ((ip_rgba){d.r+c.r, d.g+c.g, d.b+c.b, d.a+c.a}));
generate_pixel_op(RGB16, addc, ((ip_lrgb){d.r+c.r, d.g+c.g, d.b+c.b}));


/* Clipped (saturated) addition operator for each pixel type */
static inline uint8_t pixel_addc_clipped_UBYTE(uint8_t d, uint8_t c)
{
    int res = (int)d + (int)c;
    return (uint8_t)((res > 255) ? 255 : res);
}

static inline int8_t pixel_addc_clipped_BYTE(int8_t d, int8_t c)
{
    int res = (int)d + (int)c;
    if (res < INT8_MIN) res = INT8_MIN;
    if (res > INT8_MAX) res = INT8_MAX;
    return (int8_t)res;
}

static inline uint16_t pixel_addc_clipped_USHORT(uint16_t d, uint16_t c)
{
    unsigned int res = (unsigned int)d + (unsigned int)c;
    return (uint16_t)((res > UINT16_MAX) ? UINT16_MAX : res);
}

static inline int16_t pixel_addc_clipped_SHORT(int16_t d, int16_t c)
{
    int res = (int)d + (int)c;
    if (res < INT16_MIN) res = INT16_MIN;
    if (res > INT16_MAX) res = INT16_MAX;
    return (int16_t)res;
}

static inline ip_rgb pixel_addc_clipped_RGB(ip_rgb d, ip_rgb c)
{
    ip_rgb res;
    res.r = pixel_addc_clipped_UBYTE(d.r, c.r);
    res.g = pixel_addc_clipped_UBYTE(d.g, c.g);
    res.b = pixel_addc_clipped_UBYTE(d.b, c.b);
    return res;
}

static inline ip_rgba pixel_addc_clipped_RGBA(ip_rgba d, ip_rgba c)
{
    ip_rgba res;
    res.r = pixel_addc_clipped_UBYTE(d.r, c.r);
    res.g = pixel_addc_clipped_UBYTE(d.g, c.g);
    res.b = pixel_addc_clipped_UBYTE(d.b, c.b);
    res.a = pixel_addc_clipped_UBYTE(d.a, c.a);
    return res;
}

static inline ip_lrgb pixel_addc_clipped_RGB16(ip_lrgb d, ip_lrgb c)
{
    ip_lrgb res;
    res.r = pixel_addc_clipped_USHORT(d.r, c.r);
    res.g = pixel_addc_clipped_USHORT(d.g, c.g);
    res.b = pixel_addc_clipped_USHORT(d.b, c.b);
    return res;
}



/* Subtraction operator for each pixel type */
generate_pixel_op(UBYTE, subc, d - c)
generate_pixel_op(USHORT, subc, d - c)
generate_pixel_op(ULONG, subc, d - c)
/* For float/complex use the addition operators */
generate_pixel_op(RGB, subc, ((ip_rgb){d.r-c.r, d.g-c.g, d.b-c.b}));
generate_pixel_op(RGBA, subc, ((ip_rgba){d.r-c.r, d.g-c.g, d.b-c.b, d.a-c.a}));
generate_pixel_op(RGB16, subc, ((ip_lrgb){d.r-c.r, d.g-c.g, d.b-c.b}));

/* Clipped (saturated) subtraction operator for each pixel type */
static inline uint8_t pixel_subc_clipped_UBYTE(uint8_t d, uint8_t s)
{
    int res = (int)d - (int)s;
    return (uint8_t)((res < 0) ? 0 : res);
}

static inline int8_t pixel_subc_clipped_BYTE(int8_t d, int8_t s)
{
    int res = (int)d - (int)s;
    if (res < INT8_MIN) res = INT8_MIN;
    if (res > INT8_MAX) res = INT8_MAX;
    return (int8_t)res;
}

static inline uint16_t pixel_subc_clipped_USHORT(uint16_t d, uint16_t s)
{
    int res = (int)d - (int)s;
    return (uint16_t)((res < 0) ? 0 : res);
}

static inline int16_t pixel_subc_clipped_SHORT(int16_t d, int16_t s)
{
    int res = (int)d - (int)s;
    if (res < INT16_MIN) res = INT16_MIN;
    if (res > INT16_MAX) res = INT16_MAX;
    return (int16_t)res;
}

static inline ip_rgb pixel_subc_clipped_RGB(ip_rgb d, ip_rgb c)
{
    ip_rgb res;
    res.r = pixel_subc_clipped_UBYTE(d.r, c.r);
    res.g = pixel_subc_clipped_UBYTE(d.g, c.g);
    res.b = pixel_subc_clipped_UBYTE(d.b, c.b);
    return res;
}

static inline ip_rgba pixel_subc_clipped_RGBA(ip_rgba d, ip_rgba c)
{
    ip_rgba res;
    res.r = pixel_subc_clipped_UBYTE(d.r, c.r);
    res.g = pixel_subc_clipped_UBYTE(d.g, c.g);
    res.b = pixel_subc_clipped_UBYTE(d.b, c.b);
    res.a = pixel_subc_clipped_UBYTE(d.a, c.a);
    return res;
}

static inline ip_lrgb pixel_subc_clipped_RGB16(ip_lrgb d, ip_lrgb c)
{
    ip_lrgb res;
    res.r = pixel_subc_clipped_USHORT(d.r, c.r);
    res.g = pixel_subc_clipped_USHORT(d.g, c.g);
    res.b = pixel_subc_clipped_USHORT(d.b, c.b);
    return res;
}


/*
 * Multiplication operators for each image type.
 *
 * Integer multiply implemented as fixed point arithmetic to get best speed.
 * For 8-bit (UBYTE and BYTE) if the system is natively 64-bit then use 32
 * bit fixed fraction, but if the system is natively 32-bit then we
 * sacrifice precision to fit the calculation in 32-bit integers for
 * efficiency.
 *
 * But for 16-bit (USHORT/SHORT) and 32-bit (LONG/ULONG) to preserve
 * precision we always use 64-bit calculations even on 32-bit systems.
 */

#ifdef HAVE_64BIT
generate_pixel_imulf_fixed_u32(UBYTE)
generate_pixel_imulf_fixed_s32(BYTE)
generate_pixel_idivf_fixed_u32(UBYTE)
generate_pixel_idivf_fixed_s32(BYTE)
#else
generate_pixel_imulf_fixed_u24(UBYTE)
generate_pixel_imulf_fixed_s16(BYTE)
generate_pixel_idivf_fixed_u24(UBYTE)
generate_pixel_idivf_fixed_s16(BYTE)
#endif

generate_pixel_imulf_fixed_u32(USHORT)
generate_pixel_imulf_fixed_s32(SHORT)
generate_pixel_idivf_fixed_u32(USHORT)
generate_pixel_idivf_fixed_s32(SHORT)

generate_pixel_imulf_fixed_u32(ULONG)
generate_pixel_imulf_fixed_s32(LONG)
generate_pixel_idivf_fixed_u32(ULONG)
generate_pixel_idivf_fixed_s32(LONG)

generate_pixel_op(FLOAT, mulc, d *c)
generate_pixel_op(DOUBLE, mulc, d *c)
generate_pixel_op(COMPLEX, mulc, ip_complex_mul(d,c))
generate_pixel_op(DCOMPLEX, mulc, ip_dcomplex_mul(d,c))



/*
 * The integer mulc functions defined above only work for the default
 * truncation mode.
 *
 * Here we define the clipped versions.
 */

#ifdef HAVE_64BIT

/*
 * On 64-bit arches we can work in the 32/32 fixed point representation,
 * take the whole number part after the shift, and then check for overflow
 * to clip.  As long as the 32-bit whole number does not overflow then the
 * clipping works.  If the 32-bit whole number overflows clipping is not
 * much use anyway as every possible valid result (other than zero) would be
 * clipped.
 */

static inline uint8_t
pixel_imulf_clipped_UBYTE(uint8_t d, uint64_t c)
{
    uint32_t res = (uint32_t)(((uint64_t)((int64_t)d) * c + FIXED_32_HALF) >> 32);
    return (uint8_t)((res > 255) ? 255 : res);
}

static inline int8_t
pixel_imulf_clipped_BYTE(int8_t d, uint64_t c)
{
    int32_t res;
    int64_t smul = (int64_t)((uint64_t)(int64_t)d * c);
    smul += (smul >= 0) ? FIXED_32_HALF : (FIXED_32_HALF-1);
    res = (int32_t)(smul>>32);
    if (res < INT8_MIN) res = INT8_MIN;
    if (res > INT8_MAX) res = INT8_MAX;
    return (int8_t)res;
}

/*
 * There is no use in defining divc equivalents because the result of the
 * divc operators, when properly used, cannot overflow, thus clipping does
 * not occur.
 */

#else

/*
 * 32 bit arch -- sacrifice precision rather than speed.
 *
 * Because there is little room for precision the comparison for clipping
 * values to min/max must be performed in the extended fixed-point
 * representation before shifting, particularly for the BYTE case because
 * the (unsigned) shift can lose the sign bit.
 *
 * We are down to 16-bits of fraction only with these two algorithms.
 */

static inline uint8_t
pixel_imulf_clipped_UBYTE(uint8_t d, uint32_t c)
{
    uint32_t res = ((uint32_t)(int32_t)d * c + FIXED_16_HALF);
    if (res > (255<<16)) res = 255<<16;
    return (uint8_t)(res>>16);
}

static inline int8_t
pixel_imulf_clipped_BYTE(int8_t d, uint32_t c)
{
    int32_t res = (int32_t)((uint32_t)(int32_t)d * c);
    res += (res >= 0) ? FIXED_16_HALF : (FIXED_16_HALF-1);
    if (res < (INT8_MIN<<16)) res = INT8_MIN<<16;
    if (res > (INT8_MAX<<16)) res = INT8_MAX<<16;
    return (int8_t)(res>>16);
}

#endif


/*
 * (U)SHORT mulc_clipped is performed at 32/32 fixed point precision even on
 * 32-bit arches to keep sufficient precision.
 */

static inline uint16_t
pixel_imulf_clipped_USHORT(uint16_t d, uint64_t c)
{
    uint32_t res = (uint32_t)(((uint64_t)((int64_t)d) * c + FIXED_32_HALF) >> 32);
    return (uint16_t)((res > UINT16_MAX) ? UINT16_MAX : res);
}

static inline int16_t
pixel_imulf_clipped_SHORT(int16_t d, uint64_t c)
{
    int64_t smul = (int64_t)((uint64_t)((int64_t)d) * c);
    int32_t res = (int32_t)((smul + ((smul>=0) ? FIXED_32_HALF : (FIXED_32_HALF-1))) >> 32);
    if (res < INT16_MIN) res = INT16_MIN;
    if (res > INT16_MAX) res = INT16_MAX;
    return (int16_t)res;
}


/* Division operators. */

generate_pixel_op(FLOAT, divc, d / c)
generate_pixel_op(DOUBLE, divc, d / c)

/*
 * We introduce operators for complex division and choose the one that best
 * preserves the full result range.
 * Now ip_complex_div_a() is used if abs(c.r) >= abs(c.i) and the following
 * assignments are made before calling ip_complex_div_a:
 *   dc = c.i/c.r;   denom = c.r + c.i*dc;
 *   c is replaced with (dc + i*denom) in call to ip_complex_div_a().
 * Likewise ip_complex_div_b() used if abs(c.r) < abs(c.i) and the following
 * assignments are made before calling ip_complex_div_b:
 *   dc = c.r/c.i;   denom = c.r*dc + c.i;
 *   c is replaced with (dc + i*denom) in call to ip_complex_div_b().
 * so in the definitions below c.r is dc and c.i is denom.
 *
 * The two primitive operators are defined for the complex division at this
 * stage of the game to enable the comparision of abs(c.r) and abs(c.i),
 * which can result in a branch misprediction to be shifted outside the
 * PROCESS_IMAGE macro.
 */

static inline ip_complex ip_complex_div_a(ip_complex d, ip_complex c)
{
    ip_complex res;
    res.r = (d.r + d.i*c.r) / c.i;
    res.i = (d.i - d.r*c.r) / c.i;
    return res;
}

static inline ip_complex ip_complex_div_b(ip_complex d, ip_complex c)
{
    ip_complex res;
    res.r = (d.r*c.r + d.i) / c.i;
    res.i = (d.i*c.r - d.r) / c.i;
    return res;
}

static inline ip_dcomplex ip_dcomplex_div_a(ip_dcomplex d, ip_dcomplex c)
{
    ip_dcomplex res;
    res.r = (d.r + d.i*c.r) / c.i;
    res.i = (d.i - d.r*c.r) / c.i;
    return res;
}

static inline ip_dcomplex ip_dcomplex_div_b(ip_dcomplex d, ip_dcomplex c)
{
    ip_dcomplex res;
    res.r = (d.r*c.r + d.i) / c.i;
    res.i = (d.i*c.r - d.r) / c.i;
    return res;
}

generate_pixel_op(COMPLEX, divc_a, ip_complex_div_a(d,c))
generate_pixel_op(COMPLEX, divc_b, ip_complex_div_b(d,c))
generate_pixel_op(DCOMPLEX, divc_a, ip_dcomplex_div_a(d,c))
generate_pixel_op(DCOMPLEX, divc_b, ip_dcomplex_div_b(d,c))


/*
 * Subroutines for im_addc().
 */

/*
 * Generate the functions:
 *    im_addc_cround_UBYTE()
 *    im_addc_cround_USHORT()
 *    im_addc_cround_ULONG()
 *    im_addc_casis_FLOAT()
 *    im_addc_casis_DOUBLE()
 *
 * cround indicates rounding the constant value first before processing image.
 * casis indicates using the constant value as is to process image.
 */
generate_image_cround_function(UBYTE, addc)
generate_image_cround_function(USHORT, addc)
generate_image_cround_function(ULONG, addc)
generate_image_casis_function(FLOAT, addc)
generate_image_casis_function(DOUBLE, addc)

static improc_function_Icf addc_function[IM_TYPEMAX] = {
    [IM_BYTE] = im_addc_cround_UBYTE,
    [IM_UBYTE] = im_addc_cround_UBYTE,
    [IM_SHORT] = im_addc_cround_USHORT,
    [IM_USHORT] = im_addc_cround_USHORT,
    [IM_LONG] = im_addc_cround_ULONG,
    [IM_ULONG] = im_addc_cround_ULONG,
    [IM_FLOAT] = im_addc_casis_FLOAT,
    [IM_DOUBLE] = im_addc_casis_DOUBLE,
    [IM_PALETTE] = im_addc_cround_UBYTE
};

generate_image_cround_function(UBYTE, addc_clipped)
generate_image_cround_function(BYTE, addc_clipped)
generate_image_cround_function(USHORT, addc_clipped)
generate_image_cround_function(SHORT, addc_clipped)

static improc_function_Icf addc_clipped_function[IM_TYPEMAX] = {
    [IM_BYTE] = im_addc_clipped_cround_BYTE,
    [IM_UBYTE] = im_addc_clipped_cround_UBYTE,
    [IM_SHORT] = im_addc_clipped_cround_SHORT,
    [IM_USHORT] = im_addc_clipped_cround_USHORT,
    [IM_PALETTE] = im_addc_clipped_cround_UBYTE
};


/*
 * Generate the functions:
 *   im_addc_complex_COMPLEX()
 *   im_addc_complex_DCOMPLEX()
 * used in im_addc_complex().
 */

generate_image_complex_function(COMPLEX, addc)
generate_image_complex_function(DCOMPLEX, addc)


generate_image_rgb_function(RGB, addc)
generate_image_rgb_function(RGBA, addc)
generate_image_rgb_function(RGB16, addc)

generate_image_rgb_function(RGB, addc_clipped)
generate_image_rgb_function(RGBA, addc_clipped)
generate_image_rgb_function(RGB16, addc_clipped)


static improc_function_Icccc rgb_addc_function[IM_TYPEMAX] = {
    [IM_RGB] = im_addc_rgb_RGB,
    [IM_RGBA] = im_addc_rgb_RGBA,
    [IM_RGB16] = im_addc_rgb_RGB16
};

static improc_function_Icccc rgb_addc_clipped_function[IM_TYPEMAX] = {
    [IM_RGB] = im_addc_clipped_rgb_RGB,
    [IM_RGBA] = im_addc_clipped_rgb_RGBA,
    [IM_RGB16] = im_addc_clipped_rgb_RGB16
};



/*

NAME:

   im_addc() \- Add a constant value to an image

PROTOTYPE:

   #include <ip/ip.h>

   int  im_addc( im, value, flag )
   ip_image *im;
   double value;
   int flag;

DESCRIPTION:

   Add a constant value to an image.  All image types except IM_BINARY
   supported.  For integer based images 'value' is rounded to the nearest
   integer before the addition is carried out, and the addition is performed
   in the same type as the image.  If 'value' is out of range for the
   particular type of the image than an ERR_BAD_VALUE error is returned.

   If the image type is IM_BYTE, IM_UBYTE, IM_USHORT or IM_SHORT and 'flag'
   is set to FLG_CLIP then the result of the addition is clipped to the
   appropriate data type limit.  FLG_CLIP is not implemented for IM_LONG,
   IM_FLOAT, IM_double or non-scalar type images

   For complex and colour images one can use the flags FLG_SREAL and
   FLG_SIMAG for complex images, and FLG_SRED, FLG_SGREEN, FLG_SBLUE and
   FLG_SALPHA for colour images, to indicate which component to add 'value'
   to.  The flags can be ORed together.

   For complex and colour images it may be better to use the interfaces
   im_addc_complex() and im_addc_rgb() which allow the addition of arbitrary
   complex and colour values.

ERRORS:

   ERR_BAD_TYPE
   ERR_BAD_FLAG
   ERR_PARM_BAD_VALUE
   ERR_NOT_IMPLEMENTED - if FLG_CLIP called on unsupported image type

SEE ALSO:

   im_addc_rgb()       im_addc_complex()
   im_subc()           im_mulc()      im_divc()
   im_add()            im_sub()       im_mul()       im_div()

*/

int im_addc(ip_image *im, double val, int flag)
{
    int retcode;
    int done = 0;

    ip_log_routine(__func__);

    if (NOT image_valid(im))
	return ip_error;

    if (image_bad_type(im, IM_BINARY, -1))
	return ip_error;

    if (type_complex(im->type)) {
	ip_dcomplex cval;

	/* Handle complex images */
	if (NOT ip_flag_ok(flag,  FLG_SREAL, FLG_SIMAG,
			   FLG_SREAL|FLG_SIMAG, LASTFLAG))
	    return ip_error;

	cval.r = (flag & FLG_SREAL) ? val : 0.0;
	cval.i = (flag & FLG_SIMAG) ? val : 0.0;
	retcode = im_addc_complex(im, cval, NO_FLAG);

	ip_pop_routine();
	return retcode;
    }

    if (type_rgbim(im->type)) {
	/* Handle RGB images */
	if (NOT ip_flag_bits_unused(flag, ~(FLGPRT_COLOUR | FLG_CLIP)))
	    return ip_error;

	/* Require at least one colour specificed */
	if (flag == FLG_NONE || flag == FLG_CLIP) {
	    ip_log_error(ERR_BAD_FLAG);
	    retcode = ip_error;
	}else{
	    retcode = im_addc_rgb(im,
				  (flag & FLG_SRED) ? val : 0.0,
				  (flag & FLG_SGREEN) ? val : 0.0,
				  (flag & FLG_SBLUE) ? val : 0.0,
				  (flag & FLG_SALPHA) ? val : 0.0,
				  flag & FLG_CLIP);
	}

	ip_pop_routine();
	return retcode;
    }

    if (NOT ip_value_in_image("value", val, im))
	return ip_error;

    if (type_real(im->type) || im->type == IM_LONG) {

	/* No flags possible */
	if (NOT ip_flag_ok(flag, LASTFLAG))
	    return ip_error;

    }else{

	/* lower precision scalar image types including palette */
	if (NOT ip_flag_ok(flag, FLG_CLIP, LASTFLAG))
	    return ip_error;

    }

#ifdef HAVE_SIMD

    if (IP_USE_HWACCEL) {
	if (flag & FLG_CLIP) {
	    if (ip_simd_addc_clipped_function[im->type]) {
		ip_simd_addc_clipped_function[im->type](im, val, flag);
		done = 1;
	    }
	}else{
	    if (ip_simd_addc_function[im->type]) {
		ip_simd_addc_function[im->type](im, val, flag);
		done = 1;
	    }
	}
    }

    if (done) {
        ip_pop_routine();
        return OK;
    }

#endif

    /*
     * If get here then no hardware/SIMD acceleration for this image type.
     * Try packing multiple bytes/shorts into a long with some zero bits in
     * between to protect against spilling of the carry bit.  In this way we
     * can do a poor man's SIMD add, which, particularly for byte and short
     * images on a 64 bit arch, gives some speed up.
     */
    if (IP_USE_FAST_CODE && fast_code_masks[im->type].mask_even) {
        int length;
        unsigned long m1, m2;
        unsigned long a1, a2;

#ifdef USE_FAST_CODE_METHOD_1
	m1 = fast_code_masks[im->type].sign_bits;
	m2 = ~m1;
#else
	m1 = fast_code_masks[im->type].mask_even;
	m2 = fast_code_masks[im->type].mask_odd;
#endif

	if (ip_value_splat(im->type, val, &a1)) {
	    a2 = a1 & m2;
	    a1 &= m1;

	    length = containers_in_padded_row(im, sizeof(unsigned long));

	    for (int j=0; j<im->size.y; j++) {
		IMROW_FAST_ADDC(im, a1, a2, j, length);
	    }

	    ip_pop_routine();
	    return OK;
	}

	/* if couldn't splat value fall through to slow code */
    }

    /*
     * We are running out of clever tricks to speed up processing so resort
     * to basic pixel by pixel processing.
     */

    if (flag & FLG_CLIP) {
	if (addc_clipped_function[im->type]) {
	    addc_clipped_function[im->type](im, val, flag);
	    done = 1;
	}
    }else{
	if (addc_function[im->type]) {
	    addc_function[im->type](im, val, flag);
	    done = 1;
	}
    }

    retcode = OK;
    if (! done) {
        ip_log_error(ERR_NOT_IMPLEMENTED);
        retcode = ip_error;
    }

    ip_pop_routine();
    return retcode;
}




/*

NAME:

   im_addc_complex() \- Add a constant complex value to a COMPLEX image

PROTOTYPE:

   #include <ip/ip.h>

   int  im_addc_complex(im, z, flag)
   ip_image *im;
   ip_dcomplex z;
   int flag;

DESCRIPTION:

   Add the constant complex value z = u + iv to a complex (i.e. IM_COMPLEX
   or IM_DCOMPLEX) image.

   It is an error to call this function with an image that is not complex.

   'flag' must be FLG_NONE.

ERRORS:

   ERR_BAD_TYPE
   ERR_BAD_FLAG

SEE ALSO:

   im_addc_rgb()
   im_addc()           im_subc()      im_mulc()      im_divc()
   im_add()            im_sub()       im_mul()       im_div()

*/

int im_addc_complex(ip_image *im, ip_dcomplex z, int flag)
{
    ip_log_routine(__func__);

    if (NOT image_valid(im))
	return ip_error;

    if (NOT type_complex(im->type)) {
	ip_log_error(ERR_BAD_TYPE);
	ip_pop_routine();
	return ip_error;
    }

    if (flag != NO_FLAG) {
	ip_log_error(ERR_BAD_FLAG);
	ip_pop_routine();
	return ip_error;
    }

#ifdef HAVE_SIMD
    if (IP_USE_HWACCEL) {

	if (ip_simd_addc_complex_function[im->type]) {
	    ip_simd_addc_complex_function[im->type](im, z, flag);
	    ip_pop_routine();
	    return OK;
	}

    }
#endif

    /* No hardware accelarator available -- general code follows */
    if (im->type == IM_COMPLEX) {
	ip_complex zval;

	zval.r = (float)z.r;
	zval.i = (float)z.i;

	im_addc_complex_COMPLEX(im, zval);

    }else{			/* IM_DCOMPLEX */

	im_addc_complex_DCOMPLEX(im, z);
    }

    ip_pop_routine();
    return ip_error;
}



/*

NAME:

   im_addc_rgb() \- Add a constant RGB(A) value to an RGB image

PROTOTYPE:

   #include <ip/ip.h>

   int  im_addc_rgb( im, R, G, B, A, flag)
   ip_image *im;
   double R, G, B, A;
   int flag;

DESCRIPTION:

   Add the constant colour value (R,G,B) to an IM_RGB or IM_RGB16 image.
   Add the constant colour value (R,G,B,A) to an IM_RGBA image.

   It is an error to call this function on an image that is not one of the
   RGB colour types.

   Set `flag' to FLG_CLIP to specify clipping of result to type limits.

ERRORS:

   ERR_BAD_TYPE
   ERR_BAD_VALUE

SEE ALSO:

   im_addc_complex()
   im_addc()           im_subc()      im_mulc()      im_divc()
   im_add()            im_sub()       im_mul()       im_div()

*/

int im_addc_rgb(ip_image *im, double r, double g, double b, double a, int flag)
{
    ip_log_routine(__func__);

    if (NOT image_valid(im))
	return ip_error;

    if (NOT type_rgbim(im->type)) {
	ip_log_error(ERR_BAD_TYPE);
	ip_pop_routine();
	return ip_error;
    }

    if (NOT ip_flag_ok(flag, FLG_CLIP, LASTFLAG)) {
	return ip_error;
    }

    r = round(r);
    g = round(g);
    b = round(b);
    a = round(a);

#ifdef HAVE_SIMD
    if (IP_USE_HWACCEL) {

	if (ip_simd_addc_rgb_function[im->type]) {
	    ip_simd_addc_rgb_function[im->type](im, r, g, b, a, flag);
	    ip_pop_routine();
	    return OK;
	}

    }
#endif

    if (flag & FLG_CLIP) {
	if (rgb_addc_clipped_function[im->type])
	    rgb_addc_clipped_function[im->type](im, r, g, b, a);
    }else{
	if (rgb_addc_function[im->type])
	    rgb_addc_function[im->type](im, r, g, b, a);

    }

    ip_pop_routine();
    return ip_error;
}




/*
 * Subroutines for im_subc().
 */

/*
 * Generate the functions:
 *    im_subc_cround_UBYTE()
 *    im_subc_cround_USHORT()
 *    im_subc_cround_ULONG()
 *    im_subc_casis_FLOAT()
 *    im_subc_casis_DOUBLE()
 *
 * cround indicates rounding the constant value first before processing image.
 * casis indicates using the constant value as is to process image.
 */
generate_image_cround_function(UBYTE, subc)
generate_image_cround_function(USHORT, subc)
generate_image_cround_function(ULONG, subc)

static improc_function_Icf subc_function[IM_TYPEMAX] = {
    [IM_BYTE] = im_subc_cround_UBYTE,
    [IM_UBYTE] = im_subc_cround_UBYTE,
    [IM_SHORT] = im_subc_cround_USHORT,
    [IM_USHORT] = im_subc_cround_USHORT,
    [IM_LONG] = im_subc_cround_ULONG,
    [IM_ULONG] = im_subc_cround_ULONG,
    [IM_PALETTE] = im_subc_cround_UBYTE
};

generate_image_cround_function(UBYTE, subc_clipped)
generate_image_cround_function(BYTE, subc_clipped)
generate_image_cround_function(USHORT, subc_clipped)
generate_image_cround_function(SHORT, subc_clipped)

static improc_function_Icf subc_clipped_function[IM_TYPEMAX] = {
    [IM_BYTE] = im_subc_clipped_cround_BYTE,
    [IM_UBYTE] = im_subc_clipped_cround_UBYTE,
    [IM_SHORT] = im_subc_clipped_cround_SHORT,
    [IM_USHORT] = im_subc_clipped_cround_USHORT,
    [IM_PALETTE] = im_subc_clipped_cround_UBYTE
};



generate_image_rgb_function(RGB, subc)
generate_image_rgb_function(RGBA, subc)
generate_image_rgb_function(RGB16, subc)

generate_image_rgb_function(RGB, subc_clipped)
generate_image_rgb_function(RGBA, subc_clipped)
generate_image_rgb_function(RGB16, subc_clipped)


static improc_function_Icccc rgb_subc_function[IM_TYPEMAX] = {
    [IM_RGB] = im_subc_rgb_RGB,
    [IM_RGBA] = im_subc_rgb_RGBA,
    [IM_RGB16] = im_subc_rgb_RGB16
};

static improc_function_Icccc rgb_subc_clipped_function[IM_TYPEMAX] = {
    [IM_RGB] = im_subc_clipped_rgb_RGB,
    [IM_RGBA] = im_subc_clipped_rgb_RGBA,
    [IM_RGB16] = im_subc_clipped_rgb_RGB16
};



/*

NAME:

   im_subc() \- Subtract a constant value from an image

PROTOTYPE:

   #include <ip/ip.h>

   int  im_subc( im, value, flag )
   ip_image *im;
   double value;
   int flag;

DESCRIPTION:

   Subtract a constant value from an image.  All image types except
   IM_BINARY supported.  For integer based images 'value' is rounded before
   the subtraction is carried out.

   If the image type is BYTE, USHORT or SHORT and 'flag' is set to FLG_CLIP
   then the result of the addition is clipped to BYTE, USHORT or SHORT value
   limits (respectively).

   For COMPLEX images it is better to call the interface im_subc_complex(),
   however one can do limited complex subtractions with im_subc() by
   including one of FLG_SREAL or FLG_SIMAG in 'flags' to specify which
   component of the COMPLEX image to subtract 'value' from.  To subtract the
   complex number (u + iv) that has both a real and imaginary part it is
   better to call im_addc_complex() with the negative of u+iv. (There is no
   im_subc_complex() as im_addc_complex() is sufficient for all purposes.)

   For RGB type colour images it is better to call the interface
   im_subc_rgb() however one can do limited colour subtractions with
   im_subc() by specifying one or more of FLG_SRED, FLG_SGREEN and FLG_SBLUE
   (and FLG_SALPHA if RGBA type image) to specify which components from
   which the constant 'value' is subtracted.  The flags can be ORed together
   to subtract a constant from more than one component. (There is a routine
   im_subc_rgb() in addition to im_addc_rgb() as integer signed
   addition/subtraction is not completely evaluable by the other with a sign
   change only.)

ERRORS:

   ERR_BAD_TYPE
   ERR_BAD_FLAG
   ERR_BAD_VALUE

SEE ALSO:

   im_addc_complex()    im_subc_rgb()
   im_addc()    im_mulc()    im_divc()
   im_add()     im_sub()     im_mul()     im_div()

*/

int im_subc(ip_image *im, double val, int flag)
{
    int retcode;
    int done = 0;

    ip_log_routine(__func__);

    if (NOT image_valid(im))
	return ip_error;

    if (image_bad_type(im, IM_BINARY, -1))
	return ip_error;

    if (type_complex(im->type)) {

	if (NOT ip_flag_ok(flag,  FLG_SREAL, FLG_SIMAG,
			   FLG_SREAL|FLG_SIMAG, LASTFLAG))
	    return ip_error;

	retcode = im_addc_complex(im,
				  (ip_dcomplex){flag & FLG_SREAL ? -val : 0.0,
						flag & FLG_SIMAG ? -val : 0.0}, NO_FLAG);

	ip_pop_routine();
	return retcode;
    }

    if (type_rgbim(im->type)) {
	if (NOT ip_flag_bits_unused(flag, ~(FLGPRT_COLOUR | FLG_CLIP)))
	    return ip_error;

	/* Require at least one colour specificed */
	if (flag == FLG_NONE || flag == FLG_CLIP) {
	    ip_log_error(ERR_BAD_FLAG);
	    retcode = ip_error;
	}else{
	    retcode = im_subc_rgb(im,
				  (flag & FLG_SRED) ? val : 0.0,
				  (flag & FLG_SGREEN) ? val : 0.0,
				  (flag & FLG_SBLUE) ? val : 0.0,
				  (flag & FLG_SALPHA) ? val : 0.0,
				  flag & FLG_CLIP);
	}

	ip_pop_routine();
	return retcode;
    }

    if (type_real(im->type)) {
	/* Float and double easily handled by a negative addition */
	retcode = im_addc(im, -val, flag);
	ip_pop_routine();
	return retcode;
    }

    if (NOT ip_value_in_image("value", val, im))
	return ip_error;

    if (im->type == IM_LONG || im->type == IM_ULONG) {

	/* No flags possible */
	if (NOT ip_flag_ok(flag, LASTFLAG))
	    return ip_error;

    }else{

	/* lower precision integer image types including palette */
	if (NOT ip_flag_ok(flag, FLG_CLIP, LASTFLAG))
	    return ip_error;

    }

#ifdef HAVE_SIMD
    /* Attempt to use SIMD acceleration if available */
    if (IP_USE_HWACCEL) {
	if (flag & FLG_CLIP) {
	    if (ip_simd_subc_clipped_function[im->type]) {
		ip_simd_subc_clipped_function[im->type](im, val, flag);
		done = 1;
	    }
	}else{
	    if (ip_simd_subc_function[im->type]) {
		ip_simd_subc_function[im->type](im, val, flag);
		done = 1;
	    }
	}
    }

    if (done) {
        ip_pop_routine();
        return OK;
    }
#endif

    /*
     * If get here then no hardware/SIMD acceleration for this image type.
     * Try packing multiple bytes/shorts into a long with some ones/zero
     * bits in between to protect against spilling of the carry bit.  In
     * this way we can do a poor man's SIMD subtraction, which, particularly
     * for byte and short images on a 64 bit arch, gives some speed up.
     */
    if (IP_USE_FAST_CODE && fast_code_masks[im->type].mask_even) {
        int length;
        unsigned long m1, m2;
        unsigned long a1, a2;

#ifdef USE_FAST_CODE_METHOD_1
	m1 = fast_code_masks[im->type].sign_bits;
	m2 = ~m1;
#else
	m1 = fast_code_masks[im->type].mask_even;
	m2 = fast_code_masks[im->type].mask_odd;
#endif

	if (ip_value_splat(im->type, val, &a1)) {
	    a2 = a1 & m2;
	    a1 &= m1;
#ifdef USE_FAST_CODE_METHOD_1
	    a1 ^= m1;
#endif

	    length = containers_in_padded_row(im, sizeof(unsigned long));

	    for (int j=0; j<im->size.y; j++) {
		IMROW_FAST_SUBC(im, a1, a2, j, length);
	    }

	    ip_pop_routine();
	    return OK;
	}

	/* if couldn't splat value fall through to slow code */
    }

    /* Normal code following used if no hardware accelerators available */

    if (flag & FLG_CLIP) {
	if (subc_clipped_function[im->type]) {
	    subc_clipped_function[im->type](im, val, flag);
	    done = 1;
	}
    }else{
	if (subc_function[im->type]) {
	    subc_function[im->type](im, val, flag);
	    done = 1;
	}
    }

    retcode = OK;
    if (! done) {
        ip_log_error(ERR_NOT_IMPLEMENTED);
        retcode = ip_error;
    }

    ip_pop_routine();
    return retcode;
}



/*

NAME:

   im_subc_complex() \- Subtract a constant complex value from an IM_COMPLEX
   image

PROTOTYPE:

   #include <ip/ip.h>

   int  im_subc_complex( im, z, flag)
   ip_image *im;
   ip_dcomplex z;
   int flag;

DESCRIPTION:

   Subtract the constant complex value 'z' from a complex (i.e. IM_COMPLEX
   or IM_DCOMPLEX) image.

   It is an error to call this function with an image that is not complex.

   'flag' must FLG_NONE.

   This routine is implemented as a call to im_addc_complex() with the
   negative of z.

ERRORS:

   ERR_BAD_TYPE
   ERR_BAD_VALUE

SEE ALSO:

   im_subc_rgb()       im_addc_complex()
   im_addc()           im_subc()      im_mulc()      im_divc()
   im_add()            im_sub()       im_mul()       im_div()

*/

int im_subc_complex(ip_image *im, ip_dcomplex z, int flag)
{
    return im_addc_complex(im, ip_dcomplex_neg(z), flag);
}


/*

NAME:

   im_subc_rgb() \- Subtract a constant RGB(A) value from an RGB image

PROTOTYPE:

   #include <ip/ip.h>

   int  im_subc_rgb( im, R, G, B, A, flag)
   ip_image *im;
   double R, G, B, A;
   int flag;

DESCRIPTION:

   Subtract the constant colour value (R,G,B) from an RGB or RGB16 image.
   Subtract the constant colour value (R,G,B,A) from an RGBA image.

   Flag can be FLG_CLIP to specify clipping of result to type limits.

ERRORS:

   ERR_BAD_TYPE
   ERR_BAD_VALUE

SEE ALSO:

   im_addc_complex()   im_addc_rgb()
   im_addc()           im_subc()      im_mulc()      im_divc()
   im_add()            im_sub()       im_mul()       im_div()

*/

int im_subc_rgb(ip_image *im, double r, double g, double b, double a, int flag)
{
   ip_log_routine(__func__);

    if (NOT image_valid(im))
	return ip_error;

   if (NOT type_rgbim(im->type)) {
      ip_log_error(ERR_BAD_TYPE);
      ip_pop_routine();
      return ip_error;
   }

   if (NOT ip_flag_ok(flag, FLG_CLIP, LASTFLAG)) {
      return ip_error;
   }

    r = round(r);
    g = round(g);
    b = round(b);
    a = round(a);

#ifdef HAVE_SIMD
    if (IP_USE_HWACCEL) {

	if (ip_simd_subc_rgb_function[im->type]) {
	    ip_simd_subc_rgb_function[im->type](im, r, g, b, a, flag);
	    ip_pop_routine();
	    return OK;
	}

    }
#endif

    if (flag & FLG_CLIP) {
	if (rgb_subc_clipped_function[im->type])
	    rgb_subc_clipped_function[im->type](im, r, g, b, a);
    }else{
	if (rgb_subc_function[im->type])
	    rgb_subc_function[im->type](im, r, g, b, a);

    }

   ip_pop_routine();
   return ip_error;
}




/*
 * Subroutines used by im_mulc() follow.
 */

#ifdef HAVE_64BIT
generate_image_cfixed_32_function(UBYTE, imulf)
generate_image_cfixed_32_function(BYTE, imulf)
generate_image_cfixed_32_shifted_function(UBYTE, idivf)
generate_image_cfixed_32_shifted_function(BYTE, idivf)
#else
generate_image_cfixed_24_function(UBYTE, imulf)
generate_image_cfixed_16_function(BYTE, imulf)
generate_image_cfixed_24_shifted_function(UBYTE, idivf)
generate_image_cfixed_16_shifted_function(BYTE, idivf)
#endif
generate_image_cfixed_32_function(USHORT, imulf)
generate_image_cfixed_32_function(SHORT, imulf)
generate_image_cfixed_32_function(ULONG, imulf)
generate_image_cfixed_32_function(LONG, imulf)
generate_image_cfixed_32_shifted_function(USHORT, idivf)
generate_image_cfixed_32_shifted_function(SHORT, idivf)
generate_image_cfixed_32_shifted_function(ULONG, idivf)
generate_image_cfixed_32_shifted_function(LONG, idivf)
generate_image_casis_function(FLOAT, mulc)
generate_image_casis_function(DOUBLE, mulc)

static improc_function_Icf im_mulc_function[IM_TYPEMAX] = {
    [IM_BYTE] = im_imulf_cfixed_BYTE,
    [IM_UBYTE] = im_imulf_cfixed_UBYTE,
    [IM_SHORT] = im_imulf_cfixed_SHORT,
    [IM_USHORT] = im_imulf_cfixed_USHORT,
    [IM_LONG] = im_imulf_cfixed_LONG,
    [IM_ULONG] = im_imulf_cfixed_ULONG,
    [IM_FLOAT] = im_mulc_casis_FLOAT,
    [IM_DOUBLE] = im_mulc_casis_DOUBLE,
    [IM_PALETTE] = im_imulf_cfixed_UBYTE
};

static improc_function_Icf im_divc_function[IM_TYPEMAX] = {
    [IM_BYTE] = im_idivf_cfixed_BYTE,
    [IM_UBYTE] = im_idivf_cfixed_UBYTE,
    [IM_SHORT] = im_idivf_cfixed_SHORT,
    [IM_USHORT] = im_idivf_cfixed_USHORT,
    [IM_LONG] = im_idivf_cfixed_LONG,
    [IM_ULONG] = im_idivf_cfixed_ULONG,
    [IM_FLOAT] = im_mulc_casis_FLOAT,
    [IM_DOUBLE] = im_mulc_casis_DOUBLE,
    [IM_PALETTE] = im_idivf_cfixed_UBYTE
};

#ifdef HAVE_64BIT
generate_image_cfixed_32_function(UBYTE, imulf_clipped)
generate_image_cfixed_32_function(BYTE, imulf_clipped)
#else
generate_image_cfixed_16_function(UBYTE, imulf_clipped)
generate_image_cfixed_16_function(BYTE, imulf_clipped)
#endif
generate_image_cfixed_32_function(USHORT, imulf_clipped)
generate_image_cfixed_32_function(SHORT, imulf_clipped)

static improc_function_Icf im_mulc_clipped_function[IM_TYPEMAX] = {
    [IM_BYTE] = im_imulf_clipped_cfixed_BYTE,
    [IM_UBYTE] = im_imulf_clipped_cfixed_UBYTE,
    [IM_SHORT] = im_imulf_clipped_cfixed_SHORT,
    [IM_USHORT] = im_imulf_clipped_cfixed_USHORT,
    [IM_PALETTE] = im_imulf_clipped_cfixed_UBYTE
};

generate_image_complex_function(COMPLEX, mulc)
generate_image_complex_function(DCOMPLEX, mulc)


/*
 * Helper function to select from the algorithms provided above.
 */

static int im_mulc_select_operator(ip_image *im, double val, int flags,
				   improc_function_Icf *mulc,
				   improc_function_Icf *mulc_clipped,
				   improc_function_Icf *divc)
{
    improc_function_Icf *im_operator = NULL;

    if (type_real(im->type))
	im_operator = mulc;
    else if (type_integer(im->type) || im->type == IM_PALETTE) {
	if (fabs(val) >= 0.5) {
	    if (flags & FLG_CLIP)
		im_operator = mulc_clipped;
	    else
		im_operator = mulc;
	}else{
	    im_operator = divc;
	}
    }

    if (im_operator && im_operator[im->type]) {
	im_operator[im->type](im, val, flags);
	return TRUE;
    }else{
	return FALSE;
    }
}



/*

NAME:

   im_mulc() \- Multiply an image by a constant value

PROTOTYPE:

   #include <ip/ip.h>

   int  im_mulc( im, value, flag )
   ip_image *im;
   double value;
   int flag;

DESCRIPTION:

   Multiply an image by a constant value.  The multiplication is carried out
   by fixed point multiplication if the image is of integral type, and by
   floating point arithmetic if it is of real or complex type.  Thus one can
   multiply an IM_UBYTE image with, say, 1.5 and get all pixels increased to
   the nearest integer that is 50% greater than their original value.

   Integer and floating point type images are fully supported.  Complex
   images are better supported by the im_mulc_complex() function, however
   you can multiple a COMPLEX image by a real or imaginary value by using
   the flags FLG_SREAL or FLG_SIMAG in this routine.  Likewise RGB colour
   images are better support by im_mulc_rgb() however the use of the flags
   FLG_SRED, FLG_SGREEN, FLG_SBLUE, FLG_SALPHA enable some basic colour
   image multiplications with this routine.

   For integral images (signed and unsigned BYTE, SHORT and LONG) fixed
   point arithmetic is used to improve execution speed. The result is as if
   the computation was performed at 64-bit precision, truncated back to a
   32-bit whole number part (keeping the fraction intact), rounded to the
   nearest integer with ties away from zero, then truncated to the result
   type.

   In practice, the precision may be less for (U)BYTE on 32-bit arches or in
   SIMD code to ensure the whole computation can be done in 32-bits,
   particularly for the clipped variant which reduces the fraction to
   16-bits only.  Note that (U)LONG calculations are performed with a 32-bit
   whole number part thus truncation of the result to (U)LONG happens before
   rounding.  This might lead to surprising results for LONG as the sign of
   the result for determining rounding is detected after truncation.  For
   in-range results (when no truncation has occurred) all rounding is
   correct.

   Also for correct operation on integral image types, 'value' should be
   between (inclusive) the reciprocal of the type maximum and the type
   maximum.  No check is performed on 'value' before the multiplication is
   performed.

   If the image type is IM_BYTE, IM_UBYTE, IM_USHORT or IM_SHORT and flag is
   set to FLG_CLIP then the result of the multiplication is clipped to the
   appropriate data type limits.  Be aware that because fixed-point
   calculations of limited resolution could be used, the head-room for
   detecting overflow and, thus, the need for clipping is limited.  For
   (U)BYTE image, only multipliers between 1/255 and 255 are guaranteed to
   work.  Anything larger or smaller multiplier might lead to incorrectly
   clipped results (and in this case, one should be using a thresholding
   operation as that is what it effectively amounts to!).  Clipping is only
   in effect when the input muliplier ('value') is greater than one; the
   clipping flag is silently ignored if 'value' is less than one.

ERRORS:

   ERR_BAD_TYPE         ERR_BAD_FLAG

SEE ALSO:

   im_mulc_complex()         im_mulc_rgb()
   im_addc()    im_subc()    im_divc()
   im_add()     im_sub()     im_mul()     im_div()

*/

int im_mulc(ip_image *im, double val, int flag)
{
    int retcode;
    int done = 0;

    ip_log_routine(__func__);

    if (NOT image_valid(im))
	return ip_error;

    if (image_bad_type(im, IM_BINARY, -1))
	return ip_error;

    if (type_complex(im->type)) {

	if (NOT ip_flag_ok(flag,  FLG_SREAL, FLG_SIMAG,
			   FLG_SREAL|FLG_SIMAG, LASTFLAG))
	    return ip_error;

	retcode = im_mulc_complex(im,
				  (ip_dcomplex){flag & FLG_SREAL ? val : 0.0,
						flag & FLG_SIMAG ? val : 0.0}, NO_FLAG);

	ip_pop_routine();
	return retcode;
    }

    if (type_rgbim(im->type)) {

	if (NOT ip_flag_bits_unused(flag, ~(FLGPRT_COLOUR | FLG_CLIP)))
	    return ip_error;

	/* Require at least one colour specificed */
	if (flag == FLG_NONE || flag == FLG_CLIP) {
	    ip_log_error(ERR_BAD_FLAG);
	    retcode = ip_error;
	}else{
	    retcode = im_mulc_rgb(im,
				  (flag & FLG_SRED) ? val : 1.0,
				  (flag & FLG_SGREEN) ? val : 1.0,
				  (flag & FLG_SBLUE) ? val : 1.0,
				  (flag & FLG_SALPHA) ? val : 1.0,
				  flag & FLG_CLIP);
	}

	ip_pop_routine();
	return retcode;
    }

    if (type_real(im->type) || im->type == IM_LONG) {

	/* No flags possible */
	if (NOT ip_flag_ok(flag, LASTFLAG))
	    return ip_error;

    }else{

	/* lower precision scalar image types including palette */
	if (NOT ip_flag_ok(flag, FLG_CLIP, LASTFLAG))
	    return ip_error;

    }

    if (val == 1.0) {
	/* Identity operator; no point in processing. */
	ip_pop_routine();
	return OK;
    }

    if (val == 0.0 && (type_integer(im->type) || im->type == IM_PALETTE)) {
	/*
	 * Special case; result is obviously zero.  We don't special case
	 * this for floating-point data because multiplication by zero does
	 * not necessarily result in a zero.
	 */
	int err = im_clear(im);
	ip_pop_routine();
	return err;
    }

#ifdef HAVE_SIMD
    if (IP_USE_HWACCEL) {
	done = im_mulc_select_operator(im, val, flag,
				       ip_simd_mulc_function,
				       ip_simd_mulc_clipped_function,
				       ip_simd_divc_function);

	if (done) {
	    ip_pop_routine();
	    return OK;
	}
    }
#endif

    done = im_mulc_select_operator(im, val, flag,
				   im_mulc_function,
				   im_mulc_clipped_function,
				   im_divc_function);

    if (! done) {
	ip_log_error(ERR_NOT_IMPLEMENTED);
	ip_pop_routine();
	return ERR_NOT_IMPLEMENTED;
    }

    ip_pop_routine();
    return OK;
}



/*

NAME:

   im_mulc_complex() \- Multiply a COMPLEX image by a constant complex value

PROTOTYPE:

   #include <ip/ip.h>

   int  im_mulc_complex( im, z, flag)
   ip_image *im;
   ip_dcomplex z;
   int flag;

DESCRIPTION:

   Multiply the image 'im', which must be one of the two complex types, by
   the constant complex value 'z' (where z = u + iv).

   'flag' must be FLG_NONE.

   It is an error to call this function on anything but a complex image.

ERRORS:

   ERR_BAD_TYPE
   ERR_BAD_VALUE

SEE ALSO:

   im_addc_complex()   im_subc_complex()
   im_addc()           im_subc()      im_mulc()      im_divc()
   im_add()            im_sub()       im_mul()       im_div()
   im_mulc_rgb()

*/

int im_mulc_complex(ip_image *im, ip_dcomplex z, int flag)
{
    ip_log_routine(__func__);

    if (NOT image_valid(im))
	return ip_error;

    if (NOT type_complex(im->type)) {
	ip_log_error(ERR_BAD_TYPE);
	ip_pop_routine();
	return ip_error;
    }

    if (flag != NO_FLAG) {
	ip_log_error(ERR_BAD_FLAG);
	ip_pop_routine();
	return ip_error;
    }

#ifdef HAVE_SIMD
    if (IP_USE_HWACCEL) {

	if (ip_simd_mulc_complex_function[im->type]) {
	    ip_simd_mulc_complex_function[im->type](im, z, flag);
	    ip_pop_routine();
	    return OK;
	}

    }
#endif

    /* No hardware accelarator available -- general code follows */
    if (im->type == IM_COMPLEX) {
	ip_complex zval;

	zval.r = (float)z.r;
	zval.i = (float)z.i;

	im_mulc_complex_COMPLEX(im, zval);

    }else{			/* IM_DCOMPLEX */

	im_mulc_complex_DCOMPLEX(im, z);
    }

    ip_pop_routine();
    return ip_error;
}


#if 0
#define generate_pixel_rgb_fixed_op(type, basetype, fixtype, op)	\
    static inline type ## _type						\
    pixel_ ## op ## _ ## type(type ## _type p, fixtype r, fixtype b, fixtype g) \
    {									\
	return (type ## _type){						\
	    pixel_ ## op ## _ ## basetype(p.r, r),			\
	    pixel_ ## op ## _ ## basetype(p.g, g),			\
	    pixel_ ## op ## _ ## basetype(p.b, b)};			\
    }

#define generate_pixel_rgba_fixed_op(type, basetype, fixtype, op)	\
    static inline type ## _type						\
    pixel_ ## op ## _ ## type(type ## _type p, fixtype r, fixtype b, fixtype g, fixtype a) \
    {									\
	return (type ## _type){						\
	    pixel_ ## op ## _ ## basetype(p.r, r),			\
	    pixel_ ## op ## _ ## basetype(p.g, g),			\
	    pixel_ ## op ## _ ## basetype(p.b, b),			\
	    pixel_ ## op ## _ ## basetype(p.a, a)};			\
    }

#ifdef HAVE_64BIT
generate_pixel_rgb_fixed_op(RGB, UBYTE, uint64_t, imulf);
generate_pixel_rgb_fixed_op(RGB, UBYTE, uint64_t, imulf_clipped);
generate_pixel_rgba_fixed_op(RGBA, UBYTE, uint64_t, imulf);
generate_pixel_rgba_fixed_op(RGBA, UBYTE, uint64_t, imulf_clipped);
#else
generate_pixel_rgb_fixed_op(RGB, UBYTE, uint32_t, imulf);
generate_pixel_rgb_fixed_op(RGB, UBYTE, uint32_t, imulf_clipped);
generate_pixel_rgba_fixed_op(RGBA, UBYTE, uint32_t, imulf);
generate_pixel_rgba_fixed_op(RGBA, UBYTE, uint32_t, imulf_clipped);
#endif
generate_pixel_rgb_fixed_op(RGB16, USHORT, uint64_t, imulf);
generate_pixel_rgb_fixed_op(RGB16, USHORT, uint64_t, imulf_clipped);

#define generate_image_rgb_fixed_function(type, operator)		\
    static void								\
    im_ ## operator ## _rgb_ ## type(ip_image *d, double r, double g, double b, double a) \
    {									\
	type ## _type c = ip_rgb_fixed_alloc_const_ ## type(r, g, b, a); \
	PROCESS_IMAGE(d, c, type, operator);				\
    }

#ifdef HAVE_64BIT
#define ip_rgb_fixed_alloc_const_RGB(r,g,b,a)	\
    (ip_rgb){(uint64_t)r, (uint64_t)g, (uint64_t)b}
#define ip_rgb_fixed_alloc_const_RGBA(r,g,b,a)	\
    (ip_rgba){(uint64_t)r, (uint64_t)g, (uint64_t)b, (uint64_t)a}
#else
#define ip_rgb_fixed_alloc_const_RGB(r,g,b,a)	\
    (ip_rgb){(uint32_t)r, (uint32_t)g, (uint32_t)b}
#define ip_rgb_fixed_alloc_const_RGBA(r,g,b,a)	\
    (ip_rgba){(uint32_t)r, (uint32_t)g, (uint32_t)b, (uint32_t)a}
#endif
#define ip_rgb_fixed_alloc_const_RGB16(r,g,b,a)	\
    (ip_lrgb){(uint64_t)r, (uint64_t)g, (uint64_t)b}

generate_image_rgb_fixed_function(RGB, imulf);
generate_image_rgb_fixed_function(RGBA, imulf);
generate_image_rgb_fixed_function(RGB16, imulf);

generate_image_rgb_fixed_function(RGB, imulf_clipped);
generate_image_rgb_fixed_function(RGBA, imulf_clipped);
generate_image_rgb_fixed_function(RGB16, imulf_clipped);
#endif

/*

NAME:

   im_mulc_rgb() \- Multiply a RGB image by a constant RGB value

PROTOTYPE:

   #include <ip/ip.h>

   int  im_mulc_rgb( im, R, G, B, A, flag)
   ip_image *im;
   double R, G, B, A;
   int flag;

DESCRIPTION:

   Multiply an RGB or RGB16 image by the constant colour value
   (R,G,B). Multiply an RGBA image by the constant colour value (R,G,B,A).
   Flag can be FLG_CLIP to specify clipping of result to type limits.

ERRORS:

   ERR_BAD_TYPE
   ERR_BAD_VALUE

SEE ALSO:

   im_mulc_complex()
   im_addc()           im_subc()      im_mulc()      im_divc()
   im_add()            im_sub()       im_mul()       im_div()

*/

int im_mulc_rgb(ip_image *im, double r, double g, double b, double a, int flag)
{
    ip_log_routine(__func__);

    if (NOT image_valid(im))
	return ip_error;

    if (NOT type_rgbim(im->type)) {
	ip_log_error(ERR_BAD_TYPE);
	ip_pop_routine();
	return ip_error;
    }

    if (NOT ip_flag_ok(flag, FLG_CLIP, LASTFLAG)) {
	return ip_error;
    }

#ifdef HAVE_SIMD
    if (IP_USE_HWACCEL) {

	if (ip_simd_mulc_rgb_function[im->type]) {
	    ip_simd_mulc_rgb_function[im->type](im, r, g, b, a, flag);
	    ip_pop_routine();
	    return OK;
	}

    }
#endif

   if (im->type == IM_RGB || im->type == IM_RGBA) {
#ifdef HAVE_64BIT
	uint64_t rval = (uint64_t)round(4294967296.0 * r);
	uint64_t gval = (uint64_t)round(4294967296.0 * g);
	uint64_t bval = (uint64_t)round(4294967296.0 * b);
	uint64_t aval = (uint64_t)round(4294967296.0 * a);
#else
	uint32_t rval = (uint32_t)round(65536.0 * r);
	uint32_t gval = (uint32_t)round(65536.0 * g);
	uint32_t bval = (uint32_t)round(65536.0 * b);
	uint32_t aval = (uint32_t)round(65536.0 * a);
#endif
	if (flag & FLG_CLIP) {

	    if (im->type == IM_RGB) {
		/* im->type == IM_RGB with clipping */
		for (int j=0; j<im->size.y; j++) {
		    ip_rgb *pp;
		    pp = IM_ROWADR(im, j, r);
		    for (int i=0; i<im->size.x; i++) {
			pixel_imulf_clipped_UBYTE(pp->r, rval);
			pixel_imulf_clipped_UBYTE(pp->g, gval);
			pixel_imulf_clipped_UBYTE(pp->b, bval);
			pp++;
		    }
		}
	    }else{
		/* im->type == IM_RGBA with clipping */
		for (int j=0; j<im->size.y; j++) {
		    ip_rgba *pp;
		    pp = IM_ROWADR(im, j, ra);
		    for (int i=0; i<im->size.x; i++) {
			pixel_imulf_clipped_UBYTE(pp->r, rval);
			pixel_imulf_clipped_UBYTE(pp->g, gval);
			pixel_imulf_clipped_UBYTE(pp->b, bval);
			pixel_imulf_clipped_UBYTE(pp->a, aval);
			pp++;
		    }
		}
	    }

	}else{

	    if (im->type == IM_RGB) {
		/* im->type == IM_RGB and no clipping */
		for (int j=0; j<im->size.y; j++) {
		    ip_rgb *pp;
		    pp = IM_ROWADR(im, j, r);
		    for (int i=0; i<im->size.x; i++) {
			pixel_imulf_UBYTE(pp->r, rval);
			pixel_imulf_UBYTE(pp->g, gval);
			pixel_imulf_UBYTE(pp->b, bval);
			pp++;
		    }
		}
	    }else{
		/* im->type == IM_RGBA and no clipping */
		for (int j=0; j<im->size.y; j++) {
		    ip_rgba *pp;
		    pp = IM_ROWADR(im, j, ra);
		    for (int i=0; i<im->size.x; i++) {
			pixel_imulf_UBYTE(pp->r, rval);
			pixel_imulf_UBYTE(pp->g, gval);
			pixel_imulf_UBYTE(pp->b, bval);
			pixel_imulf_UBYTE(pp->a, aval);
			pp++;
		    }
		}
	    }

	}

   }else{
       /* im->type == IM_RGB16 */
	uint64_t rval = (uint64_t)round(4294967296.0 * r);
	uint64_t gval = (uint64_t)round(4294967296.0 * g);
	uint64_t bval = (uint64_t)round(4294967296.0 * b);

	if (flag & FLG_CLIP) {
	    /* im->type == IM_RGB16 with clipping */
	    for (int j=0; j<im->size.y; j++) {
		ip_lrgb *pp;
		pp = IM_ROWADR(im, j, r16);
		for (int i=0; i<im->size.x; i++) {
		    pixel_imulf_clipped_USHORT(pp->r, rval);
		    pixel_imulf_clipped_USHORT(pp->g, gval);
		    pixel_imulf_clipped_USHORT(pp->b, bval);
		    pp++;
		}
	    }
	}else{
	    /* im->type == IM_RGB16 and no clipping */
	    for (int j=0; j<im->size.y; j++) {
		ip_lrgb *pp;
		pp = IM_ROWADR(im, j, r16);
		for (int i=0; i<im->size.x; i++) {
		    pixel_imulf_USHORT(pp->r, rval);
		    pixel_imulf_USHORT(pp->g, gval);
		    pixel_imulf_USHORT(pp->b, bval);
		    pp++;
		}
	    }
	}

   }

   ip_pop_routine();
   return ip_error;
}




generate_image_casis_function(FLOAT, divc)
generate_image_casis_function(DOUBLE, divc)

generate_image_complex_function(COMPLEX, divc_a)
generate_image_complex_function(COMPLEX, divc_b)
generate_image_complex_function(DCOMPLEX, divc_a)
generate_image_complex_function(DCOMPLEX, divc_b)



/*
 * idivc
 *
 * Integer division operators, i.e., divide integer pixels by an integral
 * constant, truncating results towards zero. This implements conventional C
 * integer division.
 *
 * We use the integer division routines from divide.c.
 */


#define generate_idivc_unsigned_function(tt)				\
    static void im_idivc_##tt(ip_image *d, int64_t c, int flag)		\
    {									\
	struct ip_uint32_rcp rcp = ip_urcp32((uint32_t)c);		\
	int rowlen = ip_type_size(d->type) / ip_type_size(IM_ ## tt);	\
	rowlen *= d->size.x;						\
	for (int j=0; j<d->size.y; j++) {				\
	    tt##_type *dptr = IM_ROWADR(d, j, v);			\
	    for (int i=0; i<rowlen; i++) {				\
		*dptr = (tt##_type)ip_udiv32((uint32_t)*dptr, rcp);	\
		dptr++;							\
	    }								\
	}								\
    }

generate_idivc_unsigned_function(UBYTE)
generate_idivc_unsigned_function(USHORT)
generate_idivc_unsigned_function(ULONG)

#define generate_idivc_signed_function(tt)				\
    static void im_idivc_##tt(ip_image *d, int64_t c, int flag)		\
    {									\
	struct ip_int32_rcp rcp = ip_ircp32((uint32_t)c);		\
	int rowlen = ip_type_size(d->type) / ip_type_size(IM_ ## tt);	\
	rowlen *= d->size.x;						\
	for (int j=0; j<d->size.y; j++) {				\
	    tt##_type *dptr = IM_ROWADR(d, j, v);			\
	    for (int i=0; i<rowlen; i++) {				\
		*dptr = (tt##_type)ip_idiv32((uint32_t)*dptr, rcp);	\
		dptr++;							\
	    }								\
	}								\
    }

generate_idivc_signed_function(BYTE)
generate_idivc_signed_function(SHORT)
generate_idivc_signed_function(LONG)

static improc_function_Iif im_idivc_function[IM_TYPEMAX] = {
    [IM_BYTE] = im_idivc_BYTE,
    [IM_UBYTE] = im_idivc_UBYTE,
    [IM_SHORT] = im_idivc_SHORT,
    [IM_USHORT] = im_idivc_USHORT,
    [IM_LONG] = im_idivc_LONG,
    [IM_ULONG] = im_idivc_ULONG
};


/*

NAME:

   im_divc() \- divide an image by a constant value

PROTOTYPE:

   #include <ip/ip.h>

   int  im_divc( im, value, flag )
   ip_image *im;
   double value;
   int flag;

DESCRIPTION:

   Divide an image by a constant value.

   By default for integer based images (scalar and colour) this routine
   calculates the division by effectively calling im_mulc() with the
   reciprocal of 'value'.  This method of division allows integer images to
   be scaled by non-integral values with conventional rounding efficiently.
   See im_mulc() for further information.

   If FLG_INTDIV is past with an integer type image then 'value' (the
   divisor) is rounded to the nearest integer and conventional integer
   division is performed with the result of the division truncated towards
   zero.

   By default for floating point images floating point division is performed
   with the result correctly rounded to the nearest floating point number
   according to IEEE rules.  On most architectures floating point division
   is not pipe-lineable, thus blocks further ready floating-point CPU
   instructions from executing on the pipeline and can be very slow.  Pass
   the flag FLG_FASTFLOAT to allow calculation by multiplication with the
   reciprocal or reciprocal appromixations.  Correct rounding to the last
   bit is not guaranteed with FLG_FASTFLOAT however processing speed is
   usually far faster.

   Division by zero is an undefined operation and generally considered not a
   very bright idea.  An attempt to divide by zero is NOT checked for and
   might cause a trap.

ERRORS:

   ERR_BAD_TYPE

SEE ALSO:

   im_divc_complex()
   im_addc()    im_subc()    im_mulc()
   im_add()     im_sub()     im_mul()     im_div()

*/

int im_divc(ip_image *im, double val, int flag)
{
    int retcode;
    int done = 0;

    ip_log_routine(__func__);

    if (NOT image_valid(im))
	return ip_error;

    if (type_complex(im->type)) {

	if (NOT ip_flag_ok(flag,  FLG_SREAL, FLG_SIMAG,
			   FLG_SREAL|FLG_SIMAG, LASTFLAG))
	    return ip_error;

	retcode = im_divc_complex(im,
				  (ip_dcomplex){flag & FLG_SREAL ? val : 1.0,
						flag & FLG_SIMAG ? val : 1.0}, NO_FLAG);

	ip_pop_routine();
	return retcode;
    }

    if (NOT type_real(im->type)) {
	/* Integer and colour type images */

	if (NOT ip_flag_ok(flag, FLG_INTDIV, LASTFLAG))
	    return ip_error;

	if (flag & FLG_INTDIV) {
	    /* Perform conventional integer division by an integer divisor. */
	    int64_t divisor = (int64_t)LROUND(val);

	    /*
	     * Avoid division by 1 which provides a small optimisation in
	     * the unsigned division routines on some architectures.
	     */
	    if (divisor != 1) {
#ifdef HAVE_SIMD
		if (IP_USE_HWACCEL) {
		    if (ip_simd_idivc_function[im->type]) {
			ip_simd_idivc_function[im->type](im, val, flag);
			done = 1;
		    }
		}
#endif
		if (!done) {
		    if (im_idivc_function[im->type] != NULL)
			im_idivc_function[im->type](im, divisor, flag);
		    else
			ip_log_error(ERR_NOT_IMPLEMENTED);
		}
	    }
	    retcode = OK;
	}else{
	    /* Perform division by multiplication with reciprocal. */
	    retcode = im_mulc(im, 1.0/val, flag);
	}
	ip_pop_routine();
	return retcode;
    }

    if (NOT type_integer(im->type)) {
	if (NOT ip_flag_ok(flag, FLG_CLIP, LASTFLAG))
	    return ip_error;
    }

#ifdef HAVE_SIMD
    if (IP_USE_HWACCEL) {
	if (ip_simd_divc_function[im->type]) {
	    ip_simd_divc_function[im->type](im, val, flag);
	    done = 1;
	}
    }

    if (done) {
        ip_pop_routine();
        return OK;
    }
#endif

    if (NOT ip_flag_ok(flag, FLG_FASTFLOAT, LASTFLAG))
	return ip_error;

    if (flag & FLG_FASTFLOAT) {
	/* Use reciprocal with multiplication */
	retcode = im_mulc(im, 1.0/val, NO_FLAG);
	ip_pop_routine();
	return retcode;
    }

    if (im->type == IM_FLOAT) {
	im_divc_casis_FLOAT(im, val, flag);
    }else{
	im_divc_casis_DOUBLE(im, val, flag);
    }

    ip_pop_routine();
    return OK;
}



/*

NAME:

   im_divc_complex() \- Divide a COMPLEX image by a constant complex value

PROTOTYPE:

   #include <ip/ip.h>

   int  im_divc_complex( im, z, flag)
   ip_image *im;
   ip_dcomplex z;
   int flag;

DESCRIPTION:

   Divide the image 'im', which must be one of the two complex types, by a
   constant complex value.  This could be achieved by passing the complex
   reciprocal of u+iv to im_mulc_complex(), however you run the risk of
   causing an overflow in the calculation, despite the fact that the result
   of the complex division can be represented as a finite inrange complex
   number.  This routine, im_divc_complex(), rearranges the calculation to
   avoid overflows mid-calculation.

ERRORS:

   ERR_BAD_TYPE
   ERR_BAD_VALUE

SEE ALSO:

   im_mulc_complex()
   im_addc()           im_subc()      im_mulc()      im_divc()
   im_add()            im_sub()       im_mul()       im_div()

*/

int im_divc_complex(ip_image *im, ip_dcomplex z, int flag)
{
    ip_log_routine(__func__);

    if (NOT image_valid(im))
	return ip_error;

    if (NOT type_complex(im->type)) {
	ip_log_error(ERR_BAD_TYPE);
	ip_pop_routine();
	return ip_error;
    }

    if (flag != NO_FLAG) {
	ip_log_error(ERR_BAD_FLAG);
	ip_pop_routine();
	return ip_error;
    }

#ifdef HAVE_SIMD
    if (IP_USE_HWACCEL) {

	if (ip_simd_divc_complex_function[im->type]) {
	    ip_simd_divc_complex_function[im->type](im, z, flag);
	    ip_pop_routine();
	    return OK;
	}

    }
#endif

    if (im->type == IM_COMPLEX) {
	ip_complex zval;
	ip_complex c;

	zval.r = (float)z.r;
	zval.i = (float)z.i;

	if (fabsf(zval.r) >= fabsf(zval.i)) {
	    /* Trick: divide numerator and denominator by zval.r */
	    c.r = zval.i / zval.r;
	    c.i = zval.r + zval.i*c.r;
	    im_divc_a_complex_COMPLEX(im, c);
	}else{
	    /* Trick: divide numerator and denominator by zval.i */
	    c.r = zval.r / zval.i;
	    c.i = zval.r*c.r + zval.i;
	    im_divc_b_complex_COMPLEX(im, c);
	}
    }else{			/* IM_DCOMPLEX */
	ip_dcomplex c;

	if (fabs(z.r) >= fabs(z.i)) {
	    /* Trick: divide numerator and denominator by z.r */
	    c.r = z.i / z.r;
	    c.i = z.r + z.i*c.r;
	    im_divc_a_complex_DCOMPLEX(im, c);
	}else{
	    /* Trick: divide numerator and denominator by z.i */
	    c.r = z.r / z.i;
	    c.i = z.r*c.r + z.i;
	    im_divc_b_complex_DCOMPLEX(im, c);
	}
    }

    ip_pop_routine();
    return ip_error;
}
