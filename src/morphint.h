/*
   morphint.h
   by M. J. Cree

   Copyright (C) 2015 Michael J. Cree

   Definitions for the morphological processing code that should be kept
   internal to the library.
*/

#include <ip/types.h>
#include <ip/image.h>
#include <ip/morph.h>

/*
 * For the morphchord approach to morphological processing.
 */


/*
 * The chord table constructed as the first stage of morphological dilation
 * or erosion.
 */

typedef struct {
    /* Image details this table is constructed for */
    ip_image_type type;
    /* The structuring element (with the sorted list of chord lengths) */
    ip_strel *strel;
    /* The constructed table */
    int operator;	     /* 0 = erosion, 1 = dilation */
    int numrows;	     /* number of rows in table */
    int length;		     /* length of a row (in pixels) */
    int row_size;	     /* size of the padded row in bytes */
    int offset;		     /* offset of row to left (top) edge of image */
    int row;		     /* Row of image last updated on */
    char *table;	     /* Pointer to table data buffer */
    void **ctrow;	     /* Pointer to row address array */
} ip_chordtbl;


/*
 * This gives the address (of the row of chord length 1) of a specific row
 * in the chord table.  Use chordtbl_cl_rowadr() to get to a specific chord
 * length other than 1.
 */

#define CHORDTBL_ROWADR(ct, r) ((ct)->ctrow[r])


/*
 * Get address of table row 'row' at the chord length 'clen' in the table.
 * If compiling with SIMD support each row of a specific chord length must
 * be aligned to the SIMD vector alignment, hence the padding included in
 * ct->row_size used to calculate these row/chordlen addresses.
 *
 * We return void * as this can be cast to any pointer type, thus should
 * stop compiler warnings about up-casting pointers which break alignment.
 */

static inline void *chordtbl_cl_rowadr(ip_chordtbl *ct, int row, int clen)
{
    char *adr = CHORDTBL_ROWADR(ct, row);
    adr += ct->row_size * clen;
    return (void *)adr;
}


/* The prototype for the chord table function helpers */

typedef void (*ct_function_pIi)(void *p,  ip_image *im, int b);
typedef void (*ct_function_piii)(void *trow, int c1, int c2, int c3);
typedef void (*ct_function_ITi)(ip_image *im, ip_chordtbl *ct, int row);
typedef void (*ct_function_fTiii)(FILE *f, ip_chordtbl *ct, int c1, int c2, int c3);

/* And the standard morphology functions */

typedef void (*improc_morphfun_se2)(ip_image *, int);
typedef void (*improc_morphfun_horiz)(ip_image *, int, int, ip_image *);
typedef void (*improc_morphfun_vert)(ip_image *, int, int, ip_image *, ip_image *);

