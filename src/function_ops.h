#ifndef IP_FUNCTION_OPS_H
#define IP_FUNCTION_OPS_H

/* Returns void */


typedef void (*improc_function_I)(ip_image *d);
typedef void (*improc_function_II)(ip_image *d, ip_image *s);
typedef void (*improc_function_IIf)(ip_image *d, ip_image *s, int f);
typedef void (*improc_function_Icf)(ip_image *d, double c, int f);
typedef void (*improc_function_Iif)(ip_image *d, int64_t c, int f);
typedef void (*improc_function_Icccc)(ip_image *d, double c1, double c2, double c3, double c4);
typedef void (*improc_function_Iccccf)(ip_image *d, double c1, double c2, double c3, double c4, int f);
typedef void (*improc_function_Izf)(ip_image *d, ip_dcomplex z, int f);

typedef void (*improc_function_IIccb)(ip_image *d, ip_image *s, double c1, double c2, uint8_t b);

typedef void (*improc_function_Ia)(ip_image *d, ip_anyval v);
typedef void (*improc_function_Iaf)(ip_image *d, ip_anyval v, int f);

/* Returns int */
typedef int (*improc_function_i_II)(ip_image *d, ip_image *s);

struct function_list {
    improc_function_IIf *op_same;
    improc_function_IIf *op_clipped_same;
    improc_function_IIf *op_mixed;
    improc_function_IIf *op_clipped_mixed;
};

#endif
