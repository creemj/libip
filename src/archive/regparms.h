/* $Id$ */
/*
   Header: ip/regparms.h
   Author: M.J.Cree

   Header file for using regparms objects.

*/


#ifndef IP_REGPARMS_H
#define IP_REGPARMS_H

#include <ip/types.h>

extern regparms *alloc_regparms(void);
extern void free_regparms(regparms *rp);


#endif
