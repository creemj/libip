/* $Id$ */
/*
   Module: ip/poly2d.c
   Author: J.H.Hipwell
           M.J.Cree


*/


#include <stdlib.h>
#include <math.h>

#include <ip/internal.h>

static char rcsid[] = "$Id$";




/*

NAME:

   alloc_poly2d() \- Allocate a 2d polynomial

PROTOTYPE:

   #include <ip/ip.h>

   poly2d * alloc_poly2d( dimension, order )
   int dimension;
   int order;

DESCRIPTION:

   Allocate a poly2d type polynomial of the specified 'dimension' and
   'order'.  All coefficients are set to zero.

   Only one-dimensional and two-dimensional polynomials are currently
   supported.

   Note that poly2d objects are polynomials that return a
   two-dimensional value when evaluated, whereas poly objects (see
   alloc_poly()) return a one-dimensional value when evaluated.  In
   contrast, the 'dimension' passed to the allocate routines is the
   dimension of the polynomial domain.

ERRORS:

   ERR_OUT_OF_MEM
   ERR_OOR_PARAMETER
   ERR_TOOSMALL_PARAMETER

SEE ALSO:

   free_poly2d()
   poly2d_evaluate()   poly2d_numcoeffs()    ip_pointmap()
   alloc_poly()

AUTHOR:
 
\|   John Hipwell (j.hipwell@biomed.abdn.ac.uk)
\|   Michael Cree (m.cree@biomed.abdn.ac.uk)

*/

poly2d *alloc_poly2d(int dimension, int order)
{
   poly2d *p;
   int polypts;
   int i;

   log_routine("alloc_poly2d");

   if (NOT ip_parm_inrange("poly_dimension", dimension, 1, 2))
      return NULL;

   if (NOT ip_parm_greatereq("poly_order", order, 0))
      return NULL;

   if ((p = ip_malloc(sizeof(poly2d))) == NULL)
      return NULL;

   if ((polypts = poly_numcoeffsx(dimension, order)) == -1) {
      ip_free(p);
      return NULL;
   }

   if ((p->X = ip_malloc(sizeof(double) * polypts * 2)) == NULL) {
      ip_free(p);
      return NULL;
   }
   p->Y = p->X + polypts;

   p->dimension = dimension;
   p->order = order;

   for (i=0; i<polypts; i++) {
      p->X[i] = 0.0;
      p->Y[i] = 0.0;
   }

   pop_routine();
   return p;
}



/*

NAME:

   free_poly2d() \- Free a polynomial

PROTOTYPE:

   #include <ip/ip.h>

   void  free_poly2d( p )
   poly2d *p;

DESCRIPTION:

   Free the polynomial 'p' up.

SEE ALSO:

   alloc_poly2d()  

AUTHOR:
 
\|   John Hipwell (j.hipwell@biomed.abdn.ac.uk)
\|   Michael Cree (m.cree@biomed.abdn.ac.uk)

*/


void free_poly2d(poly2d *p)
{
   if (p) {
      ip_free(p->X);
      ip_free(p);
   }
}


/*

NAME:

   poly2d_evaluate() \- Evaluate a polynomial at a point

PROTOTYPE:

   #include <ip/ip.h>

   vector2d poly2d_evaluate( p, x, y )
   poly2d *p;
   double x;
   double y;

DESCRIPTION:

   Evaluate the polynomial at the point ('x','y').

   For a two-dimensional poly2d object and if p->X[] and p->Y[] are
   written as pX[] and pY[] then the vector v returned will evaluate
   as follows:

\|                                                      order
\|   v.x     =   pX[0]   	         	          0
\|             + pX[1].x     + pX[2].y			  1
\|             + pX[3].x.x   + pX[4].x.y   + pX[5].y.y    2
\|             + pX[6].x.x.x + pX[7].x.x.y .....          3
\|
\|   v.y     =   pY[0]   	                          0
\|             + pY[1].x     + pY[2].y			  1
\|             + pY[3].x.x   + pY[4].x.y   + pY[5].y.y    2
\|             + pY[6].x.x.x + pY[7].x.x.y .....          3

SEE ALSO:
	
   alloc_poly2d() free_poly2d() ip_pointmap() 

AUTHOR:
 
\|   John Hipwell (j.hipwell@biomed.abdn.ac.uk)
\|   Michael Cree (m.cree@biomed.abdn.ac.uk)

*/

vector2d poly2d_evaluate(poly2d *p, double x, double y)
{
   int count, i, j;
   double pow1_x, pow2_x, pow_y;
   vector2d res;


   if (p->dimension == 1) {
      res.x = p->X[p->order];
      res.y = p->Y[p->order];
      for (i=p->order-1; i>=0; i--) {
	 res.x *= x;
	 res.x += p->X[i];
	 res.y *= y;
	 res.y += p->Y[i];
      }
   }
   else if (p->dimension == 2) {
      res.x = 0.;
      res.y = 0.;

      count = 0;
      pow1_x = 1;
      for (j=1; j<=p->order+1; j++) {
	 pow_y = 1;
 	 pow2_x = pow1_x;
	 for (i=1; i<=j; i++) {
	    if (x == 0.) pow2_x = (i < j) ? 0. : 1.;

	    res.x += p->X[count]*pow2_x*pow_y;
	    res.y += p->Y[count]*pow2_x*pow_y;

	    if (x != 0.) pow2_x /= (float) x;
	    pow_y *= y;

	    count++;
	 }
	 pow1_x *= x;
      }
   }
   else{
      res.x = 0.;
      res.y = 0.;
   }
   return res;
}


/*

NAME:

   poly2d_numcoeffs() \- Return the number of coefficients of a polynomial

PROTOTYPE:

   #include <ip/ip.h>

   int  poly2d_numcoeffs( p )
   poly2d *p;

DESCRIPTION:

   Return the number of coefficients the polynomial 'p' has.

SEE ALSO:

   alloc_poly2d()

AUTHOR:
 
\|   John Hipwell (j.hipwell@biomed.abdn.ac.uk)
\|   Michael Cree (m.cree@biomed.abdn.ac.uk)

*/

int poly2d_numcoeffs(poly2d *p)
{
   return poly_numcoeffsx(p->dimension, p->order);
}

