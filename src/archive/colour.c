/* $Id$ */
/*
   Module: ip/colour.c
   Author: M.J.Cree

   Colour image routines.

   � 1997-2001 Michael Cree

ENTRY POINTS:

   im_palette_compress()
   im_form_rgb()

*/


#include <stdlib.h>

#include <ip/internal.h>
#include <ip/hist.h>

/* #define DEBUG */
#include <debug/debug.h>


static char rcsid[] = "$Id$";


/*

NAME:

   im_palette_compress() \- Compress palette of PALETTE image

PROTOTYPE:

   #include <ip/ip.h>

   int  im_palette_compress( im, hist )
   image *im;
   int *hist;

DESCRIPTION:

   Compresses the palette of the PALETTE type image 'im' into the
   lower pen numbers as much as possible without losing any colours.
   Therefore it searches the image for unused pen numbers and shifts
   any used higher numbered pens down the palette.

   For im_palette_compress() to do its job it needs the image histogram
   which should be passed as 'hist'.  Note that the histogram must be
   a 256 bin histogram. An example of usage is:

\|   hist *hg;
\|   int numcols;
\|
\|   hg = im_hist(im, NULL, 0, 255, 256);
\|   numcols = im_palette_compress(im, hg);
\|   free_hist(hg);

   The returned value ('numcols' in the example above) is the number
   of unique colours present in the image 'im'.  Therefore to detect
   any errors you must have an error handler installed.

ERRORS:

   ERR_BAD_TYPE
   ERR_BAD_PARAMETER - if histogram doesn't have 256 bins.

SEE ALSO:

   im_hist()   im_palette_get()   im_palette_set()

*/

int im_palette_compress(image *im, hist *hg)
{
   int i,j;
   int numcols;
   UBYTE *pix;
   colour pal[256];
   int map[256];

   log_routine("im_palette_compress");

   D(bug("im_palette_compress:\n"));

   numcols = -1;

   if (im->type != IM_PALETTE) {
      log_error(ERR_BAD_TYPE);
      goto exit_ipc;
   }

   if (hg->low != 0 || hg->high != 255 || hg->numbins != 256) {
      log_error(ERR_BAD_PARAMETER);
      goto exit_ipc;
   }


   if (im_palette_get(im, pal) != OK) goto exit_ipc;

   numcols = 0;
   for (i=0; i<256; i++) {
      if (HIST_BIN_COUNT(hg,i) != 0) {
	 D(bug("  shifting pen %d to %d.\n",i, numcols));
	 map[i] = numcols;
	 pal[numcols] = pal[i];
	 numcols++;
      }
   }

   D(bug("   Found %d colours and compressing...\n",numcols));

   if (numcols < 256) {
      for (j=numcols; j<256; j++) {
	 pal[j].r = 0;
	 pal[j].g = 0;
	 pal[j].b = 0;
      }

      for (j=0; j<im->size.y; j++) {
	 pix = IM_ROWADR(im, j, b);
	 for (i=0; i<im->size.x; i++) {
	    *pix = map[*pix];
	    pix++;
	 }
      }
   }
   
   im_palette_set(im, pal);

   D(bug("   ...im_palette_compress/OK\n"));

 exit_ipc:

   pop_routine();
   return numcols;
}



/*

NAME:

   im_form_rgb() \- Combine three BYTE images to make RGB image

PROTOTYPE:

   #include <ip/ip.h>

   image *  im_form_rgb( red, green, blue, flag )
   image *red;
   image *green;
   image *blue;
   int flag;

DESCRIPTION:

   Takes three UBYTE images, namely, 'red', 'green' and 'blue' and combines
   them together to make one new RGB type image which is returned.

   If 'flag' is set to FLG_RGB16 then combines three LONG type images into
   an RGB16 type image.

ERRORS:

   ERR_BAD_TYPE
   ERR_NOT_SAME_SIZE

*/



image * im_form_rgb(image *r, image *g, image *b, int flag)
{
   image *im;
   int i,j;

   log_routine("im_form_rgb");

   if (NOT flag_ok(flag, FLG_RGB8, FLG_RGB16, LASTFLAG))
      return NULL;

   if (flag == FLG_RGB16) {
      if (r->type != IM_LONG || g->type != IM_LONG || b->type != IM_LONG) {
	 log_error(ERR_BAD_TYPE);
	 pop_routine();
	 return NULL;
      }
   }else{
      if (r->type != IM_BYTE || g->type != IM_BYTE || b->type != IM_BYTE) {
	 log_error(ERR_BAD_TYPE);
	 pop_routine();
	 return NULL;
      }
   }

   if (NOT images_same_size(r,g)) 
      return NULL;
   if (NOT images_same_size(r,b)) 
      return NULL;

   if ((im = alloc_image( flag==FLG_RGB16 ? IM_RGB16 : IM_RGB,
			  r->size.x,r->size.y)) == NULL) {
      pop_routine();
      return NULL;
   }

   if (im->type == IM_RGB) {
      for (j=0; j<r->size.y; j++) {
	 UBYTE *rptr, *bptr, *gptr;
	 colour *imptr;

	 rptr = im_rowadr(r,j);
	 gptr = im_rowadr(g,j);
	 bptr = im_rowadr(b,j);
	 imptr = im_rowadr(im,j);

	 for (i=0; i<r->size.x; i++) {
	    imptr->r = *rptr++;
	    imptr->g = *gptr++;
	    imptr->b = *bptr++;
	    imptr++;
	 }
      }
   }else{
      for (j=0; j<r->size.y; j++) {
	 LONG *rptr, *bptr, *gptr;
	 lcolour *imptr;

	 rptr = im_rowadr(r,j);
	 gptr = im_rowadr(g,j);
	 bptr = im_rowadr(b,j);
	 imptr = im_rowadr(im,j);

	 for (i=0; i<r->size.x; i++) {
	    imptr->r = *rptr++;
	    imptr->g = *gptr++;
	    imptr->b = *bptr++;
	    imptr++;
	 }
      }
   }

   pop_routine();
   return im;
}


