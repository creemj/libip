/* $Id$ */
/*
   Header: ip/button.h
   Author: M.J.Cree

   Header file for button.c button management

*/


#ifndef IP_BUTTON_H
#define IP_BUTTON_H

#include <ip/types.h>

/* Structure used for initialising buttons.  Passed to a call
   of alloc_buttons() */

typedef struct {
   int flag;			/* Presently init to zero */
   char *uptext;		/* Button text */
   char *downtext;		/* Presently init to NULL */
} button_decl;

/* Structure returned by alloc_buttons() and used in all other button
   routines.  Treat this as an abstract data type.  You may not look
   at nor modify any field in this structure.  It is private to the
   button routines. */

typedef struct {
   int id;
   int flag;
   int num;
   int width;
   coord origin;
   coord size;
   button_decl *button;
   UBYTE lightpen;
   UBYTE sdwpen;
   UBYTE bgpen;
   UBYTE textpen;
   UBYTE offbgpen;
   UBYTE offtextpen;
} button;


/* Actions to use in button_get() and button_set() */

enum {
   BT_SIZE,
   BT_ORIGIN,
   BT_TEXTPEN,
   BT_LIGHTPEN,
   BT_SDWPEN,
   BT_BGPEN,
   BT_UPSTRING,
   BT_OPTIMUMPAL
};

#define BT_STRING BT_UPSTRING

button *alloc_button(int num, button_decl *buttons, coord origin);
int free_button(button *bt);
int button_display(button *bt);
int button_press(button *bt, event_info *ms);

int button_set(button *bt, int action, ...);
datum button_get(button *bt, int action);

#endif
