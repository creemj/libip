/* $Id$ */
/*
   Module: ip/morphlin.c
   Author: M.J.Cree

   Linear morphological operators.  Morphology with a linear 1-d line
   at various orientations on 2d images.

ENTRY POINTS:

   im_ldilate()
   im_lerode()
   im_lopen()
   im_lclose()

*/

#include <stdlib.h>
#include <math.h>
#include <ip/internal.h>


static char rcsid[] = "$Id$";

/* NB it's important that line remains int and not unsigned int.
   Tests are done to see if it's negative.  */

static void init_line(int *line, int Nx, coord start, coord end);
static int im_lmorph(image *im,int selen,int rotnum,int rotdenom,void (*lmorph)(image *, int *, void *, void *, int , int));


/* #define DEBUG */
#include <debug/debug.h>



/*

NAME:

   im_ldilate() \- Linear dilation in arbitrary direction

PROTOTYPE:

   #include <ip/ip.h>

   int  im_ldilate( im, selen, rotnum, rotdenom )
   image *im;
   int selen;
   int rotnum, rotdenom;

DESCRIPTION:

   Do a dilation along linear lines at the orientation
   2pi*'rotnum'/'rotdenom'.  The angle is specified with 0 degrees
   meaning along the positive x-axis, and angles taken as clockwise
   (remember that the images are in a left-handed coordinate system!)
   from the x-axis.

   The structuring element size is given by 'selen'.  The reference
   point is the centre pixel of the structuring element. For 'selen'
   odd this means the structuring element is symmetrical about 180
   degrees.  For 'selen' even the structuring element points out one
   pixel more from the reference point in the direction given by
   'rotnum' and 'rotdenom'.  Therefore there is no 180 degree
   symmetry for 'selen' even.

LIMITATIONS:

   Only works on BYTE and BINARY images.

ERRORS:

   ERR_OUT_OF_MEM
   ERR_NOT_IMPLEMENTED

SEE ALSO:

   im_lerode()    im_lopen()    im_lclose()
   im_erode()     im_dilate()    im_open()     im_close()

*/

int im_ldilate(image *im, int selen, int rotnum, int rotdenom)
{
   int err;

   log_routine("im_ldilate");
   err = im_lmorph(im, selen, rotnum, rotdenom, ip_dilate1d);
   pop_routine();
   return err;
}




/*

NAME:

   im_lerode() \- Linear erosion in arbitrary direction

PROTOTYPE:

   #include <ip/ip.h>

   int  im_lerode( im, selen, rotnum, rotdenom )
   image *im;
   int selen;
   int rotnum, int rotdenom;

DESCRIPTION:

   Perform an erosion along linear lines at the orientation
   2pi*'rotnum'/'rotdenom'.  The angle is specified with 0 degrees
   meaning along the positive x-axis, and angles taken as clockwise
   (remember that the images are in a left-handed coordinate system!)
   from the x-axis.

   The structuring element size is given by 'selen'.  The reference
   point is the centre pixel of the structuring element. For 'selen'
   odd this means the structuring element is symmetrical about 180
   degrees.  For 'selen' even the structuring element points out one
   pixel more from the reference point in the direction given by
   'rotnum' and 'rotdenom'.  Therefore there is no 180 degree
   symmetry for 'selen' even.

LIMITATIONS:

   Only works on BYTE and BINARY images.

ERRORS:

   ERR_OUT_OF_MEM
   ERR_NOT_IMPLEMENTED

SEE ALSO:

   im_ldilate()   im_lopen()    im_lclose()
   im_erode()     im_dilate()    im_open()     im_close()

*/

int im_lerode(image *im, int selen, int rotnum, int rotdenom)
{
   int err;

   log_routine("im_lerode");
   err = im_lmorph(im, selen, rotnum, rotdenom, ip_erode1d);
   pop_routine();
   return err;
}



/*

NAME:

   im_lopen() \- Linear opening in arbitrary direction

PROTOTYPE:

   #include <ip/ip.h>

   int  im_lopen( im, selen, rotnum, rotdenom )
   image *im;
   int selen;
   int rotnum, rotdenom;

DESCRIPTION:

   Perform an opening along linear lines at the orientation
   2pi*'rotnum'/'rotdenom'.  The angle is specified with 0 degrees
   meaning along the positive x-axis, and angles taken as clockwise
   (remember that the images are in a left-handed coordinate system!)
   from the x-axis.  Note that an opening is always symmetrical about
   180 degrees and that the positioning of the reference point is
   irrelevant.

LIMITATIONS:

   Only works on BYTE and BINARY images.

ERRORS:

   ERR_OUT_OF_MEM
   ERR_NOT_IMPLEMENTED

SEE ALSO:

   im_lerode()    im_ldilate()   im_lclose()
   im_erode()     im_dilate()    im_open()     im_close()

*/

int im_lopen(image *im, int selen, int rotnum, int rotdenom)
{
   if (im_lerode(im,selen,rotnum,rotdenom) != OK)
      return ip_error;
   return im_ldilate(im,selen,2*rotnum+rotdenom,2*rotdenom);
}
   



/*

NAME:

   im_lclose() \- Linear closing in arbitrary direction

PROTOTYPE:

   #include <ip/ip.h>

   int  im_lclose( im, selen, rotnum, rotdenom )
   image *im;
   int selen;
   int rotnum, rotdenom;

DESCRIPTION:

   Perform a closing along linear lines at the orientation
   2pi*rotnum/rotdenom.  The angle is specified with 0 degrees meaning
   along the positive x-axis, and angles taken as clockwise (remember
   that the images are in a left-handed coordinate system!) from the
   x-axis.  Note that a closing is always symmetrical about 180
   degrees and that the positioning of the reference point is
   irrelevant.

LIMITATIONS:

   Only works on BYTE and BINARY images.

ERRORS:

   ERR_OUT_OF_MEM
   ERR_NOT_IMPLEMENTED

SEE ALSO:

   im_lerode()    im_ldilate()   im_lopen()
   im_erode()     im_dilate()    im_open()     im_close()

*/

int im_lclose(image *im, int selen, int rotnum, int rotdenom)
{
   if (im_ldilate(im,selen,rotnum,rotdenom) != OK)
      return ip_error;
   return im_lerode(im,selen,2*rotnum+rotdenom,2*rotdenom);
}
   



/* The main work is done by this routine. */

static int im_lmorph(image *im, int selen, int rotnum, int rotdenom, 
		     void (*lmorph)(image *, int *, void *, void *,
				   int , int))
{
   int orgselen;
   int i,j;
   int tlen;
   long size;
   int *line;
   void *g,*h;
   coord pstart,pend;
   double theta, tant;
   int step, stop, str, inc, end;
   int dobyy, below, right, loop;
   
   D(bug("lmorph: selen=%d, rotnum=%d, rotdenom=%d\n",selen,rotnum,rotdenom));

   /* Alloc line buffers for doing linear morphology */

   orgselen = selen;
   if (alloc_lgh(im,(void *)&line,&g,&h,orgselen) != OK) {
      return ip_error;
   }

   /* Do horizontal orientations specially */

   if (rotnum == 0 || rotnum*2 == rotdenom) {
      for (i=0; i<im->size.x; i++)
	 line[i] = (rotnum == 0) ? i : im->size.x - i - 1;
      for (j=0; j<im->size.y; j++) {
	 lmorph(im,line,g,h,im->size.x,selen);
	 for (i=0; i<im->size.x; i++)
	    line[i] += im->size.x;
      }
      free_lgh(im,line,g,h,orgselen);
      return OK;
   }

   /* Do vertical orientations specially */

   if (rotnum*4 == rotdenom  || rotnum*4 == 3*rotdenom) {
      for (j=0; j<im->size.y; j++)
	 line[j] = (rotnum == rotdenom/4) ? j * im->size.x : (im->size.y-j-1)*im->size.x;
      for (i=0; i<im->size.x; i++) {
	 lmorph(im,line,g,h,im->size.y,selen);
	 for (j=0; j<im->size.y; j++)
	    line[j]++;
      }
      free_lgh(im,line,g,h,orgselen);
      return OK;
   }

   /* Do diagonal (45 deg, etc.) orientations specially */

   size = im->size.x * im->size.y;

   if ((rotnum*8 == rotdenom) || (rotnum*8 == 3*rotdenom)) {
      if (rotnum*8 == rotdenom) {
	 pstart.x = 0;
	 pstart.y = -(im->size.x-1);
	 pend.x = im->size.x-1;
	 pend.y = 0;
      }else{
	 pstart.x = im->size.x-1;
	 pstart.y = -(im->size.x-1);
	 pend.x = pend.y = 0;
      }
      init_line(line, im->size.x, pstart, pend);
      selen = ROUND(selen/M_SQRT2);
      str = end = im->size.x - 1;
      for (j=-(im->size.x-1); j<im->size.y; j++) {
	 for ( ; str > 0 && line[str-1] >= 0; str--)
	    ;
	 for ( ; line[end] >= size; end--)
	    ;
	 lmorph(im,&line[str],g,h,end-str+1,selen);
	 for (i=0; i<im->size.x; i++)
	    line[i] += im->size.x;
      }
      free_lgh(im,line,g,h,orgselen);
      return OK;
   }

   if ((rotnum*8 == 5*rotdenom) || (rotnum*8 == 7*rotdenom)) {
      if (rotnum*8 == 5*rotdenom) {
	 pstart.x = im->size.x-1;
	 pstart.y = 0;
	 pend.x = 0;
	 pend.y = -(im->size.x-1);
      }else{
	 pstart.x = pstart.y = 0;
	 pend.x = im->size.x-1;
	 pend.y = -(im->size.x-1);
      }
      init_line(line, im->size.x, pstart, pend);
      selen = ROUND(selen/M_SQRT2);
      str = end = 0;
      for (j=-(im->size.x-1); j<im->size.y; j++) {
	 for ( ; end < im->size.x-1 && line[end+1] >= 0; end++)
	    ;
	 for ( ; line[str] >= size; str++)
	    ;
	 lmorph(im,&line[str],g,h,end-str+1,selen);
	 for (i=0; i<im->size.x; i++)
	    line[i] += im->size.x;
      }
      free_lgh(im,line,g,h,orgselen);
      return OK;
   }

   /* Do arbitrary orientations */

   D(bug("Arbitrary orientation.\n"));

   theta = 2*M_PI*(double)rotnum/rotdenom;
   dobyy = fabs(tan(theta)) > 1.0;
   below = theta < M_PI;
   right = (theta < M_PI/2) || (theta > 3*M_PI/2);

   if (dobyy) {
      if ((right && !below) || (!right && below)) {
	 loop = below ? 3 : 5;
      }else{
	 loop = below ? 2 : 4;	
      }
      tlen = im->size.y;
      stop = im->size.x;
      step = 1;
      selen = ROUND(selen * fabs(cos(theta-M_PI/2)));
      tant = fabs(tan(theta-M_PI/2));
   }else{
      loop = below ? 1 : 6;
      step = tlen = im->size.x;
      stop = im->size.y;
      selen = ROUND(selen * fabs(cos(theta)));
      tant = fabs(tan(theta));
   }

   if (theta > M_PI) theta -= M_PI;
   if (theta < M_PI/4) {
      pstart.x = 0;
      pstart.y = -ROUND((tlen-1)*tant);
      pend.x = tlen-1;
      pend.y = 0;
   }else if (theta < M_PI/2) {
      pstart.x = -ROUND((tlen-1)*tant);
      pstart.y = 0;
      pend.x = 0;
      pend.y = tlen-1;
   }else if (theta < 3*M_PI/4) {
      pstart.x = pstart.y = 0;
      pend.x = -ROUND((tlen-1)*tant);
      pend.y = tlen-1;
   }else{
      pstart.x = tlen-1;
      pstart.y = -ROUND((tlen-1)*tant);
      pend.x = pend.y = 0;
   }
   if (!below) {
      coord tmp;

      tmp = pstart;
      pstart = pend;
      pend = tmp;
   }

   if ((dobyy && right) || (!dobyy && below)) {
      str = end = tlen-1;
   }else{
      str = end = 0;
   }
   inc = -ROUND((tlen-1)*tant);

   D(bug("dobyy=%c, below=%c, right=%c, loop=%d,    tlen=%d\n",
	 "FT"[dobyy],"FT"[below],"FT"[right],loop,tlen));
   D(bug("theta=%g,   tan(theta)=%g,  step=%d,  stop=%d\n",
	 theta,tant,step,stop));
   D(bug("start=(%d,%d)    end=(%d,%d)    str=%d   end=%d    inc=%d\n",
	 pstart.x,pstart.y,pend.x,pend.y,str,end,inc));

   init_line(line, im->size.x, pstart, pend);

#ifdef DEBUG
   {
      int *lptr;
      int i,x,y;
      for (i=0, lptr = line; ; i++,lptr++) {
	 y = *lptr / im->size.x;
	 x = *lptr - (y*im->size.x);
	 D(bug(" %3d,%-3d",x,y));
	 if (i%8 == 7) D(bug("\n"));
	 if ((pend.x +pend.y*im->size.x) == *lptr) break;
	 if (i > MAX(im->size.x,im->size.y)) {
	    D(bug("RUNAWAY LINE\n")); 
	    break;
	 }
      }
      if (NOT (i%8 == 7)) D(bug("\n"));
   }
#endif

   while (inc < stop) {
      switch (loop) {
       case 1:
	 for ( ; str > 0 && line[str-1] >= 0; str--)
	    ;
	 for ( ; line[end] >= size; end--)
	    ;
	 break;
       case 2:
	 for ( ; str > 0 && line[str-1] >= (str-1)*im->size.x; str--)
	    ;
	 for ( ; line[end] >= (end+1)*im->size.x; end--)
	    ;
	 break;
       case 3:
	 for ( ; end < tlen-1 && line[end+1] >= (end+1)*im->size.x; end++)
	    ;
	 for ( ; line[str] >= (str+1)*im->size.x; str++)
	    ;
	 break;
       case 4:
	 for ( ; end < tlen-1 && line[end+1] >= (tlen-end-2)*im->size.x; end++)
	    ;
	 for ( ; line[str] >= (tlen-str)*im->size.x; str++)
	    ;
	 break;
       case 5:
	 for ( ; str > 0 && line[str-1] >= (tlen-str)*im->size.x; str--)
	    ;
	 for ( ; line[end] >= (tlen-end)*im->size.x; end--)
	    ;
	 break;
       case 6:
	 for ( ; end < tlen-1 && line[end+1] >= 0; end++)
	    ;
	 for ( ; line[str] >= size; str++)
	    ;
	 break;
      }
      D(bug("inc: %d     str=%d  end=%d\n",inc,str,end));
      lmorph(im,&line[str],g,h,end-str+1,selen);
      inc++;
      for (i=0; i<tlen; i++)
	 line[i] += step;
   }

   free_lgh(im,line,g,h,orgselen);
   return ip_error;
}



/* For calculating lines at arbitrary angles */

static void init_line(int *line, int Nx, coord start, coord end)
{
   int error, reverse;
   coord pt, diff;
   coord step1, step2;
   coord tmp;
   int *lptr;
   int llen;

   reverse = FALSE;
   if (start.x > end.x) {
      tmp = start;
      start = end;
      end = tmp;
      reverse = TRUE;
   }
   lptr = line;
   pt = start;
   *lptr++ = pt.x + pt.y * Nx;
   diff.y = end.y - start.y;
   diff.x = end.x - start.x;
   if (abs(diff.y) < diff.x) {
      step1.x = 1;
      step1.y = 0;
      step2.x = step2.y = 1;
      if (diff.y < 0) {
	 diff.y = -diff.y;
	 step1.y = -step1.y;
	 step2.y = -step2.y;
      }
   }else{
      step1.x = 0;
      step1.y = 1;
      step2.x = step2.y = 1;
      error = diff.x;
      diff.x = diff.y;
      diff.y = error;
      if (diff.x < 0) {
	 diff.x = -diff.x;
	 step1.y = -step1.y;
	 step2.y = -step2.y;
      }
   }
   error = 0;
   llen = 1;			/* Already one value in line buffer */
   do {
      if (2*(error + diff.y) < diff.x) {
	 pt.x += step1.x;
	 pt.y += step1.y;
	 *lptr++ = pt.x + pt.y * Nx;
	 error += diff.y;
      }else{
	 pt.x += step2.x;
	 pt.y += step2.y;
	 *lptr++ = pt.x + pt.y * Nx;
	 error += diff.y - diff.x;
      }
      llen++;
   } while (pt.x != end.x || pt.y != end.y);
   if (reverse) {
      lptr = line + llen;
      llen /= 2;
      for (pt.x = 0; pt.x < llen; pt.x++) {
	 error = *line;
	 *line++ = *--lptr;
	 *lptr = error;
      }
   }
}

