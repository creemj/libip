/* $Id$ */
/*
   Header: ip/xtiffiop.h
   Author: M.J.Cree

   Private Implementation of the Extended TIFF library interface.
   This implements the tag TIFFTAG_RETINALINFO.

   Uses private LIBTIFF interface.

   This is based upon a file xtiffiop.h in the libtiff distribution
   which was originally written by Niles D. Ritter
*/

#ifndef IP_XTIFFIOP_H
#define IP_XTIFFIOP_H

#include <tiff/tiffiop.h>
#include <ip/xtiffio.h>

/**********************************************************************
 *               User Configuration
 **********************************************************************/

/* Number of extended tags  */
#define NUM_XFIELD 1
#define XFIELD_BASE (FIELD_LAST-NUM_XFIELD)

/* Extended Tag Fields */
#define	FIELD_OPHTHALMICINFO     (XFIELD_BASE+0)

/* Private directory tag structure */
struct XTIFFDirectory {
   int xd_size;
   char *xd_info;
};

typedef struct XTIFFDirectory XTIFFDirectory;


/**********************************************************************
 *    Nothing below this line should need to be changed by the user.
 **********************************************************************/

struct xtiff {
	TIFF 		*xtif_tif;	/* parent TIFF pointer */
	uint32		xtif_flags;
#define       XTIFFP_PRINT   0x00000001
	XTIFFDirectory	xtif_dir;	/* internal rep of current directory */
	TIFFVSetMethod	xtif_vsetfield;	/* inherited tag set routine */
	TIFFVGetMethod	xtif_vgetfield;	/* inherited tag get routine */
	TIFFPrintMethod	xtif_printdir;  /* inherited dir print method */
};
typedef struct xtiff xtiff;


#define PARENT(xt,pmember) ((xt)->xtif_ ## pmember) 
#define TIFFMEMBER(tf,pmember) ((tf)->tif_ ## pmember) 
#define XTIFFDIR(tif) ((xtiff *)TIFFMEMBER(tif,clientdir))
	
/* Extended TIFF flags */
#define XTIFF_INITIALIZED 0x80000000
	
#endif
