/* $Id$ */
/*

   Module: ip/rgbmedian.c
   Author: M.J.Cree

   Vector median and adaptive vector colour filtering of RGB images.

   � 2001-2003 Michael Cree

ENTRY POINTS:

   im_rbgmedian()
   im_rgbannf()

*/

#include <stdlib.h>
#include <math.h>

#include <ip/internal.h>

#include <debug/debug.h>

static char rcsid[] = "$Id$";

int i_pt_export;
int j_pt_export;

#ifdef DEBUG
int i_pt_stop;
int j_pt_stop;
#endif

double parm_r_import_1;
double parm_beta_import_1;
double parm_r_import_a;
double parm_beta_import_a;

/*

NAME:

   im_rgbmedian() \- Median filter an RGB image

PROTOTYPE:

   #include <ip/ip.h>

   int  im_rgbmedian( im, width, flag )
   image *im;
   int width;
   int flag;

DESCRIPTION:

   Median filter an RGB image with 'width'*'width' kernel.  'Width' must
   be odd.  By default, the border region where the mask goes off the
   edge of the image is left unprocessed.  This can be modified with
   the use of 'flag', which may be:

\&      FLG_ZERO   Zero border region of image where mask doesn''t cover
                 properly.

\&      FLG_EXTEND Extend the median function to the edge of the image
                 using as many pixels as are available to calculate
		 the median. (Not Implemented Yet)

   This routine applies a vector median filter.  The distance measure may be
   one of:

\&      DIST_L1       L1 magnitude
\&      DIST_L2       L2 magnitude
\&      DIST_L1L2     L1 magnitude * L2 magnitude
\&      DIST_ANGLE    Angle
\&      DIST_L1ANGLE  L1 * Angle
\&      DIST_L2ANGLE  L2 * Angle
\&      DIST_SIMUL    Simularity Measure
\&      DIST_MINE1    My invention 1
\&      DIST_MINE2    My invention 2

   Bitwise OR the distance measure into the flag.

LIMITATIONS:

\-   Only RGB images supported
\-   The kernel mask size must be odd.

ERRORS:

   ERR_UNSUPPORTED_TYPE
   ERR_BAD_PARAMETER
   ERR_BAD_FLAG
   ERR_OUT_OF_MEM

*/

int im_rgbmedian( image *im, int width, int flag )
{
   int hwidth;			/* half-width of widthxwidth median filter */
   int i,j;			/* loop variables                          */

   double  *arr;		/* Array for calculating distances */
   int arrsize;			/* Size of array arr */

   image *tmp;			/* Tempory image for storing result */
   image *mag;			/* Magnitude image = |im| */

   int flag_border;		/* Border type part of flag */
   int flag_dist;		/* Distance operator part of flag */


   log_routine("im_rgbmedian");

   if (im->type!=IM_RGB) {
      log_error(ERR_UNSUPPORTED_TYPE);
      goto exit;
   }

   flag_border = flag & FLGMSK_BORDER;
   flag_dist = flag & FLGMSK_DIST;

   if (NOT flag_ok(flag_border,FLG_ZERO,FLG_EXTEND,LASTFLAG))
      return ip_error;

   if (NOT flag_ok(flag_dist, DIST_L1, DIST_L2, DIST_ANGLE,
		   DIST_L1L2, DIST_L1ANGLE, DIST_L2ANGLE, 
		   DIST_SIMUL, DIST_MINE1, DIST_MINE2, LASTFLAG))
      return ip_error;

   if (flag_dist == 0) flag_dist = DIST_L1;

   if (NOT ip_parm_greatereq("width", width, 3.0))
      return ip_error;
      
   if (NOT ip_parm_odd("width", width))
      return ip_error;

   if ((tmp = alloc_image(im->type,im->Nx,im->Ny)) == NULL)
      goto exit;

   hwidth = width / 2;		/* half-width of filter */
   arrsize = width*width;

   /* initialise array */

   if ((arr = ip_malloc(sizeof(double) * arrsize)) == NULL) {
      free_image(tmp);
      return ip_error;
   }

   /* Initialise an image with the vector magnitude of each pixel. */

   if ((mag = im_convert(im, IM_DOUBLE, FLG_MAG)) == NULL) {
      ip_free(arr);
      free_image(tmp);
      return ip_error;
   }


   /* for each pixel in image find median over width*width mask */

   for( j = hwidth; j < (im->Ny - hwidth); j++ ) {
      colour *tptr;		/* Result image */
      j_pt_export = j;

      tptr = (colour *)im_rowadr(tmp, j) + hwidth;

      for( i = hwidth; i < (im->Nx - hwidth); i++ ) {
	 int k,l;		/* Index into mask wrt point (i,j) */
	 double *aptr;		/* Pointer into distance array arr */
	 int mpos;		/* Position of min in distance arr */
	 double min,max;
	 i_pt_export = i;	/* For debug purposes */

	 /* Load up array with distance measure of pixels
	    in neighbourhood of (i,j) */

	 aptr = arr;		/* Zero distance array arr */
	 for (l=0; l<arrsize; l++) *aptr++ = 0;

	 aptr = arr;
	 for (l=-hwidth; l<=hwidth; l++) {
	    colour *sptr1;	/* Source pixel in image im */
	    double *mptr1;	/* Magnitude of pixel in image im */

	    sptr1 = IM_ROWADR(im, j+l, r) + (i - hwidth);
	    mptr1 = IM_ROWADR(mag, j+l, d) + (i - hwidth);

	    for (k=-hwidth; k<=hwidth; k++) {
	       int kk,ll;
	       double *aptr2;	/* Second pointer into arr */

	       aptr2 = aptr+1;
	       if (k < hwidth) {
		  ll = l;
		  kk = k+1;
	       }else{
		  ll = l+1;
		  kk=-hwidth;
	       }

	       for (; ll<=hwidth; ll++) {
		  colour *sptr2; /* Source pixel in image im */
		  double *mptr2; /* Magnitude of pixel in image im */

		  sptr2 = IM_ROWADR(im, j+ll, r) + (i + kk);
		  mptr2 = IM_ROWADR(mag, j+ll, d) + (i + kk);
		  for (; kk<=hwidth; kk++) {
		     double dist, dist2;

		     switch (flag_dist) {
		      case DIST_ANGLE:
			/* Use:
			   Distance = Sum acos( x_ij . x_kl / (|x_ij|*|x_kl|))
			   
			   where x_ij is colour value of (i,j) pixel,
			   |x_ij| is magnitude thereof, 
			   '.' is vector dot-product
			   and sum is over all kl != ij.
			*/

			if (*mptr1 == 0 || *mptr2==0) {
			   /* Problem when either pixel is completely black.
			      This amounts to a mathematical singularity
			      and the angular distance is undefined. */
			   dist = (*mptr1 == *mptr2 ? 0.0 : 1.0);
			}else{
			   dist =  acos(((double)sptr1->r*sptr2->r 
				+ (double)sptr1->g*sptr2->g 
				+ (double)sptr1->b*sptr2->b) 
				     / (*mptr1 * *mptr2));
			   /* If pixels same colour sometimes can get NaN
			      back from acos() calculation, presumably because
			      roundoff error has led to the argument of
			      the acos() to be subtlely bigger than 1.0 */
			   if (isnan(dist)) dist = 0.0; 
			}
			break;
		      case DIST_L1:
			/* Distance = Sum L1 norm. */
			dist = (fabs(sptr1->r - sptr2->r)
				+ fabs(sptr1->g - sptr2->g)
				+ fabs(sptr1->b - sptr2->b));
			break;
		      case DIST_L2:
			{
			   /* Distance = Sum L2 (euclidean) norm. */
			   double tmp;
			   tmp = sptr1->r - sptr2->r;
			   dist = tmp*tmp;
			   tmp = sptr1->g - sptr2->g;
			   dist += tmp*tmp;
			   tmp = sptr1->b - sptr2->b;
			   dist += tmp*tmp;
			}
			dist = sqrt(dist);
			break;
		      case DIST_L1L2:
			{
			   /* Distance = Sum L1 * L2 */
			   double acc1,acc2;
			   dist = fabs(sptr1->r - sptr2->r);
			   acc2 = dist * dist;
			   acc1 = fabs(sptr1->g - sptr2->g);
			   acc2 += acc1*acc1;
			   dist += acc1;
			   acc1 = fabs(sptr1->b - sptr2->b);
			   acc2 += acc1*acc1;
			   dist += acc1;
			   dist *= sqrt(acc2);
			}
			break;
		      case DIST_L1ANGLE:
			if (*mptr1 == 0 || *mptr2==0) {
			   /* Problem when either pixel is completely black.
			      This amounts to a mathematical singularity
			      and the angular distance is undefined. */
			   dist = (*mptr1 == *mptr2 ? 0.0 : 1.0);
			}else{
			   dist =  acos(((double)sptr1->r*sptr2->r 
				+ (double)sptr1->g*sptr2->g 
				+ (double)sptr1->b*sptr2->b) 
				     / (*mptr1 * *mptr2));
			   /* If pixels same colour sometimes can get NaN
			      back from acos() calculation, presumably because
			      roundoff error has led to the argument of
			      the acos() to be subtlely bigger than 1.0 */
			   if (isnan(dist)) dist = 0.0; 
			}
			dist *= (fabs(sptr1->r - sptr2->r)
				+ fabs(sptr1->g - sptr2->g)
				+ fabs(sptr1->b - sptr2->b));
			break;
		      case DIST_L2ANGLE:
			if (*mptr1 == 0 || *mptr2==0) {
			   /* Problem when either pixel is completely black.
			      This amounts to a mathematical singularity
			      and the angular distance is undefined. */
			   dist *= (*mptr1 == *mptr2 ? 0.0 : 1.0);
			}else{
			   dist *=  acos(((double)sptr1->r*sptr2->r 
				+ (double)sptr1->g*sptr2->g 
				+ (double)sptr1->b*sptr2->b) 
				     / (*mptr1 * *mptr2));
			   if (isnan(dist)) dist = 0.0; 
			}
			if (dist > 0.0) {
			   double tmp1, tmp2;
			   tmp1 = sptr1->r - sptr2->r;
			   tmp2 = tmp1*tmp1;
			   tmp1 = sptr1->g - sptr2->g;
			   tmp2 += tmp1*tmp1;
			   tmp1 = sptr1->b - sptr2->b;
			   tmp2 += tmp1*tmp1;
			   dist *= sqrt(tmp2);
			}
			break;
		      case DIST_SIMUL:
			if (*mptr1 == 0 || *mptr2==0) {
			   /* Problem when either pixel is completely black.
			      This amounts to a mathematical singularity
			      and the angular distance is undefined. */
			   dist = (*mptr1 == *mptr2 ? 0.0 : 1.0);
			}else{
			   dist = (1.0 - (((double)sptr1->r*sptr2->r 
					   + (double)sptr1->g*sptr2->g 
					   + (double)sptr1->b*sptr2->b) 
					  / (*mptr1**mptr2))
				   * (1 - fabs(*mptr1-*mptr2)
				      / (*mptr1>*mptr2 ? *mptr1 : *mptr2)));
			}
			break;
		      case DIST_MINE1:
			if (*mptr1 == 0 || *mptr2==0) {
			   /* Problem when either pixel is completely black.
			      This amounts to a mathematical singularity
			      and the angular distance is undefined. */
			   dist = 0.0;
			}else{
			   dist =  acos(((double)sptr1->r*sptr2->r 
				+ (double)sptr1->g*sptr2->g 
				+ (double)sptr1->b*sptr2->b) 
				     / (*mptr1 * *mptr2));
			   /* If pixels same colour sometimes can get NaN
			      back from acos() calculation, presumably because
			      roundoff error has led to the argument of
			      the acos() to be subtlely bigger than 1.0 */
			   if (isnan(dist)) dist = 0.0; 
			   dist *= (*mptr1>*mptr2 ? *mptr1 : *mptr2);
			}
			dist += fabs(*mptr1-*mptr2);
			break;
		      case DIST_MINE2:
			if (*mptr1 == 0 || *mptr2==0) {
			   /* Problem when either pixel is completely black.
			      This amounts to a mathematical singularity
			      and the angular distance is undefined. */
			   dist = 0.0;
			}else{
			   dist =  acos(((double)sptr1->r*sptr2->r 
				+ (double)sptr1->g*sptr2->g 
				+ (double)sptr1->b*sptr2->b) 
				     / (*mptr1 * *mptr2));
			   /* If pixels same colour sometimes can get NaN
			      back from acos() calculation, presumably because
			      roundoff error has led to the argument of
			      the acos() to be subtlely bigger than 1.0 */
			   if (isnan(dist)) dist = 0.0; 
			   dist *= pow((*mptr1>*mptr2 ? *mptr1 : *mptr2),
				       parm_r_import_1)
					   / 441.68;
			}
			dist += parm_beta_import_1 * fabs(*mptr1-*mptr2) / 441.68;
			break;
		     }
		     *aptr += dist;
		     *aptr2 += dist;
		     aptr2++;
		     sptr2++; mptr2++;
		  }
		  kk = -hwidth;
	       }
	       aptr++; sptr1++; mptr1++;
	    }
	 }      

#ifdef DEBUG
	 if ((ip_verbose & 0x08) && i==i_pt_stop && j==j_pt_stop) {
	    D(bug("Distances: [ref pt is (%d,%d)]\n",i_pt_stop,j_pt_stop));
	    aptr = arr;
	    for (l=-hwidth; l<=hwidth; l++) {
	       D(bug(" %2d",l));
	       for (k=-hwidth; k<=hwidth; k++) {
		  D(bug(" %8.5f",*aptr++));
	       }
	       D(bug("\n"));
	    }
	 }

#endif

	 /* Identify max and min distance in array */

	 aptr = arr;
	 max = 0.0;
	 min = 1e30; 
	 for (k=0; k<arrsize; k++) {
	    if (*aptr > max) max = *aptr;
	    if (*aptr < min) {
	       min = *aptr; mpos = k;
	    }
	    aptr++;
	 }

#ifdef DEBUG
	 if ((ip_verbose & 0x08) && i==i_pt_stop && j==j_pt_stop) {
	    D(bug("Max distance value = %g\n",max));
	    D(bug("Min distance value = %g at k=%d [%d,%d]\n",
		  min,mpos,i-hwidth+mpos%width,j-hwidth+mpos/width));
	 }
#endif

	 {
	    colour *sptr;	/* Source pixel in image im */

	    l = j - hwidth + mpos / width;
	    k = i - hwidth + mpos % width;
	    sptr = IM_ROWADR(im, l, r);
	    sptr += k;
	    *tptr = *sptr;
	 }
#ifdef DEBUG
	 if ((ip_verbose & 0x08) && i==i_pt_stop && j==j_pt_stop) {
	    D(bug("Final result: %2x%2x%2x  (%d,%d,%d)\n",
		  tptr->r, tptr->g, tptr->b, tptr->r, tptr->g, tptr->b));
	 }
#endif
	 tptr++;
      }
   }

   if (flag_border == FLG_ZERO) {
      /* Clear edges of tmp */
      im_clear_border(tmp,hwidth);
   }

   if (flag_border == FLG_EXTEND) {
      log_error(ERR_NOT_IMPLEMENTED);
   }

   if (flag_border == FLG_ZERO || flag_border == FLG_EXTEND) {
      memcpy(im->image.v, tmp->image.v, im->Nx*im->Ny*ip_typesize[im->type]);
   }else{
      coord org,size;

      org.x = org.y = hwidth;
      size.x = im->Nx - 2*hwidth;
      size.y = im->Ny - 2*hwidth;

      im_insert(im, org, tmp, org, size);
   }
   free_image(mag);
   free_image(tmp);
   ip_free(arr);

 exit:
   pop_routine();
   return ip_error;
}



/*

NAME:

   im_rgbannf() \- Apply ANNF/FVF smoothing filter to RGB image

PROTOTYPE:

   #include <ip/ip.h>

   int  im_rgbannf( im, width, alpha, r, flag )
   image *im;
   int width;
   double alpha, r;
   int flag;

DESCRIPTION:

   Filter an RGB image with 'width'*'width' kernel using an ANNF based
   filter.  'Width' must be odd.  By default, the border region where the
   mask goes off the edge of the image is left unprocessed.  This can be
   modified with the use of 'flag', which may be:

\&      FLG_ZERO   Zero border region of image where mask doesn''t cover
                 properly.

\&      FLG_EXTEND Extend the median function to the edge of the image
                 using as many pixels as are available to calculate
		 the median.

   This routine applies an adaptive weights (ANNF) or a fuzzy (FVF) filter.
   The distance measure may be one of:

\&      DIST_L1       L1 magnitude
\&      DIST_L2       L2 magnitude
\&      DIST_L1L2     L1 magnitude * L2 magnitude
\&      DIST_ANGLE    Angle
\&      DIST_L1ANGLE  L1 * Angle
\&      DIST_L2ANGLE  L2 * Angle
\&      DIST_SIMUL    Simularity Measure
\&      DIST_MINE1    My invention 1
\&      DIST_MINE2    My invention 2

   OR the distance measure into the flag.

   The filter types are (and whether the parameters are used):

\&      filter flag         parameters          Usual distance
\&      FILT_ANNF                                  Any
\&      FILT_ANNGF            alpha		   Any
\&      FILT_ANNGF2				   Any
\&      FILT_FVF_W1           alpha,r              Angle
\&      FILT_FVF_W2           alpha,r              L1 or L2
\&      FILT_FVF_W3           r                    L1 or L2

   OR in the optional flag

\&      FLG_CENTREBIAS1
\&      FLG_CENTREBIAS2

   for the centre-biasing to be applied in between calculating the
   distance and applying the filter.

LIMITATIONS:

\-   Only RGB images supported
\-   The kernel mask size must be odd.

ERRORS:

   ERR_UNSUPPORTED_TYPE
   ERR_BAD_PARAMETER
   ERR_BAD_FLAG
   ERR_OUT_OF_MEM

*/

int im_rgbannf( image *im, int width, double alpha, double r, int flag )
{
   int hwidth;			/* half-width of widthxwidth median filter */
   int i,j;			/* loop variables                          */

   double  *arr;		/* Array for calculating distances */
   int arrsize;			/* Size of array arr */

   image *tmp;			/* Tempory image for storing result */
   image *mag;			/* Magnitude image = |im| */

   int flag_border;		/* Border type part of flag */
   int flag_dist;		/* Distance operator part of flag */
   int flag_filter;		/* Filter type to use */
   int flag_cb;			/* Centre biasing - if any */

   log_routine("im_rgbannf");

   if (im->type!=IM_RGB) {
      log_error(ERR_UNSUPPORTED_TYPE);
      goto exit;
   }

   flag_border = flag & FLGMSK_BORDER;
   flag_filter = flag & FLGMSK_FILTER;
   flag_dist = flag & FLGMSK_DIST;
   flag_cb = flag & FLGMSK_CBIAS;

   if (NOT flag_ok(flag_border,FLG_ZERO,FLG_EXTEND,LASTFLAG))
      return ip_error;

   if (NOT flag_ok(flag_dist, DIST_L1, DIST_L2, DIST_ANGLE,
		   DIST_L1L2, DIST_L1ANGLE, DIST_L2ANGLE, 
		   DIST_SIMUL, DIST_MINE1, DIST_MINE2, LASTFLAG))
      return ip_error;

   if (NOT flag_ok(flag_filter, FILT_ANNF, FILT_ANNGF, FILT_ANNGF2,
		   FILT_FVF_W1, FILT_FVF_W2, FILT_FVF_W3, LASTFLAG))
      return ip_error;

   if (NOT flag_ok(flag_cb, FLG_CBIAS_1, FLG_CBIAS_2, FLG_CBIAS_3, LASTFLAG))
      return ip_error;

   if (flag_cb) {
      fprintf(stderr,"Doing centre bias...\n");
   }

   if (flag_dist == 0) flag_dist = DIST_L1;
   if (flag_filter == 0) flag_filter = FILT_ANNF;

   if (NOT ip_parm_greatereq("width", width, 3.0))
      return ip_error;
      
   if (NOT ip_parm_odd("width", width))
      return ip_error;

   if ((tmp = alloc_image(im->type,im->Nx,im->Ny)) == NULL)
      goto exit;

   hwidth = width / 2;		/* half-width of filter */
   arrsize = width*width;

   /* initialise array */

   if ((arr = ip_malloc(sizeof(double) * arrsize)) == NULL) {
      free_image(tmp);
      return ip_error;
   }

   /* Initialise an image with the vector magnitude of each pixel. */

   if ((mag = im_convert(im, IM_DOUBLE, FLG_MAG)) == NULL) {
      ip_free(arr);
      free_image(tmp);
      return ip_error;
   }


   /* for each pixel in image find median over width*width mask */

   for( j = hwidth; j < (im->Ny - hwidth); j++ ) {
      colour *tptr;		/* Result image */
      j_pt_export = j;

      tptr = (colour *)im_rowadr(tmp, j) + hwidth;

      for( i = hwidth; i < (im->Nx - hwidth); i++ ) {
	 colour *sptr;		/* Source image */
	 int k,l;		/* Index into mask wrt point (i,j) */
	 double *aptr;		/* Pointer into distance array arr */
	 int mpos;		/* Position of min in distance arr */
	 double min,max;
	 i_pt_export = i;	/* For debug purposes */

	 /* Load up array with distance measure of pixels
	    in neighbourhood of (i,j) */

	 aptr = arr;		/* Zero distance array arr */
	 for (l=0; l<arrsize; l++) *aptr++ = 0;

	 aptr = arr;
	 for (l=-hwidth; l<=hwidth; l++) {
	    colour *sptr1;	/* Source pixel in image im */
	    double *mptr1;	/* Magnitude of pixel in image im */

	    sptr1 = IM_ROWADR(im, j+l, r) + (i - hwidth);
	    mptr1 = IM_ROWADR(mag, j+l, d) + (i - hwidth);

	    for (k=-hwidth; k<=hwidth; k++) {
	       int kk,ll;
	       double *aptr2;	/* Second pointer into arr */

	       aptr2 = aptr+1;
	       if (k < hwidth) {
		  ll = l;
		  kk = k+1;
	       }else{
		  ll = l+1;
		  kk=-hwidth;
	       }

	       for (; ll<=hwidth; ll++) {
		  colour *sptr2; /* Source pixel in image im */
		  double *mptr2; /* Magnitude of pixel in image im */

		  sptr2 = IM_ROWADR(im, j+ll, r) + (i + kk);
		  mptr2 = IM_ROWADR(mag, j+ll, d) + (i + kk);
		  for (; kk<=hwidth; kk++) {
		     double dist, dist2;

		     switch (flag_dist) {
		      case DIST_ANGLE:
			/* Use:
			   Distance = Sum acos( x_ij . x_kl / (|x_ij|*|x_kl|))
			   
			   where x_ij is colour value of (i,j) pixel,
			   |x_ij| is magnitude thereof, 
			   '.' is vector dot-product
			   and sum is over all kl != ij.
			*/

			if (*mptr1 == 0 || *mptr2==0) {
			   /* Problem when either pixel is completely black.
			      This amounts to a mathematical singularity
			      and the angular distance is undefined. */
			   dist = (*mptr1 == *mptr2 ? 0.0 : 1.0);
			}else{
			   dist =  acos(((double)sptr1->r*sptr2->r 
				+ (double)sptr1->g*sptr2->g 
				+ (double)sptr1->b*sptr2->b) 
				     / (*mptr1 * *mptr2));
			   /* If pixels same colour sometimes can get NaN
			      back from acos() calculation, presumably because
			      roundoff error has led to the argument of
			      the acos() to be subtlely bigger than 1.0 */
			   if (isnan(dist)) dist = 0.0; 
			} 
			break; 
		      case DIST_L1:
			   /* Distance = Sum L1 norm. */
			dist = (fabs(sptr1->r - sptr2->r)
				+ fabs(sptr1->g - sptr2->g)
				+ fabs(sptr1->b - sptr2->b));
			break;
		      case DIST_L2:
			{
			   /* Distance = Sum L2 (euclidean) norm. */
			   double tmp;
			   tmp = sptr1->r - sptr2->r;
			   dist = tmp*tmp;
			   tmp = sptr1->g - sptr2->g;
			   dist += tmp*tmp;
			   tmp = sptr1->b - sptr2->b;
			   dist += tmp*tmp;
			}
			dist = sqrt(dist);
			break;
		      case DIST_L1L2:
			{
			   /* Distance = Sum L1 * L2 */
			   double acc1,acc2;
			   dist = fabs(sptr1->r - sptr2->r);
			   acc2 = dist * dist;
			   acc1 = fabs(sptr1->g - sptr2->g);
			   acc2 += acc1*acc1;
			   dist += acc1;
			   acc1 = fabs(sptr1->b - sptr2->b);
			   acc2 += acc1*acc1;
			   dist += acc1;
			   dist *= sqrt(acc2);
			}
			break;
		      case DIST_L1ANGLE:
			if (*mptr1 == 0 || *mptr2==0) {
			   /* Problem when either pixel is completely black.
			      This amounts to a mathematical singularity
			      and the angular distance is undefined. */
			   dist = (*mptr1 == *mptr2 ? 0.0 : 1.0);
			}else{
			   dist =  acos(((double)sptr1->r*sptr2->r 
				+ (double)sptr1->g*sptr2->g 
				+ (double)sptr1->b*sptr2->b) 
				     / (*mptr1 * *mptr2));
			   /* If pixels same colour sometimes can get NaN
			      back from acos() calculation, presumably because
			      roundoff error has led to the argument of
			      the acos() to be subtlely bigger than 1.0 */
			   if (isnan(dist)) dist = 0.0; 
			}
			dist *= (fabs(sptr1->r - sptr2->r)
				+ fabs(sptr1->g - sptr2->g)
				+ fabs(sptr1->b - sptr2->b));
			break;
		      case DIST_L2ANGLE:
			if (*mptr1 == 0 || *mptr2==0) {
			   /* Problem when either pixel is completely black.
			      This amounts to a mathematical singularity
			      and the angular distance is undefined. */
			   dist *= (*mptr1 == *mptr2 ? 0.0 : 1.0);
			}else{
			   dist *=  acos(((double)sptr1->r*sptr2->r 
				+ (double)sptr1->g*sptr2->g 
				+ (double)sptr1->b*sptr2->b) 
				     / (*mptr1 * *mptr2));
			   if (isnan(dist)) dist = 0.0; 
			}
			if (dist > 0.0) {
			   double tmp1, tmp2;
			   tmp1 = sptr1->r - sptr2->r;
			   tmp2 = tmp1*tmp1;
			   tmp1 = sptr1->g - sptr2->g;
			   tmp2 += tmp1*tmp1;
			   tmp1 = sptr1->b - sptr2->b;
			   tmp2 += tmp1*tmp1;
			   dist *= sqrt(tmp2);
			}
			break;
		      case DIST_SIMUL:
			if (*mptr1 == 0 || *mptr2==0) {
			   /* Problem when either pixel is completely black.
			      This amounts to a mathematical singularity
			      and the angular distance is undefined. */
			   dist = (*mptr1 == *mptr2 ? 0.0 : 1.0);
			}else{
			   dist = (1.0 - (((double)sptr1->r*sptr2->r 
					   + (double)sptr1->g*sptr2->g 
					   + (double)sptr1->b*sptr2->b) 
					  / (*mptr1**mptr2))
				   * (1 - fabs(*mptr1-*mptr2)
				      / (*mptr1>*mptr2 ? *mptr1 : *mptr2)));
			}
			break;
		      case DIST_MINE1:
			if (*mptr1 == 0 || *mptr2==0) {
			   /* Problem when either pixel is completely black.
			      This amounts to a mathematical singularity
			      and the angular distance is undefined. */
			   dist = 0.0;
			}else{
			   dist =  acos(((double)sptr1->r*sptr2->r 
				+ (double)sptr1->g*sptr2->g 
				+ (double)sptr1->b*sptr2->b) 
				     / (*mptr1 * *mptr2));
			   /* If pixels same colour sometimes can get NaN
			      back from acos() calculation, presumably because
			      roundoff error has led to the argument of
			      the acos() to be subtlely bigger than 1.0 */
			   if (isnan(dist)) dist = 0.0; 
			   dist *= (*mptr1>*mptr2 ? *mptr1 : *mptr2);
			}
			dist += fabs(*mptr1-*mptr2);
			break;
		      case DIST_MINE2:
			if (*mptr1 == 0 || *mptr2==0) {
			   /* Problem when either pixel is completely black.
			      This amounts to a mathematical singularity
			      and the angular distance is undefined. */
			   dist = 0.0;
			}else{
			   dist =  acos(((double)sptr1->r*sptr2->r 
				+ (double)sptr1->g*sptr2->g 
				+ (double)sptr1->b*sptr2->b) 
				     / (*mptr1 * *mptr2));
			   /* If pixels same colour sometimes can get NaN
			      back from acos() calculation, presumably because
			      roundoff error has led to the argument of
			      the acos() to be subtlely bigger than 1.0 */
			   if (isnan(dist)) dist = 0.0; 
			   dist *= pow((*mptr1>*mptr2 ? *mptr1 : *mptr2),
				       parm_r_import_1)
					   / 441.68;
			}
			dist += parm_beta_import_1 * fabs(*mptr1-*mptr2) / 441.68;
			break;
		     }
		     if (isnan(dist)) {
			fprintf(stderr, "WARNING: NaN in distance calculation.\n"
				"Position=(%d,%d), sptr1=(%02x,%02x,%02x),"
				" sptr2=(%02x,%02x,%02x).\n",
				i,j, sptr1->r,sptr1->g,sptr1->b,
				sptr2->r, sptr2->g, sptr2->b);
		     }
		     *aptr += dist;
		     *aptr2 += dist;
		     aptr2++;
		     sptr2++; mptr2++;
		  }
		  kk = -hwidth;
	       }
	       aptr++; sptr1++; mptr1++;
	    }
	 }      

	 if (flag_cb) {

	    /* Identify max and min distance in array */

	    aptr = arr;
	    max = 0.0;
	    min = 1e30; 
	    for (k=0; k<arrsize; k++) {
	       if (*aptr > max) max = *aptr;
	       if (*aptr < min) min = *aptr;
	       aptr++;
	    }
	 }

	 switch (flag_cb) {
	    double cval, m, c, cf;
	 case FLG_CBIAS_1:
	    /* Calculate distances for each pixel by
	       newd = |d -c|
	       where c is distance of centre pixel.
	    */

	    aptr = arr;
	    cval = arr[arrsize/2];
	    for (k=0; k<arrsize; k++) {
	       *aptr = fabs(*aptr - cval + min);
	       aptr++;
	    }
	    break;

	 case FLG_CBIAS_2:
	    /* Calculate distances for each pixel by
	       newd = |d - c| if c <= (M+m)/2
	       newd = |d - (M + m - c)| if c > (M+m)/2
	       where c is distance of centre pixel
	       m = min(distances)
	       M = max(distances)
	    */

	    c = max + min;
	    cf = c / 2;

	    aptr = arr;
	    cval = arr[arrsize/2];
	    if (cval > cf) {
	       cval = c - cval;
	    }
	    for (k=0; k<arrsize; k++) {
	       *aptr = fabs(*aptr - cval + min);
	       aptr++;
	    }
	    break;

	 case FLG_CBIAS_3:
	    log_error(ERR_NOT_IMPLEMENTED);
	    goto halfexit;
	    break;
	 }

#ifdef DEBUG
	 if ((ip_verbose & 0x08) && i==i_pt_stop && j==j_pt_stop) {
	    D(bug("Colour pixel values: [ref pt is (%d,%d)]\n",
		  i_pt_stop,j_pt_stop));
	    for (l=-hwidth; l<=hwidth; l++) {
	       D(bug(" %2d",l));
	       for (k=-hwidth; k<=hwidth; k++) {
		  colour tmp;
		  tmp = IM_PIXEL(im ,i+k, j+l, r);
		  D(bug(" (%02x,%02x,%02x)", tmp.r, tmp.g, tmp.b));
	       }
	       D(bug("\n"));
	    }

	    D(bug("Magnitude values: [ref pt is (%d,%d)]\n",
		  i_pt_stop,j_pt_stop));
	    for (l=-hwidth; l<=hwidth; l++) {
	       D(bug(" %2d",l));
	       for (k=-hwidth; k<=hwidth; k++) {
		  D(bug(" %6.2f", IM_PIXEL(mag ,i+k, j+l, d)));
	       }
	       D(bug("\n"));
	    }

	    D(bug("Distances: [ref pt is (%d,%d)]\n",i_pt_stop,j_pt_stop));
	    aptr = arr;
	    for (l=-hwidth; l<=hwidth; l++) {
	       D(bug(" %2d",l));
	       for (k=-hwidth; k<=hwidth; k++) {
		  D(bug(" %8.5f",*aptr++));
	       }
	       D(bug("\n"));
	    }
	 }

#endif

	 /* Identify max and min distance in array */

	 aptr = arr;
	 max = 0.0;
	 min = 1e30; 
	 for (k=0; k<arrsize; k++) {
	    if (*aptr > max) max = *aptr;
	    if (*aptr < min) {
	       min = *aptr; mpos = k;
	    }
	    aptr++;
	 }

#ifdef DEBUG
	 if ((ip_verbose & 0x08) && i==i_pt_stop && j==j_pt_stop) {
	    D(bug("Max distance value = %g\n",max));
	    D(bug("Min distance value = %g at k=%d [%d,%d]\n",
		  min,mpos,i-hwidth+mpos%width,j-hwidth+mpos/width));
	    switch (flag_filter) {
	    case FILT_ANNF:
	       D(bug("Using filter ANNF\n"));
	       break;
	    case FILT_ANNGF:
	       D(bug("Using filter ANNF (alpha = %g)\n",alpha));
	       break;
	    case FILT_ANNGF2:
	       D(bug("Using filter ANNGF2\n"));
	       break;
	    case FILT_FVF_W1:
	       D(bug("Using filter W1 (beta = %g, r = %g)\n",alpha,r));
	       break;
	    case FILT_FVF_W2:
	       D(bug("Using filter W2 (beta = %g, r = %g)\n",alpha,r));
	       break;
	    case FILT_FVF_W3:
	       D(bug("Using filter W3 (r = %g)\n",r));
	       break;
	    default:
	       D(bug("Unrecognised weight filter.\n"));
	       break;
	    }
	 }

#endif


	 if (max < 0.000001) {
	    /* This occurs if all image values in matrix
	       are identical */

	    sptr = IM_ROWADR(im, j, r);
	    sptr += i;
	    *tptr = *sptr;
	    
	 }else{

	    switch (flag_filter) {
	    case FILT_ANNF:
	       /* Replace array values with weights calculated by
		  <newvalue> = (max - <oldvalue>)
	       */

	       aptr = arr;
	       for (k=0; k<arrsize; k++) {
		  *aptr = (max - *aptr);
		  aptr++;
	       }

	       break;
	    case FILT_ANNGF:
	       /* Replace array values with weights calculated by
		  <newvalue> = 1/(1+a)*(max - <oldvalue>)/(max-min)
		  + a/(1+a)
		  where a is alpha
	       */

	       aptr = arr;
	       {
		  double diff, onepa;

		  diff = max-min;
		  onepa = 1 + alpha;
		  for (k=0; k<arrsize; k++) {
		     *aptr = (max - *aptr)/(diff * onepa) + alpha/onepa;
		     aptr++;
		  }
	       }
	       break;

	    case FILT_ANNGF2:
	       /* Replace array values with weights calculated by
		  <newvalue> = 1/(1+a)*(max - <oldvalue>)/(max-min)
		  + a/(1+a)
		  where a  = 1/(max - <oldvalue>)
	       */

	       aptr = arr;
	       {
		  double diff, onepa;

		  diff = max-min;
		  for (k=0; k<arrsize; k++) {
		     if (max != *aptr) {
			alpha = max - *aptr;
			onepa = 1 + 1/alpha;
			*aptr = alpha/(diff * onepa) + 1/(onepa*alpha);
		     }else{
			*aptr = 1.0;  /* The limit of <oldvalue> -> max. */
		     }
		     aptr++;
		  }
	       }
	       break;

	    case FILT_FVF_W1:
	       /* Replace array values with weights calculated by
		  <newvale> = 1 / (1 + exp(a * <oldvalue>))^r
		  where a is alpha
	       */

	       aptr = arr;
	       for (k=0; k<arrsize; k++) {
		  *aptr = 1.0 / pow((1 + exp(alpha * *aptr)),r);
		  aptr++;
	       }
	       break;


	    case FILT_FVF_W2:
	       /* Replace array values with weights calculated by
		  <newvale> = exp(-<oldvalue>^r / a)
		  where a is alpha
	       */

	       aptr = arr;
	       for (k=0; k<arrsize; k++) {
		  *aptr = exp(-pow(*aptr,r)/alpha);
		  aptr++;
	       }
	       break;


	    case FILT_FVF_W3:
	       /* Replace array values with weights calculated by
		  <newvale> = 1/<oldvalue>^r
		  where a is alpha
	       */

	       aptr = arr;
	       for (k=0; k<arrsize; k++) {
		  *aptr = 1/pow(*aptr,r);
		  aptr++;
	       }
	       break;

	    }
       
	    /* Find sum of array values for normalisation to follow. */

	    aptr = arr;
	    max = 0;
	    for (k=0; k<arrsize; k++) {
	       max += *aptr++;
	    }

#ifdef DEBUG
	    if ((ip_verbose & 0x08) && i==i_pt_stop && j==j_pt_stop) {
	       aptr = arr;
	       D(bug("Weights:\n"));
	       for (l=-hwidth; l<=hwidth; l++) {
		  D(bug(" %2d:",l));
		  for (k=-hwidth; k<=hwidth; k++) {
		     D(bug(" %8.6f",*aptr++));
		  }
		  D(bug("\n"));
	       }
	       D(bug("Weight normalisation = %g.\n",max));
	    }
#endif

	    /* Calculate new colour value for pixel (i,j) by vector summation
	       over pixels in neighbourhood each weighted by appropriate
	       value in array.
	    */

	    if (max < 1e-50) {
	       /* Distance calculation has lossed significance....
		  So we just use centre pixel */
	       
	       sptr = IM_ROWADR(im, j, r);
	       sptr += i;
	       *tptr = *sptr;

	    }else{

	       double red, green , blue;

	       aptr = arr;
	       red = green = blue = 0.0;
	       for (l=-hwidth; l<=hwidth; l++) {
		  colour *sptr;

		  sptr = (colour *)im_rowadr(im, j+l) + (i - hwidth);
		  for (k=-hwidth; k<=hwidth; k++) {
		     red += *aptr * sptr->r;
		     green += *aptr * sptr->g;
		     blue += *aptr * sptr->b;
		     sptr++; aptr++;
		  }
	       }
	       tptr->r = (UBYTE)(red/max);
	       tptr->g = (UBYTE)(green/max);
	       tptr->b = (UBYTE)(blue/max);
	    }
	 }
#ifdef DEBUG
	 if ((ip_verbose & 0x08) && i==i_pt_stop && j==j_pt_stop) {
	    D(bug("Final result: %02x%02x%02x  (%d,%d,%d)\n",
		  tptr->r, tptr->g, tptr->b, tptr->r, tptr->g, tptr->b));
	 }
#endif
	 tptr++;
      }
   }

 halfexit:

   if (flag_border == FLG_ZERO) {
      /* Clear edges of tmp */
      im_clear_border(tmp,hwidth);
   }

   if (flag_border == FLG_EXTEND) {
      log_error(ERR_NOT_IMPLEMENTED);
   }

   if (flag_border == FLG_ZERO || flag_border == FLG_EXTEND) {
      memcpy(im->image.v, tmp->image.v, im->Nx*im->Ny*ip_typesize[im->type]);
   }else{
      coord org,size;

      org.x = org.y = hwidth;
      size.x = im->Nx - 2*hwidth;
      size.y = im->Ny - 2*hwidth;

      im_insert(im, org, tmp, org, size);
   }
   free_image(mag);
   free_image(tmp);
   ip_free(arr);

 exit:
   pop_routine();
   return ip_error;
}




/*

NAME:

   im_rgbmyf() \- Noise suppresion filter an RGB image

PROTOTYPE:

   #include <ip/ip.h>

   int  im_rgbmyf( im, width, flag )
   image *im;
   int width;
   int flag;

DESCRIPTION:

   Filter an RGB image with 'width'*'width' kernel with the filter process I
   have just designed.  'Width' must be odd.  By default, the border region
   where the mask goes off the edge of the image is left unprocessed.  This
   can be modified with the use of 'flag', which may be:

\&      FLG_ZERO   Zero border region of image where mask doesn''t cover
                 properly.

\&      FLG_EXTEND Extend the median function to the edge of the image
                 using as many pixels as are available to calculate
		 the median. (Not Implemented Yet)

   Only L2-norm distance measure presently supported.

LIMITATIONS:

\-   Only RGB images supported
\-   The kernel mask size must be odd.

ERRORS:

   ERR_UNSUPPORTED_TYPE
   ERR_BAD_PARAMETER
   ERR_BAD_FLAG
   ERR_OUT_OF_MEM

*/

int im_rgbmyf( image *im, int width, int flag )
{
   int hwidth;			/* half-width of widthxwidth median filter */
   int i,j;			/* loop variables                          */

   double  *arr;		/* Array for calculating distances */
   int arrsize;			/* Size of array arr */

   image *tmp;			/* Tempory image for storing result */
   image *mag;			/* Magnitude image = |im| */

   int flag_border;		/* Border type part of flag */
   int flag_dist;		/* Distance operator part of flag */


   log_routine("im_rgbmedian");

   if (im->type!=IM_RGB) {
      log_error(ERR_UNSUPPORTED_TYPE);
      goto exit;
   }

   flag_border = flag & FLGMSK_BORDER;
   flag_dist = DIST_L2;		/* Presently set solid to L2 norm */

   if (NOT flag_ok(flag_border,FLG_ZERO,FLG_EXTEND,LASTFLAG))
      return ip_error;

   if (NOT flag_ok(flag_dist, DIST_L1, DIST_L2, DIST_ANGLE,
		   DIST_L1L2, DIST_L1ANGLE, DIST_L2ANGLE, 
		   DIST_SIMUL, DIST_MINE1, DIST_MINE2, LASTFLAG))
      return ip_error;

   if (flag_dist == 0) flag_dist = DIST_L1;

   if (NOT ip_parm_greatereq("width", width, 3.0))
      return ip_error;
      
   if (NOT ip_parm_odd("width", width))
      return ip_error;

   if ((tmp = alloc_image(im->type,im->Nx,im->Ny)) == NULL)
      goto exit;

   hwidth = width / 2;		/* half-width of filter */
   arrsize = width*width;

   /* initialise array */

   if ((arr = ip_malloc(sizeof(double) * arrsize)) == NULL) {
      free_image(tmp);
      return ip_error;
   }

   /* Initialise an image with the vector magnitude of each pixel. */

   if ((mag = im_convert(im, IM_DOUBLE, FLG_MAG)) == NULL) {
      ip_free(arr);
      free_image(tmp);
      return ip_error;
   }


   /* for each pixel in image find centre-distances and dissimilarity over
      width*width mask */

   for( j = hwidth; j < (im->Ny - hwidth); j++ ) {
      colour *tptr;		/* Result image */
      colour *cptr; 		/* Pointer to current pix */
      j_pt_export = j;

      tptr = (colour *)im_rowadr(tmp, j) + hwidth;
      cptr = (colour *)im_rowadr(im, j) + hwidth;

      for( i = hwidth; i < (im->Nx - hwidth); i++ ) {
	 int k,l;		/* Index into mask wrt point (i,j) */
	 double *aptr;		/* Pointer into distance array arr */
	 int numun;		/* Num of pixels close to centre pixel */
	 double nred, ngreen, nblue; /* New pixel values */
	 double ared, agreen, ablue; /* New pixel values */

	 /* Load up array with distances to centre pixel */

	 numun = 0;
	 nred = ngreen = nblue = 0;
	 ared = agreen = ablue = 0;
	 aptr = arr;
	 for (l=-hwidth; l<=hwidth; l++) {
	    colour *sptr;	/* Source pixel in image im */
	    double dist;

	    sptr = IM_ROWADR(im, j+l, r) + (i - hwidth);
	    for (k=-hwidth; k<=hwidth; k++) {
	       double tmp;

	       tmp = cptr->r - sptr->r;
	       dist = tmp*tmp;
	       tmp = cptr->g - sptr->g;
	       dist += tmp*tmp;
	       tmp = cptr->b - sptr->b;
	       dist += tmp*tmp;
	       if ((*aptr++ = sqrt(dist)) < 80) {
		  numun++;
		  nred += sptr->r;
		  ngreen += sptr->g;
		  nblue += sptr->b;
	       }
	       ared += sptr->r;
	       agreen += sptr->g;
	       ablue += sptr->b;
	       sptr++;
	    }
	 }
	 if (numun > width-1) {
	    /* Calculate mean of pixels close to centre pixel. */
	    tptr->r = (UBYTE)(nred / numun);
	    tptr->g = (UBYTE)(ngreen / numun);
	    tptr->b = (UBYTE)(nblue / numun);
	 }else{
	    /* Calculate mean of all pixels. */
	    tptr->r = (UBYTE)(ared / arrsize);
	    tptr->g = (UBYTE)(agreen / arrsize);
	    tptr->b = (UBYTE)(ablue / arrsize);
	 }

	 cptr++;
	 tptr++;
      }
   }

   if (flag_border == FLG_ZERO) {
      /* Clear edges of tmp */
      im_clear_border(tmp,hwidth);
   }

   if (flag_border == FLG_EXTEND) {
      log_error(ERR_NOT_IMPLEMENTED);
   }

   if (flag_border == FLG_ZERO || flag_border == FLG_EXTEND) {
      memcpy(im->image.v, tmp->image.v, im->Nx*im->Ny*ip_typesize[im->type]);
   }else{
      coord org,size;

      org.x = org.y = hwidth;
      size.x = im->Nx - 2*hwidth;
      size.y = im->Ny - 2*hwidth;

      im_insert(im, org, tmp, org, size);
   }
   free_image(mag);
   free_image(tmp);
   ip_free(arr);

 exit:
   pop_routine();
   return ip_error;
}

