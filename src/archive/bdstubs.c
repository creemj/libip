/* $Id$ */
/*
   Module: ip/bdstubs.c
   Author: M.J.Cree

   Provides function stubs that allow compilation of IP library display
   routines when the BD display library is unavailable.

   � 1998-2001 Michael Cree 

*/


#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <time.h>

#include <bd/bd.h>

static char rcsid[] = "$Id$";


static int bd_width;
static int bd_height;

static int id1001_width = 0;
static int id1001_height = 0;

static time_t tmlast;

static int idcount = 1000;


int bd_Init(int *argc, char **argv)
{
  bd_height = bd_width = 512;
  tmlast = 0;
  return BD_SUCCESS;
}


int bd_WindowSize(int width, int height)
{
  tmlast = 0;
  if (width >= 0 && width <= 2000 && height >= 0 && height <= 2000) {
    bd_height = height;
    bd_width = width;
    return BD_SUCCESS;
  }else
    return BD_ERR;
}



int bd_DisplayImage(void *image, int datatype, double minval, double maxval,
		    int width, int height, int xpos, int ypos, int id)
{
  tmlast = 0;
  if (id == BD_NEWIMAGE) id = ++idcount;
  if (id == 1001) {
    id1001_width = width;
    id1001_height = height;
  }
  return id;
}



int bd_OverlaySubimage(void *image, int datatype,
		       int width, int height, int xpos, int ypos, int id)
{
  tmlast = 0;
  return BD_SUCCESS;
}


int bd_Palette(int paltype, byte *palette)
{
  tmlast = 0;
  return BD_SUCCESS;
}


int bd_OverlayText(char *string, int x, int y,
		   char *font, int size, int colour, int id)
{
  tmlast = 0;
  return BD_SUCCESS;
}



int bd_TextWidth(char *string, char *font, int size, int colour)
{
  tmlast = 0;
  return ((strlen(string) * size * 3) / 4);
}


int bd_ClearOverlays(int select, int id)
{
  tmlast = 0;
  return BD_SUCCESS;
}


int bd_ClearOverlayRegion(int select, int x, int y, int width, int height, int id)
{
  tmlast = 0;
  return BD_SUCCESS;
}

mouse *bd_MouseCoords(int block)
{
  return NULL;
}


lastclick *bd_LastMouseClick(int block)
{
  time_t tmthis;
  static lastclick click;

  tmthis = time(NULL);
  if (block || (tmthis == tmlast)) {
    tmlast = tmthis;
    if (idcount > 1000) {
      click.id = 1001;
      click.x = id1001_width/2;
      click.y = id1001_height/2;
    }else{
      click.id = 0;
      click.x = bd_width/2;
      click.y = bd_height/2;
    }
    click.whichbutton = BD_LEFT;
    return &click;
  }
  tmlast = tmthis;
  return NULL;
}


int bd_GetChar(int block)
{
  return -1;
}

void bd_DragImage(int dragimage)
{
  tmlast = 0;
  return ;
}

int bd_CanvasTitle(char *string)
{
  tmlast = 0;
  return BD_SUCCESS;
}

