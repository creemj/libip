/* $Id$ */
/*
   Module: ip/visilog.c
   Author: M.J.Cree

   Routines for reading in and saving Visilog images.

*/
/*
 * $Log$
 * Revision 1.13  1997/10/10 15:31:04  mph489
 * write_visilog_image() true routine rather than macro.
 * Generates error if fclose() fails.
 * New routine read_visilog_imagex() to read part of image.
 *
 * Revision 1.12  1997/05/23  13:15:19  mph489
 * Added necessary include files to function documentation.
 *
 * Revision 1.11  1997/04/08  12:59:44  mph489
 * Fixed some free() calls to be ip_free().
 *
 * Revision 1.10  1997/03/04  09:51:53  mph489
 * Changed all references to Nx and Ny to size (both in image and vhandle).
 * Changed use of Xi() to im_rowadr() and a pointer.
 *
 * Revision 1.9  1996/09/11  14:36:36  mph489
 * Added BINARY image support.
 * Better reporting of filename if error occurs.
 *
 * Revision 1.8  1996/08/22  15:20:35  mph489
 * Can now read Visilog files from stdin.
 * Fixed minor bugs in printing errors when writing to stdout.
 * Modified:
 *   open_visilog_image()
 *   write_visilog_image()
 *   close_visilog_image()
 * Added:
 *   open_visilog_imagex()
 *
 * Revision 1.7  1996/08/05  11:28:05  mph489
 * Can write images to a files or stdout.
 * Changed typesize[] to ip_typesize[].
 * Better error messages if pass unsupported image type to write.
 *
 * Revision 1.6  1996/07/19  13:36:56  mph489
 * Modified write_visilog_image() entry point.
 * Now have two entry points:
 *   write_visilog_image()
 *   write_visilog_imagex()
 *
 * Revision 1.5  1996/07/10  13:01:17  mph489
 * Added code for reading in Vislog binary mask images.
 *
 * Revision 1.4  1996/06/11  10:47:13  mph489
 * Uses ip_malloc() and ip_free() now.
 *
 * Revision 1.3  1996/02/22  17:32:00  mph489
 * Updated to use I_CHRU, etc rather than hard coded values.
 *
 * Revision 1.2  1995/11/02  15:00:30  mph489
 * Fix bugs with reading in vislog V4 files.
 *   - Couldn't identify V4 image type properly!
 *
 * Revision 1.1  1995/09/29  16:07:19  mph489
 * Initial revision
 *
 */


#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <ip/internal.h>
#include <ip/visilog.h>


#include <ipe/dskimg.h>

static char rcsid[] = "$Id$";

/* From Visilog manual */

#define IG_RECTANGLE 0
#define I_FLOAT	0
#define I_LONG	1
#define I_SHORT	2
#define I_CHR	4
#define I_USHORT 0x12
#define I_CHRU	0x14
#define I_BIN   0x22

/* Internal call points. */
static int convert_type(int);
static int wr_visilog_image(const char *fname, image *im, int flag);
static image *rd_visilog_image(vhandle *vh, int first_row, int nrows);
static int seek_to_row(vhandle *vh, void * rowbuf, int skip, int rowsize);



/*

NAME:

   open_visilog_image() \- Open Visilog image file ready for reading

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/visilog.h>

   vhandle *open_visilog_image( filename )
   const char * filename;

DESCRIPTION:

   Open visilog image corresponding to 'filename'. Returns visilog handle
   or NULL if any errors occur. One can check image type and size in 
   the visilog handle. This routine does not read in the actual image
   data, only the visilog image header.  If 'filename' is NULL, reads
   from stdin.

   Example of reading in Visilog image file:

\|   vhandle *vh;
\|   image *im;
\|
\|   vh = open_visilog_image("gertrude");
\|   im = read_visilog_image(vh);
\|   close_visilog_image(vh);

   Reading Visilog version 3 and version 4 images is supported.

LIMITATIONS:

   The Visilog file image data type can be signed byte, unsigned short
   or unsigned long.  These are data types are not supported by the ip
   library.  At present an attempt to open an unsigned long image
   generates an ERR_INVALID_VISILOG_TYPE error.  An attempt to open
   unsigned short or signed byte images is succesful and returns an ip
   image type code that can represent the image data without any loss
   of significance.  However the image data read routines don''t
   support these two image data types yet and will generate an
   ERR_NOT_IMPLEMENTED error.

   Visilog images defined on a hexagonal grid are not supported and
   generate an ERR_INVALID_VISILOG_TYPE error.

ERRORS:

   ERR_OUT_OF_MEMORY
   ERR_FILE_OPEN     - Error when opening file
   ERR_NOT_VISILOG   - File is not a Visilog file
   ERR_SHORT_FILE    - File terminated unexpectantly
   ERR_INVALID_VISILOG_TYPE - Visilog file type not supported

SEE ALSO:

   open_visilog_image()   open_visilog_imagex()
   read_visilog_image()   read_visilog_imagex()
   close_visilog_image()
   write_visilog_image()  write_visilog_imagex()
   ip_read_image()

*/

vhandle *open_visilog_image(const char * fname)
{
   unsigned char header[4];
   vhandle *vh;
   FILE *f;

   log_routine("open_visilog_image");

   vh = NULL;
   f = NULL;

   /* Open disk file and read in first part in anticipation 
      of visilog image */

   if (fname) {
      if ((f = fopen(fname,"rb")) == NULL) {
	 log_error(ERR_FILE_OPEN,fname);
	 goto exit_ovi;
      }
   }else{
      f = stdin;
      fname = ipmsg_stdin;
   }

   if (fread(header,1,4,f) != 4) {
      log_error(ERR_SHORT_FILE,fname);
      goto exit_ovi;
   }

   vh = open_visilog_imagex(f, fname, header, NO_FLAG);

 exit_ovi:

   if ((vh == NULL) && f && (f != stdin))
      fclose(f);

   pop_routine();
   return vh;
}



/*

NAME:

   open_visilog_imagex() \- Open Visilog image file ready for reading

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/visilog.h>

   vhandle * open_visilog_imagex( file, filename, header, flag )
   FILE *file;
   const char *filename;
   unsigned char header[4];
   int flag;

DESCRIPTION:

   Interpret header of a file that is believed to be a Visilog image.
   This routine is primarily intended for internal ip library use,
   however it may be of use to design sophisticated image loaders
   that automatically detect image file type.

   It is assumed that 'file' of filename 'filename' has already been
   opened and the first four bytes have already been read.  Pass the
   first four bytes read through 'header'.  This routine reads more of
   the file and returns a visilog handle describing the image file.
   The reason for doing it this way is so one can open a file that is
   a pipe (or otherwise non-seekable), determine the file type based
   on the first four bytes, then call the correct routine to load the
   image. The 'filename' is only for reference for printing any error
   messages.

   Supports both Visilog 3 and 4 image file types.  If 'file' is
   found not to be a Visilog image (that is, the four header bytes do
   not match any known Visilog image type) then this routine returns
   automatically (flagging the error ERR_NOT_VISILOG) without having
   touched the input file.  Thus the possibility of reading the file
   as some other image type (even if it is unseekable) is not lost.
   Other errors possibly mean that some data has been read from the
   file and thus will be lost if the file is unseekable.  Returns NULL
   if any errors occur.  The file remains opened even if an error
   occurs.

ERRORS:

   ERR_OUT_OF_MEMORY
   ERR_NOT_VISILOG
   ERR_SHORT_FILE
   ERR_INVALID_VISILOG_TYPE

SEE ALSO:

   open_visilog_image()
   read_visilog_image()   read_visilog_imagex()
   close_visilog_image()
   write_visilog_image()  write_visilog_imagex()
   ip_read_image()

*/

vhandle *open_visilog_imagex(FILE *f, const char *fname, unsigned char hdr[4],
			     int flag)
{
   vhandle *vh;
   struct V4desc_im v4head;
   struct desc_im *v3head;

   log_routine("open_visilog_imagex");

   vh = NULL;

   /* Allocate visilog handle. */

   if ((vh = ip_mallocx(sizeof(vhandle))) == NULL) {
      goto exit_ovi;
   }
   vh->f = f;
   vh->fname = fname;

   /* Read and interpret header */

   v4head.i_magic = (hdr[0] << 24) + (hdr[1] << 16) + (hdr[2] << 8) + hdr[3];

   switch (v4head.i_magic) {
    case 0x00006932:		/* Version 4 header */
    case 0x32690000:

      if (fread(&v4head.gx,1,sizeof(struct V4desc_im) - 4,vh->f)
					!= sizeof(struct V4desc_im)-4) {
	 log_error(ERR_SHORT_FILE,fname);
	 goto exit_ovi;
      }
 
      if ((vh->type = convert_type(v4head.gcode)) == IM_TYPEMAX) {
	 log_error(ERR_INVALID_VISILOG_TYPE,fname);
	 goto exit_ovi;
      }
      vh->vtype = v4head.gcode;
      vh->size.x = v4head.gx;
      vh->size.y = v4head.gy;
      if (v4head.gz != 1 || v4head.gt != 1 || v4head.grid != IG_RECTANGLE
	  					   || v4head.gtype != 0x100) {
	 log_error(ERR_INVALID_VISILOG_TYPE,fname);
	 goto exit_ovi;
      }
      break;

    case 0x00006931:		/* Version 3 header */
    case 0x31690000:

      v3head = (struct desc_im *)(&v4head);
      if (fread(&(v3head->i_dimx),1,sizeof(struct desc_im) - 4,vh->f)
					!= sizeof(struct desc_im)-4) {
	 log_error(ERR_SHORT_FILE,fname);
	 goto exit_ovi;
      }
 
      if ((vh->type = convert_type(v3head->i_code)) == IM_TYPEMAX) {
	 log_error(ERR_INVALID_VISILOG_TYPE,fname);
	 goto exit_ovi;
      }
      vh->vtype = v3head->i_code;
      vh->size.x = v3head->i_dimx;
      vh->size.y = v3head->i_dimy;
      if (v3head->i_dimz != 1 || v3head->i_maille != IG_RECTANGLE) {
	 log_error(ERR_INVALID_VISILOG_TYPE,fname);
	 goto exit_ovi;
      }
      break;

    default:			/* Not a Visilog file! */

      log_error(ERR_NOT_VISILOG,fname);
      goto exit_ovi;
      break;
   }

 exit_ovi:

   if (ip_error != OK && vh != NULL) {
      ip_free(vh);
      vh = NULL;
   }

   pop_routine();
   return vh;
}




/*

NAME:

   write_visilog_image() \- Write image as a Visilog file

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/visilog.h>

   int write_visilog_image( filename, im )
   const char * filename;
   image * im;

DESCRIPTION:

   Writes the image 'im' as a Visilog (version 3) file to
   'filename'. Any error conditions returned.  If 'filename' is NULL
   then 'im' is written to stdout.

LIMITATIONS:

   Only writes of BINARY, BYTE, SHORT, LONG and FLOAT images supported.

ERRORS:

   ERR_UNSUPPORTED_VISILOG_WRITE
   ERR_OUT_OF_MEM
   ERR_FILE_OPEN
   ERR_FILEIO
   ERR_FILE_CLOSE

SEE ALSO:

   open_visilog_image()   open_visilog_imagex()
   read_visilog_image()   read_visilog_imagex()
   close_visilog_image()
   write_visilog_imagex()
   ip_read_image()

*/


int write_visilog_image(const char *fname, image *im)
{
   log_routine("write_visilog_image");
   return wr_visilog_image(fname, im, NO_FLAG);
}



/*

NAME:

   write_visilog_imagex() \- Write image as a Visilog file

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/visilog.h>

   int write_visilog_imagex( filename, im, flag )
   const char * filename;
   image * im;
   int flag;

DESCRIPTION:

   Writes the image 'im' as a Visilog (version 3) file to
   'filename'. Any error conditions returned.  If 'filename' is NULL
   then 'im' is written to stdout.

LIMITATIONS:

   Only writes of BINARY, BYTE, SHORT, LONG and FLOAT images supported.

ERRORS:

   ERR_UNSUPPORTED_VISILOG_WRITE
   ERR_OUT_OF_MEM
   ERR_FILE_OPEN
   ERR_FILEIO
   ERR_FILE_CLOSE

SEE ALSO:

   open_visilog_image()   open_visilog_imagex()
   read_visilog_image()   read_visilog_imagex()
   close_visilog_image()
   write_visilog_image()
   ip_read_image()

*/


int write_visilog_imagex(const char *fname, image *im, int flag)
{
   log_routine("write_visilog_imagex");
   return wr_visilog_image(fname, im, flag);
}



static int wr_visilog_image(const char *fname, image *im, int flag)
{
   struct desc_im *v;
   FILE *f;
   SHORT *rowbuf;
   UBYTE *iptr;
   int i,j;
   static int vtypecode[IM_TYPEMAX] = {
      I_CHRU,
      I_SHORT,
      I_LONG,
      I_FLOAT,
      0xff,
      0xff
   };

   v = NULL;
   if (fname == NULL) {
      f = stdout;
      fname = ipmsg_stdout;
   }else{
      f = NULL;
   }

   /* No such thing as complex or double visilog images. */

   if (NOT image_type_integer(im)
		&& im->type != IM_FLOAT && im->type != IM_BINARY) {
      log_error(ERR_UNSUPPORTED_VISILOG_WRITE,fname);
      goto exit_wvi;
   }

   /* Allocate and clear header */

   if ((v = calloc(sizeof(struct desc_im),1)) == NULL) {
      log_error(ERR_OUT_OF_MEM);
      goto exit_wvi;
   }

   /* Initialise non-zero values in header */

   v->i_dimx = im->size.x;
   v->i_dimy = im->size.y;
   v->i_dimz = 1;
   v->i_magic = 0x00006931;
   v->i_orix = v->i_oriy = v->i_oriz = 1;
   v->hdr_sys = v->hdr_data = 76;
   if (flag == FLG_BINARY || im->type == IM_BINARY) {
      v->i_bstoc = sizeof(SHORT) * 8;
      v->i_code = I_BIN;
   }else{
      v->i_bstoc = ip_typesize[im->type] * 8;
      v->i_code = vtypecode[im->type];
   }

   /* Open file and write header followed by image data. */

   errno = 0;
   if (f != stdout) {
      if ((f = fopen(fname,"wb")) == NULL) {
	 log_error(ERR_FILE_OPEN,fname);
	 goto exit_wvi;
      }
   }

   if (fwrite(v, 1, sizeof(struct desc_im), f) != sizeof(struct desc_im)) {
      log_error(ERR_FILEIO,fname);
      goto exit_wvi;
   }

   if (flag == FLG_BINARY || im->type == IM_BINARY) {
      if ((rowbuf = ip_mallocx(sizeof(SHORT)*im->size.x)) == NULL) {
	 goto exit_wvi;
      }
      for (j=0; j<im->size.y; j++) {
	 iptr = im_rowadr(im, j);
	 for (i=0; i<im->size.x; i++) {
	    rowbuf[i] = (*iptr++ != 0);
	 }
	 if (fwrite(rowbuf, sizeof(SHORT), im->size.x, f) != im->size.x) {
	    log_error(ERR_FILEIO,fname);
	    ip_free(rowbuf);
	    goto exit_wvi;
	 }
      }
      ip_free(rowbuf);
   }else{
      if (fwrite(im->image.v, ip_typesize[im->type], im->size.x*im->size.y, f) 
						  != im->size.x * im->size.y) {
	 log_error(ERR_FILEIO,fname);
	 goto exit_wvi;
      }
   }

 exit_wvi:

   if (f && f != stdout) {
      if (fclose(f) != 0 && ip_error == OK) {
	 log_error(ERR_FILE_CLOSE, fname);
      }
   }

   if (v) 
      ip_free(v);

   pop_routine();

   return ip_error;
}



/*

NAME:

   read_visilog_image() \- Read in image data from opened Visilog file

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/visilog.h>

   image * read_visilog_image( handle )
   vhandle *handle;

DESCRIPTION:

   Reads in visilog image data corresponding to 'handle' from the
   disk. Returns the image data with image an opened image handle. One
   neads to call open_visilog_image() to get a valid 'handle' before
   calling this routine.  Returns NULL if an error occurs.

   Reading Visilog version 3 and version 4 images is supported.

   The reading of BINARY, BYTE, SHORT, LONG and FLOAT images is
   supported.

LIMITATIONS:

   The Visilog file image data type can also be signed byte or
   unsigned short.  At present these generate an ERR_NOT_IMPLEMENTED
   error.

ERRORS:

   ERR_OOR_PARAMETER
   ERR_OUT_OF_MEM
   ERR_NOT_IMPLEMENTED
   ERR_FILEIO_OR_TOO_SHORT

SEE ALSO:

   open_visilog_image()   open_visilog_imagex()
   read_visilog_imagex()
   close_visilog_image()
   write_visilog_image()  write_visilog_imagex()
   ip_read_image()

*/

image *read_visilog_image(vhandle *vh)
{
   log_routine("read_visilog_image");
   return rd_visilog_image(vh, 0, vh->size.y);
}




/*

NAME:

   read_visilog_imagex() \- Read in image data from opened Visilog file

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/visilog.h>

   image * read_visilog_imagex( handle, first_row, nrows )
   vhandle *handle;
   int first_row;
   int nrows;

DESCRIPTION:

   Reads in part visilog image data corresponding to 'handle' from the
   disk. Returns the image data with image an opened image handle. One
   needs to call open_visilog_image() to get a valid 'handle' before
   calling this routine.  Returns NULL if an error occurs.

   The first row of an image is numbered zero. Hence to read only the
   first row of an image specify 'first_row'=0 and 'nrows'=1.

   The image returned has 'nrows' rows in it.

   Reading Visilog version 3 and version 4 images is supported.

LIMITATIONS:

   The Visilog file image data type can also be signed byte or
   unsigned short.  At present these generate an ERR_NOT_IMPLEMENTED
   error.

ERRORS:

   ERR_OOR_PARAMETER
   ERR_OUT_OF_MEM
   ERR_NOT_IMPLEMENTED
   ERR_FILEIO_OR_TOO_SHORT

SEE ALSO:

   open_visilog_image()   open_visilog_imagex()
   read_visilog_image()
   close_visilog_image()
   write_visilog_image()  write_visilog_imagex()
   ip_read_image()

*/

image *read_visilog_imagex(vhandle *vh, int first_row, int nrows)
{
   log_routine("read_visilog_imagex");
   return rd_visilog_image(vh, first_row, nrows);
}




static image *rd_visilog_image(vhandle *vh, int first_row, int nrows)
{
   image *im;
   SHORT *rowbuf;
   UBYTE *iptr;
   int i,j;

   /* Check first_row and nrows are within range */

   if (NOT ip_parm_inrange("first_row", first_row, 0, vh->size.y-1))
      return NULL;

   if (NOT ip_parm_inrange("number_rows", nrows, 1, vh->size.y-first_row))
      return NULL;

   /* Note presence of binary image */

   rowbuf = NULL;

   /* Allocate image */

   if ((im = alloc_image(vh->type, vh->size.x, nrows)) == NULL)
      goto exit_rvi;



   /* Check to see if need to upcast image type */

   switch (vh->vtype) {
    case I_CHR:
    case I_USHORT:

      /* Havn't got around to implementing these ones.... */

      log_error(ERR_NOT_IMPLEMENTED);
      break;

    case I_BIN:
   
      /* Have to handle binary masks specially. */

      if ((rowbuf = ip_mallocx(im->size.x * sizeof(SHORT))) == NULL) {
	 break;
      }

      /* skip first rows ... */

      if (seek_to_row(vh, rowbuf, first_row, im->size.x*sizeof(SHORT)) != OK) {
	 break;
      }
	 
      /* ... and read nrows */

      for (j=0; j<im->size.y; j++) {
	 if (fread(rowbuf, sizeof(SHORT), im->size.x, vh->f) != im->size.x) {
	    log_error(ERR_FILEIO_OR_TOO_SHORT,vh->fname);
	    goto exit_rvi;
	 }
	 iptr = im_rowadr(im, j);
	 for (i=0; i<im->size.x; i++) {
	    *iptr++ = (rowbuf[i] == 0) ? 0 : 255;
	 }
      }

      break;
    default:

      /* Read in image. */

      errno = 0;

      /* skip first rows ... */

      if (seek_to_row(vh, rowbuf, first_row,
		      im->size.x*ip_typesize[im->type]) != OK) {
	 break;
      }

      /* ... and read nrows */

      if (fread(im->image.v,ip_typesize[im->type],im->size.x*im->size.y, vh->f)
						!= im->size.x*im->size.y ) {
	 log_error(ERR_FILEIO_OR_TOO_SHORT,vh->fname);
      }
      break;
   }

 exit_rvi:

   if (ip_error != OK && im) {
      free_image(im);
      im = NULL;
   }
   if (rowbuf) ip_free(rowbuf);
   pop_routine();
   return im;
}




/*

NAME:

   close_visilog_image() \- Close previously opened Visilog image file

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/visilog.h>

   void close_visilog_image( vhandle )
   vhandle *vhandle;

DESCRIPTION:

   Closes visilog file associated with 'vhandle'. 'Vhandle' is not
   valid anymore after calling close_visilog_image(). Safe to call
   with NULL 'vhandle' or on files attached to stdin.

ERRORS:

   ERR_FILE_CLOSE

SEE ALSO:

   open_visilog_image()   open_visilog_imagex()

*/

void close_visilog_image(vhandle *vh)
{
   log_routine("close_visilog_image");

   if (vh) {
      if (vh->f && vh->f != stdin)
	 if (fclose(vh->f) != 0)
	    log_error(ERR_FILE_CLOSE,vh->fname);
      ip_free(vh);
   }
   pop_routine();
}




/* Convert visilog image type into our type */
static int convert_type(int vtype)
{
   int type;

   switch (vtype) {
    case I_FLOAT:
      type = IM_FLOAT;
      break;
    case I_LONG:
      type = IM_LONG;
      break;
    case I_SHORT:
      type = IM_SHORT;
      break;
    case I_CHR:
      type = IM_SHORT;	/* Have to upcast to maintain accuracy */
      break;
    case I_USHORT:
      type = IM_LONG;	/* Have to upcast to maintain accuracy */
      break;
    case I_CHRU:
      type = IM_BYTE;
      break;
    case I_BIN:
      type = IM_BINARY;
      break;
    default:
      type = IM_TYPEMAX;	/* To signal invalid type */
      break;
   }
   return type;
}



static int seek_to_row(vhandle *vh, void * rowbuf, int skip, int rowsize)
{
   int j;
   int ownbuf;

   if (skip != 0) {		/* Only do seek if not first row of image */
      errno = 0;
      if (fseek(vh->f,(long)skip*rowsize, SEEK_CUR)) {

	 if (errno == ESPIPE) {
				/* If pipe, read through data consequtively */
	    ownbuf = FALSE;
	    if (rowbuf == NULL) {
	       if ((rowbuf = ip_mallocx(rowsize)) == NULL) {
		  goto exit_str;
	       }
	       ownbuf = TRUE;
	    }
	    for (j=0; j<skip; j++) {
	       if (fread(rowbuf,1,rowsize,vh->f) != rowsize) {
		  log_error(ERR_FILEIO_OR_TOO_SHORT,vh->fname);
		  break;
	       }
	    }
	    if (ownbuf) ip_free(rowbuf);

	 }else{
	    log_error(ERR_FILEIO_OR_TOO_SHORT,vh->fname);
	 }

      }
   }

 exit_str:
   return ip_error;

}
