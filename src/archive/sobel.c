/* $Id$ */
/*
   Module: ip/sobel.c
   Author: M.J.Cree

   Sobel edge detection operator

*/

#include <stdlib.h>
#include <math.h>
#include <ip/internal.h>

static char rcsid[] = "$Id$";


/* Sobel operator macros - calculates sobel value at pixel (i,j). */

#define SOBEL_BYTE \
{\
   long n,m;\
   UBYTE *pix;\
 \
   if (vert) {\
      pix = IM_ROWADR(im, j+1,b);\
      pix += i;\
      n = (long)*(pix-1) + 2L**pix + (long)*(pix+1);\
      pix = IM_ROWADR(im, j-1,b);\
      pix += i;\
      n -= (long)*(pix-1) + 2L**pix + (long)*(pix+1);\
   }\
   if (horiz) {\
      pix = IM_ROWADR(im, j-1,b);\
      pix += i;\
      m = (long)*(pix+1) - (long)*(pix-1);\
      pix = IM_ROWADR(im, j,b);\
      pix += i;\
      m += 2*((long)*(pix+1) - (long)*(pix-1));\
      pix = IM_ROWADR(im, j+1,b);\
      pix += i;\
      m += (long)*(pix+1) - (long)*(pix-1);\
   }else{\
      m = n;\
   }\
   if (mag) {\
      m = ROUND(sqrt(n*n+m*m));\
   }\
   if (m > 255) m = 255;\
   if (m < 0) m = 0;\
   IM_PIXEL(tmp,i,j,b) = (UBYTE)m;\
}


#define SOBEL_INT(t,b) \
{\
   long n,m;\
   t *pix;\
 \
   if (vert) {\
      pix = IM_ROWADR(im, j+1,b);\
      pix += i;\
      n = (long)*(pix-1) + 2L**pix + (long)*(pix+1);\
      pix = IM_ROWADR(im, j-1,b);\
      pix += i;\
      n -= (long)*(pix-1) + 2L**pix + (long)*(pix+1);\
   }\
   if (horiz) {\
      pix = IM_ROWADR(im, j-1,b);\
      pix += i;\
      m = (long)*(pix+1) - (long)*(pix-1);\
      pix = IM_ROWADR(im, j,b);\
      pix += i;\
      m += 2*((long)*(pix+1) - (long)*(pix-1));\
      pix = IM_ROWADR(im, j+1,b);\
      pix += i;\
      m += (long)*(pix+1) - (long)*(pix-1);\
   }else{\
      m = n;\
   }\
   if (mag) {\
      m = (t)ROUND(sqrt((double)n*n+(double)m*m));\
   }\
   IM_PIXEL(tmp,i,j,b) = (t)m;\
}


#define SOBEL_REAL(t,b) \
{\
   double n,m;\
   t *pix;\
 \
   if (vert) {\
      pix = IM_ROWADR(im, j+1,b);\
      pix += i;\
      n = (double)*(pix-1) + 2L**pix + (double)*(pix+1);\
      pix = IM_ROWADR(im, j-1,b);\
      pix += i;\
      n -= (double)*(pix-1) + 2L**pix + (double)*(pix+1);\
   }\
   if (horiz) {\
      pix = IM_ROWADR(im, j-1,b);\
      pix += i;\
      m = (double)*(pix+1) - (double)*(pix-1);\
      pix = IM_ROWADR(im, j,b);\
      pix += i;\
      m += 2*((double)*(pix+1) - (double)*(pix-1));\
      pix = IM_ROWADR(im, j+1,b);\
      pix += i;\
      m += (double)*(pix+1) - (double)*(pix-1);\
   }else{\
      m = n;\
   }\
   if (mag) {\
      m = sqrt(n*n+m*m);\
   }\
   IM_PIXEL(tmp,i,j,b) = (t)m;\
}





/*

NAME:

   im_sobel() \- Sobel edge detection on image

PROTOTYPE:

   #include <ip/ip.h>

   int im_sobel( im, flag )
   image *im;
   int flag;

DESCRIPTION:

   Do edge detection on image with Sobel operator. 'Flag' must be one
   of:

\&   FLG_FHORIZ  Do Sobel operation in horizontal direction

\&   FLG_FVERT   Do Sobel operation in vertical direction

\&   FLG_FMAG    Calculate the magnitude of the horizontal and vertical Sobel
              operators

   By default the one-pixel border is left unchanged, however FLG_ZERO may
   be ORed into 'flag' with one of the flags above, which specifies that
   the border region is to be zeroed.

   For BYTE images only, the result of the Sobel operator is clipped
   to be in the range 0-255 before being stored back into the pixel.
   For other image types calculations could overflow and the result is
   then unspecified.

   Supports BYTE, SHORT, LONG, FLOAT and DOUBLE image types only.

ERRORS:

   ERR_UNSUPPORTED_TYPE
   ERR_BAD_FLAG
   ERR_OUT_OF_MEM

SEE ALSO:

   im_convolve()

*/

int im_sobel(image *im, int flag)
{
   image *tmp;
   int i,j;
   int horiz, vert, mag, zero, flttype, bdrtype;

   log_routine("im_sobel");

   if (im->type == IM_COMPLEX ||
                   image_type_clrim(im) || im->type == IM_BINARY) {
      log_error(ERR_UNSUPPORTED_TYPE);
      goto exit;
   }

   if (NOT type_compiled(im->type))
      return ip_error;

   bdrtype = flag & FLGMSK_BORDER;
   flttype = flag & FLGMSK_FILTER;
   
   if (NOT flag_ok(bdrtype, FLG_ZERO, LASTFLAG))
      return ip_error;
   
   if (NOT flag_ok(flttype, FLG_FHORIZ, FLG_FVERT, FLG_FMAG, LASTFLAG))
      return ip_error;

   if (NOT flag_bits_unused(flag,~(FLGMSK_BORDER | FLGMSK_FILTER)))
      return ip_error;

   zero = (bdrtype == FLG_ZERO);
   horiz = (flttype == FLG_FHORIZ);
   vert = (flttype == FLG_FVERT);
   mag = (flttype == FLG_FMAG);

   if (mag) horiz = vert = TRUE;

   if ((tmp = alloc_image(im->type,im->size.x,im->size.y)) == NULL)
      goto exit;

   /* for each pixel in image do sobel operation */

   for( j = 1; j < (im->size.y - 1); j++ ) {
      for( i = 1; i < (im->size.x - 1); i++ ) {
	 switch (im->type) {
#ifdef DOBYTE
	  case IM_BYTE:
	    SOBEL_BYTE;
	    break;
#endif
#ifdef DOINTEGER
	  case IM_SHORT:
	    SOBEL_INT(SHORT,s);
	    break;

	  case IM_LONG:
	    SOBEL_INT(LONG,l);
	    break;
#endif
#ifdef DOREAL
	  case IM_FLOAT:
	    SOBEL_REAL(float,f);
	    break;

	  case IM_DOUBLE:
	    SOBEL_REAL(double,d);
	    break;
#endif
	 }
      }
   }

   /* Insert filtered image back into original image. */

   {
      coord org,size;

      org.x = org.y = 1;
      size.x = im->size.x - 1;
      size.y = im->size.y - 1;

      im_insert(im, org, tmp, org, size);
   }

   free_image(tmp);

   if (zero) {
      im_clear_border(im,1);
   }

 exit:
   pop_routine();
   return ip_error;
}


