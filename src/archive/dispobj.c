/* $Id$ */
/*
   Module: ip/dispobj.c
   Author: M.J.Cree

   Routines that should be in both display.c and object.c, but would create
   havoc if they were, so are in a separate module.

   � 1996-2001 Michael Cree 

ENTRY POINTS:

   display_object_over_image()
   display_objlist_over_image()   

*/

#include <ip/internal.h>
#include <ip/object.h>
#include <stdlib.h>


static char rcsid[] = "$Id$";

/*

NAME:

   display_objlist_over_image() \- Display an object list overlayed an image

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/object.h>

   int  display_objlist_over_image( dispid, srcim, ol, pen, flag )
   int dispid;
   image *srcim;
   objlist *ol;
   int pen;
   int flag;

DESCRIPTION:

   Display objects in objlist 'ol', overlayed the image 'srcim', in
   palette pen colour 'pen'.  'Flag' can be one of FLG_OPAQUE
   (objects opaque), FLG_TRANS (object transparent, but coloured),
   FLG_BRTTRANS (an attempt to make FLG_TRANS actually work usefully)
   or FLG_CLEAR (objects invisible).

   The display image ID 'dispid' may be FLG_NEWIMAGE or the ID to an
   image previously displayed.  It is as defined in display_imagex().
   The return value is the image ID returned from the display
   routines.

   This routine works best when the palette PAL_GREYOBJ is selected.

   No attempt is made to check that the objects are really inside the
   image boundaries. It is the callers responsibility to pass
   meaningful object lists that are valid for the image to be
   displayed.

ERRORS:

   ERR_OPEN_DISPLAY_FAIL
   ERR_OUT_OF_MEMORY
   ERR_DISPLAY_IMAGE

SEE ALSO:

   display_object_over_image()        display_image()

*/

int display_objlist_over_image(int id, image *srcim, objlist *ol, int pen, int flag)
{
   image *im;
   object *o;
   coord origin;
   int newid;

   log_routine("display_objlist_over_image");

   if (srcim->type != IM_BYTE) {
      log_error(ERR_NOT_IMPLEMENTED);
      goto exit_dloi;
   }

   if ((im = im_copy(srcim)) == NULL)
      goto exit_dloi;

   for (o=ol->firstobj; o; o=o->next) {
      if ((flag & 0x03) == FLG_OPAQUE) 
	 set_object_into_image(im,o,pen);
      else if ((flag & 0x03) == FLG_TRANS)
	 mask_object_into_image(im,o,0,127,pen-32,pen-1);
      else if ((flag & 0x03) == FLG_BRTTRANS)
	 mask_object_into_image(im,o,0,127,pen-16,pen-1);
      /* else flag is FLG_CLEAR so do nothing */
   }

   origin.x = origin.y = 0;
   newid = display_image(id, im,origin);

   free_image(im);

 exit_dloi:

   pop_routine();
   return newid;
}



/*

NAME:

   display_object_over_image() \- Display an object overlayed an image.

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/object.h>

   int  display_object_over_image( dispid, srcim, o, pen, flag )
   int dispid;
   image *srcim;
   object *o;
   int pen;
   int flag;

DESCRIPTION:

   Display an object overlayed an image. See
   display_objlist_over_image() for desciption of flags. This routine
   also takes the flags FLG_FULLIM (re-display full image) and
   FLG_OBJECT (redisplay only within object boundary box - can be
   quicker for large images). Or any opaqueness/transparency flags
   with the one of FLG_FULLIM or FLG_OBJECT for correct usage.

ERRORS:

   ERR_OPEN_DISPLAY_FAIL
   ERR_OUT_OF_MEMORY
   ERR_DISPLAY_IMAGE

SEE ALSO:

   display_objlist_over_image()
   display_image()                display_overlay()

*/

int display_object_over_image(int id, image *srcim, object *o, int pen, int flag)
{
   image *im,*tmp;
   coord origin;
   coord size;
   int newid;

   log_routine("display_object_over_image");

   if (srcim->type != IM_BYTE) {
      log_error(ERR_NOT_IMPLEMENTED);
      goto exit_dooi;
   }

   if ((im = im_copy(srcim)) == NULL)
      goto exit_dooi;

   if ((flag & 0x03) == FLG_OPAQUE) {
      set_object_into_image(im,o,pen);
   }else if ((flag & 0x03) == FLG_TRANS) {
      mask_object_into_image(im,o,0,127,pen-32,pen-1);
   }else if ((flag & 0x03) == FLG_BRTTRANS) {
      mask_object_into_image(im,o,0,127,pen-16,pen-1);
   }
   /* else flag is FLG_CLEAR so do nothing */

   if (flag & FLG_FULLIM) {
      origin.x = origin.y = 0;
      newid = display_image(id, im, origin);
   }else{
      size.x = o->bbox.br.x - o->bbox.tl.x + 1;
      size.y = o->bbox.br.y - o->bbox.tl.y + 1;
      origin = o->bbox.tl;
      tmp = im_cut(im,origin,size);
      display_overlay(id, tmp, origin, NO_FLAG);
      newid = id;
      free_image(tmp);
   }
   free_image(im);

 exit_dooi:

   pop_routine();

   if (ip_error != OK) return -1;
   else return newid;

}

