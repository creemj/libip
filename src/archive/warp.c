/* $Id$ */
/*

   Module: ip/warp.c
   Author: M.J.Cree

   To perform image warping.
   Can do up to 4th order 2d polynomial warps.

ROUTINES:

   im_warp()
   im_warpx()
   warp_to_poly2d()

*/

#include <stdlib.h>
#include <math.h>
#include <float.h>
#include <ip/internal.h>

static char rcsid[] = "$Id$";

static int imwarp(image *dest, image *src, poly2d *poly, int flag);
static void dointerpcoeffs(double *data, int N, 
			   double *poles, int Np, 
			   double gain, double threshold);

static void bspline3(double *r, double x);

/* #define DEBUG */
#include <debug/debug.h>


/*

NAME:

   im_warp() \- Warp an image

PROTOTYPE:

   #include <ip/ip.h>

   int  im_warp( im, poly, flag )
   image *im;
   poly2d *poly;
   int flag;

DESCRIPTION:

   Warps the image 'im' by the polynomial warping function 'poly'.  The
   warping is done in place on 'im'. (Actually an extra image buffer for the
   destination image is allocated and im_warpx() is called to the job.)

   'Flag' can take the values FLG_NEAREST for nearest-neighbour
   interpolation or FLG_BILINEAR for bilinear interpolation.or FLG_BSPLINE_3
   for bi-cubic interpolation with a B-spline order 3 interpolating kernel.
   Only BINARY, BYTE, SHORT, LONG, FLOAT and DOUBLE images are fully
   supported.

   See im_warpx() for further notes.

ERRORS:

   ERR_UNSUPPORTED_TYPE
   ERR_BAD_FLAG
   ERR_OUT_OF_MEM

SEE ALSO:

   alloc_poly2d()    warp_to_poly2d()   im_register()
   im_warpx()        ip_pointmap()

*/

int im_warp(image *im, poly2d *poly, int flag)
{
   image *dest;
   int err;

   log_routine("im_warp");

   dest = NULL;
   
   if ((dest = alloc_image(im->type,im->size.x,im->size.y)) == NULL) {
      pop_routine();
      return ip_error;
   }

   err = imwarp(dest, im, poly, flag);
   log_routine("im_warp");

   if (err == OK) {
      memcpy(im->image.v, dest->image.v,
	     (size_t)ip_typesize[im->type] * im->size.x * im->size.y);
   }

   if (dest)
      free_image(dest);

   if (err != OK) ip_error = err;

   pop_routine();
   return ip_error;
}



/*

NAME:

   im_warpx() \- Warp an image

PROTOTYPE:

   #include <ip/ip.h>

   int  im_warpx( dest, src, poly, flag )
   image *dest;
   image *src;
   poly2d *poly;
   int flag;

DESCRIPTION:

   Warps the image 'im' by the polynomial warping function 'poly'.  Returns
   result in image 'dest' (and 'im' is left in original state.)  Useful if
   destination image should be different size than source image.  Note that
   'dest' should be an allocated image of the same type as 'src'.

   'Flag' can take the values FLG_NEAREST for nearest-neighbour
   interpolation, FLG_BILINEAR for bilinear interpolation or FLG_BSPLINE_3
   for bi-cubic interpolation with a B-spline order 3 interpolating kernel.
   Only BINARY, BYTE, SHORT, LONG, FLOAT and DOUBLE images are fully
   supported.

   Note that the B-Spline interpolation allocates a DOUBLE image of the same
   size as 'src'.  If 'src' is a BYTE image that means im_warpx() with
   FLG_BSPLINE_3 requires eight times the memory of 'src' as a working
   buffer.  If any memory allocation fails then ERR_OUT_OF_MEM is flagged.
   It is safe to return from an error handler in this situation and
   im_warpx() will safely return without completing the warp operation.

   It can be useful to calculate the polynomial necessary for a rigid
   transformation (translation, scaling and rotation only) with the
   routine warp_to_poly2d(), or for a more general transformation with
   the ip_pointmap() function.

   The B-Spline interpolation is based upon:

   Thevenaz P, Blu T and Unser M (2000 ???)
     "Image Interpolation and Resampling"
     in 'The Handbook of Medical Imaging'  In press

   Unser M (1999)
     "Splines: A Perfect Fit for Signal and Image Processing"
     'IEEE Sig. Proc. Mag.'  Vol 16, Num 6, pp 22-38

   Unser M, Aldroubi A and Eden M (1993)
     "B-Spline Signal Processing: Part I---Theory"
     'IEEE Trans. Sig. Proc.'  Vol 41, pp 821-833

   Unser M, Aldroubi A and Eden M (1993)
     "B-Spline Signal Processing: Part 2---Efficient Design
      and Applications"
     'IEEE Trans. Sig. Proc.'  Vol 41, pp 834-848

ERRORS:

   ERR_OUT_OF_MEM
   ERR_NOT_SAME_TYPE
   ERR_UNSUPPORTED_TYPE

SEE ALSO:

   alloc_poly2d()    warp_to_poly2d()   im_register()
   im_warp()         ip_pointmap()

*/

int im_warpx(image *dest, image *src, poly2d *poly, int flag)
{
   log_routine("im_warpx");

   return imwarp(dest,src,poly,flag);
}



/*

NAME:

   warp_to_poly2d() \- Convert a warp specification into a 2D polynomial

PROTOTYPE:

   #include <ip/ip.h>

   poly2d  *warp_to_poly2d( warp, origin )
   regparms *warp;
   vector2d origin;

DESCRIPTION:

   Converts the rigid transformation description 'warp' (returned, for
   example, by im_register()) into a 1st-order polynomial description
   used by im_warp(). Note that the aforementioned routines treat the
   origin of the image as being at the centre of the image. Thus it is
   necessary to pass the location of the 'origin' of the image so that
   warp_to_poly1s() can do it''s calculation.

   Note that the registration routine considers the pixel location to
   be at the centre of the pixel.  This implies that the correct way
   to calculate half the image dimension is to subtract one of the
   image dimension before dividing by two.  This is to be calculated
   with floating-point arithmetic.  For example, origin should be
   set to 127.5x127.5 for a 256x256 sized image, and set to 127x127
   for a 255x255 image.

   The allocated 2D polynomial should be freed with free_poly2d().

ERRORS:

   ERR_OUT_OF_MEMORY

SEE ALSO:

   im_warp()  im_register()  alloc_poly2d()   free_poly2d()

*/

poly2d *warp_to_poly2d(regparms *w, vector2d o)
{
   poly2d *p;

   log_routine("warp_to_poly2d");

   if ((p = alloc_poly2d(2, 1)) == NULL) {
      pop_routine();
      return NULL;
   }

   /* Set up rotation/scaling matrix */

   p->X[1] = cos(-w->rotate) / w->scale;
   p->X[2] = -sin(-w->rotate) / w->scale;
   p->Y[1] = sin(-w->rotate) / w->scale;
   p->Y[2] = cos(-w->rotate) / w->scale;

   /* Now calculate translation parameters */

   p->X[0] = (-w->trans.x-o.x)*p->X[1] + (-w->trans.y-o.y)*p->X[2] + o.x;
   p->Y[0] = (-w->trans.x-o.x)*p->Y[1] + (-w->trans.y-o.y)*p->Y[2] + o.y;

   pop_routine();
   return p;
}



/*** Code for doing image warps follows ***/


#define IMROW_NEAREST(t,b)\
{\
   t *dp;\
   dp = im_rowadr(dest,j);\
   for (i=0; i<dsize.x; i++) {\
      int k,l,m,count;\
      if ((pt.x > -0.5) && (pt.x < rhs) && (pt.y > -0.5) && (pt.y < bhs)) {\
	 *dp++ = IM_PIXEL(src,(int)ROUND(pt.x),(int)ROUND(pt.y),b);\
      }else{\
	 *dp++ = 0;\
      }\
      count = 1;\
      for (k=0; k<poly->order; k++) {\
	 for (l=0,m=k; l<=k; l++,m--) {\
	    pt.x += poly->X[count]*D[m][i]*E[l];\
	    pt.y += poly->Y[count]*D[m][i]*E[l];\
	    count++;\
	 }\
	 count++;\
      }\
   }\
}

#define IMROW_BILINEAR_INT(t,b)\
{\
   t *dp;\
   dp = im_rowadr(dest,j);\
   for (i=0; i<dsize.x; i++) {\
      int k,l,m,count;\
      double kfrac,lfrac,e,f;\
      if ((pt.x > 0.0) && (pt.x < rhsm1) && (pt.y > 0.0) && (pt.y < bhsm1)) {\
	 k = (int)pt.x;\
	 l = (int)pt.y;\
	 kfrac = pt.x - k;\
	 lfrac = pt.y - l;\
	 e = (1.0-lfrac)*(double)IM_PIXEL(src,k,l,b)\
	           + lfrac*(double)IM_PIXEL(src,k,l+1,b);\
	 f = (1.0-lfrac)*(double)IM_PIXEL(src,k+1,l,b)\
	           + lfrac*(double)IM_PIXEL(src,k+1,l+1,b);\
	 *dp++ = (t)ROUND((1-kfrac)*e + kfrac*f);\
      }else if ((pt.x > -0.5) \
		       && (pt.x < rhs) && (pt.y > -0.5) && (pt.y < bhs)) {\
	 *dp++ = IM_PIXEL(src,(int)ROUND(pt.x),(int)ROUND(pt.y),b);\
      }else{\
	 *dp++ = 0;\
      }\
      count = 1;\
      for (k=0; k<poly->order; k++) {\
	 for (l=0,m=k; l<=k; l++,m--) {\
	    pt.x += poly->X[count]*D[m][i]*E[l];\
	    pt.y += poly->Y[count]*D[m][i]*E[l];\
	    count++;\
	 }\
	 count++;\
      }\
   }\
}

#define IMROW_BILINEAR_REAL(t,b)\
{\
   t *dp;\
   dp = im_rowadr(dest,j);\
   for (i=0; i<dsize.x; i++) {\
      int k,l,m,count;\
      double kfrac,lfrac,e,f;\
      if ((pt.x > 0.0) && (pt.x < rhsm1) && (pt.y > 0.0) && (pt.y < bhsm1)) {\
	 k = (int)pt.x;\
	 l = (int)pt.y;\
	 kfrac = pt.x - k;\
	 lfrac = pt.y - l;\
	 e = (1.0-lfrac)*(double)IM_PIXEL(src,k,l,b)\
	           + lfrac*(double)IM_PIXEL(src,k,l+1,b);\
	 f = (1.0-lfrac)*(double)IM_PIXEL(src,k+1,l,b)\
	           + lfrac*(double)IM_PIXEL(src,k+1,l+1,b);\
	 *dp++ = (t)((1-kfrac)*e + kfrac*f);\
      }else if ((pt.x > -0.5) \
		       && (pt.x < rhs) && (pt.y > -0.5) && (pt.y < bhs)) {\
	 *dp++ = IM_PIXEL(src,(int)ROUND(pt.x),(int)ROUND(pt.y),b);\
      }else{\
	 *dp++ = 0;\
      }\
      count = 1;\
      for (k=0; k<poly->order; k++) {\
	 for (l=0,m=k; l<=k; l++,m--) {\
	    pt.x += poly->X[count]*D[m][i]*E[l];\
	    pt.y += poly->Y[count]*D[m][i]*E[l];\
	    count++;\
	 }\
	 count++;\
      }\
   }\
}

#define IMROW_BSPLINE_INT(t,b,min,max)\
{\
   t *dp;\
   dp = im_rowadr(dest,j);\
   for (i=0; i<dsize.x; i++) {\
      int k,l,m,count;\
      if (flag == FLG_BSPLINE_3) {\
	 double e,f,g,h;\
	 double r[4];\
	 if ((pt.x > 1.0) && (pt.x < ssize.x-2) && \
	     (pt.y > 1.0) && (pt.y < ssize.y-2)) {\
	    k = (int)pt.x;\
	    l = (int)pt.y;\
	    bspline3(r,pt.x - k);\
	    l--;\
	    e = r[0]*IM_PIXEL(cf,k-1,l,d) + r[1]*IM_PIXEL(cf,k,l,d)\
	        + r[2]*IM_PIXEL(cf,k+1,l,d) + r[3]*IM_PIXEL(cf,k+2,l,d);\
	    l++;\
	    f = r[0]*IM_PIXEL(cf,k-1,l,d) + r[1]*IM_PIXEL(cf,k,l,d)\
	        + r[2]*IM_PIXEL(cf,k+1,l,d) + r[3]*IM_PIXEL(cf,k+2,l,d);\
	    l++;\
	    g = r[0]*IM_PIXEL(cf,k-1,l,d) + r[1]*IM_PIXEL(cf,k,l,d)\
	        + r[2]*IM_PIXEL(cf,k+1,l,d) + r[3]*IM_PIXEL(cf,k+2,l,d);\
	    l++;\
	    h = r[0]*IM_PIXEL(cf,k-1,l,d) + r[1]*IM_PIXEL(cf,k,l,d)\
	        + r[2]*IM_PIXEL(cf,k+1,l,d) + r[3]*IM_PIXEL(cf,k+2,l,d);\
	    l-=2;\
	    bspline3(r,pt.y - l);\
	    e = ROUND((1.0/36.0)*(r[0]*e + r[1]*f + r[2]*g + r[3]*h));\
            if (e > max) e = max;\
            if (e < min) e = min;\
            *dp++ = (t)e;\
	 }else if ((pt.x > 0.0) && (pt.x < rhsm1) \
                              && (pt.y > 0.0) && (pt.y < bhsm1)) {\
	    k = (int)pt.x;\
	    l = (int)pt.y;\
	    g = pt.x - k;\
	    h = pt.y - l;\
	    e = (1.0-h)*(double)IM_PIXEL(src,k,l,b)\
	              + h*(double)IM_PIXEL(src,k,l+1,b);\
	    f = (1.0-h)*(double)IM_PIXEL(src,k+1,l,b)\
	              + h*(double)IM_PIXEL(src,k+1,l+1,b);\
	    *dp++ = (t)ROUND((1-g)*e + g*f);\
         }else if ((pt.x > -0.5) \
		       && (pt.x < rhs) && (pt.y > -0.5) && (pt.y < bhs)) {\
	    *dp++ = IM_PIXEL(src,(int)ROUND(pt.x),(int)ROUND(pt.y),b);\
         }else{\
	    *dp++ = 0;\
	 }\
      }else{\
	*dp++ = 0;\
      }\
      count = 1;\
      for (k=0; k<poly->order; k++) {\
	 for (l=0,m=k; l<=k; l++,m--) {\
	    pt.x += poly->X[count]*D[m][i]*E[l];\
	    pt.y += poly->Y[count]*D[m][i]*E[l];\
	    count++;\
	 }\
	 count++;\
      }\
   }\
}


#define IMROW_BSPLINE_REAL(t,b)\
{\
   t *dp;\
   dp = im_rowadr(dest,j);\
   for (i=0; i<dsize.x; i++) {\
      int k,l,m,count;\
      if (flag == FLG_BSPLINE_3) {\
	 double e,f,g,h;\
	 double r[4];\
	 if ((pt.x > 1.0) && (pt.x < ssize.x-2) && \
	     (pt.y > 1.0) && (pt.y < ssize.y-2)) {\
	    k = (int)pt.x;\
	    l = (int)pt.y;\
	    bspline3(r,pt.x - k);\
	    l--;\
	    e = r[0]*IM_PIXEL(cf,k-1,l,d) + r[1]*IM_PIXEL(cf,k,l,d)\
	        + r[2]*IM_PIXEL(cf,k+1,l,d) + r[3]*IM_PIXEL(cf,k+2,l,d);\
	    l++;\
	    f = r[0]*IM_PIXEL(cf,k-1,l,d) + r[1]*IM_PIXEL(cf,k,l,d)\
	        + r[2]*IM_PIXEL(cf,k+1,l,d) + r[3]*IM_PIXEL(cf,k+2,l,d);\
	    l++;\
	    g = r[0]*IM_PIXEL(cf,k-1,l,d) + r[1]*IM_PIXEL(cf,k,l,d)\
	        + r[2]*IM_PIXEL(cf,k+1,l,d) + r[3]*IM_PIXEL(cf,k+2,l,d);\
	    l++;\
	    h = r[0]*IM_PIXEL(cf,k-1,l,d) + r[1]*IM_PIXEL(cf,k,l,d)\
	        + r[2]*IM_PIXEL(cf,k+1,l,d) + r[3]*IM_PIXEL(cf,k+2,l,d);\
	    l-=2;\
	    bspline3(r,pt.y - l);\
	    *dp++ = ROUND((1.0/36.0)*(r[0]*e + r[1]*f + r[2]*g + r[3]*h));\
	 }else if ((pt.x > 0.0) && (pt.x < rhsm1) \
                              && (pt.y > 0.0) && (pt.y < bhsm1)) {\
	    k = (int)pt.x;\
	    l = (int)pt.y;\
	    g = pt.x - k;\
	    h = pt.y - l;\
	    e = (1.0-h)*(double)IM_PIXEL(src,k,l,b)\
	              + h*(double)IM_PIXEL(src,k,l+1,b);\
	    f = (1.0-h)*(double)IM_PIXEL(src,k+1,l,b)\
	              + h*(double)IM_PIXEL(src,k+1,l+1,b);\
	    *dp++ = (t)ROUND((1-g)*e + g*f);\
         }else if ((pt.x > -0.5) \
		       && (pt.x < rhs) && (pt.y > -0.5) && (pt.y < bhs)) {\
	    *dp++ = IM_PIXEL(src,(int)ROUND(pt.x),(int)ROUND(pt.y),b);\
         }else{\
	    *dp++ = 0;\
	 }\
      }else{\
	*dp++ = 0;\
      }\
      count = 1;\
      for (k=0; k<poly->order; k++) {\
	 for (l=0,m=k; l<=k; l++,m--) {\
	    pt.x += poly->X[count]*D[m][i]*E[l];\
	    pt.y += poly->Y[count]*D[m][i]*E[l];\
	    count++;\
	 }\
	 count++;\
      }\
   }\
}




static int imwarp(image *dest, image *src, poly2d *poly, int flag)
{
   int j, err;
   coord ssize, dsize;
   double rhs, bhs;
   double rhsm1, bhsm1;
   double *D[4];
   double E[4];

   if (NOT images_same_type(dest, src)) {
      return ip_error;
   }

   if (NOT (image_type_integer(src) || image_type_real(src)
	     || src->type == IM_BINARY)) {
      log_error(ERR_UNSUPPORTED_TYPE);
      goto exit_wpx;
   }

   if (NOT flag_ok(flag, FLG_BILINEAR, FLG_NEAREST, FLG_BSPLINE_3, LASTFLAG))
      return ip_error;

   /* Ultimately should support FLG_BSPLINE_2,  FLG_BSPLINE_4
      and  FLG_BSPLINE_5 too. */

   ssize = src->size;
   dsize = dest->size;

   rhs = ssize.x - 0.5;
   bhs = ssize.y - 0.5;
   rhsm1 = ssize.x - 1.0;
   bhsm1 = ssize.y - 1.0;

   D[0] = D[1] = D[2] = D[3] = NULL;

   for (j=0; j<poly->order; j++) {
      if ((D[j] = ip_mallocx(sizeof(double) * dsize.x)) == NULL)
	goto exit_wpx;
   }
   if (poly->order > 0) {
      for (j=0; j<dsize.x; j++) {
	 D[0][j] = 1.0;
	 if (poly->order > 1) {
	    double x = j;
	    D[1][j] = 2*x + 1;
	    if (poly->order > 2) {
	       D[2][j] = (3*x+3)*x + 1;
	       if (poly->order > 3) {
		  D[3][j] = ((4*x+6)*x+4)*x + 1;
	       }
	    }
	 }
      }
   }

   if (flag == FLG_NEAREST) {

     for (j=0; j<dsize.y; j++) {
        int i;
	vector2d pt;
	E[0] = 1.0;
	for (i=1; i<poly->order; i++) {
	   E[i] = E[i-1] * (double)j;
	}
	pt = poly2d_evaluate(poly,0,j);
	switch(src->type) {
         case IM_BINARY:
         case IM_BYTE:
	 case IM_PALETTE:
	   IMROW_NEAREST(UBYTE,b);
	   break;
         case IM_SHORT:
	   IMROW_NEAREST(SHORT,s);
	   break;
	 case IM_LONG:
	   IMROW_NEAREST(LONG,l);
	   break;
         case IM_FLOAT:
	   IMROW_NEAREST(float,f);
	   break;
         case IM_DOUBLE:
	   IMROW_NEAREST(double,d);
	   break;
	}
     }

   }else if (flag == FLG_BILINEAR) {

     for (j=0; j<dsize.y; j++) {
        int i;
	vector2d pt;
	E[0] = 1.0;
	for (i=1; i<poly->order; i++) {
	   E[i] = E[i-1] * (double)j;
	}
	pt = poly2d_evaluate(poly,0,j);
	switch(src->type) {
         case IM_BINARY:
         case IM_BYTE:
	   IMROW_BILINEAR_INT(UBYTE,b);
	   break;
         case IM_SHORT:
	   IMROW_BILINEAR_INT(SHORT,s);
	   break;
	 case IM_LONG:
	   IMROW_BILINEAR_INT(LONG,l);
	   break;
         case IM_FLOAT:
	   IMROW_BILINEAR_REAL(float,f);
	   break;
         case IM_DOUBLE:
	   IMROW_BILINEAR_REAL(double,d);
	   break;
	}
     }

   }else{ /* Do B-Spline Order 2 up to 5 */
	  /* Use method due to Unser, Aldroubi and Eden.  */
      int order;		/* interp order */
      int Np;			/* num. poles */
      double poles[2];		/* poles */
      double gain;
      double tolerance;
      image *cf;		/* coeffs */

      order = flag - FLG_BSPLINE_0;
      tolerance = log10(FLT_EPSILON);
      Np = 0; gain = 0.0;	/* To shut gcc up */
      /* gains for other than order 3 probably incorrect */
      switch (order) {
       case 2:
	 Np = 1;
	 poles[0] = sqrt(8.0) - 3.0;
	 gain = 24.0 - sqrt(512.0);
	 break;
       case 3:
	 Np = 1;
	 poles[0] = sqrt(3.0) - 2.0;
	 gain = 6.0;
	 break;
       case 4:
	 Np = 2L;
	 poles[0] = sqrt(664.0 - sqrt(438976.0)) + sqrt(304.0) - 19.0;
	 poles[1] = sqrt(664.0 + sqrt(438976.0)) - sqrt(304.0) - 19.0;
	 gain = (1.0 - poles[0]) * (1.0 - poles[1]);
	 gain *= gain;
	 break;
       case 5:
	 Np = 2L;
	 poles[0] = 0.5 * (sqrt(270.0 - sqrt(70980.0)) + sqrt(105.0) - 13.0);
	 poles[1] = 0.5 * (sqrt(270.0 + sqrt(70980.0)) - sqrt(105.0) - 13.0);
	 gain = (1.0 - poles[0]) * (1.0 - poles[1]);
	 gain *= gain;
	 break;
      }

      if ((cf = im_convert(src, IM_DOUBLE, NO_FLAG)) == NULL) {
	 goto exit_wpx;
      }

      /* Filter each column of image */

      if (ssize.x > 1) {
	 for (j=0; j<ssize.y; j++) {
	    dointerpcoeffs(im_rowadr(cf,j),ssize.x,poles,Np,gain,tolerance);
	 }
      }

      /* Filter each row of image */
      
      if (ssize.y > 1) {
	 double *data, *dp;
	 int i;

	 if ((data = ip_malloc(sizeof(double)*ssize.y)) == NULL) {
	    free_image(cf);
	    goto exit_wpx;
	 }

	 for (i=0; i<ssize.x; i++) {
	    dp = data;
	    for (j=0; j<ssize.y; j++) 
	       *dp++ = IM_PIXEL(cf,i,j,d);
	    dointerpcoeffs(data,ssize.y,poles,Np,gain,tolerance);
	    dp = data;
	    for (j=0; j<ssize.y; j++) 
	       IM_PIXEL(cf,i,j,d) = *dp++;
	 }
	 ip_free(data);
      }

      for (j=0; j<dsize.y; j++) {
         int i;
	 vector2d pt;
	 E[0] = 1.0;
	 for (i=1; i<poly->order; i++) {
	    E[i] = E[i-1] * (double)j;
	 }
	 pt = poly2d_evaluate(poly,0,j);
	 switch(src->type) {
          case IM_BINARY:
          case IM_BYTE:
	    IMROW_BSPLINE_INT(UBYTE,b,0,255);
	    break;
          case IM_SHORT:
	    IMROW_BSPLINE_INT(SHORT,s,SHRT_MINV,SHRT_MAXV);
	    break;
	  case IM_LONG:
	    IMROW_BSPLINE_INT(LONG,l,LONG_MINV,LONG_MAXV);
	    break;
          case IM_FLOAT:
	    IMROW_BSPLINE_REAL(float,f);
	    break;
          case IM_DOUBLE:
	    IMROW_BSPLINE_REAL(double,d);
	    break;
	 }
      }
      free_image(cf);

   }

   if (dest->type==IM_BINARY && flag!=FLG_NEAREST) {
      UBYTE *pixptr;
      int i;

      for (j=0; j<dest->size.y; j++) {
	 pixptr = im_rowadr(dest, j);
	 for (i=0; i<dest->size.x; i++) {
	    *pixptr = *pixptr>=128 ? 255 : 0;
	    pixptr++;
	 }
      }
   }

 exit_wpx:

   err = ip_error;
   
   for (j=0; j<poly->order; j++) {
      if (D[j]) ip_free(D[j]);
   }

   if (err != OK) ip_error = err;

   pop_routine();
   return ip_error;;
}


/*
   Recursive prefiltering for B-spline interpolation.
   Must be applied to columns then rows of image before
   doing interpolation.
   See. Thevenaz, et al., 2000.
*/
static void dointerpcoeffs(double *data, int N, 
			   double *poles, int Np, 
			   double gain, double tolerance)
{
   int i,j,k;
   double *dp;
   double pole;
   double zn, sum, iz, z2n;

   if (N == 1) return;

   dp = data;
   for (i=0; i<N; i++)
      *dp++ *= gain;
   
   for (k=0; k<Np; k++) {
      pole = *poles++;
      j = (int)ceil(tolerance/log10(fabs(pole)));
      if (j < Np) {
	 zn = pole;
	 sum = data[0];
	 dp = data+1;
	 for (i=1; i<j; i++) {
	    sum += zn * *dp++;
	    zn *= pole;
	 }
      }else{
	 zn = pole;
	 iz = 1 / pole;
	 z2n = pow(pole, N-1);
	 sum = data[0] + z2n * data[N-1];
	 z2n = z2n * z2n * iz;
	 dp = data+1;
	 for (i=1; i<=N-2; i++) {
	    sum += (zn + z2n) * *dp++;
	    zn *= pole;
	    z2n *= iz;
	 }
	 sum /= (1.0 - zn*zn);
      }
      dp = data;
      *dp++ = sum;
      for (i=1; i<N; i++) {
	 *dp += pole * *(dp-1);
	 dp++;
      }
      dp = data + N - 1;
      *dp-- = (pole/(pole*pole-1.0)) * (pole*data[N-2] + data[N-1]);
      for (i=N-2; i>=0; i--) {
	 *dp = pole * (dp[1] - *dp);
	 dp--;
      }
   }
}


/*
   Calculate B-spline order 3 at x-(-1), x, x-1 and x-2.
   B-spline order 3 is given by

             2/3 - 1/2 |x^2| (2 - |x|)    0<=|x|<1
   B3(x) =   1/6 (2 - |x|)^3              1<=|x|<2
             0                            otherwise

   Note that we calculate the above multiplied by 6 and correct the factor
   of 6 elsewhere.  Also x on entry to bspline3 is always in the range
   0<=x<1.
*/
static void bspline3(double *r, double x)
{
#if 1
  /* This is faster code. */
   double x2, t;

   x2 = x*x;
   r[3] = x2*x;			/* 6*B3(x-2)    = x^3 */
   t = 1+x;
   r[2] = t*t*t - 4*r[3];	/* 6*B3(x-1)    = -4x^3 + (1+x)^3 */
   r[1] = 4 - 6*x2 + 3*r[3];	/* 6*B3(x)      = 3x^3 - 6x^2 + 4 */
   t = 1-x;
   *r = t*t*t;			/* 6*B3(x-(-1)) = (1-x)^3  */
   return;
#else
   int k;
   double xx;

   if (x <0.0 || x >= 1.0) {
      fprintf(stderr,"bspline: x = %g.\n",x);
   }

   for (k=-1; k<=2; k++) {
      xx = fabs(x - (double)k);
      if (xx < 1.0)
	 r[k+1] = 4.0 - 3.0 * xx * xx * (2 - xx);
      else if (xx < 2.0)
	 r[k+1] = (2 - xx) * (2 - xx) * (2 - xx);
      else
	 r[k+1] = 0.0;
   }
#endif
}


