/* $Id$ */
/*
   Module: ip/trackobj.c
   Author: M.J.Cree

   Code to do detection of objects in binary image.
*/



#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include <ip/internal.h>
#include <ip/intobject.h>

/* #define DEBUG */
#include <debug/debug.h>

static char rcsid[] = "$Id$";


/* Chain-code like moves, note they must be modulo 8. The definitions listed
   below are the 'traditional' ones. */

#define MV_RIGHT 0
#define MV_UPRIGHT 1
#define MV_UP 2
#define MV_UPLEFT 3
#define MV_LEFT 4
#define MV_DOWNLEFT 5
#define MV_DOWN 6
#define MV_DOWNRIGHT 7

/* Returned by next_move() when pixel has no neighbours */

#define NO_MOVE -1

/* Tests on a move for general directions. NB these def'ns rely an traditional
   values for MV_* as above. If you change MV_* values above then these def'ns
   may need to be adjusted accordingly. */

#define DOWN(x) ((x) >= MV_DOWNLEFT && (x) <= MV_DOWNRIGHT)
#define UP(x) ((x) >= MV_UPRIGHT && (x) <= MV_UPLEFT)
#define UPRIGHT(x) ((x) == MV_UP || (x) == MV_UPRIGHT)
#define DOWNLEFT(x) ((x) == MV_DOWN || (x) == MV_DOWNLEFT)
#define DOWNORRIGHT(x) (DOWN(x) || (x) == MV_RIGHT)
#define UPORLEFT(x) ((x) >= MV_UPRIGHT && (x) <= MV_LEFT)
#define LEFT(x) ((x) == MV_LEFT)
#define NOTLEFT(x) ((x) != MV_LEFT)
#define RIGHT(x) ((x) == MV_RIGHT)
#define NOTRIGHT(x) ((x) != MV_RIGHT)



/* Internal entry points */

static int reg_start(object *, coord);
static int reg_end(object *, coord);
static int reg_startend(object *, coord);
static int reg_any(image *im, object *, coord);

static coord move_pt(coord, int);
static int next_move(image *, coord, int);





/*

NAME:

   im_create_object_list() \- Create object list from image

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/object.h>

   objlist * im_create_object_list( im )
   image *im;

DESCRIPTION:

   Create a complete object list containing every object in the BYTE
   or BINARY image 'im'.  An object is defined to be an
   eight-connected region of non-zero pixels. This routine uses
   track_object() to create each object.

   Note that the image data in image 'im' is overwritten in the
   process.

   If an error condition arises then NULL is returned and the error is
   flagged in the normal way.

   See track_object().

LIMITATIONS:

   You must have an error handler installed when using this routine.
   It is not safe to return from the error handler, that is, the error
   handler must call exit() (or it''s equivalent).

ERRORS:

   ERR_OUT_OF_MEM

   The following errors should never occur, but if they do, it
   indicates some bug in the track_object() code:

   ERR_OBJ_INVALID_ROW
   ERR_RLE_ARRAY_OVERFLOW

SEE ALSO:

   track_object()    alloc_object()    alloc_object_list()

*/

objlist *im_create_object_list(image *im)
{
   coord pt;
   objlist *olist;
   object *obj;

   log_routine("im_create_object_list");

   if (im->type != IM_BYTE && im->type != IM_BINARY) {
      log_error(ERR_BAD_TYPE);
      pop_routine();
      return NULL;
   }

   if ((olist = alloc_object_list(im))) {
      for (pt.y=0; pt.y<im->size.y; pt.y++) {
	 for (pt.x=0; pt.x<im->size.x; pt.x++) {
	    if (IM_PIXEL(im, pt.x, pt.y, b)) {
	       if ((obj = track_object(im,pt))) {
		  append_object(olist,obj);
		  set_object_into_image(im,obj,0);
	       }else{
		  free_object_list(olist);
		  pop_routine();
		  return NULL;
	       }
	    }
	 }
      }
   }
   pop_routine();
   return olist;
}




/*

NAME:

   track_object() \- Track object in image

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/object.h>

   static object * track_object( im, origin )
   image *im;
   coord origin;

DESCRIPTION:

   This routine tracks the object boundary, starting from the 'origin'
   (which MUST be the top-left point of the object, that is, the first
   pixel encountered in the object when you scan the image
   left-to-right then top-to-bottom). An object is an eight-connected
   region of non-zero pixels, within the BYTE or BINARY image. It
   doesn''t locate any holes that may be in the object, thus the object
   returned has the holes filled in and an Euler number of 1.

   This routine is rather restricted. It only works with BYTE or
   BINARY images, MUST be seeded with only one particular point on the
   object, and doesn''t find holes in the object. For a more general
   object creation routine see region_grow().

   I keep this routine in the library (despite the superiority of
   region_grow()) because it would only take minor modifications for it to
   be able to create chain-code representations of object boundaries. If
   chain-codes are ever needed, most of the code to generate them is already
   present here.

   If an error condition should occur then NULL is returned.

LIMITATIONS:

   You must have an error handler installed when using this routine.
   It is not safe to return from the error handler, that is, the error
   handler must call exit() (or it''s equivalent).

ERRORS:

   ERR_OUT_OF_MEM

   The following errors should never occur, but if they do, it
   indicates some bug in the track_object() code:

   ERR_OBJ_INVALID_ROW
   ERR_RLE_ARRAY_OVERFLOW

SEE ALSO:

   im_create_object_list()     alloc_object()

*/

object *track_object(image *im, coord origin)
{
   object *obj;
   coord point;
   int move,prevmove,firstmove;

   log_routine("track_object");

   D(bug("Entered track_object with origin (%d,%d)\n",origin.x,origin.y));

   /* Allocate an object in list */

   if ((obj = alloc_object(origin)) == NULL) {
      pop_routine();
      return NULL;
   }

   SET_RUNSTOP(obj);		/* Run-stop encoding is easier here */

   D(bug("Allocated object\n"));

   /* Find first move from start point (and save for future reference) */

   move = next_move(im,origin,MV_DOWNRIGHT);
   firstmove = move;

   /* If no move then have one-pixel object */

   if (move == NO_MOVE) {
      reg_startend(obj, origin);
      obj->bbox.br.x = obj->bbox.tl.x = origin.x;
      obj->bbox.br.y = obj->bbox.tl.y = origin.y;
      obj_complete(obj);
      pop_routine();
      return obj;
   }

   /* Register start point at origin and move to next pixel */

   D(bug("Many pixel object, move = %d\n",move));
   if (reg_any(im, obj, origin) != OK)
      return NULL;
   D(bug("Registered origin point.\n"));

   prevmove = move;
   point = move_pt(origin,move);
   move = next_move(im,point,prevmove);

   D(bug("Did first move, now at (%d,%d), new move = %d\n",point.x,point.y,move));

   if (point.x < obj->bbox.tl.x)
      obj->bbox.tl.x = point.x;
   if (point.x > obj->bbox.br.x)
      obj->bbox.br.x = point.x;
   if (point.y > obj->bbox.br.y)
      obj->bbox.br.y = point.y;


   /* And continue to track around object until back at start */

   do {
      D(bug("Do loop: (%d,%d) prevmove = %d, move = %d\n",point.x,point.y,prevmove,move));
      /* Register rle start or end points */

      if ((DOWN(prevmove) && DOWNORRIGHT(move))
			|| (LEFT(prevmove) && DOWNORRIGHT(move))) {
	 if (reg_start(obj,point) != OK) return NULL;
      } else if ((UP(prevmove) && UPORLEFT(move))
			|| (RIGHT(prevmove) && UPORLEFT(move))) {
	 if (reg_end(obj,point) != OK) return NULL;
      } else if ((DOWN(prevmove) && UPRIGHT(move))
			|| (UP(prevmove) && DOWNLEFT(move))) {
	 if (reg_startend(obj,point) != OK) return NULL;
      } else if (NOT ((LEFT(prevmove) && UPORLEFT(move))
			|| (RIGHT(prevmove) && DOWNORRIGHT(move))
		 	|| (prevmove==MV_DOWNLEFT && move==MV_UPLEFT)
		 	|| (prevmove==MV_UPRIGHT && move==MV_DOWNRIGHT)
			|| (DOWNLEFT(prevmove) && LEFT(move))
			|| (UPRIGHT(prevmove) && RIGHT(move)))) {
	 if (reg_any(im,obj,point) != OK) return NULL;
      }

      /* Move to next point on boundary */

      prevmove = move;
      point = move_pt(point,move);
      move = next_move(im,point,prevmove);
      
      /* Update bounding box */

      if (point.x < obj->bbox.tl.x)
	 obj->bbox.tl.x = point.x;
      if (point.x > obj->bbox.br.x)
	 obj->bbox.br.x = point.x;
      if (point.y > obj->bbox.br.y)
	 obj->bbox.br.y = point.y;

      /* until we get back to origin with a move the same as firstmove */
      
   } while ((point.x != origin.x || point.y != origin.y) || move != firstmove);

D(bug("Done the loop.\n"));

   /* Do general tidy up of object structure (particularly of rle encoding) */

   obj_complete(obj);

   pop_routine();
   return obj;
}



/* Move a point to next point by move */
static coord move_pt(coord pt, int move)
{
   coord newpt;

   switch (move) {
    case MV_RIGHT:
      newpt.x = pt.x+1;
      newpt.y = pt.y;
      break;
    case MV_UPRIGHT:
      newpt.x = pt.x+1;
      newpt.y = pt.y-1;
      break;
    case MV_UP:
      newpt.x = pt.x;
      newpt.y = pt.y-1;
      break;
    case MV_UPLEFT:
      newpt.x = pt.x-1;
      newpt.y = pt.y-1;
      break;
    case MV_LEFT:
      newpt.x = pt.x-1;
      newpt.y = pt.y;
      break;
    case MV_DOWNLEFT:
      newpt.x = pt.x-1;
      newpt.y = pt.y+1;
      break;
    case MV_DOWN:
      newpt.x = pt.x;
      newpt.y = pt.y+1;
      break;
    case MV_DOWNRIGHT:
      newpt.x = pt.x+1;
      newpt.y = pt.y+1;
      break;
   }
   return newpt;
}



static int next_move(image *im, coord pt, int prevmove)
{
   int move,nextmove,val,stop;
   coord npt;

   nextmove = prevmove - 2;
   if (nextmove < 0) nextmove += 8;
   stop = nextmove-1;
   if (stop < 0) stop += 8;
   val = 0;
   do {
      move = nextmove;
      npt = move_pt(pt,move);
      val = 0;
      if (npt.x >= 0 && npt.y >= 0 && npt.x < im->size.x && npt.y < im->size.y) {
	 val = IM_PIXEL(im, npt.x, npt.y, b);
      }
      nextmove = move + 1;
      if (nextmove >= 8) nextmove -= 8;
   } while (val == 0 && move != stop);

   if (val)
      return move;
   else
      return NO_MOVE;
}




static int reg_start(object *obj, coord pt_im)
{
   coord pt;
   rowtype *row;
   rletype *rle;
   int i;

   /* Convert to object coordinates */

   pt.x = pt_im.x - obj->origin.x;
   pt.y = pt_im.y - obj->origin.y;

   D(bug("  reg_start at (%d,%d) object (%d,%d)\n",pt_im.x,pt_im.y,pt.x,pt.y));

   /* Search rle array for correct position for this start position */

   row = obj_get_row(obj,pt.y);	/* Note that it is safe to do one insertion now */
   rle = row->rle;

   for (i=0; i < row->numentries && pt.x > rle[i].length; i++)
      if (rle[i].length == UNENDED && pt.x < rle[i].start) break;

   if (i == row->numentries)
      obj_insert_run_entry(row,i,pt.x,UNENDED);
   else if (rle[i].start == UNSTARTED)
      rle[i].start = pt.x;
   else if (pt.x > rle[i].start && pt.x <= rle[i].length) {
      obj_insert_run_entry(row,i,rle[i].start,UNENDED);
      rle[i+1].start = pt.x;
   }else
      obj_insert_run_entry(row,i,pt.x,UNENDED);

   D(bug("  done reg.\n"));

   return OK;
}



static int reg_end(object *obj, coord pt_im)
{
   coord pt;
   rowtype *row;
   rletype *rle;
   int i;

   /* Convert to object coordinates */

   pt.x = pt_im.x - obj->origin.x;
   pt.y = pt_im.y - obj->origin.y;
D(bug("  reg_end at (%d,%d) object (%d,%d)\n",pt_im.x,pt_im.y,pt.x,pt.y));

   row = obj_get_row(obj,pt.y);

   /* Search list to find where this entry goes */

   rle = row->rle;
   for (i=row->numentries-1;
	i >= 0 && ((rle[i].start != UNSTARTED && pt.x < rle[i].start) 
		   || (rle[i].start == UNSTARTED)); i--) {
      if (rle[i].start == UNSTARTED && pt.x > rle[i].length) break;
   }

   if (i < 0)
      obj_insert_run_entry(row,0,UNSTARTED,pt.x);
   else if (rle[i].length == UNENDED) {
      rle[i].length = pt.x;
      D(bug("    matched %d to %d at pos %d\n",pt.x,rle[i].start,i));
   }
   else if (rle[i].start <= pt.x && rle[i].length > pt.x) {
      obj_insert_run_entry(row,i+1,UNSTARTED,rle[i].length);
      rle[i].length = pt.x;
   }else
      obj_insert_run_entry(row,i+1,UNSTARTED,pt.x);
   D(bug("  done reg.\n"));

   return OK;
}



static int reg_startend(object *obj, coord pt_im)
{
   coord pt;
   rowtype *row;
   rletype *rle;
   int i;

   /* Convert to object coordinates */

   pt.x = pt_im.x - obj->origin.x;
   pt.y = pt_im.y - obj->origin.y;
   D(bug("  reg_startend at (%d,%d) object (%d,%d)\n",pt_im.x,pt_im.y,pt.x,pt.y));

   row = obj_get_row(obj,pt.y);

   /* Search list to find where this entry goes */

   rle = row->rle;
   for (i=0; i < row->numentries && pt.x > rle[i].length; i++)
      if (rle[i].length == UNENDED && pt.x < rle[i].start) break;

   if (i == row->numentries)
      obj_insert_run_entry(row,i,pt.x,pt.x);
   else if (pt.x > rle[i].start && pt.x < rle[i].length) {
      obj_insert_run_entry(row,i,pt.x,pt.x);
      obj_insert_run_entry(row,i,rle[i+1].start,UNENDED);
      rle[i+2].start = UNSTARTED;
   }else
      obj_insert_run_entry(row,i,pt.x,pt.x);

   D(bug("  done reg.\n"));
   
   return OK;
}



static int reg_any(image *im,object *obj, coord pt)
{
   UBYTE left,right;

   /* Get pixel values to left and right of current point */

   if (pt.x > 0)
      left = IM_PIXEL(im, pt.x-1, pt.y, b);
   else
      left = 0;
   if (pt.x < im->size.x-1)
      right = IM_PIXEL(im, pt.x+1, pt.y, b);
   else
      right = 0;

   /* Do start/end registration depending on pixel values */

   if (left == 0 && right == 0)
      return reg_startend(obj,pt);
   if (left == 0 && right != 0)
      return reg_start(obj,pt);
   if (left !=0 && right == 0)
      return reg_end(obj,pt);

   return OK;
}



