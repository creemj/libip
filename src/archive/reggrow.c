/* $Id$ */
/*
   Module: ip/reggrow.c
   Author: M.J.Cree

   Code related to region growing to produce objects.

*/
/*
 * $Log$
 * Revision 1.10  1997/07/21 15:17:29  mph489
 * Added full declaration prototypes for in_region().
 *
 * Revision 1.9  1997/06/16  10:04:11  mph489
 * Fixed up function comments for mjc_com2man formatting.
 * Fixed bug in row_grow() (wrong comparision on image size).
 *
 * Revision 1.8  1997/05/23  13:15:19  mph489
 * Added necessary include files to function documentation.
 *
 * Revision 1.7  1997/03/04  10:27:49  mph489
 * Changed all references to Nx and Ny to size.
 * Fixed routine description comment to use IM_PIXEL() in example.
 * Fixed logging of region_grow() routine name.
 *
 * Revision 1.6  1996/11/05  11:20:17  mph489
 * Added some extra debugging stuff which is which on when
 * IP_DEBUG defined.
 *
 * Revision 1.5  1996/06/11  10:44:52  mph489
 * Deleted debug code (now accessed through ip_malloc(), etc.).
 *
 * Revision 1.4  1995/11/27  12:27:14  mph489
 *  Now must define IP_DEBUG to get debugging stuff.
 *
 * Revision 1.3  1995/11/22  10:16:56  mph489
 * Deleted a couple of unused variables.
 *
 * Revision 1.2  1995/10/13  12:57:17  mph489
 * Reorganised object row/rle allocation/completion mechanism.
 * Changed row_grow() to use obj_get_row() properly.
 * Modified region_grow() to only return object - doesn't append to list.
 * Documented main entry point.
 *
 * Revision 1.1  1995/09/29  16:07:19  mph489
 * Initial revision
 *
 */



#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include <ip/internal.h>
#include <ip/intobject.h>

static char rcsid[] = "$Id$";


static int row_grow(image *im, object *obj, int (*in_region)(image *i, coord pt), coord seed);

#ifdef IP_DEBUG
/* #define DEBUG */
#include <debug/debug.h>
static int numc;
static int maxc;
#endif


/*

NAME:

   region_grow() \- Region grow on an image to produce an object

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/object.h>

   object * region_grow( im, in_region(), seed )
   image *im;
   int (*in_region)(image *, coord);
   coord seed;

DESCRIPTION:
   
   Use region growing, starting at the point 'seed', on the image
   'im', to form the eight-connected region which is returned as an
   object. The criterion for determining if any one pixel is in the
   region, is determined by the function in_region(). In_region() is
   passed the image pointer and a coordinate of a point, that is to be
   tested to see if it satisfies the criteria for being in the
   region. If it is in the region, then in_region() should return
   TRUE, else it should return FALSE.

   The function in_region() provides region_grow() with much
   generality. Any image type can be used, and any fanciful criterion
   on the image can be used (as long as it can be defined in terms of
   the image and the point to be tested).

   If 'seed' fails the condition to be in the region, then
   region_grow() returns a NULL pointer. No error condition is
   flagged.

   If an error occurs, then a NULL pointer is returned and the error
   is flagged.

   Note that to check the returned pointer for NULL, is insufficient
   to check exclusively for a bad seed point or exclusively for an
   error condition.  An error handler can be used to distinguish the
   two cases. For example, an error handler that doesn''t return, and
   exits the programme, would guarantee that on return the NULL
   pointer means a bad seed point.

   An example of in_region() is (this is almost identical to
   track_object(), except holes in the object are detected):

\|   int in_region(image *im, coord pt)
\|   {
\|      // Assume BYTE image.
\|      // pt is in region if pixel non-zero.
\|      return (IM_PIXEL(im, pt.x, pt.y, b) != 0);
\|   }

   A count variable could be put into in_region() to test for region
   grow spills. But be aware that in_region() may be called more than
   once for a particular pixel, thus the count wouldn''t give an exact
   determination of the regions size.

LIMITATIONS:

   This routine is implemented as a recursive call.  It is possible to
   overflow the programme stack and cause segmentation violations with
   this routine if you grow excessively large regions.

ERRORS:
   ERR_OUT_OF_MEM

   The following errors should never occur, but if they do, it
   indicates some bug in the region_grow() code:

   ERR_OBJ_INVALID_ROW
   ERR_RLE_ARRAY_OVERFLOW

SEE ALSO:

   free_object()

*/

object *region_grow(image *im, 
		    int (*in_region)(image *i,coord pt),
		    coord seed)
{
   coord origin;
   object *obj;

   log_routine("region_grow");

   /* Protect region growing from being seeded from invalid point. */

   if (NOT in_region(im,seed)) {
      pop_routine();
      return NULL;
   }

   /* Allocate object structure with origin at bottom right of image.
      Row_grow() will dynamically shift it back to correct poisition.  Also
      make object coordinates identical to image coords.
   */

   origin.x = im->size.x;
   origin.y = im->size.y;
   if ((obj = alloc_object(origin)) == NULL) {
      pop_routine();
      return NULL;
   }

   SET_IMCOORD(obj);

   /* Do region growing starting from seed */

#ifdef IP_DEBUG
   numc = maxc = 0;
#endif

   if (row_grow(im,obj,in_region,seed) != OK) {
      free_object(obj);
      pop_routine();
      return NULL;
   }

#ifdef IP_DEBUG
   D(bug("Region growing: maxc = %d\n",maxc));
#endif

   /* Do tidy up work on object structure */

   obj_complete(obj);

   pop_routine();
   return obj;
}



/*
   Region grow one row of object, and recursively call to region grow
   neighbouring rows.
*/
static int row_grow(image *im, object *obj, 
		    int (*in_region)(image *i, coord pt),
		    coord seed)
{
   int start,end;
   int tmp;
   coord pt;

#ifdef IP_DEBUG
   /* Update call counts */

   numc++;
   if (numc > maxc) {
      D(bug("%3d,%c",numc, " \n"[(numc%16)==0]));
      maxc = numc;
   }
#endif      

   /* Scan row from seed finding start and end points of row inside region */

   pt.y = seed.y;
   for (pt.x = seed.x-1; pt.x >= 0 && in_region(im,pt); pt.x--)
      ;
   start = pt.x + 1;
   for (pt.x = seed.x+1; pt.x < im->size.x && in_region(im,pt); pt.x++)
      ;
   end = pt.x-1;

   /* Update bounding box info and origin coordinates */

   if (obj->bbox.tl.y > seed.y) obj->bbox.tl.y = seed.y;
   if (obj->bbox.br.y < seed.y) obj->bbox.br.y = seed.y;
   if (obj->bbox.tl.x > start) obj->bbox.tl.x = start;
   if (obj->bbox.br.x < end) obj->bbox.br.x = end;
   if (obj->origin.y > seed.y) {
      obj->origin.y = seed.y;
      obj->origin.x = start;
   }
   if (obj->origin.y == seed.y && obj->origin.x > start) {
      obj->origin.x = start;
   }

   /* Insert run into object describing the row found */

   obj_insert_run(obj,pt.y,start,end-start+1);

   /* Now scan row above and row below to see if we should seed any
      new rows to grow */

   if (seed.y > 0) {
      pt.y = seed.y - 1;		/* Line above */
      if (start == 0) start++;		/* Protect against going off image edge */
      if (end == im->size.x-1) end--;
      for (pt.x = start-1; pt.x < end+1; ) { /* Scan line */
	 for ( ; pt.x <= end+1 && NOT in_region(im,pt); pt.x++)
	    ;				     /* Skip points not in region */
	 if (pt.x <= end+1 && obj_get_run_end(obj,pt.y,pt.x) < 0) {
	    row_grow(im,obj,in_region,pt);   /* Got point in region and not grown before */
	    tmp = obj_get_run_end(obj,pt.y,pt.x)+1; /* so grow run, and skip past it */
	    if (tmp > 0) pt.x = tmp;
	 }else if (pt.x < end+1) {
	    pt.x = obj_get_run_end(obj,pt.y,pt.x)+1; /* Run already grown before, just skip past */
	 }
      }
   }
   if (seed.y < im->size.y-1) {
      pt.y = seed.y + 1;		/* Line below */
      if (start == 0) start++;
      if (end == im->size.x-1) end--;
      for (pt.x = start-1; pt.x < end+1; ) {
	 for ( ; pt.x <= end+1 && NOT in_region(im,pt); pt.x++)
	    ;
	 if (pt.x <= end+1 && obj_get_run_end(obj,pt.y,pt.x) < 0) {
	    row_grow(im,obj,in_region,pt);
	    tmp = obj_get_run_end(obj,pt.y,pt.x)+1;
	    if (tmp > 0) pt.x = tmp;
	 }else if (pt.x < end+1) {
	    pt.x = obj_get_run_end(obj,pt.y,pt.x)+1;
	 }
      }
   }
   return OK;
}

