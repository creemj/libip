/* $Id$ */
/*
   Header: ip/infostr.h
   Author: M.J.Cree

   Header file for infostr.c information string management

*/


#ifndef IP_INFOSTR_H
#define IP_INFOSTR_H

#include <ip/types.h>

/* Structure returned by alloc_infostr() and used in all other infostr
   routines.  Treat this as an abstract data type.  You may not look
   at nor modify any field in this structure.  It is private to the
   infostr routines. */

typedef struct {
   int id;
   int flag;
   coord origin;
   coord size;
   coord charsize;
   int ptsize;
   int skip;
   int baseline;
   char *str;
   UBYTE lightpen;
   UBYTE sdwpen;
   UBYTE bgpen;
   UBYTE textpen;
} infostr;


/* Actions to use in infostr_get() and infostr_set() */

enum {
   IFS_STRING,
   IFS_SIZE,
   IFS_CHARSIZE,
   IFS_ORIGIN,
   IFS_TEXTPEN,
   IFS_LIGHTPEN,
   IFS_SDWPEN,
   IFS_BGPEN,
   IFS_OPTIMUMPAL
};

/* Allocation/deallocation/display update */
extern infostr *alloc_infostr(coord origin, int width, int lines, int ptsize);
extern int infostr_display(infostr *ifs);
extern int free_infostr(infostr *ifs);

/* Setting/Getting of parameters */
extern datum infostr_get(infostr *ifs, int action);
extern int infostr_set(infostr *ifs, int action, ...);

/* Getting input from user */
extern char *infostr_getstr(infostr *ifs, char *msg);

#define infostr_string(i,s)  infostr_set(i,IFS_STRING,s)

/* For debugging purposes */
extern void dump_infostr(infostr *ifs);

#endif
