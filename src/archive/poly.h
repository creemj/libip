/* $Id$ */
/*
   Header: ip/poly.h
   Author: M.J.Cree

*/


#ifndef IP_POLY_H
#define IP_POLY_H

#include <ip/types.h>
#include <ip/image.h>

/* One-dimensional (range) polynomials */

extern poly *alloc_poly(int dimension, int order);
extern void free_poly(poly *p);

extern int poly_numcoeffs(poly *);
extern int poly_numcoeffsx(int dimension, int order);
extern int poly_add(poly *, poly *);


/* 'restict' is written 'restrict_' below because it appears to be a
   reserved keyword in some compilers. */
extern poly *poly_fit1d(int npoints, double pts[], double val[],
	 double origin, int order, unsigned long restrict_, double *chisq);

extern poly *poly_fit2d(int npoints, vector2d pts[], double val[],
	 vector2d origin, int order, unsigned long restrict_, double *chisq);

extern double poly_evaluate(poly *p, double x, double y);

/* Two-dimensional (range) polynomials */


extern poly2d *alloc_poly2d(int dimension, int order);
extern void free_poly2d(poly2d *p);
extern int poly2d_numcoeffs(poly2d *p);
extern vector2d poly2d_evaluate(poly2d *p, double x, double y);

/* Generate polynomial from matching control points */

extern poly2d * ip_pointmap(int npoints, vector2d *refpts, vector2d *objpts, 
			    int order,  vector2d *chisq );


#endif
