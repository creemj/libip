/* $Id$ */
/*
   Module: scansplit.c
   Author: M.J.Cree

   Split scanned image (interlaced) into odd and even lines.

*/
/*
 * $Log$
 * Revision 1.1  1996/12/10 10:09:07  mph489
 * Initial revision
 *
 */


#include <stdlib.h>
#include <ip/internal.h>

static char rcsid[] = "$Id$";


image *im_oddlines(image *im, int flag)
{
   image *odd;
   int j;
   int skip;
   
   log_routine("im_oddlines");

   odd = NULL;

   if (NOT flag_ok(flag, FLG_DOUBLE,LASTFLAG))
      goto exit_ol;

   skip = flag == FLG_DOUBLE ? im->size.y : im->size.y/2;

   if ((odd = alloc_image(im->type, im->size.x, skip)) == NULL)
      goto exit_ol;

   skip = im->size.x * ip_typesize[im->type];

   if (flag == FLG_DOUBLE) {
      for (j=0; j<im->size.y/2; j++) {
	 memcpy(&odd->image.b[(2*j)*skip],
		&im->image.b[(2*j+1)*skip],
		skip);
	 memcpy(&odd->image.b[(2*j+1)*skip],
		&im->image.b[(2*j+1)*skip],
		skip);
      }
   }else{
      for (j=0; j<im->size.y/2; j++) {
	 memcpy(&odd->image.b[j*skip],
		&im->image.b[(2*j+1)*skip],
		skip);
      }
   }

   if (im->type == IM_PALETTE)
      im_palette_copy(odd,im);

 exit_ol:

   pop_routine();
   return odd;
}




image *im_evenlines(image *im, int flag)
{
   image *even;
   int j;
   int skip;
   
   log_routine("im_evenlines");

   even = NULL;

   if (NOT flag_ok(flag, FLG_DOUBLE, LASTFLAG))
      goto exit_el;

   skip = flag == FLG_DOUBLE ? im->size.y : im->size.y/2;

   if ((even = alloc_image(im->type, im->size.x, skip)) == NULL)
      goto exit_el;

   skip = im->size.x * ip_typesize[im->type];

   if (flag == FLG_DOUBLE) {
      for (j=0; j<im->size.y/2; j++) {
	 memcpy(&even->image.b[(2*j)*skip],
		&im->image.b[(2*j)*skip],
		skip);
	 memcpy(&even->image.b[(2*j+1)*skip],
		&im->image.b[(2*j)*skip],
		skip);
      }
   }else{
      for (j=0; j<im->size.y/2; j++) {
	 memcpy(&even->image.b[j*skip],
		&im->image.b[(2*j)*skip],
		skip);
      }
   }

   if (im->type == IM_PALETTE)
      im_palette_copy(even,im);
 
 exit_el:

   pop_routine();
   return even;
}
