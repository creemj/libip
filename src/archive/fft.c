/* $Id$ */
/*
   Module: ip/fft.c
   Author: M.J.Cree

   Fast Fourier transform images.

   � 1995-2005 Michael Cree 

   This code depends on the fftw library of FFT routines.  You must link
   this with both the single precision (float) and double precision versions
   of fftw, normally with the options -lfftw3 and -lfftw3f.  (This is, of
   course, already in the Makefile for the IP library).

ENTRY POINTS:

   im_fftshift()
   im_fft()

*/


#include <math.h>
#include <ip/internal.h>
#include <fftw3.h>

static char rcsid[] = "$Id$";


/* Defn's used by im_fftshift */

#define FFTSHIFT(type,b) { \
   int i,j; \
   type *topquad, *botquad; \
   type tmp; \
   for (j=0; j<y2; j++) { \
      topquad = im_rowadr(im, j); \
      botquad = im_rowadr(im, j+y2); \
      for (i=0; i<x2; i++) { \
	 tmp = *topquad; \
         *topquad = *(botquad+x2); \
	 *(botquad+x2) = tmp; \
	 tmp = *botquad; \
         *botquad = *(topquad+x2); \
	 *(topquad+x2) = tmp; \
	 topquad++; botquad++; \
      } \
   } \
}

/*

NAME:

   im_fft() \- Fast (Discrete) Fourier Transform a COMPLEX image

PROTOTYPE:

   #include <ip/ip.h>

   int  im_fft( im, flag )
   image *im;
   int flag;

DESCRIPTION:

   Perform a two-dimensional discrete Fourier tranform on the image 'im'
   with the FFT algorithm.  The image dimensions need not be a power of 2,
   although the maximum efficiency (NlogN) is only achieved if the image
   dimensions are a power of 2.  The image need not be a square image.
   'Flag' can be set to 1 for a forward FFT and -1 for an inverse FFT. (This
   sets the sign of the exponent of the complex kernel of the FFT).  No
   normalisation is done in these two cases.  Alternatively, the flags
   FLG_INVERSE and FLG_NORM can be used.  FLG_NORM specifies that the result
   should be normalised, that is, after applying the FFT the same total
   energy is contained in the image and then after applying an inverse FFT,
   again the same total energy is contained in the image.  If FLG_NORM is
   not used, then after an FFT then an inverse FFT, the resulting image is
   multiplied by size.x * size.y (the total number of pixels in the image).
   FLG_INVERSE specifies that an inverse FFT should be done.  FLG_NORM and
   FLG_INVERSE can be ORed together.

   Only COMPLEX image types (both single and double precision) are
   supported.  The origins in image space and in Fourier space are taken as
   (0,0).  The Fourier transform is defined so that the exponent of the
   kernel contains a factor of 2pi.

ERRORS:

   ERR_UNSUPPORTED_TYPE
   ERR_OUT_OF_MEM	    (If can't allocated FFTW plan)

*/

int im_fft(image *im, int flag)
{
   int arsize;
   register int i;
   int isign, normalise;
   float divisor;
   register float *fp;
   int plan_num;

   log_routine("im_fft");

   if (NOT (im->type == IM_COMPLEX)) {
      log_error(ERR_UNSUPPORTED_TYPE);
      goto exit_fft;
   }

   if (flag == -1) {		/* For backwards compatability */
      isign = -1;
      plan_num = 1;
      normalise = FALSE;
   }else if (flag == 1) {
      isign = 1;
      plan_num = 0;
      normalise = FALSE;
   }else{			/* New method of specifying operation */
      if (flag & FLG_INVERSE) {
	 isign = -1;
	 plan_num = 1;
      }else{
	 isign = 1;
	 plan_num = 0;
      }
      normalise = (flag & FLG_NORM);
   }

   /* Allocate plan for FFT if no plan exists or if a plan exists but is not
      compatible in size with image and in direction of DFT. */

   if (im->fftw_plans[plan_num] == NULL) {

      if (im->type == IM_COMPLEX) {
	 im->fftw_plans[plan_num] 
	    = (void *)fftwf_plan_dft_2d(im->size.x, im->size.y,
				       (fftwf_complex *)im->image.v,
				       (fftwf_complex *)im->image.v,
				       isign, FFTW_ESTIMATE);
      }else{
	 im->fftw_plans[plan_num] 
	    = (void *)fftw_plan_dft_2d(im->size.x, im->size.y,
				       (fftw_complex *)im->image.v,
				       (fftw_complex *)im->image.v,
				       isign, FFTW_ESTIMATE);
      }

      if (im->fftw_plans[plan_num] == NULL) {
	 log_error(ERR_OUT_OF_MEM);
	 pop_routine();
	 return ip_error;
      }
   }

   /* Do FFT */

   if (im->type == IM_COMPLEX)
      fftwf_execute(im->fftw_plans[plan_num]);
   else
      fftw_execute(im->fftw_plans[plan_num]);

   /* Normalise if requested (only on inverse FFT) */

   if (normalise && isign == -1) {
      arsize = 2 * im->size.x * im->size.y;
      divisor = im->size.x * im->size.y;
      fp = im->image.v;
      for (i=0; i<arsize; i++)
	 *fp++ /= divisor;
   }

 exit_fft:
   pop_routine();
   return ip_error;
}


/*

NAME:

   im_fftshift() \- Shift origin of image for FFT

PROTOTYPE:

   #include <ip/ip.h>

   int  im_fftshift( im )
   image *im;

DESCRIPTION:

   Shifts origin of image from centre of image to top-left corner.
   Effectively swaps the four quandrants of the image.

SEE ALSO:

   im_fft()

*/

int im_fftshift(image *im)
{
   int x2, y2;

   log_routine("im_fft");

   if (NOT ip_parm_mult2("image_size_x", im->size.x))
      return ip_error;
   if (NOT ip_parm_mult2("image_size_y", im->size.y))
      return ip_error;

   x2 = im->size.x / 2;
   y2 = im->size.y / 2;

   switch (im->type) {
   case IM_BINARY:
   case IM_BYTE:
   case IM_PALETTE:
     FFTSHIFT(UBYTE,b);
     break;

#ifdef DOINTEGER
   case IM_SHORT:
     FFTSHIFT(SHORT,s);
     break;
         
   case IM_LONG:
     FFTSHIFT(LONG,l);
     break;
#endif

#ifdef DOREAL
   case IM_FLOAT:
     FFTSHIFT(float,f);
     break;

   case IM_DOUBLE:
     FFTSHIFT(double,d);
     break;
#endif

#ifdef DOCOMPLEX
   case IM_COMPLEX:
     FFTSHIFT(complex,c);
     break;
#endif

   case IM_RGB:
     FFTSHIFT(colour,r);
     break;

   }
   pop_routine();
   return OK;
}


