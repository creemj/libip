/* $Id$ */
/*
   Module: ip/mathfun.c
   Author: J.H.Hipwell  (im_sqrt)
           M.J.Cree     (others)

   Mathematical functions on images.

ENTRY POINTS:

   im_sqrt()
   im_log()

*/

#include <stdlib.h>

#include <ip/internal.h>
#include <math.h>

static char rcsid[] = "$Id$";


/*

NAME:

   im_sqrt() \- Calculate the square root of pixels in an image

PROTOTYPE:

   #include <ip/ip.h>

   int  im_sqrt( im, flag )
   image *im;
   int flag;

DESCRIPTION:

   Calculate the square root pixel by pixel of image 'im'. The
   operation is performed in place and hence the original contents of
   'im' are lost. The type of the image is unchanged and hence
   rounding error occurs for integer images.

   A maths exception error is flagged for non-complex image types if the
   square root of a negative number is requested.  It is safe to return from
   the error handler, in which case the pixel is set to 0 for integer type
   images and NaN for floating-point types.  The error is only reported for
   the first offending pixel encountered.

   For complex images the square root of a negative number enforces a branch
   cut along the negative real axis.

   'flag' is not currently used and should be set to NO_FLAG.

LIMITATIONS:

   The function is currently defined for IM_BYTE, IM_SHORT,
   IM_LONG, IM_FLOAT, IM_DOUBLE and IM_COMPLEX.
   
ERRORS:

   ERR_BAD_TYPE
   ERR_BAD_FLAG
   ERR_MATHS_EXCEPTION

SEE ALSO:

   im_add()     im_sub()     im_mul()     im_div()
   im_addc()    im_subc()    im_mulc()    im_divc()
   im_log()

AUTHOR:

\|   John Hipwell (j.hipwell@biomed.abdn.ac.uk)
\|   Michael Cree (m.cree@biomed.abdn.ac.uk)

*/

#define IMROW_SQRT_UBYTE {\
   UBYTE *ip;\
   ip = im_rowadr(im,j);\
   for (i=0; i<size.x; i++) {\
      *ip = (UBYTE) sqrt((double)*ip);\
      ip++;\
   }\
}

#define IMROW_SQRT(t) {\
   t *ip;\
   ip = im_rowadr(im,j);\
   for (i=0; i<size.x; i++) {\
      if (*ip > 0) {\
	 *ip = (t) sqrt((double)*ip);\
	 ip++;\
      }else{\
	 if (NOT reported) {\
	    log_error(ERR_MATHS_EXCEPTION, "Negative square root\n");\
	    reported = TRUE;\
	 }\
	 *ip++ = 0;\
      }\
   }\
}

int im_sqrt(image *im, int flag)
{
   int i,j;
   coord size;
   int reported;

   log_routine("im_sqrt");

   if (image_bad_type(im, IM_PALETTE, IM_RGB, IM_BINARY, -1))
      return ip_error;

   if (NOT flag_ok(flag, LASTFLAG)) return ip_error;

   reported = FALSE;
   size = im->size;

   for (j=0; j<size.y; j++) {
 
      switch (im->type) {

#ifdef DOBYTE
       case IM_BYTE:
	 IMROW_SQRT_UBYTE
	 break;
#endif

#ifdef DOINTEGER
       case IM_SHORT:
	 IMROW_SQRT(SHORT)
	 break;
       case IM_LONG:
	 IMROW_SQRT(LONG)
	 break;
#endif

#ifdef DOREAL
       case IM_FLOAT:
	 IMROW_SQRT(float);
	 break;
       case IM_DOUBLE:
	 IMROW_SQRT(double);
	 break;
#endif

#ifdef DOCOMPLEX
       case IM_COMPLEX: {
	 complex *ptr;
	 float w;
	 ptr = im_rowadr(im,j);
	 for (i=0; i<size.x; i++) {
	    if (ptr->r == 0.0 && ptr->i == 0.0) {
	       w = 0.0;
	    }else if (fabs(ptr->r) >= fabs(ptr->i)) {
	       w = ptr->i / ptr->r;
	       w = sqrt(fabs(ptr->r)*(1.0+sqrt(1.0+w*w))/2.0);
	    }else{
	       w = ptr->r / ptr->i;
	       w = sqrt(fabs(ptr->i)*(fabs(w)+sqrt(1.0+w*w))/2.0);
	    }
	    if (w == 0.0) {
	       ptr->r = ptr->i = 0.0;
	    }else if (ptr->r >= 0.0) {
	       ptr->r = w;
	       ptr->i /= 2.0*w;
	    }else if (ptr->i >= 0.0) {
	       ptr->r = ptr->i / (2.0*w);
	       ptr->i = w;
	    }else{
	       ptr->r = -ptr->i / (2.0*w);
	       ptr->i = -w;
	    }
	    ptr++;
	 }
         }break;
#endif

      }
   }

   pop_routine();
   return ip_error;
}


#undef IMROW_SQRT_UBYTE
#undef IMROW_SQRT

/*

NAME:

   im_log() \- Calculate the natural logarithm of pixels in an image

PROTOTYPE:

   #include <ip/ip.h>

   int  im_log( im, flag )
   image *im;
   int flag;

DESCRIPTION:

   Calculate the natural logarithm pixel by pixel of image 'im'. The
   operation is performed in place and hence the original contents of 'im'
   are lost. The type of the image is unchanged and hence rounding error
   (and huge loss of significance) occurs for integer images.

   A maths exception error is flagged if the logarithm of a negative number
   or zero is requested.  It is safe to return from the error handler. If
   the offence is log(0) then the pixel is set to 0 for BYTE images, the
   smallest possible integer for other integer types and -Inf for
   floating-point types.  If the offence is log(negative number) then the
   pixel is set to zero in all integer types and to NaN in floating-point
   types.  The maths exception error is only reported for the first
   offending pixel encountered.

   'flag' is not currently used and hence should be set to NO_FLAG.

LIMITATIONS:

   Not implemented for COMPLEX images.

ERRORS:

   ERR_BAD_TYPE
   ERR_BAD_FLAG
   ERR_MATHS_EXCEPTION

SEE ALSO:

   im_add()     im_sub()     im_mul()     im_div()
   im_addc()    im_subc()    im_mulc()    im_divc()
   im_sqrt()

AUTHOR:

\|   Michael Cree (m.cree@ieee.org)

*/

#define IMROW_LOG_UBYTE {\
   UBYTE *ip;\
   ip = im_rowadr(im,j);\
   for (i=0; i<size.x; i++) {\
      if (*ip > 0) {\
	 *ip = (UBYTE) log((double)*ip);\
	 ip++;\
      }else{\
	 if (NOT reported) {\
	    log_error(ERR_MATHS_EXCEPTION, "Zero logarithm\n");\
	    reported = TRUE;\
	 }\
	 *ip++ = 0;\
      }\
   }\
}

#define IMROW_LOG(t) {\
   t *ip;\
   ip = im_rowadr(im,j);\
   for (i=0; i<size.x; i++) {\
      if (*ip > 0) {\
	 *ip = (t) log((double)*ip);\
	 ip++;\
      }else{\
	 if (NOT reported) {\
	    if (*ip == 0)\
	       log_error(ERR_MATHS_EXCEPTION, "Zero logarithmm\n");\
            else\
	       log_error(ERR_MATHS_EXCEPTION, "Negative logarithm\n");\
	    reported = TRUE;\
	 }\
	 *ip++ = 0;\
      }\
   }\
}

int im_log(image *im, int flag)
{
   int i,j;
   coord size;
   int reported;

   log_routine("im_log");

   if (image_bad_type(im, IM_PALETTE, IM_RGB, IM_BINARY, IM_COMPLEX, -1))
      return ip_error;

   if (NOT flag_ok(flag, LASTFLAG)) return ip_error;

   reported = FALSE;
   size = im->size;

   for (j=0; j<size.y; j++) {
 
      switch (im->type) {

#ifdef DOBYTE
       case IM_BYTE:
	 IMROW_LOG_UBYTE
	 break;
#endif

#ifdef DOINTEGER
       case IM_SHORT:
	 IMROW_LOG(SHORT)
	 break;
       case IM_LONG:
	 IMROW_LOG(LONG)
	 break;
#endif

#ifdef DOREAL
       case IM_FLOAT:
	 IMROW_LOG(float);
	 break;
       case IM_DOUBLE:
	 IMROW_LOG(double);
	 break;
#endif

      }
   }

   pop_routine();
   return ip_error;
}



