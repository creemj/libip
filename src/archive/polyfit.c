/* $Id$ */
/*
   Module: ip/polyfit.c
   Author: M.J.Cree

DESCRIPTION:

   Least-squares fit of polynomial to data.

   Requires the GNU Scientific Library to perform the fitting.

*/


#include <stdlib.h>
#include <math.h>

#include <ip/internal.h>

#include <gsl/gsl_fit.h>
#include <gsl/gsl_multifit.h>

static char rcsid[] = "$Id$";





/*

NAME:

   poly_fit1d() \- Fit a one-dimensional polynomial to data

PROTOTYPE:

   #include <ip/ip.h>

   poly * poly_fit1d( npoints, pts[], val[], origin, order, restrict, chisq )
   int npoints;
   double pts[];
   double val[];
   double origin;
   int order;
   unsigned long restrict;
   double *chisq;

DESCRIPTION:

   Fit a one-dimensional polynomial expanded around 'origin' to 'npoints'
   datapoints at the locations 'pts' (on the x-axis) which evaluate to 'val'
   (on the y-axis).  Returns a one-dimensional polynomial of specified
   'order' that best approximates the given 'pts' and 'val' arrays in a
   less-squared sense.  Goodness of fit returned in 'chisq'.  If any errors
   occurs, NULL is returned (and chisq will not be valid).  The parameter
   'restrict' can be used to allow only certain terms of the polynomial to
   be used in the fitting of data.  It is a bitmask with the lowest order
   bit corresponding to the lowest order term of the polynomial (which is
   the constant term).  If a bit is set then the corresponding term in the
   polynomial is not used in the regression.  For normal operation set
   'restrict' to zero.

   The GNU Scientific Library is used to perform the polynomial fitting.
   The modified Golub-Reinsch SVD algorithm is used.

ERRORS:

   ERR_OUT_OF_MEM
   ERR_OOR_PARAMETER
   ERR_BAD_PARAMETER
   ERR_GSLLIB

SEE ALSO:

   poly_fit2d()    poly_evaluate()    free_poly()

*/

/* Modified restrict to restrictz since a reserved keyword in some
   compilers. */

poly *poly_fit1d(int npoints, double pts[], double val[], double origin,
		 int order, unsigned long restrictz, 
		 double *chisq)
{
   int polypts, ncoeffs;
   unsigned long tt;
   int i,j;
   double tchisq;
   poly *p;
   gsl_multifit_linear_workspace *mlw;
   gsl_vector *y, *c;
   gsl_matrix *X, *cov;
      

   log_routine("poly_fit1d");

   p = NULL;
   restrictz = ~restrictz;

   if (NOT ip_parm_inrange("poly_order", order, 1, sizeof(unsigned long)*8-2))
      return NULL;

   if ((polypts = poly_numcoeffsx(1, order)) == -1) {
      goto exit_pf;
   }

   if (NOT ip_parm_greatereq("number_points",npoints,polypts))
      return NULL;

   if ((p = alloc_poly(1, order)) == NULL)
      goto exit_pf;

   ncoeffs = 0;
   tt = restrictz;
   for (i=0; i<polypts; i++, tt>>=1) {
      if (tt & 0x01)
	 ncoeffs++;
   }

   register_gsl_error_handler();
   X = gsl_matrix_alloc(npoints, ncoeffs);
   y = gsl_vector_alloc(npoints);
   c = gsl_vector_alloc(ncoeffs);
   cov = gsl_matrix_alloc(ncoeffs,ncoeffs);

   for (i=0; i<npoints; i++) {
      double xup;

      tt = restrictz;
      j = 0;
      if (tt & 0x01)
	 gsl_matrix_set(X,i,j++,1);
      tt >>= 1;
      if (order > 0) {
	 xup = (pts[i] - origin);
	 for (; j<ncoeffs; tt >>= 1) {
	    if (tt & 0x01)
	       gsl_matrix_set(X,i,j++,xup);
	    xup *= (pts[i] - origin);
	 }
      }
      gsl_vector_set(y, i, val[i]);
   }

   mlw = gsl_multifit_linear_alloc(npoints, ncoeffs);
   gsl_multifit_linear(X, y, c, cov, &tchisq, mlw);

   tt = restrictz;
   j = 0;
   for (i=0; i<polypts; i++, tt >>= 1) {
      if (tt & 0x01) {
	 p->X[i] = gsl_vector_get(c, j++);
      }else{
	 p->X[i] = 0.0;
      }
   }
   p->origin.x = origin;

   if (chisq != NULL) *chisq = tchisq;

   gsl_multifit_linear_free(mlw);

   gsl_matrix_free(cov);
   gsl_vector_free(c);
   gsl_vector_free(y);
   gsl_matrix_free(X);

 exit_pf:

   if (ip_error != OK) {
      free_poly(p);
      p = NULL;
   }

   pop_routine();
   return p;
}




/*

NAME:

   poly_fit2d() \- Fit a two-dimensional polynomial to data

PROTOTYPE:

   #include <ip/ip.h>

   poly * poly_fit2d( npoints, pts[], val[], origin, order, restrict, chisq )
   int npoints;
   vector2d pts[];
   double val[];
   vector2d origin;
   int order;
   unsigned long restrict;
   double *chisq;

DESCRIPTION:

   Fit a two-dimensional polynomial expanded around 'origin' to 'npoints'
   datapoints in array 'pts' (the x,y values) which evaluate to 'val' (the z
   values).  Returns a two-dimensional polynomial of specified 'order' that
   best approximates the given points and values in a less-squared sense.
   Goodness of fit returned in 'chisq'.  If any errors occurs, NULL is
   returned (and 'chisq' will not be valid). The parameter 'restrict' can be
   used to allow only certain terms of the polynomial to be used in the
   fitting of data.  It is a bitmask with the lowest order bit corresponding
   to the lowest order term of the polynomial (which is the constant term).
   If a bit is set then the corresponding term in the polynomial is not used
   in the regression.  For normal operation set 'restrict' to zero.

   The GNU Scientific Library is used to perform the polynomial fitting.
   The modified Golub-Reinsch SVD algorithm is used.

ERRORS:

   ERR_OUT_OF_MEM
   ERR_OOR_PARAMETER
   ERR_BAD_PARAMETER
   EER_GSLLIB

SEE ALSO:

   poly_fit1d()    poly_evaluate()    free_poly()

*/


poly *poly_fit2d(int npoints, vector2d pts[], double val[], vector2d origin,
		 int order, unsigned long restrictz, double *chisq)
{
   int polypts, ncoeffs;
   unsigned long tt;
   int i,j;
   double tchisq;
   poly *p;
   gsl_multifit_linear_workspace *mlw;
   gsl_vector *y, *c;
   gsl_matrix *X, *cov;

   log_routine("ip_polyfit2d");

   p = NULL;
   restrictz = ~restrictz;

   /* Max order given by number of coefficients expressable in restrictz */
   tchisq = -1.5 + sqrt(0.25 + 2*(sizeof(unsigned long)*8-2));
   tchisq = floor(tchisq);
   if (NOT ip_parm_inrange("poly_order", order, 1, tchisq))
      return NULL;

   if ((polypts = poly_numcoeffsx(2, order)) == -1) {
      goto exit_pf;
   }

   if (NOT ip_parm_greatereq("number_points",npoints,polypts))
      return NULL;

   if ((p = alloc_poly(2, order)) == NULL)
      goto exit_pf;

   ncoeffs = 0;
   tt = restrictz;
   for (i=0; i<polypts; i++, tt>>=1) {
      if (tt & 0x01)
	 ncoeffs++;
   }

   register_gsl_error_handler();
   X = gsl_matrix_alloc(npoints, ncoeffs);
   y = gsl_vector_alloc(npoints);
   c = gsl_vector_alloc(ncoeffs);
   cov = gsl_matrix_alloc(ncoeffs,ncoeffs);

   /* Set up X and y here */

   for (i=0; i<npoints; i++) {
      int k,l;
      double xpt, ypt, xup,yup;
      double *arow;

      arow = ip_malloc(sizeof(double)*tchisq);
      tt = restrictz;
      j = 0;
      if (tt & 0x01)
	 gsl_matrix_set(X,i,j++,1);
      tt >>= 1;
      if (order > 0) {
	 arow[0] = xpt = pts[i].x - origin.x;
	 arow[1] = ypt = pts[i].y - origin.y;
	 if (tt & 0x01)
	    gsl_matrix_set(X,i,j++,xpt);
	 tt >>= 1;
	 if (tt & 0x01)
	    gsl_matrix_set(X,i,j++,ypt);
	 tt >>= 1;
	 for (k=2; k <= order; k++) {
	    arow[k] = arow[k-1] * ypt;
	    for (l=0; l<k; l++) {
	       arow[l] *= xpt;
	       if (tt & 0x01)
		  gsl_matrix_set(X,i,j++,arow[l]);
	       tt >>= 1;
	    }
	    if (tt & 0x01)
	       gsl_matrix_set(X,i,j++,arow[k]);
	    tt >>= 1;
	 }
      }
      gsl_vector_set(y, i, val[i]);
      ip_free(arow);
   }

   mlw = gsl_multifit_linear_alloc(npoints, ncoeffs);
   gsl_multifit_linear(X, y, c, cov, &tchisq, mlw);

   tt = restrictz;
   j = 0;
   for (i=0; i<polypts; i++, tt >>= 1) {
      if (tt & 0x01) {
	 p->X[i] = gsl_vector_get(c, j++);
      }else{
	 p->X[i] = 0.0;
      }
   }
   p->origin = origin;

   if (chisq != NULL) *chisq = tchisq;

   gsl_multifit_linear_free(mlw);

   gsl_matrix_free(cov);
   gsl_vector_free(c);
   gsl_vector_free(y);
   gsl_matrix_free(X);

 exit_pf:

   if (ip_error != OK) {
      free_poly(p);
      p = NULL;
   }

   pop_routine();
   return p;
}



/*

NAME:

   ip_pointmap()

PROTOTYPE:

   #include <ip/ip.h>

   poly2d * ip_pointmap( npoints, refpts, objpts, order, chisq )
   int npoints;
   vector2d *refpts;
   vector2d *objpts;
   int order;
   vector2d *chisq;

DESCRIPTION:

   Function 'ip_pointmap' generates the 2D polynomial of order 'order' required
   to transform the array of 'npoints' 2D vectors 'objpts' onto
   'refpts'.

   The function uses the GNU scientific library linear multifit routine
   which in turn uses LU decomposition to obtain a least-squares fit. At
   least (order+1)*(order+2)/2 vector pairs ('npoints') are required.

   The polynomial is returned as a poly2d object such that the coefficients
   pX and pY evaluate as follows:

\|                                             order
\|   xt =   pX[0]			         0 [but see below]
\|        + pX[1].x     + pX[2].y	         1
\|        + pX[3].x.x   + pX[4].x.y   + pX5.y.y  2
\|        + pX[6].x.x.x + pX[7].x.x.y .....      3
\|
\|   yt =   pY[0]			         0 [but see below]
\|        + pY[1].x     + pY[2].y	         1
\|        + pY[3].x.x   + pY[4].x.y   + pY5.y.y  2
\|        + pY[6].x.x.x + pY[7].x.x.y .....      3

   If the parameter 'chisq' is non-null then the chi-square error of the fit
   is returned in 'chisq' (one value each for the X polynomial fit and the Y
   polynomial fit).

   If the 'order' is set to zero then a special case transformation of
   translation only is evaluated.  A 1st order polynomial is returned.

   This routine is useful for generating a polynomial image transformation
   specification for use in im_warp().

SEE ALSO:

   im_warp()                    im_warpx()

*/

poly2d * ip_pointmap(int npoints, vector2d *refpts, vector2d *objpts, int order, 
		     vector2d *chisq )
{
   poly2d *p;				/* the polynomial coefficients */
   int i;

   log_routine("ip_pointmap");

   /* Special case of zero order transformation ie. translation only */
   /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

   if (order == 0) {
      p = alloc_poly2d(2, 1);

      p->X[1] = 1.;
      p->Y[2] = 1.;

      p->X[0] = 0.;
      p->Y[0] = 0.;
      for (i=0; i<npoints; i++) {
	 p->X[0] += refpts[i].x - objpts[i].x;
	 p->Y[0] += refpts[i].y - objpts[i].y;
      }
      p->X[0] /= (double) npoints;
      p->Y[0] /= (double) npoints;

      /* Calculate sum of squared residuals if asked for */

      if (chisq) {
	 chisq->x = chisq->y = 0;
	 for (i=0; i<npoints; i++) {
	    vector2d tv;
	    tv = poly2d_evaluate(p,refpts[i].x, refpts[i].y);
	    tv.x -= objpts[i].x;
	    chisq->x += (tv.x * tv.x);
	    tv.y -= objpts[i].y;
	    chisq->y += (tv.y * tv.y);
	 }
      }

   }

   /* Initialise variables */
   /* ~~~~~~~~~~~~~~~~~~~~ */

   else {
      int n2;
      int j,k,l;
      double tchisq;
      gsl_multifit_linear_workspace *mlw;
      gsl_vector *yx, *yy, *c;
      gsl_matrix *X, *cov;
      


      n2 = poly_numcoeffsx(2, order);

      if (NOT ip_parm_greatereq("num_points",npoints,n2)) {
	 return NULL;
      }
      
      p = alloc_poly2d(2, order);
      if (p == NULL) {
	 pop_routine();
	 return NULL;
      }

      X = gsl_matrix_alloc(npoints, n2);
      yx = gsl_vector_alloc(npoints);
      yy = gsl_vector_alloc(npoints);
      c = gsl_vector_alloc(n2);
      cov = gsl_matrix_alloc(n2,n2);

      for (i=0; i<npoints; i++) {
	 gsl_matrix_set(X, i, 0, 1.0);
	 gsl_matrix_set(X, i, 1, objpts[i].x);
	 gsl_matrix_set(X, i, 2, objpts[i].y);
	 k = 3;
	 for (j=2; j<=order; j++) {
	    for (l=0; l<j; l++) {
	       gsl_matrix_set(X, i, k, gsl_matrix_get(X,i,k-j) * objpts[i].x);
	       k++;
	    }
	    gsl_matrix_set(X, i, k, gsl_matrix_get(X,i,k-j-1) * objpts[i].y);
	    k++;
	 }

	 if (k>n2) {
	    log_error(ERR_ALGORITHM_FAULT,
		      "k (value=%d) went higher than n2 (value = %d)",k,n2);
	    free_poly2d(p);
	    pop_routine();
	    return NULL;
	 }

	 gsl_vector_set(yx, i, refpts[i].x);
	 gsl_vector_set(yy, i, refpts[i].y);
      }
       
      /* Do X polynomial fit */

      mlw = gsl_multifit_linear_alloc(npoints, n2);
      gsl_multifit_linear(X, yx, c, cov, &tchisq, mlw);

      for (i=0; i<n2; i++) {
	 p->X[i] = gsl_vector_get(c, i);
      }

      if (chisq != NULL) chisq->x = tchisq;

      /* Do Y polynomial fit */

      gsl_multifit_linear(X, yy, c, cov, &tchisq, mlw);

      for (i=0; i<n2; i++) {
	 p->Y[i] = gsl_vector_get(c, i);
      }

      if (chisq != NULL) chisq->y = tchisq;

      /* Tidy up */

      gsl_multifit_linear_free(mlw);

      gsl_vector_free(yx);
      gsl_vector_free(yy);
      gsl_vector_free(c);
      gsl_matrix_free(X);
      gsl_matrix_free(cov);
   }

   pop_routine();

   return p;
}
