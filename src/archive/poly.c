/* $Id$ */
/*
   Module: ip/poly.c
   Author: M.J.Cree

DESCRIPTION:

   Manage one-dimensional (range) polynomials.

*/


#include <stdlib.h>

#include <ip/internal.h>

static char rcsid[] = "$Id$";



/*

NAME:

   alloc_poly() \- Allocate a 1d or 2d polynomial

PROTOTYPE:

   #include <ip/ip.h>

   poly * alloc_poly( dimension, order )
   int dimension;
   int order;

DESCRIPTION:

   Allocate a polynomial of the specified 'dimension' and 'order'.
   All coefficients are set to zero.  The origin is set to 0 for
   one-dimensional polynomials and to (0.0,0.0) for two-dimensional
   polynomials.

   Only one-dimensional (of any order) and two-dimensional
   polynomials (of any order) are currently supported.  The dimension
   refers to the dimension of the domain of the polynomial.  If
   a polynomial with two dimensional output range is required then
   use the poly2d objects.

ERRORS:

   ERR_OUT_OF_MEM
   ERR_OOR_PARAMETER
   ERR_TOOSMALL_PARAMETER

SEE ALSO:

   free_poly()
   poly_add()     poly_numcoeffs()   poly_evaluate()
   poly_fit1d()   poly_fit2d()
   alloc_poly2d()

*/

poly *alloc_poly(int dimension, int order)
{
   poly *p;
   int polypts;
   int i;

   log_routine("alloc_poly");

   if (NOT ip_parm_inrange("dimension", dimension, 1, 2))
      return NULL;

   if (NOT ip_parm_greatereq("poly_order", order, 0))
      return NULL;

   if ((p = ip_malloc(sizeof(poly))) == NULL)
      return NULL;

   if ((polypts = poly_numcoeffsx(dimension, order)) == -1) {
      ip_free(p);
      return NULL;
   }

   if ((p->X = ip_malloc(sizeof(double) * polypts)) == NULL) {
      ip_free(p);
      return NULL;
   }

   p->dimension = dimension;
   p->order = order;
   p->origin.x = p->origin.y = 0.0;

   for (i=0; i<polypts; i++)
      p->X[i] = 0.0;

   pop_routine();
   return p;
}



/*

NAME:

   free_poly() \- Free a polynomial

PROTOTYPE:

   #include <ip/ip.h>

   void  free_poly( p )
   poly *p;

DESCRIPTION:

   Free the polynomial 'p' up.

SEE ALSO:

   alloc_poly()

*/


void free_poly(poly *p)
{
   if (p) {
      if (p->X) ip_free (p->X);
      ip_free(p);
   }
}



/*

NAME:

   poly_add() \- Add two polynomials together

PROTOTYPE:

   #include <ip/ip.h>

   int  poly_add( dest, src )
   poly *dest;
   poly *src;

DESCRIPTION:

   Add the two polynomials 'src' and 'dest' together and leave the
   result in 'dest'.  This is effectively a coefficient by coefficient
   addition.

   The polynomials must have the same dimension and origin.  The
   polynomial returned in 'dest' has the same order as the larger
   order of the two polynomials past.

ERRORS:

   ERR_POLY_NOT_SAME_DIMENSION
   ERR_POLY_NOT_SAME_ORIGIN

SEE ALSO:

   alloc_poly()   poly_evaluate()

*/


int poly_add(poly *dest, poly *src)
{
   double *ptr;
   int ncs,ncd,ncc;
   int i;

   log_routine("poly_add");

   if (dest->dimension != src->dimension) {
      log_error(ERR_POLY_NOT_SAME_DIMENSION);
   }else if (dest->origin.x != src->origin.x
		&& dest->origin.y != src->origin.y) {
      log_error(ERR_POLY_NOT_SAME_ORIGIN);
   }else{
      ncs = poly_numcoeffs(src);
      ncd = poly_numcoeffs(dest);
      ncc = MIN(ncs,ncd);
      if (ncd < ncs) {
	 if ((ptr = ip_realloc(dest->X, ncs*sizeof(double))) == NULL) {
	    return ip_error;
	 }
	 dest->X = ptr;
	 dest->order = src->order;
      }
      for (i=0; i<ncc; i++)
	 dest->X[i] += src->X[i];
      for (   ; i<ncs; i++)
	 dest->X[i] = src->X[i];
   }

   pop_routine();
   return ip_error;
}



/*

NAME:

   poly_numcoeffs() \- Return the number of coefficients of a polynomial

PROTOTYPE:

   #include <ip/ip.h>

   int  poly_numcoeffs( p )
   poly *p;

DESCRIPTION:

   Return the number of coefficients (that is, the number of terms)
   the polynomial 'p' has.

SEE ALSO:

   alloc_poly()

*/

int poly_numcoeffs(poly *p)
{
   return poly_numcoeffsx(p->dimension, p->order);
}



int poly_numcoeffsx(int dimension, int order)
{
   int polypts;

   log_routine("poly_numcoeffsx");

   if (dimension == 1) {
      polypts = order+1;
   }else if (dimension == 2) {
      polypts = (order+1)*(order+2)/2;
   }else{
      log_error(ERR_BAD_PARAMETER);
      polypts = -1;
   }
   pop_routine();

   return polypts;
}




/*

NAME:

   poly_evaluate() \- Evaluate a polynomial at a point

PROTOTYPE:

   #include <ip/ip.h>

   double  poly_evaluate( p, x, y )
   poly *p;
   double x;
   double y;

DESCRIPTION:

   Evaluate the polynomial at the point 'x ' if the polynomial is
   one-dimensional, or at the point ('x','y') if the polynomial is
   two-dimensional.

SEE ALSO:

   alloc_poly()

*/

double poly_evaluate(poly *p, double x, double y)
{
   int count, i, j;
   double res;
   double pow1_x, pow2_x, pow_y;

   x -= p->origin.x;
   
   if (p->dimension == 1) {
      res = p->X[p->order];
      for (i=p->order-1; i>=0; i--) {
	 res *= x;
	 res += p->X[i];
      }
   }else if (p->dimension == 2) {
      y -= p->origin.y;
      res = 0.0;
      count = 0;
      pow1_x = 1;
      for (j=1; j<=p->order+1; j++) {
	 pow_y = 1;
 	 pow2_x = pow1_x;
	 for (i=1; i<=j; i++) {
	    if (x == 0.0) pow2_x = (i < j) ? 0.0 : 1.;

	    res += p->X[count]*pow2_x*pow_y;

	    if (x != 0.0) pow2_x /= (float) x;
	    pow_y *= y;

	    count++;
	 }
	 pow1_x *= x;
      }
   }else{
      res = 0;
   }
   return res;
}


