/* $Id$ */
/*
   Header: ip/cimages.h
   Author: M.J.Cree

   Declarations for interfacing (reading/writing) to c_images images.
*/

#ifndef IP_CIMAGES_H
#define IP_CIMAGES_H

#include <ip/types.h>
#include <ip/image.h>

/* c_images image handle */

typedef struct {
   int ctype;
   int packing;
   int type; 
   coord size;
   FILE *f;
   const char *fname;
} chandle;


/* Entry points to functions. */

extern chandle *open_cimages_image( const char *fname );
extern image *read_cimages_image(chandle *);
extern int write_cimages_imagex(const char *fname, image *im, int flag);
extern void close_cimages_image(chandle *);

#define write_cimages_image(f,i) write_cimages_imagex(f,i,NO_FLAG)

#endif
