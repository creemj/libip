/* $Id$ */
/*
   Module: ip/convolve.c
   Author: M.J.Cree

   Convolve an image with a square 2d convolution kernel.

   � 1995-2001 Michael Cree 

ENTRY POINTS:

   im_convolve()

*/


#include <ip/internal.h>
#include <stdlib.h>

/* #define DEBUG */
#include <debug/debug.h>

static char rcsid[] = "$Id$";


/* Convolution macros */

#define CONVOLVE_INT(t,b)\
   lval = 0;\
   lkp = kernel;\
   for (l = -hwidth; l <= hwidth; l++) {\
      t *bp;\
      bp = IM_ROWADR(im, l+j,b);\
      bp += i-hwidth;\
      for (k = -hwidth; k <= hwidth; k++)  {\
         lval += *bp++ * *lkp++;\
      }\
   }\
   IM_PIXEL(tmp, i, j,b) = do_norm ? (t)(lval/lnorm) : (t)lval;

#define CONVOLVE_REAL(t,b)\
   dval = 0;\
   dkp = kernel;\
   for (l = -hwidth; l <= hwidth; l++) {\
      t *bp;\
      bp = IM_ROWADR(im, l+j,b);\
      bp += i-hwidth;\
      for (k = -hwidth; k <= hwidth; k++)  {\
         dval += *bp++ * *dkp++;\
      }\
   }\
   IM_PIXEL(tmp, i, j,b) = do_norm ? (t)(dval/dnorm) : (t)dval;




/*

NAME:

   im_convolve() \- Convolve an image with a square kernel

PROTOTYPE:

   #include <ip/ip.h>

   int  im_convolve( im, kernel, width, flag )
   image *im;
   void *kernel;
   int width;
   int flag;

DESCRIPTION:

   Convolve the image 'im' with 'kernel', which is of size 'width'*'width'
   pixels.  If 'im' is of integer type then 'kernel' must be a 2d array of
   elements of type LONG.  If 'im' is of real type then 'kernel' must be a
   2d array of double.  'Flag' may take FLG_ZERO in which case the borders
   of the image where the kernel cannot cover are zeroed, and/or FLG_NORM in
   which case each pixel is divided by the summation of all elements of
   'kernel'.  Note that it is ridiculous to use FLG_NORM if the average of
   kernel components is zero.  For integer types, long integer arithmetic is
   used throughout, including the normalisation.

   The convolution is performed in image space and thus is only efficient
   for smallish sized kernels.  For large kernels it is better to use
   Fourier Transform methods.

   Supports BYTE, SHORT, LONG, FLOAT and DOUBLE image types only.

LIMITATIONS:

   'Width' must be odd and greater than or equal to three.

ERRORS: 

   ERR_UNSUPPORTED_TYPE		(for IM_COMPLEX images)
   ERR_BAD_FLAG
   ERR_TOOSMALL_PARAMETER
   ERR_PARM_NOT_ODD
   ERR_OUT_OF_MEM

SEE ALSO:

   im_median()   im_mean()    im_fft()

*/

int im_convolve( image *im, void *kernel, int width, int flag )
{
   image *tmp;
   int hwidth;			/* width of widthxwidth kernel */
   int i,j;			/* loop variables                          */
   int k,l;			/* Kernel indices */
   int do_norm;

   LONG lval;			/* Incrmental value of one pixel  */
   LONG lnorm;			/* Normalisation value */
   LONG *lkp;			/* Pointer into mask */

   double dnorm;		/* Last three variables repeated for double */
   double dval;
   double *dkp;

   log_routine("im_convolve");

   if (im->type == IM_COMPLEX ||
                   image_type_clrim(im) || im->type == IM_BINARY) {
      log_error(ERR_UNSUPPORTED_TYPE);
      goto exit;
   }

   if (NOT type_compiled(im->type))
      return ip_error;

   if (NOT flag_ok(flag,FLG_ZERO,FLG_NORM,FLG_ZERO|FLG_NORM ,LASTFLAG))
      return ip_error;

   if (NOT ip_parm_greatereq("kernel_width", width, 3))
      return ip_error;

   if (NOT ip_parm_odd("kernel_width", width))
      return ip_error;

   if ((tmp = alloc_image(im->type,im->size.x,im->size.y)) == NULL)
      goto exit;

   hwidth = width / 2;		/* half-width of filter */

   if (flag & FLG_NORM) {	/* Find normalisation scale factor */
      do_norm = TRUE;
      if (image_type_integer(im)) {
	 lkp = kernel;
	 lnorm = 0;
	 for (k=0; k < width*width; k++)
	    lnorm += *lkp++;
	 if (lnorm == 0) lnorm = 1;
      }else{
	 dkp = kernel;
	 dnorm = 0.0;
	 for (k=0; k < width*width; k++)
	    dnorm += *dkp++;
	 if (dnorm == 0.0) dnorm = 1.0;
      }
   }else{
      do_norm = FALSE;
   }

   /* for each pixel in image find convolution over width*width kernel */

   for( j = hwidth; j < (im->size.y - hwidth); j++ ) {
      for( i = hwidth; i < (im->size.x - hwidth); i++ ) {
	 switch (im->type) {
#ifdef DOBYTE
	  case IM_BYTE:
	    CONVOLVE_INT(UBYTE,b);
	    break;
#endif
#ifdef DOINTEGER
	  case IM_SHORT:
	    CONVOLVE_INT(SHORT,s);
	    break;

	  case IM_LONG:
	    CONVOLVE_INT(LONG,l);
	    break;
#endif
#ifdef DOREAL
	  case IM_FLOAT:
	    CONVOLVE_REAL(float,f);
	    break;

	  case IM_DOUBLE:
	    CONVOLVE_REAL(double,d);
	    break;
#endif
	 }
      }
   }

   /* Insert convolved image back into original image. */
   
   {
      coord org,size;

      org.x = org.y = hwidth;
      size.x = im->size.x - 2*hwidth;
      size.y = im->size.y - 2*hwidth;

      im_insert(im, org, tmp, org, size);
   }

   free_image(tmp);

   /* Clear border if requested. */

   if (flag & FLG_ZERO) {
      im_clear_border(im,hwidth);
   }

 exit:
   pop_routine();
   return ip_error;
}

