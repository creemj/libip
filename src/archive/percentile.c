/* $Id$ */
/*

   percentile.c


*/
/*
 * $Log$
 * Revision 1.2  1996/11/05 11:17:35  mph489
 * Pass width of kernel (not half-width) now.
 *
 * Revision 1.1  1996/08/12  12:36:12  mph489
 * Initial revision
 *
 */


#include <ip/internal.h>
#include <stdlib.h>


static char rcsid[] = "$Id$";


static int percentile(int *arr, int size, int kth);
static int fill_arr(int *, image *, int i, int j, int hwidth);




/*

NAME:

   im_percentile()

PROTOTYPE:

   int  im_percentile( im, width, percentile, flag )
   image *im;
   int width;
   double percentile;
   int flag;

DESCRIPTION:

   
LIMITATIONS:

   Only works on BYTE or SHORT images.

ERRORS:

*/

int im_percentile( image *im, int width, double percent, int flag )
{

   long hwidth;			/* halfwidth of widthxwidth median filter */
   long i,j;			/* loop variables                          */
   int  *arr;			/* 1-d array for sorting values in select()*/
   int  *ap;
   size_t arrsize;
   UBYTE *bp;
   SHORT *sp;
   image *tmp;
   int skip;
   int kth;

   /* Check the images type :      */
   /* this function supports only the following types : */
   /* BYTE, SHORT */

   log_routine("im_median");

   if ( (im->type!=IM_SHORT) && (im->type!=IM_BYTE) ) {
      log_error(ERR_UNSUPPORTED_TYPE);
      goto exit;
   }

   if (NOT flag_ok(flag,FLG_ZERO,FLG_EXTEND,LASTFLAG))
      return ip_error;

   if ((width < 3) || (NOT width & 0x01)) { /* Width must be odd */
      log_error(ERR_BAD_PARAMETER);
      goto exit;
   }

   if ((tmp = alloc_image(im->type,im->Nx,im->Ny)) == NULL)
      goto exit;

   hwidth = width / 2;		/* half-width of filter */
   skip = ip_typesize[im->type];
   arrsize = width*width;

   kth = ROUND(percent * (arrsize-1)); /* Element to search for. */

   /* initialise array */

   if ((arr = ip_malloc(sizeof(int) * width * width)) == NULL) {
      free_image(tmp);
      return ip_error;
   }

   for (i = 0; i < arrsize; i++) arr[i] = 0;
        
   /* for each pixel in image find median over width*width mask */

   for( j = hwidth; j < (im->Ny - hwidth); j++ ) {
      for( i = hwidth; i < (im->Nx - hwidth); i++ ) {

	 int      rows,cols; /* rows and columns of mask */

	 /* for all pixels corresponding to mask positions........*/
	 /* ...from top-left to bottom-right......................*/
	 /*.........load in pixel values to 1-D array for sorting */

	 ap = arr;
	 for (rows= -hwidth; rows<=hwidth; rows++) {
	    if (im->type == IM_BYTE) {
	       bp = &im->image.b[Xi(i-hwidth,j+rows)];
	       for (cols= -hwidth; cols<=hwidth; cols++)  {
		  *ap++ = *bp++;
	       }
	    }else{
	       sp = &im->image.s[Xi(i-hwidth,j+rows)];
	       for (cols= -hwidth; cols<=hwidth; cols++)  {
		  *ap++ = *sp++;
	       }
	    }
	 }
                                                   
	 if (tmp->type == IM_BYTE)
	    tmp->image.b[X(tmp,i,j)] = percentile(arr,arrsize,kth);
	 else
	    tmp->image.s[X(tmp,i,j)] = percentile(arr,arrsize,kth);
      }
   }

   if (flag == FLG_ZERO) {
      /* Clear edges of tmp */
      im_clear_border(tmp,hwidth);
   }

   if (flag == FLG_EXTEND) {

      int size;

      /* Continue median filter into borders using reduced size mask */

      /* Do top border */

      for (j=0; j<hwidth; j++) {
	 for (i=0; i<im->Nx; i++) {
	    size = fill_arr(arr,im,i,j,hwidth);
	    if (tmp->type == IM_BYTE)
	       tmp->image.b[j*tmp->Nx+i] = percentile(arr,size,kth);
	    else
	       tmp->image.s[j*tmp->Nx+i] = percentile(arr,size,kth);
	 }
      }

      /* Do bottom border */

      for (j=im->Nx-hwidth; j<im->Nx; j++) {
	 for (i=0; i<im->Nx; i++) {
	    size = fill_arr(arr,im,i,j,hwidth);
	    if (tmp->type == IM_BYTE)
	       tmp->image.b[j*tmp->Nx+i] = percentile(arr,size,kth);
	    else
	       tmp->image.s[j*tmp->Nx+i] = percentile(arr,size,kth);
	 }
      }

      /* Do left border */

      for (j=hwidth; j<im->Ny-hwidth; j++) {
	 for (i=0; i<hwidth; i++) {
	    size = fill_arr(arr,im,i,j,hwidth);
	    if (tmp->type == IM_BYTE)
	       tmp->image.b[j*tmp->Nx+i] = percentile(arr,size,kth);
	    else
	       tmp->image.s[j*tmp->Nx+i] = percentile(arr,size,kth);
	 }
      }
	    
      /* Do right border */

      for (j=hwidth; j<im->Ny-hwidth; j++) {
	 for (i=im->Nx-hwidth; i<im->Nx; i++) {
	    size = fill_arr(arr,im,i,j,hwidth);
	    if (tmp->type == IM_BYTE)
	       tmp->image.b[j*tmp->Nx+i] = percentile(arr,size,kth);
	    else
	       tmp->image.s[j*tmp->Nx+i] = percentile(arr,size,kth);
	 }
      }
   }

   if (flag == FLG_ZERO || flag == FLG_EXTEND) {
      memcpy(im->image.v, tmp->image.v, im->Nx*im->Ny*ip_typesize[im->type]);
   }else{
      coord org,size;

      org.x = org.y = hwidth;
      size.x = im->Nx - 2*hwidth;
      size.y = im->Ny - 2*hwidth;

      im_insert(im, org, tmp, org, size);
   }
   free_image(tmp);
   ip_free(arr);

 exit:
   pop_routine();
   return ip_error;
}




static int fill_arr(int *arr, image *im, int i, int j, int hwidth)
{
   int k,l;
   int xstart,xend,xsize;
   int ystart,yend,ysize;

   /* Determine size of array so can't go off edge of image */

   xstart = (hwidth <= i) ? -hwidth :  -i;
   xend = (hwidth <= im->Nx-i-1) ? hwidth : im->Nx-i-1;
   xsize = xend - xstart + 1;
   ystart = (hwidth <= j) ? -hwidth : -j;
   yend = (hwidth <= im->Ny-j-1) ? hwidth : im->Ny-j-1;
   ysize = yend - ystart + 1;


   /* Fill array */

   for (l=ystart; l<=yend; l++) {
      for (k=xstart; k<=xend; k++) {
	 arr[(k-xstart) + (l-ystart)*xsize] = (im->type == IM_BYTE)
					? im->image.b[Xi(i+k,j+l)]
					: im->image.s[Xi(i+k,j+l)];
      }
   }
   return xsize * ysize;
}



static int percentile(int *arr, int size, int kth)
{
   long  left,right;		/* positions in 1-D array             */
   long  i,j,k;			/* loop variables for sorting         */
   short val;			/* value holding current middle value */
   short  t;			/* temporary storage of array values  */

   left  = 0;
   right = size - 1;
   k = kth;			/* Element to search for */

   while (right > left) {
			        /* To prevent j-- loop going off array start */
      if (arr[right] < arr[left]) {
	 t = arr[right];
	 arr[right] = arr[left];
	 arr[left] = t;
      }

      val = arr[right];
      i = left - 1;
      j = right;
      do {
	 do i++; while (arr[i] < val);
	 do j--; while (arr[j] > val);
	 t = arr[i];
	 arr[i] = arr[j]; 
	 arr[j] = t;
      } while (j > i);

      arr[j] = arr[i];
      arr[i] = arr[right];
      arr[right] = t;

      if (i >= k) right = i - 1;
      if (i <= k) left  = i + 1;

   }
   return arr[k];
}
