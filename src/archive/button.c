/* $Id$ */
/*
   Module: ip/button.c
   Author: M.J.Cree

   Button management within display window.

   � 1997-2001 Michael Cree 

ENTRY POINTS:

   alloc_button()
   button_display()
   button_get()
   button_press()
   button_set()
   free_button()

*/


#include <stdlib.h>

#include <ip/internal.h>
#include <ip/button.h>

/* #define DEBUG */
#include <debug/debug.h>

static char rcsid[] = "$Id$";


#define FONTSIZE 12
#define END_OFFSET 4
#define BHEIGHT 17
#define BASELINE 11

/* Button structure field flag can take following fields */

#define BT_REDISPLAY   0x01
#define BT_REDOSTR     0x02
#define BT_OPTPALETTE  0x04

/* Flag field on each individual button can take: */

#define BT_REDOTHIS    0x800000

static int draw_button_labels(button *bt);





/*

NAME:

   alloc_button() \- Allocate a button object

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/button.h>

   button * alloc_button( num, buttons, origin )
   int num;
   button_decl *buttons;
   coord origin;

DESCRIPTION:

   Allocate a button object for use within the display window.  A
   button is actually a set of buttons displayed in the display
   window, on which the user can click, normally to invoke certain
   actions.  Using the button routines makes it easy to manage the
   button object.

   Specify the number of buttons you want in the 'num' parameter and
   the look of the buttons in the array 'buttons'.  At the moment set
   the 'flag' field to zero, and the 'downtext' field to NULL.  The
   'uptext' field should be set to the string you want displayed on
   the button.  There must be 'num' button entries in the buttons
   array.

   Set 'origin' to where you want the buttons to be displayed in the
   display canvas.

   An example of use is:

\|   // Allocate buttons.   
\|
\|   button_decl bdecl[2] = {
\|      {0, "Stop", NULL},
\|      {0, "GO",   NULL}
\|   };
\|
\|   button *bt;
\|
\|   bt = alloc_button(2, bdecl, (coord){0,0});
\|
\|   // Change the text colour to be red and display.
\|
\|   button_set(bt, BT_TEXTPEN, OVL_RED);
\|   button_display(bt);
\|
\|   // Check for a button press.
\|
\|   event_info *evt;
\|   int button_num;
\|
\|   evt = display_click(FLG_MOUSELEFT | FLG_WAIT);
\|   button_num = button_press(bt, evt);
\|
\|   // button_num contains which button (if any) was pressed.
\|      ...
\|
\|   // Deallocate button.
\|
\|   free_button(bt);

ERRORS:

   ERR_OUT_OF_MEM

SEE ALSO:

   free_button()
   button_set()   button_get()   button_display()
   button_press()

*/

button *alloc_button(int num, button_decl *buttons, coord origin)
{
   button *bt;
   button_decl *bt_decl;
   int width, totalwidth;
   int i;

   log_routine("alloc_button");

   width = 0;
   for (i=0; i<num; i++) {
      totalwidth = display_labelwidth(buttons[i].uptext, FONTSIZE);
      if (totalwidth > width) width = totalwidth;
   }

   width += 2*END_OFFSET;
   totalwidth = width * num;

   if ((bt = ip_malloc(sizeof(button))) == NULL) {
      return NULL;
   }

   if ((bt_decl = ip_malloc(num*sizeof(button_decl))) == NULL) {
      ip_free(bt);
      return NULL;
   }

   memcpy(bt_decl, buttons, num*sizeof(button_decl));

   bt->id = FLG_NEWIMAGE;
   bt->num = num;
   bt->width = width;
   bt->button = bt_decl;
   bt->lightpen = 255;
   bt->sdwpen = 0;
   bt->bgpen = 128;
   bt->textpen = OVL_WHITE;
   bt->origin = origin;
   bt->size.x = totalwidth;
   bt->size.y = BHEIGHT;
   bt->flag = BT_REDISPLAY;

   pop_routine();
   return bt;
}




/*

NAME:

   button_display() \- Display button imagery

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/button.h>

   int  button_display( bt )
   button *bt;

DESCRIPTION:

   Display button imagery of the button 'bt'.  An update to the
   display is only done if any imagery of the button has changed since
   the last call to button_display() or if this is the first call to
   button_display().

ERRORS:

   ERR_OUT_OF_MEM

SEE ALSO:

   alloc_button()   free_button()
   button_set()   button_get()
   button_press()

*/

int button_display(button *bt)
{
   image *im;
   int id;
   int Nx, Ny, xs;
   int i;
   coord start,end;

   log_routine("button_display");

   if (bt->flag & BT_REDISPLAY) {

      /* Some change in button imagery has occured that requires
	 redrawing whole imagery. */

      D(bug("Button_display: fg=%d  bg=%d  sdw=%d  text=%d.\n",
	    bt->lightpen, bt->bgpen, bt->sdwpen, bt->textpen));

      if ((im = alloc_image(IM_BYTE, bt->size.x, bt->size.y)) == NULL) {
	 pop_routine();
	 return ip_error;
      }

      im_set(im, bt->bgpen);

      Nx = im->size.x;
      Ny = im->size.y;
      xs = bt->width;

      start.x = start.y = 0;
      end.x = Nx-1;
      end.y = 0;
      im_drawline(im, bt->lightpen, start, end);
      start.x = 0;
      end.y = start.y = Ny-1;
      end.x = Nx-1;
      im_drawline(im, bt->sdwpen, start, end);

      for (i=0; i<bt->num; i++) {
	 start.x = end.x = i*xs;
	 start.y = 0;
	 end.y = Ny-2;
	 im_drawline(im, bt->lightpen, start, end);
	 start.x = end.x = (i+1)*xs-1;
	 im_drawline(im, bt->sdwpen, start, end);
      }
   
      if (bt->flag & BT_OPTPALETTE) 
	 id = display_imagex(bt->id, im, bt->origin, 0,display_palentries()-1);
      else
	 id = display_image(bt->id, im, bt->origin);

      free_image(im);

      if (id < 0) {
	 pop_routine();
	 return ip_error;
      }

      if (bt->id == FLG_NEWIMAGE) bt->id = id;

      if (draw_button_labels(bt) != OK)
	 return ip_error;

      bt->flag &= ~BT_REDISPLAY;

   }else if (bt->flag & BT_REDOSTR) {

      /* Some button string has changed - must redisplay button string
         label */

      Nx = 0;
      for (i=0; i<bt->num; i++) {
	 if (bt->button[i].flag & BT_REDOTHIS) {
	    Nx++;
	 }
      }

      if (Nx == 1 || (Nx == 2 && bt->num > 2)) {
	 display_clear(bt->id, FLG_OVERLAY);
	 if (draw_button_labels(bt) != OK)
	    return ip_error;
      }else{
	 display_clear(bt->id, FLG_OVERLAY);
	 if (draw_button_labels(bt) != OK)
	    return ip_error;
      }

      for (i=0; i<bt->num; i++) {
	 bt->button[i].flag &= ~BT_REDOTHIS;
      }

      bt->flag &= ~BT_REDOSTR;

   }
   pop_routine();
   return OK;
}




/*

NAME:

   button_press() \- Check to see if a button has been pressed

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/button.h>

   int  button_press( bt, event )
   button *bt;
   event_info *event;

DESCRIPTION:

   Check to see if the event contained in the event_info structure
   'event' is a mouse click on one of the buttons.  If so, then the
   button number is returned.  Note that the buttons are numbered from
   one up (not from zero).  If no button has been pressed then -1
   is returned.

SEE ALSO:

   alloc_button()   free_button()
   button_set()   button_get()   button_display()

*/

int button_press(button *bt, event_info *ms)
{
   int but;

   if (ms->type == EVT_MOUSEBUTTON && ms->id == bt->id) {
      but = (ms->pos.x / bt->width) + 1;
      if (but < 1 || but > bt->num)
	 but = -1;
   }else{
      but = -1;
   }

   return but;
}




/*

NAME:

   free_button() \- Free up a button object

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/button.h>

   int  free_button( bt )
   button *bt;

DESCRIPTION:

   Free up all resources associated with the button object 'bt'.  All
   memory is freed and its imagery is removed from the display window.

SEE ALSO:

   alloc_button()
   button_set()   button_get()   button_display()
   button_press()

*/

int free_button(button *bt)
{
   display_free(bt->id);
   ip_free(bt->button);
   ip_free(bt);
   return OK;
}



/*

NAME:

   button_get() \- Examine some property of the button object

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/button.h>

   datum  button_get( bt, action )
   button *bt;
   int action;

DESCRIPTION:

   Get back some property of the button object 'bt'.  The possible 
   actions are:

\|   Action     Type       Description
\| BT_SIZE      coord  Size of imagery of button.
\| BT_ORIGIN    coord  Location of button imagery in display
\|                     window.
\| BT_TEXTPEN   UBYTE  Overlay pen used to display text.
\| BT_LIGHTPEN  UBYTE  Palette pen colour used to display
\|                     highlighted borders.
\| BT_SDWPEN    UBYTE  Palette pen colour used to display
\|                     borders in shadow.
\| BT_BGPEN     UBYTE  Background (general) shading of a
\|                     button.

ERRORS:

   ERR_BAD_ACTION

SEE ALSO:

   alloc_button() free_button()
   button_set()   button_display()
   button_press()

*/

datum button_get(button *bt, int action)
{
   datum res;
   
   log_routine("button_get");
   
   switch(action) {
    case BT_TEXTPEN:
      res.b = bt->textpen;
      break;
    case BT_LIGHTPEN:
      res.b = bt->lightpen;
      break;
    case BT_BGPEN:
      res.b = bt->bgpen;
      break;
    case BT_SDWPEN:
      res.b = bt->sdwpen;
      break;
    case BT_SIZE:
      res.pt = bt->size;
      break;
    case BT_ORIGIN:
      res.pt = bt->origin;
      break;
    default:
      log_error(ERR_BAD_ACTION);
      res.d = 0;
      break;
   }

   pop_routine();
   return res;
}



/*

NAME:

   button_set() \- Set a property of the button object

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/button.h>

   int button_set( bt, action, ... )
   button *bt;
   int action;

DESCRIPTION:

   Set a property of the button object 'bt' to be some particular value.
   Possible actions are:

\|   Action   Extra Args       Description
\| BT_ORIGIN    coord  Location of button imagery in display
\|                     window.
\| BT_TEXTPEN   int    Overlay pen used to display text.
\| BT_LIGHTPEN  int    Palette pen colour used to display
\|                     highlighted borders.
\| BT_SDWPEN    int    Palette pen colour used to display
\|                     borders in shadow.
\| BT_BGPEN     int    Background (general) shading of a
\|                     button.
\| BT_STRING    int, char *
\|                     Set a particular button to have
\|                     a new string.  Eg., to set button
\|                     number two to be "Doodle" do
\|                     button_set(bt, BT_STRING, 2, "Doodle");
\| BT_OPTIMUMPAL int   Display as if optimum palette has been
\|                     selected if argument is TRUE, otherwise
\|                     display with normal palette.

   None of these changes take affect until button_display() is called.

BUGS:

   At the present when changing a button string no check is made to make
   sure the string is not to long.  It is the user''s responsibility to
   make sure the string is no longer than any of the strings were when
   allocating the button.

ERRORS:

   ERR_BAD_ACTION
   ERR_BAD_PARAMETER
   ERR_OOR_PARAMETER

SEE ALSO:

   alloc_button() free_button()
   button_get()   button_display()
   button_press()

*/

int button_set(button *bt, int action, ...)
{
   va_list ap;
   int pen, button;
   coord pt;
   char *str;

   va_start(ap, action);
   log_routine("button_set");

   switch (action) {
    case BT_TEXTPEN:

      pen =  va_arg(ap, int);
      if (NOT ip_parm_inrange("text_pen", pen, OVL_START, OVL_END))      
	 goto exit_bs;
      bt->textpen = pen;
      bt->flag |= BT_REDISPLAY;
      break;

    case BT_LIGHTPEN:

      pen =  va_arg(ap, int);
      if (NOT ip_parm_inrange("light_pen", pen, 0, 255))
	 goto exit_bs;
      bt->lightpen = pen;
      bt->flag |= BT_REDISPLAY;
      break;

    case BT_SDWPEN:

      pen =  va_arg(ap, int);
      if (NOT ip_parm_inrange("shadow_pen", pen, 0, 255))
	 goto exit_bs;
      bt->sdwpen = pen;
      bt->flag |= BT_REDISPLAY;
      break;

    case BT_BGPEN:
      
      pen =  va_arg(ap, int);
      if (NOT ip_parm_inrange("background_pen", pen, 0, 255))
	 goto exit_bs;
      bt->bgpen = pen;
      bt->flag |= BT_REDISPLAY;
      break;

    case BT_ORIGIN:

      pt = va_arg(ap, coord);
      if (pt.x < 0 || pt.y < 0) {
	 log_error(ERR_BAD_PARAMETER);
	 pop_routine();
	 goto exit_bs;
      }
      bt->origin = pt;
      bt->flag |= BT_REDISPLAY;
      break;

    case BT_UPSTRING:		/* Also BT_STRING */

      button =  va_arg(ap, int);
      if (NOT ip_parm_inrange("button_number", button, 1, bt->num))
	 goto exit_bs;
      button--;
      str = va_arg(ap, char *);
      bt->button[button].uptext = str;
      bt->button[button].flag |= BT_REDOTHIS;
      bt->flag |= BT_REDOSTR;
      break;

    case BT_OPTIMUMPAL:

      pen =  va_arg(ap, int);
      if (pen)
	 bt->flag |= BT_OPTPALETTE;
      else
	 bt->flag &= ~BT_OPTPALETTE;
      break;

    default:

      log_error(ERR_BAD_ACTION);
      break;

   }
 
   pop_routine();

 exit_bs:

   va_end(ap);
   return ip_error;
}
   

/**** Private routines follow... ****/


static int draw_button_labels(button *bt)
{
   int i;
   int xs;
   coord posn;

   for (i=0; i<bt->num; i++) {
      xs = bt->width - display_labelwidth(bt->button[i].uptext, FONTSIZE);
      xs /= 2;
      posn.x = bt->width*i+xs;
      posn.y = BASELINE;
      if (display_label(bt->id, bt->button[i].uptext, bt->textpen,
			FONTSIZE, posn) != OK) {
	 pop_routine();
	 return ip_error;
      }
   }
   return OK;
}
