/* $Id$ */
/*
   Module: noise.c
   Author: M.J.Cree

   Experimental at present.

*/


#include <stdlib.h>
#include <limits.h>

#include <ip/internal.h>


static char rcsid[] = "$Id$";


/*

NAME:

   im_addnoise() \- Add noise into an image

PROTOTYPE:

   #include <ip/ip.h>

   int im_addnoise( im, skew, amount, flag )
   image *im;
   double skew;
   double amount;
   int flag;

DESCRIPTION:

   Add noise to an image. Flags may be:

\&   FLG_LINEAR  Add linear distributed noise.
\&   FLG_GAUSSIAN Add Gaussian distributed noise.

\&- The parameter 'amount' controls the amount of noise added and the
   parameter 'skew' can skew the noise distribution so it''s not
   centred on zero.  Basically, the noise is generated as a random
   number between -0.5 and 0.5 with the requested distribution and
   then is multiplied by 'amount' then has 'skew' added to it and then
   is added to the image pixel.  If an overflow occurs (for example,
   the resultant value is greater than 255 in a BYTE image) then the
   pixel value is clipped to the image data type range before being
   stored back into the image.

LIMITATIONS:

   Uses the random() function which only seems to be available on
   SunOS 5.5.

   This routine is currently under development.  I may change the
   calling interface.  Be warned.

   COMPLEX images and colour type images currently not supported.

ERRORS:

   ERR_BAD_TYPE
   ERR_BAD_FLAG

*/

int im_addnoise(image *im, double skew, double amount, int flag)
{
   double (*rnd)(void);
   int j;
   int noise_type;

   log_routine("im_addnoise");

   if (image_bad_type(im, IM_PALETTE, IM_RGB, IM_BINARY, IM_COMPLEX, -1))
      return ip_error;

   if (flag & FLGPRT_NOISE_NOTUSED) {
      log_error(ERR_BAD_FLAG);
      pop_routine();
      return ip_error;
   }

   noise_type = flag & FLGPRT_NOISE;

   switch (noise_type) {
    case FLG_LINEAR:
      rnd = ip_rand_linear;
      skew -= amount * 0.5;	/* Random numbers are to be centred on zero
				   when skew initially equals zero. */
      break;
    case FLG_GAUSSIAN:
      rnd = ip_rand_gaussian;
      break;
    default:
      log_error(ERR_BAD_FLAG);
      pop_routine();
      return ip_error;
   }

   switch (im->type) {

    case IM_BYTE:

      for (j=0; j<im->size.y; j++) {
	 UBYTE *bp;
	 double val;
	 int i;
	 
	 bp = IM_ROWADR(im, j, b);
	 for (i=0; i<im->size.x; i++) {
	    val = (double)(*bp) + amount*rnd() + skew;
	    if (val < 0.0) val = 0.0;
	    if (val > UBYTE_MAXV) val = UBYTE_MAXV;
	    *bp++ = (UBYTE)val;
	 }
      }
      break;

    case IM_SHORT:

      for (j=0; j<im->size.y; j++) {
	 SHORT *sp;
	 double val;
	 int i;
	 
	 sp = IM_ROWADR(im, j, s);
	 for (i=0; i<im->size.x; i++) {
	    val = (double)(*sp) + amount*rnd() + skew;
	    if (val < SHRT_MINV) val = SHRT_MINV;
	    if (val > SHRT_MAXV) val = SHRT_MAXV;
	    *sp++ = (SHORT)val;
	 }
      }
      break;

    case IM_LONG:

      for (j=0; j<im->size.y; j++) {
	 LONG *lp;
	 double val;
	 int i;
	 
	 lp = IM_ROWADR(im, j, l);
	 for (i=0; i<im->size.x; i++) {
	    val = (double)(*lp) + amount*rnd() + skew;
	    if (val < LONG_MINV) val = LONG_MINV;
	    if (val > LONG_MAXV) val = LONG_MAXV;
	    *lp++ = (LONG)val;
	 }
      }
      break;

    case IM_FLOAT:

      for (j=0; j<im->size.y; j++) {
	 float *fp;
	 int i;
	 
	 fp = IM_ROWADR(im, j, f);
	 for (i=0; i<im->size.x; i++) {
	    *fp++ += amount*rnd() + skew;
	 }
      }
      break;

    case IM_DOUBLE:

      for (j=0; j<im->size.y; j++) {
	 double *dp;
	 int i;
	 
	 dp = IM_ROWADR(im, j, d);
	 for (i=0; i<im->size.x; i++) {
	    *dp++ += amount*rnd() + skew;
	 }
      }
      break;
   }

   pop_routine();
   return ip_error;
}
