/* $Id$ */
/*

   Module: ip/regparms.c
   Author: M.J.Cree

   Manages regparms objects.


*/
/*
 * $Log$
 * Revision 1.1  1997/09/02 14:02:57  mph489
 * Initial revision
 *
 */


#include <stdlib.h>
#include <ip/internal.h>

static char rcsid[] = "$Id$";




/*

NAME:

   alloc_regparms() \- Allocate a regparms object

PROTOTYPE:

   #include <ip/ip.h>

   regparms * alloc_regparms( void )

DESCRIPTION:

   Allocate a regparms object.   This object contains the parameters
   for doing a rigid transformation.   It is returned by im_register()
   and used in warp_to_poly2d() for example.  The allocate routine
   sets the transformation to be the identity transformation.

   Free the regparms object with free_regparms().

ERRORS:

   ERR_OUT_OF_MEM

SEE ALSO:

   free_regparms()   im_register()   warp_to_poly2d()

*/


regparms *alloc_regparms(void)
{
   regparms *rp;

   log_routine("alloc_regparms");

   if ((rp = ip_malloc(sizeof(regparms))) == NULL)
      return NULL;
 
   rp->correlation = 0.0;
   rp->trans.x = 0.0;
   rp->trans.y = 0.0;
   rp->rotate = 0.0;
   rp->scale = 1.0;

   pop_routine();
   return rp;
}




/*

NAME:

   free_regparms() \- Free up a regparms object

PROTOTYPE:

   #include <ip/ip.h>

   void  free_regparms( rp )
   regparms *rp;

DESCRIPTION:

   Frees up the regparms object pointed to by 'rp'.

SEE ALSO:

   alloc_regparms()

*/

void free_regparms(regparms *rp)
{
   if (rp) ip_free(rp);
}

