/* $Id$ */
/*

   Module: ip/polydbg.c
   Author: M.J.Cree

   Routines for dumping polynomial structures, etc.
   Useful for debugging.

*/
/*
 * $Log$
 * Revision 1.2  2002/07/15 02:46:14  cree
 * dump_poly() can now cope with NULL pointer.
 *
 * Revision 1.1  1998/07/21  02:38:48  cree
 * Initial revision
 *
 */

static char rcsid[] = "$Id$";

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ip/internal.h>




static char *Xnames2d[] = {
   "1",
   "x",
   "y",
   "x^2",
   "xy",
   "y^2",
   "x^3",
   "x^2y",
   "xy^2",
   "y^3",
   "x^4",
   "x^3y",
   "x^2y^2",
   "xy^3",
   "y^4"
};


/*

NAME:

   dump_poly() \- Dump information about polynomial to stdout

PROTOTYPE:

   #include <ip/ip.h>

   void  dump_poly( p )
   poly *p;

DESCRIPTION:

   Dump the coefficients, etc. of the polynomial 'p'.

SEE ALSO:

   alloc_poly()

*/

void dump_poly(poly *p)
{
   int i;
   int polypts;

   printf("Polynomial (poly object) at %p:\n",p);
   if (p) {
      printf("  dimension = %d, order = %d, ",p->dimension, p->order);
      if (p->dimension == 1) {
	 printf("origin = %g\n",p->origin.x);
      }else{
	 printf("origin = (%g,%g)\n",p->origin.x, p->origin.y);
      }
   
      if (p->dimension == 1) {
	 printf("  1: %g\n",p->X[0]);
	 if (p->order > 0) printf("  x: %g\n",p->X[1]);
	 for (i=2; i <= p->order; i++)
	    printf("x^%d: %g\n",i,p->X[i]);
      }else if (p->dimension == 2) {
	 polypts = poly_numcoeffs(p);
	 for (i=0; i<polypts && i < 15; i++)
	    printf("%6s: %g\n",Xnames2d[i],p->X[i]);
	 for (; i<polypts; i++)
	    printf("      : %g\n",p->X[i]);
      }else{
	 printf("  Unsupported dimensionality.\n");
      }
   }else{
      printf("  No polynomial - null pointer.\n");
   }
   fflush(stdout);
}




/*

NAME:

   dump_poly2d() \- Dump information about 2d polynomial to stdout

PROTOTYPE:

   #include <ip/ip.h>

   void  dump_poly2d( p )
   poly2d *p;

DESCRIPTION:

   Dump the coefficients, etc. of the polynomial 'p'.

SEE ALSO:

   alloc_poly2d()

*/


void dump_poly2d(poly2d *p)
{
   char label[9];
   int count, count_save;
   int six;
   int i, j;
   int order;
   int polypts;

   printf("Transformation polynomial (poly2d object) at %p:\n",p);
   printf("  dimension = %d, order = %d\n",p->dimension, p->order);
   if (p->dimension == 2) {   
      polypts = poly2d_numcoeffs(p);
      i = 0;
      j = 0;
      order = 0;
      
      count = 0;
      count_save = 0;
      while (count < polypts) {
	 printf("          "); 
	 count_save = count;
	 for (count=count_save, six=0; 
	      (six < 6) && (count < polypts); six++, count++) {
	    if (j == 0) strcpy(label, "");
	    else if (j == 1) strcpy(label, "x");
	    else sprintf(label, "x^%d", j);

	    if (i == 1) strcat(label, "y");
	    else if (i > 1) sprintf(label, "%sy^%d", label, i);

	    printf("%10s ", label);

	    i++; j--;
	    if (i > order) {i = 0; order++; j = order;}
	 }

	 if (count == six) printf("\n     x' = "); 
	 else printf("\n          ");    

	 for (count=count_save, six=0; 
	      (six < 6) && (count < polypts); six++, count++)
	    printf("%#10.3g ",p->X[count]);
      
	 if (count == six) printf("\n     y' = "); 
	 else printf("\n          ");    

	 for (count=count_save, six=0; 
	      (six < 6) && (count < polypts); six++, count++)
	    printf("%#10.3g ",p->Y[count]);

	 printf("\n\n");
      }
   }else{
      printf("  Unsupported dimensionality.\n");
   }
  
   fflush(stdout);
}

