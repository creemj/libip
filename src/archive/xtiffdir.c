/* $Id$ */
/*
   Module: ip/xtiffdir.c
   Author: M.J.Cree

   This module is based upon the file xtif_dir.c which comes with the
   contrib/tags directory of the libtiff distribution and is written
   by Niles D. Ritter.

   This implements a Dept Bio-medical Physics, University of Aberdeen,
   in-house tag for recording Retinal Image processing information.
*/


/* Only include this module if need ophthalmic tag */
#ifdef IP_OPHTHALTAG

#include <ip/xtiffiop.h>
#include <stdio.h>


static char rcsid[] = "$Id$";



/*  Tiff info structure.
 *
 *     Entry format:
 *        { TAGNUMBER, ReadCount, WriteCount, DataType, FIELDNUM, 
 *          OkToChange, PassDirCountOnSet, AsciiName }
 *
 *     For ReadCount, WriteCount, -1 = unknown; used for mult-valued
 *         tags and ASCII.
 */

static const TIFFFieldInfo xtiffFieldInfo[] = {
   { TIFFTAG_OPHTHALMICINFO, -1, -1, TIFF_UNDEFINED, FIELD_OPHTHALMICINFO,
	TRUE, TRUE, "OphthalmicInfoTag" }
};
#define	N(a)	(sizeof (a) / sizeof (a[0]))


static void
_XTIFFPrintDirectory(TIFF* tif, FILE* fd, long flags)
{
   xtiff *xt = XTIFFDIR(tif);
   XTIFFDirectory *xd = &xt->xtif_dir;

   /* call the inherited method */
   if (PARENT(xt,printdir))
      (PARENT(xt,printdir))(tif,fd,flags);

   if (TIFFFieldSet(tif,FIELD_OPHTHALMICINFO)) {
      fprintf(fd, "  Ophthalmic Image Information: (size=%d)\n",
	      xd->xd_size);
   }


}

static int
_XTIFFVSetField(TIFF* tif, ttag_t tag, va_list ap)
{
   xtiff *xt = XTIFFDIR(tif);
   XTIFFDirectory* xd = &xt->xtif_dir;
   int status = 1;
   uint32 v32=0;
   int v=0;

   /* va_start is called by the calling routine */
	
   if (tag == TIFFTAG_OPHTHALMICINFO) {
      xd->xd_size = (uint16) va_arg(ap, int);
      _TIFFsetByteArray(&xd->xd_info, va_arg(ap, double*),
			(long) xd->xd_size);
   }else{
      return (PARENT(xt,vsetfield))(tif,tag,ap);
   }
  
   if (status) {
      /* we have to override the print method here,
       * after the compression tags have gotten to it.
       * This makes sense because the only time we would
       * need the extended print method is if an extended
       * tag is set by the reader.
       */
      if (!(xt->xtif_flags & XTIFFP_PRINT)) {
	 PARENT(xt,printdir) =  TIFFMEMBER(tif,printdir);
	 TIFFMEMBER(tif,printdir) = _XTIFFPrintDirectory;
	 xt->xtif_flags |= XTIFFP_PRINT;
      }
      TIFFSetFieldBit(tif, _TIFFFieldWithTag(tif, tag)->field_bit);
      tif->tif_flags |= TIFF_DIRTYDIRECT;
   }
   va_end(ap);
   return (status);

 badvalue:
   TIFFError(tif->tif_name, "%d: Bad value for \"%s\"", v,
	     _TIFFFieldWithTag(tif, tag)->field_name);
   va_end(ap);
   return (0);

 badvalue32:
   TIFFError(tif->tif_name, "%ld: Bad value for \"%s\"", v32,
	     _TIFFFieldWithTag(tif, tag)->field_name);
   va_end(ap);
   return (0);
}



static int
_XTIFFVGetField(TIFF* tif, ttag_t tag, va_list ap)
{
   xtiff *xt = XTIFFDIR(tif);
   XTIFFDirectory* xd = &xt->xtif_dir;

   if (tag == TIFFTAG_OPHTHALMICINFO) {
      *va_arg(ap, uint16*) = xd->xd_size;
      *va_arg(ap, void**) = xd->xd_info;
   }else{
      return (PARENT(xt,vgetfield))(tif,tag,ap);
   }

   return (1);
}



#define	CleanupField(member) { \
   if (xd->member) { \
      _TIFFfree(xd->member); \
      xd->member = 0; \
   } \
}


/*
 * Release storage associated with a directory.
 */
static void
_XTIFFFreeDirectory(xtiff* xt)
{
   XTIFFDirectory* xd = &xt->xtif_dir;

   /* 
    *  XXX - Purge all Your allocated memory except
    *  for the xtiff directory itself. This includes
    *  all fields that require a _TIFFsetXXX call in
    *  _XTIFFVSetField().
    */

   CleanupField(xd_info);	
}
#undef CleanupField



static void _XTIFFLocalDefaultDirectory(TIFF *tif)
{
   xtiff *xt = XTIFFDIR(tif);
   XTIFFDirectory* xd = &xt->xtif_dir;

   /* Install the extended Tag field info */
   _TIFFMergeFieldInfo(tif, xtiffFieldInfo, N(xtiffFieldInfo));

   /*
    *  free up any dynamically allocated arrays
    *  before the new directory is read in.
    */
	 
   _XTIFFFreeDirectory(xt);	
   _TIFFmemset(xt,0,sizeof(xtiff));

   /* Override the tag access methods */

   PARENT(xt,vsetfield) =  TIFFMEMBER(tif,vsetfield);
   TIFFMEMBER(tif,vsetfield) = _XTIFFVSetField;
   PARENT(xt,vgetfield) =  TIFFMEMBER(tif,vgetfield);
   TIFFMEMBER(tif,vgetfield) = _XTIFFVGetField;

   /* 
    * XXX Set up any default values here.
    */
   xd->xd_size = 0;
}



/**********************************************************************
 *    Nothing below this line should need to be changed.
 **********************************************************************/

static TIFFExtendProc _ParentExtender;



/*
 *  This is the callback procedure, and is
 *  called by the DefaultDirectory method
 *  every time a new TIFF directory is opened.
 */

static void
_XTIFFDefaultDirectory(TIFF *tif)
{
   xtiff *xt;
	
   /* Allocate Directory Structure if first time, and install it */
   if (!(tif->tif_flags & XTIFF_INITIALIZED)) {
      xt = _TIFFmalloc(sizeof(xtiff));
      if (!xt) {
	 /* handle memory allocation failure here ! */
	 return;
      }
      _TIFFmemset(xt,0,sizeof(xtiff));
      /*
       * Install into TIFF structure.
       */
      TIFFMEMBER(tif,clientdir) = (tidata_t)xt;
      tif->tif_flags |= XTIFF_INITIALIZED; /* dont do this again! */
   }
   
   /* set up our own defaults */
   _XTIFFLocalDefaultDirectory(tif);

   /* Since an XTIFF client module may have overridden
    * the default directory method, we call it now to
    * allow it to set up the rest of its own methods.
    */

   if (_ParentExtender) 
      (*_ParentExtender)(tif);

}




/*
 *  XTIFF Initializer -- sets up the callback
 *   procedure for the TIFF module.
 */

static
void _XTIFFInitialize(void)
{
   static first_time=1;
	
   if (! first_time) return;	/* Been there. Done that. */
   first_time = 0;
	
   /* Grab the inherited method and install */
   _ParentExtender = TIFFSetTagExtender(_XTIFFDefaultDirectory);
}


/*
 * Public File I/O Routines.
 */
TIFF*
XTIFFOpen(const char* name, const char* mode)
{
   /* Set up the callback */
   _XTIFFInitialize();	
	
   /* Open the file; the callback will set everything up */
   return TIFFOpen(name, mode);
}




TIFF*
XTIFFFdOpen(int fd, const char* name, const char* mode)
{
   /* Set up the callback */
   _XTIFFInitialize();	

   /* Open the file; the callback will set everything up */
   return TIFFFdOpen(fd, name, mode);
}



void
XTIFFClose(TIFF *tif)
{
   xtiff *xt = XTIFFDIR(tif);
	
   /* call inherited function first */
   TIFFClose(tif);
	
   /* Free up extended allocated memory */
   _XTIFFFreeDirectory(xt);
   _TIFFfree(xt);
}


#endif
