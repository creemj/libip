/* $Id$ */
/*
   Module: ip/cimages.c
   Author: M.J.Cree

   Routines for reading in and saving Foster Findlay c_images images.
   This is to be used for readin/writing images from/for the Archimedes.
*/


#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <ip/internal.h>
#include <ip/cimages.h>

static char rcsid[] = "$Id$";


/* To help useless sunos 4.1 stdio.h header a bit */
#ifndef SEEK_SET
#define SEEK_SET 0
#endif

/* From c_images manual */

#define ANYIM 0
#define NO_PACK 0
#define BIT_PACK 2
#define REV_PACK 3
#define CI_UCHAR 0x08
#define CI_USWORD 0x10
#define CI_UWORD 0x20

struct cimages_header {
   unsigned char ffa_id[4];
   unsigned long composition;
   unsigned short int n_components;
   unsigned short int xsize;
   unsigned short int ysize;
   unsigned short int zsize;
   unsigned short int imdatatype;
   unsigned short int packing;
   unsigned short int calunitlen;
   unsigned short int caldatatype;
   unsigned short int caln;
   unsigned short int tdcount;
   unsigned long imagestart;
   unsigned long calibstart;
   unsigned long textstart;
   unsigned long id;
   unsigned long nextiminfo;
};


/* Internal call points. */
static int convert_type(int,int);
static int ses(unsigned short int x);
static unsigned int sel(unsigned long x);

/*

NAME:

   open_cimages_image() \- Open C-images image ready for reading

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/cimages.h>

   chandle *open_cimages_image( filename )
   const char * filename;

DESCRIPTION:

   Open c_images image corresponding to file name. Returns c_images handle
   or NULL if any errors occur. One can check image type and size in 
   the c_images handle. This routine does not read in the actual image
   data, only the c_images image header.

*/

chandle *open_cimages_image(const char * fname)
{
   chandle *ch;
   struct cimages_header chead;

   log_routine("open_cimages_image");

   ch = NULL;

   if (fname == NULL) {         /* Havn't implemented pipe support */
      log_error(ERR_NO_STDIN,"Cimages");
      goto exit_oci;
   }

   /* Allocate c_images handle. */

   if ((ch = ip_mallocx(sizeof(chandle))) == NULL) {
      goto exit_oci;
   }
   ch->f = NULL;
   ch->fname = fname;

   /* Open disk file and read in first part in anticipation 
      of c_images image */

   errno = 0;
   if ((ch->f = fopen(fname,"rb")) == NULL) {
      log_error(ERR_FILE_OPEN,fname);
      goto exit_oci;
   }
   if (fread(&chead,1,sizeof(chead),ch->f) != sizeof(chead)) {
      log_error(ERR_SHORT_FILE,fname);
      goto exit_oci;
   }

   /* Interpret header */

   if (NOT STREQUAL(chead.ffa_id,"FFA\2")) {
      log_error(ERR_NOT_CIMAGES,fname);
      goto exit_oci;
   }
   ch->ctype = ses(chead.imdatatype);
   ch->packing = ses(chead.packing);
   ch->type = convert_type(ses(chead.imdatatype),ses(chead.packing));
   ch->size.x = ses(chead.xsize);
   ch->size.y = ses(chead.ysize);
   if (ses(chead.zsize) != 1 || ch->type == IM_TYPEMAX) {
      log_error(ERR_INVALID_CIMAGES_TYPE,fname);
      goto exit_oci;
   }
   if (fseek(ch->f,sel(chead.imagestart),SEEK_SET)) {
      log_error(ERR_FILEIO,fname);
      goto exit_oci;
   }

   pop_routine();

   return ch;

 exit_oci:
   if (ch) {
      if (ch->f)
	 fclose(ch->f);
      ip_free(ch);
   }
   pop_routine();
   return NULL;
}



/*

NAME:

   write_cimages_image() \- Write C-images image to file

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/cimages.h>

   int write_cimages_image( filename, im )
   const char * filename;
   image * im;

DESCRIPTION:

   Writes image as cimages file to filename. Any error conditions
   returned.  (Implemented as macro in header file.)

*/




/*

NAME:

   write_cimages_imagex() \- Write C-images image to file

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/cimages.h>

   int write_cimages_imagex( filename, im, flag )
   const char * filename;
   image * im;
   int flag;

DESCRIPTION:

   Writes image as cimages file to filename. Any error conditions
   returned.  Flag can take the value FLG_BINARY for writing a binary
   mask image.

*/

int write_cimages_imagex(const char *fname, image *im, int flag)
{
   struct cimages_header ch;
   FILE *f;
   UBYTE *rowbuf, *iptr;
   int i,j,mask;

   log_routine("write_cimages_image");

   if (im->type != IM_BYTE || im->type != IM_BINARY) {
      log_error(ERR_UNSUPPORTED_CIMAGES_WRITE,fname ? fname : ipmsg_stdout);
      return ip_error;
   }

   /* Initialise image header */

   ch.ffa_id[0] = 'F';
   ch.ffa_id[1] = 'F';
   ch.ffa_id[2] = 'A';
   ch.ffa_id[3] = 2;
   ch.composition = sel(ANYIM);
   ch.n_components = ses(1);
   ch.xsize = ses(im->size.x);
   ch.ysize = ses(im->size.y);
   ch.zsize = ses(1);
   ch.imdatatype = ses(CI_UCHAR);
   if (flag == FLG_BINARY || im->type == IM_BINARY)
      ch.packing = ses(BIT_PACK);
   else
      ch.packing = ses(NO_PACK);
   ch.calunitlen = ch.caldatatype = ch.caln = ch.tdcount = 0;
   ch.imagestart = sel(sizeof(ch));
   ch.calibstart = ch.textstart = ch.id = ch.nextiminfo = 0L;

   /* Open file and write header followed by image data. */

   errno = 0;
   if (fname) {
      if ((f = fopen(fname,"wb")) == NULL) {
	 log_error(ERR_FILE_OPEN,fname);
	 goto exit_wci;
      }
   }else{
      f = stdout;
      fname = ipmsg_stdout;
   }

   /* Write header */
   
   if (fwrite(&ch, 1, sizeof(ch), f) != sizeof(ch)) {
      log_error(ERR_FILEIO,fname);
      goto exit_wci;
   }

   /* If binary image then pack image else write as bytes */

   if (flag == FLG_BINARY || im->type == IM_BINARY) {
      if ((rowbuf = ip_mallocx((im->size.x+7)/8)) == NULL) {
	 goto exit_wci;
      }
      for (j=0; j<im->size.y; j++) {
	 mask = 0x80;
	 for (i=0; i<(im->size.x+7)/8; i++) {
	    rowbuf[i] = 0;
	 }
	 iptr = im_rowadr(im, j);
	 for (i=0; i<im->size.x; i++) {
	    rowbuf[i/8] |= (*iptr++ != 0) * mask;
	    mask >>= 1;
	    if (mask == 0)
	       mask = 0x80;
	 }
	 if (fwrite(rowbuf, 1, (im->size.x+7)/8, f) != (im->size.x+7)/8) {
	    log_error(ERR_FILEIO_OR_TOO_SHORT,"");
	    ip_free(rowbuf);
	    goto exit_wci;
	 }
      }
      ip_free(rowbuf);
   }else{
      if (fwrite(im->image.v, ip_typesize[im->type], im->size.x*im->size.y, f) 
						    != im->size.x*im->size.y) {
	 log_error(ERR_FILEIO,fname);
      }
   }

 exit_wci:

   if (f && f != stdout)
      fclose(f);

   pop_routine();

   return ip_error;
}



/*

NAME:

   read_cimages_image() \- Read C-images image in from opened file

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/cimages.h>

   image * read_cimages_image( handle )
   chandle *handle;

DESCRIPTION:

   Reads in c_images image corresponding to chandle from the disk. Returns
   the image data with image an opened image handle. One neads to call
   open_c_images_image() to get a valid chandle before calling this routine.
   Returns NULL if an error occurs.

*/

image *read_cimages_image(chandle *ch)
{
   image *im;
   int i,j;
   UBYTE *rowbuf,*iptr;
   UBYTE mask;

   log_routine("read_cimages_image");

   /* Allocate image */

   if ((im = alloc_image(ch->type, ch->size.x, ch->size.y)) == NULL)
      goto exit_rci;

   /* Check to see if need to upcast image type */

   if (ch->ctype == CI_USWORD) {
      log_error(ERR_NOT_IMPLEMENTED);
      free_image(im);
      im = NULL;
   }else{
      /* Read in image. */

      errno = 0;
      if (im->type == IM_BINARY) {
	 if ((rowbuf = ip_mallocx((im->size.x+7)/8)) == NULL) {
	    goto exit_rci;
	 }
	 for (j=0; j<im->size.y; j++) {
	    if (fread(rowbuf, 1, (im->size.x+7)/8,ch->f) != (im->size.x+7)/8) {
	       log_error(ERR_FILEIO_OR_TOO_SHORT,"");
	       ip_free(rowbuf);
	       goto exit_rci;
	    }
	    iptr = im_rowadr(im, j);
	    mask = (ch->packing == BIT_PACK) ? 0x80 : 0x01;
	    for (i=0; i<im->size.x; i++) {
	       *iptr++ = (rowbuf[i/8] & mask) ? 255 : 0;
	       if (ch->packing == BIT_PACK) 
		  mask >>= 1;
	       else
		  mask <<= 1;
	       if (mask == 0)
		  mask = (ch->packing == BIT_PACK) ? 0x80 : 0x01;
	    }
	 }
	 ip_free(rowbuf);
      }else{
	 
	 if (fread(im->image.v, ip_typesize[im->type], im->size.x*im->size.y, ch->f)
	     != im->size.x*im->size.y ) {
	    log_error(ERR_FILEIO_OR_TOO_SHORT,"");
	    free_image(im);
	    im = NULL;
	 }
      }
   }
 exit_rci:
   pop_routine();
   return im;
}


/*

NAME:

   close_cimages_image() \- Close a C-images image file

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/cimages.h>

   void close_cimages_image( handle )
   chandle *handle;

DESCRIPTION:

   Closes c_images file associated with chandle. Chandle is not valid anymore
   after calling close_cimages_image(). Safe to call with NULL chandle.

*/

void close_cimages_image(chandle *ch)
{
   log_routine("close_cimages_image");

   if (ch) {
      if (ch->f)
	 if (fclose(ch->f) != 0)
	    log_error(ERR_FILE_CLOSE,"");
      ip_free(ch);
   }
   pop_routine();
}
      



/* Convert c_images image type into our type */
static int convert_type(int ctype, int packing)
{
   int type;

   switch (ctype) {
    case CI_UWORD:
      type = IM_LONG;
      break;
    case CI_USWORD:
      type = IM_LONG;	/* Have to upcast to maintain accuracy */
      break;
    case CI_UCHAR:
      type = (packing==BIT_PACK || packing==REV_PACK) ? IM_BINARY : IM_BYTE;
      break;
    default:
      type = IM_TYPEMAX;	/* To signal invalid type */
      break;
   }
   if (packing != NO_PACK && ctype != CI_UCHAR)
      type = IM_TYPEMAX;
   return type;
}



/* Switch endian of short int */
static int ses(unsigned short int x)
{
   return ((x & 0xff) << 8) + ((x & 0xff00) >> 8);
}

/* Switch endian of long int */
static unsigned int sel(unsigned long x)
{
   return ((x & 0xff) << 24) + ((x & 0xff00) << 8)
	      + ((x & 0xff000000) >> 24) + ((x & 0xff0000) >> 8);
}

