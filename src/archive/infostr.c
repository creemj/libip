/* $Id$ */
/*
   Module: ip/infostr.c
   Author: M.J.Cree

   Infostr Object for displaying messages in display window.

*/


#include <stdlib.h>

#include <ip/internal.h>
#include <ip/infostr.h>

/* #define DEBUG */
#include <debug/debug.h>

static char rcsid[] = "$Id$";


#define XENDOFFSET 4
#define YENDOFFSET 2

/* Flags field of infostr structure can take */

#define IFS_NEEDTOCLR 0x01
#define IFS_REDISPLAY 0x02
#define IFS_OPTPALETTE 0x04





/*

NAME:

   alloc_infostr() \- Allocate an infostr object

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/infostr.h>

   infostr * alloc_infostr( origin, width, lines, ptsize )
   coord origin;
   int width;
   int lines;
   int ptsize;

DESCRIPTION:

   Allocates an infostr object, for the displaying of information,
   usually brief strings, in a little window within the display
   canvas.

   The infostr object is located at the coordinate 'origin' in the
   display window, has a width of 'width' pixels, consists of 'lines'
   lines of text, and uses a font size of 'ptsize' points.  At present
   'lines' must be set to one, as only one line of text display is
   currently supported.  The infostr object is displayed/updated when
   infostr_display() is called.  The text message displayed can be set
   with infostr_set().

   An example of use is:

\|   infostr *ifs;
\|
\|   ifs = alloc_infostr((coord){0,0}, 400, 1, 12);
\|   infostr_set(ifs, IFS_STRING, "This is a message");
\|   infostr_display(ifs);
\|       ...
\|   free_infostr(ifs);

LIMITATIONS:

   Only one line of information can be displayed, therefore 'lines'
   must be set to one.

ERRORS:

   ERR_NOT_IMPLEMENTED  - lines wasn't set to one.
   ERR_OUT_OF_MEM

SEE ALSO:

   free_infostr()
   infostr_set()           infostr_get()
   infostr_display()
   infostr_getstr()

*/

infostr *alloc_infostr(coord origin, int width, int lines, int ptsize)
{
   int xwidth, skip;
   infostr *ifs;

   log_routine("alloc_infostr");

   if (lines != 1) {
      log_error(ERR_NOT_IMPLEMENTED);
      pop_routine();
      return NULL;
   }

   if ((ifs = ip_malloc(sizeof(infostr))) == NULL) {
      return NULL;
   }

   xwidth = display_labelwidth("x", ptsize);
   skip = (int)ROUND(1.2 * ptsize);

   ifs->id = FLG_NEWIMAGE;
   ifs->origin = origin;
   ifs->charsize.x = (width-2*XENDOFFSET) / xwidth;
   ifs->charsize.y = lines;
   ifs->size.x = width;
   ifs->size.y = (lines-1)*skip + ptsize + 2*YENDOFFSET + 1;
   ifs->str = NULL;
   ifs->ptsize = ptsize;
   ifs->skip = skip;
   ifs->baseline = ptsize - 3;
   ifs->lightpen = 255;
   ifs->sdwpen = 0;
   ifs->bgpen = 128;
   ifs->textpen = OVL_WHITE;
   ifs->flag = IFS_REDISPLAY;

   pop_routine();
   return ifs;
}




/*

NAME:

   infostr_display() \- Display an infostr object in the display canvas

PROTOTYPE:

   #include <ip/ip.h>

   int  infostr_display( ifs )
   infostr *ifs;

DESCRIPTION:

   Update all the imagery of the infostr object 'ifs' in the display
   window.  This routine must be called for the infostr object to be
   displayed initially and after any sequence of calls to
   infostr_set() to cause an update to the imagery.

ERRORS:

   ERR_OUT_OF_MEM

SEE ALSO:

   alloc_infostr()         free_infostr()
   infostr_set()           infostr_get()
   infostr_getstr()

*/


int infostr_display(infostr *ifs)
{
   image *im;
   int id;
   int Nx, Ny;
   coord start, end;

   log_routine("infostr_display");


   if (ifs->flag & IFS_REDISPLAY) {
      
      D(bug("infostr_display: Redrawing image.\n"));

      if ((im = alloc_image(IM_BYTE, ifs->size.x, ifs->size.y)) == NULL) {
	 pop_routine();
	 return ip_error;
      }

      im_set(im, ifs->bgpen);

      Nx = im->size.x;
      Ny = im->size.y;

      end.y = start.x = start.y = 0;
      end.x = Nx-1;
      im_drawline(im, ifs->lightpen, start, end);
      start.x = 0;
      start.y = end.y = Ny-1;
      end.x = Nx-1;
      im_drawline(im, ifs->sdwpen, start, end);
      
      end.x = start.x = start.y = 0;
      end.y = Ny-2;
      im_drawline(im, ifs->lightpen, start, end);
      start.y = 0;
      start.x = end.x = Nx-1;
      end.y = Ny-2;
      im_drawline(im, ifs->sdwpen, start, end);
   
      if (ifs->flag & IFS_OPTPALETTE)
	 id = display_imagex(ifs->id, im,ifs->origin,0,display_palentries()-1);
      else
	 id = display_image(ifs->id, im, ifs->origin);
      
      free_image(im);

      if (id < 0) {
	 pop_routine();
	 return ip_error;
      }

      if (ifs->id == FLG_NEWIMAGE) ifs->id = id;

      ifs->flag &= ~IFS_NEEDTOCLR;
      ifs->flag &= ~IFS_REDISPLAY;
   }

   if (ifs->str) {
      if (ifs->flag & IFS_NEEDTOCLR) {
	 D(bug("infostr_display: Clearing before showing string.\n"));
	 display_clear(ifs->id, FLG_OVERLAY);
      }
      ifs->flag |= IFS_NEEDTOCLR;
      start.x = XENDOFFSET;
      start.y = YENDOFFSET+ifs->baseline;
      if (display_label(ifs->id, ifs->str, ifs->textpen, ifs->ptsize, 
			start) != OK) {
	 pop_routine();
	 return ip_error;
      }
   }else{
      ifs->flag &= ~IFS_NEEDTOCLR;
      display_clear(ifs->id, FLG_OVERLAY);
   }

   pop_routine();
   return OK;
}




/*

NAME:

   infostr_get() \- Get an attribute of an infostr object

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/infostr.h>

   datum  infostr_get( ifs, action )
   infostr *ifs;
   int action;

DESCRIPTION:

   Get back some property of the infostr object 'ifs'.  The possible 
   actions are:

\|   Action     Type       Description
\| IFS_STRING    char * Currently displayed message.   
\| IFS_CHARSIZE  coord  Approximate size of imagery of
\|                      infostr object in characters.
\| IFS_SIZE      coord  Exact size of imagery of infostr
\|                      object in pixels.
\| IFS_ORIGIN    coord  Location of infostr imagery in display
\|                      window.
\| IFS_TEXTPEN   int    Overlay pen used to display text.
\| IFS_LIGHTPEN  int    Palette pen colour used to display
\|                      highlighted borders.
\| IFS_SDWPEN    int    Palette pen colour used to display
\|                      borders in shadow.
\| IFS_BGPEN     int    Background (general) shading of
\|                      infostr object.

ERRORS:

   ERR_BAD_ACTION

SEE ALSO:

   alloc_infostr()         free_infostr()
   infostr_set()
   infostr_display()
   infostr_getstr()

*/

datum infostr_get(infostr *ifs, int action)
{
   datum res;
   
   log_routine("infostr_get");
   
   switch(action) {
    case IFS_STRING:
      res.ptr = ifs->str;
      break;
    case IFS_TEXTPEN:
      res.b = ifs->textpen;
      break;
    case IFS_LIGHTPEN:
      res.b = ifs->lightpen;
      break;
    case IFS_BGPEN:
      res.b = ifs->bgpen;
      break;
    case IFS_SDWPEN:
      res.b = ifs->sdwpen;
      break;
    case IFS_SIZE:
      res.pt = ifs->size;
      break;
    case IFS_CHARSIZE:
      res.pt = ifs->charsize;
      break;
    case IFS_ORIGIN:
      res.pt = ifs->origin;
      break;
    default:
      log_error(ERR_BAD_ACTION);
      res.d = 0;
      break;
   }

   pop_routine();
   return res;
}



/*

NAME:

   infostr_set() \- Set an attribute of an infostr object

PROTOTYPE:

   #include <ip.h/ip.h>
   #include <ip.h/infostr.h>

   int infostr_set( ifs, action, ... )
   infostr *ifs;
   int action;

DESCRIPTION:

   Set a property of the infostr object 'ifs' to be some particular
   value.  Possible actions are:

\|   Action   Extra Args       Description
\| IFS_STRING    char * Message to display.
\| IFS_ORIGIN    coord  Location of infostr imagery in display
\|                      window.
\| IFS_TEXTPEN   int    Overlay pen used to display text.
\| IFS_LIGHTPEN  int    Palette pen colour used to display
\|                      highlighted borders.
\| IFS_SDWPEN    int    Palette pen colour used to display
\|                      borders in shadow.
\| IFS_BGPEN     int    Background (general) shading of
\|                      infostr object.
\| IFS_OPTIMUMPAL int   Display as if optimum palette has been
\|                      selected if argument is TRUE,
\|                      otherwise display with normal palette.

   None of these changes take affect until infostr_display() is
   called.

   See alloc_infostr() for an example of using infostr_set().

ERRORS:

   ERR_BAD_ACTION
   ERR_OUT_OF_MEM
   ERR_BAD_PARAMETER
   ERR_OOR_PARAMETER

SEE ALSO:

   alloc_infostr()         free_infostr()
   infostr_get()
   infostr_display()
   infostr_getstr()

*/

int infostr_set(infostr *ifs, int action, ...)
{
   va_list ap;
   char *str;
   int pen;
   coord pt;

   va_start(ap, action);
   log_routine("infostr_set");

   switch (action) {
    case IFS_STRING:

      str = va_arg(ap, char *);

      if (ifs->str) {
	 ip_free(ifs->str);
      }

      if (str) {
	 if ((ifs->str = ip_malloc(strlen(str)+1)) == NULL)
	    goto exit_is;
	 strcpy(ifs->str, str);
      }else{
	 ifs->str = NULL;
      }

      break;

    case IFS_ORIGIN:
      pt = va_arg(ap, coord);
      if (pt.x < 0 || pt.y < 0) {
	 log_error(ERR_BAD_PARAMETER);
	 pop_routine();
	 goto exit_is;
      }
      ifs->origin = pt;
      ifs->flag |= IFS_REDISPLAY;
      break;

    case IFS_TEXTPEN:

      pen =  va_arg(ap, int);
      if (NOT ip_parm_inrange("text_pen",pen,OVL_START,OVL_END))
	 goto exit_is;
      ifs->textpen = pen;
      ifs->flag |= IFS_REDISPLAY;
      break;

    case IFS_LIGHTPEN:

      pen =  va_arg(ap, int);
      if (NOT ip_parm_inrange("light_pen",pen,0,255))
	 goto exit_is;
      ifs->lightpen = pen;
      ifs->flag |= IFS_REDISPLAY;
      break;

    case IFS_SDWPEN:

      pen =  va_arg(ap, int);
      if (NOT ip_parm_inrange("shadow_pen",pen,0,255))
	 goto exit_is;
      ifs->sdwpen = pen;
      ifs->flag |= IFS_REDISPLAY;
      break;

    case IFS_BGPEN:
      
      pen =  va_arg(ap, int);
      if (NOT ip_parm_inrange("background_pen",pen,0,255))
	 goto exit_is;
      ifs->bgpen = pen;
      ifs->flag |= IFS_REDISPLAY;
      break;

    case IFS_OPTIMUMPAL:

      pen =  va_arg(ap, int);
      if (pen)
	 ifs->flag |= IFS_OPTPALETTE;
      else
	 ifs->flag &= ~IFS_OPTPALETTE;
      break;

    default:

      log_error(ERR_BAD_ACTION);
      break;

   }
 
   pop_routine();

 exit_is:

   va_end(ap);
   return ip_error;
}
   




/*

NAME:

   infostr_getstr() \- Get message back from user

PROTOTYPE:

   #include <ip.h/ip.h>
   #include <ip.h/infostr.h>

   char * infostr_getstr( ifs, msg )
   infostr *ifs;
   char *msg;

DESCRIPTION:

   The message 'msg' is displayed in the infostr object 'ifs' and then
   input is waited for from the user.  The string entered by the user 
   is returned.  This is a simple `get info from user'' routine. 

LIMITATIONS:

   This routine in its initial incantations was rather unreliable.  It
   appears to be much more stable now, but I recommend it is only used
   with care.

ERRORS:

   All sorts....

SEE ALSO:

   alloc_infostr()         free_infostr()
   infostr_set()           infostr_get()
   infostr_display()

*/

char *infostr_getstr(infostr *ifs, char *msg)
{
   image *blank;
   static char input[128];
   int width[128];
   int idx;
   char chr[2];
   coord pos, tpos;
   int chrwidth;

   log_routine("infostr_getstr");

   infostr_set(ifs, IFS_STRING, msg);
   infostr_display(ifs);

   chrwidth = display_labelwidth("W", ifs->ptsize);
   chrwidth = (chrwidth == 0) ? ifs->ptsize : chrwidth;
   blank = alloc_image(IM_BYTE, chrwidth, ifs->ptsize);
   im_set(blank, ifs->bgpen);

   for (idx = chrwidth = 0; chrwidth==0 && idx<10; idx++)
      chrwidth = display_labelwidth(ifs->str, ifs->ptsize);

   pos.x = XENDOFFSET + chrwidth;
   pos.y = YENDOFFSET + ifs->baseline;

   input[0] = 0;
   chr[0] = chr[1] = 0;
   idx = 0;

   while (chr[0] != '\n' && chr[0] != '\015') {
      display_label(ifs->id, "_ ", ifs->textpen, ifs->ptsize, pos);
      chr[0] = display_keypress(FLG_WAIT);
      tpos.x = pos.x;
      tpos.y = YENDOFFSET;
      display_overlay(ifs->id, blank, tpos, NO_FLAG);
      if (chr[0] == '\b') {
	 if (idx > 0) {
	    idx--;
	    input[idx] = 0;
	    pos.x -= width[idx];
	    tpos.x = pos.x;
	    tpos.y = YENDOFFSET;
	    display_overlay(ifs->id, blank, tpos, NO_FLAG);
	 }else{
	    fprintf(stderr,"\a");
	 }
      }else if (chr[0] != '\n' && chr[0] != '\015') {
	 chrwidth = display_labelwidth(chr, ifs->ptsize);
	 if (pos.x + chrwidth < ifs->size.x-XENDOFFSET) {
	    input[idx] = chr[0];
	    width[idx] = chrwidth;
	    idx++;
	    display_label(ifs->id, chr, ifs->textpen, ifs->ptsize, pos);
	    pos.x += chrwidth;
	 }else{
	    fprintf(stderr,"\a");
	 }
      }
   }
   input[idx] = 0;
   free_image(blank);

   pop_routine();
   return input;
}




/*

NAME:

   free_infostr() \- Free up an infostr object

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/infostr.h>

   int  free_infostr( ifs )
   infostr *ifs;

DESCRIPTION:

   Free up an infostr object. The imagery is removed from the display.

SEE ALSO:

   alloc_infostr()

*/

int free_infostr(infostr *ifs)
{
   display_free(ifs->id);
   if (ifs->str)
      ip_free(ifs->str);
   ip_free(ifs);
   return OK;
}



/*

NAME:

   dump_infostr() \- Dump particulars of an infostr object

PROTOTYPE:

   #include <ip/ip.h>
   #include <ip/infostr.h>

   void  dump_infostr( ifs )
   infostr *ifs;

DESCRIPTION:

   Dump to stdout the particulars of the infostr object 'ifs'.  The
   fields of the 'ifs' object are private and this is the only routine
   that allows any view of them. This is intended only for debugging
   purposes.

SEE ALSO:

   alloc_infostr()

*/


void dump_infostr(infostr *ifs)
{
   int tmp, prev;

   fprintf(stdout, "\nInfostr at %p\n\n", ifs);

   fprintf(stdout, "   ID number: \t\t%4d\n", ifs->id);
   fprintf(stdout, "   Origin: \t\t%4d, %4d\n", ifs->origin.x, ifs->origin.y);
   fprintf(stdout, "   Character size: \t%4d, %4d\n", 
	   ifs->charsize.x, ifs->charsize.y);
   fprintf(stdout, "   Size: \t\t%4d, %4d\n", ifs->size.x, ifs->size.y);
   fprintf(stdout, "   Point size: \t\t%4d\n", ifs->ptsize);
   fprintf(stdout, "   Skip: \t\t%4d\n", ifs->skip);
   fprintf(stdout, "   Baseline: \t\t%4d\n", ifs->baseline);
   fprintf(stdout, "   Lightpen: \t\t%4d\n", ifs->lightpen);
   fprintf(stdout, "   Shadow pen: \t\t%4d\n", ifs->sdwpen);
   fprintf(stdout, "   Background pen: \t%4d\n", ifs->bgpen);
   fprintf(stdout, "   Text pen: \t\t%4d\n", ifs->textpen);
   fprintf(stdout, "   Flags: \t\t");
   prev = FALSE;
   if (ifs->flag & IFS_REDISPLAY) {
      fputc(prev ? '|' : '[', stdout);
      fprintf(stdout, "REDISPLAY");
      prev = TRUE;
   }
   if (ifs->flag & IFS_NEEDTOCLR) {
      fputc(prev ? '|' : '[', stdout);
      fprintf(stdout, "NEEDTOCLR");
      prev = TRUE;
   }
   if (ifs->flag & IFS_OPTPALETTE) {
      fputc(prev ? '|' : '[', stdout);
      fprintf(stdout, "OPTPALETTE");
      prev = TRUE;
   }
   tmp = ifs->flag & ~(IFS_REDISPLAY|IFS_NEEDTOCLR|IFS_OPTPALETTE);
   if (tmp) {
      fputc(prev ? '|' : '[', stdout);
      fprintf(stdout,"Unknown bits:0x%x",tmp);
      prev = TRUE;
   }
   if (prev) fputc(']', stdout);
   fputc('\n',stdout);
   fprintf(stdout, "   String: \"%s\"\n\n", (ifs->str) ? ifs->str : "NULL");
}
