/* $Id$ */
/*

   Module: ip/register.c
   Author: M.J.Cree

   Does automatic registration of images.

   NB. To include im_register() in your programme you must link in
       John Hipwell's registration library with -lcideciyan.   

*/
/*
 * $Log$
 * Revision 1.2  1998/07/21 02:36:26  cree
 * Change cideciyan library globals to start with cd_
 *
 * Revision 1.1  1997/09/02  14:02:57  mph489
 * Initial revision
 *
 */


#include <stdlib.h>
#include <math.h>
#include <ip/internal.h>

static char rcsid[] = "$Id$";

/* This routine comes from John Hipwell's code. */

int DoRegistration(float *ref, float *towarp, int *size,
		   double *rot, double *scale, double *tx, double *ty,
		   float *cx,
		   float max_scale, float max_rot, float max_trans);


/* These are declared as extern in DoReg code. So must declare them here */

char cd_dbg_file[1];
int cd_verbose;
int cd_Ne;
int cd_extend_image;
int cd_float_input;
int cd_debug_output;
int cd_rot_trans_only;
int cd_trans_only;
double cd_start_freq;
double cd_end_freq;




/*

NAME:

   im_register() \- Register one image to another

PROTOTYPE:

   #include <ip/ip.h>

   regparms * im_register( ref, towarp, maxtrans, maxrot, maxscale, flags )
   image *ref;
   image *towarp;
   double maxtrans;
   double maxrot;
   double maxscale;
   int flags;

DESCRIPTION:

   Register one image against another, and return warping parameters.

   'Ref' is the reference image and 'towarp' is the image to be
   registered to 'ref'.  The rigid transformation required to register
   'towarp' to 'ref' is returned as a regparms object.  You should
   specify maximum translations, etc. permissable in 'maxtrans', etc.
   Rotations are specified in radians.

   'Flag' can be one of:

\&   NO_FLAG      Do all of rotation, scaling and translation.
\&   FLG_ROTTRANS Do rotation and translation only, no scaling.
\&   FLG_TRANS    Do translation only, no scaling nor rotation.

\&-   At present im_register() is a stub to call up John Hipwell''s
   DoRegister() function, therefore when using this routine you must
   also link with John''s cideciyan library with -lcideciyan.

   The returned regparms object should be freed with free_regparms().

ERRORS:

   ERR_UNSUPPORTED_TYPE - If image not FLOAT.
   ERR_BAD_FLAG
   ERR_IMAGES_NOT_SAME_SIZE
   ERR_IMAGES_NOT_SAME_TYPE

SEE ALSO:

   im_warp()   DoRegister()  free_regparms()

*/

regparms * im_register(image *ref, image *towarp,
		double maxtrans, double maxrot, double maxscale, int flags)
{
   float mt, mr, ms, cc;
   double tx,ty;
   regparms *parms;

   log_routine("im_register");

   if (NOT images_compatible(ref,towarp))
      return NULL;

   if (ref->type != IM_FLOAT) {
      log_error(ERR_UNSUPPORTED_TYPE);
      pop_routine();
      return NULL;
   }

   if (NOT flag_ok(flags,FLG_ROTTRANS, FLG_TRANS, LASTFLAG))
      return NULL;

   if ((parms = alloc_regparms()) == NULL) {
      pop_routine();
      return NULL;
   }

   cd_verbose = cd_extend_image = cd_debug_output = 0;
   cd_start_freq = 6;
   cd_end_freq = 93;
   cd_rot_trans_only =  (flags == FLG_ROTTRANS);
   cd_trans_only =  (flags == FLG_TRANS);

   ms = maxscale;
   mr = maxrot*180/M_PI;
   mt = maxtrans;

   DoRegistration(ref->image.v, towarp->image.v, &ref->Nx, 
		  &parms->rotate, &parms->scale, &tx, &ty, &cc,
		  ms, mr, mt);


   parms->correlation = cc;
   parms->trans.x = tx;
   parms->trans.y = ty;
   parms->rotate *= M_PI/180;

   pop_routine();
   return parms;
}




