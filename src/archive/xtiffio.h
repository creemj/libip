/* $Id$ */
/*
   Header: ip/xtiffio.h
   Author: M.J.Cree

   Implements the in-house tag TIFFTAG_OPHTHALMICINFO

   Based upon the file which had the following heading:

 *  xtiffio.h -- Public interface to Extended TIFF tags
 *
 *    written by: Niles D. Ritter
 */

#ifndef IP_XTIFFIO_H
#define IP_XTIFFIO_H

#include <tiffio.h>

/* Tag for storing retinal image information.
   At present this tag is not registered - a little naughty :-) */

#define TIFFTAG_OPHTHALMICINFO     55439

/* 
 *  This flag may be passed in to TIFFPrintDirectory() to
 *  indicate that those particular field values should
 *  be printed out in full, rather than just an indicator
 *  of whether they are present or not.
 */
#define	TIFFPRINT_OICOMPLETE	0x08000000

/**********************************************************************
 *    Nothing below this line should need to be changed by the user.
 **********************************************************************/

#if defined(__cplusplus)
extern "C" {
#endif

extern TIFF* XTIFFOpen(const char* name, const char* mode);
extern TIFF* XTIFFFdOpen(int fd, const char* name, const char* mode);
extern void  XTIFFClose(TIFF *tif);

#if defined(__cplusplus)
}
#endif

#endif
