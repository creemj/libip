/* $Id$ */
/*
   Header: ip/visilog.h
   Author: M.J.Cree

   Declarations for interfacing (reading/writing) to visilog images.

*/

#ifndef IP_VISILOG_H
#define IP_VISILOG_H

#include <ip/types.h>
#include <ip/image.h>
#include <stdio.h>

/* Visilog image handle */

typedef struct {
   int vtype;
   int type; 
   coord size;
   FILE *f;
   const char *fname;
} vhandle;


/* Entry points to functions. */

extern vhandle *open_visilog_image( const char *name );
extern vhandle *open_visilog_imagex(FILE *f, const char *fname, unsigned char hdr[4], int flag);
extern image *read_visilog_image(vhandle *);
extern image *read_visilog_imagex(vhandle *, int first_row, int nrows);
extern int write_visilog_image(const char *fname, image *im);
extern int write_visilog_imagex(const char *fname, image *im, int flag);
extern void close_visilog_image(vhandle *);

#endif
