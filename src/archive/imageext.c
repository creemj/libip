/* $Id$ */
/*
   Module: ip/imageext.c
   Author: M.J.Cree

   Extensions to image.c
   These routines are only used very rarely and thus are in a module
   by themselves since most programmes won't need them.

*/
/*
 * $Log$
 * Revision 1.1  1997/10/14 09:21:33  mph489
 * Initial revision
 *
 */


#include <stdlib.h>

#include <ip/internal.h>

static char rcsid[] = "$Id$";



/*

NAME:

   im_attach() \- Attach an image's data array to another image

PROTOTYPE:

   #include <ip/ip.h>

   int  im_attach( dest, src )
   image *dest, *src;

DESCRIPTION:

   This provides a means to attach data arrays of one image to another
   without having to do a full copy.  It is primarily intended for use
   only within in the IP library or for designers of new image
   processing primitives.

   Im_attach() attaches the image data arrays of the image handle
   'src' to the image handle 'dest'.  The image handle 'src' is
   completely freed up, but its image data can stil be accessed
   through 'dest'.  The old image data associated with 'dest' on entry
   is lost.  This procedure is fast - it plays with image pointers
   rather than doing a full data copy.

   The primary purpose for this routine is to design image processing
   primitives that have to work on tempory image storage for the
   actual processing but want to have a calling procedure that is
   consistent with the image processing having been done in place.
   For example, im_example() below is a routine that takes an image
   'im' as an argument and is defined as if the processing is done in
   place.  Whereas, in fact, it uses a tempory image 'temp'.

\|   void im_example(image *im)
\|   {
\|      image *tmp;
\|
\|      tmp = \\ Some image processing operation on im.
\|
\|      im_attach(im, tmp);
\|   }

   If either the 'dest' or the 'src' image have had an image array
   loaded through the routine im_load() then a complete image copy is
   instituted, since the image data pointers are mainted by the user
   programme, not the ip lib.  Free_image() is then called on 'src'.
   This means that if 'src' has had im_load() called on it, then the
   array used in im_load() is free to be used by the caller again.

LIMITATIONS:

   I havn''t tested this routine yet!

ERRORS:

   ERR_NOT_SAME_TYPE
   ERR_NOT_SAME_SIZE

SEE ALSO:

   alloc_image()   free_image()
   im_load()

*/

int im_attach(image *dest, image *src)
{
   int i;

   log_routine("im_attach");

   if (NOT images_compatible(dest, src))
      return ip_error;

   if ((dest->status & IMS_USERARRAY) || (src->status & IMS_USERARRAY)) {

      /* Some of the image arrays are not IP lib allocated - safer not
	 to play around with them since we do not know what the user
	 may be doing with them.  So do a complete image data copy.  */

      memcpy(dest->image.v, src->image.v,
	     (size_t)src->size.x * src->size.y * ip_typesize[src->type]);
      if (src->type == IM_PALETTE) {
	 for (i=0; i<256; i++)
	    dest->palette[i] = src->palette[i];
      }
      free_image(src);

   }else{

      /* Attach src data array to dest data array */

      ip_free(dest->image.v);
      dest->image.v = src->image.v;
      ip_free(dest->imrow.v);
      dest->imrow.v = src->imrow.v;
      if (dest->type == IM_PALETTE) {
	 ip_free(dest->palette);
	 dest->palette = src->palette;
      }
      ip_free(src);

   }

   pop_routine();
   return ip_error;
}




/*

NAME:

   im_load() \- Load an array of pixel values into image

PROTOTYPE:

   #include <ip/ip.h>

   void im_load( im, array, flag )
   image *im;
   void *array;
   int flag;

DESCRIPTION:

   Load image 'im' with the values from a two-dimensional array,
   arranged as 'im'->size.y sequential rows, each row containing
   'im'->size.x sequential pixel values of the same type as the
   image. Thus, 'array' is arranged as x increasing first then y.
   'Array' must have the same dimensions and the same type as the
   image.

   By default, it is assumed that 'array' will remain intact (and
   untouched by the calling programme) throughout the existence of the
   image.  Thus im_load() may use all or part of 'array' for image
   memory storage. It may also modify any value within 'array'.  You
   are only free to modify or deallocate 'array' once free_image() (or
   im_attach() with the image as the second argument) has been called
   on the image.  Furthermore, even though im_load() is permitted to
   use 'array' by default, do not assume that this means the internal
   image data storage is neccesarily in the same format as initially
   passed in 'array'.

   If the default action is not acceptable then set 'flag' to
   FLG_COPYARRAY, which forces im_load() to allocate separate storage
   for the image data and to copy all the contents of 'array'.  You
   are then free to modify or deallocate 'array' straight after the
   call to im_load().

SEE ALSO:

   alloc_image()   im_attach()

*/


void im_load(image *im, void *array, int flag)
{
   int j;

   if (flag == FLG_COPYARRAY) {
      memcpy(im->image.v, array,
		    (size_t)im->size.x * im->size.y * ip_typesize[im->type]);
   }else{
      ip_free(im->image.v);
      im->image.v = array;
      im->status |= IMS_USERARRAY;
      for (j=0; j<im->size.y; j++) {
	 im->imrow.v[j] = (void *)(im->image.b
				      + (j*im->size.x*ip_typesize[im->type]));
      }
   }
}

