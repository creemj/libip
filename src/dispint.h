/* $Id$ */
/*
   Header: ip/dispint.h
   Author: M.J.Cree


*/


extern double ip_disp_gamma;
extern double ip_disp_contrast;
extern int ip_disp_pal;
