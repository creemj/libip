/*
   Module: ip/maxmin.c
   Author: M.J.Cree

   Performs maximum/minimum operations between images.

ENTRY POINTS:

   im_maximum()
   im_minimum()

*/

#include <ip/ip.h>

#include "internal.h"
#include "function_ops.h"

#ifdef HAVE_SIMD
#include "simd/binops_simd.h"
#endif


#define generate_pixel_operator(t, name, op)				\
    static inline t##_type pixel_##name##_##t(t##_type x1, t##_type x2)	\
    {									\
	return op(x1, x2);						\
    }

generate_pixel_operator(UBYTE, min, MIN)
generate_pixel_operator(BYTE, min, MIN)
generate_pixel_operator(USHORT, min, MIN)
generate_pixel_operator(SHORT, min, MIN)
generate_pixel_operator(ULONG, min, MIN)
generate_pixel_operator(LONG, min, MIN)
generate_pixel_operator(FLOAT, min, MIN)
generate_pixel_operator(DOUBLE, min, MIN)

generate_pixel_operator(UBYTE, max, MAX)
generate_pixel_operator(BYTE, max, MAX)
generate_pixel_operator(USHORT, max, MAX)
generate_pixel_operator(SHORT, max, MAX)
generate_pixel_operator(ULONG, max, MAX)
generate_pixel_operator(LONG, max, MAX)
generate_pixel_operator(FLOAT, max, MAX)
generate_pixel_operator(DOUBLE, max, MAX)


#define generate_image_process(t, op)					\
    static void image_##op##_##t(ip_image *d, ip_image *s)		\
    {									\
	for (int j=0; j<d->size.y; j++) {				\
	    t ## _type *dptr = IM_ROWADR(d, j, v);			\
	    t ## _type *sptr = IM_ROWADR(s, j, v);			\
	    for (int i=0; i<d->size.x; i++) {				\
		*dptr = pixel_ ## op ## _ ## t(*dptr, *sptr);		\
		dptr++; sptr++;						\
	    }								\
	}								\
    }

generate_image_process(BYTE, min)
generate_image_process(UBYTE, min)
generate_image_process(SHORT, min)
generate_image_process(USHORT, min)
generate_image_process(LONG, min)
generate_image_process(ULONG, min)
generate_image_process(FLOAT, min)
generate_image_process(DOUBLE, min)

generate_image_process(BYTE, max)
generate_image_process(UBYTE, max)
generate_image_process(SHORT, max)
generate_image_process(USHORT, max)
generate_image_process(LONG, max)
generate_image_process(ULONG, max)
generate_image_process(FLOAT, max)
generate_image_process(DOUBLE, max)


static improc_function_II image_minimum[IM_TYPEMAX] = {
    [IM_BYTE] = image_min_BYTE,
    [IM_UBYTE] = image_min_UBYTE,
    [IM_SHORT] = image_min_SHORT,
    [IM_USHORT] = image_min_USHORT,
    [IM_LONG] = image_min_LONG,
    [IM_ULONG] = image_min_ULONG,
    [IM_FLOAT] = image_min_FLOAT,
    [IM_DOUBLE] = image_min_DOUBLE
};

static improc_function_II image_maximum[IM_TYPEMAX] = {
    [IM_BYTE] = image_max_BYTE,
    [IM_UBYTE] = image_max_UBYTE,
    [IM_SHORT] = image_max_SHORT,
    [IM_USHORT] = image_max_USHORT,
    [IM_LONG] = image_max_LONG,
    [IM_ULONG] = image_max_ULONG,
    [IM_FLOAT] = image_max_FLOAT,
    [IM_DOUBLE] = image_max_DOUBLE
};



/*

NAME:

   im_maximum() \- Calculate pixelwise maximal value between two images

PROTOTYPE:

   #include <ip/ip.h>

   int  im_maximum( dest, src )
   ip_image *dest;
   ip_image *src;

DESCRIPTION:

   Calculate the pixel by pixel maximum of the two images and leave
   result in dest.

LIMITATIONS:

   Not implemented for complex images.

ERRORS:

   ERR_NOT_SAME_SIZE
   ERR_NOT_SAME_TYPE
   ERR_BAD_TYPE

*/

int im_maximum(ip_image *dest, ip_image *src)
{
    ip_log_routine("im_maximum");

    if (!image_valid(src) || !image_valid(dest))
	return ip_error;

    if (NOT images_compatible(dest, src))
	return ip_error;

    if (NOT (image_type_integer(src) || image_type_real(src)
	     || src->type == IM_BINARY)) {
	ip_log_error(ERR_BAD_TYPE);
	ip_pop_routine();
	return ip_error;
    }

    if (src->type == IM_BINARY) {
	im_or(dest, src);
	ip_pop_routine();
	return OK;
    }

#ifdef HAVE_SIMD
    if (IP_USE_HWACCEL && ip_simd_max_bytype[src->type]) {
        ip_simd_max_bytype[src->type](dest, src);
        ip_pop_routine();
        return OK;
    }
#endif

    if (image_maximum[src->type])
	image_maximum[src->type](dest, src);
    else
	ip_log_error(ERR_NOT_IMPLEMENTED);

    ip_pop_routine();
    return OK;
}




/*

NAME:

   im_minimum() \- Calculate pixelwise minimal value between two images

PROTOTYPE:

   #include <ip/ip.h>

   int  im_minimum( dest, src )
   ip_image *dest;
   ip_image *src;

DESCRIPTION:

   Calculate the pixel by pixel minimum of the two images and leave
   result in dest.

LIMITATIONS:

   Not implemented for complex images.

ERRORS:

   ERR_NOT_SAME_SIZE
   ERR_NOT_SAME_TYPE
   ERR_BAD_TYPE

*/


int im_minimum(ip_image *dest, ip_image *src)
{
    ip_log_routine("im_minimum");

    if (!image_valid(src) || !image_valid(dest))
	return ip_error;

    if (NOT images_compatible(dest, src))
	return ip_error;

    if (NOT (image_type_integer(src) || image_type_real(src)
	     || src->type == IM_BINARY)) {
	ip_log_error(ERR_BAD_TYPE);
	ip_pop_routine();
	return ip_error;
    }

    if (src->type == IM_BINARY) {
	im_and(dest, src);
	ip_pop_routine();
	return OK;
    }

#ifdef HAVE_SIMD
    if (IP_USE_HWACCEL && ip_simd_min_bytype[src->type]) {
        ip_simd_min_bytype[src->type](dest, src);
        ip_pop_routine();
        return OK;
    }
#endif

    if (image_minimum[src->type])
	image_minimum[src->type](dest, src);
    else
	ip_log_error(ERR_NOT_IMPLEMENTED);

    ip_pop_routine();
    return OK;
}
