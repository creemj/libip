#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>

#include <ip/types.h>
#include <ip/image.h>
#include <ip/error.h>
#include <ip/proto.h>
#include <ip/flags.h>

#include "test-macros.h"
#include "timing.h"
#include "helpers.h"
#include "errorhdl.h"


char *program_name = "imadd-test";

/* im_add like operators */
#define PIXEL_OPERATION_ADD(dval, sval, d)  dval += sval
#define PIXEL_OPERATION_ADD_CLIPPED(dval, sval, d)	\
    dval += sval;					\
    CLIP_TO_PIXEL_TYPE_LIMITS(dval, d);

im_ii_int_DEFN(ADD)
im_ii_int_DEFN(ADD_CLIPPED)

im_ii_real_DEFN(ADD)

#define PIXEL_OPERATION_ADD_COMPLEX(dval, sval, d) \
    dval.r += sval.r;				   \
    dval.i += sval.i;

im_ii_complex_DEFN(ADD_COMPLEX)
im_ii_dcomplex_DEFN(ADD_COMPLEX)

/* im_addc like operators */
#define PIXEL_OPERATION_ADDC(dval, v, d)  dval += v
#define PIXEL_OPERATION_ADDC_CLIPPED(dval, v, d)	\
    dval += v;						\
    CLIP_TO_PIXEL_TYPE_LIMITS(dval, d);

im_ic_int_DEFN(ADDC)
im_ic_int_DEFN(ADDC_CLIPPED)

im_ic_real_DEFN(ADDC)

#define PIXEL_OPERATION_ADDC_COMPLEX(dval, val, d)	\
    dval.r += val.r;					\
    dval.i += val.i;

im_ic_complex_DEFN(ADDC_COMPLEX)
im_ic_dcomplex_DEFN(ADDC_COMPLEX)



long im_add_check(int type, ip_coord2d size, int flag, char *msg)
{
    ip_image *r, *d, *s;
    int failed = 0;
    double scale;
    double offset;
    long time;

    r = ip_alloc_image(type, size);

    scale = 1.2*(ip_image_type_info[type].maxvalue 
		 - ip_image_type_info[type].minvalue) / size.x;
    offset = ip_image_type_info[type].minvalue;
    im_create(r, (ip_coord2d){0,0}, scale, offset, 1.0, TI_HWEDGE);
    d = im_copy(r);
    failed |= compare_images(r, d, "Initial compare of reference", "r", "d");

    set_image_row_padding(r, 0x53);
    failed |= check_image_row_padding(r, 0x53, "Set padding check padding");
    failed |= compare_images(r, d, "Set padding check image data", "r", "d");

    s = ip_alloc_image(type, size);
    im_create(s, (ip_coord2d){0,0}, scale, 0.0, 1.0, TI_VWEDGE);
    
    if (flag == FLG_CLIP)
	im_ii_int_ADD_CLIPPED(r, s);
    else
	im_ii_int_ADD(r, s);

    im_add(d, s, flag);

    failed |= compare_images(r, d, "Image add check", "r", "d");
    failed |= check_image_row_padding(r, 0x53, "Image add padding check");

    time_start();
    for (int j=0; j<10; j++)
	im_add(d, s, flag);
    time = time_end() / 10;

    printf("%-22s [%5ld us; %4.2f ipc] [%s]\n", msg, time, ipc, failed ? "FAILED" : "OK");

    ip_free_image(s);
    ip_free_image(d);
    ip_free_image(r);

    return time;
}


long im_real_add_check(int type, ip_coord2d size, int flag, char *msg)
{
    ip_image *r, *d, *s;
    int failed = 0;
    double scale;
    double offset;
    long time;

    r = ip_alloc_image(type, size);

    if (type != IM_DOUBLE && type != IM_DCOMPLEX) {
	scale = 1.2*(ip_image_type_info[type].maxvalue 
		     - ip_image_type_info[type].minvalue) / size.x;
	offset = ip_image_type_info[type].minvalue;
    } else {
	scale = 1e50;
	offset = -1e25;
    }
    if (type_real(type)) {
	im_create(r, (ip_coord2d){0,0}, scale, offset, 1.0, TI_HWEDGE);
    } else {
	im_random(r, -1e30, 1e30, FLG_RANDOM_UNIFORM);
    }
    d = im_copy(r);
    failed |= compare_images(r, d, "Initial compare of reference", "r", "d");

    set_image_row_padding(r, 0x53);
    failed |= check_image_row_padding(r, 0x53, "Set padding check padding");
    failed |= compare_images(r, d, "Set padding check image data", "r", "d");

    s = ip_alloc_image(type, size);
    if (type_real(type)) {
	im_create(s, (ip_coord2d){0,0}, scale, 0.0, 1.0, TI_VWEDGE);
    } else {
	im_random(s, -1e30, 1e30, FLG_RANDOM_UNIFORM);	
    }

    if (type_real(type))
	im_ii_real_ADD(r, s);
    else if (type == IM_COMPLEX)
	im_ii_complex_ADD_COMPLEX(r,s);
    else
	im_ii_dcomplex_ADD_COMPLEX(r,s);

    im_add(d, s, flag);

    failed |= compare_images(r, d, "Image add check", "r", "d");
    failed |= check_image_row_padding(r, 0x53, "Image add padding check");

    time_start();
    for (int j=0; j<10; j++)
	im_add(d, s, flag);
    time = time_end() / 10;

    printf("%-22s [%5ld us; %4.2f ipc] [%s]\n", msg, time, ipc, failed ? "FAILED" : "OK");

    ip_free_image(s);
    ip_free_image(d);
    ip_free_image(r);

    return time;
}

long im_addc_check(int type, ip_coord2d size, int flag, double val, char *msg)
{
    ip_image *r, *d;
    int failed = 0;
    double scale;
    double offset;
    long time;

    r = ip_alloc_image(type, size);

    scale = 1.2*(ip_image_type_info[type].maxvalue 
		 - ip_image_type_info[type].minvalue) / size.x;
    offset = ip_image_type_info[type].minvalue;
    im_create(r, (ip_coord2d){0,0}, scale, offset, 1.0, TI_HWEDGE);
    d = im_copy(r);
    failed |= compare_images(r, d, "Initial compare of reference", "r", "d");

    set_image_row_padding(r, 0x53);
    failed |= check_image_row_padding(r, 0x53, "Set padding check padding");
    failed |= compare_images(r, d, "Set padding check image data", "r", "d");

    if (flag == FLG_CLIP)
	im_ic_int_ADDC_CLIPPED(r, val);
    else
	im_ic_int_ADDC(r, val);

    im_addc(d, val, flag);

    failed |= compare_images(r, d, "Image add check", "r", "d");
    failed |= check_image_row_padding(r, 0x53, "Image add padding check");

    time_start();
    for (int j=0; j<10; j++) 
	im_addc(d, val, flag);
    time = time_end() / 10;

    printf("%-22s [%5ld us; %4.2f ipc] [%s]\n", msg, time, ipc, failed ? "FAILED" : "OK");

    ip_free_image(d);
    ip_free_image(r);

    return time;
}


long im_real_addc_check(int type, ip_coord2d size, int flag, double val, char *msg)
{
    ip_image *r, *d;
    int failed = 0;
    double scale;
    double offset;
    long time;

    r = ip_alloc_image(type, size);

    if (type != IM_DOUBLE && type != IM_DCOMPLEX) {
	scale = 1.2*(ip_image_type_info[type].maxvalue 
		     - ip_image_type_info[type].minvalue) / size.x;
	offset = ip_image_type_info[type].minvalue;
    } else {
	scale = 1e50;
	offset = -1e25;
    }
    im_create(r, (ip_coord2d){0,0}, scale, offset, 1.0, TI_HWEDGE);
    d = im_copy(r);
    failed |= compare_images(r, d, "Initial compare of reference", "r", "d");

    set_image_row_padding(r, 0x53);
    failed |= check_image_row_padding(r, 0x53, "Set padding check padding");
    failed |= compare_images(r, d, "Set padding check image data", "r", "d");

    im_ic_real_ADDC(r, val);

    im_addc(d, val, flag);

    failed |= compare_images(r, d, "Image add check", "r", "d");
    failed |= check_image_row_padding(r, 0x53, "Image add padding check");

    time_start();
    for (int j=0; j<10; j++)
	im_addc(d, val, flag);
    time = time_end() / 10;

    printf("%-22s [%5ld us; %4.2f ipc] [%s]\n", msg, time, ipc, failed ? "FAILED" : "OK");

    ip_free_image(d);
    ip_free_image(r);

    return time;
}


long im_complex_addc_check(int type, ip_coord2d size, int flag, double u, double v, char *msg)
{
    ip_image *r, *d;
    int failed = 0;
    double scale;
    double offset;
    long time;

    r = ip_alloc_image(type, size);

    if (type != IM_DCOMPLEX) {
	scale = 1.2*(ip_image_type_info[type].maxvalue 
		     - ip_image_type_info[type].minvalue) / size.x;
	offset = ip_image_type_info[type].minvalue;
    } else {
	scale = 1e50;
	offset = -1e25;
    }
    im_random(r, offset, scale+offset, FLG_RANDOM_UNIFORM);
    d = im_copy(r);
    failed |= compare_images(r, d, "Initial compare of reference", "r", "d");

    set_image_row_padding(r, 0x53);
    failed |= check_image_row_padding(r, 0x53, "Set padding check padding");
    failed |= compare_images(r, d, "Set padding check image data", "r", "d");

    if (type == IM_COMPLEX)
	im_ic_complex_ADDC_COMPLEX(r,u,v);
    else
	im_ic_dcomplex_ADDC_COMPLEX(r,u,v);

    im_addc_complex(d, (ip_dcomplex){u, v}, flag);

    failed |= compare_images(r, d, "Image add check", "r", "d");
    failed |= check_image_row_padding(r, 0x53, "Image add padding check");

    time_start();
    for (int j=0; j<10; j++)
	im_addc_complex(d, (ip_dcomplex){u, v}, flag);
    time = time_end() / 10;

    printf("%-22s [%5ld us; %4.2f ipc] [%s]\n", msg, time, ipc, failed ? "FAILED" : "OK");

    ip_free_image(d);
    ip_free_image(r);

    return time;
}



int main(int argc, char *argv[])
{
    ip_image *i1, *i2;
    ip_coord2d size = {1023, 1025};
    int failed = 0;

    ip_register_error_handler(error_handler);

    EXPECT_ERRORS(ERR_NOT_SAME_SIZE, ERR_NOT_SAME_TYPE);
    i1 = ip_alloc_image(IM_UBYTE, size);
    i2 = ip_alloc_image(IM_USHORT, size);
    MARK(1);
    im_add(i1, i2, NO_FLAG);
    CHECK_SEEN_ERROR;

    ip_free_image(i2);
    i2 = ip_alloc_image(IM_UBYTE, (ip_coord2d){1000, 1000});
    MARK(2);
    im_add(i1, i2, NO_FLAG);
    CHECK_SEEN_ERROR;

    ip_free_image(i1);
    ip_free_image(i2);

    EXPECT_ERRORS(ERR_BAD_TYPE, 0);
    i1 = ip_alloc_image(IM_PALETTE, size);
    i2 = ip_alloc_image(IM_PALETTE, size);
    MARK(3);
    im_add(i1, i2, NO_FLAG);
    CHECK_SEEN_ERROR;

    ip_free_image(i1);
    ip_free_image(i2);

    EXPECT_ERRORS(ERR_BAD_FLAG, ERR_BAD_FLAG2);
    i1 = ip_alloc_image(IM_BYTE, size);
    i2 = ip_alloc_image(IM_BYTE, size);
    MARK(4);
    im_add(i1, i2, FLG_REAL);
    CHECK_SEEN_ERROR;

    ip_free_image(i1);
    ip_free_image(i2);

    printf("im_add error reporting: %s\n", failed ? "FAIL" : "OK");

    EXPECT_NO_ERRORS;

    printf("CHECKING basic pixelwise add code:\n");

    im_add_check(IM_UBYTE, size, NO_FLAG, "im_add UBYTE");
    im_add_check(IM_UBYTE, size, FLG_CLIP, "im_add clipped UBYTE");
    im_add_check(IM_BYTE, size, NO_FLAG, "im_add BYTE");
    im_add_check(IM_BYTE, size, FLG_CLIP, "im_add clipped BYTE");
    im_add_check(IM_USHORT, size, NO_FLAG, "im_add USHORT" );
    im_add_check(IM_USHORT, size, FLG_CLIP, "im_add clipped USHORT");
    im_add_check(IM_SHORT, size, NO_FLAG, "im_add SHORT" );
    im_add_check(IM_SHORT, size, FLG_CLIP, "im_add clipped SHORT");
    im_add_check(IM_ULONG, size, NO_FLAG, "im_add ULONG" );
    im_add_check(IM_LONG, size, NO_FLAG, "im_add LONG" );
    im_real_add_check(IM_FLOAT, size, NO_FLAG, "im_add FLOAT" );
    im_real_add_check(IM_DOUBLE, size, NO_FLAG, "im_add DOUBLE" );
    im_real_add_check(IM_COMPLEX, size, NO_FLAG, "im_add COMPLEX" );
    im_real_add_check(IM_DCOMPLEX, size, NO_FLAG, "im_add DCOMPLEX" );

    printf("CHECKING fast code (\"poor man's SIMD\"):\n");

    ip_set_global_flags(IP_FAST_CODE);

    im_add_check(IM_UBYTE, size, NO_FLAG, "im_add UBYTE");
    im_add_check(IM_BYTE, size, NO_FLAG, "im_add BYTE");
    im_add_check(IM_USHORT, size, NO_FLAG, "im_add USHORT" );
    im_add_check(IM_SHORT, size, NO_FLAG, "im_add SHORT" );
    im_add_check(IM_ULONG, size, NO_FLAG, "im_add ULONG" );
    im_add_check(IM_LONG, size, NO_FLAG, "im_add LONG" );

    printf("CHECKING hardware (SIMD) acceleration (if available):\n");

    ip_set_global_flags(IP_HWACCEL);

    im_add_check(IM_UBYTE, size, NO_FLAG, "im_add UBYTE");
    im_add_check(IM_UBYTE, size, FLG_CLIP, "im_add clipped UBYTE");
    im_add_check(IM_BYTE, size, NO_FLAG, "im_add BYTE");
    im_add_check(IM_BYTE, size, FLG_CLIP, "im_add clipped BYTE");
    im_add_check(IM_USHORT, size, NO_FLAG, "im_add USHORT" );
    im_add_check(IM_USHORT, size, FLG_CLIP, "im_add clipped USHORT");
    im_add_check(IM_SHORT, size, NO_FLAG, "im_add SHORT" );
    im_add_check(IM_SHORT, size, FLG_CLIP, "im_add clipped SHORT");
    im_add_check(IM_ULONG, size, NO_FLAG, "im_add ULONG" );
    im_add_check(IM_LONG, size, NO_FLAG, "im_add LONG" );
    im_real_add_check(IM_FLOAT, size, NO_FLAG, "im_add FLOAT" );
    im_real_add_check(IM_DOUBLE, size, NO_FLAG, "im_add DOUBLE" );
    im_real_add_check(IM_COMPLEX, size, NO_FLAG, "im_add COMPLEX" );
    im_real_add_check(IM_DCOMPLEX, size, NO_FLAG, "im_add DCOMPLEX" );

    printf("\nCHECKING adding constant to image:\n");

    ip_clear_global_flags(IP_HWACCEL | IP_FAST_CODE);

    im_addc_check(IM_UBYTE, size, NO_FLAG, 100, "im_addc UBYTE");
    im_addc_check(IM_UBYTE, size, FLG_CLIP, 100, "im_addc clipped UBYTE");
    im_addc_check(IM_BYTE, size, NO_FLAG, -10, "im_addc BYTE");
    im_addc_check(IM_BYTE, size, FLG_CLIP, -100, "im_addc clipped BYTE");
    im_addc_check(IM_USHORT, size, NO_FLAG, 32000, "im_addc USHORT");
    im_addc_check(IM_USHORT, size, FLG_CLIP,32001, "im_addc clipped USHORT");
    im_addc_check(IM_SHORT, size, NO_FLAG, 200, "im_addc SHORT");
    im_addc_check(IM_SHORT, size, FLG_CLIP, -10000, "im_addc clipped SHORT");
    im_addc_check(IM_ULONG, size, NO_FLAG, 200, "im_addc ULONG");
    im_addc_check(IM_LONG, size, NO_FLAG, -2172645, "im_addc LONG");
    im_real_addc_check(IM_FLOAT, size, NO_FLAG, -2.63e8, "im_addc FLOAT");
    im_real_addc_check(IM_DOUBLE, size, NO_FLAG, 9.92316576e100, "im_addc DOUBLE");
    im_complex_addc_check(IM_COMPLEX, size, NO_FLAG, 123.847, -9172.00, "im_addc COMPLEX" );
    im_complex_addc_check(IM_DCOMPLEX, size, NO_FLAG, 1e10, 2e10, "im_addc DCOMPLEX" );

    printf("CHECKING fast code adding constant to image:\n");

    ip_set_global_flags(IP_FAST_CODE);

    im_addc_check(IM_UBYTE, size, NO_FLAG, 100, "im_addc UBYTE");
    im_addc_check(IM_BYTE, size, NO_FLAG, -10, "im_addc BYTE");
    im_addc_check(IM_USHORT, size, NO_FLAG, 32000, "im_addc USHORT");
    im_addc_check(IM_SHORT, size, NO_FLAG, 200, "im_addc SHORT");
    im_addc_check(IM_ULONG, size, NO_FLAG, 32000, "im_addc ULONG");
    im_addc_check(IM_LONG, size, NO_FLAG, -2000000, "im_addc LONG");

    printf("CHECKING hardware (SIMD) acceleration (if available):\n");

    ip_set_global_flags(IP_HWACCEL);

    im_addc_check(IM_UBYTE, size, NO_FLAG, 100, "im_addc UBYTE");
    im_addc_check(IM_UBYTE, size, FLG_CLIP, 100, "im_addc clipped UBYTE");
    im_addc_check(IM_BYTE, size, NO_FLAG, -10, "im_addc BYTE");
    im_addc_check(IM_BYTE, size, FLG_CLIP, -100, "im_addc clipped BYTE");
    im_addc_check(IM_USHORT, size, NO_FLAG, 32000, "im_addc USHORT");
    im_addc_check(IM_USHORT, size, FLG_CLIP,32001, "im_addc clipped USHORT");
    im_addc_check(IM_SHORT, size, NO_FLAG, 200, "im_addc SHORT");
    im_addc_check(IM_SHORT, size, FLG_CLIP, -10000, "im_addc clipped SHORT");
    im_addc_check(IM_ULONG, size, NO_FLAG, 200, "im_addc ULONG");
    im_addc_check(IM_LONG, size, NO_FLAG, -2172645, "im_addc LONG");
    im_real_addc_check(IM_FLOAT, size, NO_FLAG, -2.63e8, "im_addc FLOAT");
    im_real_addc_check(IM_DOUBLE, size, NO_FLAG, 9.92316576e100, "im_addc DOUBLE");
    im_complex_addc_check(IM_COMPLEX, size, NO_FLAG, 123.847, -9172.00, "im_addc COMPLEX" );
    im_complex_addc_check(IM_DCOMPLEX, size, NO_FLAG, 1e10, 2e10, "im_addc DCOMPLEX" );

    return 0;
}
