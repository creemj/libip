/* To get srandom() */
#define _XOPEN_SOURCE 600

#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <inttypes.h>
#include <math.h>
#include <unistd.h>

#include <openssl/md5.h>

#include <ip/ip.h>

#include "helpers.h"
#include "timing.h"


/*
 * A mechanism for returning info from command line args.
 */

static struct cmd_line_requests clr;

/*
 * Max relative errors permitted in the compare_images_float() and
 * compare_images_double() routines.  Also general flags for compare images
 * routines.
 */

static struct compare_flag cflag = {
    .float_fuzz = 1e-7,
    .double_fuzz = 1e-17
};

ip_coord2d compare_failure_point;

/*
 * Sets the compare functions (below) behaviour.  Flag bits are:
 * quiet set does not print out any messages whatsoever.
 * noisy prints out every discrepancy (not just the first).
 * permit_rounding_error allow least significant bit to be incorrect;
 *   compare functions return 2 if only rounding errors errors occur.
 * float_fuzz set permitted single-precision float fuzzing.
 * double_fuzz set permitted double-presicion float fuzzing.
 */

void set_compare_behaviour_x(struct compare_flag flag)
{
    if (isnan(flag.float_fuzz))
	flag.float_fuzz = cflag.float_fuzz;
    if (isnan(flag.double_fuzz))
	flag.double_fuzz = cflag.double_fuzz;
    cflag = flag;
}


/*
 * Out of memory bail out
 */

void out_of_memory(void)
{
    fprintf(stderr, "ERROR: Out of memory.  Bailing...\n");
    exit(255);
}


/*
 * Process command line arguments
 */

struct cmd_line_requests *process_cmdline(int argc, char *argv[], int num_ops, const char *opname[])
{
    int chk_flags = 0;
    int dotypes = 0;
    int opt, op_to_test;

    op_to_test = -1;
    clr.im_type = 0;
    clr.random_seed = 1;

    if (isatty(fileno(stdout)))
	chk_flags |= DO_COLOUR;

    while ((opt = getopt(argc, argv, "a:b:rtTmchlnvepxVo:s:S:f:d:C:")) != -1) {
	char *cptr;
	double fuzz;

	switch (opt) {
	case 'r':		/* Use random data in test image */
	    chk_flags |= DO_RANDOM;
	    break;
	case 't':		/* Do timings */
	    chk_flags |= DO_TIMING;
	    break;
	case 'T':		/* Do timings and print with xtra prec. */
	    chk_flags |= DO_TIMING | DO_XPREC;
	    break;
	case 'm':		/* Add cache misses to timing info */
	    do_cache_misses = 1;
	    break;
	case 'c':		/* Print checksums of final result */
	    chk_flags |= DO_CHECKSUM;
	    break;
	case 'S':		/* Set initial random seed */
	    clr.random_seed = (int)strtol(optarg, NULL, 0);
	    srandom(clr.random_seed);
	    break;
	case 'o':		/* Process this operator only */
	    for (int j=0; j<num_ops; j++)
		if (!strcmp(optarg, opname[j])) {
		    op_to_test = j;
		    break;
		}
	    if (op_to_test < 0) {
		fprintf(stderr, "ERROR: Don't recognise operator type '%s'!\n",
			optarg);
		exit(255);
	    }
	    break;
	case 'l':		/* List operators names */
	    printf("Operator names useable with '-o' option:\n");
	    for (int j=0; j<num_ops; j++)
		printf("  %s\n", opname[j]);
	    exit(0);
	    break;
	case 'a':
	    /* Select an image type to test (otherwise do all) */
	    for (int i=1; i<IM_TYPEMAX; i++) {
		if (! strcmp(optarg, ip_type_name(i))) {
		    clr.im_type = i;
		    break;
		}
	    }
	    if (clr.im_type == 0) {
		fprintf(stderr, "ERROR: Don't recognise image type: %s\n", optarg);
		exit(255);
	    }
	    chk_flags |= DO_TYPE;
	    break;
	case 'b':	       /* Provide an extra parameter to the test. */
	    clr.op_parm = strtol(optarg, NULL, 0);
	    chk_flags |= DO_PARM;
	    break;
	case 's':		/* Specify image size as X,Y or XxY. */
	    clr.im_size.y = -1;
	    clr.im_size.x = strtol(optarg, &cptr, 0);
	    if (*cptr == 'x' || *cptr == ',')
		clr.im_size.y = strtol(cptr+1, &cptr, 0);
	    if (clr.im_size.x <= 0 || clr.im_size.y <= 0) {
		fprintf(stderr, "ERROR: Invalid size (%d,%d): specify size as X[,|x]Y.\n",
				clr.im_size.x, clr.im_size.y);
		exit(1);
	    }
	    chk_flags |= DO_SIZE;
	    break;
	case 'v':		/* Do SIMD vector tests */
	    dotypes |= DO_SIMD;
	    break;
	case 'p':		/* Do pixel based processing tests */
	    dotypes |= DO_PIXEL;
	    break;
	case 'e':		/* Do error checking tests */
	    dotypes |= DO_ERROR;
	    break;
	case 'x':		/* Save all images involved in tests */
	    chk_flags |= DO_SAVE;
	    break;
	case 'n':     /* Don't test against the reference: test will pass */
	    chk_flags |= DO_NO_TEST;
	    break;
	case 'f':	     /* Get float max permissable relative error. */
	    fuzz = strtod(optarg, NULL);
	    if (fuzz > 1.0 || fuzz < 1e-7)
		fprintf(stderr,
			"WARNING: ridiculous FLOAT max rel error on command line; using %g instead.\n",
			cflag.float_fuzz);
	    else
		cflag.float_fuzz = fuzz;
	    break;
	case 'd':	    /* Get double max permissable relative error. */
	    fuzz = strtod(optarg, NULL);
	    if (fuzz > 1.0 || fuzz < 1e-17)
		fprintf(stderr,
			"WARNING: ridiculous DOUBLE max rel error on command line; using %g instead.\n",
			cflag.double_fuzz);
	    else
		cflag.double_fuzz = fuzz;
	    break;
	case 'V':		/* Increase verbosity */
	    chk_flags |= DO_VERBOSE;
	    break;
	case 'C':		/* Use colour in output */
	    if (strtol(optarg, NULL, 0))
		chk_flags |= DO_COLOUR;
	    else
		chk_flags &= ~DO_COLOUR;
	    break;
	case 'h':		/* Help! */
	    /* Fall through */
	default:
	    fprintf(stderr,
		    "Usage: %s [-hrctTmpvexnfdV] [-S <random_seed>] [-o <operator_name>] [-a <image_type>]\n",
		    argv[0]);
	    for (int j=0; j<strlen(argv[0])+8; j++)
		fputc(' ', stderr);
	    fprintf(stderr, "[-b <extra_op_parm>] [-C[0|1]]\n"
		    "       %s -l (to get useable operator names)\n", argv[0]);
	    if (opt == 'h') {
		fprintf(stderr, "       -h Print this help message.\n");
		fprintf(stderr, "       -r Use random pixel values in test image.\n");
		fprintf(stderr, "       -S Set random seed when using -r.\n");
		fprintf(stderr, "       -c Print out checksums of result images.\n");
		fprintf(stderr, "       -t Time calculations and print out times.\n");
		fprintf(stderr, "       -T As -t but as machine readable format.\n");
		fprintf(stderr, "       -m Also count cache misses if timing.\n");
		fprintf(stderr, "       -p Check pixel pumping (not SIMD) code.\n");
		fprintf(stderr, "       -v Check SIMD code (if possible).\n");
		fprintf(stderr, "       -e Check error reporting.\n");
		fprintf(stderr, "       -x Save all images (can be lots of them).\n");
		fprintf(stderr, "       -n Don't run reference code (test will pass).\n");
		fprintf(stderr, "       -o Test specific operator (use -l to find them).\n");
		fprintf(stderr, "       -s Specify image size (as XxY or X,Y) to use in tests.\n");
		fprintf(stderr, "       -a Test specific image type (UBYTE, ...).\n");
		fprintf(stderr, "       -b Set a parameter useful for some operators.\n");
		fprintf(stderr, "       -f Set max relative error for FLOAT image results.\n");
		fprintf(stderr, "       -d Set max relative error for DOUBLE image results.\n");
		fprintf(stderr, "       -V increase output verbosity in some tests.\n");
		fprintf(stderr, "       -C Switch on/off coloured output [def=on when tty].\n");
	    }
	    exit(opt == 'h' ? EXIT_SUCCESS : 255);
	    break;
	}
    }

    if (dotypes == 0)
	dotypes = DO_ERROR | DO_PIXEL | DO_SIMD;

    clr.flags = chk_flags | dotypes;
    clr.op_to_test = op_to_test;

    return &clr;
}


/*
 * create_test_image()
 *
 * Allocates an image and fills it with a good spread of values across the
 * range of the basic pixel type of the image.  Either uses im_create() with
 * wedge type test images or im_random() to get uniformly distributed random
 * pixel values.
 *
 * flag bit 0 -- true to use random
 * flag bit 1 -- choose horizontal (0) or vertical (1) wedge if bit 0 is 0.
 * flag bit 2 -- an image with a bit more structure if bit 0 is 0.
 * flag bit 3 -- soft limit the values to another type in bits 0x1f0000
 *
 * Will force to random despite flag bits if im_create() doesn't support
 * requested image type.
 */

void fill_test_image(ip_image *im, int flag)
{
    double offset, scale;
    int type = im->type;
    ip_coord2d size = im->size;

    if (type_real(type) || type_complex(type)) {
        if (type == IM_DOUBLE || type == IM_DCOMPLEX) {
            scale = 2e30;
        }else{
            scale = 2e10;
        }
	if (flag & DO_VALLIMIT) {
	    int etype = (flag & VALLIMIT_TYPE_BITS) >> VALLIMIT_TYPE_SHIFT;
	    double maxv =  1.5*ip_type_max(etype);
	    if (scale > maxv) scale = maxv;
	}
        offset = -(scale / 2.0);
    }else{
        scale = (ip_type_max(type) - ip_type_min(type));
        offset = ip_type_min(type);
    }
    if (((flag & DO_RANDOM) == 0) && type != IM_COMPLEX && type != IM_DCOMPLEX && type != IM_BINARY && type != IM_PALETTE) {
	if (flag & DO_FTIMAGE) {
	    /*
	     * Create an image with a mixture of things including a patch of
	     * noise of 4x4 pixel blobs.
	     */
	    ip_image *tmp_exp = ip_alloc_image(type, size);
	    ip_image *tmp = ip_alloc_image(type, size);
	    double rmaxv;

	    if (type_real(type))
		rmaxv = scale / 1e2;
	    else
		rmaxv = 10 * (scale/255);
	    scale *= 1.2/size.x;
	    im_create(im, (ip_coord2d){0,0}, scale, offset, 1.0, TI_HWEDGE);
	    im_create(tmp_exp, (ip_coord2d){0,0}, scale, offset, 1.0, TI_VWEDGE);
	    im_create(tmp, (ip_coord2d){size.x/2, size.y/2}, scale, offset, 1.0, TI_CONE);
	    im_add(im, tmp_exp, NO_FLAG);
	    im_sub(im, tmp, NO_FLAG);
	    ip_free_image(tmp);
	    ip_free_image(tmp_exp);

	    tmp = ip_alloc_image(type, (ip_coord2d){size.x/8, size.y/8});
	    im_random(tmp, (ip_type_min(type)==0) ? 0 : -rmaxv, rmaxv, FLG_RANDOM_UNIFORM);
	    tmp_exp = im_expand(tmp, 4, FLG_ZOOM);
	    ip_free_image(tmp);
	    tmp = ip_alloc_image(type, size);
	    im_clear(tmp);
	    im_insert(tmp, (ip_coord2d){200,100}, tmp_exp, (ip_coord2d){0,0}, tmp_exp->size);
	    im_add(im, tmp, NO_FLAG);
	    ip_free_image(tmp);
	    ip_free_image(tmp_exp);
	}else{
	    /* Use an image with increasing pixel values across it */
	    scale *= 1.2/size.x;
	    if (type_rgbim(type)) {
		ip_image *r, *g, *b, *a;
		int btype = (type == IM_RGB16) ? IM_USHORT : IM_UBYTE;
		r = ip_alloc_image(btype, size);
		g = ip_alloc_image(btype, size);
		b = ip_alloc_image(btype, size);
		a = (type == IM_RGBA) ? ip_alloc_image(btype, size) : NULL;
		im_create(r,  (ip_coord2d){0,0}, scale, offset, 1.0, TI_HWEDGE);
		im_create(g,  (ip_coord2d){0,0}, scale, offset, 1.0, TI_VWEDGE);
		im_create(b,  (ip_coord2d){size.x/2, size.y/2}, scale, offset, 1.0, TI_CONE);
		if (type == IM_RGBA)
		    im_random(a, offset, scale+offset, FLG_RANDOM_UNIFORM);
		im_form_rgb(im, r, g, b, a);
		ip_free_image(r);
		ip_free_image(g);
		ip_free_image(b);
		ip_free_image(a);
	    }else{
		im_create(im, (ip_coord2d){0,0}, scale, offset, 1.0,
			  (flag & DO_VWEDGE) ? TI_VWEDGE : TI_HWEDGE);
	    }
	}
    }else{
        /* Create an image with random pixel values */
        im_random(im, offset, scale+offset, FLG_RANDOM_UNIFORM);
    }
}


ip_image *create_test_image(int type, ip_coord2d size, int flag)
{
    ip_image *im;

    im = ip_alloc_image(type, size);
    fill_test_image(im, flag);
    return im;
}


/*
 * Calculate an MD5 checksum on the image data.  Useful in the test suite to
 * help detect if result images are identical across platforms.
 *
 * Note that we treat image data as a bit stream and calculate the MD5
 * checksum for little-endian format, thus have to do byte swapping on a
 * big-endian arch.
 */

unsigned char *im_md5(ip_image *im)
{
    MD5_CTX ctx;
    static unsigned char chksum[16];

    MD5_Init(&ctx);
#ifdef HAVE_BIGENDIAN
    /*
     * Big Endian -- have to swap bytes around in data types larger than a
     * byte to achieve little endian format before calculating the MD5
     * checksum.  Note that this code is generic, that is, it will produce
     * the correct result on little-endian as well, but it is not efficient
     * as the little endian specific code below.
     */
    for (int j=0; j<im->size.y; j++) {
	int rowlen = (ip_type_size(im->type) / ip_type_basesize(im->type)) * im->size.x;

	if (ip_type_basesize(im->type) == 1) {
	    /* Byte orientated data --- no byte swapping, easy to process. */
	    MD5_Update(&ctx, IM_ROWADR(im, j, v), ip_type_size(im->type) * im->size.x);
	}else{
	    /* Word or larger oriented data --- do byte swapping */
	    for (int i=0; i<rowlen; i++) {
		unsigned char buf[8];
		switch (ip_type_basesize(im->type)) {
		case 2:
		    buf[0] = IM_PIXEL(im, i, j, us) & 0xff;
		    buf[1] = IM_PIXEL(im, i, j, us) >> 8;
		    break;
		case 4:
		    buf[0] = IM_PIXEL(im, i, j, ul) & 0xff;
		    buf[1] = (IM_PIXEL(im, i, j, ul) >> 8) & 0xff;
		    buf[2] = (IM_PIXEL(im, i, j, ul) >> 16) & 0xff;
		    buf[3] = (IM_PIXEL(im, i, j, ul) >> 24);
		    break;
		case 8:
		    buf[0] = IM_PIXEL(im, i, j, ull) & 0xff;
		    buf[1] = (IM_PIXEL(im, i, j, ull) >> 8) & 0xff;
		    buf[2] = (IM_PIXEL(im, i, j, ull) >> 16) & 0xff;
		    buf[3] = (IM_PIXEL(im, i, j, ull) >> 24) & 0xff;
		    buf[4] = (IM_PIXEL(im, i, j, ull) >> 32) & 0xff;
		    buf[5] = (IM_PIXEL(im, i, j, ull) >> 40) & 0xff;
		    buf[6] = (IM_PIXEL(im, i, j, ull) >> 48) & 0xff;
		    buf[7] = (IM_PIXEL(im, i, j, ull) >> 56);
		    break;
		default:
		    break;
		}
		MD5_Update(&ctx, buf, ip_type_basesize(im->type));
	    }
	}
    }
#else
    /* Little endian processing is straightforward. */
    for (int j=0; j<im->size.y; j++) {
	MD5_Update(&ctx, IM_ROWADR(im, j, v), ip_type_size(im->type) * im->size.x);
    }
#endif
    MD5_Final(chksum, &ctx);

    return chksum;
}


/*
 * Print an MD5 checksum.
 */
void print_md5(unsigned char *chksum)
{
    for (int j=0; j<16; j++) {
	printf("%02x", chksum[j]);
    }
}



int image_compatible_check(ip_image *d, ip_image *s)
{
    if (d->type != s->type)
	return 0;
    if (d->size.x != s->size.x || d->size.y != s->size.y)
	return 0;
    return 1;
}


void set_image_row_padding(ip_image *im, uint8_t val)
{
    for (int j=0; j<im->size.y; j++) {
	uint8_t *p;
	p = IM_ROWADR(im, j, ub);
	for (int i = (im->size.x*ip_image_type_info[im->type].typesize);
	     i < im->row_size;
	     i++)
	    p[i] = val;
    }
}


int check_image_row_padding(ip_image *im, uint8_t val, char * msg)
{
    for (int j=0; j<im->size.y; j++) {
	uint8_t *p;
	p = IM_ROWADR(im, j, ub);
	for (int i = (im->size.x*ip_image_type_info[im->type].typesize)+1;
	     i < im->row_size;
	     i++) {
	    if (p[i] != val) {
		printf("%s.\n", msg);
		return 1;
	    }
	}
    }
    return 0;
}


/*
 * compare_images_XXX(im1, im2, msg, im1_name, im2_name)
 *
 * Functions to compare image im1 pixel data against im2.  The XXX part can
 * be integer, real or complex.  Normally one would call compare_images()
 * which calls the appropriate XXX type routine.
 *
 * Return convention:
 *   0 -- image data are identical
 *   bit 1 set -- image data are not identical.
 *   bit 2 set -- image data are not identical but not considered a failure.
 *   all bits set -- error occurred; e.g. if images are not same type and size.
 *
 * The bit 2 set possibility is not returned by the routines below but is
 * reserved for use elsewhere in the test suite.
 */

int compare_images_integer(ip_image *im1, ip_image *im2, char *msg, char *im1_name, char *im2_name)
{
    long v1, v2;
    int firsttime = 1;
    int rounding_errors_only = 1;
    long maxdiff = 0;
    int numdiffs = 0;

    v1 = v2 = 0;
    for (int j=0; j<im1->size.y; j++) {
	for (int i=0; i<im1->size.x; i++) {
	    int rounding_error = 0;

	    switch (im1->type) {
	    case IM_BINARY:
	    case IM_PALETTE:
	    case IM_UBYTE:
		v1 = IM_PIXEL(im1, i, j, ub);
		v2 = IM_PIXEL(im2, i, j, ub);
		if ((uint8_t)(IM_PIXEL(im1, i, j, ub)+1) == IM_PIXEL(im2, i, j, ub)
		    || (uint8_t)(IM_PIXEL(im2, i, j, ub)+1) == IM_PIXEL(im1, i, j, ub))
		    rounding_error = 1;
		break;
	    case IM_BYTE:
		v1 = IM_PIXEL(im1, i, j, b);
		v2 = IM_PIXEL(im2, i, j, b);
		if ((uint8_t)(IM_PIXEL(im1, i, j, ub)+1) == IM_PIXEL(im2, i, j, ub)
		    || (uint8_t)(IM_PIXEL(im2, i, j, ub)+1) == IM_PIXEL(im1, i, j, ub))
		    rounding_error = 1;
		break;
	    case IM_USHORT:
		v1 = IM_PIXEL(im1, i, j, us);
		v2 = IM_PIXEL(im2, i, j, us);
		if ((uint16_t)(IM_PIXEL(im1, i, j, us)+1) == IM_PIXEL(im2, i, j, us)
		    || (uint16_t)(IM_PIXEL(im2, i, j, us)+1) == IM_PIXEL(im1, i, j, us))
		    rounding_error = 1;
		break;
	    case IM_SHORT:
		v1 = IM_PIXEL(im1, i, j, s);
		v2 = IM_PIXEL(im2, i, j, s);
		if ((uint16_t)(IM_PIXEL(im1, i, j, us)+1) == IM_PIXEL(im2, i, j, us)
		    || (uint16_t)(IM_PIXEL(im2, i, j, us)+1) == IM_PIXEL(im1, i, j, us))
		    rounding_error = 1;
		break;
	    case IM_ULONG:
		v1 = IM_PIXEL(im1, i, j, ul);
		v2 = IM_PIXEL(im2, i, j, ul);
		if ((uint32_t)(IM_PIXEL(im1, i, j, ul)+1) == IM_PIXEL(im2, i, j, ul)
		    || (uint32_t)(IM_PIXEL(im2, i, j, ul)+1) == IM_PIXEL(im1, i, j, ul))
		    rounding_error = 1;
		break;
	    case IM_LONG:
		v1 = IM_PIXEL(im1, i, j, l);
		v2 = IM_PIXEL(im2, i, j, l);
		if ((uint32_t)(IM_PIXEL(im1, i, j, ul)+1) == IM_PIXEL(im2, i, j, ul)
		    || (uint32_t)(IM_PIXEL(im2, i, j, ul)+1) == IM_PIXEL(im1, i, j, ul))
		    rounding_error = 1;
		break;
	    default:
		break;
	    }

	    if (v1 != v2) {
		long diff = v1 > v2 ? (v1-v2) : (v2-v1);

		if (!rounding_error)
		    rounding_errors_only = 0;
		if (cflag.noisy || (firsttime && !cflag.quiet)) {
		    printf("%s: Images %s [%ld] and %s [%ld] differ at (%d,%d)\n", msg, im1_name, v1, im2_name, v2, i, j);
		    firsttime = 0;
		}
		numdiffs++;
		if (diff > maxdiff) {
		    maxdiff = diff;
		    if (!rounding_error || !cflag.permit_rounding_error)
			compare_failure_point = (ip_coord2d){i, j};
		}
	    }
	}
    }
    if (numdiffs && !cflag.quiet) {
	printf("...num differing pixels = %d; maximum difference = %ld.\n", numdiffs, maxdiff);
	if (cflag.permit_rounding_error & rounding_errors_only)
	    printf("...look like rounding errors only.\n");
    }

    int retval = 0;
    if (numdiffs > 0) {
	if (cflag.permit_rounding_error & rounding_errors_only)
	    retval = 2;
	else
	    retval = 1;
    }
    return retval;
}



int compare_images_float(ip_image *im1, ip_image *im2, char *msg, char *im1_name, char *im2_name)
{
    float v1, v2;
    int firsttime = 1;
    float maxdiff = 0;
    float maxrel = 0;
    int numdiffs = 0;
    int rnderror = 1;

    v1 = v2 = 0;
    for (int j=0; j<im1->size.y; j++) {
	for (int i=0; i<im1->size.x; i++) {
	    int cmp = 0;
	    float diff;

	    v1 = IM_PIXEL(im1, i, j, f);
	    v2 = IM_PIXEL(im2, i, j, f);

	    diff = fabsf(v1 - v2);
	    if (isnan(v1) || isnan(v2))
		cmp = isnan(v1) & isnan(v2);
	    else{
		if (v1 != 0.0)
		    cmp = ((diff/v1) < cflag.float_fuzz);
		else
		    cmp = (v1 == v2);
	    }

	    if (! cmp) {
		float reldiff = fabsf(diff / v1);
		if (diff > maxdiff) {
		    maxdiff = diff;
		    compare_failure_point = (ip_coord2d){i, j};
		}
		if (isfinite(reldiff) && reldiff > maxrel)
		    maxrel = reldiff;
		if (nextafterf(v1,v2) != v2)
		    rnderror = 0;
		if (cflag.noisy || (firsttime && !cflag.quiet)) {
		    printf("%s: Images %s [%.9g] and %s [%.9g] differ at (%d,%d)\n",
			   msg, im1_name, v1, im2_name, v2, i, j);
		    firsttime = 0;
		}
		numdiffs++;
	    }
	}
    }
    if (numdiffs && !cflag.quiet) {
	printf("...num differing pixels = %d; max difference = %g;  max rel diff = %g.\n",
	       numdiffs, maxdiff, maxrel);
	if (rnderror)
	    printf("...all differences look like rounding errors.\n");
    }

    return (numdiffs > 0) ? 1 : 0;
}


int compare_images_double(ip_image *im1, ip_image *im2, char *msg, char *im1_name, char *im2_name)
{
    double v1, v2;
    int firsttime = 1;
    double maxdiff = 0;
    double maxrel = 0;
    int numdiffs = 0;
    int rnderror = 1;

    v1 = v2 = 0;
    for (int j=0; j<im1->size.y; j++) {
	for (int i=0; i<im1->size.x; i++) {
	    double diff;
	    int cmp = 0;

	    v1 = IM_PIXEL(im1, i, j, d);
	    v2 = IM_PIXEL(im2, i, j, d);

	    diff = fabs(v1 - v2);
	    if (isnan(v1) || isnan(v2))
		cmp = isnan(v1) & isnan(v2);
	    else{
		if (v1 != 0.0)
		    cmp = ((diff/v1) < cflag.double_fuzz);
		else
		    cmp = (v1 == v2);
	    }

	    if (! cmp) {
		double reldiff = fabs(diff / v1);
		if (diff > maxdiff) {
		    maxdiff = diff;
		    compare_failure_point = (ip_coord2d){i, j};
		}
		if (isfinite(reldiff) && reldiff > maxrel)
		    maxrel = reldiff;
		if (nextafter(v1,v2) != v2)
		    rnderror = 0;

		if (cflag.noisy || (firsttime && cflag.quiet)) {
		    printf("%s: Images %s [%g] and %s [%g] differ at (%d,%d)\n",
			   msg, im1_name, v1, im2_name, v2, i, j);
		    firsttime = 0;
		}
		numdiffs++;
	    }
	}
    }
    if (numdiffs && !cflag.quiet) {
	printf("...num differing pixels = %d; max difference = %g; max rel diff = %g.\n",
	       numdiffs, maxdiff, maxrel);
	if (rnderror)
	    printf("...all differences look like rounding errors.\n");
    }

    return (numdiffs > 0) ? 1 : 0;
}


int compare_images_complex(ip_image *im1, ip_image *im2, char *msg, char *im1_name, char *im2_name)
{
    ip_dcomplex v1, v2;
    int firsttime = 1;
    double maxdiff = 0;
    int numdiffs = 0;

    v1.r = v1.i = v2.r = v2.i = 0;
    for (int j=0; j<im1->size.y; j++) {
	for (int i=0; i<im1->size.x; i++) {
	    int cmp = 0;

	    switch (im1->type) {
	    case IM_COMPLEX:
		cmp = IM_PIXEL(im1, i, j, c).r == IM_PIXEL(im2, i, j, c).r;
		cmp &= IM_PIXEL(im1, i, j, c).i == IM_PIXEL(im2, i, j, c).i;
		v1.r = (double)IM_PIXEL(im1, i, j, c).r;
		v1.i = (double)IM_PIXEL(im1, i, j, c).i;
		v2.r = (double)IM_PIXEL(im2, i, j, c).r;
		v2.i = (double)IM_PIXEL(im2, i, j, c).i;
		break;
	    case IM_DCOMPLEX:
		cmp = IM_PIXEL(im1, i, j, dc).r == IM_PIXEL(im2, i, j, dc).r;
		cmp &= IM_PIXEL(im1, i, j, dc).i == IM_PIXEL(im2, i, j, dc).i;
		v1 = IM_PIXEL(im1, i, j, dc);
		v2 = IM_PIXEL(im2, i, j, dc);
		break;
	    default:
		break;
	    }

	    if (! cmp) {
		double diff = (v1.r - v2.r)*(v1.r - v2.r) + (v1.i - v2.i)*(v1.i - v2.i);
		diff /= sqrt(v2.r*v2.r + v2.i*v2.i);
		if (cflag.noisy || (firsttime && !cflag.quiet)) {
		    printf("%s: Images %s [%g+i%g] and %s [%g+i%g] differ at (%d,%d)\n", msg, im1_name, v1.r, v1.i, im2_name, v2.r, v2.i, i, j);
		    firsttime = 0;
		}
		numdiffs++;
		if (diff > maxdiff) {
		    maxdiff = diff;
		    compare_failure_point = (ip_coord2d){i, j};
		}
	    }
	}
    }
    if (numdiffs && !cflag.quiet) {
	printf("...num differing pixels = %d; max rel. sq. magn. diff. = %g.\n", numdiffs, maxdiff);
    }

    return (numdiffs > 0) ? 1 : 0;
}


static ip_lrgb rgb_to_lrgb(int r, int g, int b)
{
    ip_lrgb y;
    y.r = r;
    y.g = g;
    y.b = b;
    return y;
}

static void print_colour(ip_lrgb c, int a)
{
    if (a < 0)
	printf("[%d,%d,%d]", c.r, c.g, c.b);
    else
	printf("[%d,%d,%d,%d]", c.r, c.g, c.b, a);
}


int compare_images_rgb(ip_image *im1, ip_image *im2, char *msg, char *im1_name, char *im2_name)
{
    ip_lrgb v1, v2;
    int a1 = -1;
    int a2 = -1;
    int numdiffs = 0;
    int firsttime = 1;

    for (int j=0; j<im1->size.y; j++) {
	for (int i=0; i<im1->size.x; i++) {
	    int cmp = 0;
	    switch (im1->type) {
	    case IM_RGB: {
		ip_rgb *p1, *p2;
		p1 = &IM_PIXEL(im1, i, j, r);
		p2 = &IM_PIXEL(im2, i, j, r);
		cmp = p1->r == p2->r;
		cmp &= p1->g == p2->g;
		cmp &= p1->b == p2->b;
		v1 = rgb_to_lrgb(p1->r, p1->g, p1->b);
		v2 = rgb_to_lrgb(p2->r, p2->g, p2->b);
	    }
		break;
	    case IM_RGBA: {
		ip_rgba *p1, *p2;
		p1 = &IM_PIXEL(im1, i, j, ra);
		p2 = &IM_PIXEL(im2, i, j, ra);
		cmp = p1->r == p2->r;
		cmp &= p1->g == p2->g;
		cmp &= p1->b == p2->b;
		cmp &= p1->a == p2->a;
		v1 = rgb_to_lrgb(p1->r, p1->g, p1->b);
		v2 = rgb_to_lrgb(p2->r, p2->g, p2->b);
		a1 = p1->a;
		a2 = p2->a;
	    }
		break;
	    case IM_RGB16: {
		ip_lrgb *p1, *p2;
		p1 = &IM_PIXEL(im1, i, j, r16);
		p2 = &IM_PIXEL(im2, i, j, r16);
		cmp = p1->r == p2->r;
		cmp &= p1->g == p2->g;
		cmp &= p1->b == p2->b;
		v1 = *p1;
		v2 = *p2;
	    }
		break;
	    default:
		break;
	    }

	    if (! cmp) {
		if (firsttime) {
		    printf("%s: Images %s ", msg, im1_name);
		    print_colour(v1, a1);
		    printf(" and %s ", im2_name);
		    print_colour(v2, a2);
		    printf(" differ at (%d,%d)\n", i, j);
		    firsttime = 0;
		}
		numdiffs++;
	    }
	}
    }
    if (numdiffs) {
	printf("...num differing pixels = %d.\n", numdiffs);
    }

    return (numdiffs  > 0) ? 1 : 0;
}

int compare_palette(ip_image *im1, ip_image *im2, char *msg, char *im1_name, char *im2_name)
{
    int numdiffs = 0;
    int firsttime = 1;

    for (int j=0; j<256; j++) {
	int cmp;
	cmp = im1->palette[j].r == im2->palette[j].r;
	cmp &= im1->palette[j].g == im2->palette[j].g;
	cmp &= im1->palette[j].b == im2->palette[j].b;
	if (!cmp) {
	    if (firsttime) {
		printf("%s: Images %s ", msg, im1_name);
		printf("and %s ", im2_name);
		printf("palette entry %d differ\n", j);
		firsttime = 0;
	    }
	    numdiffs++;
	}
    }
    if (numdiffs) {
	printf("...num differing palette entries = %d.\n", numdiffs);
    }

    return (numdiffs  > 0) ? 1 : 0;
}


int compare_images(ip_image *im1, ip_image *im2, char *msg, char *im1_name, char *im2_name)
{
    if (!image_valid(im1)) {
	printf("%s: Image object %s corrupted!!!\n", msg, im1_name);
	return -1;
    }

    if (!image_valid(im2)) {
	printf("%s: Image object %s corrupted!!!\n", msg, im2_name);
	return -1;
    }

    if ((im1->size.x != im2->size.x) || (im1->size.y != im2->size.y)) {
	printf("%s: Images %s and %s not compatible.\n", msg, im1_name, im2_name);
	return -1;
    }

    if (type_integer(im1->type) | (im1->type == IM_BINARY))
	return compare_images_integer(im1, im2, msg, im1_name, im2_name);

    if (type_real(im1->type)) {
	if (im1->type == IM_FLOAT)
	    return compare_images_float(im1, im2, msg, im1_name, im2_name);
	else
	    return compare_images_double(im1, im2, msg, im1_name, im2_name);
    }

    if (type_complex(im1->type))
	return compare_images_complex(im1, im2, msg, im1_name, im2_name);

    if (type_rgbim(im1->type))
	return compare_images_rgb(im1, im2, msg, im1_name, im2_name);

    if (im1->palette) {
	int retcode;
	retcode = compare_images_integer(im1, im2, msg, im1_name, im2_name);
	return retcode || compare_palette(im1, im2, msg, im1_name, im2_name);
    }

    return -1;
}


static int compare_images_transposed_integer(ip_image *im1, ip_image *im2, char *msg,
					     char *im1_name, char *im2_name)
{
    long v1, v2;
    int firsttime = 1;
    long maxdiff = 0;
    int numdiffs = 0;

    v1 = v2 = 0;
    for (int j=0; j<im1->size.y; j++) {
	for (int i=0; i<im1->size.x; i++) {
	    int cmp = 0;

	    switch (im1->type) {
	    case IM_BINARY:
	    case IM_PALETTE:
	    case IM_UBYTE:
	    case IM_BYTE:
		cmp = IM_PIXEL(im1, i, j, b) == IM_PIXEL(im2, j, i, b);
		v1 = IM_PIXEL(im1, i, j, b);
		v2 = IM_PIXEL(im2, j, i, b);
		break;
	    case IM_USHORT:
	    case IM_SHORT:
		cmp = IM_PIXEL(im1, i, j, s) == IM_PIXEL(im2, j, i, s);
		v1 = IM_PIXEL(im1, i, j, s);
		v2 = IM_PIXEL(im2, j, i, s);
		break;
	    case IM_ULONG:
	    case IM_LONG:
		cmp = IM_PIXEL(im1, i, j, l) == IM_PIXEL(im2, j, i, l);
		v1 = IM_PIXEL(im1, i, j, l);
		v2 = IM_PIXEL(im2, j, i, l);
		break;
	    default:
		break;
	    }

	    if (! cmp) {
		long diff = v1 > v2 ? (v1-v2) : (v2-v1);

		if (firsttime) {
		    printf("%s: Images %s [%ld] and %s [%ld] differ at (%d,%d)\n", msg, im1_name, v1, im2_name, v2, i, j);
		    firsttime = 0;
		}
		numdiffs++;
		if (diff > maxdiff) {
		    maxdiff = diff;
		    compare_failure_point = (ip_coord2d){i, j};
		}
	    }
	}
    }
    if (numdiffs) {
	printf("...num differing pixels = %d; maximum difference = %ld.\n", numdiffs, maxdiff);
    }

    return (numdiffs > 0) ? 1 : 0;
}



static int compare_images_transposed_real(ip_image *im1, ip_image *im2, char *msg,
					  char *im1_name, char *im2_name)
{
    double v1, v2;
    int firsttime = 1;
    long maxdiff = 0;
    int numdiffs = 0;

    v1 = v2 = 0;
    for (int j=0; j<im1->size.y; j++) {
	for (int i=0; i<im1->size.x; i++) {
	    int cmp = 0;

	    switch (im1->type) {
	    case IM_FLOAT:
		cmp = IM_PIXEL(im1, i, j, f) == IM_PIXEL(im2, j, i, f);
		v1 = IM_PIXEL(im1, i, j, f);
		v2 = IM_PIXEL(im2, j, i, f);
		break;
	    case IM_DOUBLE:
		cmp = IM_PIXEL(im1, i, j, d) == IM_PIXEL(im2, j, i, d);
		v1 = IM_PIXEL(im1, i, j, d);
		v2 = IM_PIXEL(im2, j, i, d);
		break;
	    default:
		break;
	    }

	    if (! cmp) {
		double diff = (v1 > v2) ? (v1-v2) : (v2-v1);

		if (firsttime) {
		    printf("%s: Images %s [%g] and %s [%g] differ at (%d,%d)\n", msg, im1_name, v1, im2_name, v2, i, j);
		    firsttime = 0;
		}
		numdiffs++;
		if (diff > maxdiff) {
		    maxdiff = diff;
		    compare_failure_point = (ip_coord2d){i, j};
		}
	    }
	}
    }
    if (numdiffs) {
	printf("...num differing pixels = %d; maximum difference = %ld.\n", numdiffs, maxdiff);
    }

    return (numdiffs > 0) ? 1 : 0;
}



static int compare_images_transposed_complex(ip_image *im1, ip_image *im2, char *msg,
					     char *im1_name, char *im2_name)
{
    ip_dcomplex v1, v2;
    int firsttime = 1;
    long maxdiff = 0;
    int numdiffs = 0;

    v1.r = v1.i = v2.r = v2.i = 0;
    for (int j=0; j<im1->size.y; j++) {
	for (int i=0; i<im1->size.x; i++) {
	    int cmp = 0;

	    switch (im1->type) {
	    case IM_COMPLEX:
		cmp = (IM_PIXEL(im1, i, j, c).r == IM_PIXEL(im2, j, i, c).r) &&
			    (IM_PIXEL(im1, i, j, c).i == IM_PIXEL(im2, j, i, c).i);
		v1.r = IM_PIXEL(im1, i, j, c).r;
		v1.i = IM_PIXEL(im1, i, j, c).i;
		v2.r = IM_PIXEL(im2, j, i, c).r;
		v2.i = IM_PIXEL(im2, j, i, c).i;
		break;
	    case IM_DCOMPLEX:
		cmp = (IM_PIXEL(im1, i, j, dc).r == IM_PIXEL(im2, j, i, dc).r) &&
			    (IM_PIXEL(im1, i, j, dc).i == IM_PIXEL(im2, j, i, dc).i);
		v1 = IM_PIXEL(im1, i, j, dc);
		v2 = IM_PIXEL(im2, j, i, dc);
		break;
	    default:
		break;
	    }

	    if (! cmp) {
		/* TBD */
		double diff = hypot(v1.r - v2.r, v1.i - v2.i);

		if (firsttime) {
		    printf("%s: Images %s [%g+i%g] and %s [%g+i%g] differ at (%d,%d)\n",
			   msg, im1_name, v1.r, v1.i, im2_name, v2.r, v2.i, i, j);
		    firsttime = 0;
		}
		numdiffs++;
		if (diff > maxdiff) {
		    maxdiff = diff;
		    compare_failure_point = (ip_coord2d){i, j};
		}
	    }
	}
    }
    if (numdiffs) {
	printf("...num differing pixels = %d; maximum difference = %ld.\n", numdiffs, maxdiff);
    }

    return (numdiffs > 0) ? 1 : 0;
}

int compare_images_transposed_rgb(ip_image *im1, ip_image *im2, char *msg,
				  char *im1_name, char *im2_name)
{
    ip_lrgb v1, v2;
    int a1 = -1;
    int a2 = -1;
    int numdiffs = 0;
    int firsttime = 1;

    for (int j=0; j<im1->size.y; j++) {
	for (int i=0; i<im1->size.x; i++) {
	    int cmp = 0;
	    switch (im1->type) {
	    case IM_RGB: {
		ip_rgb *p1, *p2;
		p1 = &IM_PIXEL(im1, i, j, r);
		p2 = &IM_PIXEL(im2, j, i, r);
		cmp = p1->r == p2->r;
		cmp &= p1->g == p2->g;
		cmp &= p1->b == p2->b;
		v1 = rgb_to_lrgb(p1->r, p1->g, p1->b);
		v2 = rgb_to_lrgb(p2->r, p2->g, p2->b);
	    }
		break;
	    case IM_RGBA: {
		ip_rgba *p1, *p2;
		p1 = &IM_PIXEL(im1, i, j, ra);
		p2 = &IM_PIXEL(im2, j, i, ra);
		cmp = p1->r == p2->r;
		cmp &= p1->g == p2->g;
		cmp &= p1->b == p2->b;
		cmp &= p1->a == p2->a;
		v1 = rgb_to_lrgb(p1->r, p1->g, p1->b);
		v2 = rgb_to_lrgb(p2->r, p2->g, p2->b);
		a1 = p1->a;
		a2 = p2->a;
	    }
		break;
	    case IM_RGB16: {
		ip_lrgb *p1, *p2;
		p1 = &IM_PIXEL(im1, i, j, r16);
		p2 = &IM_PIXEL(im2, j, i, r16);
		cmp = p1->r == p2->r;
		cmp &= p1->g == p2->g;
		cmp &= p1->b == p2->b;
		v1 = *p1;
		v2 = *p2;
	    }
		break;
	    default:
		break;
	    }

	    if (! cmp) {
		if (firsttime) {
		    printf("%s: Images %s ", msg, im1_name);
		    print_colour(v1, a1);
		    printf(" and %s ", im2_name);
		    print_colour(v2, a2);
		    printf(" differ at (%d,%d)\n", i, j);
		    firsttime = 0;
		}
		numdiffs++;
	    }
	}
    }
    if (numdiffs) {
	printf("...num differing pixels = %d.\n", numdiffs);
    }

    return (numdiffs  > 0) ? 1 : 0;
}


int compare_images_transposed(ip_image *im1, ip_image *im2, char *msg, char *im1_name, char *im2_name)
{
    if (!image_valid(im1)) {
	printf("%s: Image object %s corrupted!!!\n", msg, im1_name);
	return -1;
    }

    if (!image_valid(im2)) {
	printf("%s: Image object %s corrupted!!!\n", msg, im2_name);
	return -1;
    }

    if ((im1->size.x != im2->size.y) || (im1->size.y != im2->size.x)) {
	printf("%s: Images %s and %s not compatible.\n", msg, im1_name, im2_name);
	return -1;
    }

    if (type_integer(im1->type) | (im1->type == IM_BINARY))
	return compare_images_transposed_integer(im1, im2, msg, im1_name, im2_name);
    else if (im1->type == IM_FLOAT || im1->type == IM_DOUBLE)
	return compare_images_transposed_real(im1, im2, msg, im1_name, im2_name);
    else if (type_complex(im1->type))
	return compare_images_transposed_complex(im1, im2, msg, im1_name, im2_name);
    else if (type_rgbim(im1->type))
	return compare_images_transposed_rgb(im1, im2, msg, im1_name, im2_name);
    else
	printf("Haven't implemented compare of that image type.\n");

    return -1;
}


/*
 * Simple mechanism to register footnotes on tests.
 *
 * In a test use the REGISTER_NOTE() macro to register a footnote to the
 * test.  The footnotes are printed at the end of all test reports. This
 * is useful if wish to report some behaviour which does not constitute
 * failure but would be useful to inform the user.
 */

struct notes_node {
    struct notes_node *next;
    char *note;
};

static int old_footnote_num = 0;
static int footnote_num = 0;
static struct notes_node *notes_list = NULL;


int push_footnote(char *note)
{
    struct notes_node *nnode;
    struct notes_node *prev;

    if ((nnode = malloc(sizeof(struct notes_node))) == NULL)
	out_of_memory();

    nnode->next = NULL;
    if ((nnode->note = strdup(note)) == NULL)
	out_of_memory();

    if (notes_list) {
        for (prev = notes_list; prev->next; prev = prev->next)
            /* do nothing */ ;
        prev->next = nnode;
        footnote_num++;
    } else {
        notes_list = nnode;
        footnote_num = 1;
    }

    return footnote_num;
}


int push_footnote_v(char *fmt, ...)
{
    char *str;
    int size;
    va_list args;
    int num;

    va_start(args, fmt);
    size = vsnprintf(NULL, 0, fmt, args);
    va_end(args);

    if (size < 0)
	return -1;

    size++;
    if ((str = malloc(size)) == NULL)
	out_of_memory();

    va_start(args, fmt);
    size = vsnprintf(str, size, fmt, args);
    va_end(args);

    if (size < 0)
	str[0] = 0;

    num = push_footnote(str);

    free(str);
    return num;
}


/*
 * Called from print_final_stats()
 * Will print out all pushed footnotes.
 */

static void dump_footnotes(void)
{
    struct notes_node *note;

    if (notes_list) {
        int num;

        printf("\nNotes:\n");
        for (note = notes_list, num=1; note; note = note->next, num++) {
            printf("  %d: %s\n", num, note->note);
        }
    }
}



/*
 * A simple array for storing timing information so that it can be retrieved
 * for speed up comparisons.
 */

struct time_entry {
    uint64_t key;
    uint64_t time;
};

struct time_array {
    int N;
    int used;
    struct time_entry *array;
};

static struct time_array time_array_head;


/*
 * Allocates the time info array.  Currently uses a statically allocated
 * head to the array so cannot allocate multiple arrays...
 */

static struct time_array *alloc_time_array(void)
{
    struct time_array *head = &time_array_head;

    if (head->N > 0 && head->array)
	return head;

    /* Allocate 100 extries for starters */
    head->N = 100;
    head->used = 0;
    head->array = malloc(100*sizeof(struct time_entry));
    if (head->array)
	return head;
    else{
	head->N = 0;
	return NULL;
    }
}


/*
 * Insert a time measurement into the array using a key as the index to the
 * time.  No check made as to whether the key is in the array already or
 * not.  If out of memory error occurs does not expand array or insert time
 * as loss of information is preferred to exiting.
 */

static void time_array_insert(struct time_array *head, uint64_t key, uint64_t time)
{
    if (! head)
	return;

    /* Make sure we have space to insert */
    if (head->used >= head->N) {
	struct time_entry *node = realloc(head->array, (head->N+100)*sizeof(struct time_entry));
	/* Just bail without insertion if out of memory */
	if (! node) return;
	head->array = node;
	head->N += 100;
    }
    head->array[head->used].key = key;
    head->array[head->used].time = time;
    head->used++;
}


/*
 * Look for key in array and if found return previous time info associated
 * with key.  Return 0 if no match found.
 */

static uint64_t time_array_get(struct time_array *head, uint64_t key)
{
    if (! head)
	return 0;

    for (int k=0; k<head->used; k++)
	if (head->array[k].key == key)
	    return head->array[k].time;
    return 0;
}


/*
 * Look for key in time info array and if found calculate the speed up in
 * execution time of the passed time relative to the stored time.  Normally
 * the first store is during generic pixel pumping code timing tests, and
 * subsequent timing tests with the same key are with some hardware (SIMD)
 * acceleration, thus we get the speed up of SIMD over generic code for the
 * subsequent timing tests.
 */

static double get_speed_up(uint64_t key, uint64_t time)
{
    struct time_array *ta;
    int old_time;
    double speed_up = 0.0;

    ta = alloc_time_array();
    old_time = time_array_get(ta, key);
    if (old_time == 0) {
	/* Not found, so insert this */
	time_array_insert(ta, key, time);
    }else{
	/* Found old entry; calculate speed up of this one */
	speed_up = (double)old_time / (double)time;
    }
    return speed_up;
}


static void print_num_scaled(uint64_t num, const char *unit, const char *kunit, const char *Munit)
{
    if (num >= 10000000UL)
	printf("%4" PRIu64 " %s", (num+500000UL)/1000000UL, Munit);
    else if (num >= 10000UL)
	printf("%4" PRIu64 " %s", (num+500)/1000, kunit);
    else
	printf("%4" PRIu64 " %s", num, unit);
}


void print_time_status(uint64_t key, uint64_t cycles, uint64_t time, double ipc, int failed)
{
    if (time == -1LL)
	printf("                                   ");
    else{
	double speed_up = get_speed_up(key, time);

	if (clr.flags & DO_XPREC) {
	    /* Machine readable output */
	    printf(" %" PRIu64, time);
	    printf(" %llu", 1000ULL*cycles);
	    printf(" %llu", 1000ULL*instructions);
	    if (do_cache_misses)
		printf(" %" PRIu64, cache_misses);
	    putchar(' ');
	}else{
	    /* Usual human readable output */
	    fputs("[", stdout);
	    print_num_scaled(cycles, "kc", "Mc", "Gc");
	    fputs("; ", stdout);
	    print_num_scaled(time, "us", "ms", "s ");
	    printf("; %4.2f ipc", ipc);
	    if (do_cache_misses)
	    printf("; %6" PRIu64 " cm", cache_misses);
	    if (speed_up == 0.0)
		printf("      ");
	    else{
		printf(" %4.1fx", speed_up);
	    }
	    fputs("] ", stdout);
	}
    }

    if (clr.flags & DO_COLOUR) {
	if (failed & 1)
	    printf(COLOUR_FAILED "[FAIL]" COLOUR_NORMAL);
	else if (failed == 0)
	    printf(COLOUR_OK "[OK]" COLOUR_NORMAL);
	else
	    printf(COLOUR_WARN "[WARN]" COLOUR_NORMAL);
    }else{
	if (failed & 1)
	    printf("[FAIL]");
	else if (failed == 0)
	    printf("[OK]");
	else
	    printf("[WARN]");
    }

    if (footnote_num > old_footnote_num) {
	int diff = footnote_num - old_footnote_num;
	if (diff == 1) {
	    printf(" [Note %d]", footnote_num);
	}else{
	    printf(" [Notes %d-%d]", old_footnote_num+1, footnote_num);
	}
    }
    putchar('\n');

    old_footnote_num = footnote_num;
}



void print_final_stats(int num_tests, int num_failures, int num_warnings)
{
    dump_footnotes();

    printf("\n%d tests performed;", num_tests);
    if (num_failures == 0)
        printf(" all passed");
    else
        printf(" %d FAILURE%s",num_failures, num_failures == 1 ? "" : "S");
    if (num_warnings) {
	printf(" with %d WARNING%s", num_warnings, num_warnings == 1 ? "" : "S");
    }
    printf(".\n");
}
