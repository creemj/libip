/* To get srandom() */
#define _XOPEN_SOURCE 600

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <getopt.h>

#include <ip/ip.h>

#include "test-macros.h"
#include "timing.h"
#include "helpers.h"
#include "errorhdl.h"


char *program_name = "arith-test";

/* Keep counts of failed and tested routines as globals */
int num_tests;
int num_failures;


/*
 * Arithmetic operators between two images
 */

/* im_add like operators on int images */
#define PIXEL_OPERATION_ADD(dval, sval, d)  dval += sval
#define PIXEL_OPERATION_ADD_CLIPPED(dval, sval, d)	\
    dval += sval;					\
    CLIP_TO_PIXEL_TYPE_LIMITS(dval, d);

im_ii_int_DEFN(ADD)
im_ii_int_DEFN(ADD_CLIPPED)

/* im_add like operators on real images */
im_ii_float_DEFN(ADD)
im_ii_real_DEFN(ADD)

/* im_add like operators on complex images */
#define PIXEL_OPERATION_ADD_COMPLEX(dval, sval, d) \
    dval.r += sval.r;				   \
    dval.i += sval.i;

im_ii_complex_DEFN(ADD_COMPLEX)
im_ii_dcomplex_DEFN(ADD_COMPLEX)


/* im_sub like operators on int images */
#define PIXEL_OPERATION_SUB(dval, sval, d)  dval -= sval
#define PIXEL_OPERATION_SUB_CLIPPED(dval, sval, d)	\
    dval -= sval;					\
    CLIP_TO_PIXEL_TYPE_LIMITS(dval, d);

im_ii_int_DEFN(SUB)
im_ii_int_DEFN(SUB_CLIPPED)

/* im_sub like operators on real images */
im_ii_float_DEFN(SUB)
im_ii_real_DEFN(SUB)

/* im_sub like operators on complex images */
#define PIXEL_OPERATION_SUB_COMPLEX(dval, sval, d) \
    dval.r -= sval.r;				   \
    dval.i -= sval.i;

im_ii_complex_DEFN(SUB_COMPLEX)
im_ii_dcomplex_DEFN(SUB_COMPLEX)


/* im_mul like operators on int images */
#define PIXEL_OPERATION_MUL(dval, sval, d)  dval *= sval
#define PIXEL_OPERATION_MUL_CLIPPED(dval, sval, d)      \
    dval *= sval;                                       \
    CLIP_TO_PIXEL_TYPE_LIMITS(dval, d);

im_ii_int_DEFN(MUL)
im_ii_int_DEFN(MUL_CLIPPED)

/* im_mul like operators on real images */
im_ii_float_DEFN(MUL)
im_ii_real_DEFN(MUL)

/* im_mul like operators on complex images */
#define PIXEL_OPERATION_MUL_COMPLEX(dval, sval, d) {    \
        __typeof(dval.r) t;                             \
        t = dval.r*sval.r - dval.i*sval.i;              \
        dval.i = dval.i*sval.r + dval.r*sval.i;         \
        dval.r = t;                                     \
    }

im_ii_complex_DEFN(MUL_COMPLEX)
im_ii_dcomplex_DEFN(MUL_COMPLEX)


/* im_div like operators on int and real images */
#define PIXEL_OPERATION_DIV(dval, sval, d)  dval /= sval

im_ii_int_DEFN(DIV)
im_ii_float_DEFN(DIV)
im_ii_real_DEFN(DIV)

/* im_div like operators on complex images */
#define PIXEL_OPERATION_DIV_COMPLEX(dval, sval, d) {    \
        __typeof(dval.r) dc,tmp,denom,u,v;              \
        u = sval.r;                                     \
        v = sval.i;                                     \
        if (fabs(u) > fabs(v)) {                        \
            dc = v/u;                                   \
            denom = u + v*dc;                           \
            tmp = (dval.r + dval.i * dc) / denom;       \
            dval.i = (dval.i - dval.r * dc) / denom;    \
            dval.r = tmp;                               \
        }else{                                          \
            dc = u/v;                                   \
            denom = u*dc + v;                           \
            tmp = (dval.r*dc + dval.i) / denom;         \
            dval.i = (dval.i*dc - dval.r) / denom;      \
            dval.r = tmp;                               \
        }                                               \
    }

im_ii_complex_DEFN(DIV_COMPLEX)
im_ii_dcomplex_DEFN(DIV_COMPLEX)


/*
 * Logical operators between two images
 */

#define PIXEL_OPERATION_OR(dval, sval, d)  dval |= sval
#define PIXEL_OPERATION_XOR(dval, sval, d)  dval ^= sval
#define PIXEL_OPERATION_AND(dval, sval, d)  dval &= sval

im_ii_int_DEFN(OR);
im_ii_int_DEFN(XOR);
im_ii_int_DEFN(AND);

/*
 * Min/Max operators between two images
 */

#define PIXEL_OPERATION_MAX(dval, sval, d) dval = (dval >= sval) ? (dval) : (sval)
#define PIXEL_OPERATION_MIN(dval, sval, d) dval = (dval <= sval) ? (dval) : (sval)

im_ii_int_DEFN(MAX)
im_ii_float_DEFN(MAX)
im_ii_real_DEFN(MAX)
im_ii_int_DEFN(MIN)
im_ii_float_DEFN(MIN)
im_ii_real_DEFN(MIN)


struct operator_list {
    int id;
    int flags;
    char *name;
    char *op_name;
    int (*ip_im_iif_op)(ip_image *, ip_image *, int);
    int (*ip_im_ii_op)(ip_image *, ip_image *);
    void (*f_ii_int_op)(ip_image *, ip_image *);
    void (*f_ii_float_op)(ip_image *, ip_image *);
    void (*f_ii_double_op)(ip_image *, ip_image *);
    void (*f_ii_complex_op)(ip_image *, ip_image *);
    void (*f_ii_dcomplex_op)(ip_image *, ip_image *);
    void (*f_ii_int_clipped_op)(ip_image *, ip_image *);
};

/* id field takes these numbers; must match array index */
enum {
    IM_ADD = 0,    IM_SUB, IM_MUL, IM_DIV,
    IM_OR, IM_XOR, IM_AND,
    IM_MAX, IM_MIN,
};

#define NUM_IDS (IM_MIN+1)

/* flags field takes these numbers */
#define NO_ZEROS_IN_DIVISOR 1
#define DO_BINARY 2

/* Tests we can run */
struct operator_list operators[NUM_IDS] = {
    {IM_ADD, 0, "im_add", "addition", im_add, NULL,
     im_ii_int_ADD, im_ii_float_ADD, im_ii_real_ADD,
     im_ii_complex_ADD_COMPLEX, im_ii_dcomplex_ADD_COMPLEX,
     im_ii_int_ADD_CLIPPED},
    {IM_SUB, 0, "im_sub", "subtraction", im_sub, NULL,
     im_ii_int_SUB, im_ii_float_SUB, im_ii_real_SUB,
     im_ii_complex_SUB_COMPLEX, im_ii_dcomplex_SUB_COMPLEX,
     im_ii_int_SUB_CLIPPED},
    {IM_MUL, 0, "im_mul", "multiplication", im_mul, NULL,
     im_ii_int_MUL, im_ii_float_MUL, im_ii_real_MUL,
     im_ii_complex_MUL_COMPLEX, im_ii_dcomplex_MUL_COMPLEX,
     im_ii_int_MUL_CLIPPED},
    {IM_DIV, NO_ZEROS_IN_DIVISOR, "im_div", "division", im_div, NULL,
     im_ii_int_DIV, im_ii_float_DIV, im_ii_real_DIV,
     im_ii_complex_DIV_COMPLEX, im_ii_dcomplex_DIV_COMPLEX,
     NULL},
    {IM_OR, DO_BINARY, "im_or", "logical or", NULL, im_or,
     im_ii_int_OR, NULL, NULL, NULL, NULL, NULL},
    {IM_XOR, DO_BINARY, "im_xor", "logical xor", NULL, im_xor,
     im_ii_int_XOR, NULL, NULL, NULL, NULL, NULL},
    {IM_AND, DO_BINARY, "im_and", "logical and", NULL, im_and,
     im_ii_int_AND, NULL, NULL, NULL, NULL, NULL},
    {IM_MAX, DO_BINARY, "im_min", "maximum", NULL, im_maximum,
     im_ii_int_MAX, im_ii_float_MAX, im_ii_real_MAX,
     NULL, NULL, NULL},
    {IM_MIN, DO_BINARY, "im_max", "minimum", NULL, im_minimum,
     im_ii_int_MIN, im_ii_float_MIN, im_ii_real_MIN,
     NULL, NULL, NULL}
};

const char *op_names[NUM_IDS];


static inline void run_libip_operator(int op, ip_image *d, ip_image *s, int flag)
{
    if (operators[op].ip_im_iif_op)
	operators[op].ip_im_iif_op(d, s, flag);
    else if (operators[op].ip_im_ii_op)
	operators[op].ip_im_ii_op(d, s);
}


int im_op_ii_check(int op, int type, ip_coord2d size, int flag, int source)
{
    ip_image *r, *d, *s, *s_copy;
    int failed = 0;
    char str[64];
    unsigned char *chksum;
    char *op_name = operators[op].op_name;

    if (operators[op].id != op) {
	fprintf(stderr, "ERROR: operator id numbers (%d and %d) don't match.\n",
		op, operators[op].id);
	fprintf(stderr, "FIX AND RECOMPILE ME.\n");
	exit(1000);
    }

    r = create_test_image(type, size, (source & DO_RANDOM));
    s = create_test_image(type, size, (source & DO_RANDOM) | DO_VWEDGE);

    d = im_copy(r);
    failed |= compare_images(r, d, "Initial compare of reference", "r", "d");

    set_image_row_padding(r, 0x53);
    failed |= check_image_row_padding(r, 0x53, "Set padding check padding");
    failed |= compare_images(r, d, "Set padding check image data", "r", "d");

    /* If doing integer division avoid any divisions by zero. */
    if (type_integer(type) && (operators[op].flags & NO_ZEROS_IN_DIVISOR))
	im_replace(s, 1.0, 0.0);

    /* Keep a copy of s so can check that s remains unaffected */
    s_copy = im_copy(s);

    /*
     * Create a reference image (r) that results from the operator applied
     *  between itself and the source image (s).
     */
    if (flag == FLG_CLIP) {
	if (type_integer(type) && operators[op].f_ii_int_clipped_op != NULL)
	    operators[op].f_ii_int_clipped_op(r, s);
    }else{
	switch (type) {
	case IM_BINARY:
	case IM_PALETTE:
	case IM_BYTE:
	case IM_UBYTE:
	case IM_SHORT:
	case IM_USHORT:
	case IM_LONG:
	case IM_ULONG:
	    if (operators[op].f_ii_int_op != NULL)
		operators[op].f_ii_int_op(r, s);
	    break;
	case IM_FLOAT:
	    if (operators[op].f_ii_float_op != NULL)
		operators[op].f_ii_float_op(r, s);
	    break;
	case IM_DOUBLE:
	    if (operators[op].f_ii_double_op != NULL)
		operators[op].f_ii_double_op(r, s);
	    break;
	case IM_COMPLEX:
	    if (operators[op].f_ii_complex_op != NULL)
		operators[op].f_ii_complex_op(r, s);
	    break;
	case IM_DCOMPLEX:
	    if (operators[op].f_ii_dcomplex_op != NULL)
		operators[op].f_ii_dcomplex_op(r, s);
	    break;
	default:
	    break;
	}
    }

    if (flag & FLG_PROTECT) {
        /* Set up some divide by zeros for testing. */
        SET_PIXEL_VALUE_FROM_SIGNED_LONG(d, 0L, 0, 0);
        SET_PIXEL_VALUE_FROM_SIGNED_LONG(d, 1L, 1, 0);
        SET_PIXEL_VALUE_FROM_SIGNED_LONG(d, -1L, 2, 0);
	/* Set up some matching numerators */
        SET_PIXEL_VALUE_FROM_SIGNED_LONG(s, 0L, 0, 0);
        SET_PIXEL_VALUE_FROM_SIGNED_LONG(s, 0L, 1, 0);
        SET_PIXEL_VALUE_FROM_SIGNED_LONG(s, 0L, 2, 0);
	/* Must duplicate changes to s into s_copy */
	SET_PIXEL_VALUE_FROM_SIGNED_LONG(s_copy, 0L, 0, 0);
        SET_PIXEL_VALUE_FROM_SIGNED_LONG(s_copy, 0L, 1, 0);
        SET_PIXEL_VALUE_FROM_SIGNED_LONG(s_copy, 0L, 2, 0);
       /* Put the expected answers into the reference r */
        SET_PIXEL_VALUE_FROM_SIGNED_LONG(r, 0L, 0, 0);
        SET_PIXEL_VALUE_FROM_SIGNED_LONG(r, ip_image_type_info[s->type].maxvalue, 1, 0);
        SET_PIXEL_VALUE_FROM_SIGNED_LONG(r,
		ip_image_type_info[s->type].minvalue == 0 ?
                ip_image_type_info[s->type].maxvalue :
                ip_image_type_info[s->type].minvalue, 2, 0);
    }

    /*
     * Now call the IP library image processing operator under test applied
     * between the destination image (d) and the source (s).
     */

    run_libip_operator(op, d, s, flag);

    /* Get md5 checksum of result if requested */
    if (source & DO_CHECKSUM) {
	chksum = im_md5(d);
    }

    /*
     * Compare d against r.  They should be exactly the same!
     * Also check source s is unchanged.
     */

    sprintf(str, "Image %s check", op_name);
    failed |= compare_images(r, d, str, "r", "d");
    sprintf(str, "Original image %s uncorrupted check", op_name);
    failed |= compare_images(s_copy, s, str, "s_copy", "s");
    sprintf(str, "Image %s padding check", op_name);
    failed |= check_image_row_padding(r, 0x53, str);

    /*
     * We don't check padding on d as the IP library is permitted to modify
     * the row padding at its whim (which it will normally do so if using
     * vector based (SIMD) operations).
     */

    /* Message of the operator and type of image tested */
    printf("%-6s %-8s %7s ", operators[op].name,
	   ip_image_type_info[type].name,
	   (flag == FLG_CLIP) ? "clipped" : "       ");

    if (source & DO_CHECKSUM) {
	print_md5(chksum);
	putchar('\n');
    }else{
	uint64_t time;
	uint64_t key = KEY_CONSTRUCT(op, type, 0, flag);

	if (source & DO_TIMING) {
	    /* Time the operator when requested */
	    time_start();
	    for (int j=0; j<10; j++) {
		run_libip_operator(op, d, s, flag);
	    }
	    time = time_end() / 10;
	}else{
	    time = -1LL;
	}
	print_time_status(key, cycles, time, ipc, failed);
    }

    ip_free_image(s_copy);
    ip_free_image(s);
    ip_free_image(d);
    ip_free_image(r);

    /* Increment counts (global variables) */
    num_tests++;
    if (failed) num_failures++;

    return failed;
}


static void run_with_and_without_flags(int op, int type,
				       ip_coord2d size, int flags, int chk_flag)
{
    if (flags)
	im_op_ii_check(op, type, size, flags, chk_flag);
    im_op_ii_check(op, type, size, NO_FLAG, chk_flag);
}



static void run_test_bank(int op, ip_coord2d size, int flags, int chk_flag)
{
    if (operators[op].flags & DO_BINARY)
	im_op_ii_check(op, IM_BINARY, size, flags, chk_flag);

    /* Do integer types */

    run_with_and_without_flags(op, IM_UBYTE, size, flags, chk_flag);
    run_with_and_without_flags(op, IM_BYTE, size, flags, chk_flag);
    run_with_and_without_flags(op, IM_USHORT, size, flags, chk_flag);
    run_with_and_without_flags(op, IM_SHORT, size, flags, chk_flag);

    flags &= ~FLG_CLIP;

    im_op_ii_check(op, IM_ULONG, size, flags, chk_flag);
    im_op_ii_check(op, IM_LONG, size, flags, chk_flag);

    /* Do floating point types */

    if (operators[op].f_ii_float_op)
	im_op_ii_check(op, IM_FLOAT, size, NO_FLAG, chk_flag);
    if (operators[op].f_ii_double_op)
	im_op_ii_check(op, IM_DOUBLE, size, NO_FLAG, chk_flag);

    /* Do complex types */

    if (operators[op].f_ii_complex_op)
	im_op_ii_check(op, IM_COMPLEX, size, NO_FLAG, chk_flag);
    if (operators[op].f_ii_dcomplex_op)
	im_op_ii_check(op, IM_DCOMPLEX, size, NO_FLAG, chk_flag);
}


void run_im_ii_tests(int operator, ip_coord2d size, int chk_flag)
{
    int flags;

    /* Choose correct flags to test */

    switch (operator) {
    case IM_ADD:
    case IM_SUB:
    case IM_MUL:
	flags = FLG_CLIP;
	break;
    case IM_DIV:
	flags = FLG_PROTECT;
	break;
    default:
	flags = NO_FLAG;
	break;
    }

    if (chk_flag & DO_PIXEL) {
	printf("CHECKING basic pixelwise %s code:\n",operators[operator].op_name);

	EXPECT_NO_ERRORS;

	ip_clear_global_flags(IP_FAST_CODE | IP_HWACCEL);

	run_test_bank(operator, size, flags, chk_flag);

	if (operator == IM_ADD || operator == IM_SUB) {
	    printf("CHECKING fast code (\"poor man's SIMD\"):\n");

	    ip_set_global_flags(IP_FAST_CODE);

	    im_op_ii_check(operator, IM_UBYTE, size, NO_FLAG, chk_flag);
	    im_op_ii_check(operator, IM_BYTE, size, NO_FLAG, chk_flag);
	    im_op_ii_check(operator, IM_USHORT, size, NO_FLAG, chk_flag);
	    im_op_ii_check(operator, IM_SHORT, size, NO_FLAG, chk_flag);
	    im_op_ii_check(operator, IM_ULONG, size, NO_FLAG, chk_flag);
	    im_op_ii_check(operator, IM_LONG, size, NO_FLAG, chk_flag);
	}
    }

    if (chk_flag & DO_SIMD) {
	printf("CHECKING hardware (SIMD) acceleration (if available):\n");

	ip_clear_global_flags(IP_FAST_CODE);
	ip_set_global_flags(IP_HWACCEL);

	run_test_bank(operator, size, flags, chk_flag);
    }
}


int run_im_ii_error_reporting_tests(int operator, ip_coord2d size)
{
    ip_image *i1, *i2;
    int failed = 0;

    printf("CHECKING errors reported correctly:\n");

    EXPECT_ERRORS(ERR_NOT_SAME_SIZE, ERR_NOT_SAME_TYPE);
    i1 = ip_alloc_image(IM_UBYTE, size);
    i2 = ip_alloc_image(IM_USHORT, size);

    /* Should generate ERR_NOT_SAME_TYPE */
    MARK(1);
    run_libip_operator(operator, i1, i2, NO_FLAG);
    CHECK_SEEN_ERROR_WITH_MSG;

    ip_free_image(i2);
    i2 = ip_alloc_image(IM_UBYTE, (ip_coord2d){1000, size.y+1});

    /* Should generate ERR_NOT_SAME_SIZE */
    MARK(2);
    run_libip_operator(operator, i1, i2, NO_FLAG);
    CHECK_SEEN_ERROR_WITH_MSG;

    ip_free_image(i1);
    ip_free_image(i2);

    EXPECT_ERRORS(ERR_BAD_TYPE, 0);
    i1 = ip_alloc_image(IM_PALETTE, size);
    i2 = ip_alloc_image(IM_PALETTE, size);

    /* Should generate ERR_BAD_TYPE */
    MARK(3);
    run_libip_operator(operator, i1, i2, NO_FLAG);
    CHECK_SEEN_ERROR_WITH_MSG;

    ip_free_image(i1);
    ip_free_image(i2);

    EXPECT_ERRORS(ERR_BAD_FLAG, ERR_BAD_FLAG2);
    i1 = ip_alloc_image(IM_BYTE, size);
    i2 = ip_alloc_image(IM_BYTE, size);

    /* Should generate one of ERR_BAD_FLAG or ERR_BAD_FLAG2 */
    if (operators[operator].ip_im_iif_op) {
	MARK(4);
	run_libip_operator(operator, i1, i2, FLG_REAL);
	CHECK_SEEN_ERROR_WITH_MSG;
    }

    ip_free_image(i1);
    ip_free_image(i2);

    num_tests++;
    if (failed) num_failures++;
    printf("%-6s error reporting:                                    [%s]\n",
	   operators[operator].name, failed ? "FAIL" : "OK");

    return failed;
}



int main(int argc, char *argv[])
{
    ip_coord2d size = {1023, 1025};
    struct cmd_line_requests *clr;

    for (int j=0; j<NUM_IDS; j++)
	op_names[j] = operators[j].name;

    clr = process_cmdline(argc, argv, NUM_IDS, op_names);

    if (clr->flags & DO_SIZE)
	size = clr->im_size;

    ip_register_error_handler(error_handler);

    if (clr->op_to_test < 0) {
	for (int j=0; j<NUM_IDS; j++) {
	    if (clr->flags & DO_ERROR)
		run_im_ii_error_reporting_tests(j, size);
	    run_im_ii_tests(j, size, clr->flags);
	    if (j < NUM_IDS - 1) {
		printf("\n");
	    }
	}
    }else{
	if (clr->flags & DO_ERROR)
	    run_im_ii_error_reporting_tests(clr->op_to_test, size);
	run_im_ii_tests(clr->op_to_test, size, clr->flags);
    }

    print_final_stats(num_tests, num_failures, 0);

    return num_failures;
}
