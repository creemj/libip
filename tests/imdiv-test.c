#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <math.h>

#include <ip/types.h>
#include <ip/image.h>
#include <ip/error.h>
#include <ip/proto.h>
#include <ip/flags.h>

#include "test-macros.h"
#include "timing.h"
#include "helpers.h"
#include "errorhdl.h"


char *program_name = "imdiv-test";

#define PIXEL_OPERATION_DIV(dval, sval, d)  dval /= sval

im_ii_int_DEFN(DIV)
im_ii_real_DEFN(DIV)

#define PIXEL_OPERATION_DIV_COMPLEX(dval, sval, d) {	\
	__typeof(dval.r) dc,tmp,denom,u,v;		\
	u = sval.r;					\
	v = sval.i;					\
	if (fabs(u) > fabs(v)) {			\
	    dc = v/u;					\
	    denom = u + v*dc;				\
	    tmp = (dval.r + dval.i * dc) / denom;	\
	    dval.i = (dval.i - dval.r * dc) / denom;	\
	    dval.r = tmp;				\
	}else{						\
	    dc = u/v;					\
	    denom = u*dc + v;				\
	    tmp = (dval.r*dc + dval.i) / denom;		\
	    dval.i = (dval.i*dc - dval.r) / denom;	\
	    dval.r = tmp;				\
	}						\
    }

im_ii_complex_DEFN(DIV_COMPLEX)
im_ii_dcomplex_DEFN(DIV_COMPLEX)


long im_div_check(int type, ip_coord2d size, int flag, char *msg)
{
    ip_image *r, *d, *s;
    int failed = 0;
    double scale;
    double offset;
    long time;

    r = ip_alloc_image(type, size);

    scale = 1.2*(ip_image_type_info[type].maxvalue 
		 - ip_image_type_info[type].minvalue) / size.x;
    offset = ip_image_type_info[type].minvalue;
    im_create(r, (ip_coord2d){0,0}, scale, offset, 1.0, TI_HWEDGE);
    d = im_copy(r);
    failed |= compare_images(r, d, "Initial compare of reference", "r", "d");

    set_image_row_padding(r, 0x53);
    failed |= check_image_row_padding(r, 0x53, "Set padding check padding");
    failed |= compare_images(r, d, "Set padding check image data", "r", "d");

    s = ip_alloc_image(type, size);
    im_create(s, (ip_coord2d){0,0}, scale, 0.0, 1.0, TI_VWEDGE);

    /* On many systems division by zero causes exceptions so protect against
       it */
    im_replace(s, 1, 0);

    im_ii_int_DIV(r, s);

    if (flag & FLG_PROTECT) {
	/* Set up some divide by zeros for testing. */
	SET_PIXEL_VALUE_FROM_SIGNED_LONG(d, 0L, 0, 0);
	SET_PIXEL_VALUE_FROM_SIGNED_LONG(d, 1L, 1, 0);
	SET_PIXEL_VALUE_FROM_SIGNED_LONG(d, -1L, 2, 0);
	SET_PIXEL_VALUE_FROM_SIGNED_LONG(s, 0L, 0, 0);
	SET_PIXEL_VALUE_FROM_SIGNED_LONG(s, 0L, 1, 0);
	SET_PIXEL_VALUE_FROM_SIGNED_LONG(s, 0L, 2, 0);
	SET_PIXEL_VALUE_FROM_SIGNED_LONG(r, 0L, 0, 0);
	SET_PIXEL_VALUE_FROM_SIGNED_LONG(r, ip_image_type_info[s->type].maxvalue, 1, 0);
	SET_PIXEL_VALUE_FROM_SIGNED_LONG(r, 
	        ip_image_type_info[s->type].minvalue == 0 ?
		ip_image_type_info[s->type].maxvalue :
		ip_image_type_info[s->type].minvalue, 2, 0);
    }

    time_start();
    im_div(d, s, flag);
    time = time_end();

    failed |= compare_images(r, d, "Image mul check", "r", "d");
    failed |= check_image_row_padding(r, 0x53, "Image mul padding check");

    printf("%s: [%ld us] %s\n", msg, time, failed ? "FAILED" : "OK");

    ip_free_image(s);
    ip_free_image(d);
    ip_free_image(r);

    return time;
}


long im_real_div_check(int type, ip_coord2d size, int flag, char *msg)
{
    ip_image *r, *d, *s;
    int failed = 0;
    double scale;
    double offset;
    long time;

    r = ip_alloc_image(type, size);

    scale = (0.1*ip_image_type_info[type].maxvalue
		     - 0.1*ip_image_type_info[type].minvalue) / size.x;
    offset = -0.5*scale;

    if (type_real(type)) {
	im_create(r, (ip_coord2d){0,0}, scale, offset, 1.0, TI_HWEDGE);
    } else {
	im_random(r, offset, offset+scale, FLG_RANDOM_UNIFORM);
    }
    d = im_copy(r);
    failed |= compare_images(r, d, "Initial compare of reference", "r", "d");

    set_image_row_padding(r, 0x53);
    failed |= check_image_row_padding(r, 0x53, "Set padding check padding");
    failed |= compare_images(r, d, "Set padding check image data", "r", "d");

    s = ip_alloc_image(type, size);
    if (type_real(type)) {
	im_create(s, (ip_coord2d){0,0}, scale, 0.0, 1.0, TI_VWEDGE);
    } else {
	im_random(s, offset, offset+scale, FLG_RANDOM_UNIFORM);	
    }

    if (type_real(type))
	im_ii_real_DIV(r, s);
    else if (type == IM_DCOMPLEX)
	im_ii_dcomplex_DIV_COMPLEX(r, s);
    else
	im_ii_complex_DIV_COMPLEX(r,s);


    time_start();
    im_div(d, s, flag);
    time = time_end();

    failed |= compare_images(r, d, "Image mul check", "r", "d");
    failed |= check_image_row_padding(r, 0x53, "Image mul padding check");

    printf("%s: [%ld us] %s\n", msg, time, failed ? "FAILED" : "OK");

    ip_free_image(s);
    ip_free_image(d);
    ip_free_image(r);

    return time;
}



int main(int argc, char *argv[])
{
    ip_image *i1, *i2;
    ip_coord2d size = {1023, 1025};
    int failed = 0;

    ip_register_error_handler(error_handler);

    EXPECT_ERRORS(ERR_NOT_SAME_SIZE, ERR_NOT_SAME_TYPE);
    i1 = ip_alloc_image(IM_UBYTE, size);
    i2 = ip_alloc_image(IM_USHORT, size);
    MARK(1);
    im_mul(i1, i2, NO_FLAG);
    CHECK_SEEN_ERROR;

    ip_free_image(i2);
    i2 = ip_alloc_image(IM_UBYTE, (ip_coord2d){1000, 1000});
    MARK(2);
    im_div(i1, i2, NO_FLAG);
    CHECK_SEEN_ERROR;

    ip_free_image(i1);
    ip_free_image(i2);

    EXPECT_ERRORS(ERR_BAD_TYPE, 0);
    i1 = ip_alloc_image(IM_PALETTE, size);
    i2 = ip_alloc_image(IM_PALETTE, size);
    MARK(3);
    im_div(i1, i2, NO_FLAG);
    CHECK_SEEN_ERROR;

    ip_free_image(i1);
    ip_free_image(i2);

    EXPECT_ERRORS(ERR_BAD_FLAG, ERR_BAD_FLAG2);
    i1 = ip_alloc_image(IM_BYTE, size);
    i2 = ip_alloc_image(IM_BYTE, size);
    MARK(4);
    im_div(i1, i2, FLG_REAL);
    CHECK_SEEN_ERROR;

    ip_free_image(i1);
    ip_free_image(i2);

    printf("im_div error reporting: %s\n", failed ? "FAIL" : "OK");

    EXPECT_NO_ERRORS;

    printf("CHECKING basic pixelwise division code:\n");

    im_div_check(IM_UBYTE, size, NO_FLAG, "im_div UBYTE");
    im_div_check(IM_BYTE, size, NO_FLAG, "im_div BYTE");
    im_div_check(IM_USHORT, size, NO_FLAG, "im_div USHORT" );
    im_div_check(IM_SHORT, size, NO_FLAG, "im_div SHORT" );
    im_div_check(IM_ULONG, size, NO_FLAG, "im_div ULONG" );
    im_div_check(IM_LONG, size, NO_FLAG, "im_div LONG" );
    im_real_div_check(IM_FLOAT, size, NO_FLAG, "im_div FLOAT" );
    im_real_div_check(IM_DOUBLE, size, NO_FLAG, "im_div DOUBLE" );
    im_real_div_check(IM_COMPLEX, size, NO_FLAG, "im_div COMPLEX" );
    im_real_div_check(IM_DCOMPLEX, size, NO_FLAG, "im_div DCOMPLEX" );

    printf("CHECKING pixelwise code with FLG_PROTECT:\n");

    im_div_check(IM_UBYTE, size, FLG_PROTECT, "im_div UBYTE protected");
    im_div_check(IM_BYTE, size, FLG_PROTECT, "im_div BYTE protected");
    im_div_check(IM_USHORT, size, FLG_PROTECT, "im_div USHORT protected");
    im_div_check(IM_SHORT, size, FLG_PROTECT, "im_div SHORT protected");

    printf("CHECKING hardware (SIMD) acceleration (if available):\n");

    ip_set_global_flags(IP_HWACCEL);

    im_real_div_check(IM_FLOAT, size, NO_FLAG, "im_div FLOAT" );
    im_real_div_check(IM_DOUBLE, size, NO_FLAG, "im_div DOUBLE" );

    /* EXPECT_ERRORS(10001, ERR_MATHS_EXCEPTION); */
    /* EXPECT_MULTIPLE_ERRORS; */


    return 0;
}
