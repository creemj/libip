/* To get srandom() */
#define _XOPEN_SOURCE 600

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <getopt.h>

#include <ip/ip.h>

#include "test-macros.h"
#include "timing.h"
#include "helpers.h"
#include "errorhdl.h"

char *program_name = "logical-test";

/* Keep counts of failed and tested routines as globals */
int num_tests;
int num_failures;


/*
 * Logical operators between two images
 */

#define PIXEL_OPERATION_OR(dval, sval, d)  dval |= sval
#define PIXEL_OPERATION_XOR(dval, sval, d)  dval ^= sval
#define PIXEL_OPERATION_AND(dval, sval, d)  dval &= sval

im_ii_int_DEFN(OR)
im_ii_int_DEFN(XOR)
im_ii_int_DEFN(AND)

#define PIXEL_OPERATION_iNOT(dval, d) dval = ~dval;

im_i_int_DEFN(iNOT)


struct operator_list {
    const int id;
    const char *name;
    const char *op_name;
    int (*ip_im_op)(ip_image *, ip_image *);
    void (*f_ii_int_op)(ip_image *, ip_image *);
};

/* id field takes these numbers; must match array index */
#define IM_OR 0
#define IM_XOR 1
#define IM_AND 2
#define IM_NOT 3

#define NUM_IDS 4


/* Tests we can run */
struct operator_list operators[NUM_IDS] = {
    {IM_OR,  "im_or",  "logical or", im_or, im_ii_int_OR},
    {IM_XOR, "im_xor", "logical xor", im_xor, im_ii_int_XOR},
    {IM_AND, "im_and", "logical and", im_and, im_ii_int_AND},
    {IM_NOT, "im_not", "logical not", NULL, NULL}
};

/* For passing operator names to argument parsing. */
const char *op_names[NUM_IDS];


int im_op_ii_check(int op, int type, ip_coord2d size, int source)
{
    ip_image *r, *d, *s;
    int failed = 0;
    char str[64];
    unsigned char *chksum;
    const char *op_name = operators[op].op_name;

    if (operators[op].id != op) {
	fprintf(stderr, "ERROR: operator id numbers (%d and %d) don't match.\n",
		op, operators[op].id);
	fprintf(stderr, "FIX AND RECOMPILE ME.\n");
	exit(255);
    }

    r = create_test_image(type, size, (source & DO_RANDOM));
    if (op != IM_NOT)
	s = create_test_image(type, size, (source & DO_RANDOM) | DO_VWEDGE);

    d = im_copy(r);
    failed |= compare_images(r, d, "Initial compare of reference", "r", "d");

    set_image_row_padding(r, 0x53);
    failed |= check_image_row_padding(r, 0x53, "Set padding check padding");
    failed |= compare_images(r, d, "Set padding check image data", "r", "d");

    /*
     * Create a reference image (r) that results from the operator applied
     *  between itself and the source image (s).
     *
     * Also call the IP library image processing operator under test applied
     * between the destination image (d) and the source (s).
     *
     * (Except NOT operator operates on one image only)
     */

    if (op == IM_NOT) {
	im_i_int_iNOT(r);
	im_not(d);
    }else{
	operators[op].f_ii_int_op(r, s);
	operators[op].ip_im_op(d, s);
    }

    /* Compare d against r.  They should be exactly the same! */

    sprintf(str, "Image %s check", op_name);
    failed |= compare_images(r, d, str, "r", "d");
    sprintf(str, "Image %s padding check", op_name);
    failed |= check_image_row_padding(r, 0x53, str);

    /*
     * We don't check padding on d as the IP library is permitted to modify
     * the row padding at its whim (which it will normally do so if using
     * vector based (SIMD) operations).
     */

    /* Message of the operator and type of image tested */
    printf("%-6s %-8s       ", operators[op].name, ip_image_type_info[type].name);

    /* Print out comparison result including timing info if requested. */
    if (source & DO_CHECKSUM) {
        chksum = im_md5(d);
	print_md5(chksum);
	putchar('\n');
    }else{
	uint64_t time;
	uint64_t key = KEY_CONSTRUCT(type, op, 0, 0);

	if (source & DO_TIMING) {
	    /* Time the operator when requested */
	    time_start();
	    for (int j=0; j<10; j++) {
		if (op == IM_NOT)
		    im_not(r);
		else
		    operators[op].ip_im_op(d, s);
	    }
	    time = time_end() / 10;
	}else{
	    time = -1LL;
	}
	print_time_status(key, cycles, time, ipc, failed);
    }

    if (op != IM_NOT)
	ip_free_image(s);

    ip_free_image(d);
    ip_free_image(r);

    /* Increment counts (global variables) */
    num_tests++;
    if (failed) num_failures++;

    return failed;
}


void run_im_ii_tests(int operator, ip_coord2d size, int chk_flag)
{
    EXPECT_NO_ERRORS;

    if (chk_flag & DO_PIXEL) {
	printf("CHECKING basic pixelwise %s code:\n",operators[operator].op_name);

	ip_clear_global_flags(IP_HWACCEL | IP_FAST_CODE);

	im_op_ii_check(operator, IM_BINARY, size, chk_flag);
	im_op_ii_check(operator, IM_UBYTE, size, chk_flag);
	im_op_ii_check(operator, IM_BYTE, size, chk_flag);
	im_op_ii_check(operator, IM_USHORT, size, chk_flag);
	im_op_ii_check(operator, IM_SHORT, size, chk_flag);
	im_op_ii_check(operator, IM_ULONG, size, chk_flag);
	im_op_ii_check(operator, IM_LONG, size, chk_flag);
    }

    if (chk_flag & DO_SIMD) {
	printf("CHECKING hardware (SIMD) acceleration (if available):\n");

	ip_set_global_flags(IP_HWACCEL);

	im_op_ii_check(operator, IM_BINARY, size, chk_flag);
	im_op_ii_check(operator, IM_UBYTE, size, chk_flag);
	im_op_ii_check(operator, IM_BYTE, size, chk_flag);
	im_op_ii_check(operator, IM_USHORT, size, chk_flag);
	im_op_ii_check(operator, IM_SHORT, size, chk_flag);
	im_op_ii_check(operator, IM_ULONG, size, chk_flag);
	im_op_ii_check(operator, IM_LONG, size, chk_flag);
    }
}


int run_im_ii_error_reporting_tests(int operator, ip_coord2d size, int chk_flag)
{
    ip_image *i1, *i2;
    int failed = 0;

    if (NOT (chk_flag & DO_ERROR))
	return 0;

    printf("CHECKING errors reported correctly:\n");

    if (operator == IM_NOT) {

	EXPECT_ERRORS(ERR_BAD_TYPE, 0);
	i1 = ip_alloc_image(IM_COMPLEX, size);

	/* Should generate ERR_BAD_TYPE */
	MARK(3);
	im_not(i1);
	CHECK_SEEN_ERROR_WITH_MSG;

	ip_free_image(i1);

    }else{

	EXPECT_ERRORS(ERR_NOT_SAME_SIZE, ERR_NOT_SAME_TYPE);
	i1 = ip_alloc_image(IM_UBYTE, size);
	i2 = ip_alloc_image(IM_USHORT, size);

	/* Should generate ERR_NOT_SAME_TYPE */
	MARK(1);
	operators[operator].ip_im_op(i1, i2);
	CHECK_SEEN_ERROR_WITH_MSG;

	ip_free_image(i2);
	i2 = ip_alloc_image(IM_UBYTE, (ip_coord2d){1000, 1000});

	/* Should generate ERR_NOT_SAME_SIZE */
	MARK(2);
	operators[operator].ip_im_op(i1, i2);
	CHECK_SEEN_ERROR_WITH_MSG;

	ip_free_image(i1);
	ip_free_image(i2);

	EXPECT_ERRORS(ERR_BAD_TYPE, 0);
	i1 = ip_alloc_image(IM_PALETTE, size);
	i2 = ip_alloc_image(IM_PALETTE, size);

	/* Should generate ERR_BAD_TYPE */
	MARK(3);
	operators[operator].ip_im_op(i1, i2);
	CHECK_SEEN_ERROR_WITH_MSG;

	ip_free_image(i1);
	ip_free_image(i2);

    }

    num_tests++;
    if (failed) num_failures++;
    printf("%-6s error reporting:                                  [%s]\n",
	   operators[operator].name, failed ? "FAIL" : "OK");

    return failed;
}



int main(int argc, char *argv[])
{
    ip_coord2d size = {1023, 1025};
    int operator_to_test;
    int chk_flag;
    struct cmd_line_requests *clr;

    for (int j=0; j<NUM_IDS; j++)
	op_names[j] = operators[j].name;

    clr = process_cmdline(argc, argv, NUM_IDS, op_names);

    operator_to_test = clr->op_to_test;
    chk_flag = clr->flags;
    if (clr->flags & DO_SIZE)
        size = clr->im_size;

    ip_register_error_handler(error_handler);

    if (operator_to_test < 0) {
	for (int j=0; j<NUM_IDS; j++) {
	    run_im_ii_error_reporting_tests(j, size, chk_flag);
	    run_im_ii_tests(j, size, chk_flag);
	    if (j < NUM_IDS - 1) {
		printf("\n");
	    }
	}
    }else{
	run_im_ii_error_reporting_tests(operator_to_test, size, chk_flag);
	run_im_ii_tests(operator_to_test, size, chk_flag);
    }

    print_final_stats(num_tests, num_failures, 0);

    return num_failures;
}
