/* To get srandom() */
#define _XOPEN_SOURCE 600

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include <limits.h>
#include <float.h>
#include <unistd.h>
#include <getopt.h>

#include <ip/ip.h>
#include <ip/morph.h>
#include <ip/tiff.h>

#include "test-macros.h"
#include "timing.h"
#include "helpers.h"
#include "errorhdl.h"

#include "../src/internal.h"

char *program_name = "reconstruct-test";

extern int ip_reconstruct_iterations;

/* Keep counts of failed and tested routines as globals */
int num_tests;
int num_failures;


#define NUM_IDS 1

/* For passing operator names to argument parsing. */
const char *op_names[NUM_IDS] = {
    "reconstruct"
};



/*
 * Straight-forward algorithm for morphological reconstruction.  This
 * provides the reference implementation but it can be extremely slow.
 */

static int im_reconstruct_naive(ip_image *marker, ip_image *mask, int flag)
{
    ip_strel *se;
    ip_image *prev;
    int iterating;
    int from_above;

    ip_error = OK;

    from_above = flag & FLG_RECON_FROM_ABOVE;
    if ((flag & 0x0f) == IP_STREL_N8) {
        if ((se = ip_alloc_strel(IP_STREL_EIGHT_NEIGHBOURS)) == NULL)
            return ip_error;
    }else{
        if ((se = ip_alloc_strel(IP_STREL_FOUR_NEIGHBOURS)) == NULL)
            return ip_error;
    }

    iterating = TRUE;
    while (iterating) {
        prev = im_copy(marker);
        if (! prev)
            goto irn_exit;
        if (from_above) {
            im_erode(marker, se);
            im_maximum(marker, mask);
        }else{
            im_dilate(marker, se);
            im_minimum(marker, mask);
        }
        ip_reconstruct_iterations++;
        iterating = im_differ(prev, marker) > 0;
        ip_free_image(prev);
    }

 irn_exit:
    ip_free_strel(se);
    return ip_error;
}



static void save_image(ip_image *im, char *msg)
{
    char fn[128];

    snprintf(fn, 128, "reconstruct-%s-%s.tiff", ip_type_name(im->type), msg);
    write_tiff_image(fn, im);
}

#define CASE_HALF_VALUE(tt, mm, ms, x, y, t)				\
    case IM_##tt:							\
    IM_PIXEL(mm, x, y, t) = (((IM_PIXEL(ms, x, y, t) - tt##_min) / 2) + tt##_min)

#define CASE_HALF_VALUE_INT_SIGNED(tt, mm, ms, x, y, t)			\
    case IM_##tt:							\
    IM_PIXEL(mm, x, y, t) = (IM_PIXEL(ms, x, y, t) > (tt##_min+10)) ? (IM_PIXEL(ms, x, y, t) - 10) : tt##_min

#define CASE_FULL_VALUE(tt, mm, ms, x, y, t)				\
    case IM_##tt:							\
    IM_PIXEL(mm, x, y, t) = IM_PIXEL(ms, x, y, t)


static void SET_PIXEL_TO_HALF(ip_image *mm, ip_image *ms, int x, int y)
{
    switch(ms->type) {
    case IM_BINARY:
        IM_PIXEL(mm, x, y, ub) = 255;
        IM_PIXEL(ms, x, y, ub) = 255;
        break;
	CASE_HALF_VALUE(UBYTE, mm, ms, x, y, ub);
	break;
	CASE_HALF_VALUE_INT_SIGNED(BYTE, mm, ms, x, y, b);
	break;
	CASE_HALF_VALUE(USHORT, mm, ms, x, y, us);
	break;
	CASE_HALF_VALUE_INT_SIGNED(SHORT, mm, ms, x, y, s);
	break;
	CASE_HALF_VALUE(ULONG, mm, ms, x, y, ul);
	break;
	CASE_HALF_VALUE_INT_SIGNED(LONG, mm, ms, x, y, l);
	break;
	CASE_HALF_VALUE(FLOAT, mm, ms, x, y, f);
	break;
	CASE_HALF_VALUE(DOUBLE, mm, ms, x, y, d);
	break;
    default:
	break;
    }
}

static void SET_PIXEL_TO_FULL(ip_image *mm, ip_image *ms, int x, int y)
{
    switch(ms->type) {
    case IM_BINARY:
        IM_PIXEL(mm, x, y, ub) = 255;
        IM_PIXEL(ms, x, y, ub) = 255;
        break;
	CASE_FULL_VALUE(UBYTE, mm, ms, x, y, ub);
	break;
	CASE_FULL_VALUE(BYTE, mm, ms, x, y, b);
	break;
	CASE_FULL_VALUE(USHORT, mm, ms, x, y, us);
	break;
	CASE_FULL_VALUE(SHORT, mm, ms, x, y, s);
	break;
	CASE_FULL_VALUE(ULONG, mm, ms, x, y, ul);
	break;
	CASE_FULL_VALUE(LONG, mm, ms, x, y, l);
	break;
	CASE_FULL_VALUE(FLOAT, mm, ms, x, y, f);
	break;
	CASE_FULL_VALUE(DOUBLE, mm, ms, x, y, d);
	break;
    default:
	break;
    }
}

static int im_recon_check(int type, ip_coord2d size, int flags, int chk_flag)
{
    ip_image *marker, *mask, *marker_original, *mask_original, *ref;
    int failed = 0;
    unsigned char *chksum;
    uint64_t key = KEY_CONSTRUCT(1, type, 0, flags);

    mask = create_test_image(type, size, (chk_flag & DO_RANDOM));
    marker = ip_alloc_image(type, size);
    im_set(marker, ip_type_min(type), NO_FLAG);
    SET_PIXEL_TO_HALF(marker, mask, size.x/4, size.y/4);
    SET_PIXEL_TO_HALF(marker, mask, size.x/4+1, size.y/4);
    SET_PIXEL_TO_FULL(marker, mask, size.x/4+2, size.y/4);
    SET_PIXEL_TO_HALF(marker, mask, size.x/2, size.y/4);
    SET_PIXEL_TO_HALF(marker, mask, 3*size.x/4, size.y/4);
    SET_PIXEL_TO_HALF(marker, mask, size.x/4, size.y/2);
    SET_PIXEL_TO_HALF(marker, mask, size.x/2, size.y/2);
    SET_PIXEL_TO_HALF(marker, mask, 3*size.x/4, size.y/2);

    marker_original = im_copy(marker);
    ref = im_copy(marker);
    mask_original = im_copy(mask);

    if (chk_flag & DO_SAVE) {
        save_image(marker, "original-marker");
        save_image(mask, "original-mask");
    }

    im_reconstruct(marker, mask, flags);

    if (chk_flag & DO_SAVE) {
        save_image(marker, "result");
    }

    failed |= compare_images(mask_original, mask, "Not modified check", "mask_org", "mask");

    im_reconstruct_naive(ref, mask, flags);
    
    failed |= compare_images(ref, marker, "Image reconstruct check", "ref", "marker");

    /* Get md5 checksum of result if requested */
    if (chk_flag & DO_CHECKSUM) {
        chksum = im_md5(marker);
    }

    printf("reconstruct %-8s  %04x (%4d)  ", ip_type_name(type), flags, ip_reconstruct_iterations);

    /* Print out comparison result including timing info if requested. */
    if (chk_flag & DO_CHECKSUM) {
	print_md5(chksum);
	putchar('\n');
    }else{
	uint64_t time;

	if (chk_flag & DO_TIMING) {
	    time_start();
	    im_reconstruct(marker_original, mask, flags);
	    time = time_end();
        }else{
	    time = -1LL;
	}
	print_time_status(key, cycles, time, ipc, failed);
    }

    ip_free_image(ref);
    ip_free_image(marker);
    ip_free_image(marker_original);
    ip_free_image(mask);
    ip_free_image(mask_original);

    /* Increment counts (global variables) */
    num_tests++;
    if (failed) num_failures++;

    return failed;
}



void run_im_recon_tests(ip_coord2d size, int chk_flag)
{
    EXPECT_NO_ERRORS;

    if (chk_flag & DO_PIXEL) {
	printf("CHECKING basic pixelwise %s code:\n","reconstruct");;

	ip_clear_global_flags(IP_HWACCEL);

	im_recon_check(IM_UBYTE, size, IP_STREL_N4, chk_flag);
	im_recon_check(IM_UBYTE, size, IP_STREL_N8, chk_flag);
	im_recon_check(IM_USHORT, size, IP_STREL_N4, chk_flag);
	im_recon_check(IM_USHORT, size, IP_STREL_N8, chk_flag);
	im_recon_check(IM_SHORT, size, IP_STREL_N4, chk_flag);
	im_recon_check(IM_SHORT, size, IP_STREL_N8, chk_flag);
	im_recon_check(IM_ULONG, size, IP_STREL_N4, chk_flag);
	im_recon_check(IM_ULONG, size, IP_STREL_N8, chk_flag);
	im_recon_check(IM_LONG, size, IP_STREL_N4, chk_flag);
	im_recon_check(IM_LONG, size, IP_STREL_N8, chk_flag);
    }

#if 0
    /* There is currently no SIMD implementation */
    if (chk_flag & DO_SIMD) {
	printf("CHECKING hardware (SIMD) acceleration (if available):\n");

	ip_set_global_flags(IP_HWACCEL);

    }
#endif
}


int main(int argc, char *argv[])
{
    ip_coord2d size = {1051, 1021};
    struct cmd_line_requests *clr;

    clr = process_cmdline(argc, argv, NUM_IDS, op_names);

    ip_register_error_handler(error_handler);

    if (clr->flags & DO_SIZE)
	size = clr->im_size;

    run_im_recon_tests(size, clr->flags);

    print_final_stats(num_tests, num_failures, 0);

    return num_failures;
}
