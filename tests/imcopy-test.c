#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>
#include <inttypes.h>
#include <float.h>
#include <math.h>
#include <unistd.h>
#include <getopt.h>

#include <ip/ip.h>
#include <ip/tiff.h>

#include "errorhdl.h"
#include "timing.h"
#include "helpers.h"

char * program_name = "copy-test";



/* Keep counts of failed and tested routines as globals */
int num_tests;
int num_failures;


const char *op_names[] = {
    "im_copy"
};

#define NUM_OPS 1


static void save_image(ip_image *im, const char *info)
{
    char fstr[128];

    snprintf(fstr, 128, "copy-%s-%s.tiff", ip_type_name(im->type), info);
    write_tiff_image(fstr, im);
}


void copy_image_check(int type, ip_coord2d size, int chk_flag)
{
    ip_image *im, *result;
    unsigned char *chksum;
    int failed = 0;

    im = create_test_image(type, size, chk_flag & DO_RANDOM);

    if (chk_flag & DO_SAVE) {
        save_image(im, "original");
    }

    result = im_copy(im);

    /* Get md5 checksum of result if requested */
    if (chk_flag & DO_CHECKSUM) {
        chksum = im_md5(im);
    }

    failed = compare_images(result, im, "Image copy check", "d", "s");
    /*
     * TBD:
     * We should also check the palette of the PALETTE image.
     */

    if (chk_flag & DO_SAVE) {
        save_image(result, "result");
    }

    printf("Copy %8s    ", ip_type_name(type));

    if (chk_flag & DO_CHECKSUM) {
        print_md5(chksum);
        putchar('\n');
    }else{
        uint64_t time;
	uint64_t key = KEY_CONSTRUCT(type, 0, 0, 0);

        if (chk_flag & DO_TIMING) {
            ip_image *dummies[10];

            time_start();
            for (int j=0; j<10; j++) {
                dummies[j] = im_copy(im);
            }
            time = time_end() / 10;

            for (int j=0; j<10; j++)
                ip_free_image(dummies[j]);
        }else{
            time = -1LL;
        }

        print_time_status(key, cycles, time, ipc, failed);
    }


    ip_free_image(im);
    ip_free_image(result);

    num_tests++;
    if (failed)
	num_failures++;
}


void run_tests(int op_to_test, int imtype, ip_coord2d size, int chk_flag)
{
    EXPECT_NO_ERRORS;

    if (imtype) {
	copy_image_check(imtype, size, chk_flag);
    }else{
	copy_image_check(IM_BINARY, size, chk_flag);
	copy_image_check(IM_BYTE, size, chk_flag);
	copy_image_check(IM_UBYTE, size, chk_flag);
	copy_image_check(IM_SHORT, size, chk_flag);
	copy_image_check(IM_USHORT, size, chk_flag);
	copy_image_check(IM_LONG, size, chk_flag);
	copy_image_check(IM_ULONG, size, chk_flag);
	copy_image_check(IM_FLOAT, size, chk_flag);
	copy_image_check(IM_DOUBLE, size, chk_flag);
	copy_image_check(IM_COMPLEX, size, chk_flag);
	copy_image_check(IM_DCOMPLEX, size, chk_flag);
	copy_image_check(IM_PALETTE, size, chk_flag);
	copy_image_check(IM_RGB, size, chk_flag);
	copy_image_check(IM_RGB16, size, chk_flag);
	copy_image_check(IM_RGBA, size, chk_flag);
    }
}



int main(int argc, char *argv[])
{
    ip_coord2d size = {1025, 1023};
    int chk_flag;
    int op_to_test;
    int imtype;
    struct cmd_line_requests *clr;

    clr = process_cmdline(argc, argv, NUM_OPS, op_names);

    op_to_test = clr->op_to_test;
    chk_flag = clr->flags;
    if (clr->flags & DO_SIZE)
        size = clr->im_size;
    imtype = clr->im_type;

    ip_register_error_handler(error_handler);

    if (chk_flag & DO_PIXEL) {
	printf("CHECKING generic code:\n");
	ip_clear_global_flags(IP_FAST_CODE | IP_HWACCEL);
	run_tests(op_to_test, imtype, size, chk_flag);
    }

    if (chk_flag & DO_SIMD) {
	printf("CHECKING simd code:\n");
	ip_set_global_flags(IP_HWACCEL);
	run_tests(op_to_test, imtype, size, chk_flag);
    }

    print_final_stats(num_tests, num_failures, 0);

    return num_failures;
}
