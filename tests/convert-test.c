#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <limits.h>

#include <ip/ip.h>
#include <ip/tiff.h>

#include "timing.h"
#include "errorhdl.h"
#include "helpers.h"
#include "test-macros.h"

char *program_name = "convert-test";

/* Keep counts of failed and tested routines as globals */
int num_tests;
int num_failures;
int num_warnings;

/* Verbosity */
int verbose;

/* Operators we test */
const char *op_names[] = {
    "im_convert"
};

#define NUM_OPS 1


#define PIXEL_OPERATION_ASSIGN(dval,sval,d) (dval) = (sval)
#define PIXEL_OPERATION_ROUND(dval,sval,d) (dval) = round(sval)
#define PIXEL_OPERATION_ASSIGN_CLIPPED(dval,sval,dim)	\
    do {						\
	(dval) = (sval);				\
	CLIP_TO_PIXEL_TYPE_LIMITS(dval, dim);		\
    } while (0)

#define PIXEL_OPERATION_ROUND_CLIPPED(dval,sval,dim)	\
    do {						\
	(dval) = round(sval);				\
	CLIP_TO_PIXEL_TYPE_LIMITS(dval, dim);		\
    } while (0)

#define PIXEL_OPERATION_THRESHOLD(dval,sval,d) \
    do {				       \
	(dval) = (sval) ? 255 : 0;	       \
    } while(0)

#define PIXEL_OPERATION_CNVRTBIN(dval, sval, d, flag) \
    do {					      \
	if (flag == FLG_B1)			      \
	    (dval) = (sval) ? 1 : 0;		      \
	else					      \
	    (dval) = (sval) ? 255 : 0;		      \
    } while(0)

/* For RGB images */
#define PIXEL_OPERATION_UPSCALE(dval,sval,d) \
    do {				     \
	dval.r = (sval.r << 8) | sval.r;     \
	dval.g = (sval.g << 8) | sval.g;     \
	dval.b = (sval.b << 8) | sval.b;     \
	dval.a = (sval.a << 8) | sval.a;     \
    } while (0)

#define PIXEL_OPERATION_DOWNSCALE(dval,sval,d) \
    do {				       \
	dval.r = sval.r >> 8;		       \
	dval.g = sval.g >> 8;		       \
	dval.b = sval.b >> 8;		       \
	dval.a = sval.a >> 8;		       \
    } while (0)

#define PIXEL_OPERATION_PALETTE(dval,sval,s) \
    do {				     \
	dval.r = (s)->palette[sval].r;	     \
	dval.g = (s)->palette[sval].g;	     \
	dval.b = (s)->palette[sval].b;	     \
	dval.a = 255;			     \
    } while (0)

/* Integer image conversions reference functions */
im_Xii_int_DEFN(ASSIGN)
im_Xii_int_DEFN(ASSIGN_CLIPPED)

/* Binary/integer image conversions reference functions */
im_Xii_int_DEFN(THRESHOLD)
im_Xiif_int_DEFN(CNVRTBIN)

/* Real/real and real/integer image conversions reference functions */
im_Xii_real_DEFN(ASSIGN)
im_Xii_real_DEFN(ASSIGN_CLIPPED)
im_Xii_real_DEFN(ROUND)
im_Xii_real_DEFN(ROUND_CLIPPED)

/* binary/real image conversions reference functions */
im_Xii_real_DEFN(THRESHOLD)
im_Xiif_real_DEFN(CNVRTBIN)

/* complex/complex conversion */
im_Xii_dcomplex_DEFN(ASSIGN)

/* rgb/rgb16 conversions */
im_Xii_rgb_DEFN(ASSIGN)
im_Xii_rgb_DEFN(UPSCALE)
im_Xii_rgb_DEFN(DOWNSCALE)
im_Xii_palette_DEFN(PALETTE)

/* complex/floating point conversions */
static void im_Xii_complex_float(ip_image *dest, ip_image *src, int flag)
{
    for (int j=0; j<dest->size.y; j++) {
	for (int i=0; i<dest->size.x; i++) {
	    double sval;
	    ip_dcomplex dval;
	    GET_PIXEL_VALUE_AS_DOUBLE(sval, src, i, j);
	    if (flag == FLG_IMAG) {
		dval.r = 0.0;
		dval.i = sval;
	    }else{
		dval.r = sval;
		dval.i = 0.0;
	    }
	    SET_PIXEL_VALUE_FROM_DCOMPLEX(dest, dval, i, j);
	}
    }
}


/* The list of flagged error tests that we run */
struct errtest {
    int dtype, stype;
    int flags;
    int exp_err1, exp_err2;
};

struct errtest error_tests[] = {
    {IM_TYPEMAX,  IM_UBYTE,      0,            ERR_INVALID_IMAGE_TYPE, 0},
    {IM_UBYTE,    IM_TYPEMAX,    0,            ERR_INVALID_IMAGE_TYPE, ERR_INVALID_IMAGE},
    {IM_FLOAT,    IM_FLOAT,      0,            ERR_SAME_TYPE, 0},
    {IM_FLOAT,    IM_DOUBLE,     0xffff,       ERR_BAD_FLAG, ERR_BAD_FLAG2},
    {IM_BINARY,   IM_UBYTE,      FLG_B1,       ERR_BAD_FLAG, ERR_BAD_FLAG2},
    {IM_FLOAT,    IM_SHORT,      FLG_B255,     ERR_BAD_FLAG, ERR_BAD_FLAG2},
    {IM_DCOMPLEX, IM_DOUBLE,     FLG_SRED,     ERR_BAD_FLAG, ERR_BAD_FLAG2},
    {IM_COMPLEX,  IM_DOUBLE,     FLG_MAG,      ERR_BAD_FLAG, ERR_BAD_FLAG2},
    {IM_DOUBLE,   IM_DCOMPLEX,   FLG_RED,      ERR_BAD_FLAG, ERR_BAD_FLAG2},
    {IM_FLOAT,    IM_COMPLEX,    FLG_GREEN,    ERR_BAD_FLAG, ERR_BAD_FLAG2},
    {IM_SHORT,    IM_LONG,       FLG_REAL,     ERR_BAD_FLAG, ERR_BAD_FLAG2},
    {IM_SHORT,    IM_LONG,       FLG_ANGLE,    ERR_BAD_FLAG, ERR_BAD_FLAG2},
    {IM_FLOAT,    IM_LONG,       FLG_REAL,     ERR_BAD_FLAG, ERR_BAD_FLAG2},
    {IM_LONG,     IM_ULONG,      FLG_ROUND,    ERR_BAD_FLAG, ERR_BAD_FLAG2},
    {IM_BINARY,   IM_RGBA,       0,            ERR_NOT_IMPLEMENTED, 0},
    {IM_UBYTE,    IM_RGB,        FLG_IMAG,     ERR_BAD_FLAG, ERR_BAD_FLAG2},
    {IM_USHORT,   IM_RGB16,      FLG_B1,       ERR_BAD_FLAG, ERR_BAD_FLAG2},
    {-1, 0, 0, 0, 0}
};

struct flag_nums {
    int num;
    char *str;
};

#define FLG_NUM_STR(x) { x, #x }

struct flag_nums cnvrt_flags[] = {
    FLG_NUM_STR( FLG_REAL ),
    FLG_NUM_STR( FLG_IMAG ),
    FLG_NUM_STR( FLG_MAG ),
    FLG_NUM_STR( FLG_ANGLE ),
    FLG_NUM_STR( FLG_B1 ),
    FLG_NUM_STR( FLG_B255 ),
    FLG_NUM_STR( FLG_RED ),
    FLG_NUM_STR( FLG_GREEN ),
    FLG_NUM_STR( FLG_BLUE ),
    FLG_NUM_STR( FLG_MAGYUV ),
    {0, NULL}
};


static void print_verbose_flags(int flags, int width)
{
    int first = 1;
    static char msg[256];
    int cnvrt_part = flags & FLGPRT_CNVRT;
    int bits = flags & ~FLGPRT_CNVRT;

    msg[0] = 0;
    if (cnvrt_part) {
	for (int j=0; cnvrt_flags[j].num; j++) {
	    if (cnvrt_part == cnvrt_flags[j].num) {
		strcat(msg, cnvrt_flags[j].str);
		first = 0;
		break;
	    }
	}
	if (first) /* if not found */
	    strcat(msg, "!BAD!");
	first = 0;
    }
    if (bits & FLG_CLIP) {
	if (! first)
	    strcat(msg, "|");
	strcat(msg, "FLG_CLIP");
	first = 0;
    }
    if (bits & FLG_ROUND) {
	if (! first)
	    strcat(msg, "|");
	strcat(msg, "FLG_ROUND");
	first = 0;
    }
    if (bits & ~(FLG_ROUND|FLG_CLIP)) {
	char buf[20];
	if (! first)
	    strcat(msg, "|");
	snprintf(buf, 20, "0x%08x", bits);
	strcat(msg, buf);
	first = 0;
    }
    if (flags == 0)
	strcat(msg, "NO_FLAG");

    fputs(msg, stdout);
    for (int j = width - strlen(msg); j > 0; j--)
	putchar(' ');
}




void errors_flagged_check(void)
{
    ip_image *im;
    ip_image *res;
    int mark = 0;
    int failed = 0;

    while (error_tests[mark].dtype >= 0) {
	int dtype = error_tests[mark].dtype;
	int stype = error_tests[mark].stype;
	int flags = error_tests[mark].flags;

	EXPECT_ERRORS(error_tests[mark].exp_err1, error_tests[mark].exp_err2);
	MARK(mark+1);

	if (stype == IM_TYPEMAX) {
	    /* Construct an invalid image object */
	    im = ip_alloc_image(IM_UBYTE, (ip_coord2d){126, 127});
	    im->type = IM_TYPEMAX;
	}else{
	    im = ip_alloc_image(stype, (ip_coord2d){126, 127});
	}

	if (im) {
	    res = im_convert(im, dtype, flags);
	}else{
	    res = NULL;
	}

	if (dtype == IM_TYPEMAX)
	    printf("BAD      ");
	else
	    printf("%-8s ", ip_type_name(dtype));
	printf("<- ");
	if (stype == IM_TYPEMAX)
	    printf("BAD      ");
	else
	    printf("%-8s ", ip_type_name(stype));

	if (im) {
	    print_verbose_flags(flags, 24);
	    printf(")  ");
	}else{
	    printf("                       ");
	}

	CHECK_SEEN_ERROR_WITH_MSG;

	if (im) {
	    /* Repair any damage to the image object that we made! */
	    if (im->type == IM_TYPEMAX) im->type = IM_UBYTE;
	    ip_free_image(im);
	}
	if (res) {
	    failed |= 1;
	    ip_free_image(res);
	}

	mark++;
    }

    num_tests++;
    if (failed)
	num_failures++;

    printf("Error checks                                                                           %s\n",
	   failed ? "[FAIL]": "[OK]");
}




static ip_image *baseline_convert(ip_image *s, int dtype, int flags)
{
    ip_image *r;

    r = ip_alloc_image(dtype, s->size);
    if (type_integer(dtype) && image_type_integer(s)) {
	if (flags & FLG_CLIP)
	    im_Xii_int_ASSIGN_CLIPPED(r, s);
	else
	    im_Xii_int_ASSIGN(r, s);
    }else if (type_real(dtype) && image_type_real(s)) {
	if (flags & FLG_CLIP)
	    im_Xii_real_ASSIGN_CLIPPED(r, s);
	else
	    im_Xii_real_ASSIGN(r, s);
    }else if (type_real(dtype) && image_type_integer(s)) {
	im_Xii_real_ASSIGN(r, s);
    }else if (type_integer(dtype) && image_type_real(s)) {
	if (flags == FLG_CLIP) {
	    im_Xii_real_ASSIGN_CLIPPED(r, s);
	}else if (flags == FLG_ROUND) {
	    im_Xii_real_ROUND(r, s);
	}else if (flags == (FLG_CLIP | FLG_ROUND)) {
	    im_Xii_real_ROUND_CLIPPED(r, s);
	}else{
	    im_Xii_real_ASSIGN(r, s);
	}
    }else if (dtype == IM_BINARY) {
	if (image_type_integer(s))
	    im_Xii_int_THRESHOLD(r, s);
	else if (image_type_real(s))
	    im_Xii_real_THRESHOLD(r, s);
    }else if (s->type == IM_BINARY) {
	if (type_integer(dtype))
	    im_Xiif_int_CNVRTBIN(r, s, flags);
	else if (type_real(dtype))
	    im_Xiif_real_CNVRTBIN(r, s, flags);
    }else if (type_complex(dtype) && image_type_complex(s)) {
	im_Xii_dcomplex_ASSIGN(r, s);
    }else if (type_complex(dtype) && image_type_real(s)) {
	im_Xii_complex_float(r, s, flags);
    }else if (type_rgbim(dtype) && image_type_rgbim(s)) {
	if (dtype == IM_RGB16 && s->type == IM_RGB)
	    im_Xii_rgb_UPSCALE(r, s);
	else if (dtype == IM_RGB && s->type == IM_RGB16)
	    im_Xii_rgb_DOWNSCALE(r, s);
	else
	    im_Xii_rgb_ASSIGN(r, s);
    }else if (type_rgbim(dtype) && s->type == IM_PALETTE) {
	im_Xii_palette_PALETTE(r, s);
    }

    return r;
}


#define NEXT_PT_IN_IMAGE(im,p)				\
    do {						\
	p.x++;						\
	if (p.x>=im->size.x) {				\
	    p.x=0; p.y++;				\
	    if (p.y >= im->size.y) p.y--;		\
	}						\
    } while(0)


/*
 * Insert some useful values into the image that tests boundary cases, etc.
 */
static void embed_useful_values(ip_image *im, int dtype)
{
    const double half = 0.5;
    double halfu = nextafter(half, 1.0);
    double halfd = nextafter(half, 0.0);
    double minv = ip_type_min(dtype);
    double maxv = ip_type_max(dtype);
    double midv = minv + (maxv - minv)/2.0;
    ip_coord2d pt;

    /* Protect against triggering an error in im_drawpoint() */
    if ((minv-1.0) < ip_type_min(im->type)) minv = ip_type_min(im->type)+1;
    if ((maxv+1.0) > ip_type_max(im->type)) maxv = ip_type_max(im->type)-1;
    if ((midv) <= ip_type_min(im->type)) midv = ip_type_min(im->type)+10;
    if ((midv) >= ip_type_max(im->type)) midv = ip_type_max(im->type)-10;

    pt.x = pt.y = 0;
    im_drawpoint(im, minv-1, pt); /* 0 */
    NEXT_PT_IN_IMAGE(im, pt);
    im_drawpoint(im, minv-0.5, pt); /* 1 */
    NEXT_PT_IN_IMAGE(im, pt);
    im_drawpoint(im, minv, pt);	/* 2 */
    NEXT_PT_IN_IMAGE(im, pt);
    im_drawpoint(im, minv+0.5, pt); /* 3 */
    NEXT_PT_IN_IMAGE(im, pt);
    im_drawpoint(im, minv+1, pt); /* 4 */

    NEXT_PT_IN_IMAGE(im, pt);
    im_drawpoint(im, midv, pt);	/* 5 */
    NEXT_PT_IN_IMAGE(im, pt);
    im_drawpoint(im, nextafter(midv, maxv), pt); /* 6 */
    NEXT_PT_IN_IMAGE(im, pt);
    im_drawpoint(im, nextafter(midv, minv), pt); /* 7 */

    NEXT_PT_IN_IMAGE(im, pt);
    im_drawpoint(im, maxv-1, pt); /* 8 */
    NEXT_PT_IN_IMAGE(im, pt);
    im_drawpoint(im, maxv-0.5, pt); /* 9 */
    NEXT_PT_IN_IMAGE(im, pt);
    im_drawpoint(im, maxv, pt);	/* 10 */
    NEXT_PT_IN_IMAGE(im, pt);
    im_drawpoint(im, maxv+0.5, pt); /* 11 */
    NEXT_PT_IN_IMAGE(im, pt);
    im_drawpoint(im, maxv+1, pt); /* 12 */

    NEXT_PT_IN_IMAGE(im, pt);
    im_drawpoint(im, halfd, pt); /* 13 */
    NEXT_PT_IN_IMAGE(im, pt);
    im_drawpoint(im, half, pt);	/* 14 */
    NEXT_PT_IN_IMAGE(im, pt);
    im_drawpoint(im, halfu, pt); /* 15 */

    if (type_real(im->type) && type_integer(dtype)) {
	/*
	 * Insert some > 2^32 values to test extreme floating point values
	 * to integer.  Ideally they should be converted to (u)int64_t type
	 * then truncated to the final integer type, however on some arches,
	 * and likely for SIMD, the conversion may only be good if values
	 * are within (u)int32_t range only.
	 *
	 * Also note that in single precision values such as INT32_MAX are
	 * not exactly representable, and are rounded to the nearest
	 * available value.
	 */
	NEXT_PT_IN_IMAGE(im, pt);
	im_drawpoint(im, INT32_MAX, pt); /* 16 */
	NEXT_PT_IN_IMAGE(im, pt);
	im_drawpoint(im, (double)INT32_MAX+1.0, pt); /* 17 */
	NEXT_PT_IN_IMAGE(im, pt);
	im_drawpoint(im, (double)UINT32_MAX, pt); /* 18 */
	NEXT_PT_IN_IMAGE(im, pt);
	im_drawpoint(im, (double)UINT32_MAX+1.0, pt); /* 19 */
	NEXT_PT_IN_IMAGE(im, pt);
	im_drawpoint(im, (double)UINT32_MAX + 125.0, pt); /* 20 */
	NEXT_PT_IN_IMAGE(im, pt);
	im_drawpoint(im, INT32_MIN, pt); /* 21 */
	NEXT_PT_IN_IMAGE(im, pt);
	im_drawpoint(im, (double)INT32_MIN-1.0, pt); /* 22 */
	NEXT_PT_IN_IMAGE(im, pt);
	im_drawpoint(im, -(double)UINT32_MAX, pt); /* 23 */
	NEXT_PT_IN_IMAGE(im, pt);
	im_drawpoint(im, -(double)UINT32_MAX-1.0, pt); /* 24 */
	NEXT_PT_IN_IMAGE(im, pt);
	im_drawpoint(im, -((double)UINT32_MAX + 125.0), pt); /* 25 */
    }
}


static void save_image(ip_image *im, int dtype, int stype, int flags, const char *info)
{
    char fstr[180];

    snprintf(fstr, 180, "convert-%s-%s-0x%04x-%s.tiff",
	     ip_type_name(dtype), ip_type_name(stype), flags, info);
    write_tiff_image(fstr, im);
}


/*
 * Special compare function that takes into account if the original source
 * pixel value was out of range of (u)int32_t when converting to integer
 * type.  On some arches preserving a conversion from float/double to
 * (u)int64_t then truncation to integer is not efficiently possible,
 * particularly for some SIMD implementations.  We therefore distinguish
 * failure to convert in range pixels (an outright fail) and failure to
 * convert out of range (i.e., extreme) pixel values (a warning).
 */


int compare_images_integer_special(ip_image *im1, ip_image *im2,
				   char *msg, char *im1_name, char *im2_name,
				   ip_image *src)
{
    long v1, v2;
    int firsttime = 1;
    long maxdiff = 0;
    int numdiffs = 0;
    int warnings = 0;
    double minval, maxval;

    /* Record 32-bit maximum/minimum depending on destination type */
    if (im1->type == IM_ULONG) {
	minval = 0.0;
	maxval = (double)UINT32_MAX;
    }else{
	minval = (double)INT32_MIN;
	maxval = (double)INT32_MAX;
    }

    /* Make sure we conduct the tests calling compare_images() would do */
    if (!image_valid(im1)) {
	printf("%s: Image object %s corrupted!!!\n", msg, im1_name);
	return -1;
    }

    if (!image_valid(im2)) {
	printf("%s: Image object %s corrupted!!!\n", msg, im2_name);
	return -1;
    }

    if ((im1->size.x != im2->size.x) || (im1->size.y != im2->size.y)) {
	printf("%s: Images %s and %s not compatible.\n", msg, im1_name, im2_name);
	return -1;
    }

    /* For each pixel do */
    v1 = v2 = 0;
    for (int j=0; j<im1->size.y; j++) {
	for (int i=0; i<im1->size.x; i++) {
	    int cmp = 0;

	    switch (im1->type) {
	    case IM_BINARY:
	    case IM_PALETTE:
	    case IM_UBYTE:
	    case IM_BYTE:
		cmp = IM_PIXEL(im1, i, j, b) == IM_PIXEL(im2, i, j, b);
		v1 = IM_PIXEL(im1, i, j, b);
		v2 = IM_PIXEL(im2, i, j, b);
		break;
	    case IM_USHORT:
	    case IM_SHORT:
		cmp = IM_PIXEL(im1, i, j, s) == IM_PIXEL(im2, i, j, s);
		v1 = IM_PIXEL(im1, i, j, s);
		v2 = IM_PIXEL(im2, i, j, s);
		break;
	    case IM_ULONG:
	    case IM_LONG:
		cmp = IM_PIXEL(im1, i, j, l) == IM_PIXEL(im2, i, j, l);
		v1 = IM_PIXEL(im1, i, j, l);
		v2 = IM_PIXEL(im2, i, j, l);
		break;
	    default:
		break;
	    }

	    if (! cmp) {
		double srcval = 0.0;
		long diff = v1 > v2 ? (v1-v2) : (v2-v1);

		/* Failure to compare; check the source image pixel value... */
		switch (src->type) {
		case IM_FLOAT:
		    srcval = (double)IM_PIXEL(src, i, j, f);
		    break;
		case IM_DOUBLE:
		    srcval = IM_PIXEL(src, i, j, d);
		    break;
		default:
		    break;
		}
		/* and if it is out of range of 32-bit then just warn. */
		if (srcval > maxval || srcval < minval || !isfinite(srcval)) {
		    warnings++;
		    if (verbose) {
			printf("WARN: srcval %g at (%d,%d) converted to %ld (should be %ld)\n",srcval,i,j,v2,v1);
		    }
		    if (warnings == 1) {
			push_footnote("Values out of range of target type are not truncated to target type.");
		    }
		}else{
		    if (firsttime) {
			printf("%s: Images %s [%ld] and %s [%ld] differ at (%d,%d)\n",
			       msg, im1_name, v1, im2_name, v2, i, j);
			firsttime = 0;
		    }
		    numdiffs++;
		    if (diff > maxdiff)
			maxdiff = diff;
		}
	    }
	}
    }

    if (numdiffs) {
	printf("...num differing pixels = %d; maximum difference = %ld.\n", numdiffs, maxdiff);
    }

    if (numdiffs) {
	if (warnings)
	    return 3;
	else
	    return 1;
    }

    return warnings ? 2 : 0;
}



static int im_convert_check(ip_coord2d size, int dtype, int stype, int flags, int chk_flag)
{
    ip_image *r, *d, *s, *s_copy;
    int failed = 0;
    int create_flag = chk_flag & DO_RANDOM;
    unsigned char *chksum = NULL;

    if (type_real(stype) && type_integer(dtype))
	create_flag |= DO_VALLIMIT | (dtype<<VALLIMIT_TYPE_SHIFT);

    s = create_test_image(stype, size, create_flag);
    if (type_scalar(stype) || type_complex(stype))
	embed_useful_values(s, dtype);
    s_copy = im_copy(s);

    if (chk_flag & DO_SAVE) {
	save_image(s, dtype, stype, flags, "original");
    }

    ignore_error = ERR_NOT_IMPLEMENTED;
    d = im_convert(s, dtype,  flags);
    ignore_error = -1;

    if (d) {
	/* is implemented */

	/* Get md5 checksum of result if requested */
	if (chk_flag & DO_CHECKSUM) {
	    chksum = im_md5(d);
	}

	r = baseline_convert(s, dtype, flags);

	if (type_integer(dtype) && type_real(stype) && !(flags & FLG_CLIP))
	    failed = compare_images_integer_special(r, d, "Image convert check", "r", "d", s);
	else
	    failed = compare_images(r, d, "Image convert check", "r", "d");

	failed |= compare_images(s_copy, s, "Original image intact check", "s_copy", "s");

	if (chk_flag & DO_SAVE) {
	    save_image(d, dtype, stype, flags, "result");
	    save_image(r, dtype, stype, flags, "reference");
	}
    }

    printf("%8s <- %8s with flags 0x%04x ", ip_type_name(dtype), ip_type_name(stype), flags);

    if (d) {
	if (chk_flag & DO_CHECKSUM) {
	    print_md5(chksum);
	    putchar('\n');
	}else{
	    uint64_t time, key;

	    if (chk_flag & DO_TIMING) {
		ip_image *dummies[10];

		time_start();
		for (int j=0; j<10; j++) {
		    dummies[j] = im_convert(s, dtype, flags);
		}
            time = time_end() / 10;

	    for (int j=0; j<10; j++)
		ip_free_image(dummies[j]);
	    }else{
		time = -1LL;
	    }

	    key = KEY_CONSTRUCT(dtype, stype, 0, flags);
	    print_time_status(key, cycles, time, ipc, failed);
	}

	ip_free_image(d);
	ip_free_image(r);
    }else{
	failed = 2;
	printf("[     ...Not Implemented....     ] [WARN]\n");
    }

    ip_free_image(s_copy);
    ip_free_image(s);

    num_tests++;
    if (failed & 1)
	num_failures++;
    else if (failed & 2)
	num_warnings++;

    return failed;
}



void run_convert_tests(ip_coord2d size, int imtype, int chk_flag)
{
    int inttypes[6] = {IM_BYTE, IM_UBYTE, IM_SHORT, IM_USHORT, IM_LONG, IM_ULONG};
    int floattypes[2] = {IM_FLOAT, IM_DOUBLE};

    /* Scalar to Binary conversion */
    printf("..Scalar to Binary conversions\n");
    for (int j=0; j<6; j++) {
	if (imtype <= 0 || inttypes[j] == imtype)
	    im_convert_check(size, IM_BINARY, inttypes[j], NO_FLAG, chk_flag);
    }
    if (imtype <= 0 || imtype == IM_FLOAT)
	im_convert_check(size, IM_BINARY, IM_FLOAT, NO_FLAG, chk_flag);
    if (imtype <= 0 || imtype == IM_DOUBLE)
	im_convert_check(size, IM_BINARY, IM_DOUBLE, NO_FLAG, chk_flag);

    /* Binary to other scalars */

    printf("..Binary to Scalar conversions\n");
    if (imtype <= 0 || imtype == IM_BINARY) {
	for (int j=0; j<6; j++) {
	    im_convert_check(size, inttypes[j], IM_BINARY, NO_FLAG, chk_flag);
	    im_convert_check(size, inttypes[j], IM_BINARY, FLG_B1, chk_flag);
	}
	im_convert_check(size, IM_FLOAT, IM_BINARY, NO_FLAG, chk_flag);
	im_convert_check(size, IM_FLOAT, IM_BINARY, FLG_B1, chk_flag);
	im_convert_check(size, IM_DOUBLE, IM_BINARY, NO_FLAG, chk_flag);
	im_convert_check(size, IM_DOUBLE, IM_BINARY, FLG_B1, chk_flag);
    }

    /* Check integer to integer conversions */
    printf("..Integer to Integer conversion\n");
    for (int j=0; j<6; j++) {
	if (imtype <= 0 || inttypes[j] == imtype) {
	    for (int k=0; k<6; k++) {
		if (k != j) {
		    im_convert_check(size, inttypes[k], inttypes[j], NO_FLAG, chk_flag);
		    im_convert_check(size, inttypes[k], inttypes[j], FLG_CLIP, chk_flag);
		}
	    }
	}
    }

    /* Check float to float conversions */
    printf("..Float to float conversion\n");
    for (int j=0; j<2; j++) {
	if (imtype <= 0 || floattypes[j] == imtype) {
	    for (int k=0; k<2; k++) {
		if (k != j) {
		    im_convert_check(size, floattypes[k], floattypes[j], NO_FLAG, chk_flag);
		    im_convert_check(size, floattypes[k], floattypes[j], FLG_CLIP, chk_flag);
		}
	    }
	}
    }

    /* Check integer to float conversions */
    printf("..Integer to float conversions\n");
    for (int j=0; j<6; j++) {
	if (imtype <= 0 || inttypes[j] == imtype) {
	    for (int k=0; k<2; k++) {
		im_convert_check(size, floattypes[k], inttypes[j], NO_FLAG, chk_flag);
	    }
	}
    }

    /* Check float to integer conversions */
    printf("..Float to integer conversions\n");
    for (int j=0; j<2; j++) {
	if (imtype <= 0 || floattypes[j] == imtype) {
	    for (int k=0; k<6; k++) {
		im_convert_check(size, inttypes[k], floattypes[j], NO_FLAG, chk_flag);
		im_convert_check(size, inttypes[k], floattypes[j], FLG_CLIP, chk_flag);
		im_convert_check(size, inttypes[k], floattypes[j], FLG_ROUND, chk_flag);
		im_convert_check(size, inttypes[k], floattypes[j], FLG_ROUND | FLG_CLIP, chk_flag);
	    }
	}
    }

    /* Check complex/complex conversion */
    if (imtype <= 0 || type_complex(imtype)) {
	printf("..Complex to complex conversions\n");
	im_convert_check(size, IM_COMPLEX, IM_FLOAT, NO_FLAG, chk_flag);
	im_convert_check(size, IM_COMPLEX, IM_FLOAT, FLG_REAL, chk_flag);
	im_convert_check(size, IM_COMPLEX, IM_FLOAT, FLG_IMAG, chk_flag);
	im_convert_check(size, IM_COMPLEX, IM_DCOMPLEX, NO_FLAG, chk_flag);
	im_convert_check(size, IM_DCOMPLEX, IM_DOUBLE, NO_FLAG, chk_flag);
	im_convert_check(size, IM_DCOMPLEX, IM_DOUBLE, FLG_REAL, chk_flag);
	im_convert_check(size, IM_DCOMPLEX, IM_DOUBLE, FLG_IMAG, chk_flag);
	im_convert_check(size, IM_DCOMPLEX, IM_COMPLEX, NO_FLAG, chk_flag);
    }

    /* Colour image conversions */
    if (imtype <=0 || type_clrim(imtype)) {
	printf("..Colour to colour image conversions\n");
	im_convert_check(size, IM_RGB, IM_PALETTE, NO_FLAG, chk_flag);
	im_convert_check(size, IM_RGB, IM_RGB16, NO_FLAG, chk_flag);
	im_convert_check(size, IM_RGB, IM_RGBA, NO_FLAG, chk_flag);
	im_convert_check(size, IM_RGBA, IM_RGB, NO_FLAG, chk_flag);
	im_convert_check(size, IM_RGB16, IM_RGB, NO_FLAG, chk_flag);
    }
}




int main(int argc, char *argv[])
{
    ip_coord2d size = {923, 1025};
    int chk_flag;
    int imtype;
    struct cmd_line_requests *clr;

    clr = process_cmdline(argc, argv, NUM_OPS, op_names);

    chk_flag = clr->flags;
    if (clr->flags & DO_SIZE)
	size = clr->im_size;
    imtype = clr->im_type;
    if (chk_flag & DO_VERBOSE)
	verbose = 1;

    ip_register_error_handler(error_handler);


    if (chk_flag & DO_ERROR) {
	printf("CHECKING errors are flagged:\n");
	errors_flagged_check();
    }

    if (chk_flag & DO_PIXEL) {
        printf("CHECKING basic pixelwise code:\n");

        EXPECT_NO_ERRORS;
        ip_clear_global_flags(IP_FAST_CODE | IP_HWACCEL);

 	run_convert_tests(size, imtype, chk_flag);
    }

    if (chk_flag & DO_SIMD) {
        printf("CHECKING hardware (SIMD) acceleration (if available):\n");

        EXPECT_NO_ERRORS;
	ip_set_global_flags(IP_HWACCEL);

 	run_convert_tests(size, imtype, chk_flag);
    }

    print_final_stats(num_tests, num_failures, num_warnings);

    return num_failures;
}
