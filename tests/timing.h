#ifndef IP_TESTS_TIMING_H
#define IP_TESTS_TIMING_H

#include <stdint.h>

extern uint64_t instructions;
extern uint64_t cycles;
extern uint64_t cache_misses;
extern int do_cache_misses;
extern double ipc;

extern void time_start(void);
extern uint64_t time_end(void);

#endif
