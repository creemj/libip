/*
 * timing.h
 * By Michael J. Cree
 *
 * Define an accurate timing function that uses the performance events
 * subsytem of the Linux kernel for timing, or failing that, falls back to
 * using the less accurate getrusage facility.
 *
 * Copyright (C) 2013-2016 Michael J. Cree
 */

#ifdef HAVE_PERFORMANCE_EVENTS
/* To enable access to syscall */
#define _GNU_SOURCE
#endif

#include <stdint.h>

#include "timing.h"

uint64_t instructions;
uint64_t cycles;
uint64_t cache_misses;
int do_cache_misses = 0;
double ipc;

#ifndef HAVE_PERFORMANCE_EVENTS

/*
 * Use getrusage() if don't have Linux performance events.
 */

#include <sys/time.h>
#include <sys/resource.h>

struct timeval last_utime;
struct timeval last_stime;

void time_start(void)
{
    struct rusage ru;

    getrusage(0, &ru);
    last_utime = ru.ru_utime;
    last_stime = ru.ru_stime;
}


uint64_t time_end(void)
{
    struct rusage ru;

    getrusage(0, &ru);
    return (uint64_t)(ru.ru_utime.tv_usec-last_utime.tv_usec)
	+ (uint64_t)1000000UL*(ru.ru_utime.tv_sec-last_utime.tv_sec)
	+ (uint64_t)(ru.ru_stime.tv_usec-last_stime.tv_usec)
	+ (uint64_t)1000000UL*(ru.ru_stime.tv_sec-last_stime.tv_sec) ;
}

#else

/*
 * Do have Linux performance events.
 */

#define _GNU_SOURCE
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include <sys/unistd.h>
#include <sys/syscall.h>
#include <sys/prctl.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <linux/perf_event.h>

#ifndef __NR_perf_event_open
#error __NR_perf_event_open not defined.
#endif

#define NUM_EVENTS 4

struct perf_event_attr *attr;
int fd[NUM_EVENTS];


static inline int
sys_perf_event_open(struct perf_event_attr *attr,
                      pid_t pid, int cpu, int group_fd,
                      unsigned long flags)
{
        attr->size = sizeof(*attr);
        return syscall(__NR_perf_event_open, attr, pid, cpu,
                       group_fd, flags);
}

static inline uint64_t read_perf_event_count(int fd)
{
    __u64 count[3];

    /* Return 0 if event failed to be opened; */
    if (fd < 0)
	return 0UL;

    /* otherwise read the count and scale up if needed. */
    if (read(fd, count, 3*sizeof(__u64)) != 3*sizeof(__u64))
	perror("read of event failed");
    if (count[2] == 0) {
	return 0;
    }else if (count[2] < count[1]) {
	return (uint64_t)((double)count[0]*(double)count[1]/count[2] + 0.5);
    }else{
	return (uint64_t)count[0];
    }
}


void time_start(void)
{
    if (! attr)
	attr = calloc(NUM_EVENTS, sizeof(*attr));

    attr[0].type = PERF_TYPE_HARDWARE;
    attr[0].config = PERF_COUNT_HW_CPU_CYCLES;
    attr[0].read_format = PERF_FORMAT_TOTAL_TIME_ENABLED |
                                    PERF_FORMAT_TOTAL_TIME_RUNNING;
    attr[0].disabled = 1;

    attr[1].type = PERF_TYPE_HARDWARE;
    attr[1].config = PERF_COUNT_HW_INSTRUCTIONS;
    attr[1].read_format = PERF_FORMAT_TOTAL_TIME_ENABLED |
                                    PERF_FORMAT_TOTAL_TIME_RUNNING;
    attr[1].disabled = 1;

    attr[2].type = PERF_TYPE_SOFTWARE;
    attr[2].config = PERF_COUNT_SW_TASK_CLOCK;
    attr[2].read_format = PERF_FORMAT_TOTAL_TIME_ENABLED |
                                    PERF_FORMAT_TOTAL_TIME_RUNNING;
    attr[2].disabled = 1;

    if (do_cache_misses) {
	attr[3].type = PERF_TYPE_HARDWARE;
	attr[3].config = PERF_COUNT_HW_CACHE_MISSES;
	attr[3].read_format = PERF_FORMAT_TOTAL_TIME_ENABLED |
				    PERF_FORMAT_TOTAL_TIME_RUNNING;
	attr[3].disabled = 1;
    }else{
	attr[3].type = -1;
    }

    for (int j=0; j<NUM_EVENTS; j++) {
	if (attr[j].type != -1) {
	    fd[j] = sys_perf_event_open(attr+j, 0, -1, -1, 0);
	    /* Allow failure to open hardware events but not software event */
	    if (fd[j] < 0 && attr[j].type != PERF_TYPE_HARDWARE) {
		perror("perf_open event");
		exit(1);
	    }
	}else{
	    fd[j] = -1;
	}
    }

    prctl(PR_TASK_PERF_EVENTS_ENABLE);
}


uint64_t time_end(void)
{
    uint64_t timing;

    prctl(PR_TASK_PERF_EVENTS_DISABLE);

    cycles = read_perf_event_count(fd[0]);
    instructions = read_perf_event_count(fd[1]);
    cache_misses = read_perf_event_count(fd[3]);
    if (cycles > 0)
	ipc = (double)instructions / cycles;
    else
	ipc = 0.0;

    timing = read_perf_event_count(fd[2]);

    for (int j=0; j<NUM_EVENTS; j++) {
	if (fd[j] >= 0)
	    close(fd[j]);
    }

    cycles = (cycles + 500) / 1000;
    instructions = (instructions + 500) / 1000;

    return (timing + 500 ) /1000;
}

#endif
