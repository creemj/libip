#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>

#include <ip/types.h>
#include <ip/image.h>
#include <ip/error.h>
#include <ip/proto.h>
#include <ip/flags.h>

#include "test-macros.h"
#include "timing.h"
#include "helpers.h"
#include "errorhdl.h"


char *program_name = "imxor-test";

#define PIXEL_OPERATION_XOR(dval, sval, d)  dval ^= sval

im_ii_int_DEFN(XOR)


long im_xor_check(int type, ip_coord2d size, char *msg)
{
    ip_image *r, *d, *s;
    int failed = 0;
    double scale;
    double offset;
    long time;

    r = ip_alloc_image(type, size);

    scale = 1.2*(ip_image_type_info[type].maxvalue 
		       - ip_image_type_info[type].minvalue) / size.x;
    offset = ip_image_type_info[type].minvalue;
    im_create(r, (ip_coord2d){0,0}, scale, offset, 1.0, TI_HWEDGE);
    d = im_copy(r);
    failed |= compare_images(r, d, "Initial compare of reference", "r", "d");

    set_image_row_padding(r, 0x53);
    failed |= check_image_row_padding(r, 0x53, "Set padding check padding");
    failed |= compare_images(r, d, "Set padding check image data", "r", "d");

    s = ip_alloc_image(type, size);
    im_create(s, (ip_coord2d){0,0}, scale, 0.0, 1.0, TI_VWEDGE);
    
    im_ii_int_XOR(r, s);

    time_start();
    im_xor(d, s);
    time = time_end();

    failed |= compare_images(r, d, "Image add check", "r", "d");
    failed |= check_image_row_padding(r, 0x53, "Image add padding check");

    printf("%s: [%ld us] %s\n", msg, time, failed ? "FAILED" : "OK");

    ip_free_image(s);
    ip_free_image(d);
    ip_free_image(r);

    return time;
}



int main(int argc, char *argv[])
{
    ip_coord2d size = {1021, 1026};
    ip_image *i1, *i2;
    int failed = 0;

    ip_register_error_handler(error_handler);

    EXPECT_ERRORS(ERR_NOT_SAME_SIZE, ERR_NOT_SAME_TYPE);
    i1 = ip_alloc_image(IM_LONG, size);
    i2 = ip_alloc_image(IM_ULONG, size);
    MARK(1);
    im_xor(i1, i2);
    CHECK_SEEN_ERROR;

    ip_free_image(i2);
    i2 = ip_alloc_image(IM_LONG, (ip_coord2d){1000, 1000});
    MARK(2);
    im_xor(i1, i2);
    CHECK_SEEN_ERROR;

    ip_free_image(i1);
    ip_free_image(i2);

    EXPECT_ERRORS(ERR_BAD_TYPE, 0);
    i1 = ip_alloc_image(IM_RGB, size);
    i2 = ip_alloc_image(IM_RGB, size);
    MARK(3);
    im_xor(i1, i2);
    CHECK_SEEN_ERROR;

    ip_free_image(i1);
    ip_free_image(i2);

    printf("im_xor error reporting: %s\n", failed ? "FAIL" : "OK");

    EXPECT_NO_ERRORS;
    im_xor_check(IM_UBYTE, size, "im_xor UBYTE");
    im_xor_check(IM_BYTE, size, "im_xor BYTE");
    im_xor_check(IM_USHORT, size, "im_xor USHORT" );
    im_xor_check(IM_SHORT, size, "im_xor SHORT" );
    im_xor_check(IM_ULONG, size, "im_xor ULONG" );
    im_xor_check(IM_LONG, size, "im_xor LONG" );

    return 0;
}
