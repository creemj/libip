/* To get getopt() and srandom() */
#define _XOPEN_SOURCE 600

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <unistd.h>

#include <ip/ip.h>

#include "test-macros.h"
#include "timing.h"
#include "helpers.h"
#include "errorhdl.h"

char *program_name = "threshold-test";

/* Keep counts of failed and tested routines as globals */
int num_tests;
int num_failures;

const char *op_names[1] = { "im_threshold"};

/* threshold operator */
#define PIXEL_OPERATION_THRESHOLD(dval, sval, d, p1, p2 ,p3 ,p4)	\
    dval = (sval >= p1 && sval <= p2) ? (uint8_t)p3 : (uint8_t)p4

im_bicccc_int_DEFN(THRESHOLD);
im_bicccc_real_DEFN(THRESHOLD);

void im_threshold_check(int type, ip_coord2d size, double low, double high, int flag)
{
    ip_image *r, *d, *s, *s_copy;
    int failed = 0;
    long time;
    ip_image *dummies[10];
    char str[64];
    unsigned char *chksum;

    s = create_test_image(type, size, flag & DO_RANDOM);
    s_copy = im_copy(s);

    r = ip_alloc_image(IM_BINARY, size);
    if (type_integer(type)) {
	if (low <= high)
	    im_bicccc_int_THRESHOLD(r, s, low, high, 255, 0);
	else
	    im_bicccc_int_THRESHOLD(r, s, high, low, 0, 255);
    }else{
	if (low <= high)
	    im_bicccc_real_THRESHOLD(r, s, low, high, 255, 0);
	else
	    im_bicccc_real_THRESHOLD(r, s, high, low, 0, 255);
    }

    d = im_threshold(s, low, high);

    /* Get md5 checksum of result if requested */
    if (flag & DO_CHECKSUM) {
        chksum = im_md5(d);
    }

    failed = compare_images(r, d, "Image threshold check", "r", "d");
    failed |= compare_images(s_copy, s, "Original image intact check", "s_copy", "s");

    printf("imthreshold %-7s ", ip_type_name(type));
    sprintf(str, "(%g - %g)", low, high);
    printf("%-22s ", str);

    if (flag & DO_CHECKSUM) {
	print_md5(chksum);
	putchar('\n');
    }else{
	uint64_t key = KEY_CONSTRUCT(type, 0, 0, (uint32_t)low);
	if (flag & DO_TIMING) {
	    time_start();
	    for (int j=0; j<10; j++) {
		dummies[j] = im_threshold(s, low, high);
	    }
	    time = time_end() / 10;

	    for (int j=0; j<10; j++) {
		ip_free_image(dummies[j]);
	    }
        }else{
	    time = -1LL;
	}
	print_time_status(key, cycles, time, ipc, failed);
    }

    /* Increment counts (global variables) */
    num_tests++;
    if (failed) num_failures++;

    ip_free_image(s);
    ip_free_image(s_copy);
    ip_free_image(d);
    ip_free_image(r);
}


void do_threshold_checks(ip_coord2d size, int chk_flag)
{
    im_threshold_check(IM_UBYTE, size, 100, 200, chk_flag);
    im_threshold_check(IM_UBYTE, size, 199, 20, chk_flag);
    im_threshold_check(IM_BYTE, size, -1, 120, chk_flag);
    im_threshold_check(IM_BYTE, size, 87, -20, chk_flag);
    im_threshold_check(IM_USHORT, size, 100, 2000, chk_flag);
    im_threshold_check(IM_USHORT, size, 43300, 2000, chk_flag);
    im_threshold_check(IM_SHORT, size, 100, 2000, chk_flag);
    im_threshold_check(IM_SHORT, size, 2000, -25800, chk_flag);
    im_threshold_check(IM_ULONG, size, 100, 20003748, chk_flag);
    im_threshold_check(IM_ULONG, size, 20001263, 43300, chk_flag);
    im_threshold_check(IM_LONG, size, 100, 20234700, chk_flag);
    im_threshold_check(IM_LONG, size, 20657300, -25800, chk_flag);
    im_threshold_check(IM_FLOAT, size, 100, 20234700, chk_flag);
    im_threshold_check(IM_FLOAT, size, 20657300, -25800, chk_flag);
    im_threshold_check(IM_DOUBLE, size, 100, 20234700e10, chk_flag);
    im_threshold_check(IM_DOUBLE, size, 20657300e10, -25800, chk_flag);
}


int main(int argc, char *argv[])
{
    ip_coord2d size = {1123, 1225};
    int failed = 0;
    int chk_flag = 0;
    struct cmd_line_requests *clr;

    clr = process_cmdline(argc, argv, 1, op_names);

    ip_register_error_handler(error_handler);

    /* op_to_test = clr->op_to_test; */
    chk_flag = clr->flags;
    if (chk_flag & DO_SIZE)
	size = clr->im_size;

    if (chk_flag & DO_ERROR) {
	ip_image *im;

	printf("CHECKING errors reported correctly:\n");
	im = ip_alloc_image(IM_UBYTE, size);

	EXPECT_ERRORS(ERR_PARM_BAD_VALUE, 0);
	MARK(1);
	im_threshold(im, -129, 2000.0);
	CHECK_SEEN_ERROR_WITH_MSG;
	MARK(2);
	im_threshold(im, -0.1, 256.7);
	CHECK_SEEN_ERROR_WITH_MSG;

	ip_free_image(im);
	im = ip_alloc_image(IM_RGB, size);

	EXPECT_ERRORS(ERR_BAD_TYPE, 0);
	MARK(3);
	im_threshold(im, 10, 20);
	CHECK_SEEN_ERROR_WITH_MSG;

	num_tests++;
	if (failed)
	    num_failures++;

	printf("imthreshold error reporting:                                                  [%s]\n",
	       failed ? "FAIL" : "OK");

	failed = 0;
	ip_free_image(im);
    }

    EXPECT_NO_ERRORS;

    if (chk_flag & DO_PIXEL) {
	printf("CHECKING basic pixelwise thresholding code:\n");
	ip_clear_global_flags(IP_HWACCEL);
	do_threshold_checks(size, chk_flag);
    }

    if (chk_flag & DO_SIMD) {
	printf("CHECKING hardware (SIMD) acceleration (if available):\n");
	ip_set_global_flags(IP_HWACCEL);
	do_threshold_checks(size, chk_flag);
    }

    print_final_stats(num_tests, num_failures, 0);

    return num_failures;
}
