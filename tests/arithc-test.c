#define _XOPEN_SOURCE 600

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <inttypes.h>
#include <math.h>
#include <unistd.h>

#include <ip/ip.h>
#include <ip/tiff.h>
#include <ip/divide.h>
#include <ip/random.h>

#include "test-macros.h"
#include "timing.h"
#include "helpers.h"
#include "errorhdl.h"

#ifdef HAVE_MPFR
#include <mpfr.h>
#endif

char *program_name = "arithc-test";

/* Keep counts of failed and tested routines as globals */
int num_tests;
int num_failures;

/* Random seed to use when creating images; initialised from command line. */
static unsigned int random_seed;

/* im_addc like operators on int images */
#define PIXEL_OPERATION_ADDC(dval, v, d)  dval += v
#define PIXEL_OPERATION_ADDC_CLIPPED(dval, v, d)	\
    dval += v;						\
    CLIP_TO_PIXEL_TYPE_LIMITS(dval, d);

im_ic_int_DEFN(ADDC)
im_ic_int_DEFN(ADDC_CLIPPED)

/* im_addc like operators on real images */
im_ic_float_DEFN(ADDC)
im_ic_real_DEFN(ADDC)

/* im_addc like operators on complex images */
#define PIXEL_OPERATION_ADDC_COMPLEX(dval, val, d)	\
    dval.r += val.r;					\
    dval.i += val.i;

im_ic_complex_DEFN(ADDC_COMPLEX)
im_ic_dcomplex_DEFN(ADDC_COMPLEX)

/* im_subc like operators on int images */
#define PIXEL_OPERATION_SUBC(dval, v, d)  dval -= v
#define PIXEL_OPERATION_SUBC_CLIPPED(dval, v, d)	\
    dval -= v;						\
    CLIP_TO_PIXEL_TYPE_LIMITS(dval, d);

im_ic_int_DEFN(SUBC)
im_ic_int_DEFN(SUBC_CLIPPED)

/* im_subc like operators on real images */
im_ic_float_DEFN(SUBC)
im_ic_real_DEFN(SUBC)

/* im_subc like operators on complex images */
#define PIXEL_OPERATION_SUBC_COMPLEX(dval, val, d)	\
    dval.r -= val.r;					\
    dval.i -= val.i;

im_ic_complex_DEFN(SUBC_COMPLEX)
im_ic_dcomplex_DEFN(SUBC_COMPLEX)

/* im_mulc like operators on integer and real images */
#define PIXEL_OPERATION_MULC(dval, v, d)  dval *= v
#define PIXEL_OPERATION_MULC_ROUND(dval, v, d)  dval = round(dval * v)
#define PIXEL_OPERATION_MULC_CLIPPED(dval, v, d)	\
    dval = round(dval * v);				\
    CLIP_TO_PIXEL_TYPE_LIMITS(dval, d);


im_ic_float_DEFN(MULC)
im_ic_real_DEFN(MULC)
im_ic_real_DEFN(MULC_ROUND)
im_ic_real_DEFN(MULC_CLIPPED)


/*
 * The above MULC_ROUND function is inadequate as a reference for the
 * im_mulc() and im_divc() integer fixed-point functionality because (1)
 * double-precision has only 53-bits of precision (whereas 32/32 fixed-point
 * integer has 64-bits of precision) thus rounds the result differently, and
 * (2) double precision being floating-point does not truncate the integer
 * part to a 32-bit integer representation.
 *
 * The MULC_ROUND function above has sufficient precision to be accurate for
 * (U)BYTE image data.
 *
 * For (U)SHORT image data we must extend the calculation to 64-bits of
 * precision for it to be an accurate reference.  (The integer part always
 * has fewer than 32-bits for multipliers <= 65535 so don't need to
 * truncate.)
 *
 * For (U)LONG image data we must both extend the calculation to 64-bits and
 * truncate the integer part to 32-bits.
 *
 * With a fused multiply-add (FMA) instruction an efficient doubling of the
 * precision of floating-point results.  The code below uses the FMA to
 * implement MULC_ROUND with over 64-bits of precision so that the rounding
 * behaviour can be correct to the IP library fixed-point integer
 * implementation. GCC is smart enough to replace the fma() library calls
 * with a FMA cpu instruction if the arch supports it.  If the arch does not
 * have a FMA cpu instruction then the fma() library calls are substantially
 * slower, nevertheless the this implementation is faster than the
 * implementation with the MPFR library.
 *
 * Because the truncation of the integer part to 32-bits requires in
 * addition to the modulo arithmetic six floating-point operations to
 * rearrange the result to ensure the integer part is entirely contained in
 * one floating-point word, this operation is quite a bit slower to execute.
 * It is therefore worth the effort to provide separate reference
 * implementations for short (16-bit) and long (32-bit) image data.
 */

#ifdef HAVE_FMA


/*
 * A structure to hold a doubled precision double precision result.
 * The number represented is h + l with |l| < ulp(h)/2.
 */

struct double2 {
    double h;
    double l;
};


/*
 * Add two double numbers a and b and return the result so that res.h +
 * res.l = a + b and res.h = round_to_nearest_double(a + b).
 *
 * This algorithm was first described by Knuth.
 */

static inline struct double2 add_hl(double a, double b)
{
    double bb, aa, erra, errb;
    struct double2 res;

    res.h = a + b;
    bb = res.h - a;
    aa = res.h - bb;
    errb = b - bb;
    erra = a - aa;
    res.l = erra + errb;
    return res;
}

static inline double mulcx_round_short(double y, double v)
{
    /* rh+rl = y*v with rh = round_to_nearest_double(y*v) */
    double rh = y * v;
    double rl = fma(y, v, -rh);

    /* Integer part has fewer than 32-bits so straight into rounding. */
    double whole = (rh>=0.0) ? floor(rh) : ceil(rh);
    double fract = (rh - whole) + rl;
    return whole + round(fract);
}
#define PIXEL_OPERATION_MULCX_ROUND_SHORT(dval, v, d)  dval = mulcx_round_short(dval, v)

static inline double mulcx_round_ulong(double y, double v)
{
    struct double2 tmp;
    /* rh+rl = y*v with rh = round_to_nearest_double(y*v) */
    double rh = y * v;
    double rl = fma(y, v, -rh);

    /* Truncate integer part to 32-bits ... */
    rh = fmod(rh, (double)UINT32_MAX+1.0);
    tmp = add_hl(rh, rl);
    rh = tmp.h; rl = tmp.l;

    /* ... then do rounding. */
    double whole = (rh>=0.0) ? floor(rh) : ceil(rh);
    double fract = (rh - whole) + rl;
    return whole + round(fract);
}
#define PIXEL_OPERATION_MULCX_ROUND_ULONG(dval, v, d)  dval = mulcx_round_ulong(dval, v)

static inline double mulcx_round_long(double y, double v)
{
    struct double2 tmp;
    /* rh+rl = y*v with rh = round_to_nearest_double(y*v) */
    double rh = y * v;
    double rl = fma(y, v, -rh);
    double adj = copysign(INT32_MAX+1.0, rh);

    /* Truncate integer part to 32-bits ... */
    /*
     * If don't use add_hl() here can loose 1-bit of the calculation on the
     * very rare occasion, indeed, in about 1 in 100 million calculations!
     */
    tmp = add_hl(rh, adj);
    rh = tmp.h;
    tmp = add_hl(tmp.l, rl);
    rl = tmp.h;
    rh = fmod(rh, (double)UINT32_MAX+1.0);
    rh -= adj;
    tmp = add_hl(rh, rl);
    rh = tmp.h; rl = tmp.l;

    /* ... then do rounding. */
    double whole = (rh>=0.0) ? floor(rh) : ceil(rh);
    double fract = (rh - whole) + rl;
    return whole + round(fract);
}
#define PIXEL_OPERATION_MULCX_ROUND_LONG(dval, v, d)  dval = mulcx_round_long(dval, v)


void im_ic_real_MULCX_ROUND(ip_image *dd, double val)
{
	for (int j=0; j<dd->size.y; j++) {
	    for (int i=0; i<dd->size.x; i++) {
		double dval;
		GET_PIXEL_VALUE_AS_DOUBLE(dval, dd, i, j);
		switch (dd->type) {
		case IM_UBYTE:
		case IM_BYTE:
		    PIXEL_OPERATION_MULC_ROUND(dval, val, dd);
		    break;
		case IM_USHORT:
		case IM_SHORT:
		    PIXEL_OPERATION_MULCX_ROUND_SHORT(dval, val, dd);
		    break;
		case IM_ULONG:
		    PIXEL_OPERATION_MULCX_ROUND_ULONG(dval, val, dd);
		    break;
		case IM_LONG:
		    PIXEL_OPERATION_MULCX_ROUND_LONG(dval, val, dd);
		    break;
		default:
		    break;
		}
		SET_PIXEL_VALUE_FROM_DOUBLE(dd, dval, i, j);
	    }
	}
    }

#else
#  ifdef HAVE_MPFR
/*
 * An extended precision mulc reference routine.  Needed for the reference
 * to be actually correct for (U)LONG mulc as 64-bit arithmetic is needed.
 *
 * TBD: this is incorrect for (U)LONG as it does not take account of
 * truncating the whole number part to 32-bits.
 */
static inline double mulcx_round(double y, double v)
{
    MPFR_DECL_INIT(my, 96);
    mpfr_set_d(my, y, GMP_RNDN);
    mpfr_mul_d(my, my, v, GMP_RNDN);
    mpfr_round(my, my);
    return mpfr_get_d(my, GMP_RNDN);
}

#define PIXEL_OPERATION_MULCX_ROUND(dval, v, d)  dval = mulcx_round(dval, v)

im_ic_real_DEFN(MULCX_ROUND)

#  else
/* No mpfr lib nor FMA, can't provide the extended precision mulc routine. */
#  define im_ic_real_MULCX_ROUND NULL
#  define NO_GOOD_LONG_MULC_REFERENCE
#  endif
#endif

/* im_mulc like operators on complex images */
#define PIXEL_OPERATION_MULC_COMPLEX(dval, sval, d) {    \
        __typeof(dval.r) t;                             \
        t = dval.r*sval.r - dval.i*sval.i;              \
        dval.i = dval.i*sval.r + dval.r*sval.i;         \
        dval.r = t;                                     \
    }

im_ic_complex_DEFN(MULC_COMPLEX)
im_ic_dcomplex_DEFN(MULC_COMPLEX)

/* im_divc like operators on integer and real images */
#define PIXEL_OPERATION_DIVC(dval, v, d)  dval /= v
#define PIXEL_OPERATION_DIVC_ROUND(dval, v, d)  dval = round(dval / v)

im_ic_float_DEFN(DIVC)
im_ic_real_DEFN(DIVC)
im_ic_real_DEFN(DIVC_ROUND)

/* idivc like operators on integer images */
#define PIXEL_OPERATION_IDIVC(dval, v, d) dval /= v
im_ic_int_DEFN(IDIVC)

/* im_divc like operators on complex images */
#define PIXEL_OPERATION_DIVC_COMPLEX(dval, sval, d) {    \
        __typeof(dval.r) dc,tmp,denom,u,v;              \
        u = sval.r;                                     \
        v = sval.i;                                     \
        if (fabs(u) > fabs(v)) {                        \
            dc = v/u;                                   \
            denom = u + v*dc;                           \
            tmp = (dval.r + dval.i * dc) / denom;       \
            dval.i = (dval.i - dval.r * dc) / denom;    \
            dval.r = tmp;                               \
        }else{                                          \
            dc = u/v;                                   \
            denom = u*dc + v;                           \
            tmp = (dval.r*dc + dval.i) / denom;         \
            dval.i = (dval.i*dc - dval.r) / denom;      \
            dval.r = tmp;                               \
        }                                               \
    }

im_ic_complex_DEFN(DIVC_COMPLEX)
im_ic_dcomplex_DEFN(DIVC_COMPLEX)

struct operator_list {
    int id;
    int flags;
    char *name;
    char *op_name;
    int (*ip_im_op)(ip_image *, double, int);
    int (*ip_im_op_complex)(ip_image *, ip_dcomplex z, int);
    void (*f_ic_int_op)(ip_image *, double);
    void (*f_ic_int_op_x)(ip_image *, double);
    void (*f_ic_float_op)(ip_image *, double);
    void (*f_ic_double_op)(ip_image *, double);
    void (*f_ic_complex_op)(ip_image *, double, double);
    void (*f_ic_dcomplex_op)(ip_image *, double, double);
    void (*f_ic_int_clipped_op)(ip_image *, double);
};

/* id field takes these numbers; must match array index */
#define IM_ADDC 0
#define IM_SUBC 1
#define IM_MULC 2
#define IM_DIVC 3

#define NUM_IDS 4

/* Tests we can run */
struct operator_list operators[NUM_IDS] = {
    {IM_ADDC, 0, "im_addc", "addition", im_addc, im_addc_complex,
     im_ic_int_ADDC, NULL,
     im_ic_float_ADDC, im_ic_real_ADDC,
     im_ic_complex_ADDC_COMPLEX, im_ic_dcomplex_ADDC_COMPLEX,
     im_ic_int_ADDC_CLIPPED},
    {IM_SUBC, 0, "im_subc", "subtraction", im_subc, im_subc_complex,
     im_ic_int_SUBC, NULL,
     im_ic_float_SUBC, im_ic_real_SUBC,
     im_ic_complex_SUBC_COMPLEX, im_ic_dcomplex_SUBC_COMPLEX,
     im_ic_int_SUBC_CLIPPED},
    {IM_MULC, 0, "im_mulc", "multiplication", im_mulc, im_mulc_complex,
     im_ic_real_MULC_ROUND, im_ic_real_MULCX_ROUND,
     im_ic_float_MULC, im_ic_real_MULC,
     im_ic_complex_MULC_COMPLEX, im_ic_dcomplex_MULC_COMPLEX,
     im_ic_real_MULC_CLIPPED},
    {IM_DIVC, 0, "im_divc", "division", im_divc, im_divc_complex,
     /*
      * A reciprocal is performed on the divisor in both the IP library and
      * the reference code so we can use the equivalent mulc routines here.
      */
     im_ic_real_MULC_ROUND, im_ic_real_MULCX_ROUND,
     /* But a real division for floating-point and complex */
     im_ic_float_DIVC, im_ic_real_DIVC,
     im_ic_complex_DIVC_COMPLEX, im_ic_dcomplex_DIVC_COMPLEX,
     /*
      * Since there is not clipped division operator we subvert this field
      * to pass in the idivc (integer division) option.
      */
     im_ic_int_IDIVC}
};

const char *op_names[NUM_IDS] = {
    "im_addc", "im_subc", "im_mulc", "im_divc"
};


/*
 * Implementations:
 *  scalar -- scalar pixel pumping code
 *  fastcode -- "Poor Man's SIMD" doing tricks with scalar code.
 *  simd -- SIMD vectorised code
 */
const char *implementation_names[3] = {
    "scalar", "fastcode", "SIMD"
};


#define IMP_SCALAR 0x00
#define IMP_FAST 0x01
#define IMP_SIMD 0x02


/*
 * Fixed-point operations used in im_mulc/im_divc
 *  imulf -- fixed-point multiplication with multiplier >= 0.5
 *  idivf -- fixed-point multiplication with multiplier < 0.5
 */
const char *fpop_names[2] = {
    "imulf", "idivf"
};

#define IMULF 0x00
#define IDIVF 0x01



/*
 * Basic tests that we run by default.
 */

struct test_specification {
    int type;
    double valr;
    double vali;
    int flags;
};

static struct test_specification add_sub_tests[] = {
    { IM_UBYTE, 1.283, 0.0, NO_FLAG },
    { IM_UBYTE, 35.12746, 0.0, FLG_CLIP },
    { IM_BYTE, -10.09123, 0.0, NO_FLAG },
    { IM_BYTE, 10.0912, 0.0, FLG_CLIP },
    { IM_USHORT, 2764.12, 0.0, NO_FLAG },
    { IM_USHORT, 32001, 0.0, FLG_CLIP },
    { IM_SHORT, -7.7356245, 0.0, NO_FLAG },
    { IM_SHORT, -1.253441, 0.0, FLG_CLIP },
    { IM_ULONG, 200, 0.0, NO_FLAG },
    { IM_LONG, -2172645, 0.0, NO_FLAG },
    { IM_FLOAT, -2.6e8, 0.0, NO_FLAG },
    { IM_DOUBLE, 9.98217e2, 0.0, NO_FLAG },
    { IM_COMPLEX, 123.8, -9172, NO_FLAG },
    { IM_DCOMPLEX, 1e5, 3846.152, NO_FLAG },
    { 0, 0, 0, 0}
};

static struct test_specification add_sub_fastcode_tests[] = {
    { IM_UBYTE, 100, 0.0, NO_FLAG },
    { IM_BYTE, -10, 0.0, NO_FLAG },
    { IM_USHORT, 32000, 0.0, NO_FLAG },
    { IM_SHORT,  250, 0.0, NO_FLAG },
    { IM_ULONG, 182700, 0.0, NO_FLAG },
    { IM_LONG, -1827176, 0.0, NO_FLAG },
    { 0, 0, 0, 0}
};

static struct test_specification mul_tests[] = {
    { IM_UBYTE, 1.283, 0.0, NO_FLAG },
    { IM_UBYTE, 35.12746, 0.0, FLG_CLIP },
    { IM_BYTE, -1.5, 0.0, NO_FLAG },
    { IM_BYTE, 10.0912, 0.0, FLG_CLIP },
    { IM_USHORT, 2764.12, 0.0, NO_FLAG },
    { IM_USHORT, 32001, 0.0, FLG_CLIP },
    { IM_SHORT, 7.7356245, 0.0, NO_FLAG },
    { IM_SHORT, -1.253441, 0.0, FLG_CLIP },
    { IM_ULONG, 101.25, 0.0, NO_FLAG },
    /*
     * TBD
     * A multiplier of 1.5 is causing a test failure in AVX2 SIMD
     * implementation; possibly because of the way it calculates the sign of
     * the result which could be incorrect if the result is wrapped.  Keep
     * this here as an option so we don't loose sight of it.
     */
#if 0
    { IM_LONG, 1.5, 0.0, NO_FLAG },
#else
    { IM_LONG, -21645, 0.0, NO_FLAG },
#endif
    { IM_FLOAT, -2.6e8, 0.0, NO_FLAG },
    { IM_DOUBLE, 9.98217e2, 0.0, NO_FLAG },
    { IM_COMPLEX, 123.8, -9172, NO_FLAG },
    { IM_DCOMPLEX, 1e5, 3846.152, NO_FLAG },
    { 0, 0, 0, 0}
};

static struct test_specification div_tests[] = {
    { IM_UBYTE, 1.283, 0.0, NO_FLAG },
    { IM_UBYTE, 35.12746, 0.0, FLG_INTDIV },
    { IM_BYTE, -10.091217, 0.0, NO_FLAG },
    { IM_BYTE, 2.091263, 0.0, NO_FLAG },
    { IM_BYTE, 10.0912, 0.0, FLG_INTDIV },
    { IM_USHORT, 2764.12, 0.0, NO_FLAG },
    { IM_USHORT, 32001, 0.0, FLG_INTDIV },
    { IM_SHORT, 7.7356245, 0.0, NO_FLAG },
    { IM_SHORT, -12.7356245, 0.0, NO_FLAG },
    { IM_SHORT, -2.253441, 0.0, FLG_INTDIV },
    /*
     * TBD
     * A divisor of 200 is causing a test failure but the bug is probably in
     * the reference code, not the IP library.  Keep this here as an option
     * so we don't loose sight of it.
     */
#if 0
    { IM_ULONG, 200, 0.0, NO_FLAG },
#else
    { IM_ULONG, 11.253, 0.0, NO_FLAG },
#endif
    { IM_ULONG, 200, 0.0, FLG_INTDIV },
    { IM_LONG, -2172645, 0.0, NO_FLAG },
    { IM_LONG, -2172645, 0.0, FLG_INTDIV },
    { IM_FLOAT, -2.6e8, 0.0, NO_FLAG },
    { IM_DOUBLE, 9.98217e2, 0.0, NO_FLAG },
    { IM_COMPLEX, 123.8, -9172, NO_FLAG },
    { IM_DCOMPLEX, 1e5, 3846.152, NO_FLAG },
    { 0, 0, 0, 0}
};



/*
 * Fix the number bits used to represent the fraction of a number (only for
 * processing integer data).
 */

static inline double fix_number_fraction_bits(double val, int bits)
{
    /* Special case: abort if bits == 0 */
    if (! bits)
	return val;

    if (fabs(val) < 0.5) {
	/*
	 * If the multiplier is less than one-half then the integer idivf
	 * routines with a shift are used to guarantee the maximum number of
	 * bits are used.  We take that shift into account.
	 */
	int l = ceil(-log2(fabs(val))) - 1;
	bits += l;
    }
    return floor(val * pow(2, bits)) / pow(2, bits);
}



/*
 * Calculate the reference image given operation op, operation value val1
 * (and val2 if complex) and the flags.
 */

static void calculate_reference_image(ip_image *r, int op, double val1, double val2, int flag, int fbits)
{
    int type = r->type;

    if (flag == FLG_CLIP) {
	double u = (op == IM_ADDC || op == IM_SUBC) ? round(val1) : val1;

	if (type_integer(type) && operators[op].f_ic_int_clipped_op != NULL)
	    operators[op].f_ic_int_clipped_op(r, u);

    }else if (flag == FLG_INTDIV && op == IM_DIVC) {
	double u = round(val1);

	if (type_integer(type) && operators[op].f_ic_int_clipped_op != NULL)
	    operators[op].f_ic_int_clipped_op(r, u);

    }else{
	double u;

	if (type_integer(type)) {
	    if (op == IM_ADDC || op == IM_SUBC)
		u = round(val1);
	    else if (op == IM_MULC) {
		u = fix_number_fraction_bits(val1, fbits);
	    }else if (op == IM_DIVC) {
		u = fix_number_fraction_bits(1.0/val1, fbits);
	    }else
		u = val1;
	}else{
	    u = val1;		/* STFCU */
	}

	switch (type) {
	case IM_BINARY:
	case IM_PALETTE:
	case IM_BYTE:
	case IM_UBYTE:
	    /* Even for im_mulc/im_divc the basic reference operators are fine */
            if (operators[op].f_ic_int_op != NULL)
                operators[op].f_ic_int_op(r, u);
            break;
	case IM_SHORT:
	case IM_USHORT:
	case IM_LONG:
	case IM_ULONG:
	    if (op == IM_ADDC || op == IM_SUBC) {
		if (operators[op].f_ic_int_op != NULL)
		    operators[op].f_ic_int_op(r, u);
	    }else{
		if (operators[op].f_ic_int_op_x != NULL)
		    operators[op].f_ic_int_op_x(r, u);
		else if (operators[op].f_ic_int_op != NULL)
		    operators[op].f_ic_int_op(r, u);
	    }
	    break;
	case IM_FLOAT:
	    if (operators[op].f_ic_float_op != NULL)
		operators[op].f_ic_float_op(r, val1);
	    break;
	case IM_DOUBLE:
	    if (operators[op].f_ic_double_op != NULL)
		operators[op].f_ic_double_op(r, val1);
	    break;
	case IM_COMPLEX:
	    if (operators[op].f_ic_complex_op != NULL)
		operators[op].f_ic_complex_op(r, val1, val2);
	    break;
	case IM_DCOMPLEX:
	    if (operators[op].f_ic_dcomplex_op != NULL)
		operators[op].f_ic_dcomplex_op(r, val1, val2);
	    break;
	default:
	    break;
	}
    }
}




/*
 * The im_mulc() and im_divc() operators on integer data use fix-point
 * integer arithmetic.  This routine attempts to find the number of bits
 * used in the fraction.  The idea is that if we use too many bits to
 * characterise the value used in the multiplier, then rounding errors
 * occur.  We reduce the number of bits in the fraction of the multiplier
 * until we eliminate rounding errors.
 *
 * fpop should be one of IMULF or IDIVF. The routine only calls im_mulc()
 * but if IDIVF is passed it uses a multiplicative value less than 0.5 which
 * exercises the divc/idivf algorithm in im_mulc().
 *
 * Returns:
 *   >=0 is the fraction size.
 *   -1 Couldn't find it; got rounding errors down to 8-bits fraction.
 *   -2 Error; failed test (e.g. failed comparison with differences
 *                               greater than rounding errors.)
 */

static int find_fraction_size(int fpop, int type, int flag)
{
    ip_image *im, *r, *d;
    double val, val2;
    int fraction_idx = -1;
    const ip_coord2d size = (ip_coord2d){16, 16};
    const int fractions[9] = {48, 40, 32, 31, 24, 23, 16, 15, 8};

    /* Allocate a small test image with random data */
    im = create_test_image(type, size, DO_RANDOM);

    /*
     * The scaling factor; neither of these is exactly representable in
     * binary.  0.3 and 1.1 seemed to be good numbers here,
     */
    if (fpop == IDIVF) {
	val = 0.3;
	/*val2 = 0x1.aaaaaaaaaaaaa8p-2; */
	val2= 0x1.55555555555558p-2;
    }else{
	val = 1.1;
	/* val = 0x1.aaaaaaaaaaaaa8p-0; */
	val2 = 0x1.55555555555558p-0;
    }
    r = d = NULL;
    /* For each precision to be tested do */
    for (int j=0; j<9; j++) {
	int fraction_size = fractions[j];
	double valx;
	int fail;

	/* The value with reduced precision */
	valx = fix_number_fraction_bits(val, fraction_size);

	r = im_copy(im);
	d = im_copy(im);

	im_mulc(d, valx, flag);
	calculate_reference_image(r, IM_MULC, valx, valx, flag, fraction_size);

	set_compare_behaviour( .quiet=1, .permit_rounding_error=1 );
	fail = compare_images(r, d, NULL, NULL, NULL);

	if (fail == 0) {
	    /* Run a second test to be really sure */
	    ip_free_image(d);
	    ip_free_image(r);

	    valx = fix_number_fraction_bits(val2, fraction_size);

	    d = im_copy(im);
	    r = im_copy(im);

	    im_mulc(d, valx, flag);
	    calculate_reference_image(r, IM_MULC, valx, valx, flag, fraction_size);

	    fail = compare_images(r, d, NULL, NULL, NULL);
	}

	if (fail == 0) {
	    /* Exact compare of images; no need to check further */
	    fraction_idx = j;
	    ip_free_image(r);
	    ip_free_image(d);
	    break;
	}else if (fail != 2) {
	    /* Differences greater than rounding errors; something's terribly wrong */
	    int64_t imval, dval, rval;
	    int x, y;
	    printf("Unexpected failure of image compare for type %s in fraction find.\n",
		   ip_type_name(type));

	    x = compare_failure_point.x;
	    y = compare_failure_point.y;

	    GET_PIXEL_VALUE_AS_SIGNED_LONG(imval, im, x, y);
	    GET_PIXEL_VALUE_AS_SIGNED_LONG(dval, d, x, y);
	    GET_PIXEL_VALUE_AS_SIGNED_LONG(rval, r, x, y);

	    printf(".... while calculating %a * %jd got %jd but should be %jd.\n",
		   valx, imval, dval, rval);

	    fraction_idx = -2;
	    ip_free_image(r);
	    ip_free_image(d);
	    break;
	} /* else only rounding errors did occur so continue to next in for next loop */

	ip_free_image(r);
	ip_free_image(d);
    }

    ip_free_image(im);

    if (fraction_idx < 0)
	return fraction_idx;
    else if (fraction_idx < 9)
	return fractions[fraction_idx];
    else
	return -2;
}


static void print_fraction_size_fail(const char *msg, int error, int type)
{
    if (error < 0) {
	printf("Problem: couldn't find number of bits in %s fixed-point fraction.\n", msg);
	if (error == -1)
	    printf("... rounding errors continued down to 16 bits.\n");
	else
	    printf("... complete error in calculation!\n");
	printf("... continuing tests with using 16-bits fraction.\n");
	push_footnote_v("%s %s failure to detect fraction precision.",
			ip_type_name(type), msg);
    }else{
	printf("Problem: fewer than 16-bits maintained in %s fixed-point fraction.\n", msg);
	printf("... rounding errors stopped at %d bits fraction which is below spec.\n", error);
	push_footnote_v("%s %s too few bits in fraction precision (is %d).",
			ip_type_name(type), msg, error);
    }
}



/*
 * Test for fixed-point fraction size for a particular op/type/flag
 * combination only once in whole test suite.  Once we have it we store it
 * for future reference.
 *
 * We need an array that is at least 6 types * 2 ops * 2 flags = 24 entries.
 */


struct fixptfract_tbl {
    unsigned int key;
    unsigned int bits;
};

static struct fixptfract_tbl *fpftbl;
static int fpftbl_max_n, fpftbl_n;


static void alloc_fpftbl(int size)
{
    if (fpftbl == NULL || size > fpftbl_max_n) {
	if ((fpftbl = realloc(fpftbl, sizeof(struct fixptfract_tbl) * size)) == NULL)
	    out_of_memory();
	fpftbl_max_n = size;
    }
}

static void add_to_fpftbl(unsigned int key, unsigned int bits)
{
    if (fpftbl_n == fpftbl_max_n)
	alloc_fpftbl(fpftbl_max_n + 24);
    fpftbl[fpftbl_n].key = key;
    fpftbl[fpftbl_n].bits = bits;
    fpftbl_n++;
}


static int lookup_fraction_size(int implementation, int fpop, int type, int flag)
{
    int found, bits;
    unsigned int key;

    alloc_fpftbl(48);

    /*
     * Key constructed from:
     * lowest 4 bits of implementation and of fpop,
     * lowest 8 bits of type, and
     * lowest 16 bits of flag.
     */

    key = ((implementation&0xf)<<28 | (fpop&0xf)<<24 | (type&0xff)<<16 | (flag&0xffff));

    found = -1;
    for (int j=0; j<fpftbl_n; j++) {
	if (fpftbl[j].key == key) {
	    found = j;
	    break;
	}
    }

    if (found >= 0)
	return fpftbl[found].bits;
    else{
	bits = find_fraction_size(fpop, type, flag);

	if (bits < 15) {
	    print_fraction_size_fail(fpop_names[fpop], bits, type);
	}else{
	    push_footnote_v("%s %s %s%s calculates with %d bits in the fraction.",
			    implementation_names[implementation],
			    ip_type_name(type), fpop_names[fpop],
			    (flag & FLG_CLIP) ? "_clipped" : "", bits);
	}
	add_to_fpftbl(key, bits);
    }
    return bits;
}


static void save_image(ip_image *im, int type,
		       int op, int flags, const char *info)
{
    char fstr[180];
    const char *flag_info;

    if (flags & FLG_CLIP)
	flag_info = "clipped-";
    else if (flags & FLG_INTDIV)
	flag_info = "intdiv-";
    else
	flag_info = "";

    snprintf(fstr, 180, "%s-%s-%s%s.tiff",
	     op_names[op], ip_type_name(type), flag_info, info);
    write_tiff_image(fstr, im);
}



int im_op_ic_check(int implementation, int op, int type, ip_coord2d size,
		   double u, double v, int flag, int source)
{
    ip_image *r, *d, *orig;
    int failed = 0;
    int fbits = 0;
    int fail;
    char str[64];
    unsigned char *chksum;
    char *op_name = operators[op].op_name;

    if (operators[op].id != op) {
	fprintf(stderr, "ERROR: operator id numbers (%d and %d) don't match.\n",
		op, operators[op].id);
	fprintf(stderr, "FIX AND RECOMPILE ME.\n");
	exit(255);
    }

    if (type_integer(type) && ((op == IM_MULC) || ((op == IM_DIVC) & !(flag & FLG_INTDIV)))) {
	/*
         * Determine:
	 *   - which of imulf/idivf routine is used internally in IP lib.
	 *   - number (fraction) bits IP library supports in imulf/idivf calculation.
	 */
	int fpop = (op == IM_MULC) ? IMULF : IDIVF;
	if (fabs(u) < 0.5) fpop ^= IDIVF;

	/*
	 * IDIVF has no clipped variety as no clipping can occur; the flag
	 * is silently ignored in the IP library under these circumstances
	 * so there is no point in distinguishing an idivf_clipped variety
	 * of idivf.
	 */
	if (fpop == IDIVF && (flag & FLG_CLIP))
	    flag &= ~FLG_CLIP;

	fbits = lookup_fraction_size(implementation, fpop, type, flag);
	if (fbits < 15) {
	    printf("Failed fraction size determiniation\n");
	    failed = 1;
	    fbits = 15;
	}

	/* Limit u to fbits of fraction precision otherwise test below will fail */
	//u = fix_number_fraction_bits(u, fbits);
    }

    srandom(random_seed);
    r = create_test_image(type, size, source & DO_RANDOM);

    if (source & DO_SAVE) {
	save_image(r, type, op, flag, "original");
    }

    d = im_copy(r);
    orig = im_copy(r);
    failed |= compare_images(r, d, "Initial compare of reference", "r", "d");

    set_image_row_padding(r, 0x53);
    failed |= check_image_row_padding(r, 0x53, "Set padding check padding");
    failed |= compare_images(r, d, "Set padding check image data", "r", "d");

    /*
     * Create a reference image (r) that results from the operator applied
     *  on the source image (s).
     */
    calculate_reference_image(r, op, u, v, flag, fbits);

    /*
     * Now call the IP library image processing operator on the image d.
     */

    if (type_complex(type))
	operators[op].ip_im_op_complex(d, (ip_dcomplex){u, v}, flag);
    else
	operators[op].ip_im_op(d, u, flag);

    /* Get md5 checksum of result if requested */
    if (source & DO_CHECKSUM) {
	chksum = im_md5(d);
    }

    if (source & DO_SAVE) {
	save_image(d, type, op, flag, "result");
	save_image(r, type, op, flag, "reference");
    }

    /* Compare d against r.  They should be exactly the same! */

    set_compare_behaviour( .quiet=0, .permit_rounding_error=0 );
    sprintf(str, "Image %s check", op_name);
    fail = compare_images(r, d, str, "r", "d");
#if 0
    if (fail) {
	/*
	 * For explorative testing purposes only; once this code is deleted
	 * the copy of the original image into orig can also be deleted.
	 */
	ip_coord2d pt = compare_failure_point;
	printf("...Operand = %g\n", u);
	if (orig->type==IM_USHORT) {
	printf("...at (%d,%d) %d [0x%08x] gives...\n",
	       pt.x, pt.y, IM_PIXEL(orig, pt.x, pt.y, us), IM_PIXEL(orig, pt.x, pt.y, us));
	printf("...%d [0x%08x] but should be %d [%08x]\n",
	       IM_PIXEL(d, pt.x, pt.y, us), IM_PIXEL(d, pt.x, pt.y, us),
	       IM_PIXEL(r, pt.x, pt.y, us), IM_PIXEL(r, pt.x, pt.y, us));
	}else{
	printf("...at (%d,%d) %d [0x%08x] gives...\n",
	       pt.x, pt.y, IM_PIXEL(orig, pt.x, pt.y, ul), IM_PIXEL(orig, pt.x, pt.y, ul));
	printf("...%d [0x%08x] but should be %d [%08x]\n",
	       IM_PIXEL(d, pt.x, pt.y, ul), IM_PIXEL(d, pt.x, pt.y, l),
	       IM_PIXEL(r, pt.x, pt.y, ul), IM_PIXEL(r, pt.x, pt.y, l));
	}
    }
#endif
    failed |= fail;
    sprintf(str, "Image %s padding check", op_name);
    failed |= check_image_row_padding(r, 0x53, str);

    /*
     * We don't check padding on d as the IP library is permitted to modify
     * the row padding at its whim (which it will normally do so if using
     * vector based (SIMD) operations).
     */

    /* Message of the operator and type of image tested */
    printf("%-6s %-8s %s ", operators[op].name,
	   ip_image_type_info[type].name,
	   (flag == FLG_CLIP) ? "clipped" : ((flag == FLG_INTDIV) ? "intdiv " : "       "));

    /* Print out comparison result including timing info if requested. */
    if (source & DO_CHECKSUM) {
	print_md5(chksum);
	putchar('\n');
    }else{
	uint64_t time;
	uint64_t key = KEY_CONSTRUCT(type, op, 0, flag);

	if (source & DO_TIMING) {
	    /* Time the operator when requested */
	    time_start();
	    for (int j=0; j<10; j++) {
		if (type_complex(type))
		    operators[op].ip_im_op_complex(d, (ip_dcomplex){u, v}, flag);
		else
		    operators[op].ip_im_op(d, u, flag);
	    }
	    time = time_end() / 10;
	}else{
	    time = -1LL;
	}
	print_time_status(key, cycles, time, ipc, failed);
    }

    ip_free_image(orig);
    ip_free_image(d);
    ip_free_image(r);

    /* Increment counts (global variables) */
    num_tests++;
    if (failed) num_failures++;

    return failed;
}



static int run_test(int op, ip_image *orig, double val1, double val2, int flag)
{
    ip_image *d, *r;
    int failed;
    char str[64];

    d = im_copy(orig);
    r = im_copy(orig);

    calculate_reference_image(r, op, val1, val2, flag, 0);

    /*
     * Now call the IP library image processing operator on the image d.
     */

    if (type_complex(orig->type))
	operators[op].ip_im_op_complex(d, (ip_dcomplex){val1, val2}, flag);
    else
	operators[op].ip_im_op(d, val1, flag);

    /* Compare d against r.  They should be exactly the same! */

    sprintf(str, "Image %s check [val=%a]", operators[op].op_name, val1);
    failed = compare_images(r, d, str, "r", "d");
#if 0
    /* For explorative testing purposes only */
    if (failed) {
	ip_coord2d pt = compare_failure_point;
	printf("...at (%d,%d) %d [0x%08x] gives...\n",
	       pt.x, pt.y, IM_PIXEL(orig, pt.x, pt.y, l), IM_PIXEL(orig, pt.x, pt.y, l));
	printf("...%d [0x%08x] but should be %d [%08x]\n",
	       IM_PIXEL(d, pt.x, pt.y, l), IM_PIXEL(d, pt.x, pt.y, l),
	       IM_PIXEL(r, pt.x, pt.y, l), IM_PIXEL(r, pt.x, pt.y, l));
    }
#endif

    ip_free_image(d);
    ip_free_image(r);

    return failed;
}



#define FIXED_32_HALF (1ULL << 31)
#define FIXED_32_ONE (1ULL << 32)
#define FIXED_32_ROUND(x) llround(x)



static int im_op_ic_check_indepth(int implementation, int op, int type, ip_coord2d size,
				  int flag, int num)
{
    ip_image *orig;
    int failed = 0;
    int fraction_bits_mulc, fraction_bits_divc;
    double multiplier;
    int num_failed = 0;
    int num_warned = 0;
    int disp_failed;
    int disp_warned;
    int displaying = 1;

    if (operators[op].id != op) {
	fprintf(stderr, "ERROR: operator id numbers (%d and %d) don't match.\n",
		op, operators[op].id);
	fprintf(stderr, "FIX AND RECOMPILE ME.\n");
	exit(1000);
    }

    /* Currently only support in-depth analysis on IM_MULC */
    if (op != IM_MULC)
	return 0;

    /* We need to know what fraction size is needed for comparison */
    fraction_bits_mulc = lookup_fraction_size(implementation, IMULF, type, flag);
    fraction_bits_divc = lookup_fraction_size(implementation, IDIVF, type, flag & ~FLG_CLIP);

    srandom(random_seed);
    orig = create_test_image(type, size, DO_RANDOM);
    save_image(orig, orig->type, IM_MULC, flag, "indepth-origin");

    /*
     * Run some specific tests.  The half-integral ones are useful for
     * picking up if half-integral results get rounded correctly.
     */

    set_compare_behaviour( .permit_rounding_error=0 );

    failed |= run_test(op, orig, 0.0, 0.0, flag);
    failed |= run_test(op, orig, 0.25, 0.0, flag);
    failed |= run_test(op, orig, 0.5, 0.0, flag);
    failed |= run_test(op, orig, 1.0, 0.0, flag);
    failed |= run_test(op, orig, 1.5, 0.0, flag);
    failed |= run_test(op, orig, 2.0, 0.0, flag);
    failed |= run_test(op, orig, 2.5, 0.0, flag);
    failed |= run_test(op, orig, 200, 0.0, flag);
    multiplier = fix_number_fraction_bits(1.0/200.0, fraction_bits_divc);
    failed |= run_test(op, orig, multiplier, 0.0, flag);
    if (ip_type_min(type) < 0.0) {
	failed |= run_test(op, orig, -0.25, 0.0, flag);
	failed |= run_test(op, orig, -0.5, 0.0, flag);
	failed |= run_test(op, orig, -1.0, 0.0, flag);
	failed |= run_test(op, orig, -1.5, 0.0, flag);
    }
    if (failed & 1) {
	num_failed++;
    }
    if (failed & 2) {
	num_warned++;
    }
    if (type != IM_LONG && type != IM_ULONG) {
	/* Known failure; convert to warn */
	if (run_test(op, orig, ip_type_max(type), 0.0, flag))
	    failed |= 2;
	num_warned++;
    }


    multiplier = fix_number_fraction_bits(1.0/ip_type_max(type), fraction_bits_divc);
    failed |= run_test(op, orig, multiplier, 0.0, flag);
    if (ip_type_min(type) != 0.0 || (op == IM_ADDC || op == IM_SUBC)) {
	failed |= run_test(op, orig, ip_type_min(type), 0.0, flag);
	multiplier = fix_number_fraction_bits(1.0/ip_type_min(type), fraction_bits_divc);
	failed |= run_test(op, orig, multiplier, 0.0, flag);
    }
    disp_failed = num_failed;
    disp_warned = num_warned;

    for (int j=0; j<num; j++) {
	int64_t whole;
	int fraction;
	double u;
	int fail;

	/*
	 *  We construct a double precision value to have no more bits in
	 *  the fraction than what we detected above that the IP library
	 *  supports. If the value were constructed with more bits we would
	 *  get false positive failures due to rounding errors.
	 */
	switch (type) {
	case IM_BYTE:
	    whole = random() / ((RAND_MAX+1LL)/256) - 128;
	    break;
	case IM_UBYTE:
	    whole = random() / ((RAND_MAX+1LL)/256);
	    break;
	case IM_SHORT:
	    whole = ip_rand_wshort();
	    break;
	case IM_USHORT:
	    whole = ip_rand_wushort();
	    break;
	case IM_LONG:
	    whole = ip_rand_wlong();
	    break;
	case IM_ULONG:
	    whole = ip_rand_wulong();
	    break;
	default:
	    whole = random();
	    break;
	}
	fraction = random();
	u = whole + (double)fraction / (RAND_MAX+1.0);
	if (fraction & 0x1000)
	    u = 1.0 / u;

	if (fabs(u) >= 0.5)
	    u = fix_number_fraction_bits(u, fraction_bits_mulc);
	else
	    u = fix_number_fraction_bits(u, fraction_bits_divc);

	/* Run the test. */
	fail = run_test(op, orig, u, u, flag);
	if (fail && (u > 0x1p31)) {
	    /* Known failure on extreme (and probably unuseful) cases */
	    fail = 2;
	}
	failed |= fail;

	if (fail & 1) {
	    num_failed++;
	    if (displaying) disp_failed++;
	}else if (fail & 2) {
	    num_warned++;
	    if (displaying) {
		int l;
		uint64_t cval;
		if (u < 0.5) {
		    l = (int)ceil(-log2(fabs(u))) - 1;
		    cval = (uint64_t)(int64_t)FIXED_32_ROUND((double)FIXED_32_ONE * u * pow(2,l));
		    printf("...u = %g [%a] cval = %" PRIu64 " [0x%016" PRIx64 "] l = %d\n",
			   u, u, cval, cval, l);
		}else{
		    cval = (uint64_t)(int64_t)FIXED_32_ROUND((double)FIXED_32_ONE * u);
		    printf("...u = %g [%a] cval = %" PRIu64 " [0x%016" PRIx64 "]\n",
			   u, u, cval, cval);
		}
		disp_warned++;
	    }
	}
	/* Limit the number of failures reported on stdout */
	if (fail && ((num_failed + num_warned) >= 5)) {
	    set_compare_behaviour( .quiet=1, .permit_rounding_error=1 );
	    displaying = 0;
	}
    }

    if (num_warned > disp_warned)
	printf("...... plus %d more warning messages suppressed.\n",
	       num_warned-disp_warned);
    if (num_failed > disp_failed)
	printf("...... plus %d more failure messages suppressed.\n",
	       num_failed-disp_failed);

#ifdef NO_GOOD_LONG_MULC_REFERENCE
    if ((num_failed || num_warned) && (type == IM_LONG || type == IM_ULONG)) {
	push_footnote_v("Failure report of %s imulf may be due to incorrect test reference code.",
			ip_type_name(type));
    }
#endif

    if (failed & 1) {
	printf("Random seed used in failing tests: %u\n", random_seed);
    }
    if (failed == 2) {
	push_footnote_v("Warning due to failure on extreme values of multiplication constant only.");
    }
    random_seed++;

    /* Message of the operator and type of image tested */
    printf("%-6s %-8s %s ", operators[op].name,
	   ip_image_type_info[type].name,
	   (flag == FLG_CLIP) ? "clipped" : ((flag == FLG_INTDIV) ? "intdiv " : "       "));

    /* Print out comparison result including timing info if requested. */
    uint64_t key = KEY_CONSTRUCT(type, op, 0, flag);
    print_time_status(key, cycles, -1ULL, ipc, failed);
    num_tests++;
    if (failed & 1) num_failures++;

    ip_free_image(orig);

    return failed;
}


void run_im_ic_tests(int operator, int type, ip_coord2d size, int chk_flag)
{
    struct test_specification *tests;
    struct test_specification *fastcode_tests;

    /* Choose correct flags to test */

    switch (operator) {
    case IM_ADDC:
    case IM_SUBC:
	tests = add_sub_tests;
	fastcode_tests = add_sub_fastcode_tests;
	break;
    case IM_MULC:
	tests = mul_tests;
	fastcode_tests = NULL;
	break;
    case IM_DIVC:
	tests = div_tests;
	fastcode_tests = NULL;
	break;
    default:
	/* This should never be possible! */
	tests = fastcode_tests = NULL;
	break;
    }

    EXPECT_NO_ERRORS;

    if (chk_flag & DO_PIXEL) {
	if (tests) {
	    printf("CHECKING basic pixelwise %s code:\n",operators[operator].op_name);

	    ip_clear_global_flags(IP_FAST_CODE | IP_HWACCEL);

	    for (struct test_specification *test = tests; test->type; test++) {
		if (type == 0 || type == test->type) {
		    im_op_ic_check(IMP_SCALAR, operator, test->type, size,
				   test->valr, test->vali, test->flags, chk_flag);
		    if ((chk_flag & DO_VERBOSE) && type_integer(test->type)) {
			im_op_ic_check_indepth(IMP_SCALAR, operator, test->type, size,
					       test->flags, 500);
		    }
		}
	    }
	}

	if (fastcode_tests) {
	    printf("CHECKING fast code (\"poor man's SIMD\"):\n");

	    ip_set_global_flags(IP_FAST_CODE);

	    for (struct test_specification *test = fastcode_tests; test->type; test++) {
		if (type == 0 || type == test->type)
		    im_op_ic_check(IMP_FAST, operator, test->type, size,
				   test->valr, test->vali, test->flags, chk_flag);
	    }
	}
    }

    if (chk_flag & DO_SIMD) {

	if (tests) {
	    printf("CHECKING hardware (SIMD) acceleration (if available):\n");

	    ip_clear_global_flags(IP_FAST_CODE);
	    ip_set_global_flags(IP_HWACCEL);

	    for (struct test_specification *test = tests; test->type; test++) {
		if (type == 0 || type == test->type) {
		    im_op_ic_check(IMP_SIMD, operator, test->type, size,
				   test->valr, test->vali, test->flags, chk_flag);
		    if ((chk_flag & DO_VERBOSE) && type_integer(test->type)) {
			im_op_ic_check_indepth(IMP_SIMD, operator, test->type, size,
					       test->flags, 500);
		    }
		}
	    }
	}
    }
}


int run_im_ic_error_reporting_tests(int operator, ip_coord2d size, int chk_flag)
{
    ip_image *i1;
    int failed = 0;

    if (NOT (chk_flag & DO_ERROR))
	return 0;

    printf("CHECKING errors reported correctly:\n");

    if (operator <= 1) {
	EXPECT_ERRORS(ERR_PARM_BAD_VALUE, 0);
	i1 = ip_alloc_image(IM_UBYTE, size);

	/* Should generate ERR_PARM_BAD_VALUE (im_addc() and im_subc() only) */
	MARK(1);
	operators[operator].ip_im_op(i1, 257, NO_FLAG);
	CHECK_SEEN_ERROR_WITH_MSG;

	ip_free_image(i1);
    }

    EXPECT_ERRORS(ERR_BAD_TYPE, 0);
    i1 = ip_alloc_image(IM_BINARY, size);

    /* Should generate ERR_BAD_TYPE */
    MARK(3);
    operators[operator].ip_im_op(i1, 0, NO_FLAG);
    CHECK_SEEN_ERROR_WITH_MSG;

    ip_free_image(i1);

    EXPECT_ERRORS(ERR_BAD_FLAG, ERR_BAD_FLAG2);
    i1 = ip_alloc_image(IM_LONG, size);

    /* Should generate one of ERR_BAD_FLAG or ERR_BAD_FLAG2 */
    MARK(4);
    operators[operator].ip_im_op(i1, 0, FLG_REAL);
    CHECK_SEEN_ERROR_WITH_MSG;

    ip_free_image(i1);

    num_tests++;
    if (failed) num_failures++;
    printf("%s error reporting:                                    [%s]\n",
	   operators[operator].name, failed ? "FAIL" : "OK");

    return failed;
}



int main(int argc, char *argv[])
{
    ip_coord2d size = {1023, 1025};
    int operator_to_test = -1;
    int chk_flag;
    struct cmd_line_requests *clr;

    clr = process_cmdline(argc, argv, NUM_IDS, op_names);

    operator_to_test = clr->op_to_test;
    chk_flag = clr->flags;
    if (clr->flags & DO_SIZE)
	size = clr->im_size;
    random_seed = clr->random_seed;

    ip_register_error_handler(error_handler);

    if (operator_to_test < 0) {
	for (int j=0; j<NUM_IDS; j++) {
	    run_im_ic_error_reporting_tests(j, size, chk_flag);
	    run_im_ic_tests(j, clr->im_type, size, chk_flag);
	    if (j < NUM_IDS - 1) {
		printf("\n");
	    }
	}
    }else{
	run_im_ic_error_reporting_tests(operator_to_test, size, chk_flag);
	run_im_ic_tests(operator_to_test, clr->im_type, size, chk_flag);
    }

    print_final_stats(num_tests, num_failures, 0);

    return num_failures;
}
