#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

#include <ip/divide.h>

int verbose = 1;

static unsigned int test_udiv32(uint32_t d, uint32_t nstart, uint32_t nstep, uint32_t nend)
{
    unsigned int Nfails = 0;
    unsigned int Ntotal = 0;
    struct ip_uint32_rcp rcp = ip_urcp32(d);

    printf("Test unsigned division by %u ...", d);
    for (int n=nstart; n<nend; n+=nstep) {
	uint32_t res = ip_udiv32(n, rcp);
	uint32_t ref = n / d;
	if (res != ref) {
	    Nfails++;
	    if (verbose) {
		printf("%u / %u = %u which should be %u [rcp=%u, shift=%d]\n",
		       n, d, res, ref, rcp.m, rcp.shift);
	    }
	}
	Ntotal++;
    }
    if (Nfails) {
	printf(" FAIL [%u out of %u cases]\n", Nfails, Ntotal);
    }else
	printf(" OK [%u cases]\n", Ntotal);
    return Nfails;
}


static unsigned int test_idiv32(int32_t d, int32_t nstart, int32_t nstep, int32_t nend)
{
    unsigned int Nfails = 0;
    unsigned int Ntotal  = 0;
    struct ip_int32_rcp rcp = ip_ircp32(d);

    printf("Test signed division by %d ...", d);
    for (int n=nstart; n<nend; n+=nstep) {
	uint32_t res = ip_idiv32(n, rcp);
	uint32_t ref = n / d;
	if (res != ref) {
	    Nfails++;
	    if (verbose) {
		printf("%d / %d = %d which should be %d [rcp=%u, shift=%d]\n",
		       n, d, res, ref, rcp.m, rcp.shift);
	    }
	}
	Ntotal++;
    }
    if (Nfails) {
	printf(" FAIL [%u out of %u cases!\n", Nfails, Ntotal);
    }else
	printf(" OK [%u cases]\n", Ntotal);
    return Nfails;
}


int main(void)
{
    int Nfails = 0;
    int stepsize = 1;

    Nfails += test_udiv32(2, 0, 1, 10000);
    Nfails += test_udiv32(2, 10001, stepsize*431, UINT32_MAX-10000);
    Nfails += test_udiv32(3, 0, 1, 10000);
    Nfails += test_udiv32(3, 10001, stepsize*341, UINT32_MAX-10000);
    Nfails += test_udiv32(4, 0, 1, 10000);
    Nfails += test_udiv32(4, 10001, stepsize*645, UINT32_MAX-10000);
    Nfails += test_udiv32(5, 0, 1, 10000);
    Nfails += test_udiv32(5, 10001, stepsize*729, UINT32_MAX-10000);
    Nfails += test_udiv32(6, 0, 1, 10000);
    Nfails += test_udiv32(6, 10001, stepsize*729, UINT32_MAX-10000);
    Nfails += test_udiv32(7, 0, 1, 10000);
    Nfails += test_udiv32(7, 10001, stepsize*729, UINT32_MAX-10000);
    Nfails += test_udiv32(8, 0, 1, 10000);
    Nfails += test_udiv32(8, 10001, stepsize*729, UINT32_MAX-10000);
    Nfails += test_udiv32(31, 0, 1, 10000);
    Nfails += test_udiv32(31, 10000, stepsize*1321, UINT32_MAX-10000);
    Nfails += test_udiv32(32, 0, stepsize*921, UINT32_MAX-10000);
    Nfails += test_udiv32(33, 0, stepsize*728, UINT32_MAX-10000);
    Nfails += test_udiv32(729, 0, stepsize*495, UINT32_MAX-10000);

    Nfails += test_idiv32(2, -10000, 1, 10000);
    Nfails += test_idiv32(-2, 0, 1, 10000);
    Nfails += test_idiv32(7, -10000, 1, 10000);
    Nfails += test_idiv32(7, 10001, stepsize*79, INT32_MAX-100);
    Nfails += test_idiv32(7, -10000, 1, 10000);
    Nfails += test_idiv32(-7, 10001, stepsize*79, INT32_MAX-100);
    Nfails += test_idiv32(-7, INT32_MAX-100, 1, INT32_MAX);
    Nfails += test_idiv32(-7, INT32_MIN, 1, INT32_MIN+10000);
    Nfails += test_idiv32(7, -2125184695, 1, -2125184695+10);
    Nfails += test_idiv32(31, 0, 1, 10000);
    Nfails += test_idiv32(31, 10000, stepsize*1321, INT32_MAX-10000);
    Nfails += test_idiv32(32, INT32_MIN, stepsize*921, 1000);
    Nfails += test_idiv32(33, INT32_MIN, stepsize*728, INT32_MAX-10000);
    Nfails += test_idiv32(729, 0, stepsize*495, INT32_MAX-10000);

    if (Nfails)
	printf("There were failures.\n");
    else
	printf("OK.\n");

    return Nfails ? 1 : 0;
}
