#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <math.h>

#include <ip/types.h>
#include <ip/image.h>
#include <ip/error.h>
#include <ip/proto.h>
#include <ip/flags.h>

#include "test-macros.h"
#include "timing.h"
#include "helpers.h"
#include "errorhdl.h"

char *program_name = "immul-test";


#define PIXEL_OPERATION_MUL(dval, sval, d)  dval *= sval
#define PIXEL_OPERATION_MUL_CLIPPED(dval, sval, d)	\
    dval *= sval;					\
    CLIP_TO_PIXEL_TYPE_LIMITS(dval, d);

im_ii_int_DEFN(MUL)
im_ii_int_DEFN(MUL_CLIPPED)

im_ii_real_DEFN(MUL)

#define PIXEL_OPERATION_MUL_COMPLEX(dval, sval, d) {	\
	__typeof(dval.r) t;				\
	t = dval.r*sval.r - dval.i*sval.i;		\
	dval.i = dval.i*sval.r + dval.r*sval.i;		\
	dval.r = t;					\
    }

im_ii_complex_DEFN(MUL_COMPLEX)
im_ii_dcomplex_DEFN(MUL_COMPLEX)


long im_mul_check(int type, ip_coord2d size, int flag, char *msg)
{
    ip_image *r, *d, *s;
    int failed = 0;
    double scale;
    double offset;
    long time;

    r = ip_alloc_image(type, size);

    scale = 1.2*(ip_image_type_info[type].maxvalue 
		 - ip_image_type_info[type].minvalue) / size.x;
    offset = ip_image_type_info[type].minvalue;
    im_create(r, (ip_coord2d){0,0}, scale, offset, 1.0, TI_HWEDGE);
    d = im_copy(r);
    failed |= compare_images(r, d, "Initial compare of reference", "r", "d");

    set_image_row_padding(r, 0x53);
    failed |= check_image_row_padding(r, 0x53, "Set padding check padding");
    failed |= compare_images(r, d, "Set padding check image data", "r", "d");

    s = ip_alloc_image(type, size);
    im_create(s, (ip_coord2d){0,0}, scale, 0.0, 1.0, TI_VWEDGE);
    
    if (flag == FLG_CLIP)
	im_ii_int_MUL_CLIPPED(r, s);
    else
	im_ii_int_MUL(r, s);

    time_start();
    im_mul(d, s, flag);
    time = time_end();

    failed |= compare_images(r, d, "Image mul check", "r", "d");
    failed |= check_image_row_padding(r, 0x53, "Image mul padding check");

    printf("%s: [%ld us] %s\n", msg, time, failed ? "FAILED" : "OK");

    ip_free_image(s);
    ip_free_image(d);
    ip_free_image(r);

    return time;
}


long im_real_mul_check(int type, ip_coord2d size, int flag, char *msg)
{
    ip_image *r, *d, *s;
    int failed = 0;
    double scale;
    double offset;
    long time;

    r = ip_alloc_image(type, size);

    scale = 0.1*sqrt(0.1*ip_image_type_info[type].maxvalue
		     - 0.1*ip_image_type_info[type].minvalue) / size.x;
    offset = -0.5*scale;

    if (type_real(type)) {
	im_create(r, (ip_coord2d){0,0}, scale, offset, 1.0, TI_HWEDGE);
    } else {
	im_random(r, offset, offset+scale, FLG_RANDOM_UNIFORM);
    }
    d = im_copy(r);
    failed |= compare_images(r, d, "Initial compare of reference", "r", "d");

    set_image_row_padding(r, 0x53);
    failed |= check_image_row_padding(r, 0x53, "Set padding check padding");
    failed |= compare_images(r, d, "Set padding check image data", "r", "d");

    s = ip_alloc_image(type, size);
    if (type_real(type)) {
	im_create(s, (ip_coord2d){0,0}, scale, 0.0, 1.0, TI_VWEDGE);
    } else {
	im_random(s, offset, offset+scale, FLG_RANDOM_UNIFORM);	
    }

    if (type_real(type))
	im_ii_real_MUL(r, s);
    else if (type == IM_DCOMPLEX)
	im_ii_dcomplex_MUL_COMPLEX(r,s);
    else
	im_ii_complex_MUL_COMPLEX(r,s);

    time_start();
    im_mul(d, s, flag);
    time = time_end();

    failed |= compare_images(r, d, "Image mul check", "r", "d");
    failed |= check_image_row_padding(r, 0x53, "Image mul padding check");

    printf("%s: [%ld us] %s\n", msg, time, failed ? "FAILED" : "OK");

    ip_free_image(s);
    ip_free_image(d);
    ip_free_image(r);

    return time;
}



int main(int argc, char *argv[])
{
    ip_image *i1, *i2;
    ip_coord2d size = {1023, 1025};
    int failed = 0;

    ip_register_error_handler(error_handler);

    EXPECT_ERRORS(ERR_NOT_SAME_SIZE, ERR_NOT_SAME_TYPE);
    i1 = ip_alloc_image(IM_UBYTE, size);
    i2 = ip_alloc_image(IM_USHORT, size);
    MARK(1);
    im_mul(i1, i2, NO_FLAG);
    CHECK_SEEN_ERROR;

    ip_free_image(i2);
    i2 = ip_alloc_image(IM_UBYTE, (ip_coord2d){1000, 1000});
    MARK(2);
    im_mul(i1, i2, NO_FLAG);
    CHECK_SEEN_ERROR;

    ip_free_image(i1);
    ip_free_image(i2);

    EXPECT_ERRORS(ERR_BAD_TYPE, 0);
    i1 = ip_alloc_image(IM_PALETTE, size);
    i2 = ip_alloc_image(IM_PALETTE, size);
    MARK(3);
    im_mul(i1, i2, NO_FLAG);
    CHECK_SEEN_ERROR;

    ip_free_image(i1);
    ip_free_image(i2);

    EXPECT_ERRORS(ERR_BAD_FLAG, ERR_BAD_FLAG2);
    i1 = ip_alloc_image(IM_BYTE, size);
    i2 = ip_alloc_image(IM_BYTE, size);
    MARK(4);
    im_mul(i1, i2, FLG_REAL);
    CHECK_SEEN_ERROR;

    ip_free_image(i1);
    ip_free_image(i2);

    printf("im_mul error reporting: %s\n", failed ? "FAIL" : "OK");

    EXPECT_NO_ERRORS;

    printf("CHECKING basic pixelwise multiple code:\n");

    im_mul_check(IM_UBYTE, size, NO_FLAG, "im_mul UBYTE");
    im_mul_check(IM_UBYTE, size, FLG_CLIP, "im_mul clipped UBYTE");
    im_mul_check(IM_BYTE, size, NO_FLAG, "im_mul BYTE");
    im_mul_check(IM_BYTE, size, FLG_CLIP, "im_mul clipped BYTE");
    im_mul_check(IM_USHORT, size, NO_FLAG, "im_mul USHORT" );
    im_mul_check(IM_USHORT, size, FLG_CLIP, "im_mul clipped USHORT");
    im_mul_check(IM_SHORT, size, NO_FLAG, "im_mul SHORT" );
    im_mul_check(IM_SHORT, size, FLG_CLIP, "im_mul clipped SHORT");
    im_mul_check(IM_ULONG, size, NO_FLAG, "im_mul ULONG" );
    im_mul_check(IM_LONG, size, NO_FLAG, "im_mul LONG" );
    im_real_mul_check(IM_FLOAT, size, NO_FLAG, "im_mul FLOAT" );
    im_real_mul_check(IM_DOUBLE, size, NO_FLAG, "im_mul DOUBLE" );
    im_real_mul_check(IM_COMPLEX, size, NO_FLAG, "im_mul COMPLEX" );
    im_real_mul_check(IM_DCOMPLEX, size, NO_FLAG, "im_mul DCOMPLEX" );

    printf("CHECKING hardware (SIMD) acceleration (if available):\n");

    ip_set_global_flags(IP_HWACCEL);

    im_mul_check(IM_UBYTE, size, NO_FLAG, "im_mul UBYTE");
    im_mul_check(IM_UBYTE, size, FLG_CLIP, "im_mul clipped UBYTE");
    im_mul_check(IM_BYTE, size, NO_FLAG, "im_mul BYTE");
    im_mul_check(IM_BYTE, size, FLG_CLIP, "im_mul clipped BYTE");
    im_mul_check(IM_USHORT, size, NO_FLAG, "im_mul USHORT" );
    im_mul_check(IM_USHORT, size, FLG_CLIP, "im_mul clipped USHORT");
    im_mul_check(IM_SHORT, size, NO_FLAG, "im_mul SHORT" );
    im_mul_check(IM_SHORT, size, FLG_CLIP, "im_mul clipped SHORT");
    im_mul_check(IM_ULONG, size, NO_FLAG, "im_mul ULONG" );
    im_mul_check(IM_LONG, size, NO_FLAG, "im_mul LONG" );
    im_real_mul_check(IM_FLOAT, size, NO_FLAG, "im_mul FLOAT" );
    im_real_mul_check(IM_DOUBLE, size, NO_FLAG, "im_mul DOUBLE" );
    im_real_mul_check(IM_COMPLEX, size, NO_FLAG, "im_mul COMPLEX" );
    im_real_mul_check(IM_DCOMPLEX, size, NO_FLAG, "im_mul DCOMPLEX" );

    return 0;
}
