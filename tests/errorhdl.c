#include <stdio.h>
#include <stdlib.h>

#include <ip/types.h>
#include <ip/image.h>
#include <ip/error.h>

#include "errorhdl.h"

#ifdef DO_BACKTRACE_DUMP
#  include <execinfo.h>
#  define BACKTRACE_VARS void *btbuf[12]; int btnum
#  define BACKTRACE_DUMP btnum=backtrace(btbuf,12); putchar('\n'); backtrace_symbols_fd(btbuf,btnum,2)
#else
#  define BACKTRACE_VARS
#  define BACKTRACE_DUMP
#endif

/* It is assumed any program linking this has defined this global variable. */
extern char *program_name;

int ignore_error = -1;
int expected_error1;
int expected_error2;
int error_seen;
int expect_multiple_errors;
int mark_number;

void error_handler(ip_error_logstack *ipr, int error, const char *extra, va_list ap)
{
    BACKTRACE_VARS;

    if ((error_seen == 0 || expect_multiple_errors)
	     && (error == expected_error1 || error == expected_error2)) {
	error_seen = error;
	return;
    }

    if (error == ignore_error)
	return;

    fprintf(stderr, "COMPLETELY UNEXPECTED ERROR %d [%s] from IP library (aborting):-\n",
	    error,  ip_error_code_as_str(error));
    ip_print_error(program_name,ipr,error,extra,ap);

    BACKTRACE_DUMP;

    exit(1);
}


static void output_error_code(int mark, int err)
{
    printf("%d: Expected error %d [%s] not received!\n", mark, err, ip_error_code_as_str(err));
}


void output_error_codes(int mark, int err1, int err2)
{
    if (err1)
	output_error_code(mark, err1);
    if (err2)
	output_error_code(mark, err2);
}
