/* To get srandom() */
#define _XOPEN_SOURCE 600

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>

#include <ip/ip.h>
#include <ip/tiff.h>

#include "test-macros.h"
#include "timing.h"
#include "helpers.h"
#include "errorhdl.h"

#include "../src/internal.h"

char *program_name = "median-test";

/* Keep counts of failed and tested routines as globals */
int num_tests;
int num_failures;


#define NUM_IDS 1

/* For passing operator names to argument parsing. */
const char *op_names[NUM_IDS] = {
    "median"
};

typedef int i32_type;
typedef unsigned int u32_type;
typedef int64_t i64_type;
typedef float f32_type;
typedef double d64_type;

/*
 * Quickselect algorithm to produce median from array of specified size.
 */

#define generate_median_function(tt)					\
    static tt##_type median_##tt(tt##_type *arr, int size)		\
    {									\
	int  left, right;						\
	int  i, j, k;							\
	left  = 0;							\
	right = size - 1;						\
	k = (right+1)/2;						\
	while (right > left) {						\
	    tt##_type val;						\
	    tt##_type  t;						\
	    if (arr[right] < arr[left]) {				\
		t = arr[right];						\
		arr[right] = arr[left];					\
		arr[left] = t;						\
	    }								\
	    val = arr[right];						\
	    i = left - 1;						\
	    j = right;							\
	    do {							\
		do i++; while (arr[i] < val);				\
		do j--; while (arr[j] > val);				\
		t = arr[i];						\
		arr[i] = arr[j];					\
		arr[j] = t;						\
	    } while (j > i);						\
	    arr[j] = arr[i];						\
	    arr[i] = arr[right];					\
	    arr[right] = t;						\
	    if (i >= k) right = i - 1;					\
	    if (i <= k) left  = i + 1;					\
	}								\
	return arr[k];							\
    }

#if 0
generate_median_function(i32)
generate_median_function(u32)
generate_median_function(f32)
generate_median_function(d64)
#else
generate_median_function(i64)
generate_median_function(d64)
#endif

#if 0
static int test_median_functions(int ksize)
{
    int failed = 0;
    int size = ksize*ksize;
    int median_idx = (size+1)/2;
    int Ntimes = 64;
    i64_type *arr;

    if ((arr = malloc(size*sizeof(i64_type))) == NULL) {
	fprintf(stderr, "Out of memory.... aborting.\n");
	exit(1);
    }

    for (int k = 0; k<Ntimes; k++) {
	int64_t median;
	int Nless, Nmore;

	for (int j=0; j<size; j++) {
	    arr[j] = random();
	    if (random() & 0x10000)
		arr[j] = -arr[j];
	}

	median = median_i64(arr, size);

	Nless = Nmore = 0;
	for (int j=0; j<size; j++) {
	    if (arr[j] < median) Nless++;
	    else if (arr[j] > median) Nmore++;
	}

	if (Nless >= median_idx || Nmore >= median_idx)
	    failed++;
    }

    printf("Internal i64 median check: ");
    if (failed)
	printf("failed %d/%d times             [FAIL]\n", failed, Ntimes);
    else
	printf("                               [OK]\n");

    return failed;
}
#endif



static char *flag_str(int flag)
{
    static char str[10];

    if (flag == FLG_ZEROEXTEND)
	return "zeroextend";
    else if (flag == FLG_EDGEEXTEND)
	return "edgeextend";
    else if (flag == NO_FLAG)
	return "noflag";
    else {
	snprintf(str, 10, "%08x", flag);
	return str;
    }
}


static void save_image(ip_image *im, char *msg, int flag)
{
    char fn[128];

    snprintf(fn, 128, "median-%s-%s-%s.tiff", ip_type_name(im->type), flag_str(flag), msg);
    write_tiff_image(fn, im);
}



static int load_array(int64_t *arr, ip_image *im, int i, int j, int flag, int ksize)
{
    int64_t *ap = arr;
    int hsize = ksize/2;
    int size = 0;

    if (flag == FLG_EDGEEXTEND) {
	for (int l=-hsize; l<=hsize; l++) {
	    for (int k=-hsize; k<=hsize; k++) {
		int64_t val;
		int x = i + k;
		int y = j + l;
		if (x < 0) x = 0;
		if (x >= im->size.x) x = im->size.x-1;
		if (y < 0) y = 0;
		if (y >= im->size.y) y = im->size.y-1;
		GET_PIXEL_VALUE_AS_SIGNED_LONG(val, im, x, y);
		*ap++ = val;
		size++;
	    }
	}
    }else if (flag == FLG_ZEROEXTEND) {
	for (int l=-hsize; l<=hsize; l++) {
	    for (int k=-hsize; k<=hsize; k++) {
		int64_t val;
		int x = i + k;
		int y = j + l;
		if (x >= 0 && x < im->size.x && y >= 0 && y < im->size.y) {
		    GET_PIXEL_VALUE_AS_SIGNED_LONG(val, im, x, y);
		}else{
		    val = 0;
		}
		*ap++ = val;
		size++;
	    }
	}
    }else{
	for (int l=-hsize; l<=hsize; l++) {
	    for (int k=-hsize; k<=hsize; k++) {
		int x = i + k;
		int y = j + l;
		if (x >= 0 && x < im->size.x && y >= 0 && y < im->size.y) {
		    int64_t val;
		    GET_PIXEL_VALUE_AS_SIGNED_LONG(val, im, x, y);
		    *ap++ = val;
		    size++;
		}
	    }
	}
    }
#if 0
    printf("arr (%d):", size);
    for (int j=0; j<size; j++)
	printf(" %3ld", arr[j]);
    printf(" %04x %04x [%d %d]\n", flag, FLG_ZEROEXTEND, ksize, hsize);
    putchar('\n');
#endif
    return size;
}

static int load_array_real(double *arr, ip_image *im, int i, int j, int flag, int ksize)
{
    double *ap = arr;
    int hsize = ksize/2;
    int size = 0;

    if (flag == FLG_EDGEEXTEND) {
	for (int l=-hsize; l<=hsize; l++) {
	    for (int k=-hsize; k<=hsize; k++) {
		double val;
		int x = i + k;
		int y = j + l;
		if (x < 0) x = 0;
		if (x >= im->size.x) x = im->size.x-1;
		if (y < 0) y = 0;
		if (y >= im->size.y) y = im->size.y-1;
		GET_PIXEL_VALUE_AS_DOUBLE(val, im, x, y);
		*ap++ = val;
		size++;
	    }
	}
    }else if (flag == FLG_ZEROEXTEND) {
	for (int l=-hsize; l<=hsize; l++) {
	    for (int k=-hsize; k<=hsize; k++) {
		double val;
		int x = i + k;
		int y = j + l;
		if (x >= 0 && x < im->size.x && y >= 0 && y < im->size.y) {
		    GET_PIXEL_VALUE_AS_DOUBLE(val, im, x, y);
		}else{
		    val = 0;
		}
		*ap++ = val;
		size++;
	    }
	}
    }else{
	for (int l=-hsize; l<=hsize; l++) {
	    for (int k=-hsize; k<=hsize; k++) {
		int x = i + k;
		int y = j + l;
		if (x >= 0 && x < im->size.x && y >= 0 && y < im->size.y) {
		    double val;
		    GET_PIXEL_VALUE_AS_DOUBLE(val, im, x, y);
		    *ap++ = val;
		    size++;
		}
	    }
	}
    }
    return size;
}



/*
 * TBD: implement a reference median filter image generation
 */

static int im_median_naive(ip_image *im, int ksize, int flag)
{
    return OK;
}


static int im_median_check_int(ip_image *orig, ip_image *res, int ksize, int flag)
{
    int failed = 0;
    int hsize = ksize/2;
    for (int j=0; j<res->size.y; j++) {
	for (int i=0; i<res->size.x; i++) {
	    int64_t median;
	    int Nmore = 0, Nless = 0, Nsame = 0;
	    int Nmed = (ksize*ksize+1);
	    GET_PIXEL_VALUE_AS_SIGNED_LONG(median, res, i, j);
	    for (int l=-hsize; l<=hsize; l++) {
		for (int k=-hsize; k<=hsize; k++) {
		    int64_t val;
		    int x = i+k;
		    int y = j+l;
		    if (flag == FLG_EDGEEXTEND) {
			if (x < 0) x = 0;
			if (x >= orig->size.x) x = orig->size.x-1;
			if (y < 0) y = 0;
			if (y >= orig->size.y) y = orig->size.y-1;
			GET_PIXEL_VALUE_AS_SIGNED_LONG(val, orig, x, y);
			if (val > median) Nmore++;
			else if (val < median) Nless++;
			else Nsame++;
		    }else if (flag == FLG_ZEROEXTEND) {
			if (x >= 0 && y >= 0 && x < orig->size.x &&  y < orig->size.y) {
			    GET_PIXEL_VALUE_AS_SIGNED_LONG(val, orig, x, y);
			}else{
			    val = 0;
			}
			if (val > median) Nmore++;
			else if (val < median) Nless++;
			else Nsame++;
		    }else if (x >= 0 && y >=0 && x < orig->size.x &&  y < orig->size.y) {
			GET_PIXEL_VALUE_AS_SIGNED_LONG(val, orig, x, y);
			if (val > median) Nmore++;
			else if (val < median) Nless++;
			else Nsame++;
		    }else{
			Nmed--;
		    }
		}
	    }
	    /* This calculation works for both odd and even number of elements to sort */
	    Nmed--;
	    Nmed /= 2;
	    Nmed++;
	    if (Nmore >= Nmed || Nless >= Nmed || Nsame == 0) {
		/* Failure; the result median value is not the median ! */
		failed++;
		if (failed == 1) {
		    int64_t *arr = malloc(ksize*ksize*sizeof(int64_t));
		    int64_t actmed = 0;
		    if (arr) {
			int arr_size = load_array(arr, orig, i, j, flag, ksize);
			actmed = median_i64(arr, arr_size);
			free(arr);
		    }
		    printf("%s: wrong result at (%d,%d): got %ld but should be %ld [%d-%d,%d,%d].\n",
			   "median", i, j, median, actmed, Nmed, Nless, Nsame, Nmore);
		}
	    }
	}
    }
    if (failed) {
	printf("...num incorrect pixel = %d\n", failed);
    }

    return failed ? 1 : 0;
}

static int im_median_check_real(ip_image *orig, ip_image *res, int ksize, int flag)
{
    int failed = 0;
    int hsize = ksize/2;
    for (int j=0; j<res->size.y; j++) {
	for (int i=0; i<res->size.x; i++) {
	    double median;
	    int Nmore = 0, Nless = 0, Nsame = 0;
	    int Nmed = (ksize*ksize+1);
	    GET_PIXEL_VALUE_AS_DOUBLE(median, res, i, j);
	    for (int l=-hsize; l<=hsize; l++) {
		for (int k=-hsize; k<=hsize; k++) {
		    double val;
		    int x = i+k;
		    int y = j+l;
		    if (flag == FLG_EDGEEXTEND) {
			if (x < 0) x = 0;
			if (x >= orig->size.x) x = orig->size.x-1;
			if (y < 0) y = 0;
			if (y >= orig->size.y) y = orig->size.y-1;
			GET_PIXEL_VALUE_AS_DOUBLE(val, orig, x, y);
			if (val > median) Nmore++;
			else if (val < median) Nless++;
			else Nsame++;
		    }else if (flag == FLG_ZEROEXTEND) {
			if (x >= 0 && y >= 0 && x < orig->size.x &&  y < orig->size.y) {
			    GET_PIXEL_VALUE_AS_DOUBLE(val, orig, x, y);
			}else{
			    val = 0.0;
			}
			if (val > median) Nmore++;
			else if (val < median) Nless++;
			else Nsame++;
		    }else if (x >= 0 && y >=0 && x < orig->size.x &&  y < orig->size.y) {
			GET_PIXEL_VALUE_AS_DOUBLE(val, orig, x, y);
			if (val > median) Nmore++;
			else if (val < median) Nless++;
			else Nsame++;
		    }else{
			Nmed--;
		    }
		}
	    }
	    /* This calculation works for both odd and even number of elements to sort */
	    Nmed--;
	    Nmed /= 2;
	    Nmed++;
	    if (Nmore >= Nmed || Nless >= Nmed || Nsame == 0) {
		/* Failure; the result median value is not the median ! */
		failed++;
		if (failed == 1) {
		    double *arr = malloc(ksize*ksize*sizeof(double));
		    double actmed = 0;
		    if (arr) {
			int arr_size = load_array_real(arr, orig, i, j, flag, ksize);
			actmed = median_d64(arr, arr_size);
			free(arr);
		    }
		    printf("%s: wrong result at (%d,%d): got %g but should be %g [%d-%d,%d,%d].\n",
			   "median", i, j, median, actmed, Nmed, Nless, Nsame, Nmore);
		}
	    }
	}
    }
    if (failed) {
	printf("...num incorrect pixel = %d\n", failed);
    }

    return failed ? 1 : 0;
}



static int im_median_check(int op, int type, ip_coord2d size, int ksize, int flags, int chk_flag)
{
    ip_image *orig, *d, *r;
    int failed = 0;
    unsigned char *chksum = NULL;
    uint64_t key = KEY_CONSTRUCT(1, type, 0, flags);

    orig = create_test_image(type, size, (chk_flag & DO_RANDOM));
    d = im_copy(orig);
    r = im_copy(d);

    if (chk_flag & DO_SAVE) {
        save_image(d, "original", flags);
    }

    im_median(d, ksize, flags);


    if (chk_flag & DO_SAVE) {
        save_image(d, "result", flags);
    }

    /*
     * We can check that median is correct without actually calculating the
     * correct reference image to compare with.  It is both faster and
     * simpler to do it this way.  The downside is that this test cannot
     * generate a correct result image that can be saved with the DO_SAVE
     * flag.
     */

    if (NOT (chk_flag & DO_NO_TEST)) {
	if (type_integer(type))
	    failed = im_median_check_int(orig, d, ksize, flags);
	else
	    failed = im_median_check_real(orig, d, ksize, flags);
	im_median_naive(r, ksize, flags);
    }

    printf("median %-8s  %10s  ", ip_type_name(type), flag_str(flags));

    /* Print out results including timing info if requested. */
    if (chk_flag & DO_CHECKSUM) {
        chksum = im_md5(d);
	print_md5(chksum);
	putchar('\n');
    }else{
	uint64_t time;

	if (chk_flag & DO_TIMING) {
	    time_start();
	    im_median(orig, ksize, flags);
	    time = time_end();
        }else{
	    time = -1LL;
	}
	print_time_status(key, cycles, time, ipc, failed);
    }

    ip_free_image(r);
    ip_free_image(d);
    ip_free_image(orig);

    /* Increment counts (global variables) */
    num_tests++;
    if (failed) num_failures++;

    return failed;
}


void run_tests_all_types(int op, int type, ip_coord2d size, int ksize, int chk_flag)
{
    int types_to_do[8] = {IM_BYTE, IM_UBYTE, IM_SHORT, IM_USHORT,
			  IM_LONG, IM_ULONG, IM_FLOAT, IM_DOUBLE};
    int flags_to_do[2] = {FLG_ZEROEXTEND, FLG_EDGEEXTEND};

    for (int tt = 0; tt < 8; tt++) {
	if (type == 0 || (types_to_do[tt] == type)) {
	    for (int ff=0; ff < 2; ff++) {
		im_median_check(op, types_to_do[tt], size, ksize, flags_to_do[ff], chk_flag);
	    }
	}
    }
}

void run_im_median_tests(int op, int type, ip_coord2d size, int ksize, int chk_flag)
{
    EXPECT_NO_ERRORS;

    if (chk_flag & DO_PIXEL) {
	printf("CHECKING basic pixelwise %s code:\n","median");;

	ip_clear_global_flags(IP_HWACCEL);

	run_tests_all_types(op, type, size, ksize, chk_flag);
    }

    /* There is currently no SIMD implementation */
    if (chk_flag & DO_SIMD) {
	printf("CHECKING hardware (SIMD) acceleration (if available):\n");

	ip_set_global_flags(IP_HWACCEL);

	run_tests_all_types(op, type, size, ksize, chk_flag);
    }
}


int main(int argc, char *argv[])
{
    int imtype;
    ip_coord2d size = {1043, 1021};
    int ksize = 3;
    struct cmd_line_requests *clr;

    clr = process_cmdline(argc, argv, NUM_IDS, op_names);

    ip_register_error_handler(error_handler);

    if (clr->flags & DO_SIZE)
	size = clr->im_size;

    if (clr->flags & DO_PARM)
	ksize = clr->op_parm;

    if (clr->op_to_test < 0)
	clr->op_to_test = 0;

    imtype = clr->im_type;

    if (ksize < 2) {
	fprintf(stderr, "ERROR: No point running test with kernel size of %d\n", ksize);
	return 127;
    }

#if 0
    /* The reference function is correct; we don't need to test */
    if (test_median_functions(ksize)) {
	fprintf(stderr, "ERROR: Internal reference median functions broken; aborting,\n");
	return 127;
    }
#endif

    run_im_median_tests(clr->op_to_test, imtype, size, ksize, clr->flags);

    print_final_stats(num_tests, num_failures, 0);

    return num_failures;
}
