/* Error handling: we set up the error handler so that it can return
 * (without aborting) on expected errors from the IP library and leave a
 * mark that the error was seen.
 *
 * Expected errors can be set up with the macros EXPECT_NO_ERRORS and 
 * EXPECT_ERRORS.
 *
 * The mark left by the error handler that an error occurred can be checked
 * with the CHECK_SEEN_ERROR macro.
 *
 * If an unexpected error occurs then the error handler aborts.
 */

#include <ip/error.h>

extern int ignore_error;
extern int expected_error1;
extern int expected_error2;
extern int error_seen;
extern int mark_number;
extern int expect_multiple_errors;

#define MARK(x) mark_number=(x)
#define EXPECT_NO_ERRORS do {expected_error1 = expected_error2 = error_seen = expect_multiple_errors = 0; ignore_error = -1;} while (0)
#define EXPECT_NO_ERRORS_EXCEPT(x) do {expected_error1 = expected_error2 = error_seen = expect_multiple_errors = 0; ignore_error = x;} while (0)
#define EXPECT_ERRORS(a,b) do {error_seen = 0; expected_error1 = (a); expected_error2 = (b); expect_multiple_errors = 0; ignore_error = -1;} while (0)
#define EXPECT_MULTIPLE_ERRORS expect_multiple_errors = 1

#define CHECK_SEEN_ERROR						\
    {									\
	if (! error_seen) {						\
	    output_error_codes(mark_number, expected_error1, expected_error2); \
	    failed |= 1;						\
	}								\
	error_seen = 0;							\
    }



#define CHECK_SEEN_ERROR_WITH_MSG					\
    {									\
	if (! error_seen) {						\
	    output_error_codes(mark_number, expected_error1, expected_error2); \
	    failed |= 1;						\
	}else{								\
	    printf("received error %-21s as expected.\n",		\
	       ip_error_code_as_str(error_seen));			\
	}								\
	error_seen = 0;							\
    }


extern void output_error_codes(int mark, int err1, int err2);
extern void error_handler(ip_error_logstack *ipr, int error, const char *extra, va_list ap);
