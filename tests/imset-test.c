#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>
#include <inttypes.h>
#include <float.h>
#include <math.h>
#include <unistd.h>
#include <getopt.h>

#include <ip/ip.h>
#include <ip/stats.h>
#include <ip/tiff.h>

#include "errorhdl.h"
#include "timing.h"
#include "helpers.h"

char * program_name = "imset-test";


/* A global stats object that is reused to save allocating and de-allocating
 *  one all the time
 */
ip_stats *st;


/* Keep counts of failed and tested routines as globals */
int num_tests;
int num_failures;


const char *op_names[] = {
    "im_clear",
    "im_set",
    "im_set_av"
};

#define NUM_OPS 3

/* The list of flagged error tests that we run */
struct errtest {
    int type;
    double parm;
    int flags;
    int exp_err1, exp_err2;
};

struct errtest error_tests[] = {
    {IM_TYPEMAX, 0,             0,            ERR_INVALID_IMAGE, 0},
    {IM_BINARY, -1,             0,            ERR_PARM_BAD_VALUE, 0},
    {IM_BINARY, 256,            0,            ERR_PARM_BAD_VALUE, 0},
    {IM_BYTE,   1,              FLG_SRED,     ERR_BAD_FLAG, ERR_BAD_FLAG2},
    {IM_BYTE,   1,              FLG_CLIP,     ERR_BAD_FLAG, ERR_BAD_FLAG2},
    {IM_BYTE,  -128.5001,       0,            ERR_PARM_BAD_VALUE, 0},
    {IM_BYTE,   127.9,          0,            ERR_PARM_BAD_VALUE, 0},
    {IM_UBYTE,  1,              FLG_SIMAG,    ERR_BAD_FLAG, ERR_BAD_FLAG2},
    {IM_UBYTE,  -1,             0,            ERR_PARM_BAD_VALUE, 0},
    {IM_UBYTE,  255.51,         0,            ERR_PARM_BAD_VALUE, 0},
    {IM_SHORT,  -1000,          FLG_ZEROREST, ERR_BAD_FLAG, ERR_BAD_FLAG2},
    {IM_SHORT,  INT16_MIN-1.0,  0,            ERR_PARM_BAD_VALUE, 0},
    {IM_SHORT,  INT16_MAX+1.0,  0,            ERR_PARM_BAD_VALUE, 0},
    {IM_USHORT, 1000,           FLG_SALPHA,   ERR_BAD_FLAG, ERR_BAD_FLAG2},
    {IM_USHORT, -1,             0,            ERR_PARM_BAD_VALUE, 0},
    {IM_USHORT, UINT16_MAX+1,   0,            ERR_PARM_BAD_VALUE, 0},
    {IM_LONG,  -1000,           FLG_ZEROREST, ERR_BAD_FLAG, ERR_BAD_FLAG2},
    {IM_LONG,  INT32_MIN-1.0,   0,            ERR_PARM_BAD_VALUE, 0},
    {IM_LONG,  INT32_MAX+1.0,   0,            ERR_PARM_BAD_VALUE, 0},
    {IM_ULONG, 1000,            FLG_SALPHA,   ERR_BAD_FLAG, ERR_BAD_FLAG2},
    {IM_ULONG, -1,              0,            ERR_PARM_BAD_VALUE, 0},
    {IM_ULONG, UINT32_MAX+1.0,  0,            ERR_PARM_BAD_VALUE, 0},
    /*
     * Note that for FLOAT parm is multiplied by nextafter(FLT_MAX, DBL_MAX)
     * before the test is run.
     */
    {IM_FLOAT, -1,              0,            ERR_PARM_BAD_VALUE, 0},
    {IM_FLOAT, 1,               0,            ERR_PARM_BAD_VALUE, 0},
    {IM_RGB,   100,             FLG_SALPHA,   ERR_BAD_FLAG, ERR_BAD_FLAG2},
    {-1, 0, 0, 0, 0}
};


/*
 * The list of tests to perform on im_set()
 */
struct settests {
    int type;
    double parm;
    int flags;
};

struct settests set_tests[] = {
    {IM_BINARY,   0,           NO_FLAG},
    {IM_BINARY,   1,           NO_FLAG},
    {IM_BYTE,     INT8_MIN,    NO_FLAG},
    {IM_BYTE,     12,          NO_FLAG},
    {IM_BYTE,     INT8_MAX,    NO_FLAG},
    {IM_UBYTE,    0,           NO_FLAG},
    {IM_UBYTE,    123,         NO_FLAG},
    {IM_UBYTE,    UINT8_MAX,   NO_FLAG},
    {IM_SHORT,    INT16_MIN,   NO_FLAG},
    {IM_SHORT,    -1027,       NO_FLAG},
    {IM_SHORT,    INT16_MAX,   NO_FLAG},
    {IM_USHORT,   0,           NO_FLAG},
    {IM_USHORT,   123,         NO_FLAG},
    {IM_USHORT,   UINT16_MAX,  NO_FLAG},
    {IM_LONG,     INT32_MIN,   NO_FLAG},
    {IM_LONG,     18237456,    NO_FLAG},
    {IM_LONG,     INT32_MAX,   NO_FLAG},
    {IM_ULONG,    0,           NO_FLAG},
    {IM_ULONG,    8572561,     NO_FLAG},
    {IM_ULONG,    UINT32_MAX,  NO_FLAG},
    {IM_FLOAT,    -1.123e10,   NO_FLAG},
    {IM_FLOAT,    1823.7456,   NO_FLAG},
    {IM_FLOAT,    FLT_MAX,     NO_FLAG},
    {IM_DOUBLE,   -1.123e112,  NO_FLAG},
    {IM_DOUBLE,   91823.7456,  NO_FLAG},
    {IM_DOUBLE,   1e100,       NO_FLAG},
    {IM_PALETTE,  0,           NO_FLAG},
    {IM_PALETTE,  101,         NO_FLAG},
    {IM_PALETTE,  255,         NO_FLAG},
    {-1, 0, 0}
};


struct setavtests {
    int type;
    ip_anyval av;
    int flags;
};


struct setavtests set_av_tests[] = {
#ifdef IP_INCLUDE_LONG64
    {IM_LONG64,   {.ull[0] = -2646127, .ull[1]=0},       NO_FLAG},
    {IM_ULONG64,  {.ull[0] = 26461278, .ull[1]=0},       NO_FLAG},
#endif
    {IM_COMPLEX,  {.d[0] = -1.1324e3, .d[1] = 7.021e2},  NO_FLAG},
    {IM_COMPLEX,  {.d[0] = 9276.4612, .d[1] = -17250.1}, FLG_SIMAG | FLG_ZEROREST},
    {IM_COMPLEX,  {.d[0] = 92.4612,   .d[1] = 104.21},   FLG_SREAL},
    {IM_DCOMPLEX, {.d[0] = -1.1321e6, .d[1] = 8.1234e6}, NO_FLAG},
    {IM_DCOMPLEX, {.d[0] = 2364.1263, .d[1] = -7456.12}, FLG_SREAL | FLG_ZEROREST},
    {IM_DCOMPLEX, {.d[0] = -92.4,     .d[1] = 107.1},    FLG_SIMAG},
    {IM_RGB,      {.ul[0] =100, .ul[1] = 32,  .ul[2] = 210},   NO_FLAG},
    {IM_RGB,      {.ul[0] =23,  .ul[1] = 132, .ul[2] = 1},     FLG_SGREEN | FLG_ZEROREST},
    {IM_RGB,      {.ul[0] =100, .ul[1] = 32,  .ul[2] = 210},   FLG_SRED | FLG_SBLUE},
    {IM_RGBA,     {.ul[0] =100, .ul[1] = 32,  .ul[2] = 210, .ul[3] = 100}, NO_FLAG},
    {IM_RGBA,     {.ul[0] =23,  .ul[1] = 132, .ul[2] = 1,   .ul[3] = 10},  FLG_SRED | FLG_ZEROREST},
    {IM_RGBA,     {.ul[0] =100, .ul[1] = 32,  .ul[2] = 210, .ul[3] = 201}, FLG_SALPHA | FLG_SBLUE},
    {IM_RGB16,    {.ul[0] =1001,.ul[1] = 32,  .ul[2] = 45001}, NO_FLAG},
    {IM_RGB16,    {.ul[0] =8301,  .ul[1] = 43132, .ul[2] = 511},     FLG_SRED | FLG_SGREEN | FLG_ZEROREST},
    {IM_RGB16,    {.ul[0] =21100, .ul[1] = 16032, .ul[2] = 2100},  FLG_SGREEN},
    {-1, {.ull[0] = 0, .ull[1] = 0}, 0}
};

struct flag_bits {
    int bit;
    char *str;
};

#define FLG_BIT(x) { x, #x }

static struct flag_bits flag_str[] = {
    FLG_BIT(FLG_SREAL),
    FLG_BIT(FLG_SIMAG),
    FLG_BIT(FLG_SRED),
    FLG_BIT(FLG_SGREEN),
    FLG_BIT(FLG_SBLUE),
    FLG_BIT(FLG_SALPHA),
    FLG_BIT(FLG_ZEROREST),
    FLG_BIT(FLG_CLIP),
    {0, NULL }
};

static void print_verbose_flags(int flags, int width)
{
    int first = 1;
    static char msg[256];

    msg[0] = 0;
    for (int j=0; flag_str[j].bit; j++) {
	if (flags & flag_str[j].bit) {
	    if (! first) {
		strcat(msg, "|");
	    }
	    strcat(msg, flag_str[j].str);
	    first = 0;
	}
    }

    if (first)
	strcat(msg, "NO_FLAG");

    fputs(msg, stdout);
    for (int j = width - strlen(msg); j > 0; j--)
	putchar(' ');
}



void errors_flagged_check(void)
{
    ip_image *im;
    int mark = 0;
    int failed = 0;

    while (error_tests[mark].type >= 0) {
	int type = error_tests[mark].type;
	double parm = error_tests[mark].parm;

	EXPECT_ERRORS(error_tests[mark].exp_err1, error_tests[mark].exp_err2);
	MARK(mark+1);

	if (type == IM_TYPEMAX) {
	    /* Construct an invalid image object */
	    im = ip_alloc_image(IM_UBYTE, (ip_coord2d){126, 127});
	    im->type = IM_TYPEMAX;
	}else{
	    im = ip_alloc_image(type, (ip_coord2d){126, 127});
	}

	if (im) {
	    if (type == IM_FLOAT)
		parm *= nextafter(FLT_MAX, DBL_MAX);
	    im_set(im, parm, error_tests[mark].flags);
	}

	if (im->type == IM_TYPEMAX)
	    printf("BAD      ");
	else
	    printf("%-8s ", ip_type_name(type));

	if (im) {
	    printf("(%12g, ", parm);
	    print_verbose_flags(error_tests[mark].flags, 12);
	    printf(")  ");
	}else{
	    printf("                       ");
	}

	CHECK_SEEN_ERROR_WITH_MSG;

	if (im) {
	    /* Repair any damage to the image object that we made! */
	    if (im->type == IM_TYPEMAX) im->type = IM_UBYTE;
	    ip_free_image(im);
	}

	mark++;
    }

    num_tests++;
    if (failed)
	num_failures++;

    printf("Error checks                                                                                %s\n",
	   failed ? "[FAIL]": "[OK]");
}


static void save_image(ip_image *im, const char *opname, double val, int flags, const char *info)
{
    char fstr[180];

    if (flags == -1) {
	snprintf(fstr, 180, "%s-%s-%s.tiff",
		 opname, ip_type_name(im->type), info);
    }else{
	snprintf(fstr, 180, "%s-%s-%g-0x%04x-%s.tiff",
		 opname, ip_type_name(im->type), val, flags, info);
    }
    write_tiff_image(fstr, im);
}


void clear_image_check(int type, ip_coord2d size, int chk_flags)
{
    ip_image *im;
    double tmin, tmax, required_diff;
    int failed = 0;

    im = ip_alloc_image(type, size);

    printf("%-8s cleared via im_clear()", ip_type_name(type));

    if (! im) {
	/* If image failed to be allocated short-circuit out of here. */
	printf("      [failure to allocate image!]                         [FAIL]");
	num_tests++;
	num_failures++;
	return;
    }

    /* Make sure image is not cleared first */
    if (type == IM_BINARY) {
	tmin = 0.5;		/* 50% set pixels */
	tmax = 0.0;		/* value does not matter. */
    }else if (ip_type_max(type) < 1e20) {
	/* integer types */
	tmin = ip_type_min(type);
	tmax = ip_type_max(type);
    }else{
	/* floating point types */
	tmin = -1e10;
	tmax = 1e10;
    }
    im_random(im, tmin, tmax, FLG_RANDOM_UNIFORM);

    /* Clear fields in stats structure and we will check they are changed */
    st->min = st->max = 0.0;

    /* Get stats on random data */
    im_extrema(im, st, NULL);

    /* Note that on RGB images the max-min can be less than 255 hence the 150 */
    required_diff = (type == IM_BINARY) ? 1 : 150;
    if ((st->max - st->min) < required_diff) {
	/*
	 * Something's not right; either im_random() or im_extrema() failing
	 * thus we cannot be sure about our im_clear() test.
	 */
	failed = 1;
	printf("      [failure in im_random/im_extrema set up.]              ");
    }else{
	if (chk_flags & DO_SAVE) {
	    save_image(im, "imclear", 0.0, -1, "original");
	}

	im_clear(im);

	if (chk_flags & DO_SAVE) {
	    save_image(im, "imclear", 0.0, -1, "result");
	}

	/* Get stats on cleared data */
	im_extrema(im, st, NULL);

	if (st->max != 0 || st->min != 0) {
	    failed = 1;
	}
	printf("      [min = %-12g   max = %-12g]              ", st->min, st->max);
    }

    ip_free_image(im);

    num_tests++;
    if (failed) {
	num_failures++;
	printf("[FAIL]\n");
    }else{
	printf("[OK]\n");
    }
}


int set_and_check_image(ip_image *im, double val, int flags, int dotime)
{
    int failed;
    double val_for_comparison;
    unsigned char *chksum;

    fill_test_image(im, dotime);

    if (dotime & DO_SAVE) {
	save_image(im, "imset", val, flags, "original");
    }

    im_set(im, val, flags);

    if (dotime & DO_SAVE) {
	save_image(im, "imset", val, flags, "result");
    }

    if (image_type_integer(im))
	val_for_comparison = round(val);
    else if (im->type == IM_FLOAT)
	val_for_comparison = (double)((float)val);
    else
	val_for_comparison = val;

    im_extrema(im, st, NULL);
    failed =  (st->min != val_for_comparison || st->max != val_for_comparison);

    printf("%-8s set to %12g           with flags 0x%04x ",
	   ip_type_name(im->type), val, flags);

    if (dotime & DO_CHECKSUM) {
	/* Get md5 checksum of result if requested */
	chksum = im_md5(im);
	print_md5(chksum);
	putchar('\n');
    }else{
	uint64_t time;
	uint64_t key;

	if (dotime & DO_TIMING) {
	    /* Time the operator when requested */
	    time_start();
	    for (int j=0; j<10; j++)
		im_set(im, val, flags);
	    time = time_end() / 10;
	}else{
	    time = -1LL;
	}

	key = KEY_CONSTRUCT_DVAL(im->type, 0, flags & 0xffff, val);

	print_time_status(key, cycles, time, ipc, failed);
	if (failed) {
	    printf("... stats on failure: values ranged from %g to %g\n", st->min, st->max);
	}
    }

    /* Increment counts (global variables) */
    num_tests++;
    if (failed) num_failures++;

    return failed;
}



static void print_timing_or_checksum(ip_image *im, ip_anyval av, int flags, int failed, int dotime)
{
    uint64_t time;
    unsigned char *chksum;

    if (dotime & DO_CHECKSUM) {
	/* Get md5 checksum of result if requested */
	chksum = im_md5(im);
	print_md5(chksum);
	putchar('\n');
    }else{
	uint64_t key;
	double val;

	if (image_type_complex(im))
	    val = av.d[0] + av.d[1];
	else if (image_type_rgbim(im))
	    val = (double)(av.ul[0] + av.ul[1] + av.ul[2]);
	else
	    val = av.d[0];

	key = KEY_CONSTRUCT_DVAL(im->type, 0, flags & 0xffff, val);

	if (dotime & DO_TIMING) {
	    /* Time the operator when requested */
	    time_start();
	    for (int j=0; j<10; j++)
		im_set_av(im, av, flags);
	    time = time_end() / 10;
	}else{
	    time = -1LL;
	}

	print_time_status(key, cycles, time, ipc, failed);
    }
}


int set_and_check_complex_image(ip_image *im, ip_anyval av, int flags, int dotime)
{
    int failed;
    int cflag;
    ip_image *copy;

    im_random(im, 0, 1e5, FLG_RANDOM_NORMAL);
    copy = im_copy(im);

    if (dotime & DO_SAVE) {
	save_image(im, "imset", av.d[0], flags, "original");
    }

    im_set_av(im, av, flags);

    if (dotime & DO_SAVE) {
	save_image(im, "imset", av.d[0], flags, "result");
    }

    im_extrema(im, st, NULL);

    cflag = flags;
    if (cflag & FLG_ZEROREST) {
	if (! (cflag & FLG_SREAL))
	    av.d[0] = 0.0;
	if (! (cflag & FLG_SIMAG))
	    av.d[1] = 0.0;
	cflag = NO_FLAG;
    }

    if (im->type == IM_COMPLEX) {
	av.d[0] = (double)(float)av.d[0];
	av.d[1] = (double)(float)av.d[1];
    }

    if (cflag == NO_FLAG) {
	/* All fields should be exactly as given by av */
	failed = (st->cmin.r != av.d[0]) || (st->cmin.i != av.d[1])
	    || (st->cmax.r != av.d[0]) || (st->cmax.i != av.d[1]);
    }else{
	/* We check only some fields; the others must be unchanged. */
	im_sub(copy, im, NO_FLAG);
	if (cflag & FLG_SREAL)
	    failed = (st->cmin.r != av.d[0]) || (st->cmax.r != av.d[0]);
	else
	    failed = (st->cmin.i != av.d[1]) || (st->cmax.i != av.d[1]);
	im_extrema(copy, st, NULL);
	if (cflag & FLG_SREAL)
	    failed |= (st->cmin.i != 0.0) || (st->cmax.i != 0.0);
	else
	    failed |= (st->cmin.r != 0.0) || (st->cmax.r != 0.0);
    }

    printf("%-8s set to %10g+i%-10g with flags 0x%04x ",
	   ip_type_name(im->type), av.d[0], av.d[1], flags);

    print_timing_or_checksum(im, av, flags, failed, dotime);

    if (failed && !(dotime & DO_CHECKSUM)) {
	printf("... stats on failure: real (%g to %g), imaginary (%g to %g)\n",
	       st->cmin.r, st->cmax.r, st->cmin.i, st->cmax.i);
    }

    /* Increment counts (global variables) */
    num_tests++;
    if (failed) num_failures++;

    ip_free_image(copy);

    return failed;
}



int set_and_check_rgb_image(ip_image *im, ip_anyval av, int flags, int dotime)
{
    int failed;
    int cflag;
    ip_image *copy;

    im_random(im, 0, (im->type == IM_RGB) ? 255 : 65535, FLG_RANDOM_UNIFORM);
    copy = im_copy(im);

    if (dotime & DO_SAVE) {
	save_image(im, "imset", av.ul[0], flags, "original");
    }

    im_set_av(im, av, flags);

    if (dotime & DO_SAVE) {
	save_image(im, "imset", av.ul[0], flags, "result");
    }

    im_extrema(im, st, NULL);

    cflag = flags;
    if (cflag & FLG_ZEROREST) {
	if (! (cflag & FLG_SRED))
	    av.ul[0] = 0.0;
	if (! (cflag & FLG_SGREEN))
	    av.ul[1] = 0.0;
	if (! (cflag & FLG_SBLUE))
	    av.ul[2] = 0.0;
	cflag = NO_FLAG;
    }

    if (cflag == NO_FLAG) {
	/* All fields should be exactly as given by av */
	failed = (st->rmin.r != av.ul[0]) || (st->rmin.g != av.ul[1])
	    || (st->rmax.r != av.ul[0]) || (st->rmax.g != av.ul[1])
	    || (st->rmax.b != av.ul[2]) || (st->rmin.b != av.ul[2]);
    }else{
	/* We check only some fields; the others must be unchanged. */
	failed = 0;
	if (cflag & FLG_SRED)
	    failed |= (st->rmin.r != av.ul[0]) || (st->rmax.r != av.ul[0]);
	if (cflag & FLG_SGREEN)
	    failed |= (st->rmin.g != av.ul[1]) || (st->rmax.g != av.ul[1]);
	if (cflag & FLG_SBLUE)
	    failed |= (st->rmin.b != av.ul[2]) || (st->rmax.b != av.ul[2]);
	im_sub(copy, im, NO_FLAG);
	im_extrema(copy, st, NULL);
	if (! (cflag & FLG_SRED))
	    failed |= (st->rmin.r != 0.0) || (st->rmax.r != 0.0);
	if (! (cflag & FLG_SGREEN))
	    failed |= (st->rmin.g != 0.0) || (st->rmax.g != 0.0);
	if (! (cflag & FLG_SBLUE))
	    failed |= (st->rmin.b != 0.0) || (st->rmax.b != 0.0);
    }

    printf("%-8s set to (%5u,%5u,%5u)    with flags 0x%04x ",
	   ip_type_name(im->type), av.ul[0], av.ul[1], av.ul[2], flags);

    print_timing_or_checksum(im, av, flags, failed, dotime);

    if (failed && !(dotime & DO_CHECKSUM)) {
	printf("... stats on failure: red (%g to %g), green (%g to %g), blue (%g to %g)\n",
	       st->rmin.r, st->rmax.r, st->rmin.g, st->rmax.g, st->rmin.b, st->rmax.b);
    }

    /* Increment counts (global variables) */
    num_tests++;
    if (failed) num_failures++;

    ip_free_image(copy);

    return failed;
}


int set_and_check_rgba_image(ip_image *im, ip_anyval av, int flags, int dotime)
{
    int failed;
    int cflag;
    ip_image *copy;

    im_random(im, 0, 255, FLG_RANDOM_UNIFORM);
    copy = im_copy(im);

    if (dotime & DO_SAVE) {
	save_image(im, "imset", av.ul[0], flags, "original");
    }

    im_set_av(im, av, flags);

    if (dotime & DO_SAVE) {
	save_image(im, "imset", av.ul[0], flags, "result");
    }

    im_extrema(im, st, NULL);

    cflag = flags;
    if (cflag & FLG_ZEROREST) {
	if (! (cflag & FLG_SRED))
	    av.ul[0] = 0.0;
	if (! (cflag & FLG_SGREEN))
	    av.ul[1] = 0.0;
	if (! (cflag & FLG_SBLUE))
	    av.ul[2] = 0.0;
	if (! (cflag & FLG_SALPHA))
	    av.ul[3] = 0.0;
	cflag = NO_FLAG;
    }

    if (cflag == NO_FLAG) {
	/* All fields should be exactly as given by av */
	failed = (st->rmin.r != av.ul[0]) || (st->rmin.g != av.ul[1])
	    || (st->rmax.r != av.ul[0]) || (st->rmax.g != av.ul[1])
	    || (st->rmax.b != av.ul[2]) || (st->rmin.b != av.ul[2])
	    || (st->rmax.a != av.ul[3]) || (st->rmin.a != av.ul[3]);
    }else{
	/* We check only some fields; the others must be unchanged. */
	failed = 0;
	im_sub(copy, im, NO_FLAG);
	if (cflag & FLG_SRED)
	    failed |= (st->rmin.r != av.ul[0]) || (st->rmax.r != av.ul[0]);
	if (cflag & FLG_SGREEN)
	    failed |= (st->rmin.g != av.ul[1]) || (st->rmax.g != av.ul[1]);
	if (cflag & FLG_SBLUE)
	    failed |= (st->rmin.b != av.ul[2]) || (st->rmax.b != av.ul[2]);
	if (cflag & FLG_SALPHA)
	    failed |= (st->rmin.a != av.ul[3]) || (st->rmax.a != av.ul[3]);
	im_extrema(copy, st, NULL);
	if (! (cflag & FLG_SRED))
	    failed |= (st->rmin.r != 0.0) || (st->rmax.r != 0.0);
	if (! (cflag & FLG_SGREEN))
	    failed |= (st->rmin.g != 0.0) || (st->rmax.g != 0.0);
	if (! (cflag & FLG_SBLUE))
	    failed |= (st->rmin.b != 0.0) || (st->rmax.b != 0.0);
	if (! (cflag & FLG_SALPHA))
	    failed |= (st->rmin.a != 0.0) || (st->rmax.a != 0.0);
    }

    printf("%-8s set to (%3u, %3u, %3u, %3u)   with flags 0x%04x ",
	   ip_type_name(im->type), av.ul[0], av.ul[1], av.ul[2], av.ul[3], flags);

    print_timing_or_checksum(im, av, flags, failed, dotime);

    if (failed && !(dotime & DO_CHECKSUM)) {
	printf("... stats on failure: red (%g to %g), green (%g to %g), blue (%g to %g), alpha (%g to %g)\n",
	       st->rmin.r, st->rmax.r, st->rmin.g, st->rmax.g, st->rmin.b, st->rmax.b, st->rmin.a, st->rmax.a);
    }

    /* Increment counts (global variables) */
    num_tests++;
    if (failed) num_failures++;

    ip_free_image(copy);

    return failed;
}


void run_im_clear_tests(ip_coord2d size, int chk_flags)
{
    /*
     * We don't want to abort if there is not full support for some image
     * type.  Note that clear_image_check() does detect that something has
     * gone astray if the ERR_NOT_IMPLEMENTED error is flagged and prints a
     * message about a failure either in allocating the image, in the
     * im_random/im_extrema setup, or in the image not being cleared.
     */
    EXPECT_NO_ERRORS_EXCEPT(ERR_NOT_IMPLEMENTED);

    for (int type = 1; type < IM_TYPEMAX; type++) {
	clear_image_check(type, size, chk_flags);
    }
}


void run_im_set_tests(ip_coord2d size, int dotiming)
{
    ip_image *im;

    EXPECT_NO_ERRORS;

    for (int j=0; set_tests[j].type > 0; j++) {
	im = ip_alloc_image(set_tests[j].type, size);
	set_and_check_image(im, set_tests[j].parm, set_tests[j].flags, dotiming);
	ip_free_image(im);
    }
}


void run_im_set_av_tests(ip_coord2d size, int dotiming)
{
    ip_image *im;

    EXPECT_NO_ERRORS;

    for (int j=0; set_av_tests[j].type > 0; j++) {
	im = ip_alloc_image(set_av_tests[j].type, size);
	if (type_integer(im->type)) {
	    /* Will be one of ULONG64 or LONG64 */
	    printf("%-8s test not currently supported, skipping.\n", ip_type_name(im->type));
	}else if (type_complex(im->type))
	    set_and_check_complex_image(im, set_av_tests[j].av, set_av_tests[j].flags, dotiming);
	else if (im->type == IM_RGBA)
	    set_and_check_rgba_image(im, set_av_tests[j].av, set_av_tests[j].flags, dotiming);
	else
	    set_and_check_rgb_image(im, set_av_tests[j].av, set_av_tests[j].flags, dotiming);

	ip_free_image(im);
    }
}


void run_tests(int op_to_test, ip_coord2d size, int chk_flag)
{
    if (op_to_test <= 0)
	run_im_clear_tests(size, chk_flag);
    if (op_to_test < 0 || op_to_test == 1)
	run_im_set_tests(size, chk_flag);
    if (op_to_test < 0 || op_to_test == 2)
	run_im_set_av_tests(size, chk_flag);
}


int main(int argc, char *argv[])
{
    ip_coord2d size = {1025, 1023};
    int chk_flag;
    int op_to_test;
    struct cmd_line_requests *clr;

    clr = process_cmdline(argc, argv, NUM_OPS, op_names);

    chk_flag = clr->flags;
    op_to_test = clr->op_to_test;
    if (clr->flags & DO_SIZE)
        size = clr->im_size;

    ip_register_error_handler(error_handler);

    st = ip_alloc_stats(NO_FLAG);

    if (chk_flag & DO_ERROR) {
	printf("CHECKING errors are flagged:\n");
	errors_flagged_check();
    }

    if (chk_flag & DO_PIXEL) {
	printf("CHECKING generic code:\n");
	ip_clear_global_flags(IP_FAST_CODE | IP_HWACCEL);
	run_tests(op_to_test, size, chk_flag);
    }

    if (chk_flag & DO_SIMD) {
	printf("CHECKING simd code:\n");
	ip_set_global_flags(IP_HWACCEL);
	run_tests(op_to_test, size, chk_flag);
    }

    ip_free_stats(st);

    print_final_stats(num_tests, num_failures, 0);

    return num_failures;
}
