#define _XOPEN_SOURCE 600
#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include <float.h>
#include <math.h>
#include <unistd.h>
#include <getopt.h>

#include <ip/ip.h>
#include <ip/random.h>
#include <ip/stats.h>
#include <ip/hist.h>

#include "errorhdl.h"
#include "timing.h"

char *program_name = "imrandom-test";

/* Keep counts of failed and tested routines as globals */
int num_tests;
int num_failures;


#define MIN(a,b) ((a)<(b)?(a):(b))
#define MAX(a,b) ((a)>(b)?(a):(b))


int set_and_check_uniform(ip_image *im, double low, double high, int dotime)
{
    int failed = 0;
    unsigned long time;
    ip_hist *h;
    ip_stats *st;
    double z_high, z_low;
    int numbins;
    int bins_under, bins_over;
    double E_im_mean, E_im_stddev;
    double Evalue, Estddev;
    double maxdiff = 0;
    double h_mean, h_stddev, h_N;
    int Nfailed = 0;

    /* Clobber any prior efforts */
    im_clear(im);

    /* Initialise image with random numbers */
    im_random(im, low, high, FLG_RANDOM_UNIFORM);

    /* Get a histogram of the image data */
    if (im->type == IM_BYTE || im->type == IM_UBYTE) {
	z_high = MIN(high+2, ip_image_type_info[im->type].maxvalue);
	z_low = MAX(low-2, ip_image_type_info[im->type].minvalue);
	numbins = z_high - z_low + 1;
	bins_over = z_high - high;
	bins_under = low - z_low;
    }else if (type_integer(im->type)) {
	double binsize;
	/* We want approx. 1000 bins with a couple of bins extending past
	 *  the possible pixel values either side of the histogram if
	 *  possible.
	 */
	high = round(high);
	low = round(low);
	binsize = (high - low + 1.0) / 1000.0;

	if ((high + 2*binsize) <= ip_image_type_info[im->type].maxvalue)
	    z_high = round(high + 2*binsize);
	else
	    z_high = high;
	if ((low-2*binsize) > ip_image_type_info[im->type].minvalue)
	    z_low = round(low - 2*binsize);
	else
	    z_low = low;

	numbins = (z_high - z_low + 1.0) / binsize;
	bins_over = (z_high - high)/binsize;
	bins_under = (low - z_low)/binsize;
    }else{
	double binsize = (high - low) / 1000.0;
	z_high = high+2*binsize;
	z_low = low-2*binsize;
	numbins = lround((z_high - z_low) / binsize);
	bins_over = lround((z_high - high) / binsize);
	bins_under = lround((low - z_low) / binsize);
    }

    Evalue = (double)im->size.x*im->size.y / ((double)numbins - bins_over - bins_under);
    Estddev = sqrt(Evalue);

#if 0
    printf("im_random uniform from %g to %g\n", low, high);
    printf("Allocating histogram from %g to %g with %d bins.\n",
	   z_low,z_high,numbins);
    printf("There are %d bins under and %d bins over; Evalue %g, stddef %g\n\n",
	   bins_under, bins_over, Evalue, Estddev);
#endif

    h = im_hist(im, NULL, z_low, z_high, numbins);

    /* Check that we do not have values outside expected range */

    if (ip_hist_under_count(h, 0) || ip_hist_over_count(h, 0))
	failed = 1;

    for (int j=0; j<bins_under; j++)
	if (ip_hist_bin_count(h, 0, j) != 0)
	    failed = 1;

    for (int j=numbins-bins_over; j<numbins; j++)
	if (ip_hist_bin_count(h, 0, j) != 0)
	    failed = 1;

    if (failed) printf("Pixel values spilt beyond allowed range.\n");

    /* Check that the spread of pixel values is relatively flat */
    h_mean = h_stddev = 0.0;
    Nfailed = 0;
    for (int j=bins_under; j<numbins-bins_over; j++) {
	int count = ip_hist_bin_count(h, 0, j);
	if (count < (Evalue-5*Estddev) || count > (Evalue+5*Estddev)) {
	    Nfailed++;
	    printf("expected %g with stddev %g but got %d on bin %d\n",
		   Evalue, Estddev, count, j);
	}
	maxdiff = fabs(count - Evalue);
	h_mean += count;
	h_stddev += count * count;
    }
    h_N = numbins - bins_under - bins_over;
    h_stddev = sqrt((h_stddev - h_mean*h_mean/h_N)/h_N);
    h_mean /= h_N;

    if (Nfailed) {
	failed = 1;
	printf("Histogram counts outside 5 std. devs. of range on %d bins.\n",Nfailed);
    }

    if (((Evalue - h_mean)/Evalue > 0.01) || ((Estddev - h_stddev)/Estddev > 0.1)) {
	failed = 1;
	printf("Histogram stats wonky: mean %g stddev %g; expected %g and %g\n",
	       h_mean, h_stddev, Evalue, Estddev);
    }

    /* Get image stats from the histogram */
    st = ip_alloc_stats(NO_FLAG);
    ip_hist_stats(h, st);
    E_im_mean = (high + low) / 2; /* mean image pixel value */
    E_im_stddev = (high - low) / (2 * sqrt(3)); /* stddev of image pixel
						   values */
    /* Tolerate up to 1% difference from expected values */
    if ((fabs(st->mean - E_im_mean) / E_im_mean > 0.01)
	|| (fabs(st->stddev - E_im_stddev) / E_im_stddev > 0.01)) {
	failed = 1;
	printf("Image stats strange: mean %g, stddev %g; expected %g and %g.\n",
	       st->mean, st->stddev, E_im_mean, E_im_stddev);
    }

    if (failed) {
	ip_dump_hist(stdout, h, dotime & DBG_VERBOSE);
    }

    if (dotime) {
        /* Time the operator when requested */
        time_start();
        for (int j=0; j<10; j++)
	    im_random(im, low, high, FLG_RANDOM_UNIFORM);
        time = time_end() / 10;
	printf("%-6s random numbers btw %12g to %-12g maxdiff %8g [%5ld us; %4.2f ipc]   [%s]\n",
	       ip_image_type_info[im->type].name, low, high, maxdiff,
	       time, ipc, failed ? "FAIL" : "OK");
    }else{
	printf("%-6s random numbers btw %12g to %-12g maxdiff %8g                        [%s]\n",
	       ip_image_type_info[im->type].name,
	       low, high, maxdiff, failed ? "FAIL" : "OK");
    }

    ip_free_stats(st);
    ip_free_hist(h);

    /* Increment counts (global variables) */
    num_tests++;
    if (failed) num_failures++;

    return failed;
}


int main(int argc, char *argv[])
{
    ip_image *im;
    ip_coord2d size = {1025, 1023};
    int failed = 0;
    int opt;
    int dotiming;

    while ((opt = getopt(argc, argv, "th")) != -1) {
        switch (opt) {
        case 't':
            dotiming = 1;
            break;
        case 'h':
            /* Fall through */
        default:
            fprintf(stderr, "Usage: %s [-t]\n",argv[0]);
            exit(1000);
	}
    }

    ip_register_error_handler(error_handler);

    im = ip_alloc_image(IM_BYTE, size);

    /* Force some errors */
    EXPECT_ERRORS(ERR_BAD_FLAG, ERR_BAD_FLAG2);
    MARK(1);
    im_random(im, 0, 1, FLG_SRED);
    CHECK_SEEN_ERROR;
    MARK(2);
    im_random(im, 5, 10, FLG_CLIP | FLG_SREAL);
    CHECK_SEEN_ERROR;

    EXPECT_ERRORS(ERR_PARM_BAD_VALUE, 0);
    MARK(3);
    im_random(im, 0, 2000, FLG_RANDOM_UNIFORM);
    CHECK_SEEN_ERROR;
    MARK(4);
    im_random(im, -1000, 0, FLG_RANDOM_UNIFORM);
    CHECK_SEEN_ERROR;

    /* Set BYTE image to some values */
    EXPECT_NO_ERRORS;
    set_and_check_uniform(im, 0, 100, dotiming);
    set_and_check_uniform(im, -128, 127, dotiming);
    ip_free_image(im);

    /* Check UBYTE images */
    im = ip_alloc_image(IM_UBYTE, size);
    set_and_check_uniform(im, 10, 164,  dotiming);
    set_and_check_uniform(im, 0, 255, dotiming);
    ip_free_image(im);

    /*
     * For the other integer types below 1000 bins are allocated in the
     * histogram so if analysing over a smallish range (say <20000) make
     * sure the ranges checked are a multiple of 1000 bins to ensure that
     * the same number of possible pixel values go in each bin.  Otherwise
     * the test will likely fail.
     */

    /* Check SHORT image */
    im = ip_alloc_image(IM_SHORT, size);
    set_and_check_uniform(im, -2, 5997,  dotiming);
    set_and_check_uniform(im, INT16_MIN, INT16_MAX, dotiming);
    ip_free_image(im);

    /* Check USHORT image */
    im = ip_alloc_image(IM_USHORT, size);
    set_and_check_uniform(im, 2, 12001,  dotiming);
    set_and_check_uniform(im, 0, UINT16_MAX, dotiming);
    ip_free_image(im);

    /* Check LONG image */
    im = ip_alloc_image(IM_LONG, size);
    set_and_check_uniform(im, -64523, 64476,  dotiming);
    set_and_check_uniform(im, INT32_MIN, INT32_MAX, dotiming);
    ip_free_image(im);

    /* Check ULONG image */
    im = ip_alloc_image(IM_ULONG, size);
    set_and_check_uniform(im, 64523, 264522,  dotiming);
    set_and_check_uniform(im, 0, UINT32_MAX, dotiming);
    ip_free_image(im);

    /* Check FLOAT image */
    im = ip_alloc_image(IM_FLOAT, size);
    set_and_check_uniform(im, -7462.1, 2645.543,  dotiming);
    set_and_check_uniform(im, -1e-30*FLT_MAX, 1e-30*FLT_MAX, dotiming);
    ip_free_image(im);

    /* Check DOUBLE image */
    im = ip_alloc_image(IM_DOUBLE, size);
    set_and_check_uniform(im, -0.746121, 0.2645543,  dotiming);
    set_and_check_uniform(im, -1e30, 1e30, dotiming);
    ip_free_image(im);

    printf("\n%d tests performed;", num_tests);
    if (num_failures == 0)
        printf(" all passed.\n");
    else
        printf(" %d FAILURE%s\n",num_failures, num_failures == 1 ? "." : "s.");

    return num_failures;
}
