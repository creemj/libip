#include <math.h>


struct cmd_line_requests {
    int op_to_test;
    int flags;
    ip_image_type  im_type;
    ip_coord2d im_size;
    int op_parm;
    int random_seed;
};


/* Flags returned from process_cmdline() in field flags */
#define DO_RANDOM   0x01
#define DO_TIMING   0x02
#define DO_CHECKSUM 0x04
#define DO_PIXEL    0x10
#define DO_SIMD     0x20
#define DO_ERROR    0x40
#define DO_NO_TEST  0x80
#define DO_SAVE     0x100
#define DO_PARM	    0x200
#define DO_SIZE	    0x400
#define DO_TYPE     0x800
#define DO_VERBOSE  0x1000
#define DO_XPREC    0x2000
#define DO_COLOUR   0x4000

/* Flags taken as flag arg in create_test_image() */
/* DO_RANDOM as defined above */
#define DO_VWEDGE   0x02
#define DO_FTIMAGE  0x04
#define DO_VALLIMIT 0x08
#define VALLIMIT_TYPE_SHIFT 16
#define VALLIMIT_TYPE_BITS 0x1f0000

/* Colour terminal sequences */

#define COLOUR_NORMAL "\033[0m"
#define COLOUR_OK ""
#define COLOUR_WARN "\033[0;33m"
#define COLOUR_FAILED "\033[1;31m"

/* Used in argument of set_compare_behaviour() */
struct compare_flag {
    unsigned int quiet : 1;
    unsigned int noisy : 1;
    unsigned int permit_rounding_error : 1;
    double float_fuzz;
    double double_fuzz;
};

extern void set_compare_behaviour_x(struct compare_flag flag);

#define set_compare_behaviour(...) \
    set_compare_behaviour_x((struct compare_flag){.float_fuzz=NAN, .double_fuzz=NAN, __VA_ARGS__})

/* Returns point of failure in the compare returns */
extern ip_coord2d compare_failure_point;

/*
 * Mechanism to register footnotes.
 */

extern int push_footnote(char *note);
extern int push_footnote_v(char *fmt, ...);


/*
 * Construction of a unique key to use print_time_status()
 *
 * KEY_CONSTRUCT - construct key from integer values
 * KEY_CONSTUCT_DVAL - construct a key using a double value in addition to
 *    image type etc.  Not guaranteed to be unique but good enough for the
 *    tests programmed up so far.
 */

#define KEY_CONSTRUCT(type, op, extra, flags) \
    (((uint64_t)(type)<<56) | ((uint64_t)(op)<<48) | ((uint64_t)(extra)<<32) | (flags))

static inline uint64_t KEY_CONSTRUCT_DVAL(int type, int op, int flags, double val)
{
    double cval = val;

    /*
     * Construct key from type, flags and a double value.  Play with val a
     * bit to make it more likely to be unique even for values greater than
     * UINT32_MAX when reduced to 32 bits.
     */
    if (fabs(cval) > UINT32_MAX)
	cval = copysign(1024*log2(fabs(cval)), cval);

    return KEY_CONSTRUCT(type, op, flags & 0xffff, (uint32_t)cval);
}


extern struct cmd_line_requests *process_cmdline(int argc, char *argv[],
						 int num_ops, const char *opname[]);

extern ip_image *create_test_image(int type, ip_coord2d size, int flag);
extern void fill_test_image(ip_image *im, int flag);

extern unsigned char *im_md5(ip_image *im);
extern void print_md5(unsigned char *chksum);

extern int image_compatible_check(ip_image *d, ip_image *s);
extern void set_image_row_padding(ip_image *im, uint8_t val);
extern int check_image_row_padding(ip_image *im, uint8_t val, char * msg);
extern int compare_images(ip_image *im1, ip_image *im2, char *msg, char *im1_name, char *im2_name);
extern int compare_images_transposed(ip_image *im1, ip_image *im2, char *msg,
				     char *im1_name, char *im2_name);

extern void print_time_status(uint64_t key, uint64_t cycles, uint64_t time, double ipc, int failed);
extern void print_final_stats(int num_tests, int num_failures, int num_warnings);

extern void out_of_memory(void);
