/* To get srandom() */
#define _XOPEN_SOURCE 600

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <getopt.h>

#include <ip/ip.h>
#include <ip/tiff.h>

#include "test-macros.h"
#include "timing.h"
#include "helpers.h"
#include "errorhdl.h"

char *program_name = "transpose-test";

/* Keep counts of failed and tested routines as globals */
int num_tests;
int num_failures;


/*
 * Logical operators between two images
 */



struct operator_list {
    const int id;
    const char *name;
    const char *op_name;
    int (*ip_im_op)(ip_image *, ip_image *);
    void (*f_ii_int_op)(ip_image *, ip_image *);
};

#define NUM_IDS 1

/* For passing operator names to argument parsing. */
const char *op_names[NUM_IDS];



static void save_image(ip_image *im, char *msg)
{
    char fn[128];

    snprintf(fn, 128, "transpose-%s-%s.tiff", ip_type_name(im->type), msg);
    write_tiff_image(fn, im);
}


static int im_op_ii_check(int type, ip_coord2d size, int chk_flag, int chk_type)
{
    ip_image *r, *d;
    int failed = 0;
    unsigned char *chksum;

    if (chk_type > 0 && chk_type != type)
	return 0;

    r = create_test_image(type, size, (chk_flag & DO_RANDOM));

    if (chk_flag & DO_SAVE) {
        save_image(r, "original");
    }

    set_image_row_padding(r, 0x53);
    failed |= check_image_row_padding(r, 0x53, "Set padding check padding");

    d = im_transpose(NULL, r);

    if (chk_flag & DO_SAVE) {
        save_image(d, "result");
    }

    /* Compare d against r.  They should be exactly the same! */

    failed |= compare_images_transposed(r, d, "Image transpose check", "r", "d");

    /* Get md5 checksum of result if requested */
    if (chk_flag & DO_CHECKSUM) {
        chksum = im_md5(d);
    }

    printf("transpose %-8s    ", ip_type_name(type));

    /* Print out comparison result including timing info if requested. */
    if (chk_flag & DO_CHECKSUM) {
	print_md5(chksum);
	putchar('\n');
    }else{
	uint64_t time, key;

	if (chk_flag & DO_TIMING) {
	    time_start();
	    for (int j=0; j<10; j++) {
		im_transpose(d, r);
	    }
	    time = time_end() / 10;
        }else{
	    time = -1LL;
	}

	key = KEY_CONSTRUCT(type, 0, 0, 0);
	print_time_status(key, cycles, time, ipc, failed);
    }

    ip_free_image(d);
    ip_free_image(r);

    /* Increment counts (global variables) */
    num_tests++;
    if (failed) num_failures++;

    return failed;
}


void run_im_ii_tests(ip_coord2d size, int chk_flag, int chktype)
{
    EXPECT_NO_ERRORS;

    if (chk_flag & DO_PIXEL) {
	printf("CHECKING basic pixelwise %s code:\n","tranpose");;

	ip_clear_global_flags(IP_HWACCEL);

	im_op_ii_check(IM_BINARY, size, chk_flag, chktype);
	im_op_ii_check(IM_UBYTE, size, chk_flag, chktype);
	im_op_ii_check(IM_BYTE, size, chk_flag, chktype);
	im_op_ii_check(IM_USHORT, size, chk_flag, chktype);
	im_op_ii_check(IM_SHORT, size, chk_flag, chktype);
	im_op_ii_check(IM_ULONG, size, chk_flag, chktype);
	im_op_ii_check(IM_LONG, size, chk_flag, chktype);
	im_op_ii_check(IM_FLOAT, size, chk_flag, chktype);
	im_op_ii_check(IM_DOUBLE, size, chk_flag, chktype);
	im_op_ii_check(IM_COMPLEX, size, chk_flag, chktype);
	im_op_ii_check(IM_DCOMPLEX, size, chk_flag, chktype);
	im_op_ii_check(IM_RGBA, size, chk_flag, chktype);
    }

    if (chk_flag & DO_SIMD) {
	printf("CHECKING hardware (SIMD) acceleration (if available):\n");

	ip_set_global_flags(IP_HWACCEL);

	im_op_ii_check(IM_BINARY, size, chk_flag, chktype);
	im_op_ii_check(IM_UBYTE, size, chk_flag, chktype);
	im_op_ii_check(IM_BYTE, size, chk_flag, chktype);
	im_op_ii_check(IM_USHORT, size, chk_flag, chktype);
	im_op_ii_check(IM_SHORT, size, chk_flag, chktype);
	im_op_ii_check(IM_ULONG, size, chk_flag, chktype);
	im_op_ii_check(IM_LONG, size, chk_flag, chktype);
	im_op_ii_check(IM_FLOAT, size, chk_flag, chktype);
	im_op_ii_check(IM_DOUBLE, size, chk_flag, chktype);
	im_op_ii_check(IM_COMPLEX, size, chk_flag, chktype);
	im_op_ii_check(IM_DCOMPLEX, size, chk_flag, chktype);
	im_op_ii_check(IM_RGBA, size, chk_flag, chktype);
    }
}

#if 0
int run_im_ii_error_reporting_tests(int ip_coord2d size)
{
    ip_image *i1, *i2;
    int failed = 0;

    printf("CHECKING errors reported correctly:\n");

    if (operator == IM_NOT) {

	EXPECT_ERRORS(ERR_BAD_TYPE, 0);
	i1 = ip_alloc_image(IM_COMPLEX, size);

	/* Should generate ERR_BAD_TYPE */
	MARK(3);
	im_not(i1);
	CHECK_SEEN_ERROR;

	ip_free_image(i1);

    }else{

	EXPECT_ERRORS(ERR_NOT_SAME_SIZE, ERR_NOT_SAME_TYPE);
	i1 = ip_alloc_image(IM_UBYTE, size);
	i2 = ip_alloc_image(IM_USHORT, size);

	/* Should generate ERR_NOT_SAME_TYPE */
	MARK(1);
	operators[operator].ip_im_op(i1, i2);
	CHECK_SEEN_ERROR;

	ip_free_image(i2);
	i2 = ip_alloc_image(IM_UBYTE, (ip_coord2d){1000, 1000});

	/* Should generate ERR_NOT_SAME_SIZE */
	MARK(2);
	operators[operator].ip_im_op(i1, i2);
	CHECK_SEEN_ERROR;

	ip_free_image(i1);
	ip_free_image(i2);

	EXPECT_ERRORS(ERR_BAD_TYPE, 0);
	i1 = ip_alloc_image(IM_PALETTE, size);
	i2 = ip_alloc_image(IM_PALETTE, size);

	/* Should generate ERR_BAD_TYPE */
	MARK(3);
	operators[operator].ip_im_op(i1, i2);
	CHECK_SEEN_ERROR;

	ip_free_image(i1);
	ip_free_image(i2);

    }

    num_tests++;
    if (failed) num_failures++;
    printf("%-6s error reporting:                         [%s]\n",
	   operators[operator].name, failed ? "FAIL" : "OK");

    return failed;
}
#endif


int main(int argc, char *argv[])
{
    ip_coord2d size = {1051, 1021};
    struct cmd_line_requests *clr;

    op_names[0] = "transpose";

    clr = process_cmdline(argc, argv, NUM_IDS, op_names);

    ip_register_error_handler(error_handler);

    if (clr->flags & DO_SIZE)
	size = clr->im_size;

    run_im_ii_tests(size, clr->flags, clr->im_type);

    print_final_stats(num_tests, num_failures, 0);

    return num_failures;
}
