#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <limits.h>

#include <ip/ip.h>
#include <ip/filter.h>
#include <ip/png.h>
#include <ip/tiff.h>

#include "../src/filterint.h"

#include "timing.h"
#include "errorhdl.h"
#include "helpers.h"
#include "test-macros.h"

char *program_name = "filter-test";

char *kernel_type_names[] = {
    "UNKNOWN",
    [IP_KERNEL_FORWARD_DIFF] = "FORWARD_DIFF",
    [IP_KERNEL_BACKWARD_DIFF] = "BACKWARD_DIFF",
    [IP_KERNEL_CENTRE_DIFF] = "CENTRE_DIFF",
    [IP_KERNEL_LAPLACE_DIFF] = "LAPLACE_DIFF",
    [IP_KERNEL_SOBEL] = "SOBEL",
    [IP_KERNEL_PREWITT] = "PREWITT",
    [IP_KERNEL_MEAN] = "MEAN",
    [IP_KERNEL_GAUSSIAN] = "GAUSSIAN",
};

struct operator_list {
    char *name;
    int kernid;
};

#define NUM_OPS 15

struct operator_list operators[NUM_OPS] = {
    {"fdiff_h", IP_KERNEL_FORWARD_DIFF | IP_KERNEL_HORIZONTAL},
    {"fdiff_v", IP_KERNEL_FORWARD_DIFF | IP_KERNEL_VERTICAL},
    {"bdiff_h", IP_KERNEL_BACKWARD_DIFF | IP_KERNEL_HORIZONTAL},
    {"bdiff_v", IP_KERNEL_BACKWARD_DIFF | IP_KERNEL_VERTICAL},
    {"cdiff_h", IP_KERNEL_CENTRE_DIFF | IP_KERNEL_HORIZONTAL},
    {"cdiff_v", IP_KERNEL_CENTRE_DIFF | IP_KERNEL_VERTICAL},
    {"ldiff_h", IP_KERNEL_LAPLACE_DIFF | IP_KERNEL_HORIZONTAL},
    {"ldiff_v", IP_KERNEL_LAPLACE_DIFF | IP_KERNEL_VERTICAL},
    {"sobel_h", IP_KERNEL_SOBEL | IP_KERNEL_HORIZONTAL},
    {"sobel_v", IP_KERNEL_SOBEL | IP_KERNEL_VERTICAL},
    {"prewitt_h", IP_KERNEL_PREWITT | IP_KERNEL_HORIZONTAL},
    {"prewitt_v", IP_KERNEL_PREWITT | IP_KERNEL_VERTICAL},
    {"mean_h", IP_KERNEL_MEAN | IP_KERNEL_HORIZONTAL},
    {"mean_v", IP_KERNEL_MEAN | IP_KERNEL_VERTICAL},
    {"mean", IP_KERNEL_MEAN | IP_KERNEL_2D}
};

const char *op_names[NUM_OPS];

/* Keep counts of failed and tested routines as globals */
int num_tests;
int num_failures;


/*
 * Naive implementations of the filter operations
 */

static void baseline_filter_int(ip_image *dest, ip_image *src, ip_kernel *kern)
{
    int ox, oy;

    ox = -(((kern->size.x + 1) / 2) - 1);
    oy = -(((kern->size.y + 1) / 2) - 1);
    if (kern->type & KERNEL_BACKWARDS) {
	if ((kern->type & 0xff) & KERNEL_HORIZONTAL)
	    ox = -1;
	else
	    oy = -1;
    }

    /* For each pixel in image */
    for (int j=0; j<src->size.y; j++) {
        for (int i=0; i<src->size.x; i++) {
	    int accum = 0;
	    /* For each element in kernel */
	    for (int l=0; l<kern->size.y; l++) {
		for (int k=0; k<kern->size.x; k++) {
		    int64_t pval;
		    int xx = ox+i+k;
		    int yy = oy+j+l;
		    int kidx = l*kern->size.x + k;

		    /* Effect image boundary extension */
                    if (xx < 0) xx = 0;
                    if (yy < 0) yy = 0;
                    if (xx >= src->size.x) xx = src->size.x-1;
                    if (yy >= src->size.y) yy = src->size.y-1;

		    GET_PIXEL_VALUE_AS_SIGNED_LONG(pval, src, xx, yy);
		    accum += (int)kern->filter[kidx]*(int)pval;
		}
	    }
	    SET_PIXEL_VALUE_FROM_SIGNED_LONG(dest, (int64_t)accum, i, j);
	}
    }
}

static void baseline_filter_float(ip_image *dest, ip_image *src, ip_kernel *kern)
{
    int ox, oy;

    ox = -(((kern->size.x + 1) / 2) - 1);
    oy = -(((kern->size.y + 1) / 2) - 1);
    if (kern->type & KERNEL_BACKWARDS) {
	if ((kern->type & 0xff) & KERNEL_HORIZONTAL)
	    ox = -1;
	else
	    oy = -1;
    }

    /* For each pixel in image */
    for (int j=0; j<src->size.y; j++) {
        for (int i=0; i<src->size.x; i++) {
	    double accum = 0;
	    /* For each element in kernel */
	    for (int l=0; l<kern->size.y; l++) {
		for (int k=0; k<kern->size.x; k++) {
		    double pval;
		    int xx = ox+i+k;
		    int yy = oy+j+l;
		    int kidx = l*kern->size.x + k;

		    /* Effect image boundary extension */
                    if (xx < 0) xx = 0;
                    if (yy < 0) yy = 0;
                    if (xx >= src->size.x) xx = src->size.x-1;
                    if (yy >= src->size.y) yy = src->size.y-1;

		    pval = (double)IM_PIXEL(src, xx, yy, f);
		    accum += kern->filter[kidx]*pval;
		}
	    }
	    IM_PIXEL(dest, i, j, f) = (float)accum;
	}
    }
}

static void baseline_filter_double(ip_image *dest, ip_image *src, ip_kernel *kern)
{
    int ox, oy;

    ox = -(((kern->size.x + 1) / 2) - 1);
    oy = -(((kern->size.y + 1) / 2) - 1);
    if (kern->type & KERNEL_BACKWARDS) {
	if ((kern->type & 0xff) & KERNEL_HORIZONTAL)
	    ox = -1;
	else
	    oy = -1;
    }

    /* For each pixel in image */
    for (int j=0; j<src->size.y; j++) {
        for (int i=0; i<src->size.x; i++) {
	    double accum = 0;
	    /* For each element in kernel */
	    for (int l=0; l<kern->size.y; l++) {
		for (int k=0; k<kern->size.x; k++) {
		    double pval;
		    int xx = ox+i+k;
		    int yy = oy+j+l;
		    int kidx = l*kern->size.x + k;

		    /* Effect image boundary extension */
                    if (xx < 0) xx = 0;
                    if (yy < 0) yy = 0;
                    if (xx >= src->size.x) xx = src->size.x-1;
                    if (yy >= src->size.y) yy = src->size.y-1;

		    GET_PIXEL_VALUE_AS_DOUBLE(pval, src, xx, yy);
		    accum += kern->filter[kidx]*pval;
		}
	    }
	    SET_PIXEL_VALUE_FROM_DOUBLE(dest, accum, i, j);
	}
    }
}


static void baseline_mean_filter_int(ip_image *dest, ip_image *src, ip_kernel *kern)
{
    int ox, oy;
    int ksize = kern->size.x * kern->size.y;
    int hksize = ksize/2 ;

    ox = -(((kern->size.x + 1) / 2) - 1);
    oy = -(((kern->size.y + 1) / 2) - 1);

    /* For each pixel in image */
    for (int j=0; j<src->size.y; j++) {
        for (int i=0; i<src->size.x; i++) {
	    int32_t accum = 0;
	    /* For each element in kernel */
	    for (int l=0; l<kern->size.y; l++) {
		for (int k=0; k<kern->size.x; k++) {
		    int64_t pval;
		    int xx = ox+i+k;
		    int yy = oy+j+l;

		    /* Effect image boundary extension */
                    if (xx < 0) xx = 0;
                    if (yy < 0) yy = 0;
                    if (xx >= src->size.x) xx = src->size.x-1;
                    if (yy >= src->size.y) yy = src->size.y-1;

		    GET_PIXEL_VALUE_AS_SIGNED_LONG(pval, src, xx, yy);
		    accum += (int32_t)pval;
		}
	    }
	    accum += (accum >= 0) ? hksize : -hksize;
	    accum /= ksize;
	    SET_PIXEL_VALUE_FROM_SIGNED_LONG(dest, (int64_t)accum, i, j);
	}
    }
}


static void baseline_mean_filter_ULONG(ip_image *dest, ip_image *src, ip_kernel *kern)
{
    int ox, oy;
    int ksize = kern->size.x * kern->size.y;
    int hksize = ksize/2 ;

    ox = -(((kern->size.x + 1) / 2) - 1);
    oy = -(((kern->size.y + 1) / 2) - 1);

    /* For each pixel in image */
    for (int j=0; j<src->size.y; j++) {
        for (int i=0; i<src->size.x; i++) {
	    uint32_t accum = 0;
	    /* For each element in kernel */
	    for (int l=0; l<kern->size.y; l++) {
		for (int k=0; k<kern->size.x; k++) {
		    int64_t pval;
		    int xx = ox+i+k;
		    int yy = oy+j+l;

		    /* Effect image boundary extension */
                    if (xx < 0) xx = 0;
                    if (yy < 0) yy = 0;
                    if (xx >= src->size.x) xx = src->size.x-1;
                    if (yy >= src->size.y) yy = src->size.y-1;

		    GET_PIXEL_VALUE_AS_SIGNED_LONG(pval, src, xx, yy);
		    accum += (uint32_t)pval;
		}
	    }
	    accum += hksize;
	    accum /= ksize;
	    SET_PIXEL_VALUE_FROM_SIGNED_LONG(dest, (int64_t)accum, i, j);
	}
    }
}

static void baseline_mean_filter_float(ip_image *dest, ip_image *src, ip_kernel *kern)
{
    int ox, oy;
    float ksize = (float)(kern->size.x * kern->size.y);

    ox = -(((kern->size.x + 1) / 2) - 1);
    oy = -(((kern->size.y + 1) / 2) - 1);

    /* For each pixel in image */
    for (int j=0; j<src->size.y; j++) {
        for (int i=0; i<src->size.x; i++) {
	    float accum;

	    accum = 0;
	    /* For each element in kernel */
	    for (int l=0; l<kern->size.y; l++) {
		for (int k=0; k<kern->size.x; k++) {
		    float pval;
		    int xx = ox+i+k;
		    int yy = oy+j+l;

		    /* Effect image boundary extension */
                    if (xx < 0) xx = 0;
                    if (yy < 0) yy = 0;
                    if (xx >= src->size.x) xx = src->size.x-1;
                    if (yy >= src->size.y) yy = src->size.y-1;

		    pval = IM_PIXEL(src, xx, yy, f);
		    accum += pval;
		}
	    }
	    accum /= ksize;
	    IM_PIXEL(dest, i, j, f) = accum;
	}
    }
}


static void baseline_mean_filter_double(ip_image *dest, ip_image *src, ip_kernel *kern)
{
    int ox, oy;
    double ksize = (double)(kern->size.x * kern->size.y);

    ox = -(((kern->size.x + 1) / 2) - 1);
    oy = -(((kern->size.y + 1) / 2) - 1);

    /* For each pixel in image */
    for (int j=0; j<src->size.y; j++) {
        for (int i=0; i<src->size.x; i++) {
	    double accum;

	    accum = 0;
	    /* For each element in kernel */
	    for (int l=0; l<kern->size.y; l++) {
		for (int k=0; k<kern->size.x; k++) {
		    double pval;
		    int xx = ox+i+k;
		    int yy = oy+j+l;

		    /* Effect image boundary extension */
                    if (xx < 0) xx = 0;
                    if (yy < 0) yy = 0;
                    if (xx >= src->size.x) xx = src->size.x-1;
                    if (yy >= src->size.y) yy = src->size.y-1;

		    pval = IM_PIXEL(src, xx, yy, d);
		    accum += pval;
		}
	    }
	    accum /= ksize;
	    IM_PIXEL(dest, i, j, d) = accum;
	}
    }
}


static void baseline_filter(ip_image *dest, ip_image *src, ip_kernel *kern)
{
    if ((kern->type & 0xff) == KERNEL_MEAN) {
	if (src->type == IM_FLOAT)
	    baseline_mean_filter_float(dest, src, kern);
	else if (src->type == IM_DOUBLE)
	    baseline_mean_filter_double(dest, src, kern);
	else if (src->type == IM_ULONG)
	    baseline_mean_filter_ULONG(dest, src, kern);
	else
	    baseline_mean_filter_int(dest, src, kern);
    }else{
	if (type_real(src->type))
	    if (src->type == IM_FLOAT)
		baseline_filter_float(dest, src, kern);
	    else
		baseline_filter_double(dest, src, kern);
	else
	    baseline_filter_int(dest, src, kern);
    }
}



int im_filter_check(int type, ip_coord2d size, int ktype, int parm, int source)
{
    ip_image *r, *d;
    int failed = 0;
    unsigned char *chksum;
    ip_kernel *kern;
    char str[64];
    char fstr[128];
    char *gmsg;

    int gtype = ktype & IP_KERNEL_GTYPE_MASK;
    if (gtype == IP_KERNEL_VERTICAL)
	gmsg = "Vertical";
    else if (gtype == IP_KERNEL_HORIZONTAL)
	gmsg = "Horizontal";
    else if (gtype == IP_KERNEL_2D) {
	gmsg = "2D";
    }else
	gmsg = "UNKONWN";

    kern = ip_alloc_kernel(ktype, parm, parm);
    ktype &= 0xf;

    d = create_test_image(type, size, (source & DO_RANDOM) | 0x04);
    r = ip_alloc_image(type, size);

    if (source & DO_SAVE) {
	sprintf(fstr, "filter-image-%s-%s-%s-original.tiff",
		kernel_type_names[ktype], gmsg, ip_image_type_info[type].name);
	write_tiff_image(fstr, d);
    }

    if (NOT (source & DO_NO_TEST))
	baseline_filter(r, d, kern);

    im_filter(d, kern, NO_FLAG);

    if (source & DO_SAVE) {
	sprintf(fstr, "filter-image-%s-%s-%s-reference.tiff",
		kernel_type_names[ktype], gmsg, ip_image_type_info[type].name);
	write_tiff_image(fstr, r);
	sprintf(fstr, "filter-image-%s-%s-%s-filtered.tiff",
		kernel_type_names[ktype], gmsg, ip_image_type_info[type].name);
	write_tiff_image(fstr, d);
    }

    /* Get md5 checksum of result if requested */
    if (source & DO_CHECKSUM) {
        chksum = im_md5(d);
    }

    sprintf(str, "Filter %s check", kernel_type_names[ktype]);
    if (NOT (source & DO_NO_TEST))
	failed |= compare_images(r, d, str, "r", "d");

    /* Message of the operator and type of image tested */
    printf("%14s (%10s) %-7s", kernel_type_names[ktype],  gmsg,
	   ip_image_type_info[type].name);

    if (source & DO_CHECKSUM) {
	print_md5(chksum);
	putchar('\n');
    }else{
	uint64_t time;
	uint64_t key = KEY_CONSTRUCT(type, 0, ktype | gtype, parm);
	/* Print out comparison result including timing info if requested. */
	if (source & DO_TIMING) {
	    /* Time the operator when requested */
	    time_start();
	    for (int j=0; j<10; j++)
		im_filter(d, kern, NO_FLAG);
	    time = time_end() / 10;
	}else{
	    time = -1LL;
	}
	print_time_status(key, cycles, time, ipc, failed);
    }

    ip_free_image(d);
    ip_free_image(r);
    ip_free_kernel(kern);

    /* Increment counts (global variables) */
    num_tests++;
    if (failed) num_failures++;

    return failed;
}


static void run_some_tests_bytype(ip_coord2d size, int type, int op, int parm, int chk_flag)
{
    int kern_type = operators[op].kernid;

    if (type == 0) {
	im_filter_check(IM_BYTE, size, kern_type, parm, chk_flag);
	im_filter_check(IM_UBYTE, size, kern_type, parm, chk_flag);
	im_filter_check(IM_SHORT, size, kern_type, parm, chk_flag);
	im_filter_check(IM_USHORT, size, kern_type, parm, chk_flag);
	im_filter_check(IM_LONG, size, kern_type, parm, chk_flag);
	im_filter_check(IM_ULONG, size, kern_type, parm, chk_flag);
	im_filter_check(IM_FLOAT, size, kern_type, parm, chk_flag);
	im_filter_check(IM_DOUBLE, size, kern_type, parm, chk_flag);
    }else{
	im_filter_check(type, size, kern_type, parm, chk_flag);
    }
}

static void run_some_tests_byop(ip_coord2d size, int type, int op, int parm, int chk_flag)
{
    if (op < 0) {
	for (int j=0; j<NUM_OPS; j++) {
	    run_some_tests_bytype(size, type, j, parm, chk_flag);
	    if (type == 0 && (j < NUM_OPS - 1))
		printf("\n");
	}
    }else{
	run_some_tests_bytype(size, type, op, parm, chk_flag);
    }
}


int main(int argc, char *argv[])
{
    ip_coord2d size = {1023, 1025};
    int operator_to_test;
    int chk_flag;
    int imtype;
    int parm;
    struct cmd_line_requests *clr;

    for (int j=0; j<NUM_OPS; j++)
	op_names[j] = operators[j].name;

    clr = process_cmdline(argc, argv, NUM_OPS, op_names);

    operator_to_test = clr->op_to_test;
    chk_flag = clr->flags;
    imtype = clr->im_type;

    if (clr->flags & DO_SIZE)
        size = clr->im_size;

    ip_register_error_handler(error_handler);

    if (chk_flag & DO_PARM) {
	parm = clr->op_parm;
    }else
	parm = 3;

    ip_set_cache_size(1, 32, 8);

    if (chk_flag & DO_PIXEL) {
        printf("CHECKING basic pixelwise code:\n");

        EXPECT_NO_ERRORS;
        ip_clear_global_flags(IP_FAST_CODE | IP_HWACCEL);

	run_some_tests_byop(size, imtype, operator_to_test, parm, chk_flag);
    }

    if (chk_flag & DO_SIMD) {
        printf("CHECKING hardware (SIMD) acceleration (if available):\n");

        EXPECT_NO_ERRORS;
	ip_set_global_flags(IP_HWACCEL);

	run_some_tests_byop(size, imtype, operator_to_test, parm, chk_flag);
    }

    print_final_stats(num_tests, num_failures, 0);

    return num_failures;
}
