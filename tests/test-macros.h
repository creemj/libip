#include <stdlib.h>
#include <stdio.h>

/* Used in comparison routines for rgb */
struct h_rgba {
    int r, g, b, a;
};


#ifdef HAVE_LONG_LONG
#define GET_UNSIGNED_LONG_LONG(dval, dd, x, y)	\
    dval = IM_PIXEL(dd, x, y, ull);
#define GET_UNSIGNED_LONG_LONG_2(dval, sval, dd, ss, x, y)	\
    dval = IM_PIXEL(dd, x, y, ull);				\
    sval = IM_PIXEL(ss, x, y, ull);
#define SET_UNSIGNED_LONG_LONG(dd, dval, x, y)	\
    IM_PIXEL(dd, x, y, ull) = dval;
#define GET_SIGNED_LONG_LONG(dval, dd, x, y)	\
    dval = IM_PIXEL(dd, x, y, ll);
#define GET_SIGNED_LONG_LONG_2(dval, sval, dd, ss, x, y)	\
    dval = IM_PIXEL(dd, x, y, ll);				\
    sval = IM_PIXEL(ss, x, y, ll);
#define SET_SIGNED_LONG_LONG(dd, dval, x, y)	\
    IM_PIXEL(dd, x, y, ll) = dval;
#else
#define GET_UNSIGNED_LONG_LONG(dval, dd, x, y) dval = 0
#define GET_UNSIGNED_LONG_LONG_2(dval, sval, dd, ss, x, y) dval = sval  = 0
#define SET_UNSIGNED_LONG_LONG(dd, dval, x, y)
#define GET_SIGNED_LONG_LONG(dval, dd, x, y) dval = 0
#define GET_SIGNED_LONG_LONG_2(dval, sval, dd, ss, x, y) dval = sval = 0
#define SET_SIGNED_LONG_LONG(dd, dval, x, y)
#endif



#define GET_PIXEL_VALUE_AS_UNSIGNED_LONG(dval, dd, x, y)	\
    switch (dd->type) {						\
    case IM_BINARY:						\
    case IM_UBYTE:						\
	dval = (uint64_t)IM_PIXEL(dd, x, y, ub);		\
	break;							\
    case IM_USHORT:						\
	dval = (uint64_t)IM_PIXEL(dd, x, y, us);		\
	break;							\
    case IM_ULONG:						\
	dval = (uint64_t)IM_PIXEL(dd, x, y, ul);		\
	break;							\
    case IM_ULONG64:						\
	GET_UNSIGNED_LONG_LONG(dval, dd, x, y);			\
	break;							\
    default:							\
	dval = 0;						\
	break;							\
    }

#define GET_PIXEL_VALUE_AS_UNSIGNED_LONG_2(dval, sval, dd, ss, x, y)	\
    switch (dd->type) {							\
    case IM_BINARY:							\
    case IM_UBYTE:							\
	dval = (uint64_t)IM_PIXEL(dd, x, y, ub);			\
	sval = (uint64_t)IM_PIXEL(ss, x, y, ub);			\
	break;								\
    case IM_USHORT:							\
	dval = (uint64_t)IM_PIXEL(dd, x, y, us);			\
	sval = (uint64_t)IM_PIXEL(ss, x, y, us);			\
	break;								\
    case IM_ULONG:							\
	dval = (uint64_t)IM_PIXEL(dd, x, y, ul);			\
	sval = (uint64_t)IM_PIXEL(ss, x, y, ul);			\
	break;								\
    case IM_ULONG64:							\
	GET_UNSIGNED_LONG_LONG_2(dval, sval, dd, ss, x, y);		\
	break;								\
    default:								\
	dval = sval = 0;						\
	break;								\
    }

#define GET_PIXEL_VALUE_AS_SIGNED_LONG(dval, dd, x, y)	\
    switch (dd->type) {					\
    case IM_BINARY:					\
    case IM_UBYTE:					\
	dval = (int64_t)IM_PIXEL(dd, x, y, ub);		\
	break;						\
    case IM_USHORT:					\
	dval = (int64_t)IM_PIXEL(dd, x, y, us);		\
	break;						\
    case IM_ULONG:					\
	dval = (int64_t)IM_PIXEL(dd, x, y, ul);		\
	break;						\
    case IM_BYTE:					\
	dval = (int64_t)IM_PIXEL(dd, x, y, b);		\
	break;						\
    case IM_SHORT:					\
	dval = (int64_t)IM_PIXEL(dd, x, y, s);		\
	break;						\
    case IM_LONG:					\
	dval = (int64_t)IM_PIXEL(dd, x, y, l);		\
	break;						\
    case IM_LONG64:					\
	GET_SIGNED_LONG_LONG(dval, d, x, y);		\
	break;						\
    default:						\
	dval = 0;					\
	break;						\
    }


#define GET_PIXEL_VALUE_AS_SIGNED_LONG_2(dval, sval, dd, ss, x, y)	\
    switch (dd->type) {							\
    case IM_BINARY:							\
    case IM_UBYTE:							\
	dval = (int64_t)IM_PIXEL(dd, x, y, ub);				\
	sval = (int64_t)IM_PIXEL(ss, x, y, ub);				\
	break;								\
    case IM_USHORT:							\
	dval = (int64_t)IM_PIXEL(dd, x, y, us);				\
	sval = (int64_t)IM_PIXEL(ss, x, y, us);				\
	break;								\
    case IM_ULONG:							\
	dval = (int64_t)IM_PIXEL(dd, x, y, ul);				\
	sval = (int64_t)IM_PIXEL(ss, x, y, ul);				\
	break;								\
    case IM_BYTE:							\
	dval = (int64_t)IM_PIXEL(dd, x, y, b);				\
	sval = (int64_t)IM_PIXEL(ss, x, y, b);				\
	break;								\
    case IM_SHORT:							\
	dval = (int64_t)IM_PIXEL(dd, x, y, s);				\
	sval = (int64_t)IM_PIXEL(ss, x, y, s);				\
	break;								\
    case IM_LONG:							\
	dval = (int64_t)IM_PIXEL(dd, x, y, l);				\
	sval = (int64_t)IM_PIXEL(ss, x, y, l);				\
	break;								\
    case IM_LONG64:							\
	GET_SIGNED_LONG_LONG_2(dval, sval, dd, ss, x, y);		\
	break;								\
    default:								\
	dval = sval = 0;						\
	break;								\
    }

#define GET_PIXEL_VALUE_AS_DOUBLE(dval, dd, x, y)	\
    switch (dd->type) {					\
    case IM_BINARY:					\
    case IM_UBYTE:					\
	dval = (double)IM_PIXEL(dd, x, y, ub);		\
	break;						\
    case IM_BYTE:					\
	dval = (double)IM_PIXEL(dd, x, y, b);		\
	break;						\
    case IM_USHORT:					\
	dval = (double)IM_PIXEL(dd, x, y, us);		\
	break;						\
    case IM_SHORT:					\
	dval = (double)IM_PIXEL(dd, x, y, s);		\
	break;						\
    case IM_ULONG:					\
	dval = (double)IM_PIXEL(dd, x, y, ul);		\
	break;						\
    case IM_LONG:					\
	dval = (double)IM_PIXEL(dd, x, y, l);		\
	break;						\
    case IM_FLOAT:					\
	dval = (double)IM_PIXEL(dd, x, y, f);		\
	break;						\
    case IM_DOUBLE:					\
	dval = IM_PIXEL(dd, x, y, d);			\
	break;						\
    default:						\
	dval = 0.0;					\
	break;						\
    }

#define GET_PIXEL_VALUE_AS_DOUBLE_2(dval, sval, dd, ss, x, y)	\
    switch (dd->type) {						\
    case IM_FLOAT:						\
	dval = (double)IM_PIXEL(dd, x, y, f);			\
	sval = (double)IM_PIXEL(ss, x, y, f);			\
	break;							\
    case IM_DOUBLE:						\
	dval = IM_PIXEL(dd, x, y, d);				\
	sval = IM_PIXEL(ss, x, y, d);				\
	break;							\
    default:							\
	dval = sval = 0.0;					\
	break;							\
    }

#define GET_PIXEL_VALUE_AS_DCOMPLEX(dval, dd, x, y)	\
    switch (dd->type) {					\
    case IM_COMPLEX:					\
	dval.r = (double)IM_PIXEL(dd, x, y, c).r;	\
	dval.i = (double)IM_PIXEL(dd, x, y, c).i;	\
	break;						\
    case IM_DCOMPLEX:					\
	dval = IM_PIXEL(dd, x, y, dc);			\
	break;						\
    default:						\
	dval.r = dval.i = 0.0;				\
	break;						\
    }

#define GET_PIXEL_VALUE_AS_RGBA(dval, dd, x, y)		\
    switch (dd->type) {					\
    case IM_RGB:					\
	dval.r = (int)IM_PIXEL(dd, x, y, r).r;		\
	dval.g = (int)IM_PIXEL(dd, x, y, r).g;		\
	dval.b = (int)IM_PIXEL(dd, x, y, r).b;		\
	dval.a = 255;					\
	break;						\
    case IM_RGB16:					\
	dval.r = (int)IM_PIXEL(dd, x, y, r16).r;	\
	dval.g = (int)IM_PIXEL(dd, x, y, r16).g;	\
	dval.b = (int)IM_PIXEL(dd, x, y, r16).b;	\
	dval.a = 65525;					\
	break;						\
    case IM_RGBA:					\
	dval.r = (int)IM_PIXEL(dd, x, y, ra).r;		\
	dval.g = (int)IM_PIXEL(dd, x, y, ra).g;		\
	dval.b = (int)IM_PIXEL(dd, x, y, ra).b;		\
	dval.a = (int)IM_PIXEL(dd, x, y, ra).a;		\
	break;						\
    default:						\
	dval.r = dval.g = dval.b = dval.a = 0;		\
	break;						\
    }

#define GET_PIXEL_VALUE_AS_DCOMPLEX_2(dval, sval, dd, ss, x, y)	\
    switch (dd->type) {						\
    case IM_COMPLEX:						\
	dval.r = (double)IM_PIXEL(dd, x, y, c).r;		\
	dval.i = (double)IM_PIXEL(dd, x, y, c).i;		\
	sval.r = (double)IM_PIXEL(ss, x, y, c).r;		\
	sval.i = (double)IM_PIXEL(ss, x, y, c).i;		\
	break;							\
    case IM_DCOMPLEX:						\
	dval = IM_PIXEL(dd, x, y, dc);				\
	sval = IM_PIXEL(ss, x, y, dc);				\
	break;							\
    default:							\
	dval.r = dval.i = sval.r = sval.i = 0.0;		\
	break;							\
    }


#define SET_PIXEL_VALUE_FROM_UNSIGNED_LONG(dd, dval, x, y)	\
    switch (dd->type) {						\
    case IM_BINARY:						\
    case IM_UBYTE:						\
	IM_PIXEL(dd, x, y, ub) = (uint8_t)dval;			\
	break;							\
    case IM_USHORT:						\
	IM_PIXEL(dd, x, y, us) = (uint16_t)dval;		\
	break;							\
    case IM_ULONG:						\
	IM_PIXEL(dd, x, y, ul) = (uint32_t)dval;		\
	break;							\
    case IM_ULONG64:						\
	SET_UNSIGNED_LONG_LONG(dd, dval, x, y);			\
	break;							\
    default:							\
	break;							\
    }


#define SET_PIXEL_VALUE_FROM_SIGNED_LONG(dd, dval, x, y)	\
    switch (dd->type) {						\
    case IM_BINARY:						\
    case IM_UBYTE:						\
	IM_PIXEL(dd, x, y, ub) = (uint8_t)dval;			\
	break;							\
    case IM_USHORT:						\
	IM_PIXEL(dd, x, y, us) = (uint16_t)dval;		\
	break;							\
    case IM_ULONG:						\
	IM_PIXEL(dd, x, y, ul) = (uint32_t)dval;		\
	break;							\
    case IM_BYTE:						\
	IM_PIXEL(dd, x, y, b) = (int8_t)dval;			\
	break;							\
    case IM_SHORT:						\
	IM_PIXEL(dd, x, y, s) = (int16_t)dval;			\
	break;							\
    case IM_LONG:						\
	IM_PIXEL(dd, x, y, l) = (int32_t)dval;			\
	break;							\
    case IM_LONG64:						\
	SET_SIGNED_LONG_LONG(dd, dval, x, y);			\
	break;							\
    default:							\
	break;							\
    }

#define SET_PIXEL_VALUE_FROM_DOUBLE(dd, dval, x, y)		\
    switch (dd->type) {						\
    case IM_BINARY:						\
    case IM_UBYTE:						\
	IM_PIXEL(dd, x, y, ub) = (uint8_t)(int64_t)dval;	\
	break;							\
    case IM_BYTE:						\
	IM_PIXEL(dd, x, y, b) = (int8_t)(int64_t)dval;		\
	break;							\
    case IM_USHORT:						\
	IM_PIXEL(dd, x, y, us) = (uint16_t)(int64_t)dval;	\
	break;							\
    case IM_SHORT:						\
	IM_PIXEL(dd, x, y, s) = (int16_t)(int64_t)dval;		\
	break;							\
    case IM_ULONG:						\
	IM_PIXEL(dd, x, y, ul) = (uint32_t)(int64_t)dval;	\
	break;							\
    case IM_LONG:						\
	IM_PIXEL(dd, x, y, l) = (int32_t)(int64_t)dval;		\
	break;							\
    case IM_FLOAT:						\
	IM_PIXEL(dd, x, y, f) = (float)dval;			\
	break;							\
    case IM_DOUBLE:						\
	IM_PIXEL(dd, x, y, d) = dval;				\
	break;							\
    default:							\
	break;							\
    }

#define SET_PIXEL_VALUE_FROM_DCOMPLEX(dd, dval, x, y)	\
    switch (dd->type) {					\
    case IM_COMPLEX:					\
	IM_PIXEL(dd, x, y, c).r = (float)dval.r;	\
	IM_PIXEL(dd, x, y, c).i = (float)dval.i;	\
	break;						\
    case IM_DCOMPLEX:					\
        IM_PIXEL(dd, x, y, dc) = dval;			\
	break;						\
    default:						\
	break;						\
    }

#define SET_PIXEL_VALUE_FROM_RGBA(dd, dval, x, y)	\
    switch (dd->type) {					\
    case IM_RGB:					\
	IM_PIXEL(dd, x, y, r).r = dval.r;		\
	IM_PIXEL(dd, x, y, r).g = dval.g;		\
	IM_PIXEL(dd, x, y, r).b = dval.b;		\
	break;						\
    case IM_RGB16:					\
	IM_PIXEL(dd, x, y, r16).r = dval.r;		\
	IM_PIXEL(dd, x, y, r16).g = dval.g;		\
	IM_PIXEL(dd, x, y, r16).b = dval.b;		\
	break;						\
    case IM_RGBA:					\
	IM_PIXEL(dd, x, y, ra).r = dval.r;		\
	IM_PIXEL(dd, x, y, ra).g = dval.g;		\
	IM_PIXEL(dd, x, y, ra).b = dval.b;		\
	IM_PIXEL(dd, x, y, ra).a = dval.a;		\
	break;						\
    default:						\
	break;						\
    }


#define CLIP_TO_PIXEL_TYPE_LIMITS(val, im)	\
    do {					\
	if (val > ip_type_max(im->type))	\
	    val = ip_type_max(im->type);	\
	if (val < ip_type_min(im->type))	\
	    val = ip_type_min(im->type);	\
    } while(0)


/*
 * These operators operate on an image pixel by pixel, that is, the
 * destination pixel value can be calculated from itself and the
 * corresponding source pixel.
 */


/* Operating on integer type image only */
#define im_i_int_DEFN(op)						\
    void im_i_int_ ## op(ip_image *dd)					\
    {									\
	switch (dd->type) {						\
	case IM_BINARY:							\
	case IM_ULONG:							\
	case IM_ULONG64:						\
	    for (int j=0; j<dd->size.y; j++) {				\
		for (int i=0; i<dd->size.x; i++) {			\
		    uint64_t dval;					\
		    GET_PIXEL_VALUE_AS_UNSIGNED_LONG(dval, dd, i, j);	\
		    PIXEL_OPERATION_ ## op(dval, dd);			\
		    SET_PIXEL_VALUE_FROM_UNSIGNED_LONG(dd, dval, i, j);	\
		}							\
	    }								\
	    break;							\
	case IM_UBYTE:							\
	case IM_USHORT:							\
	case IM_BYTE:							\
	case IM_SHORT:							\
	case IM_LONG:							\
	case IM_LONG64:							\
	    for (int j=0; j<dd->size.y; j++) {				\
		for (int i=0; i<dd->size.x; i++) {			\
		    int64_t dval;					\
		    GET_PIXEL_VALUE_AS_SIGNED_LONG(dval, dd, i, j);	\
		    PIXEL_OPERATION_ ## op(dval, dd);			\
		    SET_PIXEL_VALUE_FROM_SIGNED_LONG(dd, dval, i, j);	\
		}							\
	    }								\
	    break;							\
	default:							\
	    break;							\
	}								\
    }


#define im_ii_int_DEFN(op)						\
    void im_ii_int_ ## op(ip_image *dd, ip_image *ss)			\
    {									\
	switch (dd->type) {						\
	case IM_BINARY:							\
	case IM_ULONG:							\
	case IM_ULONG64:						\
	    for (int j=0; j<dd->size.y; j++) {				\
		for (int i=0; i<dd->size.x; i++) {			\
		    uint64_t dval, sval;				\
		    GET_PIXEL_VALUE_AS_UNSIGNED_LONG_2(dval, sval, dd, ss, i, j); \
		    PIXEL_OPERATION_ ## op(dval, sval, dd);		\
		    SET_PIXEL_VALUE_FROM_UNSIGNED_LONG(dd, dval, i, j);	\
		}							\
	    }								\
	    break;							\
	case IM_UBYTE:							\
	case IM_USHORT:							\
	case IM_BYTE:							\
	case IM_SHORT:							\
	case IM_LONG:							\
	case IM_LONG64:							\
	    for (int j=0; j<dd->size.y; j++) {				\
		for (int i=0; i<dd->size.x; i++) {			\
		    int64_t dval, sval;					\
		    GET_PIXEL_VALUE_AS_SIGNED_LONG_2(dval, sval, dd, ss, i, j);	\
		    PIXEL_OPERATION_ ## op(dval, sval, dd);		\
		    SET_PIXEL_VALUE_FROM_SIGNED_LONG(dd, dval, i, j);	\
		}							\
	    }								\
	    break;							\
	default:							\
	    break;							\
	}								\
    }


/* ...where the result image is a different type to source image */
#define im_Xii_int_DEFN(op)						\
    void im_Xii_int_ ## op(ip_image *dd, ip_image *ss)			\
    {									\
	for (int j=0; j<dd->size.y; j++) {				\
	    for (int i=0; i<dd->size.x; i++) {				\
		int64_t dval, sval;					\
		GET_PIXEL_VALUE_AS_SIGNED_LONG(sval, ss, i, j);		\
		PIXEL_OPERATION_ ## op(dval, sval, dd);			\
		SET_PIXEL_VALUE_FROM_SIGNED_LONG(dd, dval, i, j);	\
	    }								\
	}								\
    }

#define im_Xiif_int_DEFN(op)						\
    void im_Xiif_int_ ## op(ip_image *dd, ip_image *ss, int flag)	\
    {									\
	for (int j=0; j<dd->size.y; j++) {				\
	    for (int i=0; i<dd->size.x; i++) {				\
		int64_t dval, sval;					\
		GET_PIXEL_VALUE_AS_SIGNED_LONG(sval, ss, i, j);		\
		PIXEL_OPERATION_ ## op(dval, sval, dd, flag);		\
		SET_PIXEL_VALUE_FROM_SIGNED_LONG(dd, dval, i, j);	\
	    }								\
	}								\
    }


#define im_ic_int_DEFN(op)						\
    void im_ic_int_ ## op(ip_image *dd, const double val)		\
    {									\
	uint64_t ulval = (uint64_t)val;					\
	int64_t lval = (int64_t)val;					\
	switch (dd->type) {						\
	case IM_ULONG:							\
	case IM_ULONG64:						\
	    for (int j=0; j<dd->size.y; j++) {				\
		for (int i=0; i<dd->size.x; i++) {			\
		    uint64_t dval;					\
		    GET_PIXEL_VALUE_AS_UNSIGNED_LONG(dval, dd, i, j);	\
		    PIXEL_OPERATION_ ## op(dval, ulval, dd);		\
		    SET_PIXEL_VALUE_FROM_UNSIGNED_LONG(dd, dval, i, j);	\
		}							\
	    }								\
	    break;							\
	case IM_UBYTE:							\
	case IM_USHORT:							\
	case IM_BYTE:							\
	case IM_SHORT:							\
	case IM_LONG:							\
	case IM_LONG64:							\
	    for (int j=0; j<dd->size.y; j++) {				\
		for (int i=0; i<dd->size.x; i++) {			\
		    int64_t dval;					\
		    GET_PIXEL_VALUE_AS_SIGNED_LONG(dval, dd, i, j);	\
		    PIXEL_OPERATION_ ## op(dval, lval, dd);		\
		    SET_PIXEL_VALUE_FROM_SIGNED_LONG(dd, dval, i, j);	\
		}							\
	    }								\
	    break;							\
	default:							\
	    break;							\
	}								\
    }



#define im_ii_float_DEFN(op)						\
    void im_ii_float_ ## op(ip_image *dd, ip_image *ss)			\
    {									\
	for (int j=0; j<dd->size.y; j++) {				\
	    for (int i=0; i<dd->size.x; i++) {				\
		float dval = IM_PIXEL(dd, i, j, f);			\
		float sval = IM_PIXEL(ss, i, j, f);			\
		PIXEL_OPERATION_ ## op(dval, sval, d);			\
	        IM_PIXEL(dd, i, j, f) = dval;				\
	        IM_PIXEL(ss, i, j, f) = sval;				\
	    }								\
	}								\
    }



#define im_ii_real_DEFN(op)						\
    void im_ii_real_ ## op(ip_image *dd, ip_image *ss)			\
    {									\
	switch (dd->type) {						\
	case IM_FLOAT:							\
	case IM_DOUBLE:							\
	    for (int j=0; j<dd->size.y; j++) {				\
		for (int i=0; i<dd->size.x; i++) {			\
		    double dval, sval;					\
		    GET_PIXEL_VALUE_AS_DOUBLE_2(dval, sval, dd, ss, i, j); \
		    PIXEL_OPERATION_ ## op(dval, sval, dd);		\
		    SET_PIXEL_VALUE_FROM_DOUBLE(dd, dval, i, j);	\
		}							\
	    }								\
	    break;							\
	default:							\
	    break;							\
	}								\
    }

#define im_Xii_real_DEFN(op)						\
    void im_Xii_real_ ## op(ip_image *dd, ip_image *ss)			\
    {									\
	for (int j=0; j<dd->size.y; j++) {				\
	    for (int i=0; i<dd->size.x; i++) {				\
		double dval, sval;					\
		GET_PIXEL_VALUE_AS_DOUBLE(sval, ss, i, j);		\
		PIXEL_OPERATION_ ## op(dval, sval, dd);			\
		SET_PIXEL_VALUE_FROM_DOUBLE(dd, dval, i, j);		\
	    }								\
	}								\
    }

#define im_Xiif_real_DEFN(op)						\
    void im_Xiif_real_ ## op(ip_image *dd, ip_image *ss, int flags)	\
    {									\
	for (int j=0; j<dd->size.y; j++) {				\
	    for (int i=0; i<dd->size.x; i++) {				\
		double dval, sval;					\
		GET_PIXEL_VALUE_AS_DOUBLE(sval, ss, i, j);		\
		PIXEL_OPERATION_ ## op(dval, sval, dd, flags);		\
		SET_PIXEL_VALUE_FROM_DOUBLE(dd, dval, i, j);		\
	    }								\
	}								\
    }

#define im_ic_float_DEFN(op)						\
    void im_ic_float_ ## op(ip_image *dd, double val)			\
    {									\
	float fval = (float)val;					\
	for (int j=0; j<dd->size.y; j++) {				\
	    for (int i=0; i<dd->size.x; i++) {				\
		float dval = IM_PIXEL(dd, i, j, f);			\
		PIXEL_OPERATION_ ## op(dval, fval, dd);			\
		IM_PIXEL(dd, i, j, f) = dval;				\
	    }								\
	}								\
    }

#define im_ic_real_DEFN(op)						\
    void im_ic_real_ ## op(ip_image *dd, double val)			\
    {									\
	for (int j=0; j<dd->size.y; j++) {				\
	    for (int i=0; i<dd->size.x; i++) {				\
		double dval;						\
		GET_PIXEL_VALUE_AS_DOUBLE(dval, dd, i, j);		\
		PIXEL_OPERATION_ ## op(dval, val, dd);			\
		SET_PIXEL_VALUE_FROM_DOUBLE(dd, dval, i, j);		\
	    }								\
	}								\
    }

#define im_ii_complex_DEFN(op)						\
    void im_ii_complex_ ## op(ip_image *dd, ip_image *ss)		\
    {									\
	for (int j=0; j<dd->size.y; j++) {				\
	    for (int i=0; i<dd->size.x; i++) {				\
		ip_complex dval = IM_PIXEL(dd, i, j, c);		\
		ip_complex sval = IM_PIXEL(ss, i, j, c);		\
		PIXEL_OPERATION_ ## op(dval, sval, dd);			\
	        IM_PIXEL(dd, i, j, c) = dval;				\
	    }								\
	}								\
    }

#define im_ii_dcomplex_DEFN(op)						\
    void im_ii_dcomplex_ ## op(ip_image *dd, ip_image *ss)		\
    {									\
	for (int j=0; j<dd->size.y; j++) {				\
	    for (int i=0; i<dd->size.x; i++) {				\
		ip_dcomplex dval = IM_PIXEL(dd, i, j, dc);		\
		ip_dcomplex sval = IM_PIXEL(ss, i, j, dc);		\
		PIXEL_OPERATION_ ## op(dval, sval, dd);			\
	        IM_PIXEL(dd, i, j, dc) = dval;				\
	    }								\
	}								\
    }

#define im_Xii_dcomplex_DEFN(op)					\
    void im_Xii_dcomplex_ ## op(ip_image *dd, ip_image *ss)		\
    {									\
	for (int j=0; j<dd->size.y; j++) {				\
	    for (int i=0; i<dd->size.x; i++) {				\
		ip_dcomplex sval, dval;					\
		GET_PIXEL_VALUE_AS_DCOMPLEX(sval, ss, i, j);		\
		GET_PIXEL_VALUE_AS_DCOMPLEX(dval, dd, i, j);		\
		PIXEL_OPERATION_ ## op(dval, sval, dd);			\
		SET_PIXEL_VALUE_FROM_DCOMPLEX(dd, dval, i, j);		\
	    }								\
	}								\
    }


#define im_ic_complex_DEFN(op)						\
    void im_ic_complex_ ## op(ip_image *dd, double u, double v)		\
    {									\
	ip_complex c = (ip_complex){(float)u, (float)v};		\
	for (int j=0; j<dd->size.y; j++) {				\
	    for (int i=0; i<dd->size.x; i++) {				\
		ip_complex dval = IM_PIXEL(dd, i, j, c);		\
		PIXEL_OPERATION_ ## op(dval, c, d);			\
		IM_PIXEL(dd, i, j, c) = dval;				\
	    }								\
	}								\
    }

#define im_ic_dcomplex_DEFN(op)						\
    void im_ic_dcomplex_ ## op(ip_image *dd, double u, double v)	\
    {									\
	ip_dcomplex c = (ip_dcomplex){u, v};				\
	for (int j=0; j<dd->size.y; j++) {				\
	    for (int i=0; i<dd->size.x; i++) {				\
		ip_dcomplex dval = IM_PIXEL(dd, i, j, dc);		\
		PIXEL_OPERATION_ ## op(dval, c, dd);			\
		IM_PIXEL(dd, i, j, dc) = dval;				\
	    }								\
	}								\
    }

#define im_Xii_rgb_DEFN(op)						\
    void im_Xii_rgb_ ## op(ip_image *dd, ip_image *ss)			\
    {									\
	for (int j=0; j<dd->size.y; j++) {				\
	    for (int i=0; i<dd->size.x; i++) {				\
		struct h_rgba sval, dval;				\
		GET_PIXEL_VALUE_AS_RGBA(sval, ss, i, j);		\
		GET_PIXEL_VALUE_AS_RGBA(dval, dd, i, j);		\
		PIXEL_OPERATION_ ## op(dval, sval, dd);			\
		SET_PIXEL_VALUE_FROM_RGBA(dd, dval, i, j);		\
	    }								\
	}								\
    }

#define im_Xii_palette_DEFN(op)						\
    void im_Xii_palette_ ## op(ip_image *dd, ip_image *ss)		\
    {									\
	for (int j=0; j<dd->size.y; j++) {				\
	    for (int i=0; i<dd->size.x; i++) {				\
		int sval;						\
		struct h_rgba  dval;					\
		sval = IM_PIXEL(ss, i, j, ub);				\
		GET_PIXEL_VALUE_AS_RGBA(dval, dd, i, j);		\
		PIXEL_OPERATION_ ## op(dval, sval, ss);			\
		SET_PIXEL_VALUE_FROM_RGBA(dd, dval, i, j);		\
	    }								\
	}								\
    }

/* Operate on image plus 4 parameters to generate binary image output */
#define im_bicccc_int_DEFN(op)                                          \
    void im_bicccc_int_ ## op(ip_image *dd, ip_image *ss, double p1, double p2, double p3, double p4) \
    {                                                                   \
        switch (ss->type) {                                             \
        case IM_ULONG:                                                  \
        case IM_ULONG64:                                                \
            for (int j=0; j<dd->size.y; j++) {                          \
                for (int i=0; i<dd->size.x; i++) {                      \
                    uint8_t dval;                                       \
                    unsigned long sval;                                 \
                    GET_PIXEL_VALUE_AS_UNSIGNED_LONG(sval, ss, i, j);   \
                    PIXEL_OPERATION_ ## op(dval, sval, dd, p1, p2 ,p3 ,p4); \
                    IM_PIXEL(dd,i,j,ub) = dval;                         \
                }                                                       \
            }                                                           \
            break;                                                      \
        case IM_UBYTE:                                                  \
        case IM_USHORT:                                                 \
        case IM_BYTE:                                                   \
        case IM_SHORT:                                                  \
        case IM_LONG:                                                   \
        case IM_LONG64:                                                 \
            for (int j=0; j<dd->size.y; j++) {                          \
                for (int i=0; i<dd->size.x; i++) {                      \
                    uint8_t dval;                                       \
                    long sval;                                          \
                    GET_PIXEL_VALUE_AS_SIGNED_LONG(sval, ss, i, j);     \
                    PIXEL_OPERATION_ ## op(dval, sval, dd, p1, p2, p3, p4); \
                    IM_PIXEL(dd,i,j,ub) = dval;                         \
                }                                                       \
            }                                                           \
            break;                                                      \
        default:                                                        \
            break;                                                      \
        }                                                               \
    }

#define im_bicccc_real_DEFN(op)                                         \
    void im_bicccc_real_ ## op(ip_image *dd, ip_image *ss, double p1, double p2, double p3, double p4) \
    {                                                                   \
        for (int j=0; j<dd->size.y; j++) {                              \
            for (int i=0; i<dd->size.x; i++) {                          \
                uint8_t dval;                                           \
                double sval;                                            \
                GET_PIXEL_VALUE_AS_DOUBLE(sval, ss, i, j);              \
                PIXEL_OPERATION_ ## op(dval, sval, d, p1, p2, p3, p4);  \
                IM_PIXEL(dd,i,j,ub) = dval;                             \
            }                                                           \
        }                                                               \
    }
